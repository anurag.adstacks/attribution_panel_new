//package com.attribution.panel.config;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import lombok.SneakyThrows;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.http.HttpHost;
//import org.apache.http.client.config.RequestConfig;
//import org.apache.http.impl.nio.client.HttpAsyncClientBuilder;
//import org.elasticsearch.client.RestClient;
//import org.elasticsearch.client.RestClientBuilder;
//import org.elasticsearch.client.RestHighLevelClient;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.data.elasticsearch.config.AbstractElasticsearchConfiguration;
//import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
//import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
//import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;
//
//
//@Configuration
//@EnableElasticsearchRepositories
//@Slf4j
//public class ElasticSearchRestClient extends AbstractElasticsearchConfiguration {
//
//    @Value("${clusterName}")
//    private String clusterName;
//    @Value("${host}")
//    private String host;
//    @Value("${port}")
//    private Integer port;
//    ObjectMapper objectMapper = new ObjectMapper();
//
//
//    @SneakyThrows
//    @Bean("elasticClient")
//    public RestHighLevelClient elasticsearchClient() {
//
//        RestClientBuilder builder = RestClient.builder(
//                        new HttpHost(host, port))
//                .setRequestConfigCallback(new RestClientBuilder.RequestConfigCallback() {
//                                              @Override
//                                              public RequestConfig.Builder customizeRequestConfig(RequestConfig.Builder builder) {
//                                                  return builder.setSocketTimeout(12000);
//                                              }
//                                          }
//                )
//                .setHttpClientConfigCallback(new RestClientBuilder.HttpClientConfigCallback() {
//                    @Override
//                    public HttpAsyncClientBuilder customizeHttpClient(
//                            HttpAsyncClientBuilder httpClientBuilder) {
////                        httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider);
////                         httpClientBuilder.setSSLContext(sslContext);
//                        return httpClientBuilder;
//                    }
//                });
//        RestHighLevelClient client = new RestHighLevelClient(builder);
//        return client;
//    }
//
//
//
///*    @SneakyThrows
//    @Bean("elasticClient")
//    public RestHighLevelClient elasticsearchClient() {
////        final CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
////        credentialsProvider.setCredentials(AuthScope.ANY,
////                new UsernamePasswordCredentials("elastic", "Adstacks@1230"));
//
//        RestClientBuilder builder = RestClient.builder(new HttpHost(host, port))
//                .setHttpClientConfigCallback(new RestClientBuilder.HttpClientConfigCallback() {
//                    @Override
//                    public HttpAsyncClientBuilder customizeHttpClient(HttpAsyncClientBuilder httpClientBuilder) {
////                         httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider);
//                        return httpClientBuilder;
//                    }
//                });
//
//        RestHighLevelClient client = new RestHighLevelClient(builder);
//        return client;
//    }*/
//
//
//    @Bean
//    public ElasticsearchOperations elasticsearchTemplate() {
//        return new ElasticsearchRestTemplate(elasticsearchClient());
//    }
//
//
///*    @Bean
//    public ElasticsearchOperations elasticsearchTemplate() {
//        return new ElasticsearchRestTemplate(elasticsearchClient());
//    }*/
//
//
///*    @Bean
//    @Override
//    public ElasticsearchCustomConversions elasticsearchCustomConversions() {
////        List<?> list = new ArrayList<>();
////        list.add(new StringToCHR_IdConverter());
////        list.add(new CHR_IdToStringConverter());
//        return new ElasticsearchCustomConversions(Arrays.asList(new StringToCHR_IdConverter()
////                , new CHR_IdToStringConverter()
//        )
//        );
//    }*/
//
//
////    @WritingConverter
////    public class CHR_IdToStringConverter implements Converter<CampaignHourlyRecordId, String> {
////
////        @Override
////        public String convert(CampaignHourlyRecordId source) {
////           return source.toString();
////        }
////    }
///*
//    @ReadingConverter
//    public class StringToCHR_IdConverter implements Converter<String, CampaignHourlyRecordId> {
//
//        @SneakyThrows
//        @Override
//        public CampaignHourlyRecordId convert(String source) {
////            Gson gson = new Gson(); // Or use new GsonBuilder().create();
////
//            source = source.substring(23,source.length()-1);
////            log.info("source:"+source);
////            Map<String, String> reconstructedUtilMap = Arrays.stream(source.split(","))
////                    .map(s -> s.split("="))
////                    .collect(Collectors.toMap(s -> s[0], s -> s[1]));
//
//            List<String> keyArr = Arrays.asList(source.split(","));//For multiple group by we get list of keys
//            Map<String, String> map = new HashMap<>();
//            for (String key : keyArr) {
//                List<String> stringList = Arrays.asList(key.split("="));
//                try {
//                    map.put(stringList.get(0).trim(), stringList.get(1).trim());
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//
////            log.info("map:"+map);
//
//            CampaignHourlyRecordId id = new CampaignHourlyRecordId(
//                    Long.parseLong(map.get("offerId")),
//                    Long.parseLong(map.get("pubId")),
//                    map.get("subAffiliate"),
//                    map.get("hourTimestamp"),
//                    (map.get("eventId").contains("null"))?null:Long.parseLong(map.get("eventId"))
//            );
////            CampaignHourlyRecordId campaignHourlyRecordId = gson.fromJson(source, CampaignHourlyRecordId.class);
////            return objectMapper.readValue(source,CampaignHourlyRecordId.class);
//            return id;
//        }
//    }*/
//
///*    @Override
//    public RestHighLevelClient elasticsearchClient() {
//
//        HttpHeaders compatibilityHeaders = new HttpHeaders();
//        compatibilityHeaders.add("Accept", "application/vnd.elasticsearch+json;compatible-with=7");
//        compatibilityHeaders.add("Content-Type", "application/vnd.elasticsearch+json;"
//                + "compatible-with=7");
//
//        ClientConfiguration clientConfiguration = ClientConfiguration.builder()
//                .connectedTo("${host}:80")
//                .withDefaultHeaders(compatibilityHeaders)
//                .build();
//
//        return RestClients.create(clientConfiguration).rest();
//    }*/
//
//   /* @SneakyThrows
//    @Bean("elasticClient")
//    public RestHighLevelClient elasticsearchClient() {
//        System.out.println("host " + host + "  " + port);
//
//        RestClientBuilder builder = RestClient.builder(
//                        new HttpHost(host, port))
//                .setRequestConfigCallback(builder1 -> builder1.setSocketTimeout(12000)
//                )
//                .setHttpClientConfigCallback(httpClientBuilder -> {
////                        httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider);
////                         httpClientBuilder.setSSLContext(sslContext);
//                    return httpClientBuilder;
//                });
//        RestHighLevelClient client = new RestHighLevelClient(builder);
//        return client;
//    }
//
//
//    @Bean
//    public ElasticsearchOperations elasticsearchTemplate() {
//        return new ElasticsearchRestTemplate(elasticsearchClient());
//    }*/
//
//}
//
