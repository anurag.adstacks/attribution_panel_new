package com.attribution.panel.Controller;


import com.attribution.panel.bean.App;
import com.attribution.panel.bean.Event;
import com.attribution.panel.bean.Partner;
import com.attribution.panel.constants.BackendConstants;
import com.attribution.panel.enums.AccessStatus;
import com.attribution.panel.enums.PayoutModel;
import com.attribution.panel.enums.RevenueModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping("/event")
public class EventController {

    @Autowired
    BackendConstants backendConstants;

    RestTemplate restTemplate = new RestTemplate();


    @PostMapping(value = "/saveNewEvent")
    public ResponseEntity<?> saveNewEvent(
            @RequestParam(value = "eventName", required = false) String eventName,
            @RequestParam(value = "ajaxPreDefineEvent", required = false) String ajaxPreDefineEvent,
            @RequestParam(value = "description", required = false) String description,
            @RequestParam(value = "status", required = false) int status,
            @RequestParam(value = "revenueModel", required = false) RevenueModel revenueModel,
            @RequestParam(value = "revenueModel1", required = false) Float revenueModel1,
            @RequestParam(value = "multiConv", required = false, defaultValue = "false") Boolean multiConv,
            @RequestParam(value = "token", required = false) String token,
            @RequestParam(value = "appId", required = false) Long appId,
            @RequestParam(value = "accessStatus", required = false) String accessStatus,
            @RequestParam(value = "partnersInEvent", required = false) List<Long> partnersInEvent) {
        System.out.println("hello hi bye bye0 " + ajaxPreDefineEvent);
        System.out.println("hello hi bye bye1 " + AccessStatus.valueOf(accessStatus));
        System.out.println("hello hi bye bye2 " + description);
        System.out.println("partnersInEvent " + partnersInEvent + " " + status);

        App app = restTemplate.getForObject(backendConstants.baseUrlDbAccess + "/adminConsole/findAppById?id=" + appId, App.class);


        Event event = new Event();
        event.setEventName(eventName);
        event.setEventValue("101.1");;
        event.setRevenue(revenueModel1);
        event.setEventRevenueCurrency("INR");
        event.setEventRevenueUsd(1.18);
        event.setDescription(description);
        event.setStatus(status == 0 ? true : false); // active - inactive
        event.setToken(token);
        event.setRevenueModel(revenueModel);
        event.setPayout((float) 1.04);
        event.setMultiConv(multiConv);
        event.setPayoutModel(PayoutModel.CPI);
        event.setAccessStatus(AccessStatus.valueOf(accessStatus));
        event.setEventKey(UUID.randomUUID().toString().replace("-", ""));
        event.setApp(app);
        event.setEventCount(3);
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        event.setCreationDateTime(dateFormat.format(new Date()));
        event.setUpdateDateTime(dateFormat.format(new Date()));

        if (accessStatus.equals("NeedApproval")) {
            List<Partner> partner = restTemplate.exchange(backendConstants.baseUrlDbAccess +"/adminConsole/findAllByPartnerId/{partnersInEvent}",
                    HttpMethod.GET, null, new ParameterizedTypeReference<List<Partner>>() {
                    },partnersInEvent).getBody();
            event.setPartners(partner);

        }
//        eventService.save(event);
        restTemplate.postForEntity(backendConstants.baseUrlDbAccess + "/adminConsole/saveEvent", event, Event.class);
        return ResponseEntity.ok().body("ok");
    }




    @PostMapping(value = "/savePreNewEvent")
    public ResponseEntity<?> savePreNewEvent(
            @RequestParam(value = "ajaxPreDefineEvent", required = false) List<Long> ajaxPreDefineEvent,
            @RequestParam(value = "appId", required = false) Long appId
    ) {

        restTemplate.getForObject(backendConstants.baseUrlDbAccess + "/adminConsole/savePreNewEvent/{ajaxPreDefineEvent}/{appId}", List.class, ajaxPreDefineEvent, appId);

        System.out.println("hello hi bye bye0 " + ajaxPreDefineEvent);
        System.out.println("hello hi bye bye1 ");

        return ResponseEntity.ok().body("ok");

    }


    @RequestMapping(value = "/findByPreDefineEvent", method = RequestMethod.GET)
    public ResponseEntity<?> findByPreDefineEvent(@RequestParam(value = "search", defaultValue = "", required = false) String search,
                                                  @RequestParam("page") Integer page,
                                                  @RequestParam("size") Integer size) {
        System.out.println("pubListYo2 ");
        Long searchLong = null;
        try {
            searchLong = (search.equals("")) ? null : Long.parseLong(search);
        } catch (Exception e) {
        }

        ResponseEntity<Map<String, Object>> responseSelectDTOS = restTemplate.exchange(backendConstants.baseUrlDbAccess + "/adminConsole/findByPreDefineEvent?search={search}&page={page}&size={size}",
                HttpMethod.GET, null, new ParameterizedTypeReference<Map<String, Object>>() {
                }, search, page, size);
        Map<String, Object> map = responseSelectDTOS.getBody();


        return ResponseEntity.ok().body(map);
    }

    @GetMapping(value = "/findByEventAjax")
    public ResponseEntity<?> findByEventAjax(@RequestParam(value = "appId") Long appId) {

        ResponseEntity<Map<String, Object>> responseSelectDTOS = restTemplate.exchange(backendConstants.baseUrlDbAccess + "/adminConsole/findByEventAjax?appId={appId}",
                HttpMethod.GET, null, new ParameterizedTypeReference<Map<String, Object>>() {
                }, appId);

        Map<String, Object> map = responseSelectDTOS.getBody();
        return ResponseEntity.ok().body(map);
    }


    @GetMapping(value = "/findByEventAjax1")
    public ResponseEntity<?> findByEventAjax1(@RequestParam(value = "appId") Long appId,
                                              @RequestParam(value = "dateRange") String dateRange,
                                              @RequestParam(value = "selectEvent") String selectEvent
    ) {
        ResponseEntity<Map<String, Object>> responseSelectDTOS = restTemplate.exchange(backendConstants.baseUrlDbAccess + "/adminConsole/findByEventAjax1?appId={appId}&dateRange={dateRange}&selectEvent={selectEvent}",
                HttpMethod.GET, null, new ParameterizedTypeReference<Map<String, Object>>() {
                }, appId, dateRange, selectEvent);

        Map<String, Object> map = responseSelectDTOS.getBody();
        return ResponseEntity.ok().body(map);
    }


    @GetMapping(value = "/eventTracking")
    public ResponseEntity<?> attributionPostback(@RequestParam(value = "eventKey") String eventKey,
                                                 @RequestParam(value = "param1") String param1,
                                                 @RequestParam(value = "param2") String param2,
                                                 @RequestParam(value = "param3") String param3,
                                                 @RequestParam(value = "param4") String param4,
                                                 @RequestParam(value = "param5") String param5,
                                                 @RequestParam(value = "param6") String param6,
                                                 @RequestParam(value = "param7") String param7,
                                                 @RequestParam(value = "param8") String param8,
                                                 @RequestParam(value = "param9") String param9,
                                                 @RequestParam(value = "param10") String param10) {
        Map<String, Object> data = new HashMap<String, Object>();
//        Event event = eventService.findByEventKey(eventKey);

        Event event = restTemplate.getForObject(backendConstants.baseUrlDbAccess + "/adminConsole/findByEventKey?eventKey=" + eventKey, Event.class);

        restTemplate.postForEntity(backendConstants.baseUrlDbAccess + "/adminConsole/saveEvent", event, Event.class);

        return ResponseEntity.ok().body("Save Event");
    }

    @GetMapping("/getEventById/{eventId}")
    public ResponseEntity<?> getEventById(@PathVariable("eventId") Long eventId, Map<String, Object> model) {
        Map<String, Long> advParam = new HashMap<>();

        Event event = restTemplate.getForObject(backendConstants.baseUrlDbAccess + "/adminConsole/findByEventId/{eventId}", Event.class, eventId);

//        Event event = eventService.findById(eventId);
        model.put("event", event);
        return ResponseEntity.ok().body(model);
    }

    @GetMapping(value = "/deleteEventById/{id}")
    public ResponseEntity<?> getDelete(@PathVariable("id") Long id,
                                       Map<String, Object> model) {

        restTemplate.getForObject(backendConstants.baseUrlDbAccess + "/adminConsole/deleteEventById/{id}", Event.class, id);

        return ResponseEntity.ok().body("ok");
    }

    public Map<String, String> getDates(String dateRange) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat formatterForJsDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String startDate = null;
        String endDate = null;
        if (dateRange.equals("")) {
            endDate = formatter.format(new Date());
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, -7);
            startDate = formatter.format(cal.getTime());
        } else {
            String[] startDateArr = dateRange.split(" - ");
            try {
                startDate = formatter.format(formatterForJsDate.parse(startDateArr[0]));
                endDate = formatter.format(formatterForJsDate.parse(startDateArr[1]));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Map<String, String> mp = new HashMap<>();
        mp.put("startDate", startDate);
        mp.put("endDate", endDate);
        return mp;
    }

//
//
//    @RequestMapping(value = "/saveEvent", method = RequestMethod.POST)
//    public ResponseEntity<?> saveLeave(@RequestParam(value = "appId") Long appId,
//                                       @RequestParam(value = "eventId") Long eventId,
//                                       @RequestParam(value = "eventName") String eventName,
//                                       HttpServletRequest request, Map<String, Object> model) {
//        App app = appService.findById(appId);
//        Event event = new Event();
//        if (eventId != null) {
//            event = eventService.findById(eventId);
//            event.setEventName(eventName);
//        } else {
//            String eventKey = RandomStringUtils.randomAlphanumeric(12);
//            event.setEventName(eventName);
//            event.setEventKey(eventKey);
//            event.setApp(app);
//        }
//        eventService.save(event);
//        return ResponseEntity.ok().body("Saved Event");
//    }


}
