package com.attribution.panel.Controller;

import com.attribution.panel.bean.Event;
import com.attribution.panel.bean.UserEvent;
import com.attribution.panel.constants.BackendConstants;
import com.attribution.panel.constants.UrlConstants;
import com.attribution.panel.enums.CountryEnum;
import com.attribution.panel.service.IpRequestService;
import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.model.CityResponse;
import com.maxmind.geoip2.record.Country;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;

import static com.attribution.panel.constants.BackendConstants.allReport;

@Controller
@RequestMapping("/event")
@Slf4j
public class UserEventController {

    @Autowired
    BackendConstants backendConstants;

    RestTemplate restTemplate = new RestTemplate();

    @Autowired
    IpRequestService ipRequestService;

    @GetMapping(value = "/saveUserEvent")
    public ResponseEntity<?> saveUserEvent(@RequestParam(value = "eventKey") String eventKey,
                                           @RequestParam(value = "appId") Long appId,
                                           @RequestParam(value = "param1") String param1,
                                           @RequestParam(value = "param2") String param2,
                                           @RequestParam(value = "param3") String param3,
                                           @RequestParam(value = "param4") String param4,
                                           @RequestParam(value = "param5") String param5,
                                           @RequestParam(value = "param6") String param6,
                                           @RequestParam(value = "param7") String param7,
                                           @RequestParam(value = "param8") String param8,
                                           @RequestParam(value = "param9") String param9,
                                           @RequestParam(value = "param10") String param10,
                                           @RequestParam(value = "os") String os,
                                           @RequestParam(value = "device") String device,
                                           @RequestParam(value = "google_aid") String google_aid,
                                           HttpServletRequest request, Map<String, Object> model) throws Exception {
        CountryEnum countrys = null;
        String ip = null;

        Event event = restTemplate.getForObject(backendConstants.baseUrlDbAccess + "/adminConsole/findByEventKey/{eventKey}", Event.class, eventKey);
        System.out.println("saveUserEvent " + event);

        if(event!=null) {
            Long count = restTemplate.getForObject(backendConstants.baseUrlDbAccess + "/adminConsole/findByEventAndGoogle_aid/{appId}/{event}/{google_aid}", Long.class, appId, event,google_aid);
            System.out.println("saveUserEvent2 " + count);
//            Long count = userEventService.findByEventAndGoogle_aid(appId, event, google_aid);

            UserEvent userEvent = new UserEvent();
            if (count == 0) {

                ip = ipRequestService.getClientIp(request);

                File database = new File(UrlConstants.DATABASE_CITY_PATH);
                File database1 = new File(UrlConstants.DATABASE_COUNTRY_PATH);
                DatabaseReader reader = new DatabaseReader.Builder(database).build();
                DatabaseReader reader1 = new DatabaseReader.Builder(database1).build();

//        File dbFile = new File(UrlConstants.DATABASE_CITY_PATH);

                // This creates the DatabaseReader object,
                // which should be reused across lookups.

//        DatabaseReader reader = new DatabaseReader.Builder(dbFile).build();

                // A IP Address
                InetAddress ipAddress = InetAddress.getByName(ip);
                System.out.println("InetAddress " + ipAddress);

                // Get City info
                CityResponse response = reader.city(ipAddress);

                // Country Info
                Country country = response.getCountry();
                System.out.println("Country IsoCode: " + country.getIsoCode()); // 'US'
                System.out.println("Country Name: " + country.getName()); // 'United States'


                userEvent.setEvent(event);
                userEvent.setParam1(param1);
                userEvent.setParam2(param2);
                userEvent.setParam3(param3);
                userEvent.setParam4(param4);
                userEvent.setParam5(param5);
                userEvent.setParam6(param6);
                userEvent.setParam7(param7);
                userEvent.setParam8(param8);
                userEvent.setParam9(param9);
                userEvent.setParam10(param10);
                userEvent.setIp(ip);
                userEvent.setCountry(country.getName());
                userEvent.setDeviceName(device);
                userEvent.setGoogle_aid(google_aid);
                userEvent.setOs(os);
                userEvent.setAppId(appId);

                restTemplate.postForEntity(backendConstants.baseUrlDbAccess + "/adminConsole/saveUserEvent", userEvent, UserEvent.class);
                return ResponseEntity.ok().body("Saved User Event");
            } else
                return ResponseEntity.ok().body("User Event Already Exist");
        }else
            return ResponseEntity.ok().body("Event does not exist");
    }

    /*@GetMapping(value = "/findByUserEventAjax")
    public ResponseEntity<?> findByEventAjax(@RequestParam(value = "appId") Long appId, @RequestParam(value = "dateRange") String dateRange) {
        Map<String, Object> data = new HashMap<String, Object>();
            App app=appService.findById(appId);
        Map<String, String> map = getDates(dateRange);
        String startDate = map.get("startDate");
        String endDate = map.get("endDate");
            List<UserEvent> userEventList=userEventService.findByAppIdAndStartBetweenEndDate(appId, startDate, endDate);
            data.put("userEventList", userEventList);
        return ResponseEntity.ok().body(data);
    }*/


//    public Map<String, String> getDates(String dateRange) {
//        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        SimpleDateFormat formatterForJsDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        String startDate = null;
//        String endDate = null;
//        if (dateRange.equals("")) {
//            endDate = formatter.format(new Date());
//            Calendar cal = Calendar.getInstance();
//            cal.add(Calendar.DATE, -7);
//            startDate = formatter.format(cal.getTime());
//        } else {
//            String[] startDateArr = dateRange.split(" - ");
//            try {
//                startDate = formatter.format(formatterForJsDate.parse(startDateArr[0]));
//                endDate = formatter.format(formatterForJsDate.parse(startDateArr[1]));
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//        Map<String, String> mp = new HashMap<>();
//        mp.put("startDate", startDate);
//        mp.put("endDate", endDate);
//        return mp;
//    }

    @GetMapping("/getUserEventAjax")
    public ResponseEntity<?> getOffersAjax(@RequestParam(value = "id", required = false, defaultValue = "0") Long appId, DataTablesInput search) {
        // TODO Auto-generated method stub


        System.out.println("Inside get getAllReportAjax " + appId + " " + search );

        Map<String, Object> data = new HashMap<>();
        data.put("id", appId);
        data.put("dataTablesInput", search);

        ResponseEntity<String> response = restTemplate.postForEntity(backendConstants.baseUrlDbAccess + "/adminConsole/getUserEventAjax", data, String.class);



//        ResponseEntity<Map<String, Object>> map = restTemplate.exchange(backendConstants.baseUrlDbAccess + "/adminConsole/getUserEventAjax{search}{appId}",
//                HttpMethod.GET, null, new ParameterizedTypeReference<Map<String, Object>>() {
//                }, search, appId);

//        Map<String, Object> res = userEventService.listUserEventAjax(search, appId);
        return ResponseEntity.ok().body(response.getBody());
    }


}
