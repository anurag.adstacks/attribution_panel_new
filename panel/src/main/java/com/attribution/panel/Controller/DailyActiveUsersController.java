//package com.attribution.panel.Controller;
//
//import com.adstacks.attribution.bean.*;
//import com.adstacks.attribution.enums.CountryEnum;
//import com.adstacks.attribution.service.*;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.ResponseEntity;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//
//import javax.servlet.http.HttpServletRequest;
//import java.text.SimpleDateFormat;
//import java.util.Calendar;
//import java.util.Map;
//
//@Controller
//@RequestMapping("/dailyActiveUsers")
//public class DailyActiveUsersController {
//    @Autowired
//    AppService appService;
//    @Autowired
//    IpRequestService ipRequestService;
//    @Autowired
//    DailyActiveUsersService dailyActiveUsersService;
//    @Autowired
//    InstallReportService installReportService;
//
//    @Autowired
//    PartnerService partnerService;
//
//    @GetMapping(value = "/saveDAU")
//    public ResponseEntity<?> saveData(@RequestParam(value = "appID") Long appID,
//                                      @RequestParam(value = "partnerId") Long partnerId,
//                                      @RequestParam(value = "os") String os,
//                                      @RequestParam(value = "device") String device,
//                                      @RequestParam(value = "google_aid") String google_aid,
//                                      HttpServletRequest request, Map<String, Object> model) throws Exception {
//        System.out.println("google_aid  = "+google_aid+"device = "+device+"os = "+os+"appId = "+appID);
//        CountryEnum country = null;
//        String ip = null;
//        App app = appService.findById(appID);
//        Partner partner = partnerService.findById(partnerId);
//        String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
//        InstallReport installReport=installReportService.findByAppIdAndGaid(appID, google_aid);
//        InstallReport installReport1 = null;
//        /*if(installReport!=null)
//            installReport1=installReportService.findByAppIdAndGoogle_aidAndDate(app, google_aid, currentDate);
//        System.out.println("installReport1 = "+installReport1);*/
//        if(installReport!=null) {
//            DailyActiveUsers dailyActiveUsers = dailyActiveUsersService.findByAppIDAndGaid(appID, google_aid, currentDate);
//            System.out.println("dailyActiveUsers  = "+dailyActiveUsers);
//            ip = ipRequestService.getClientIp(request);
//            String countryCode = request.getHeader("CF-IPCountry");
//            try {
//                country = CountryEnum.valueOf(countryCode);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            if (dailyActiveUsers == null) {
//                dailyActiveUsers = new DailyActiveUsers();
//                dailyActiveUsers.setIp(ip);
//                dailyActiveUsers.setCountry(String.valueOf(country));
//                dailyActiveUsers.setDeviceName(device);
//                dailyActiveUsers.setGaid(google_aid);
//                dailyActiveUsers.setOs(os);
////                dailyActiveUsers.setApp(app);
//                dailyActiveUsers.setAppID(appID);
//                dailyActiveUsers.setAppName(app.getName());
//                dailyActiveUsers.setPartnerId(partnerId);
//                dailyActiveUsers.setPartnerName(partner.getName());
//                dailyActiveUsersService.save(dailyActiveUsers);
//                return ResponseEntity.ok().body("Save Daily Active User");
//            } else
//                return ResponseEntity.ok().body("Daily Active User Already Exist");
//        } else
//            return ResponseEntity.ok().body("User does not exist");
//    }
//
////    @GetMapping(value = "/findDUAByAppId")
////    public ResponseEntity<?> countDAU(@RequestParam(value = "appId") Long appId) {
////        Map<String, Object> data = new HashMap<String, Object>();
////        String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
////        Long count = dailyActiveUsersService.findByAppIDAndToday(appId, currentDate);
////        data.put("count", count);
////        return ResponseEntity.ok().body(data);
////    }
//
//
//}
