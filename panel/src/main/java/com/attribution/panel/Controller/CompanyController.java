package com.attribution.panel.Controller;

import com.attribution.panel.bean.Company;
import com.attribution.panel.bean.User;
import com.attribution.panel.constants.BackendConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

@Controller
public class CompanyController {

    @Autowired
    BackendConstants backendConstants;

    RestTemplate restTemplate = new RestTemplate();

    BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();


//    @PostMapping("/registrationCompany")
//    public ResponseEntity<?> registrationCompany(@RequestParam("fName") String firstName,
//            @RequestParam("lName") String lastName,
//            @RequestParam("email") String email,
//            @RequestParam("password") String password,
//            @RequestParam("confirmPassword") String confirmPassword,
//            @RequestParam("imType") String imType){
//
//        System.out.println("inside company Controller");
//
//        User user = new User();
//        Company company = new Company();
//
//        company.setFName(firstName);
//        company.setLName(lastName);
//        company.setEmail(email);
//        company.setPassword(bCryptPasswordEncoder.encode(password));
//        company.setConfirmPassword(confirmPassword);
//        company.setImType(imType);
//        companyService.saveCompany(company);
//
//        user.setFName(firstName);
//        user.setLName(lastName);
//        user.setEmail(email);
//        user.setPassword(bCryptPasswordEncoder.encode(password));
//        user.setConfirmPassword(confirmPassword);
//        user.setImType(imType);
//        userService.saveUser(user);
//
//        return ResponseEntity.ok().body("Saved");
//    }

    @RequestMapping(value = "/registrationCompany", method = RequestMethod.POST)
    public ResponseEntity<?> registrationCompany(@RequestParam("fName") String firstName,
                                        @RequestParam("lName") String lastName,
                                        @RequestParam("email") String email,
                                        @RequestParam("password") String password,
                                        @RequestParam("confirmPassword") String confirmPassword,
                                        @RequestParam("imType") String imType) throws IOException {

        User user = new User();
        Company company = new Company();

        company.setFName(firstName);
        company.setLName(lastName);
        company.setEmail(email);
        company.setPassword(bCryptPasswordEncoder.encode(password));
        company.setConfirmPassword(confirmPassword);
        company.setImType(imType);

        ResponseEntity<Company> company1 = restTemplate.postForEntity(backendConstants.baseUrlDbAccess + "/adminConsole/registrationCompany", company, Company.class);

        System.out.println("InsideregistrationCompany " + company1.getBody());


        user.setFName(firstName);
        user.setLName(lastName);
        user.setEmail(email);
        user.setPassword(bCryptPasswordEncoder.encode(password));
        user.setConfirmPassword(confirmPassword);
        user.setImType(imType);

        ResponseEntity<User> user1 = restTemplate.postForEntity(backendConstants.baseUrlDbAccess + "/adminConsole/registrationUser", user, User.class);
        System.out.println("InsideregistrationCompany " + user1.getBody());


        return ResponseEntity.ok().body("ok");
    }

}
