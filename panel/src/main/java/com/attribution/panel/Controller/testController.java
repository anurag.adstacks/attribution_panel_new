//package com.attribution.panel.Controller;
//
//import com.adstacks.attribution.service.IpRequestService;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.ResponseEntity;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//
//import javax.servlet.http.HttpServletRequest;
//
//@Controller
//@Slf4j
//@RequestMapping("/track")
//public class testController {
//    @Autowired
//    IpRequestService ipRequestService;
//
//    //Attribution platform api
//    @GetMapping("/testAttributionPostback")
//    public ResponseEntity<?> testAttributionPostback(@RequestParam(name = "offerId", required = false, defaultValue = "0") Long offerId,
//                                                     @RequestParam(name = "affid", required = false, defaultValue = "0") Long affid,
//                                                     @RequestParam(name = "token", required = false, defaultValue = "") String token,
//                                                     @RequestParam(name = "gaid", required = false, defaultValue = "") String gaid,
//                                                     @RequestParam(name = "userAgent", required = false, defaultValue = "") String userAgent,
//                                                     @RequestParam(value = "s1", required = false, defaultValue = "") String s1,
//                                                     @RequestParam(value = "s2", required = false, defaultValue = "") String s2,
//                                                     @RequestParam(value = "s3", required = false, defaultValue = "") String s3,
//                                                     @RequestParam(value = "s4", required = false, defaultValue = "") String s4,
//                                                     @RequestParam(value = "s5", required = false, defaultValue = "") String s5,
//                                                     @RequestParam(value = "isApk", required = false, defaultValue = "false") Boolean isApk,
//                                                     @RequestParam(name = "ip", required = false, defaultValue = "") String ip,
//                                                     HttpServletRequest request) {
//        userAgent = request.getHeader("User-Agent");
//        log.info("userAgent = " + userAgent);
//        log.info("IP Address = " + ip);
//        log.info("Event Name = " + token);
//        log.info("Offer Id = " + offerId);
//        log.info("affid = " + affid);
//        log.info("gaid = " + gaid);
//        ip = ipRequestService.getClientIp(request);
//        log.info("IP Address header = " + ip);
//        return ResponseEntity.ok().body("Ok");
//    }
//
//    //Attribution platform api
//    @GetMapping("/testDeviceUniqueIdentifierId")
//    public ResponseEntity<?> testAttributionPostback(@RequestParam(name = "deviceId", required = false, defaultValue = "0") String deviceId,
//                                                     HttpServletRequest request) {
//        log.info("deviceUniqueIdentifierId = " + deviceId);
//        return ResponseEntity.ok().body("Ok");
//    }
//}
