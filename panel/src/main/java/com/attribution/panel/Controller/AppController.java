package com.attribution.panel.Controller;


import com.attribution.panel.bean.App;
import com.attribution.panel.constants.BackendConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Controller
public class AppController {

    RestTemplate restTemplate = new RestTemplate();

    @Autowired
    BackendConstants backendConstants;

    @RequestMapping(value = "/saveApp", method = RequestMethod.POST)
    public ResponseEntity<?> saveNewApp(@ModelAttribute("userFormData") App app,
                                        @RequestParam("appLogo") MultipartFile file) throws IOException {

        app.setAppLogo(file.getBytes());

        ResponseEntity<App> offer1 = restTemplate.postForEntity(backendConstants.baseUrlDbAccess + "/adminConsole/saveApp", app, App.class);
        app = offer1.getBody();
        System.out.println("InsideSaveApp " + app.getId());

        return ResponseEntity.ok().body("ok");
    }

/*    @RequestMapping(value = "/findByOfferNames", method = RequestMethod.GET)
    public Map<String, Object> findByOfferNames(@RequestParam(value = "search", defaultValue = "", required = false) String search,
                                                @RequestParam("page") Integer page,
                                                @RequestParam("size") Integer size) {
        Long searchLong = null;
        try {
            searchLong = (search.equals("")) ? null : Long.parseLong(search);
        } catch (Exception e) {
        }
        Pageable pageRequest = PageRequest.of(page, size);
        Page<App> offerList = appService.findByNameContainingIgnoreCaseOrId(search, searchLong, pageRequest);
        System.out.println("samikshaOggfrs "+offerList);
        List<AjaxSelectDTO> selectDTOS = new ArrayList<>();
        for (App offer : offerList) {
            selectDTOS.add(new AjaxSelectDTO(offer));
        }

        ResponseEntity<Map<String, Object>> responseSelectDTOS = restTemplate.exchange(backendConstants.baseUrlDbAccess + "/adminConsole/findByPublisherCompany?search={search}&page={page}&size={size}",
                HttpMethod.GET, null, new ParameterizedTypeReference<Map<String, Object>>() {
                }, search, page, size);
        Map<String, Object> map = responseSelectDTOS.getBody();

        System.out.println("findByOfferNames1 " +selectDTOS);
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("results", selectDTOS);
        map.put("count_filtered", offerList.getTotalPages());
        return map;
    }*/


    @GetMapping("/findByAppNames")
    public ResponseEntity<?> findByAppNames(@RequestParam(value = "search", defaultValue = "", required = false) String search,
                                              @RequestParam("page") Integer page,
                                              @RequestParam("size") Integer size) {

        ResponseEntity<Map<String, Object>> responseSelectDTOS = restTemplate.exchange(backendConstants.baseUrlDbAccess + "/adminConsole/findByAppNames?search={search}&page={page}&size={size}",
                HttpMethod.GET, null, new ParameterizedTypeReference<Map<String, Object>>() {
                }, search, page, size);
        Map<String, Object> map = responseSelectDTOS.getBody();
        System.out.println("findByAppNames " + map.size());
        return ResponseEntity.ok().body(map);

    }


/*    @RequestMapping(value = {"/adminConsole/appRedirect"},method = RequestMethod.GET)
    public String appRedirect(Principal principal, Map<String, Object> model){
        return "jsp/apps";
    }


    @RequestMapping(value = {"/adminConsole/getConfiguration"},method = RequestMethod.GET)
    public String getConfiguration(Principal principal, Map<String, Object> model){
        return "jsp/Configuration";
    }*/


    @RequestMapping(value = "/imageAppLogoDisplay", method = RequestMethod.GET)
    public void showImage(@RequestParam("id") long id, HttpServletResponse response, HttpServletRequest request)
            throws IOException {
//        App user = appService.findById(id);
        App app = restTemplate.getForObject(backendConstants.baseUrlDbAccess + "/adminConsole/findAppById?id=" + id, App.class);

//        response.setContentType("image/jpeg, image/jpg, image/png, image/gif");
//        response.getOutputStream().write(app.getAppLogo());
        response.getOutputStream().close();
    }

    @PostMapping("/getAppsListAjax")
    public ResponseEntity<?> getAppsAjax(@RequestParam(value="search", required = false, defaultValue = "") String searchVal,
                                          @RequestParam("start") int start,
                                          @RequestParam("end") int end, Map<String, Object> model) {

        ResponseEntity<Map<String, Object>> responseSelectDTOS = restTemplate.exchange(backendConstants.baseUrlDbAccess + "/adminConsole/getAppsListAjax?search={searchVal}&start={start}&end={end}",
                HttpMethod.GET, null, new ParameterizedTypeReference<Map<String, Object>>() {
                }, searchVal, start, end);
        System.out.println("getAppsListAjax " + responseSelectDTOS);

        return ResponseEntity.ok().body(responseSelectDTOS.getBody());
    }

    @GetMapping("/findAppById")
    public ResponseEntity<?> findAppById(@RequestParam(value = "appId") Long appId, HttpServletRequest request) {
        Map<String, Object> data = new HashMap<String, Object>();
        App app = restTemplate.getForObject(backendConstants.baseUrlDbAccess + "/adminConsole/findAppById?id=" + appId, App.class);

        data.put("app", app);
        return ResponseEntity.ok().body(data);
    }
    @RequestMapping(value = "/uploadImageDocument", method = RequestMethod.POST)
    public ResponseEntity<?> uploadIODocument(@RequestParam(value = "documentImageFile", required = false) MultipartFile documentImageFile,
                                              @RequestParam(value = "appId", required = false) Long appId,
                                              HttpServletRequest request, Map<String, Object> model) throws IOException {
        System.out.println("Inside Upload Image Document");
        Map<String, Object> data = new HashMap<String, Object>();
        ResponseEntity<Map<String, Object>> map = restTemplate.exchange(backendConstants.baseUrlDbAccess + "/adminConsole/uploadImageDocument?documentImageFile={documentImageFile}&appId={appId}",
                HttpMethod.GET, null, new ParameterizedTypeReference<Map<String, Object>>() {
                }, documentImageFile, appId);
//        Map<String, Object> map = appService.saveFile(documentImageFile, app);
        return ResponseEntity.ok().body(data);

    }

    @GetMapping("/findDownloadImageFile")
    public ResponseEntity<?> findDownloadIOFile(@RequestParam(value = "appId") Long appId, HttpServletRequest request) {


        ResponseEntity<Map<String, Object>> map = restTemplate.exchange(backendConstants.baseUrlDbAccess + "/adminConsole/findDownloadImageFile?appId={appId}",
                HttpMethod.GET, null, new ParameterizedTypeReference<Map<String, Object>>() {
                }, appId);

        System.out.println("findDownloadIOFile " + map.getBody());
        return ResponseEntity.ok().body(map.getBody());
    }


}
