package com.attribution.panel.Controller;

import com.attribution.panel.constants.BackendConstants;
import nl.basjes.parse.useragent.UserAgent;
import nl.basjes.parse.useragent.UserAgentAnalyzer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/install")
public class InstallReportController {



    static UserAgentAnalyzer uaa;
    static UserAgent userAgent;
    RestTemplate restTemplate = new RestTemplate();

    @Autowired
    BackendConstants backendConstants;


//  http://localhost:9098/install/saveInstall?appId=1&partnerId=2&os={os}&device={device}&google_aid={google_aid}




    @GetMapping(value = "/installCount")
    public ResponseEntity<?> installCount(@RequestParam(value = "appId") Long appId, @RequestParam(value = "dateRange") String dateRange) {
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String, String> map = getDates(dateRange);
        String startDate = map.get("startDate");
        String endDate = map.get("endDate");
        System.out.println("installCountIs " +startDate + " " + endDate);

//        App app = restTemplate.getForObject(backendConstants.baseUrlDbAccess + "/adminConsole/findAppById?id=" + id, App.class);

        Long count = restTemplate.getForObject(backendConstants.baseUrlDbAccess + "/adminConsole/findByAppIDAndStartBetweenEndDate?id=" + appId + "&stDate=" +  startDate + "&enDate=" + endDate, Long.class);

        System.out.println("installCountIs " + count);
//        Long count1 = installReportService.findByAppIDAndStartBetweenEndDate(appId, startDate, endDate);
        data.put("count", count);
        return ResponseEntity.ok().body(data);
    }

    public Map<String, String> getDates(String dateRange) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat formatterForJsDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String startDate = null;
        String endDate = null;
        if (dateRange.equals("")) {
            endDate = formatter.format(new Date());
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, -7);
            startDate = formatter.format(cal.getTime());
        } else {
            String[] startDateArr = dateRange.split(" - ");
            try {
                startDate = formatter.format(formatterForJsDate.parse(startDateArr[0]));
                endDate = formatter.format(formatterForJsDate.parse(startDateArr[1]));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Map<String, String> mp = new HashMap<>();
        mp.put("startDate", startDate);
        mp.put("endDate", endDate);
        return mp;
    }

//    @GetMapping(value = "/installCountWiseGeo")
//    public ResponseEntity<?> installCountWiseGeo(@RequestParam(value = "appId") Long appId, @RequestParam(value = "dateRange") String dateRange) {
//        Map<String, Object> data = new HashMap<String, Object>();
//        Map<String, String> map = getDates(dateRange);
//        String startDate = map.get("startDate");
//        String endDate = map.get("endDate");
//        App app = appService.findById(appId);
//        List<String> installHours = installReportService.findByAppIdAndCountryWiseCount(appId, startDate, endDate);
//        List<InstallGeoWiseDTO> installGeoWiseDTOS = new ArrayList<>();
//        for (int i = 0; i < installHours.size(); i++) {
//            String[] arr = installHours.get(i).split(",");
//            InstallGeoWiseDTO installGeoWiseDTO = new InstallGeoWiseDTO(arr[0], arr[1]);
//            installGeoWiseDTOS.add(installGeoWiseDTO);
//        }
//        List<InstallGeoWise> installGeoWise = installGeoWiseService.findByApp(app);
//        /*for(InstallGeoWise installGeoWise1:installGeoWise){
//            InstallGeoWiseDTO installGeoWiseDTO=new InstallGeoWiseDTO(installGeoWise1);
//            System.out.println("country = "+installGeoWiseDTO);
//            installGeoWiseDTOS.add(installGeoWiseDTO);
//        }*/
//        data.put("installGeoWise", installGeoWiseDTOS);
//        return ResponseEntity.ok().body(data);
//    }
//
//    @GetMapping(value = "/installCountWiseDate")
//    public ResponseEntity<?> installCountWiseDate(@RequestParam(value = "appId") Long appId, @RequestParam(value = "dateRange") String dateRange) throws ParseException {
//        Map<String, Object> data = new HashMap<String, Object>();
//        Map<String, String> map = getDates(dateRange);
//        String startDate = map.get("startDate");
//        String endDate = map.get("endDate");
//        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        Date date = dt.parse(startDate);
//        Date date1 = dt.parse(endDate);
//        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
//        String start1 = formatter.format(date);
//        String endDate1 = formatter.format(date1);
//        App app = appService.findById(appId);
//        List<String> installHours = new ArrayList<>();
//        List<String> hours = new ArrayList<>();
//        List<String> count = new ArrayList<>();
//        if (start1.equals(endDate1)) {
//            installHours = installReportService.findByAppIdAndHourCount(appId, startDate, endDate);
//            String timeStamp = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
//            int currentHour;
//            if (timeStamp.equals(start1))
//                currentHour = LocalDateTime.now().getHour();
//            else
//                currentHour = 24;
//            for (int j = 1; j <= currentHour; j++) {
//                hours.add(String.valueOf(j));
//                count.add("0");
//            }
//            for (int i = 0; i < installHours.size(); i++) {
//                String[] arr = installHours.get(i).split(",");
//                count.set(Integer.parseInt(arr[0]), arr[1]);
//            }
//        } else {
//            installHours = installReportService.findByAppIdDateWiseCount(appId, startDate, endDate);
//            // Parses the date
//            DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("dd/MM/YYYY");
//            LocalDate start = LocalDate.parse(start1);
//            LocalDate end = LocalDate.parse(endDate1);
//            List<String> totalDates = new ArrayList<>();
//            while (!start.isAfter(end)) {
//                hours.add(formatter1.format(start));
//                count.add("0");
//                start = start.plusDays(1);
//            }
//            for (int i = 0; i < installHours.size(); i++) {
//                String[] arr = installHours.get(i).split(",");
//                LocalDate dbDate = LocalDate.parse(arr[0]);
//                int pos = hours.indexOf(formatter1.format(dbDate));
//                count.set(pos, arr[1]);
//            }
//        }
//        data.put("hours", hours);
//        data.put("count", count);
//        return ResponseEntity.ok().body(data);
//    }
//
//    @GetMapping(value = "/updateInstallByRetention")
//    public ResponseEntity<?> updateInstallByRetention(@RequestParam(value = "status") boolean status,
//                                                      @RequestParam(value = "google_aid") String google_aid,
//                                                      @RequestParam(value = "appId") Long appId,
//                                                      HttpServletRequest request, Map<String, Object> model) throws Exception {
//        InstallReport installReport = null;
//        if (!status)
//            installReport = installReportService.findByAppIdAndGoogle_aidAndStatus(appId, google_aid, false);
//        if (installReport == null) {
//            installReport = installReportService.findByAppIdAndGoogle_aid(appId, google_aid);
//            installReport.setStatus(status);
//            String currentDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
//            SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//            Date date = dt.parse(currentDate);
//            installReport.setUpdateDateTime(date);
//            installReportService.save(installReport);
//        }
//        return ResponseEntity.ok().body("Updated User Install");
//    }
//
//    @GetMapping(value = "/retentionCount")
//    public ResponseEntity<?> updateInstallByRetention(@RequestParam(value = "appId") Long appId,
//                                                      HttpServletRequest request, Map<String, Object> model) throws Exception {
//        Map<String, Object> data = new HashMap<String, Object>();
//        Map<String, String> map = getDates("");
//        String startDate = map.get("startDate");
//        String endDate = map.get("endDate");
//        Long activeUserCount = installReportService.findActiveUserByAppIdAndStartBetweenEndDate(appId, startDate, endDate);
//        Long totalInstallCount = installReportService.findByAppId(appId);
//        double percentage = 0;
//        if (totalInstallCount != 0) {
//            percentage = (double) (activeUserCount * 100) / totalInstallCount;
//            percentage = Double.parseDouble(new DecimalFormat("##.##").format(percentage));
//        }
//        data.put("percentage", percentage);
//        return ResponseEntity.ok().body(data);
//    }


}
