package com.attribution.panel.Controller;


import com.attribution.panel.constants.BackendConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@Controller
@RequestMapping(value = "/adminConsole")
public class PartnerController {

    @Autowired
    BackendConstants backendConstants;

    RestTemplate restTemplate = new RestTemplate();


    @RequestMapping(value = "/findByPartnerCompany1", method = RequestMethod.GET)
    public ResponseEntity<?> findByPartnerCompany1(@RequestParam(value = "search", defaultValue = "", required = false) String search,
                          @RequestParam("page") Integer page,
                          @RequestParam("size") Integer size) {
        System.out.println("pubListYo1 ");

        ResponseEntity<Map<String, Object>> responseSelectDTOS = restTemplate.exchange(backendConstants.baseUrlDbAccess + "/adminConsole/findByPartnerCompany1?search={search}&page={page}&size={size}",
                HttpMethod.GET, null, new ParameterizedTypeReference<Map<String, Object>>() {
                }, search, page, size);
        Map<String, Object> map = responseSelectDTOS.getBody();

        return ResponseEntity.ok().body(map);
    }


}
