package com.attribution.panel.Controller;

import com.attribution.panel.bean.App;
import com.attribution.panel.bean.Country;
import com.attribution.panel.bean.Event;
import com.attribution.panel.bean.Partner;
import com.attribution.panel.constants.BackendConstants;
import com.attribution.panel.enums.ConversionStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/adminConsole")
public class AdminController {

    @Autowired
    BackendConstants backendConstants;

    RestTemplate restTemplate = new RestTemplate();

    @GetMapping("/eventReport")
    public String eventReport(@RequestParam("id") Long id, Map<String, Object> model, HttpServletRequest request) {

        App app = restTemplate.getForObject(backendConstants.baseUrlDbAccess + "/adminConsole/findAppById?id=" + id, App.class);

        ResponseEntity<List<App>> appsList = restTemplate.exchange(backendConstants.baseUrlDbAccess + "/adminConsole/findAllApp",
                HttpMethod.GET, null, new ParameterizedTypeReference<List<App>>() {
                });

//        List<Event> eventList = eventService.findByApp(app);
        ResponseEntity<List<Event>> eventList = restTemplate.exchange(backendConstants.baseUrlDbAccess + "/adminConsole/findByAllEvent/{appId}",
                HttpMethod.GET, null, new ParameterizedTypeReference<List<Event>>() {
                }, app.getId());


        ResponseEntity<List<Country>> countries = restTemplate.exchange(backendConstants.baseUrlDbAccess + "/adminConsole/findAllCountry",
                HttpMethod.GET, null, new ParameterizedTypeReference<List<Country>>() {
                });
        System.out.println("eventReport " + app.getId()+ " " + appsList.getBody().size() + " " +  eventList);

        model.put("appsList", appsList.getBody());
        model.put("app", app);
        model.put("appId", app.getId());
        model.put("eventList", eventList.getBody());
        model.put("countries", countries.getBody());
        return "jsp/userEvent";
    }

    @GetMapping("/event")
    public String findEvent(@RequestParam("id") Long id, Map<String, Object> model, HttpServletRequest request) {

        App app = restTemplate.getForObject(backendConstants.baseUrlDbAccess + "/adminConsole/findAppById?id=" + id, App.class);

        ResponseEntity<List<App>> appsList = restTemplate.exchange(backendConstants.baseUrlDbAccess + "/adminConsole/findAllApp",
                HttpMethod.GET, null, new ParameterizedTypeReference<List<App>>() {
                });
//        Partner partners= partnerService.findAll();
        model.put("appsList", appsList.getBody());
        model.put("app", app);
//        model.put("partnerId", partners.);
        return "jsp/events";
    }

    @RequestMapping(value = {"/appRedirect"}, method = RequestMethod.GET)
    public String appRedirect(Principal principal, Map<String, Object> model) {
        return "jsp/apps";
    }

    @RequestMapping(value = {"/apps"}, method = RequestMethod.GET)
    public String getApp(Principal principal, Map<String, Object> model) {
        return "jsp/apps";
    }


    @RequestMapping(value = {"/addApp"}, method = RequestMethod.GET)
    public String addApp(Principal principal, Map<String, Object> model) {
        return "jsp/addApp";
    }


    @RequestMapping(value = {"/getConfiguration"}, method = RequestMethod.GET)
    public String getConfiguration(Principal principal, Map<String, Object> model) {

        List<Partner> partners = restTemplate.exchange(backendConstants.baseUrlDbAccess + "/adminConsole/findAllPartner",
                HttpMethod.GET, null, new ParameterizedTypeReference<List<Partner>>() {
                }).getBody();

        ResponseEntity<List<App>> appsList = restTemplate.exchange(backendConstants.baseUrlDbAccess + "/adminConsole/findAllApp",
                HttpMethod.GET, null, new ParameterizedTypeReference<List<App>>() {
                });

        System.out.println("getConfiguration " + partners.size() + appsList.getBody().size());
//        System.out.println("Partner are : -  " + apps);
//        List<Publisher> publishers;
//        if (request.isUserInRole("ROLE_EMPLOYEE")) {
//            Admin admin = restTemplate.getForEntity(backendConstants.baseUrlDbAccess + "/adminConsole/findAllAdminBYId/{email}", Admin.class, request.getUserPrincipal().getName()).getBody();
//            assert admin != null;
//            publishers = admin.getPublishers();
//        } else
//            publishers = restTemplate.exchange(backendConstants.baseUrlDbAccess + "/adminConsole/findAllPublisher",
//                    HttpMethod.GET, null, new ParameterizedTypeReference<List<Publisher>>() {
//                    }).getBody();
//        model.put("base", base);
        model.put("app", appsList.getBody());
        model.put("partner", partners);

        return "jsp/Configuration";
    }

    @RequestMapping(value = {"/getpubs"}, method = RequestMethod.GET)
    public String listPubs(Map<String, Object> model, HttpServletRequest request) {

        List<Partner> partners = restTemplate.exchange(backendConstants.baseUrlDbAccess + "/adminConsole/findAllPartner",
                HttpMethod.GET, null, new ParameterizedTypeReference<List<Partner>>() {
                }).getBody();

        List<Long> partnersIds = new ArrayList<>();

//        List<Partner> partners =  partnerService.findAll();
        System.out.println("himank");
        System.out.println("Partner are : -  " + partners);
//        List<Publisher> publishers;
//        if (request.isUserInRole("ROLE_EMPLOYEE")) {
//            Admin admin = restTemplate.getForEntity(backendConstants.baseUrlDbAccess + "/adminConsole/findAllAdminBYId/{email}", Admin.class, request.getUserPrincipal().getName()).getBody();
//            assert admin != null;
//            publishers = admin.getPublishers();
//        } else
//            publishers = restTemplate.exchange(backendConstants.baseUrlDbAccess + "/adminConsole/findAllPublisher",
//                    HttpMethod.GET, null, new ParameterizedTypeReference<List<Publisher>>() {
//                    }).getBody();
//        model.put("base", base);
        model.put("partnersIds", partnersIds);
        model.put("partner", partners);

        return "jsp/partnerList";
    }

    @GetMapping("/dashboard")
    public String dashboard(@RequestParam("id") Long id, Map<String, Object> model, HttpServletRequest request) {

        App app = restTemplate.getForObject(backendConstants.baseUrlDbAccess + "/adminConsole/findAppById?id=" + id, App.class);

        ResponseEntity<List<Event>> eventList = restTemplate.exchange(backendConstants.baseUrlDbAccess + "/adminConsole/findByAllEvent/{appId}",
                HttpMethod.GET, null, new ParameterizedTypeReference<List<Event>>() {
                }, app.getId());

        ResponseEntity<List<App>> appsList = restTemplate.exchange(backendConstants.baseUrlDbAccess + "/adminConsole/findAllApp",
                HttpMethod.GET, null, new ParameterizedTypeReference<List<App>>() {
                });
        model.put("appsList", appsList.getBody());
        model.put("app", app);
        model.put("eventList", eventList.getBody());
        return "jsp/dashboard";
    }

    @GetMapping("/getClickLogs")
    public String listClickLogs(
            @RequestParam(value = "daterange", required = false, defaultValue = "") String daterange,
            @RequestParam(value = "advertisers", required = false, defaultValue = "") List<String> advertIds,
            Map<String, Object> model) {

/*        ResponseEntity<List<App>> offers = restTemplate.exchange(backendConstants.baseUrlDbAccess + "/adminConsole/findAllApp",
                HttpMethod.GET, null, new ParameterizedTypeReference<List<App>>() {
                });
        ResponseEntity<List<Company>> responseEntity = restTemplate.exchange(backendConstants.baseUrlDbAccess + "/adminConsole/findAllCompany",
                HttpMethod.GET, null, new ParameterizedTypeReference<List<Company>>() {
                });
        List<Company> adverts = responseEntity.getBody();

        model.put("offerList", offers.getBody());
        model.put("adverts", adverts);*/

//        model.put("base", base);
        return "jsp/clickLogs";
    }


//    @GetMapping(value = "/getAllReport")
//    public String getAllReport(@RequestParam("id") Long appId, Map<String, Object> model) {
//        System.out.println("Inside getAll report:" + id);
//        App app = appService.findById(id);
//        System.out.println("Inside getAll report2:" + " " + model);
//        List<AllReportDTO> list = null;
////        list = reportService.reportAllAjax(new ReportFilterDTO(), id);
//
//        System.out.println("Inside getAll report2: " + list.size() + " " + list);
//        System.out.println(",reportFilterDTO.getPublisherName() ");
//
//        list.get(0).getAppId();
//
//
////        log.info("Inside getAll report:");
////        List<AllReportDTO> list = null;
////        RestTemplate restTemplate = new RestTemplate();
////        ResponseEntity<Object> responseEntity = restTemplate.postForEntity(backendConstants.baseUrlDbAccess + allReport, new ReportFilterDTO(), Object.class);
////        list = (List<AllReportDTO>) responseEntity.getBody();
//        List<App> appsList = appService.findAll();
//        model.put("appsList", appsList);
//        model.put("app", app);
//        model.put("list", list);
//        model.put("filters", new ReportFilterDTO());
//        return "jsp/allReport";
//    }


    @GetMapping("/getReport")
    public String getReport(Map<String, Object> model) {
        System.out.println("this is testing ");
        ResponseEntity<List<App>> appsList = restTemplate.exchange(backendConstants.baseUrlDbAccess + "/adminConsole/findAllApp",
                HttpMethod.GET, null, new ParameterizedTypeReference<List<App>>() {
                });

        List<Partner> partner = restTemplate.exchange(backendConstants.baseUrlDbAccess + "/adminConsole/findAllPartner",
                HttpMethod.GET, null, new ParameterizedTypeReference<List<Partner>>() {
                }).getBody();

        System.out.println("this is testing " + partner);
        List<ConversionStatus> conversionStatus = Arrays.asList(ConversionStatus.values());
        model.put("publishers", partner);
        model.put("offerList", appsList.getBody());
        model.put("conversionStatus", conversionStatus);
//        model.put("base", base);
        return "jsp/report";
    }

    @GetMapping("/getAllReport")
    public String getAllReport(@RequestParam("id") Long appId, Map<String, Object> model) {
        System.out.println("this is testing 2 " + appId);
        App app = restTemplate.getForObject(backendConstants.baseUrlDbAccess + "/adminConsole/findAppById?id=" + appId, App.class);
        List<Partner> partner = restTemplate.exchange(backendConstants.baseUrlDbAccess + "/adminConsole/findAllPartner",
                HttpMethod.GET, null, new ParameterizedTypeReference<List<Partner>>() {
                }).getBody();

        List<ConversionStatus> conversionStatus = Arrays.asList(ConversionStatus.values());
        ResponseEntity<List<App>> appsList = restTemplate.exchange(backendConstants.baseUrlDbAccess + "/adminConsole/findAllApp",
                HttpMethod.GET, null, new ParameterizedTypeReference<List<App>>() {
                });
        System.out.println("this is testing 3 " + app.getId() + partner.size() + appsList.getBody().size());
        model.put("app", app);
        model.put("appsList", appsList.getBody());
        model.put("partners", partner);
        return "jsp/allReport";
    }

}
