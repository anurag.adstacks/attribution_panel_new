package com.attribution.panel.constants;

public final class UIConstants {


    final public static String primaryColorClass = "primary";
    //primaryBackgroundColor
    final public static String primaryBackgroundColor = "#F2F4F8";
    //primaryTextColorClass
    final public static String primaryTextColorClass = "text-dark";
    //primaryCardBackground
    final public static String primaryCardBackground = "#FFFFFF";
    //primaryCardModalBackground
    final public static String primaryCardModalBackground = "#FFFFFF";
    //primaryFontFamily
    final public static String primaryFontFamily = "Times New Roman, Times, serif";

    final public static String primaryFontFamilyTable = "Times New Roman, Times, serif";

    final public static String primaryFilterFontFamilyTable = "'Lucida Console', Monaco, monospace";

    final public static String timeZoneValue = "Asia/Kolkata";

    final public static String primaryHyperLinkColor = "#303654";

    final public static String primaryModalCardHeaderBackground = "#F2F4F8";

    final public static String primaryButtonColor = "#303654";

    final public static String textColor = "#000000";

    final public static String borderColor = "#000000";

    final public static String arrowColor = "#000000";

    final public static String selectBoxColor = "#ffffff";

    final public static String hoverButtonBgColor = "#303654";

    final public static String headingTextColor = "#303654";

    final public static String sideBgColor="#323954";

    final public static String chartLineColor="#303654";

    final public static String chartLineColor1 ="#303654";

    final public static String dropDownHoverColor = "#585a5c";

    final public static String chartBgColor = "#F2F4F8";

    final public static String chartTextColor = "#FFFFFF";

    final public static String dropDownSelectedColor="#303654";

    final public static String sweetAlertBgColor="#ffffff";

    final public static String tableIconBgColor="#29a80a";

    final public static String tableIconTextColor="#ffffff";

    final public static String dropDownMenuColor="#ffffff";

    final public static String paginateHoverTextColor="#ffffff";

    final public static String paginateHoverBgColor="#585858";

    final public static String bannerTextColor="#FFFFFF";

    final public static String bannerBgColor="#303654";

    final public static String primaryButtonTextColor="#FFFFFF";

    final public static String scrollbarBgColor="#303654";

    final public static String chartBoxBgColor="#303654";

    final public static String primaryBootstrapColor="#303654";

    final public static String dataPickerBgColor="#FFFFFF";

    final public static String trackingBaseUrl = "http://panel.adjarmedia.com";

    /*
    //UI constants
    final public static String primaryColorClass = "primary";
    //primaryBackgroundColor
    final public static String primaryBackgroundColor = "#F2F4F8";
    //primaryTextColorClass
    final public static String primaryTextColorClass = "text-dark";
    //primaryCardBackground
    final public static String primaryCardBackground = "#FFFFFF";
    //primaryCardModalBackground
    final public static String primaryCardModalBackground = "#FFFFFF";
    //primaryFontFamily
    final public static String primaryFontFamily = "Times New Roman, Times, serif";

    final public static String primaryFontFamilyTable = "Times New Roman, Times, serif";

    final public static String primaryFilterFontFamilyTable = "'Lucida Console', Monaco, monospace";

    final public static String timeZoneValue = "Asia/Kolkata";

    final public static String primaryHyperLinkColor = "#303654";

    final public static String primaryModalCardHeaderBackground = "#F2F4F8";

    final public static String primaryButtonColor = "#303654";

    final public static String textColor = "#000000";

    final public static String borderColor = "#000000";

    final public static String arrowColor = "#000000";

    final public static String selectBoxColor = "#ffffff";

    final public static String hoverButtonBgColor = "#303654";

    final public static String headingTextColor = "#303654";

    final public static String sideBgColor="#323954";

    final public static String chartLineColor="#303654";

    final public static String chartLineColor1 ="#303654";

    final public static String dropDownHoverColor = "#585a5c";

    final public static String chartBgColor = "#F2F4F8";

    final public static String chartTextColor = "#FFFFFF";

    final public static String dropDownSelectedColor="#303654";

    final public static String sweetAlertBgColor="#ffffff";

    final public static String tableIconBgColor="#29a80a";

    final public static String tableIconTextColor="#ffffff";

    final public static String dropDownMenuColor="#ffffff";

    final public static String paginateHoverTextColor="#ffffff";

    final public static String paginateHoverBgColor="#585858";

    final public static String bannerTextColor="#FFFFFF";

    final public static String bannerBgColor="#303654";

    final public static String primaryButtonTextColor="#FFFFFF";

    final public static String scrollbarBgColor="#303654";

    final public static String chartBoxBgColor="#303654";

    final public static String primaryBootstrapColor="#303654";

    final public static String dataPickerBgColor="#FFFFFF";

    final public static String trackingBaseUrl = "http://panel.adjarmedia.com";*/

}

