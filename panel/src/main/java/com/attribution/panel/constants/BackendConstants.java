package com.attribution.panel.constants;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public final class BackendConstants {


    private String name;

    public static String dbAccessBaseUrl;

    final public String UPLOADED_FOLDER;

    @Value("${baseUrlDbAccessMicroservice}")
    public void setNameStatic(String name){
        BackendConstants.dbAccessBaseUrl = name;
    }

    final public String baseUrlDashboardMicroservice;
    final public String baseUrlReportsMicroservice;
    final public String trackingBaseUrl;
    final public String fileUploadDownloadMicroserviceBaseUrl;
    final public String baseUrlDbAccess;
    final public String publicBaseUrlDbAccess;

    final public  String fileDownloadUrl;
    final public  String fileDeleteUrl;

    //Constants for all graphs in dashboard
    final public static String offerGraph = "/dash1/top10ConversionOfferWise";
    final public static String advertiserGraph = "/dash1/top10ConversionAdvertiserWise";
    final public static String publisherGraph = "/dash1/top10ConversionPublisherWise";
    final public static String geoGraph = "/dash1/top10ConversionGeoWise";
    final public static String deviceGraph = "/dash1/top10ConversionDeviceWise";
    final public static String clicksVsConversionGraph = "/adminConsole/clickVsConversion";

    //dashboard counts
    final public static String dailyCounts = "/adminConsole/getDailyCounts";

    //reports
    final public static String offerReport = "/reports/offerReport";
    final public static String advertiserReport = "/reports/advertiserReport";
    final public static String overallReport = "/reports/overallReport";
    final public static String affiliateReport = "/reports/affiliateReport";
    final public static String analysisReport = "/reports/analysisReport";
    final public static String conversionReport = "/reports/conversionReport";
    final public static String clickReport = "/reports/clickReport";
    final public static String allReport = "/reports/allReport";


    @Autowired
    public BackendConstants(@Value("${baseUrlDashboardMicroservice}") String baseUrlDashboardMicroservice,
                            @Value("${baseUrlReportsMicroservice}") String baseUrlReportsMicroservice,
                            @Value("${trackingBaseUrl}") String trackingBaseUrl,
                            @Value("${fileUploadDownloadMicroserviceBaseUrl}") String fileUploadDownloadMicroserviceBaseUrl,
                            @Value("${baseUrlDbAccessMicroservice}") String baseUrlDbAccess,
                            @Value("${UPLOADED_FOLDER}") String UPLOADED_FOLDER,
                            @Value("${publicBaseUrlDbAccess}") String publicBaseUrlDbAccess
    ) {
        this.baseUrlDashboardMicroservice = baseUrlDashboardMicroservice;
        this.baseUrlReportsMicroservice = baseUrlReportsMicroservice;
        this.trackingBaseUrl = trackingBaseUrl;
        this.fileUploadDownloadMicroserviceBaseUrl = fileUploadDownloadMicroserviceBaseUrl;
        this.fileDownloadUrl = fileUploadDownloadMicroserviceBaseUrl + "/downloadFile/";
        this.fileDeleteUrl =  "/deleteFile/";
        this.publicBaseUrlDbAccess = publicBaseUrlDbAccess;
        this.baseUrlDbAccess = baseUrlDbAccess;
        this.UPLOADED_FOLDER = UPLOADED_FOLDER;
    }

//    final public String baseUrl;
//    final public String baseUrlDbAccess;
//
//    @Autowired
//    public BackendConstants(
//            @Value("${baseUrl}") String baseUrl,
//            @Value("${baseUrlDbAccess}") String baseUrlDbAccess
//    ) {
//        this.baseUrl = baseUrl;
//        this.baseUrlDbAccess = baseUrlDbAccess;
//    }


}
