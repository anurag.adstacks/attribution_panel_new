//package com.attribution.panel.dao;
//
//import com.adstacks.attribution.bean.AttributionAllReport;
//import com.adstacks.attribution.bean.AttributionClick;
//import com.adstacks.attribution.bean.Attributionconversion;
//import com.adstacks.attribution.console.dto.AllReportDTO;
//import com.adstacks.attribution.constants.ElasticSearchConstants;
//import com.alibaba.fastjson.JSON;
//import lombok.extern.slf4j.Slf4j;
//import org.elasticsearch.action.search.SearchRequest;
//import org.elasticsearch.action.search.SearchResponse;
//import org.elasticsearch.client.RequestOptions;
//import org.elasticsearch.client.RestHighLevelClient;
//import org.elasticsearch.client.core.CountRequest;
//import org.elasticsearch.client.core.CountResponse;
//import org.elasticsearch.index.query.BoolQueryBuilder;
//import org.elasticsearch.index.query.QueryBuilders;
//import org.elasticsearch.search.SearchHit;
//import org.elasticsearch.search.aggregations.AggregationBuilders;
//import org.elasticsearch.search.aggregations.bucket.composite.CompositeAggregationBuilder;
//import org.elasticsearch.search.aggregations.bucket.composite.CompositeValuesSourceBuilder;
//import org.elasticsearch.search.aggregations.bucket.composite.ParsedComposite;
//import org.elasticsearch.search.aggregations.bucket.composite.TermsValuesSourceBuilder;
//import org.elasticsearch.search.aggregations.metrics.Sum;
//import org.elasticsearch.search.builder.SearchSourceBuilder;
//import org.elasticsearch.search.sort.FieldSortBuilder;
//import org.elasticsearch.search.sort.SortBuilders;
//import org.elasticsearch.search.sort.SortOrder;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.data.domain.Pageable;
//import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
//import org.springframework.data.jpa.datatables.mapping.Order;
//import org.springframework.stereotype.Component;
//
//import java.io.IOException;
//import java.util.*;
//import java.util.stream.Collectors;
//
//@Component
//@Slf4j
//public class ReportQueryBuilder {
//
//    @Autowired
//    @Qualifier("elasticClient")
//    RestHighLevelClient client;
//
////    @Autowired
////    CSVService csvService;
//
////    Set<AllReportDTO> allReportDTOList = new HashSet<>();
//
//
//    public Map<String, Object> conversionReport(String startDate, String endDate, List<String> pubArr, List<String> offerArr,
//                                                List<String> conversionStatuses, Pageable pageable, DataTablesInput input) {
//        BoolQueryBuilder query = QueryBuilders.boolQuery();
//        System.out.println("BoolQueryBuilder " + pubArr);
//        if (pubArr != null && !pubArr.isEmpty()) {
//            System.out.println("BoolQueryBuilder1 " + pubArr);
//            query.must(QueryBuilders.termsQuery("partnerId", pubArr));
//        }
//        if (offerArr != null && !offerArr.isEmpty()) {
//            query.must(QueryBuilders.termsQuery("appId", offerArr));
//            System.out.println("BoolQueryBuilder2 " + offerArr);
//        }
//        log.info("conversionStatus:" + conversionStatuses);
////        if (advArr != null && !advArr.isEmpty()) {
////            query.must(QueryBuilders.termsQuery("advertiserId", advArr));
////        }
//        if (conversionStatuses != null && !conversionStatuses.isEmpty()) {
//            query.must(QueryBuilders.termsQuery("conversionStatus", conversionStatuses));
//        }
//        query.must(QueryBuilders.rangeQuery("creationDateTime").gte(startDate).lte(endDate));
//
//        SearchRequest searchRequest = new SearchRequest(ElasticSearchConstants.conversionIndexSearchPattern);
//        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
//        searchSourceBuilder.query(query);
//        searchRequest.source(searchSourceBuilder);
//        searchSourceBuilder.size(pageable.getPageSize());
//        searchSourceBuilder.from((int) pageable.getOffset());
//        searchSourceBuilder.sort(getConversionFieldSortBuilder(input));
//        SearchResponse response = null;
//
//        try {
//            response = client.search(searchRequest, RequestOptions.DEFAULT);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        System.out.println("client.search " + response);
//
//        SearchHit[] searchHits = response.getHits().getHits();
//        List<Attributionconversion> conversionList =
//                Arrays.stream(searchHits)
//                        .map(hit -> JSON.parseObject(hit.getSourceAsString(), Attributionconversion.class))
//                        .collect(Collectors.toList());
//        Long count = response.getHits().getTotalHits().value;
//        System.out.println("conversionList = " + conversionList);
//        Map<String, Object> map = new HashMap<>();
//        map.put("conversionList", conversionList);
//        map.put("count", count);
//        return map;
//    }
//
//    public Map<String, Object> clickReport(String startDate, String endDate, List<String> pubArr, List<String> offerArr,
//                                           Pageable pageable, String uuid, DataTablesInput input) {
//        BoolQueryBuilder query = QueryBuilders.boolQuery();
//        System.out.println("clickReport " + input);
//        if (pubArr != null && !pubArr.isEmpty()) {
//            query.must(QueryBuilders.termsQuery("pId", pubArr));
//        }
//        if (offerArr != null && !offerArr.isEmpty()) {
//            query.must(QueryBuilders.termsQuery("appID", offerArr));
//        }
//        if (uuid != null && uuid != "") {
//            query.must(QueryBuilders.termQuery("id", uuid));
//        }
//        query.must(QueryBuilders.rangeQuery("creationDateTime").gte(startDate).lte(endDate));
//
//        SearchRequest searchRequest = new SearchRequest(ElasticSearchConstants.clickIndexSearchPattern);
//        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
//        searchSourceBuilder.query(query);
//        searchRequest.source(searchSourceBuilder);
//        log.info("page size:" + pageable.getPageSize());
//        log.info("page offset:" + pageable.getOffset());
//        searchSourceBuilder.size(pageable.getPageSize());
//        searchSourceBuilder.from((int) pageable.getOffset());
//        searchSourceBuilder.sort(getClickFieldSortBuilder(input));
////        searchSourceBuilder.searchAfter()
//        SearchResponse response = null;
//        try {
//            response = client.search(searchRequest, RequestOptions.DEFAULT);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        System.out.println("clickReport1 " + response);
//
//        SearchHit[] searchHits = response.getHits().getHits();
//        System.out.println("clickReport3 " + searchHits);
//
//        List<AttributionClick> clickList =
//                Arrays.stream(searchHits)
//                        .map(hit -> JSON.parseObject(hit.getSourceAsString(), AttributionClick.class))
//                        .collect(Collectors.toList());
//
//
//        System.out.println("clickReport2 " + clickList);
//        SearchSourceBuilder searchSourceBuilder1 = new SearchSourceBuilder();
//        searchSourceBuilder1.query(query);
//        CountRequest countRequest = new CountRequest(ElasticSearchConstants.clickIndexSearchPattern);
//        countRequest.source(searchSourceBuilder);
//        Long count = 0l;
//
//        try {
//            CountResponse countResponse = client.count(countRequest, RequestOptions.DEFAULT);
//            count = countResponse.getCount();
//            /*log.info("count:" + count);*/
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
////        SearchHits hits = response.getHits();
////        hits.getAt((int) pageable.getOffset()).getSortValues();
//        Map<String, Object> map = new HashMap<>();
//        map.put("clickList", clickList);
//        map.put("count", count);
//        return map;
//    }
//
//
//    private FieldSortBuilder getConversionFieldSortBuilder(DataTablesInput input) {
//        Map<String, String> orderMap = new HashMap<String, String>();
//        Order order = input.getOrder().get(0);
//        orderMap.put("orderColumnName", input.getColumns().get(order.getColumn()).getData());
//        orderMap.put("orderType", order.getDir());
//        String orderValue = orderMap.get("orderType");
//        FieldSortBuilder mySort = null;
//        switch (orderMap.get("orderColumnName")) {
//            case "appId":
//                mySort = SortBuilders.fieldSort("appId").order(SortOrder.fromString(orderValue));
//                break;
//            case "partnerId":
//                mySort = SortBuilders.fieldSort("partnerId").order(SortOrder.fromString(orderValue));
//                break;
//            default:
//                mySort = SortBuilders.fieldSort("creationDateTime").order(SortOrder.DESC);
//                break;
//        }
//        System.out.println("getConversionFieldSortBuilder " + mySort);
//        return mySort;
//    }
//
//    private FieldSortBuilder getClickFieldSortBuilder(DataTablesInput input) {
//        Map<String, String> orderMap = new HashMap<String, String>();
//        Order order = input.getOrder().get(0);
//        orderMap.put("orderColumnName", input.getColumns().get(order.getColumn()).getData());
//        orderMap.put("orderType", order.getDir());
//        String orderValue = orderMap.get("orderType");
//        FieldSortBuilder mySort = null;
//        switch (orderMap.get("orderColumnName")) {
//            case "appID":
//                mySort = SortBuilders.fieldSort("appID").order(SortOrder.fromString(orderValue));
//                break;
//            case "pId":
//                mySort = SortBuilders.fieldSort("pId").order(SortOrder.fromString(orderValue));
//                break;
//            default:
//                mySort = SortBuilders.fieldSort("creationDateTime").order(SortOrder.DESC);
//                break;
//        }
//        return mySort;
//    }
//
//
//    public List<AllReportDTO> allReportCHR(String startDate, String endDate, List<String> pubArr, List<String> groupByArr, Long appId) throws IOException {
//        System.out.println("allReportCHR1 ");
//        Map<String, String> nameAndFieldMapCHR = new HashMap<>();
////        nameAndFieldMapCHR.put("App", "id.appId");
////        nameAndFieldMapCHR.put("Partner", "id.partnerId");
////        nameAndFieldMapCHR.put("Advertiser", "advertiserId");
////        nameAndFieldMapCHR.put("Sub_Aff", "id.subAffiliate.keyword");
//        nameAndFieldMapCHR.put("App", "appId");
//        nameAndFieldMapCHR.put("Partner", "partnerId");
//
//
//        System.out.println("allReportCHR2 " + nameAndFieldMapCHR);
//
//        //For filters
//        BoolQueryBuilder query = QueryBuilders.boolQuery();
////        if (pubArr != null && !pubArr.isEmpty()) {
////            query.must(QueryBuilders.termsQuery("id.partnerId", pubArr));
////            System.out.println("allReportCHR3 " + query);
////        }
////        if (offerArr != null && !offerArr.isEmpty()) {
////            query.must(QueryBuilders.termsQuery("id.appId", offerArr));
////            System.out.println("allReportCHR4 " + query);
////        }
//
//        if (pubArr != null && !pubArr.isEmpty()) {
//            query.must(QueryBuilders.termsQuery("partnerId", pubArr));
//            System.out.println("allReportCHR3 " + query);
//        }
//
////        if (offerArr != null && !offerArr.isEmpty()) {
////            query.must(QueryBuilders.termsQuery("appId", offerArr));
////            System.out.println("allReportCHR4 " + query);
////        }
//
//
//        String appids = String.valueOf(appId);
//        query.must(QueryBuilders.termsQuery("appId", appids));
//        System.out.println("query is " + query);
//
////        if (advArr != null && !advArr.isEmpty()) {
////            query.must(QueryBuilders.termsQuery("advertiserId", advArr));
////        }
////
////        if (subAff != null && subAff != "") {
////            query.must(QueryBuilders.termQuery("id.subAffiliate.keyword", subAff));
////        }
////        query.must(QueryBuilders.rangeQuery("id.hourTimestamp").gte(startDate).lte(endDate));
//        //
//
//        //For creation of group by components having max count of 3000
//        List<CompositeValuesSourceBuilder<?>> sources = new ArrayList<>();
//        for (String groupBy : groupByArr) {
//            sources.add(new TermsValuesSourceBuilder(groupBy).field(nameAndFieldMapCHR.get(groupBy)));
//        }
//        System.out.print("sources = " + sources);
//        CompositeAggregationBuilder compositeAggregationBuilder =
//                new CompositeAggregationBuilder("AllReport", sources).size(3000);
//        System.out.println("thisis whatiWantP55" + compositeAggregationBuilder);
//        //
//
//        //For adding of sum aggregation fields on existing group by components
//        compositeAggregationBuilder
//                .subAggregation(AggregationBuilders.sum("clickCount").field("clickCount"))
//                .subAggregation(AggregationBuilders.sum("ratio").field("ratio"))
//                .subAggregation(AggregationBuilders.sum("ctr").field("ctr"))
//                .subAggregation(AggregationBuilders.sum("approvedConversions").field("approvedConversions"))
//                .subAggregation(AggregationBuilders.sum("cancelledConversions").field("cancelledConversions"))
//                .subAggregation(AggregationBuilders.sum("pendingConversions").field("pendingConversions"))
//                .subAggregation(AggregationBuilders.sum("rejectedClicks").field("rejectedClicks"))
//                .subAggregation(AggregationBuilders.sum("approvedRevenue").field("approvedRevenue"))
//                .subAggregation(AggregationBuilders.sum("cancelledRevenue").field("cancelledRevenue"))
//                .subAggregation(AggregationBuilders.sum("pendingRevenue").field("pendingRevenue"))
//                .subAggregation(AggregationBuilders.sum("approvedPayout").field("approvedPayout"))
//        ;
//        //
//        System.out.println("allReportCHR7 " + compositeAggregationBuilder);
//        SearchRequest searchRequest = new SearchRequest("attributionrecord*");
//        System.out.println("thisis whatiWantP77" + searchRequest);
//        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
//        searchSourceBuilder.query(query);
//        searchSourceBuilder.aggregation(compositeAggregationBuilder);
//        searchRequest.source(searchSourceBuilder);
//        SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
//        System.out.println("thisis whatiWantP78" + searchResponse.getAggregations());
//
//        List<AllReportDTO> allReportDTOList = new ArrayList<>();
//
//        ParsedComposite internalComposite = searchResponse.getAggregations().get("AllReport");//get aggregation bucket from name
//        System.out.println("allReportCHR11 " + internalComposite);
//
//        for (ParsedComposite.ParsedBucket termBucket : internalComposite.getBuckets()) {//Loop through array of buckets returned
//            AllReportDTO allReportDTO = new AllReportDTO();
//            Sum aggValue = termBucket.getAggregations().get("clickCount");//Fetching each sum aggregation from bucket by name
//            Sum ratio = termBucket.getAggregations().get("ratio");
//            Sum ctr = termBucket.getAggregations().get("ctr");
//            Sum approvedConversions = termBucket.getAggregations().get("approvedConversions");
//            Sum cancelledConversions = termBucket.getAggregations().get("cancelledConversions");
//            Sum pendingConversions = termBucket.getAggregations().get("pendingConversions");
//            Sum rejectedClicks = termBucket.getAggregations().get("rejectedClicks");
//            Sum approvedRevenue = termBucket.getAggregations().get("approvedRevenue");
//            Sum cancelledRevenue = termBucket.getAggregations().get("cancelledRevenue");
//            Sum pendingRevenue = termBucket.getAggregations().get("pendingRevenue");
//            Sum approvedPayout = termBucket.getAggregations().get("approvedPayout");
//
//            String keys = termBucket.getKeyAsString();//For getting aggregation keys(e.g:For Offer group by key is offerId=id)
//            System.out.println("allReportCHR11 " + keys);
//
//            List<String> keyArr = Arrays.asList(keys.replaceAll("[{}]", "").split(","));//For multiple group by we get list of keys
//            System.out.println("allReportCHR12 " + keyArr);
//
//            Map<String, String> map = new HashMap<>();
//            for (String key : keyArr) {
//                List<String> stringList = Arrays.asList(key.split("="));
//                map.put(stringList.get(0).trim(),
//                        (stringList.size() == 2) ?
//                                stringList.get(1).trim() : null);
//            }
//            System.out.println("allReportCHR13 " + map);
//
//            putId(allReportDTO, map, groupByArr);//For populating id fields in DTO which were in group by
//
//            allReportDTO.setClick((long) aggValue.getValue());//For Populating aggregated sum results in DTO
//            allReportDTO.setRatio(ratio.getValue());
//            allReportDTO.setCtr(ctr.getValue());
//            allReportDTO.setApprovedConversions((long) approvedConversions.getValue());
//            allReportDTO.setCancelledConversions((long) cancelledConversions.getValue());
//            allReportDTO.setPendingConversions((long) pendingConversions.getValue());
//            allReportDTO.setRejectedClicks((int) rejectedClicks.getValue());
//            allReportDTO.setApprovedRevenue((long) approvedRevenue.getValue());
//            allReportDTO.setCancelledRevenue((long) cancelledRevenue.getValue());
//            allReportDTO.setPendingRevenue((long) pendingRevenue.getValue());
//            allReportDTO.setApprovedPayout((long) approvedPayout.getValue());
//            allReportDTOList.add(new AllReportDTO(allReportDTO));
//        }
//        System.out.println("allReportCHR14 " + allReportDTOList);
//        return allReportDTOList;
//    }
//
//
//    public void putId(AllReportDTO allReportDTO, Map<String, String> key, List<String> groupByArr) {
//        System.out.println("putId inside " + groupByArr);
//        for (String groupBy : groupByArr) {
//            switch (groupBy) {
//                case "App":
//                    System.out.println("1234");
//                    allReportDTO.setAppId(Long.parseLong(key.get("App")));
//                    break;
//                case "Partner":
//                    System.out.println("12345");
//                    allReportDTO.setPartnerId(Long.parseLong(key.get("Partner")));
//                    break;
//            }
//        }
//    }
//
//    public List<AttributionClick> getCLickListForIPAndOfferId(String ip, String offerId) {
//        BoolQueryBuilder query = QueryBuilders.boolQuery();
//        query.must(QueryBuilders.matchQuery("ip", ip));
//        query.must(QueryBuilders.termsQuery("appID", offerId));
//        SearchRequest searchRequest = new SearchRequest(ElasticSearchConstants.clickIndexSearchPattern);
//        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
//        searchSourceBuilder.query(query);
//        searchRequest.source(searchSourceBuilder);
//        SearchResponse response = null;
//        try {
//            response = client.search(searchRequest, RequestOptions.DEFAULT);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        SearchHit[] searchHits = response.getHits().getHits();
//        List<AttributionClick> clickList =
//                Arrays.stream(searchHits)
//                        .map(hit -> JSON.parseObject(hit.getSourceAsString(), AttributionClick.class))
//                        .collect(Collectors.toList());
//
//        System.out.println("getCLickListForIPAndOfferId " + clickList);
//        return clickList;
//    }
//
//    public Long countByAppIdAndPartnerIdAndCreationDateTimeBetween(Long appId, Long partnerId /*, String startDate, String endDate*/) {
//        BoolQueryBuilder query = QueryBuilders.boolQuery();
//        query.must(QueryBuilders.termQuery("appId", appId));
//        query.must(QueryBuilders.termQuery("partnerId", partnerId));
////        query.must(QueryBuilders.rangeQuery("creationDateTime").gte(startDate).lte(endDate));
//
//        long count = 0L;
//        CountRequest countRequest = new CountRequest(ElasticSearchConstants.impressionIndexSearchPattern);
//        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
//        searchSourceBuilder.query(query);
//        countRequest.source(searchSourceBuilder);
//        System.out.println("countRequest " + countRequest);
//        try {
//            CountResponse countResponse = client.count(countRequest, RequestOptions.DEFAULT);
//            count = countResponse.getCount();
//            System.out.println("countRequest1 " + count);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return count;
//    }
//
//    public List<Attributionconversion> findByClickIdAndAppIdAndPartnerIdOrderByCreationDateTimeDesc(String clickId, Long appId, Long partnerId /*, String startDate, String endDate*/) {
//
//        BoolQueryBuilder query = QueryBuilders.boolQuery();
//        query.must(QueryBuilders.matchQuery("clickId", clickId));
//        query.must(QueryBuilders.matchQuery("appId", String.valueOf(appId)));
//        query.must(QueryBuilders.termsQuery("partnerId", String.valueOf(partnerId)));
//        SearchRequest searchRequest = new SearchRequest(ElasticSearchConstants.conversionIndexSearchPattern);
//        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
//        searchSourceBuilder.query(query);
//        searchRequest.source(searchSourceBuilder);
//        SearchResponse response = null;
//        try {
//            response = client.search(searchRequest, RequestOptions.DEFAULT);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        SearchHit[] searchHits = response.getHits().getHits();
//        List<Attributionconversion> attributionconversionList =
//                Arrays.stream(searchHits)
//                        .map(hit -> JSON.parseObject(hit.getSourceAsString(), Attributionconversion.class))
//                        .collect(Collectors.toList());
//
//        System.out.println("getCLickListForIPAndOfferId " + attributionconversionList);
//        return attributionconversionList;
//    }
//
//    public Map<String, Object> allReportAjax(Long appsId, String startDate, String endDate, List<String> pubArr, List<String> offerArr, Pageable pageable, DataTablesInput input) {
//
//        BoolQueryBuilder query = QueryBuilders.boolQuery();
//        System.out.println("BoolQueryBuilder " + pubArr);
//
////        if (pubArr != null && !pubArr.isEmpty()) {
////            System.out.println("BoolQueryBuilder1 " + pubArr);
////            query.must(QueryBuilders.termsQuery("partnerId", pubArr));
////        }
//
//        if (offerArr != null && !offerArr.isEmpty()) {
//            query.must(QueryBuilders.termsQuery("attributedTouchType", offerArr));
//            System.out.println("BoolQueryBuilder2 " + offerArr);
////            using This
////            var myform = document.getElementById("attributedTouchTypeSelect").value;
////            table.column(37).search(myform).draw();
//        }
//
//        String appids = String.valueOf(appsId);
//        System.out.println("appids " + appids);
//        query.must(QueryBuilders.termsQuery("appId", appids));
////        query.must(QueryBuilders.termsQuery("attributedTouchType", "conversion"));
//
//        query.must(QueryBuilders.rangeQuery("dayTimestamp").gte(startDate).lte(endDate));
//        System.out.println("query is " + query);
//
//        SearchRequest searchRequest = new SearchRequest(ElasticSearchConstants.attributionAllReportIndexSearchPattern);
//        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
//        searchSourceBuilder.query(query);
//        searchRequest.source(searchSourceBuilder);
//        searchSourceBuilder.size(pageable.getPageSize());
//        searchSourceBuilder.from((int) pageable.getOffset());
////        searchSourceBuilder.sort(getConversionFieldSortBuilder(input));
//        SearchResponse response = null;
//
//        try {
//            response = client.search(searchRequest, RequestOptions.DEFAULT);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        System.out.println("client.search " + response);
//
//        assert response != null;
//        SearchHit[] searchHits = response.getHits().getHits();
//
//        List<AttributionAllReport> attributionList =
//                Arrays.stream(searchHits)
//                        .map(hit -> JSON.parseObject(hit.getSourceAsString(), AttributionAllReport.class))
//                        .collect(Collectors.toList());
//
//        Long count = response.getHits().getTotalHits().value;
//        System.out.println("attributionList = " + attributionList.size());
//        Map<String, Object> map = new HashMap<>();
//        map.put("attributionList", attributionList);
//        map.put("count", count);
//        return map;
//
//    }
//
//
//    //    public List<AllReportDTO> allReportCHR(List<String> pubArr, List<String> offerArrY,
////                                           List<String> smartArr, List<String> groupByArr) throws IOException {
////        System.out.println("thisIsOurCulture");
////        Map<String, String> nameAndFieldMapCHR = new HashMap<>();
////        nameAndFieldMapCHR.put("Offer", "id.offerId");
////        nameAndFieldMapCHR.put("Publisher", "id.pubId");
////        nameAndFieldMapCHR.put("SmartLink", "smartLinkId");
////        System.out.println("tisIsMYnAMEMUSGFFBTFG"+nameAndFieldMapCHR);
////
////
////        //For filters
////        BoolQueryBuilder query = QueryBuilders.boolQuery();
////        if (pubArr != null && !pubArr.isEmpty()) {
////            System.out.println("thisis whatiWantP1122");
////            query.must(QueryBuilders.termsQuery("id.pubId", pubArr));
////            System.out.println(" BoolQueryBuilderPpubArr"+query);
////
////        }
////        if (offerArrY != null && !offerArrY.isEmpty()) {
////            System.out.println("thisis whatiWantP11");
////            query.must(QueryBuilders.termsQuery("id.offerId", offerArrY));
////            System.out.println(" BoolQueryBuilderofferIdr"+query);
////        }
////        if (smartArr != null && !smartArr.isEmpty()) {
////            System.out.println("thisisWhatiWantP22");
////            query.must(QueryBuilders.termsQuery("smartLinkId", smartArr));
////            System.out.println(" BoolQueryBuilderofsmartLinkIdIdr"+query);
////        }
////
////        //For creation of group by components having max count of 3000
////        List<CompositeValuesSourceBuilder<?>> sources = new ArrayList<>();
////        for (String groupBy : groupByArr) {
////            sources.add(new TermsValuesSourceBuilder(groupBy).field(nameAndFieldMapCHR.get(groupBy)));
////        }
////        System.out.print("sources = "+sources);
////        CompositeAggregationBuilder compositeAggregationBuilder =
////                new CompositeAggregationBuilder("AllReport", sources).size(3000);
////        System.out.println("thisis whatiWantP55"+compositeAggregationBuilder);
////        //
////
////        //For adding of sum aggregation fields on existing group by components
////        compositeAggregationBuilder
////                .subAggregation(AggregationBuilders.sum("clickCount").field("clickCount"))
////                .subAggregation(AggregationBuilders.sum("approvedConversions").field("approvedConversions"))
////                .subAggregation(AggregationBuilders.sum("cancelledConversions").field("cancelledConversions"))
////                .subAggregation(AggregationBuilders.sum("pendingConversions").field("pendingConversions"))
////                .subAggregation(AggregationBuilders.sum("rejectedClicks").field("rejectedClicks"))
////                .subAggregation(AggregationBuilders.sum("approvedRevenue").field("approvedRevenue"))
////                .subAggregation(AggregationBuilders.sum("cancelledRevenue").field("cancelledRevenue"))
////                .subAggregation(AggregationBuilders.sum("pendingRevenue").field("pendingRevenue"))
////                .subAggregation(AggregationBuilders.sum("approvedPayout").field("approvedPayout"))
////        ;
////        SearchRequest searchRequest = new SearchRequest("clickconversionhourlyrecord");
////        System.out.println("thisis whatiWantP77"+searchRequest);
////        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
////        searchSourceBuilder.query(query);
////        searchSourceBuilder.aggregation(compositeAggregationBuilder);
////        searchRequest.source(searchSourceBuilder);
////        SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
////        System.out.println("thisis whatiWantP78"+searchResponse.getAggregations());
////        List<AllReportDTO> allReportDTOList = new ArrayList<>();
//////         allReportDTOList =
////        ParsedComposite internalComposite = searchResponse.getAggregations().get("AllReport");
////        System.out.println("internalComposite"+internalComposite);//get aggregation bucket from name
////        for (ParsedComposite.ParsedBucket termBucket : internalComposite.getBuckets()) {//Loop through array of buckets returned
////            AllReportDTO allReportDTO = new AllReportDTO();
////            Sum aggValue = termBucket.getAggregations().get("clickCount");
////            //Fetching each sum aggregation from bucket by name
////            Sum approvedConversions = termBucket.getAggregations().get("approvedConversions");
////            Sum cancelledConversions = termBucket.getAggregations().get("cancelledConversions");
////            Sum pendingConversions = termBucket.getAggregations().get("pendingConversions");
////            Sum rejectedClicks = termBucket.getAggregations().get("rejectedClicks");
////            Sum approvedRevenue = termBucket.getAggregations().get("approvedRevenue");
////            Sum cancelledRevenue = termBucket.getAggregations().get("cancelledRevenue");
////            Sum pendingRevenue = termBucket.getAggregations().get("pendingRevenue");
////            Sum approvedPayout = termBucket.getAggregations().get("approvedPayout");
////
////            String keys = termBucket.getKeyAsString();
////
////            //For getting aggregation keys(e.g:For Offer group by key is offerId=id)
////
////            List<String> keyArr = Arrays.asList(keys.replaceAll("[{}]", "").split(","));//For multiple group by we get list of keys
////            System.out.println("keyArr"+keyArr);
////
////            Map<String, String> map = new HashMap<>();
////            for (String key : keyArr) {
////                List<String> stringList = Arrays.asList(key.split("="));
////                map.put(stringList.get(0).trim(),
////                        (stringList.size() == 2) ?
////                                stringList.get(1).trim() : null);
////            }
////            System.out.println("keyArr"+map);
////
////            putId(allReportDTO, map, groupByArr);
////            //For populating id fields in DTO which were in group by
////            allReportDTO.setClick((long) aggValue.getValue());
////            System.out.println("assdffh  "+ aggValue.getValue());
////            //For Populating aggregated sum results in DTO
////            allReportDTO.setApprovedConversions((long) approvedConversions.getValue());
////            allReportDTO.setCancelledConversions((long) cancelledConversions.getValue());
////            allReportDTO.setPendingConversions((long) pendingConversions.getValue());
////            allReportDTO.setRejectedClicks((int) rejectedClicks.getValue());
////            allReportDTO.setApprovedRevenue((long) approvedRevenue.getValue());
////            allReportDTO.setCancelledRevenue((long) cancelledRevenue.getValue());
////            allReportDTO.setPendingRevenue((long) pendingRevenue.getValue());
////            allReportDTO.setApprovedPayout((long) approvedPayout.getValue());
////
////            //For Populating aggregated sum results in DTO
////            allReportDTOList.add(new AllReportDTO(allReportDTO));
////            System.out.println("thisIsWatIWANT   "+allReportDTOList);
////        }
////        return allReportDTOList;
////    }
//    //    public Map<String, Object> conversionReport(String startDate, String endDate, List<String> pubArr, List<String> offerArr,
////                                                List<String> conversionStatuses, Pageable pageable, DataTablesInput input) {
////        BoolQueryBuilder query = QueryBuilders.boolQuery();
////        System.out.println("BoolQueryBuilder " + pubArr );
////        if (pubArr != null && !pubArr.isEmpty()) {
////            System.out.println("BoolQueryBuilder1 " + pubArr);
////            query.must(QueryBuilders.termsQuery("Partner.partnerId", pubArr));
////        }
////        if (offerArr != null && !offerArr.isEmpty()) {
////            System.out.println("BoolQueryBuilder2 " + offerArr);
////            query.must(QueryBuilders.termsQuery("app.appId", offerArr));
////        }
////        log.info("conversionStatus:"+conversionStatuses);
//////        if (advArr != null && !advArr.isEmpty()) {
//////            query.must(QueryBuilders.termsQuery("advertiserId", advArr));
//////        }
////        if (conversionStatuses != null && !conversionStatuses.isEmpty()) {
////            query.must(QueryBuilders.termsQuery("conversionStatus", conversionStatuses));
////        }
////        query.must(QueryBuilders.rangeQuery("creationDateTime").gte(startDate).lte(endDate));
////
////        SearchRequest searchRequest = new SearchRequest("conversion");
////        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
////        searchSourceBuilder.query(query);
////        searchRequest.source(searchSourceBuilder);
////        searchSourceBuilder.size(pageable.getPageSize());
////        searchSourceBuilder.from((int) pageable.getOffset());
////        searchSourceBuilder.sort(getConversionFieldSortBuilder(input));
////        SearchResponse response = null;
////
////        try {
////            response = client.search(searchRequest, RequestOptions.DEFAULT);
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
////
////        SearchHit[] searchHits = response.getHits().getHits();
////        List<Attributionconversion> conversionList =
////                Arrays.stream(searchHits)
////                        .map(hit -> JSON.parseObject(hit.getSourceAsString(), Attributionconversion.class))
////                        .collect(Collectors.toList());
////        Long count = response.getHits().getTotalHits().value;
////        Map<String, Object> map = new HashMap<>();
////        map.put("conversionList", conversionList);
////        map.put("count", count);
////        return map;
////    }
//
//
////    public Map<String, Object> clickReport(String startDate, String endDate, List<String> pubArr, List<String> offerArr,
////                                           Pageable pageable, String uuid, List<String> advArr, DataTablesInput input) {
////        BoolQueryBuilder query = QueryBuilders.boolQuery();
////        if (pubArr != null && !pubArr.isEmpty()) {
////            query.must(QueryBuilders.termsQuery("pubId", pubArr));
////        }
////        if (offerArr != null && !offerArr.isEmpty()) {
////            query.must(QueryBuilders.termsQuery("offerId", offerArr));
////        }
////        if (uuid != null && uuid != "") {
////            query.must(QueryBuilders.termQuery("id", uuid));
////        }
////        if (advArr != null && !advArr.isEmpty()) {
////            query.must(QueryBuilders.termsQuery("advertiserId", advArr));
////        }
////        query.must(QueryBuilders.rangeQuery("creationDateTime").gte(startDate).lte(endDate));
////
////        SearchRequest searchRequest = new SearchRequest(ElasticSearchConstants.clickIndexSearchPattern);
////        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
////        searchSourceBuilder.query(query);
////        searchRequest.source(searchSourceBuilder);
////        log.info("page size:" + pageable.getPageSize());
////        log.info("page offset:" + pageable.getOffset());
////        searchSourceBuilder.size(pageable.getPageSize());
////        searchSourceBuilder.from((int) pageable.getOffset());
////        searchSourceBuilder.sort(getClickFieldSortBuilder(input));
//////        searchSourceBuilder.searchAfter()
////        SearchResponse response = null;
////        try {
////            response = client.search(searchRequest, RequestOptions.DEFAULT);
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
////
////        SearchHit[] searchHits = response.getHits().getHits();
////        List<Click> clickList =
////                Arrays.stream(searchHits)
////                        .map(hit -> JSON.parseObject(hit.getSourceAsString(), Click.class))
////                        .collect(Collectors.toList());
////
////        SearchSourceBuilder searchSourceBuilder1 = new SearchSourceBuilder();
////        searchSourceBuilder1.query(query);
////        CountRequest countRequest = new CountRequest(ElasticSearchConstants.clickIndexSearchPattern);
////        countRequest.source(searchSourceBuilder);
////        Long count = 0l;
////
////        try {
////            CountResponse countResponse = client.count(countRequest, RequestOptions.DEFAULT);
////            count = countResponse.getCount();
////            /*log.info("count:" + count);*/
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
//////        SearchHits hits = response.getHits();
//////        hits.getAt((int) pageable.getOffset()).getSortValues();
////        Map<String, Object> map = new HashMap<>();
////        map.put("clickList", clickList);
////        map.put("count", count);
////        return map;
////    }
////
////    private FieldSortBuilder getClickFieldSortBuilder(DataTablesInput input) {
////        Map<String, String> orderMap = new HashMap<String, String>();
////        Order order = input.getOrder().get(0);
////        orderMap.put("orderColumnName", input.getColumns().get(order.getColumn()).getData());
////        orderMap.put("orderType", order.getDir());
////        String orderValue = orderMap.get("orderType");
////        FieldSortBuilder mySort = null;
////        switch (orderMap.get("orderColumnName")) {
////            case "offerId":
////                mySort = SortBuilders.fieldSort("offerId").order(SortOrder.fromString(orderValue));
////                break;
////            case "pubId":
////                mySort = SortBuilders.fieldSort("pubId").order(SortOrder.fromString(orderValue));
////                break;
////            default:
////                mySort = SortBuilders.fieldSort("creationDateTime").order(SortOrder.DESC);
////                break;
////        }
////        return mySort;
////    }
////
//
//    //    public void putId(AllReportDTO allReportDTO, Map<String, String> key, List<String> groupByArr) {
////        System.out.println("sdfgtresdfg"+key.get("SmartLinkId"));
////        for (String groupBy : groupByArr) {
////            switch (groupBy) {
////                case "SmartLink":
////                    System.out.println("1234567890");
////                    allReportDTO.setSmartLinkId(Long.parseLong(key.get("SmartLinkId")));
////                    break;
////                case "Publisher":
////                    System.out.println("1234567");
////                    allReportDTO.setPublisherId(Long.parseLong(key.get("Publisher")));
////                    break;
////                case "Offer":
////                    System.out.println("1234");
////                    allReportDTO.setOfferId(Long.parseLong(key.get("Offer")));
////                    break;
////
////            }
////        }
////    }
//
//
////    private FieldSortBuilder getConversionFieldSortBuilder(DataTablesInput input) {
////        Map<String, String> orderMap = new HashMap<String, String>();
////        Order order = input.getOrder().get(0);
////        orderMap.put("orderColumnName", input.getColumns().get(order.getColumn()).getData());
////        orderMap.put("orderType", order.getDir());
////        String orderValue = orderMap.get("orderType");
////        FieldSortBuilder mySort = null;
////        switch (orderMap.get("orderColumnName")) {
////            case "offerId":
////                mySort = SortBuilders.fieldSort("offer.offerId").order(SortOrder.fromString(orderValue));
////                break;
////            case "pubId":
////                mySort = SortBuilders.fieldSort("publisher.publisherId").order(SortOrder.fromString(orderValue));
////                break;
////            default:
////                mySort = SortBuilders.fieldSort("creationDateTime").order(SortOrder.DESC);
////                break;
////        }
////        return mySort;
////    }
//
//
////
////    public Set<AllReportDTO> allReportCHR(String startDate, String endDate, List<String> pubArr, List<String> offerArr,
////                                          List<String> advArr, List<String> groupByArr, String subAff) throws IOException {
////        Map<String, String> nameAndFieldMapCHR = new HashMap<>();
////        nameAndFieldMapCHR.put("Offer", "id.offerId");
////        nameAndFieldMapCHR.put("Publisher", "id.pubId");
////        nameAndFieldMapCHR.put("Advertiser", "advertiserId");
////        nameAndFieldMapCHR.put("Sub_Aff", "id.subAffiliate.keyword");
////
////        //For filters
////        BoolQueryBuilder query = QueryBuilders.boolQuery();
////        if (pubArr != null && !pubArr.isEmpty()) {
////            query.must(QueryBuilders.termsQuery("id.pubId", pubArr));
////        }
////        if (offerArr != null && !offerArr.isEmpty()) {
////            query.must(QueryBuilders.termsQuery("id.offerId", offerArr));
////        }
////        if (advArr != null && !advArr.isEmpty()) {
////            query.must(QueryBuilders.termsQuery("advertiserId", advArr));
////        }
////
////        if (subAff != null && subAff != "") {
////            query.must(QueryBuilders.termQuery("id.subAffiliate.keyword", subAff));
////        }
////        query.must(QueryBuilders.rangeQuery("id.hourTimestamp").gte(startDate).lte(endDate));
////        //
////
////        //For creation of group by components having max count of 3000
////        List<CompositeValuesSourceBuilder<?>> sources = new ArrayList<>();
////        for (String groupBy : groupByArr) {
////            sources.add(new TermsValuesSourceBuilder(groupBy).field(nameAndFieldMapCHR.get(groupBy)));
////        }
////        CompositeAggregationBuilder compositeAggregationBuilder =
////                new CompositeAggregationBuilder("AllReport", sources).size(3000);
////        //
////
////        //For adding of sum aggregation fields on existing group by components
////        compositeAggregationBuilder
////                .subAggregation(AggregationBuilders.sum("clickCount").field("clickCount"))
////                .subAggregation(AggregationBuilders.sum("approvedConversions").field("approvedConversions"))
////                .subAggregation(AggregationBuilders.sum("cancelledConversions").field("cancelledConversions"))
////                .subAggregation(AggregationBuilders.sum("pendingConversions").field("pendingConversions"))
////                .subAggregation(AggregationBuilders.sum("rejectedClicks").field("rejectedClicks"))
////                .subAggregation(AggregationBuilders.sum("approvedRevenue").field("approvedRevenue"))
////                .subAggregation(AggregationBuilders.sum("cancelledRevenue").field("cancelledRevenue"))
////                .subAggregation(AggregationBuilders.sum("pendingRevenue").field("pendingRevenue"))
////                .subAggregation(AggregationBuilders.sum("approvedPayout").field("approvedPayout"))
////        ;
////        //
////
////        SearchRequest searchRequest = new SearchRequest("campaignhourlyrecord");
////        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
////        searchSourceBuilder.query(query);
////        searchSourceBuilder.aggregation(compositeAggregationBuilder);
////        searchRequest.source(searchSourceBuilder);
////        SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
////
////        allReportDTOList = new HashSet<>();
////        ParsedComposite internalComposite = searchResponse.getAggregations().get("AllReport");//get aggregation bucket from name
////        for (ParsedComposite.ParsedBucket termBucket : internalComposite.getBuckets()) {//Loop through array of buckets returned
////            AllReportDTO allReportDTO = new AllReportDTO();
////            Sum aggValue = termBucket.getAggregations().get("clickCount");//Fetching each sum aggregation from bucket by name
////            Sum approvedConversions = termBucket.getAggregations().get("approvedConversions");
////            Sum cancelledConversions = termBucket.getAggregations().get("cancelledConversions");
////            Sum pendingConversions = termBucket.getAggregations().get("pendingConversions");
////            Sum rejectedClicks = termBucket.getAggregations().get("rejectedClicks");
////            Sum approvedRevenue = termBucket.getAggregations().get("approvedRevenue");
////            Sum cancelledRevenue = termBucket.getAggregations().get("cancelledRevenue");
////            Sum pendingRevenue = termBucket.getAggregations().get("pendingRevenue");
////            Sum approvedPayout = termBucket.getAggregations().get("approvedPayout");
////            String keys = termBucket.getKeyAsString();//For getting aggregation keys(e.g:For Offer group by key is offerId=id)
////            List<String> keyArr = Arrays.asList(keys.replaceAll("[{}]", "").split(","));//For multiple group by we get list of keys
////            Map<String, String> map = new HashMap<>();
////            for (String key : keyArr) {
////                List<String> stringList = Arrays.asList(key.split("="));
////                map.put(stringList.get(0).trim(),
////                        (stringList.size() == 2) ?
////                                stringList.get(1).trim() : null);
////            }
////            putId(allReportDTO, map, groupByArr);//For populating id fields in DTO which were in group by
////            allReportDTO.setClick((long) aggValue.getValue());//For Populating aggregated sum results in DTO
////            allReportDTO.setApprovedConversions((long) approvedConversions.getValue());
////            allReportDTO.setCancelledConversions((long) cancelledConversions.getValue());
////            allReportDTO.setPendingConversions((long) pendingConversions.getValue());
////            allReportDTO.setRejectedClicks((int) rejectedClicks.getValue());
////            allReportDTO.setApprovedRevenue((long) approvedRevenue.getValue());
////            allReportDTO.setCancelledRevenue((long) cancelledRevenue.getValue());
////            allReportDTO.setPendingRevenue((long) pendingRevenue.getValue());
////            allReportDTO.setApprovedPayout((long) approvedPayout.getValue());
////            allReportDTOList.add(new AllReportDTO(allReportDTO));
////        }
////        return allReportDTOList;
////    }
////
////    public void putId(AllReportDTO allReportDTO, Map<String, String> key, List<String> groupByArr) {
////        for (String groupBy : groupByArr) {
////            switch (groupBy) {
////                case "Offer":
////                    allReportDTO.setOfferId(Long.parseLong(key.get("Offer")));
////                    break;
////                case "Advertiser":
////                    allReportDTO.setAdvertiserId(Long.parseLong(key.get("Advertiser")));
////                    break;
////                case "Publisher":
////                    allReportDTO.setPublisherId(Long.parseLong(key.get("Publisher")));
////                    break;
////                case "Sub_Aff":
////                    allReportDTO.setSubAffiliate(key.get("Sub_Aff"));
////                    break;
////            }
////        }
////    }
////
////    public List<Click> getCLickList(String startDate, String endDate, List<String> pubArr, List<String> offerArr) {
////        BoolQueryBuilder query = QueryBuilders.boolQuery();
////      /*  if (pubArr != null && !pubArr.isEmpty()) {
////            query.must(QueryBuilders.termsQuery("pubId", pubArr));
////        }*/
////        if (offerArr != null && !offerArr.isEmpty()) {
////            query.must(QueryBuilders.termsQuery("offerId", offerArr));
////        }
////        query.must(QueryBuilders.rangeQuery("creationDateTime").gte(startDate).lte(endDate));
////
////        SearchRequest searchRequest = new SearchRequest(ElasticSearchConstants.clickIndexSearchPattern);
////        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
////        searchSourceBuilder.query(query);
////        searchRequest.source(searchSourceBuilder);
////        SearchResponse response = null;
////        try {
////            response = client.search(searchRequest, RequestOptions.DEFAULT);
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
////
////        SearchHit[] searchHits = response.getHits().getHits();
////        List<Click> clickList =
////                Arrays.stream(searchHits)
////                        .map(hit -> JSON.parseObject(hit.getSourceAsString(), Click.class))
////                        .collect(Collectors.toList());
////        return clickList;
////    }
////
////    public List<SearchHit[]> conversionExportDataList(String startDate, String endDate, List<String> pubArr, List<String> offerArr,
////                                                        List<String> conversionStatuses) throws IOException {
////        BoolQueryBuilder query = QueryBuilders.boolQuery();
////        if (pubArr != null && !pubArr.isEmpty())
////            query.must(QueryBuilders.termsQuery("publisher.publisherId", pubArr));
////
////        if (offerArr != null && !offerArr.isEmpty())
////            query.must(QueryBuilders.termsQuery("offer.offerId", offerArr));
////
////        if (conversionStatuses != null && !conversionStatuses.isEmpty())
////            query.must(QueryBuilders.termsQuery("conversionStatus", conversionStatuses));
////
////        query.must(QueryBuilders.rangeQuery("creationDateTime").gte(startDate).lte(endDate));
////
////        SearchRequest searchRequest = new SearchRequest(ElasticSearchConstants.conversionIndexSearchPattern);
////        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
////        searchSourceBuilder.query(query);
////        searchRequest.source(searchSourceBuilder);
////        final Scroll scroll = new Scroll(TimeValue.timeValueMinutes(1L));
////        searchRequest.scroll(scroll);
////        searchSourceBuilder.query(query);
////        searchSourceBuilder.size(10000);
////        searchSourceBuilder.sort(new FieldSortBuilder("_doc"));
////        searchRequest.source(searchSourceBuilder);
////
////        SearchResponse searchResponse = null;
////        try {
////            searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
////        String scrollId = searchResponse.getScrollId();
////        SearchHit[] searchHits = searchResponse.getHits().getHits();
////        int scrollNum = 0;
////        List<SearchHit[]> conversionList = new ArrayList<>();
////        while (searchHits != null && searchHits.length > 0) {
////            scrollNum++;
////            log.info("scrollNum:" + scrollNum);
////            SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId);
////            scrollRequest.scroll(scroll);
////            searchResponse = client.scroll(scrollRequest, RequestOptions.DEFAULT);
////            scrollId = searchResponse.getScrollId();
////            conversionList.add(searchHits);
////            if (conversionList.size() >= 10) {
////                return conversionList;
////            }
////            searchHits = searchResponse.getHits().getHits();
////        }
////
////        ClearScrollRequest clearScrollRequest = new ClearScrollRequest();
////        clearScrollRequest.addScrollId(scrollId);
////        ClearScrollResponse clearScrollResponse = client.clearScroll(clearScrollRequest, RequestOptions.DEFAULT);
////        return conversionList;
////    }
////
////    public List<SearchHit[]> clickExportDataList(String startDate, String endDate, List<String> pubArr, List<String> offerArr,
////                                                 List<String> advArr, String clickId) throws IOException {
////        BoolQueryBuilder query = QueryBuilders.boolQuery();
////        if (pubArr != null && !pubArr.isEmpty()) {
////            query.must(QueryBuilders.termsQuery("pubId", pubArr));
////        }
////        if (offerArr != null && !offerArr.isEmpty()) {
////            query.must(QueryBuilders.termsQuery("offerId", offerArr));
////        }
////        if (clickId != null && !clickId.equals("")) {
////            query.must(QueryBuilders.termQuery("id", clickId));
////        }
////        if (advArr != null && !advArr.isEmpty()) {
////            query.must(QueryBuilders.termsQuery("advertiserId", advArr));
////        }
////        query.must(QueryBuilders.rangeQuery("creationDateTime").gte(startDate).lte(endDate));
////        SearchRequest searchRequest = new SearchRequest(ElasticSearchConstants.clickIndexSearchPattern);
////        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
////        searchSourceBuilder.query(query);
////        searchRequest.source(searchSourceBuilder);
////        final Scroll scroll = new Scroll(TimeValue.timeValueMinutes(1L));
////        searchRequest.scroll(scroll);
////        searchSourceBuilder.query(query);
////        searchSourceBuilder.size(10000);
////        searchSourceBuilder.sort(new FieldSortBuilder("_doc"));
////        searchRequest.source(searchSourceBuilder);
////
////        SearchResponse searchResponse = null;
////        try {
////            searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
////        String scrollId = searchResponse.getScrollId();
////        SearchHit[] searchHits = searchResponse.getHits().getHits();
////        int scrollNum = 0;
////        List<SearchHit[]> clickList = new ArrayList<>();
////        while (searchHits != null && searchHits.length > 0) {
////            scrollNum++;
////            log.info("scrollNum:" + scrollNum);
////            SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId);
////            scrollRequest.scroll(scroll);
////            searchResponse = client.scroll(scrollRequest, RequestOptions.DEFAULT);
////            scrollId = searchResponse.getScrollId();
////            clickList.add(searchHits);
////            if (clickList.size() >= 10) {
////                return clickList;
////            }
////            searchHits = searchResponse.getHits().getHits();
////        }
////
////        ClearScrollRequest clearScrollRequest = new ClearScrollRequest();
////        clearScrollRequest.addScrollId(scrollId);
////        ClearScrollResponse clearScrollResponse = client.clearScroll(clearScrollRequest, RequestOptions.DEFAULT);
////        return clickList;
////    }
////
////    public Long clickExportDataListCount(String startDate, String endDate, List<String> offerIds, List<String> pubIds, List<String> advIds, String uuid) {
////        BoolQueryBuilder query = QueryBuilders.boolQuery();
////        if (pubIds != null && !pubIds.isEmpty()) {
////            query.must(QueryBuilders.termsQuery("pubId", pubIds));
////        }
////        if (offerIds != null && !offerIds.isEmpty()) {
////            query.must(QueryBuilders.termsQuery("offerId", offerIds));
////        }
////        if (uuid != null && !uuid.equals("")) {
////            query.must(QueryBuilders.termQuery("id", uuid));
////        }
////        if (advIds != null && !advIds.isEmpty()) {
////            query.must(QueryBuilders.termsQuery("advertiserId", advIds));
////        }
////        query.must(QueryBuilders.rangeQuery("creationDateTime").gte(startDate).lte(endDate));
////        long count = 0L;
////        CountRequest countRequest = new CountRequest(ElasticSearchConstants.clickIndexSearchPattern);
////        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
////        searchSourceBuilder.query(query);
////        countRequest.source(searchSourceBuilder);
////        try {
////            CountResponse countResponse = client.count(countRequest, RequestOptions.DEFAULT);
////            count = countResponse.getCount();
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
////        return count;
////    }
////
////    public Long conversionExportDataListCount(String startDate, String endDate, List<String> offerArr, List<String> pubArr,List<String> conversionStatuses) {
////        BoolQueryBuilder query = QueryBuilders.boolQuery();
////        if (pubArr != null && !pubArr.isEmpty()) {
////            query.must(QueryBuilders.termsQuery("publisher.publisherId", pubArr));
////        }
////        if (offerArr != null && !offerArr.isEmpty()) {
////            query.must(QueryBuilders.termsQuery("offer.offerId", offerArr));
////        }
////        if (conversionStatuses != null && !conversionStatuses.isEmpty()) {
////            query.must(QueryBuilders.termsQuery("conversionStatus", conversionStatuses));
////        }
////        query.must(QueryBuilders.rangeQuery("creationDateTime").gte(startDate).lte(endDate));
////
////        long count = 0L;
////        CountRequest countRequest = new CountRequest(ElasticSearchConstants.conversionIndexSearchPattern);
////        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
////        searchSourceBuilder.query(query);
////        countRequest.source(searchSourceBuilder);
////        try {
////            CountResponse countResponse = client.count(countRequest, RequestOptions.DEFAULT);
////            count = countResponse.getCount();
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
////        return count;
////    }
//
//
//}
