//package com.attribution.panel.dao;
//
//import com.alibaba.fastjson.JSON;
//import com.attribution.panel.bean.AttributionClick;
//import com.attribution.panel.constants.ElasticSearchConstants;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import lombok.extern.slf4j.Slf4j;
//import org.elasticsearch.action.index.IndexRequest;
//import org.elasticsearch.action.index.IndexResponse;
//import org.elasticsearch.action.search.SearchRequest;
//import org.elasticsearch.action.search.SearchResponse;
//import org.elasticsearch.client.RequestOptions;
//import org.elasticsearch.client.RestHighLevelClient;
//import org.elasticsearch.client.core.CountRequest;
//import org.elasticsearch.client.core.CountResponse;
//import org.elasticsearch.index.query.BoolQueryBuilder;
//import org.elasticsearch.index.query.QueryBuilders;
//import org.elasticsearch.search.SearchHit;
//import org.elasticsearch.search.builder.SearchSourceBuilder;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.stereotype.Component;
//
//import java.util.Map;
//
//@Component
//@Slf4j
//public class ClickQueryBuilder {
//    @Autowired
//    @Qualifier("elasticClient")
//    RestHighLevelClient client;
//
////    @Autowired
////    CSVService csvService;
//
////    @Autowired
////    RabbitTemplate rabbitTemplate;
//
//
//    public void save(AttributionClick click) {
//        ObjectMapper objectMapper = new ObjectMapper();
//        log.info("Inside click save " + click.getId());
//        IndexRequest indexRequest = new IndexRequest("attributionclick").id(click.getId()).source(objectMapper.convertValue(click, Map.class));
//        try {
//            IndexResponse indexResponse = client.index(indexRequest, RequestOptions.DEFAULT);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    public AttributionClick findById(String id) {
//        System.out.println("findById1 " + id);
//        BoolQueryBuilder query = QueryBuilders.boolQuery();
//        query.must(QueryBuilders.termQuery("id", id));
//        SearchRequest searchRequest = new SearchRequest("attributionclick");
//        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
//        searchSourceBuilder.query(query);
//        searchRequest.source(searchSourceBuilder);
//        SearchResponse response = null;
//        AttributionClick click = null;
//        try {
//            response = client.search(searchRequest, RequestOptions.DEFAULT);
//            System.out.println("findById2 "+ response);
//            SearchHit[] searchHits = response.getHits().getHits();
//            System.out.println("searchHits "+ searchHits[0].getSourceAsString());
//            click = JSON.parseObject(searchHits[0].getSourceAsString(), AttributionClick.class);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        System.out.println("findById3 "+ click);
//        return click;
//    }
//
//    public Long count() {
//        long count = 0L;
//        CountRequest countRequest = new CountRequest(ElasticSearchConstants.clickIndexSearchPattern);
//        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
//        countRequest.source(searchSourceBuilder);
//        try {
//            CountResponse countResponse = client.count(countRequest, RequestOptions.DEFAULT);
//            count = countResponse.getCount();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return count;
//    }
//
//
////    public List<Click> findByOfferIdAndPubIdOrderByCreationDateTimeDesc(Long offerId, Long pubId) {
////        BoolQueryBuilder query = QueryBuilders.boolQuery();
////        query.must(QueryBuilders.termQuery("offerId", offerId));
////        query.must(QueryBuilders.termQuery("pubId", pubId));
////
////        SearchRequest searchRequest = new SearchRequest(ElasticSearchConstants.clickIndexSearchPattern);
////        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
////        searchSourceBuilder.query(query);
////        searchRequest.source(searchSourceBuilder);
////        searchSourceBuilder.sort(SortBuilders.fieldSort("creationDateTime").order(SortOrder.DESC));
////        SearchResponse response = null;
////        try {
////            response = client.search(searchRequest, RequestOptions.DEFAULT);
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
////        SearchHit[] searchHits = response.getHits().getHits();
////        List<Click> clickList =
////                Arrays.stream(searchHits)
////                        .map(hit -> JSON.parseObject(hit.getSourceAsString(), Click.class))
////                        .collect(Collectors.toList());
////        return clickList;
////    }
////
////    public List<Click> findByPubClickIdOrderByCreationDateTimeDesc(String pubClickId) {
////        BoolQueryBuilder query = QueryBuilders.boolQuery();
////        query.must(QueryBuilders.termQuery("pubClickId", pubClickId));
////
////        SearchRequest searchRequest = new SearchRequest(ElasticSearchConstants.clickIndexSearchPattern);
////        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
////        searchSourceBuilder.query(query);
////        searchRequest.source(searchSourceBuilder);
////        searchSourceBuilder.sort(SortBuilders.fieldSort("creationDateTime").order(SortOrder.DESC));
////        SearchResponse response = null;
////        try {
////            response = client.search(searchRequest, RequestOptions.DEFAULT);
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
////        SearchHit[] searchHits = response.getHits().getHits();
////        List<Click> clickList =
////                Arrays.stream(searchHits)
////                        .map(hit -> JSON.parseObject(hit.getSourceAsString(), Click.class))
////                        .collect(Collectors.toList());
////        return clickList;
////    }
////
////    public Long countByOfferIdAndPubIdAndCreationDateTimeBetween(Long offerId, Long pubId, String startDate, String endDate) {
////        BoolQueryBuilder query = QueryBuilders.boolQuery();
////        query.must(QueryBuilders.termQuery("offerId", offerId));
////        query.must(QueryBuilders.termQuery("pubId", pubId));
////        query.must(QueryBuilders.rangeQuery("creationDateTime").gte(startDate).lte(endDate));
////        long count = 0L;
////        CountRequest countRequest = new CountRequest(ElasticSearchConstants.clickIndexSearchPattern);
////        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
////        searchSourceBuilder.query(query);
////        countRequest.source(searchSourceBuilder);
////        try {
////            CountResponse countResponse = client.count(countRequest, RequestOptions.DEFAULT);
////            count = countResponse.getCount();
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
////        return count;
////    }
////
////    public void deleteById(String id) {
////        BoolQueryBuilder query = QueryBuilders.boolQuery();
////        query.must(QueryBuilders.termQuery("id", id));
////        DeleteByQueryRequest request = new DeleteByQueryRequest(ElasticSearchConstants.clickIndexSearchPattern);
////        request.setQuery(query);
////        try {
////            BulkByScrollResponse response = client.deleteByQuery(request, RequestOptions.DEFAULT);
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
////    }
////
////    public void deleteByPubClickId(String id) {
////        BoolQueryBuilder query = QueryBuilders.boolQuery();
////        query.must(QueryBuilders.termQuery("pubClickId", id));
////        DeleteByQueryRequest request = new DeleteByQueryRequest(ElasticSearchConstants.clickIndexSearchPattern);
////        request.setQuery(query);
////        try {
////            BulkByScrollResponse response = client.deleteByQuery(request, RequestOptions.DEFAULT);
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
////    }
////
////    public void deleteByOfferId(Long id) {
////        BoolQueryBuilder query = QueryBuilders.boolQuery();
////        query.must(QueryBuilders.termQuery("offerId", id));
////        DeleteByQueryRequest request = new DeleteByQueryRequest(ElasticSearchConstants.clickIndexSearchPattern);
////        request.setQuery(query);
////        try {
////            BulkByScrollResponse response = client.deleteByQuery(request, RequestOptions.DEFAULT);
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
////    }
//
//
///*    public void restoreClickIndex(String snapshotRepoName, String snapshotName, String indexName) {
//        RestoreSnapshotRequest request = new RestoreSnapshotRequest(snapshotRepoName,
//                snapshotName);
//        request.indices("*"+indexName+"*");
//        request.renamePattern(indexName);
//        request.renameReplacement("restored_"+indexName);
//        request.indexSettings(
//                Settings.builder()
//                        .put("index.number_of_replicas", 0)
//                        .build());
//
//        request.ignoreIndexSettings("index.refresh_interval", "index.search.idle.after");
//        request.indicesOptions(new IndicesOptions(
//                EnumSet.of(IndicesOptions.Option.IGNORE_UNAVAILABLE),
//                EnumSet.of(IndicesOptions.WildcardStates.OPEN)));
//
//        request.waitForCompletion(true);
//        request.partial(false);
//        request.includeGlobalState(false);
//        request.includeAliases(false);
//        try {
//            log.info("Before restore index:");
//            client.snapshot().restoreAsync(request, RequestOptions.DEFAULT, new ActionListener<RestoreSnapshotResponse>() {
//                @SneakyThrows
//                @Override
//                public void onResponse(RestoreSnapshotResponse restoreSnapshotResponse) {
//                    log.info("restoreSnapshotResponse:" + restoreSnapshotResponse);
//                    Map<String,Object> map = CSVService.queue.poll();
//                    log.info("map:"+map.toString());
//                    rabbitTemplate.convertAndSend("app1-exchange", "exportData-routing-key", map);
//                }
//
//                @Override
//                public void onFailure(Exception e) {
//                    e.printStackTrace();
//                }
//            });
////            log.info("response:"+response);
//            log.info("After restore index:");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }*/
//
//
////    public void createIndex(String date) throws IOException {
////        CreateIndexRequest request = new CreateIndexRequest("click" + date);
////        request.settings(Settings.builder()
////                .put("index.number_of_shards", 1)
////                .put("index.number_of_replicas", 1)
////                .put("index.refresh_interval", "30s")
////                .put("index.lifecycle.name","delete_index")
////        );
////        try {
////            CreateIndexResponse createIndexResponse = client.indices().create(request, RequestOptions.DEFAULT);
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
////    }
////
////    //Function to get last snapshot of day,using during index restore
////    public boolean checkIndexExists(String index) {
////        GetIndexRequest request = new GetIndexRequest(index);
////        boolean exists = false;
////        try {
////            exists = client.indices().exists(request, RequestOptions.DEFAULT);
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
////
////        return exists;
////    }
////
////    //Function to get last snapshot of day,using during index restore
////    public String getLastSnapshotOfDay(String snapshotPattern, String repositoryName) {
////        GetSnapshotsRequest request = new GetSnapshotsRequest();
////        request.repository(repositoryName);
////        String[] snapshots = {snapshotPattern};
////        request.snapshots(snapshots);
////        GetSnapshotsResponse response = null;
////        try {
////            response = client.snapshot().get(request, RequestOptions.DEFAULT);
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
////
////        List<SnapshotInfo> snapshotInfos = response.getSnapshots();
////
////        List<SnapshotInfo> modifiableList = new ArrayList<>(snapshotInfos);
////
////        Collections.sort(modifiableList, Comparator.comparing(x -> x.snapshotId()));
////
////        SnapshotInfo snapShotInfo = snapshotInfos.get(snapshotInfos.size() - 1);
////        String snapshotName = snapShotInfo.snapshotId().getName();
////        return snapshotName;
////    }
//
//}
