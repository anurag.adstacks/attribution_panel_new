//package com.attribution.panel.dao;
//
//
//import com.adstacks.attribution.bean.App;
//import org.springframework.stereotype.Repository;
//
//import javax.persistence.EntityManager;
//import javax.persistence.PersistenceContext;
//import javax.persistence.criteria.CriteriaBuilder;
//import javax.persistence.criteria.CriteriaQuery;
//import javax.persistence.criteria.Predicate;
//import javax.persistence.criteria.Root;
//import java.util.ArrayList;
//import java.util.List;
//
//@Repository
//public class AppRepositoryImpl implements AppRepositoryCustom{
//    @PersistenceContext
//    EntityManager em;
//
//    @Override
//    public List<App> filtersAppsTable(int first, int len, String searchVal) {
//        CriteriaBuilder cb = em.getCriteriaBuilder();
//        CriteriaQuery<App> cq = cb.createQuery(App.class);
//        Root<App> root = cq.from(App.class);
//        List<Predicate> predicates = appTablePredicate(cb, root, cq,searchVal);
//        cq.where(predicates.toArray(new Predicate[0]));
//        return em.createQuery(cq).setFirstResult(first).setMaxResults(len).getResultList();
//    }
//    @Override
//    public List<Long> countAppsData(String searchVal) {
//        CriteriaBuilder cb = em.getCriteriaBuilder();
//        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
//        Root<App> root = cq.from(App.class);
//        List<Predicate> predicates = appTablePredicate(cb, root, cq,searchVal);
//        cq.select(cb.count(root));
//        cq.where(predicates.toArray(new Predicate[0]));
//        return em.createQuery(cq).getResultList();
//    }
//    private List<Predicate> appTablePredicate(CriteriaBuilder cb, Root<App> root, CriteriaQuery<?> cq, String searchVal) {
//        List<Predicate> predicates = new ArrayList<>();
//        Predicate predicate = searchValue(cb, root, cq, searchVal);
//        if (predicate != null)
//            predicates.add(predicate);
//        return predicates;
//    }
//
//    private Predicate searchValue(CriteriaBuilder cb, Root<App> root, CriteriaQuery<?> cq, String searchVal) {
//        if (searchVal.equals(""))
//            return null;
//        else
//            return cb.like(root.get("name"), "%" + searchVal + "%");
//    }
//}
