//package com.attribution.panel.dao;
//
//import com.attribution.panel.bean.App;
//import com.attribution.panel.bean.Event;
//import com.attribution.panel.bean.UserEvent;
//import com.attribution.panel.dto.UserEventDTO;
//import com.attribution.panel.enums.CountryEnum;
//import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
//import org.springframework.data.jpa.datatables.mapping.Order;
//import org.springframework.stereotype.Repository;
//
//import javax.persistence.EntityManager;
//import javax.persistence.PersistenceContext;
//import javax.persistence.criteria.*;
//import java.time.ZonedDateTime;
//import java.util.ArrayList;
//import java.util.List;
//
//@Repository
//public class UserEventRepositoryImpl implements UserEventRepositoryCustom {
//    @PersistenceContext
//    EntityManager em;
//
//    @Override
//    public List<UserEventDTO> findByAppIdAndStartBetweenEndDate(DataTablesInput tablesInput, ZonedDateTime startDate, ZonedDateTime endDate, int first, int len, Long appId, List<Long> eventIds, String deviceId, List<String> countryIds){
//        CriteriaBuilder cb = em.getCriteriaBuilder();
//        CriteriaQuery<UserEventDTO> cq = cb.createQuery(UserEventDTO.class);
//        Root<UserEvent> root = cq.from(UserEvent.class);
//        Join<UserEvent, Event> event = root.join("event", JoinType.INNER);
//        Order order = tablesInput.getOrder().get(0);
//        String orderType = order.getDir();
//        String orderColumn = tablesInput.getColumns().get(order.getColumn()).getData();
//        if (orderType.equals("asc")) {
//            cq.orderBy(cb.asc(root.get(orderColumn)));
//        } else {
//            cq.orderBy(cb.desc(root.get(orderColumn)));
//        }
//        cq.distinct(true);
//        List<Predicate> predicates = userEventTablePredicate(cb, root, cq, appId, eventIds, deviceId, countryIds, startDate, endDate);
//        cq.where(predicates.toArray(new Predicate[0]))
//                .multiselect(root.get("id"), root.get("param1"), root.get("param2"), root.get("param3"), root.get("param4"), root.get("param5"), root.get("param6"), root.get("param7"),
//                        root.get("param8"), root.get("param9"), root.get("param10"), root.get("ip"), root.get("os"), root.get("google_aid"), root.get("deviceName"), root.get("country"), event.get("eventName"), root.get("creationDateTime"));
//        return em.createQuery(cq).setFirstResult(first).setMaxResults(len).getResultList();
//    }
//    @Override
//    public List<Long> getUserEventFilterCount(Long appId, List<Long> eventIds, String deviceId, List<String> countryIds, ZonedDateTime startDate, ZonedDateTime endDate){
//        CriteriaBuilder cb = em.getCriteriaBuilder();
//        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
//        Root<UserEvent> root = cq.from(UserEvent.class);
//        Join<UserEvent, Event> event = root.join("event", JoinType.INNER);
//        cq.distinct(true);
//        List<Predicate> predicates = userEventTablePredicate(cb, root, cq, appId, eventIds, deviceId, countryIds, startDate, endDate);
//        cq.select(cb.count(root));
//        cq.where(predicates.toArray(new Predicate[0]));
//        return em.createQuery(cq).getResultList();
//    }
//
//    private List<Predicate> userEventTablePredicate(CriteriaBuilder cb, Root<UserEvent> root, CriteriaQuery<?> cq, Long appId, List<Long> eventIds, String deviceId, List<String> countryIds, ZonedDateTime startDate, ZonedDateTime endDate) {
//        List<Predicate> predicates = new ArrayList<>();
//        Predicate predicate1 = filterApp(root, cq, appId);
//        Predicate predicate2 = filterEvent(cb, root, cq, eventIds);
//        Predicate predicate3 = filterDevice(cb, root, cq, deviceId);
//        Predicate predicate4 = filterCountry(cb, root, cq, countryIds);
//        Predicate predicate5 = filterDate(cb, root, cq, startDate, endDate);
//        if (predicate1 != null)
//            predicates.add(predicate1);
//        if (predicate2 != null)
//            predicates.add(predicate2);
//        if (predicate3 != null)
//            predicates.add(predicate3);
//        if (predicate4 != null)
//            predicates.add(predicate4);
//        if (predicate5 != null)
//            predicates.add(predicate5);
//        return predicates;
//    }
//
//    private Predicate filterDate(CriteriaBuilder cb, Root<UserEvent> root, CriteriaQuery<?> cq, ZonedDateTime startDate, ZonedDateTime endDate) {
//        return cb.between(root.get("creationDateTime"), new java.sql.Date(startDate.toInstant().toEpochMilli()), new java.sql.Date(endDate.toInstant().toEpochMilli()));
//    }
//
//    private Predicate filterCountry(CriteriaBuilder cb, Root<UserEvent> root, CriteriaQuery<?> cq, List<String> countryIds) {
//        List<CountryEnum> CountryEnumCodes=new ArrayList<>();
//        for(String countryCode:countryIds)
//            CountryEnumCodes.add(CountryEnum.valueOf(countryCode));
//        if (countryIds.size()==0)
//            return null;
//        else
//            return root.get("country").in(CountryEnumCodes);
//    }
//
//    private Predicate filterDevice(CriteriaBuilder cb, Root<UserEvent> root, CriteriaQuery<?> cq, String deviceId) {
//        if (deviceId.isEmpty())
//            return null;
//        else
//           return cb.equal(root.get("google_aid"), deviceId);
//    }
//
//    private Predicate filterEvent(CriteriaBuilder cb, Root<UserEvent> root, CriteriaQuery<?> cq, List<Long> eventIds) {
//        Join<UserEvent, Event> event = root.join("event", JoinType.INNER);
//        if (eventIds.size()==0)
//            return null;
//        else
//            return event.get("id").in(eventIds);
//    }
//
//    private Predicate filterApp(Root<UserEvent> root, CriteriaQuery<?> cq, Long appId) {
//        Join<UserEvent, App> app = root.join("app", JoinType.INNER);
//        return app.get("id").in(appId);
//    }
//
//}
