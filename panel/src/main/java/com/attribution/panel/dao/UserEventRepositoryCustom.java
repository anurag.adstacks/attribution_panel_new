//package com.attribution.panel.dao;
//
//import com.attribution.panel.dto.UserEventDTO;
//import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
//
//import java.time.ZonedDateTime;
//import java.util.List;
//
//public interface UserEventRepositoryCustom {
//    List<UserEventDTO> findByAppIdAndStartBetweenEndDate(DataTablesInput tablesInput, ZonedDateTime startDate, ZonedDateTime endDate, int first, int len, Long appId, List<Long> eventIds, String deviceId, List<String> countryIds);
//
//    List<Long> getUserEventFilterCount(Long appId, List<Long> eventIds, String deviceId, List<String> countryIds, ZonedDateTime startDate, ZonedDateTime endDate);
//}
