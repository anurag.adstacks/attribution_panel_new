package com.attribution.panel.bean;

import com.attribution.panel.enums.AccessStatus;
import com.attribution.panel.enums.ConversionPoint;
import com.attribution.panel.enums.PayoutModel;
import com.attribution.panel.enums.RevenueModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor
public class Event implements Serializable {

    private Long id;

    private String eventName;

    private String eventValue;
    private double revenue;

    private String eventRevenueCurrency;

    private double eventRevenueUsd;

    private String description;

    //	@JsonIgnore
    private boolean status;

    //	@JsonProperty("e_tkn")
    private String token; //Event token to be integrated in postback url

    //	@JsonIgnore

    private float payout;

    private Long appsId;
    private Long partnersId;

    //	@JsonIgnore
//    @Enumerated(EnumType.STRING)
//    private ConvTrackProto proto;

    private App app;

    //	@JsonIgnore
    private boolean privateEvent;

    private RevenueModel revenueModel;

    private PayoutModel payoutModel;

    //	@JsonIgnore
    private AccessStatus accessStatus;

    //	@JsonIgnore
    private boolean multiConv;

    private String eventKey;

    private int eventCount;

    private ConversionPoint convPoint;

    private String creationDateTime;    //date

    private String updateDateTime;
    private List<Partner> partners;

    //    public Event(EventDTO event) {
//        if (event.getId() != null) {
//            this.id = event.getId();
//        }
//        this.eventName = event.getEventName();
//        this.description = event.getDescription();
//        this.status = event.isStatus();
//        this.token = event.getToken();
//        this.revenue = event.getRevenue();
//        this.revenueModel = event.getRevenueModel();
//        this.privateEvent = event.isPrivateEvent();
//        this.accessStatus = event.getAccessStatus();
//        this.multiConv = event.isMultiConv();
//        this.convPoint = event.getConvPoint();
//        this.app = event.getApp();
//        this.creationDateTime = String.valueOf(event.getCreationDateTime());
//        this.updateDateTime = String.valueOf(event.getUpdateDateTime());
//        this.partners=(event.getPartnerTrackingDTOS() == null) ? null : convertPubs2(event.getPartnerTrackingDTOS());
//    }

    public Event(Event events) {
        this.id = events.getId();
        this.eventName = events.getEventName();
        this.description = events.getDescription();
        this.status = events.isStatus();
        this.token = events.getToken();
        this.revenue = events.getRevenue();
        this.privateEvent = events.isPrivateEvent();
        this.revenueModel = events.getRevenueModel();
        this.accessStatus = events.getAccessStatus();
        this.multiConv = events.isMultiConv();
        this.convPoint = events.getConvPoint();
        this.app = events.getApp();
        this.creationDateTime = events.getCreationDateTime();
        this.updateDateTime = events.getUpdateDateTime();
        this.partners=(events.getPartners() == null) ? null : convertPubs(events.getPartners());
    }

    public List<Partner> convertPubs(List<Partner> partList) {
        List<Partner> pubs = new ArrayList<>();
        for (Partner partLists : partList) {
            pubs.add(new Partner(partLists));
        }
        return pubs;
    }

//    public List<Partner> convertPubs2(List<PartnerTrackingDTO> partList) {
//        List<Partner> pubs = new ArrayList<>();
//        for (PartnerTrackingDTO partLists : partList) {
//            pubs.add(new Partner(partLists));
//        }
//        return pubs;
//    }

    //INSERT INTO `attribution`.`event` (`id`, `multi_conv`, `payout`, `private_event`, `revenue`, `status`, `app_id`) VALUES ('4', 0, '9.5', 0, '9.5', 0, '3');

/*    public void updateEvent(EventSaveDTO createEventDTO) {
        if (createEventDTO.getEventName() != null)
            this.eventName = createEventDTO.getEventName();
        if (createEventDTO.getDescription() != null)
            this.description = createEventDTO.getDescription();
        if (createEventDTO.getPayout() != 0)
            this.payout = createEventDTO.getPayout();
        if (createEventDTO.getRevenue() != 0)
            this.revenue = createEventDTO.getRevenue();
        if (createEventDTO.getPayoutModel() != null)
            this.payoutModel = createEventDTO.getPayoutModel();
        if (createEventDTO.getRevenueModel() != null)
            this.revenueModel=createEventDTO.getRevenueModel();
        if (createEventDTO.getAccessStatus() != null)
            this.accessStatus=createEventDTO.getAccessStatus();

    }*/

}




/*import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.util.Date;
@Entity
@Data
public class Event {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String eventName;
    private String eventKey;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "app_id", nullable = false)
    @JsonIgnore
    @ToString.Exclude
    @EqualsAndHashCode.Include
    @NotFound(action = NotFoundAction.IGNORE)
    private App app;
}*/
