package com.attribution.panel.bean;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.UUID;

@Data
@NoArgsConstructor
public class Partner {

    private Long id;

    private String name;

    private String email;
    private String phone;
//    private String imType;
//    private String imId;
//    private String fax;
    private String company;
//    private String jobTitle;
//    private Integer numEmp;
//    private String addr1;
//    private String addr2;
//    private String city;
    private String country;
    private String state;
//    private String zip;

    private Boolean status;

    private String postBack;
//    private float payout;
//    private Float cutFactor;
//    private Integer offset;
//
//    private float revenue;
//    @Column(columnDefinition = "bigint(20) default 0")
//    private long totalClicks;
//    @Column(columnDefinition = "bigint(20) default 0")
//    private long grossClicks;
//    @Column(columnDefinition = "bigint(20) default 0")
//    private long conversions;
//


    private Date creationDateTime;    //date


    private Date updateDateTime;

//    @OneToOne(cascade = CascadeType.ALL)
//    @JoinColumn(unique = true)
//    @JsonIgnore
//    @ToString.Exclude
//    private CampaignRule rule;

//    @OneToOne(fetch = FetchType.EAGER)
//    @ToString.Exclude
//    @DiffIgnore
//    private User user;


    private String uuid = UUID.randomUUID().toString().replace("-", "");


//    public Partner(PartnerTrackingDTO partnerTrackingDTO){
//        this.id = partnerTrackingDTO.getId();
//        this.name = partnerTrackingDTO.getName();
//        this.email = partnerTrackingDTO.getEmail();
//        this.phone = partnerTrackingDTO.getPhone();
//        this.company = partnerTrackingDTO.getCompany();
//    }

    public Partner(String partnerName, Long partnerId) {
        this.name = partnerName;
        this.id = partnerId;
    }

    public Partner(Partner partLists) {
        this.id = partLists.getId();
        this.name = partLists.getName();
        this.email = partLists.getEmail();
        this.phone = partLists.getPhone();
        this.company = partLists.getCompany();
        this.country = partLists.getCountry();
        this.state = partLists.getState();
        this.uuid = partLists.getUuid();
        this.creationDateTime = partLists.getCreationDateTime();
        this.updateDateTime = partLists.getUpdateDateTime();
        this.status = partLists.getStatus();
        this.postBack = partLists.getPostBack();
    }


/*
    public Publisher(PublisherDTO pub) {
        if(pub.getId()!=null){
            this.id=pub.getId();}
        this.fname=pub.getFname();
        this.lname=pub.getLname();
        this.email=pub.getEmail();
        this.phone=pub.getPhone();
        this.imType=pub.getImType();
        this.imId=pub.getImId();
        this.fax=pub.getFax();
        this.company=pub.getCompany();
        this.jobTitle=pub.getJobTitle();
        this.addr1=pub.getAddr1();
        this.addr2=pub.getAddr2();
        this.city=pub.getCity();
        this.country=pub.getCountry();
        this.state=pub.getState();
        this.zip=pub.getZip();
        this.status=pub.getStatus();
        this.uuid=pub.getUuid();
        this.pb=pub.getPb();
        this.cutFactor=pub.getCutFactor();
        this.offset=pub.getOffset();
        if(pub.getCreationDateTime()!=null)
            this.creationDateTime=pub.getCreationDateTime();
    }
*/

    /*public Partner(Long pubId, User userDetails, String fname, String lname, String email, String phone, String imType, String imId, String fax,
                   String company, String jobTitle, String addr1, String addr2, String city, String country, String state, String zip,
                   Boolean status, String pb, Float cutFactor, Integer offset) {
        if (pubId != null) {
            this.id = pubId;
        }
        this.user = userDetails;
        this.fname = fname;
        this.lname = lname;
        this.email = email;
        this.phone = phone;
        this.imType = imType;
        this.imId = imId;
        this.fax = fax;
        this.company = company;
        this.jobTitle = jobTitle;
        this.addr1 = addr1;
        this.addr2 = addr2;
        this.city = city;
        this.country = country;
        this.state = state;
        this.zip = zip;
        this.status = status;
        this.pb = pb;
        this.cutFactor = cutFactor;
        this.offset = offset;
    }
*/

}
