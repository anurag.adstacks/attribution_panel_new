package com.attribution.panel.bean;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Category {

    private Long id;

    private String name;


    private Date creationDateTime; // date

    private Date updateDateTime;

    private App app;
}
