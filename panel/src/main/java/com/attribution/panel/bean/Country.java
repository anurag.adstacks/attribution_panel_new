package com.attribution.panel.bean;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

@Data
public class Country implements Serializable {

    public Long id;

    public String shortName;

    public String name;

    public Integer phoneCode;

//    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
//    @Fetch(FetchMode.SELECT)
//    private List<State> state;
}
