package com.attribution.panel.bean;


import com.attribution.panel.enums.ConversionStatus;
import lombok.Data;
import lombok.NoArgsConstructor;
import nl.basjes.parse.useragent.UserAgent;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

@Data
@NoArgsConstructor
@Component
public class AttributionClick implements Serializable {

    private String id;

    private String pClickId;  // yeah jab publisher humara link lgaiega toh yeah id uski taraf se aiieghi
    private String gaid; //android device id
    private String idfa; //iOS device id
    private String ip;    //IP address
    private String country;

    private long durationInMillis; //Calculate total duration of time in completing a request.

    private Long pId; //partnerID
    private String pName;

    private int pResp;

    private String agent; //user agent

    //We can use P for Passed and A for Approved
    private String clickMessage; //message for click rejection or approval
    private boolean isGross;

    private String subPid;

    private ConversionStatus conversionStatus;

    private String source;


    private String s1;

    private Long appID;
    private String appName;

    private float revenue;

    private float payout;



//    private DeviceId deviceId;
    private String operatingSystemNameVersion;
    private String deviceName;
    private String agentNameVersion;
    private String layoutEngineNameVersion;


    private String devOs;

    //    @Field(type = FieldType.Date, format = DateFormat.date_optional_time)

    //    @Field(type = FieldType.Date, format = DateFormat.date_optional_time)

    private String creationDateTime;

    private String updateDateTime;


    public AttributionClick(String pClickId, String devOs, UserAgent agent, String agentt, String subPid, String s1, String source, Partner partner, Long pId,
            App app, boolean isGross,
            String ip, String country) {
        this.id = UUID.randomUUID().toString().replace("-", "");
        this.pClickId = pClickId;
        this.agent = agentt;
        this.isGross = isGross;
        this.subPid = subPid;
        this.source = source;
        this.devOs = devOs;
        this.s1 = s1;
        this.pId = pId;
        this.pName = partner.getName();
        this.appID = app.getId();
        this.appName = app.getName();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        this.creationDateTime = dateFormat.format(new Date());
        this.updateDateTime = dateFormat.format(new Date());
//        this.creationDateTime = getCreationDateTime();
//        this.updateDateTime = getUpdateDateTime();
        this.ip = ip;
        this.country = country;
        this.operatingSystemNameVersion = (agent.getValue("OperatingSystemNameVersion")==null)?"":agent.getValue("OperatingSystemNameVersion");
        this.deviceName = (agent.getValue("DeviceName")==null)?"":agent.getValue("DeviceName");
        this.agentNameVersion = (agent.getValue("AgentNameVersion")==null)?"":agent.getValue("AgentNameVersion");
        this.layoutEngineNameVersion = (agent.getValue("LayoutEngineNameVersion")==null)?"":agent.getValue("LayoutEngineNameVersion");
    }

//    public AttributionClick(App app, String gaid, DeviceId deviceId, String ip, String userAgent, String subPid, String s1, Partner Partner, Date date) {
//
//        this.id = UUID.randomUUID().toString().replace("-", "");
//        this.gaid = gaid;
//        this.s1 = s1;
////        this.deviceId = deviceId;
////        this.deviceId = getDeviceId();
//        this.agent = userAgent;
//        this.ip = ip;
//        this.subPid = subPid;
//        this.pId = Partner.getId();
//        this.appID = app.getId();
//        this.isGross = true;
//        this.clickMessage="Passed: APPROVED";
//        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
//        this.creationDateTime = dateFormat.format(new Date(String.valueOf(date))) ;
//        this.updateDateTime = dateFormat.format(new Date(String.valueOf(date)));
//        this.creationDateTime = String.valueOf(date);
//        this.updateDateTime = String.valueOf(date);
//        this.OperatingSystemNameVersion = (agent.getValue("OperatingSystemNameVersion")==null)?"":agent.getValue("OperatingSystemNameVersion");
//        this.DeviceName = (agent.getValue("DeviceName")==null)?"":agent.getValue("DeviceName");
//        this.AgentNameVersion = (agent.getValue("AgentNameVersion")==null)?"":agent.getValue("AgentNameVersion");
//        this.LayoutEngineNameVersion = (agent.getValue("LayoutEngineNameVersion")==null)?"":agent.getValue("LayoutEngineNameVersion");
//    }

    public AttributionClick(App app, String gaid, String agents, String ip, UserAgent agent, String subPid, String s1, Partner partner, String date) {

        this.id = UUID.randomUUID().toString().replace("-", "");
        this.gaid = gaid;
        this.s1 = s1;
//        this.deviceId = deviceId;
//        this.deviceId = getDeviceId();
        this.agent = agents;
        this.ip = ip;
        this.subPid = subPid;
        this.pId = partner.getId();
        this.pName=partner.getName();
        this.appID = app.getId();
        this.appName = app.getName();
        this.isGross = true;
        this.clickMessage="Passed: APPROVED";
//        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
//        this.creationDateTime = dateFormat.format(new Date());
//        this.updateDateTime = dateFormat.format(new Date(date));
        this.creationDateTime=date;
        this.updateDateTime=date;
        this.operatingSystemNameVersion = (agent.getValue("OperatingSystemNameVersion")==null)?"":agent.getValue("OperatingSystemNameVersion");
        this.deviceName = (agent.getValue("DeviceName")==null)?"":agent.getValue("DeviceName");
        this.agentNameVersion = (agent.getValue("AgentNameVersion")==null)?"":agent.getValue("AgentNameVersion");
        this.layoutEngineNameVersion = (agent.getValue("LayoutEngineNameVersion")==null)?"":agent.getValue("LayoutEngineNameVersion");
    }


    public AttributionClick(AttributionClick click, App offerTrackingDTO, Partner publisherTrackingDTO) {
        this.appID = click.getAppID();
        this.appName = click.getAppName();
        this.id = click.getId();
        this.pClickId = click.getPClickId();
        this.pId = publisherTrackingDTO.getId();
        this.pName = publisherTrackingDTO.getName();
        this.creationDateTime = click.getCreationDateTime();

    }

    public AttributionClick(AttributionClick attributionclick, UserAgent agent) {
//        System.out.println("List<AttributionClick> " + clickAttributions.get(0).getAgent());
        System.out.println("List<AttributionClick> " + attributionclick.getAgent());

        this.id = UUID.randomUUID().toString().replace("-", "");
        this.pClickId = attributionclick.getPClickId();
        this.agent = attributionclick.getAgent();
//        this.agent = clickAttributions.get(0).getAgent();
//        this.isGross = clickAttribution.get;
        this.subPid = attributionclick.getSubPid();
        this.source = attributionclick.getSource();
        this.devOs = attributionclick.getDevOs();
        this.s1 = attributionclick.getS1();
        this.pId = attributionclick.getPId();
        this.pName = attributionclick.getPName();
        this.appID = attributionclick.getAppID();
        this.appName = attributionclick.getAppName();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        this.creationDateTime = dateFormat.format(new Date());
        this.updateDateTime = dateFormat.format(new Date());
//        this.creationDateTime = getCreationDateTime();
//        this.updateDateTime = getUpdateDateTime();
        this.ip = ip;
        this.country = country;

        this.operatingSystemNameVersion = (agent.getValue("OperatingSystemNameVersion")==null)?"":agent.getValue("OperatingSystemNameVersion");
        this.deviceName = (agent.getValue("DeviceName")==null)?"":agent.getValue("DeviceName");
        this.agentNameVersion = (agent.getValue("AgentNameVersion")==null)?"":agent.getValue("AgentNameVersion");
        this.layoutEngineNameVersion = (agent.getValue("LayoutEngineNameVersion")==null)?"":agent.getValue("LayoutEngineNameVersion");

    }

    public AttributionClick(String operatingSystemNameVersion, String deviceName, String agentNameVersion, String layoutEngineNameVersion) {

        this.operatingSystemNameVersion = operatingSystemNameVersion;
        this.deviceName = deviceName;
        this.agentNameVersion = agentNameVersion;
        this.layoutEngineNameVersion = layoutEngineNameVersion;

    }

//    public AttributionClick(AttributionClick click) {
//
//
//
//    }

//    public Click(String partnerClickId, String gaid, String idfa, String agent, String subAff, String s1, String s2, String s3, String s4, String s5, Long id, Long id1, String source, Long companyId, boolean b, String ip, CountryEnum country) {
//    }

/*public Click(ClickTrackingDTO clickTrackingDTO) {

        this.id = clickTrackingDTO.getId();
        this.partnerClickId = clickTrackingDTO.getPartnerClickId();
        this.gaid = clickTrackingDTO.getGaid();
        this.idfa = clickTrackingDTO.getIdfa();
        this.ip = clickTrackingDTO.getIp();
        this.country = clickTrackingDTO.getCountry();
        this.agent = clickTrackingDTO.getAgent();
        this.clickMessage = clickTrackingDTO.getClickMessage();
        this.isGross = clickTrackingDTO.isGross();
        this.subAff = clickTrackingDTO.getSubAff();
        this.source = clickTrackingDTO.getSource();
        this.s1 = clickTrackingDTO.getS1();
        this.s2 = clickTrackingDTO.getS2();
        this.s3 = clickTrackingDTO.getS3();
        this.s4 = clickTrackingDTO.getS4();
        this.s5 = clickTrackingDTO.getS5();
        this.partnerId = clickTrackingDTO.getPartnerId();
        this.partnerName = clickTrackingDTO.getPartnerName();
        this.companyId = clickTrackingDTO.getCompanyId();
        this.companyName = clickTrackingDTO.getCompanyName();
        this.partnerResponse = clickTrackingDTO.getPartnerResponse();
        this.appId = clickTrackingDTO.getAppId();
        this.revenue = clickTrackingDTO.getRevenue();
        this.payout = clickTrackingDTO.getPayout();
        this.creationDateTime = clickTrackingDTO.getCreationDateTime();
        this.updateDateTime = clickTrackingDTO.getUpdateDateTime();
        this.creationDateTime = clickTrackingDTO.getCreationDateTime();
        this.updateDateTime = clickTrackingDTO.getUpdateDateTime();
        this.durationInMillis = clickTrackingDTO.getDurationInMillis();
    }*/


}
