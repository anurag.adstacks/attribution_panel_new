package com.attribution.panel.bean;

import com.attribution.panel.enums.CountryEnum;
import lombok.Data;

import java.awt.*;
import java.util.Date;

@Data
public class UserEvent{

     private Long id;
    private String param1;
    private String param2;
    private String param3;
    private String param4;
    private String param5;
    private String param6;
    private String param7;
    private String param8;
    private String param9;
    private String param10;
    private String ip;
    private String os;
    private String google_aid;
    private String deviceName;
    private String country;

    private Event event;

    private Long appId;


    private Date creationDateTime;
}
