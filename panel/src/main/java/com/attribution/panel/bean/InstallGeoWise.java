package com.attribution.panel.bean;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Data
@NoArgsConstructor
@Component
public class InstallGeoWise {

//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Long id;

    private String id;

    private String countryCode;

    private int count;

//    private App app;

    private Long appId;
    private String appName;

    private Long partnerId;
    private String partnerName;

}
