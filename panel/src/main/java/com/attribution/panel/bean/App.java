package com.attribution.panel.bean;


import com.attribution.panel.enums.AppStatus;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Data
@NoArgsConstructor
public class App {

    private Long id;
    private String name;

    /*@Enumerated(EnumType.STRING)
    private OS os;*/

    private String devOS;
    private String description;

    private String appVersion;
    private String sdkVersion;

    private String appType;
    private String bundleId;  //The Apple bundle ID is a unique identifier associated with iOS apps.


    private String gameLink;

    private Date creationDateTime;

    private Date updateDateTime;

    private User user;

    private List<Partner> approvedPartner;

    private Long compId;

    private Set<Company> approvedCompany;

    private byte[] appLogo;

    private String previewUrl;

    private String trackingUrl;

    private String uuid = UUID.randomUUID().toString().replace("-", "");

    private Long allClicks = (long) 0;

    private Long grossClicks = (long) 0;

    private Long conversions = (long) 0;

    private List<Event> events;

    private List<Category> category;

    private AppStatus status; //

    public List<Country> countries;


/*    public App(AppDTO appDTO) {
        this.id = appDTO.getId();
        this.name = appDTO.getName();
        this.devOS = appDTO.getDevOS();
        this.description = appDTO.getDescription();
        this.gameLink = appDTO.getGameLink();
    }

    public App(Long appId, String appName) {
        this.id = appId;
        this.name = appName;
    }*/


    /*@JoinTable(name = "app_approved_partner", joinColumns = {
            @JoinColumn(name = "app_id")}, inverseJoinColumns = {@JoinColumn(name = "approved_partner_id")})
    @Fetch(FetchMode.SELECT)
    @ToString.Exclude
    @DiffIgnore*/


}
