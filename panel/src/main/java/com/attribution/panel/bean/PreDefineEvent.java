
package com.attribution.panel.bean;

import com.attribution.panel.enums.AccessStatus;
import com.attribution.panel.enums.ConversionPoint;
import com.attribution.panel.enums.PayoutModel;
import com.attribution.panel.enums.RevenueModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;


@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor
public class PreDefineEvent implements Serializable {

     private Long id;

    private String eventName;
    private String eventValue;
    private double revenue;
    private String eventRevenueCurrency;
    private double eventRevenueUsd;

    private String description;

    private boolean status;

    private String token; //Event token to be integrated in postback url

    private float payout;

     private boolean privateEvent;

     private RevenueModel revenueModel;

    private PayoutModel payoutModel;

    private AccessStatus accessStatus;

    //	@JsonIgnore
    private boolean multiConv;

    private int eventCount;

    private ConversionPoint convPoint;


}




/*import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.util.Date;
@Entity
@Data
public class Event {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String eventName;
    private String eventKey;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "app_id", nullable = false)
    @JsonIgnore
    @ToString.Exclude
    @EqualsAndHashCode.Include
    @NotFound(action = NotFoundAction.IGNORE)
    private App app;
}*/
