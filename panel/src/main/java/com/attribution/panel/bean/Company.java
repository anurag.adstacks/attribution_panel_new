package com.attribution.panel.bean;


import lombok.Data;

import javax.persistence.*;

@Data
public class Company {

    private Long id;

    private String fName;

    private String lName;

    private String email;

    private String imType;

    private String password;

    private Boolean status;

    private String confirmPassword;

}
