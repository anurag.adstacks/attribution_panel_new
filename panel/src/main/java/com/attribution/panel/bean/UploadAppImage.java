package com.attribution.panel.bean;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Data
@NoArgsConstructor
public class UploadAppImage {

    private Long id;

    private String name;

    private String fileName;

    private String fileType;

    private String Url;

    private App app;

    private Date creationDateTime;
}
