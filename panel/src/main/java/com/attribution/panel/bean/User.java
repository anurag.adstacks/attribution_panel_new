package com.attribution.panel.bean;


import com.attribution.panel.enums.RoleEnum;
import com.attribution.panel.enums.StateEnum;
import lombok.Data;

import java.util.Set;


@Data
public class User {

     private Long id;

    private String fName;

    private String lName;

    private String email;

     private Set<RoleEnum> roles;

    private String imType;

    private String password;

    private StateEnum state;

    private String confirmPassword;

}

