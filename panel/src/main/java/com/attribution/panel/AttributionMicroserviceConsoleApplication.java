package com.attribution.panel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AttributionMicroserviceConsoleApplication {


//	Logger logger = LogManager.getLogger();
//
//
//	@Value("${spring.jpa.properties.hibernate.jdbc.time_zone}")
//	private String timeZone;
//
//	@PostConstruct
//	public void init() {
//		TimeZone.setDefault(TimeZone.getTimeZone(timeZone));   // It will set UTC timezone
//		logger.info("Spring boot application running in Indian timezone :" + new Date());   // It will print UTC timezone
//	}


	public static void main(String[] args) {
		SpringApplication.run(AttributionMicroserviceConsoleApplication.class, args);
	}

//	@Bean
//	public RestTemplate getRestTemplate() {
//		return new RestTemplate();
//	}


}
