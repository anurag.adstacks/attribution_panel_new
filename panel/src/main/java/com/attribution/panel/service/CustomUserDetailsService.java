package com.attribution.panel.service;


import com.attribution.panel.bean.User;
import com.attribution.panel.constants.BackendConstants;
import com.attribution.panel.enums.RoleEnum;
import com.attribution.panel.enums.StateEnum;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService {

//	@Autowired
//	private RestTemplate restTemplate;

	RestTemplate restTemplate = new RestTemplate();

	@Autowired
	BackendConstants backendConstants;

	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

		Map<String, Object> user1 = restTemplate.exchange(backendConstants.baseUrlDbAccess + "/adminConsole/findByUserEmail/{value}",
				HttpMethod.GET, null, new ParameterizedTypeReference<Map<String, Object>>() {
				}, email).getBody();

		ObjectMapper mapper = new ObjectMapper();
		User user = mapper.convertValue(user1.get("user"), User.class);

		if (user == null) {
			System.out.println("User not found");
			throw new UsernameNotFoundException("Username not found");
		}
		return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(),
				 user.getState().equals(StateEnum.ACTIVE), true, true, true, getGrantedAuthorities(user));
	}

	
	private List<GrantedAuthority> getGrantedAuthorities(User user) {
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();

		for (RoleEnum userProfile : user.getRoles()) {
			authorities.add(new SimpleGrantedAuthority("ROLE_" + userProfile));
		}
		return authorities;
	}
}
