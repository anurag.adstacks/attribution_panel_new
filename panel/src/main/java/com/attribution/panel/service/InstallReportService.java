//package com.attribution.panel.service;
//
//import com.attribution.panel.bean.InstallReport;
//import com.attribution.panel.elasticsearchRepo.InstallReportRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.List;
//
//@Service
//@Transactional
//public class InstallReportService {
//    @Autowired
//    InstallReportRepository installReportRepository;
//
//    public InstallReport findByAppIdAndGaid(Long appId, String google_aid) {
//        return installReportRepository.findByAppIdAndGaid(appId,google_aid);
//    }
//
//    public void save(InstallReport installReport) {
//        installReportRepository.save(installReport);
//    }
//
//    public List<InstallReport> findByCountryAndAppId(String country, Long appId) {
//        return installReportRepository.findByCountryAndAppId(country,appId);
//    }
//
////    public int findByCountryAndAppId(CountryEnum country, Long appId) {
////        return installReportRepository.findByCountryAndAppId(String.valueOf(country), appId);
////    }
//
//
////    public void save(InstallReport installReport) {
////        installReportRepository.save(installReport);
////    }
////
////    public Long findByAppIDAndStartBetweenEndDate(Long appId, String startDate, String endDate) {
////        return installReportRepository.findByAppIDAndStartBetweenEndDate(appId, startDate, endDate);
////    }
////
////
////    public int findByCountryAndAppId(CountryEnum country, Long appId) {
////        return installReportRepository.findByCountryAndAppId(String.valueOf(country), appId);
////    }
////
////    public List<String> findByAppIdAndHourCount(Long appId, String startDate, String endDate) {
////        return installReportRepository.findByAppIdAndHourCount(appId, startDate, endDate);
////    }
////
////    public List<String> findByAppIdDateWiseCount(Long appId, String startDate, String endDate) {
////        return installReportRepository.findByAppIdAndDateWiseCount(appId, startDate, endDate);
////    }
////
////    public InstallReport findByAppIdAndGoogle_aid(Long appId, String google_aid) {
////        return installReportRepository.findByAppIdAndGoogle_aid(appId, google_aid);
////    }
////
////    public InstallReport findByAppIdAndGoogle_aidAndStatus(Long appId, String google_aid, boolean status) {
////        return installReportRepository.findByAppIdAndGoogle_aidAndStatus(appId, google_aid, status);
////    }
////
////    public Long findActiveUserByAppIdAndStartBetweenEndDate(Long appId, String startDate, String endDate) {
////    return installReportRepository.findActiveUserByAppIdAndStartBetweenEndDate(appId, startDate, endDate);
////    }
////
////    public Long findByAppId(Long appId) {
////        return installReportRepository.findByAppId(appId);
////    }
////
////    public List<String> findByAppIdAndCountryWiseCount(Long appId, String s, String s1) {
////        return installReportRepository.findByAppIdAndCountryWiseCount(appId, s, s1);
////    }
////
////    public InstallReport findByAppIdAndGoogle_aidAndDate(App app, String google_aid, String currentDate) {
////        return installReportRepository.findByAppIdAndGoogle_aidAndDate(app.getId(), google_aid, currentDate);
////    }
//
//}
