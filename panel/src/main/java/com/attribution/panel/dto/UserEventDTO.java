package com.attribution.panel.dto;

import lombok.Data;

import java.util.Date;

@Data
public class UserEventDTO {
    private Long id;
    private String param1;
    private String param2;
    private String param3;
    private String param4;
    private String param5;
    private String param6;
    private String param7;
    private String param8;
    private String param9;
    private String param10;
    private String ip;
    private String os;
    private String google_aid;
    private String deviceName;
    private String country;
    private String eventName;
    private Long appId;
    private Date creationDateTime;

    public UserEventDTO(Long id, String param1, String param2, String param3, String param4, String param5, String param6, String param7, String param8, String param9, String param10, String ip, String os, String google_aid, String deviceName, String country, String eventName, Date creationDateTime) {
        this.id = id;
        this.param1=param1;
        this.param2=param2;
        this.param3=param3;
        this.param4=param4;
        this.param5=param5;
        this.param6=param6;
        this.param7=param7;
        this.param8=param8;
        this.param9=param9;
        this.param10=param10;
        this.ip=ip;
        this.os=os;
        this.google_aid=google_aid;
        this.deviceName=deviceName;
        this.country=country;
        this.eventName=eventName;
        this.creationDateTime=creationDateTime;
    }
}
