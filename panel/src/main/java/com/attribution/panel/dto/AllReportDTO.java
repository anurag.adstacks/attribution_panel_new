package com.attribution.panel.dto;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class AllReportDTO {


    private long click;
//    private String smartLinkName;
    private long approvedConversions;
    private long cancelledConversions;
    private long pendingConversions;
    private long grossConversions;

    private double approvedRevenue;
    private double cancelledRevenue;
    private double pendingRevenue;
    private double grossRevenue;
    private int rejectedClicks;

    private double approvedPayout;

    private String appName;

 /*   private String advertiserName;*/

    private String partnerName;

//    @EqualsAndHashCode.Include
//    private long smartLinkId;

    @EqualsAndHashCode.Include
    private long appId;

    @EqualsAndHashCode.Include
    private long partnerId;


    private Date dayTimestamp;

    private double ratio;
    private double ctr;



    public AllReportDTO(AllReportDTO allReportDTO) {
        this.approvedConversions = allReportDTO.approvedConversions;
        this.cancelledConversions = allReportDTO.cancelledConversions;
        this.pendingConversions = allReportDTO.pendingConversions;
        this.approvedRevenue = allReportDTO.approvedRevenue;
        this.cancelledRevenue = allReportDTO.cancelledRevenue;
        this.pendingRevenue = allReportDTO.pendingRevenue;
        this.approvedPayout = allReportDTO.approvedPayout;
        this.click = allReportDTO.getClick();
        this.appName = allReportDTO.getAppName();
//        this.smartLinkName = null;
        this.partnerName = allReportDTO.getPartnerName();
        this.appId = allReportDTO.getAppId();
        this.partnerId = allReportDTO.getPartnerId();
//        this.smartLinkId = allReportDTO.getSmartLinkId();
        this.rejectedClicks=allReportDTO.getRejectedClicks();
        this.ratio=allReportDTO.getRatio();
        this.ctr = allReportDTO.getCtr();
        System.out.println("nanananaMitraToDurr  "+this.ratio);
    }


}
