package com.attribution.panel.dto;

import com.attribution.panel.bean.App;
import com.attribution.panel.bean.Event;
import com.attribution.panel.enums.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@NoArgsConstructor
public class EventDTO {
    private Long id;
    private String eventName;
    private String eventValue;
    private double revenue;
    private String eventRevenueCurrency;
    private double eventRevenueUsd;
    private String description;
    private boolean status;
    private String token;

    private float payout;

    private String eventKey;
    private int eventCount;
    private ConvTrackProto proto;
    private boolean privateEvent;
    private RevenueModel revenueModel;
    private PayoutModel payoutModel;
    @ToString.Exclude
    private AccessStatus accessStatus;
    private boolean multiConv;
    private ConversionPoint convPoint;

    @ToString.Exclude
    @EqualsAndHashCode.Include
    private App app;
    private String creationDateTime;
    private String updateDateTime;

    private List<PartnerDTO> partnerDTOS;

    public EventDTO(Event event) {
        this.id = event.getId();
        this.eventName = event.getEventName();
        this.description = event.getDescription();
        this.status = event.isStatus();
        this.token = event.getToken();
        this.revenue = event.getRevenue();
        this.privateEvent = event.isPrivateEvent();
        this.revenueModel = event.getRevenueModel();
        this.accessStatus = event.getAccessStatus();
        this.multiConv = event.isMultiConv();
        this.convPoint = event.getConvPoint();
        this.app = event.getApp();
        this.creationDateTime = event.getCreationDateTime();
        this.updateDateTime = event.getUpdateDateTime();
    }

    public EventDTO(Event event, int eventCount){
        this.id=event.getId();
        this.eventName=event.getEventName();
        this.eventCount=eventCount;
        this.eventKey=event.getEventKey();
    }

}



/*
import com.adstacks.attribution.bean.Event;
import lombok.Data;

@Data
public class EventDTO {
    private Long id;
    private String eventName;
    private int eventCount;
    private String eventKey;

    public EventDTO(Event event, int eventCount){
        this.id=event.getId();
        this.eventName=event.getEventName();
        this.eventCount=eventCount;
        this.eventKey=event.getEventKey();
    }
}
*/
