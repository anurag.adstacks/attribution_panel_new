package com.attribution.panel.dto;

import com.attribution.panel.bean.InstallGeoWise;
import lombok.Data;

import java.util.Locale;

@Data
public class InstallGeoWiseDTO {
    private String id;
    private String country;
    private String countryCode;
    private int count;

    public InstallGeoWiseDTO(InstallGeoWise installGeoWise){
        this.id=installGeoWise.getId();
        this.count=installGeoWise.getCount();
        this.countryCode= String.valueOf(installGeoWise.getCountryCode());
        Locale l = new Locale("", String.valueOf(installGeoWise.getCountryCode()));
        this.country=l.getDisplayCountry();
    }

    public InstallGeoWiseDTO(String countryCode, String count) {
        this.count= Integer.parseInt(count);
        this.countryCode=countryCode;
        Locale l = new Locale("", String.valueOf(countryCode));
        this.country=l.getDisplayCountry();
    }
}
