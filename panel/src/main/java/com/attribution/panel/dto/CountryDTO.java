package com.attribution.panel.dto;

import com.attribution.panel.bean.Country;
import lombok.Data;

@Data
public class CountryDTO {

    public Long id;

    public String shortName;

    public String name;

    public Integer phoneCode;

    public CountryDTO(Country country) {
        this.id = country.getId();
        this.shortName = country.getShortName();
        this.name = country.getName();
        this.phoneCode = country.getPhoneCode();
    }

}
