package com.attribution.panel.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CompanyTrackingDTO {
    private Long id;

    private String fName;

    private String lName;

    private String email;

    private String imType;

    private String password;

    private Boolean status;

    private String confirmPassword;

}
