package com.attribution.panel.dto;


import com.attribution.panel.bean.App;
import com.attribution.panel.bean.Partner;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
public class ReportFilterDTO {

//    private List<String> smartLinks;
    private Long appsId;
//    private List<String> apps;
    private List<String> partners;
    private List<String> groupBy;
    private List<String> daterange;

//    private List<SmartLink> smartLinkName=new ArrayList<>();
    private List<App> appName=new ArrayList<>();
    private List<Partner> partnerName=new ArrayList<>();
    private List<String> columns=new ArrayList<>();



}
