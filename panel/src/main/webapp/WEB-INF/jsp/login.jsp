<!DOCTYPE html>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="com.attribution.panel.constants.UIConstants" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>


<html lang="en">
<head>
    <meta charset="utf-8"/>
    <link rel="apple-touch-icon" sizes="76x76" href="asset/img/apple-icon.png">
    <link rel="icon" type="image/png" href="../asset/img/logoAttribution.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>
        User Login
    </title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport'/>
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- CSS Files -->
    <link href="../assets/css/material-dashboard.css?v=2.1.0" rel="stylesheet"/>
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="../assets/demo/demo.css" rel="stylesheet"/>
    <link rel="canonical" href="https://github.com/dbrekalo/fastselect/"/>
    <link href="../assets/css/style-sheet-adjar.css?v=2.1.0" rel="stylesheet"/>
    <!--  <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700,900&subset=latin,latin-ext' rel='stylesheet' type='text/css'> -->
    <!-- <link rel="stylesheet" href="https://rawgit.com/dbrekalo/attire/master/dist/css/build.min.css"> -->
    <script src="https://rawgit.com/dbrekalo/attire/master/dist/js/build.min.js"></script>
    <link rel="stylesheet" href="../assets/fastselect.min.css">
    <script src="../assets/fastselect.standalone.js"></script>
    <%--loader CSS Files--%>
    <link href="../assets/css/loader.css" rel="stylesheet"/>
    <%--    Tooltip textarea CSS --%>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote-lite.min.css" rel="stylesheet">
    <link href="../dist/css/select2.css" rel="stylesheet"/>
    <%--responsive css file--%>
    <link href="../assets/css/responsive-modal-pages.css" rel="stylesheet"/>
    <link href="../assets/css/summary.css" rel="stylesheet"/>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport'/>
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- CSS Files -->
    <link href="asset/css/material-dashboard.css?v=2.1.0" rel="stylesheet"/>
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <!-- <link href="assets/demo/demo.css" rel="stylesheet" /> -->
    <style>
        :root {
            --primaryButtonColor: <%=UIConstants.primaryButtonColor%>;
            --borderColor: <%=UIConstants.borderColor%>;
            --textColor: <%=UIConstants.textColor%>;
            --arrowColor: <%=UIConstants.arrowColor%>;
            --hoverButtonBgColor: <%=UIConstants.hoverButtonBgColor%>;
            --dropDownHoverColor: <%=UIConstants.dropDownHoverColor%>;
            --dropDownSelectedColor: <%=UIConstants.dropDownSelectedColor%>;
            --dropDownMenuColor: <%=UIConstants.dropDownMenuColor%>;
            --paginatehoverTextColor: <%=UIConstants.paginateHoverTextColor%>;
            --paginatehoverBgColor: <%=UIConstants.paginateHoverBgColor%>;
            --tableIconBgColor: <%=UIConstants.tableIconBgColor%>;
            --tableIconTextColor: <%=UIConstants.tableIconTextColor%>;
            --primaryBackgroundColor: <%=UIConstants.primaryBackgroundColor%>;
            --primaryCardBackground: <%=UIConstants.primaryCardBackground%>;
            --bannerTextColor: <%=UIConstants.bannerTextColor%>;
            --bannerBgColor: <%=UIConstants.bannerBgColor%>;
            --sideBgColor: <%=UIConstants.sideBgColor%>;
            --scrollbarBgColor: <%=UIConstants.scrollbarBgColor%>;
            --primaryButtonTextColor: <%=UIConstants.primaryButtonTextColor%>;
            --chartTextColor: <%=UIConstants.chartTextColor%>;
            --chartBoxBgColor: <%=UIConstants.chartBoxBgColor%>;
            --primaryBootstrapColor: <%=UIConstants.primaryBootstrapColor%>;
            --dataPickerBgColor: <%=UIConstants.dataPickerBgColor%>;
        }

        .nav-link {
            cursor: pointer;
        }


        .dropdown-menu .nav-link:hover {
            background-color: var(--primaryButtonColor);
            color: var(--primaryButtonTextColor) !important;
        }

        .swal2-popup {
            font-size: 0.7rem !important;
        }

        .navbar-toggler {
            display: none;
        }

        .modal-backdrop {
            display: none;
        }

        .form-control {
            color: <%=UIConstants.textColor%>;
            font-weight: 400;
        }

        /*close modal style*/
        .modalClose {
            position: absolute;
            top: 0;
            right: 0;
            height: 45px;
            width: 50px;
            background-color: <%=UIConstants.primaryButtonColor%>;
            line-height: 40px;
            color: var(--primaryButtonTextColor);
            font-size: 18px;
            border-radius: 0 5px 0 50px;
            padding-left: 25px !important;
            cursor: pointer;
            z-index: 5;
        }

        .bmd-label-floating {
            color: <%=UIConstants.textColor%> !important;
        }

        .bmd-form-group .form-control {
            border-bottom: 1px solid var(--textColor);
        }
    </style>
</head>

<body class="text-white"
      style="background-color:<%=UIConstants.primaryBackgroundColor%>;font-family:<%=UIConstants.primaryTextColorClass%>"
      onload="/*allPublisherList(),approvePublisherList(),getEventList(),getBlockedList(),approvePublisherListAndApprovedCampaign(),getBannerList(),getTragetingList(),getCapsList(),getCategories(),getOfferPostback()*/">

<div class="wrapper ">
    <%--    <jsp:include page="adminSidebar.jsp">
            <jsp:param value="" name="adv"/>
            <jsp:param value="active" name="offers"/>
            <jsp:param value="" name="aff"/>
            <jsp:param value="show" name="offersTab"/>
        </jsp:include>--%>
    <div class="main-panel">
        <%--<!-- Navbar -->
        <jsp:include page="adminHeader.jsp"/>
        <!-- End Navbar -->--%>
        <div class="content">

            <div class="row" style="margin-top: -20px;">

                <div class="<%--col-xl-8 offset-xl-2--%>" style="width: 40%;">

                    <div class="topHeader" style="margin-left: 1000px; margin-top: -50px;">
                        <h1>
                            <a href="#" style="color: #303654">Attribution</a>
                        </h1>
                    </div>

                    <div class="navOfferDetails"
                         style=" width: 90%; /*margin-top:14px;background-color:<%=UIConstants.primaryCardBackground%>;height:50px;*/">
                        <ul class="nav nav-pills nav-pills-<%=UIConstants.primaryColorClass%>  <%=UIConstants.primaryTextColorClass%> justify-content-center"
                            role="tablist" style="">

                            <li class="nav-item">
                                <a class="nav-link active <%=UIConstants.textColor%>" data-toggle="tab"
                                   href="#signIn"
                                   role="tablist" id="nav-link" style="font-size: 25px;">
                                    Sign In
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link <%=UIConstants.textColor%>" data-toggle="tab"
                                   href="#signUp"
                                   role="tablist" id="nav-link" style="font-size: 25px;">
                                    Sign Up
                                </a>
                            </li>

                        </ul>
                    </div>
                    <br>
                    <hr style=",mkheight: 5px;border-width:0;color:gray;background-color:#303654; margin-top: 2px;">
                    <br>
                    <div class="tab-content tab-space" style="height: 450px;">

                        <div class="tab-pane active" id="signIn">
                            <div class="row" style="width:100%;margin-left:0px"><br>
                                <div class="card <%=UIConstants.primaryTextColorClass%>"
                                     Style="font-family:<%=UIConstants.primaryFontFamily%>;background-color:<%=UIConstants.primaryCardBackground%>;margin-top:-25px;">
                                    <div class="card-header card-header-warning card-header-text">

                                    </div>
                                    <div class="card-body <%=UIConstants.primaryTextColorClass%> infoTabCard"
                                         style="margin-left:0px;">

                                        <form class="form" method="POST" action="${contextPath}/login">
                                            <div class="card card-login card-hidden">

                                                <div class="card-header card-header-<%=UIConstants.primaryColorClass%> text-center">
                                                    <p class="card-description text-center"
                                                       style="color:whitesmoke;font-weight: bold; font-size: 25px; margin-top: 10px;">
                                                        Enter credentials</p></div>
                                                <br>
                                                <br>
                                                <div class="card-body">

                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                             <span class="input-group-text">
                                                                 <i class="material-icons">email</i>
                                                             </span>
                                                        </div>
                                                        <input type="text" class="form-control" placeholder="Email"
                                                               name="email">
                                                    </div>

                                                    <br>
                                                    <br>

                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                <i class="material-icons">lock_outline</i>
                                                            </span>
                                                        </div>
                                                        <input type="password" name="password" id="loginPassword"
                                                               class="form-control"
                                                               placeholder="Password">
                                                        <i class="fa fa-eye-slash" aria-hidden="true"
                                                           style="position:absolute;margin-top:10px;margin-left:94%;cursor:pointer;z-index:5"
                                                           id="iconLoginPass"
                                                           onclick="showPassword('loginPassword','#iconLoginPass')"></i>
                                                    </div>

                                                    <br>
                                                    <br>

                                                    <div class="row" style="margin-left:230px;margin-top:-15px">
                                                        <button class="btn btn-<%=UIConstants.primaryColorClass%> btn-round"
                                                                type="submit"
                                                                style="font-size:15px;height:30px;padding:4px 20px;border:1px solid var(--borderColor)">
                                                            Submit
                                                        </button>
                                                    </div>
                                                    <%--     <span class="bmd-form-group">
                  <br>



                      	<p class="text-center text-danger font-weight-bold font-italic">${error}</p>



                  </span>
                                                    <input type="hidden" name="${_csrf.parameterName}"
                                                           value="${_csrf.token}"/>
                                                </div>
                                                <div class="row" style="margin-left:130px;margin-top:-15px">
                                                    <button class="btn btn-<%=UIConstants.primaryColorClass%> btn-round"
                                                            type="submit"
                                                            style="font-size:15px;height:30px;padding:4px 20px;border:1px solid var(--borderColor)">
                                                        Submit
                                                    </button>
                                                </div>
                                                <br>--%>

                                                </div>

                                            </div>

                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane justify-content-center" id="signUp">
                            <div class="row" style="width:100%;margin-left:0px"><br>
                                <div class="card <%=UIConstants.primaryTextColorClass%>"
                                     Style="font-family:<%=UIConstants.primaryFontFamily%>;background-color:<%=UIConstants.primaryCardBackground%>;margin-top:-25px;">
                                    <div class="card-header card-header-warning card-header-text">

                                    </div>
                                    <div class="card-body <%=UIConstants.primaryTextColorClass%> infoTabCard"
                                         style="margin-left:0px;">

                                        <form  id="saveDetails" class="form-horizontal" onsubmit="saveDetails()">
                                            <div class="card card-login card-hidden">

                                                <div class="card-header card-header-<%=UIConstants.primaryColorClass%> text-center">
                                                    <p class="card-description text-center"
                                                       style="color:whitesmoke;font-weight: bold; font-size: 25px; margin-top: 10px;">
                                                        Enter
                                                        credentials</p>
                                                </div>

                                                <br>
                                                <br>

                                                <div class="card-body">

                                                    <div class="row">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                             <span class="input-group-text">
                                                                 <i class="material-icons">person</i>
                                                             </span>
                                                            </div>
                                                            <input type="text" class="form-control" placeholder="First Name"
                                                                   name="fName">
                                                        </div>

                                                        <div class="input-group" style="margin-left: 15px;">
                                                            <div class="input-group-prepend">
                                                             <span class="input-group-text">
                                                                 <i class="material-icons">person</i>
                                                             </span>
                                                            </div>
                                                            <input type="text" class="form-control" placeholder="Last Name"
                                                                   name="lName">
                                                        </div>

                                                    </div>

                                                    <br>
                                                    <br>

                                                    <div class="input-group" style="margin-left: -15px;">
                                                        <div class="input-group-prepend">
                                                             <span class="input-group-text">
                                                                 <i class="material-icons">email</i>
                                                             </span>
                                                        </div>
                                                        <input type="email" class="form-control" placeholder="Email"
                                                               name="email">
                                                    </div>

                                                    <br>
                                                    <br>

                                                    <div class="input-group" style="margin-left: -15px;">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                <i class="material-icons">lock_outline</i>
                                                            </span>
                                                        </div>
                                                        <input type="password" name="password" id="loginPassword"
                                                               class="form-control"
                                                               placeholder="Password">
                                                        <i class="fa fa-eye-slash" aria-hidden="true"
                                                           style="position:absolute;margin-top:10px;margin-left:94%;cursor:pointer;z-index:5"
                                                           id="iconLoginPass"
                                                           onclick="showPassword('loginPassword','#iconLoginPass')"></i>
                                                    </div>

                                                    <br>
                                                    <br>

                                                    <div class="input-group" style="margin-left: -15px;">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                <i class="material-icons">lock_outline</i>
                                                            </span>
                                                        </div>
                                                        <input type="password" name="confirmPassword" id="confirmLoginPassword"
                                                               class="form-control"
                                                               placeholder="Confirm Password">
                                                        <i class="fa fa-eye-slash" aria-hidden="true"
                                                           style="position:absolute;margin-top:10px;margin-left:94%;cursor:pointer;z-index:5"
                                                           for="confirmLoginPassword"
                                                           onclick="showPassword('confirmLoginPassword','#confirmLoginPassword')"></i>
                                                    </div>

                                                    <br>
                                                    <br>

                                                    <div class="form-group" style="margin-left: 15px;">
                                                        <label for="imType" style="color: grey; width: 100%;">Primary
                                                            Role</label>
                                                        <div class="dropdown bootstrap-select show-tick show <%=UIConstants.primaryTextColorClass%>"
                                                             style="">
                                                            <select class="selectpicker"
                                                                    style=""
                                                                    data-style="btn" title="IM TYPE"
                                                                    data-size="7" name="imType"
                                                                    id="imType">
                                                                <option value="Skype">Skype</option>
                                                                <option value="Email">Email</option>
                                                                <option value="Other">Other</option>
                                                            </select>
                                                        </div>
                                                        <div class="help-block with-errors"></div>
                                                    </div>

                                                    <br>
                                                    <br>

                                                    <%--<div data-testid="terms-checkbox" class="sign-up-checkbox" style="margin-left: 15px;">
                                                        <div class="checkbox-container"><input class="auth_check_box"
                                                                                               id="terms-checkbox" name="check"
                                                                                               type="checkbox"><label for="terms-checkbox" style="color: grey; margin-left: 5px;"><span>I agree to Attribution's </span><a
                                                                href="#" target="_blank">Terms &amp; Conditions</a><span> and </span><a
                                                                href="#" target="_blank">Privacy Policy</a><span>.</span></label>
                                                        </div>
                                                    </div>--%>

                                                    <br>
                                                    <br>



                                                    <div class="row" style="margin-left:200px;margin-top:-15px">

                                                        <button class="btn btn-<%=UIConstants.primaryColorClass%> btn-round"
                                                                type="submit"
                                                                style="font-size:15px;height:30px;padding:4px 20px;border:1px solid var(--borderColor)">
                                                            Register
                                                        </button>
                                                    </div>
                                                    <%--     <span class="bmd-form-group">
                  <br>



                      	<p class="text-center text-danger font-weight-bold font-italic">${error}</p>



                  </span>
                                                    <input type="hidden" name="${_csrf.parameterName}"
                                                           value="${_csrf.token}"/>
                                                </div>
                                                <div class="row" style="margin-left:130px;margin-top:-15px">
                                                    <button class="btn btn-<%=UIConstants.primaryColorClass%> btn-round"
                                                            type="submit"
                                                            style="font-size:15px;height:30px;padding:4px 20px;border:1px solid var(--borderColor)">
                                                        Submit
                                                    </button>
                                                </div>
                                                <br>--%>

                                                </div>

                                            </div>

                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <hr style="height: 5px;border-width:0;color:gray;background-color:#303654; margin-top: 2px;">

                </div>

                <div class="" style="width: 60%;">

                    <div class="addApps-img" style="margin-left: 100px; margin-top: 20px;">
                        <figure><img src="../assets/img/register.png"
                                     style="height: 500px; width: 450px; margin-left: 100px; margin-top: 50px;" alt="sing up image">
                        </figure>
                    </div>

                </div>

            </div>

        </div>
    </div>

</div>
</div>
<!--   Core JS Files   -->
<!-- <script src="https://code.jquery.com/jquery-3.3.1.js"></script>  -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<!-- <script src="../assets/js/core/jquery.min.js"></script> -->
<script src="../assets/js/core/popper.min.js"></script>
<script src="../assets/js/core/bootstrap-material-design.min.js"></script>
<script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!-- Plugin for the momentJs  -->
<script src="../assets/js/plugins/moment.min.js"></script>
<!--  Plugin for Sweet Alert -->
<script src="../assets/js/plugins/sweetalert2.js"></script>
<!-- Forms Validations Plugin -->
<script src="../assets/js/plugins/jquery.validate.min.js"></script>
<!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
<script src="../assets/js/plugins/jquery.bootstrap-wizard.js"></script>
<!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script src="../assets/js/plugins/bootstrap-selectpicker.js"></script>
<!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
<script src="../assets/js/plugins/bootstrap-datetimepicker.min.js"></script>
<!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
<script src="../assets/js/plugins/jquery.dataTables.min.js"></script>
<!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
<script src="../assets/js/plugins/bootstrap-tagsinput.js"></script>
<!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="../assets/js/plugins/jasny-bootstrap.min.js"></script>
<!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
<script src="../assets/js/plugins/fullcalendar.min.js"></script>
<!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
<script src="../assets/js/plugins/jquery-jvectormap.js"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="../assets/js/plugins/nouislider.min.js"></script>
<!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
<!-- Library for adding dinamically elements -->
<script src="../assets/js/plugins/arrive.min.js"></script>
<!--  Google Maps Plugin    -->
<%--<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>--%>
<!-- Chartist JS -->
<script src="../assets/js/plugins/chartist.min.js"></script>
<!--  Notifications Plugin    -->
<script src="../assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="../assets/js/material-dashboard.js?v=2.1.0" type="text/javascript"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="../assets/demo/demo.js"></script>
<%--loader JS Files--%>
<script src="../assets/js/loader.js"></script>
<%--Tooltip textArea JS--%>
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote-lite.min.js"></script>
<script src="../dist/js/select2.js"></script>


<script>
    $(document).ready(function () {
        $().ready(function () {
            $sidebar = $('.sidebar');

            $sidebar_img_container = $sidebar.find('.sidebar-background');

            $full_page = $('.full-page');

            $sidebar_responsive = $('body > .navbar-collapse');

            window_width = $(window).width();

            fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

            if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
                if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
                    $('.fixed-plugin .dropdown').addClass('open');
                }

            }

            $('.fixed-plugin a').click(function (event) {
                // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
                if ($(this).hasClass('switch-trigger')) {
                    if (event.stopPropagation) {
                        event.stopPropagation();
                    } else if (window.event) {
                        window.event.cancelBubble = true;
                    }
                }
            });

            $('.fixed-plugin .active-color span').click(function () {
                $full_page_background = $('.full-page-background');

                $(this).siblings().removeClass('active');
                $(this).addClass('active');

                var new_color = $(this).data('color');

                if ($sidebar.length != 0) {
                    $sidebar.attr('data-color', new_color);
                }

                if ($full_page.length != 0) {
                    $full_page.attr('filter-color', new_color);
                }

                if ($sidebar_responsive.length != 0) {
                    $sidebar_responsive.attr('data-color', new_color);
                }
            });

            $('.fixed-plugin .background-color .badge').click(function () {
                $(this).siblings().removeClass('active');
                $(this).addClass('active');

                var new_color = $(this).data('background-color');

                if ($sidebar.length != 0) {
                    $sidebar.attr('data-background-color', new_color);
                }
            });

            $('.fixed-plugin .img-holder').click(function () {
                $full_page_background = $('.full-page-background');

                $(this).parent('li').siblings().removeClass('active');
                $(this).parent('li').addClass('active');


                var new_image = $(this).find("img").attr('src');

                if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
                    $sidebar_img_container.fadeOut('fast', function () {
                        $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
                        $sidebar_img_container.fadeIn('fast');
                    });
                }

                if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
                    var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

                    $full_page_background.fadeOut('fast', function () {
                        $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
                        $full_page_background.fadeIn('fast');
                    });
                }

                if ($('.switch-sidebar-image input:checked').length == 0) {
                    var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
                    var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

                    $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
                    $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
                }

                if ($sidebar_responsive.length != 0) {
                    $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
                }
            });

            $('.switch-sidebar-image input').change(function () {
                $full_page_background = $('.full-page-background');

                $input = $(this);

                if ($input.is(':checked')) {
                    if ($sidebar_img_container.length != 0) {
                        $sidebar_img_container.fadeIn('fast');
                        $sidebar.attr('data-image', '#');
                    }

                    if ($full_page_background.length != 0) {
                        $full_page_background.fadeIn('fast');
                        $full_page.attr('data-image', '#');
                    }

                    background_image = true;
                } else {
                    if ($sidebar_img_container.length != 0) {
                        $sidebar.removeAttr('data-image');
                        $sidebar_img_container.fadeOut('fast');
                    }

                    if ($full_page_background.length != 0) {
                        $full_page.removeAttr('data-image', '#');
                        $full_page_background.fadeOut('fast');
                    }

                    background_image = false;
                }
            });

            $('.switch-sidebar-mini input').change(function () {
                $body = $('body');

                $input = $(this);

                if (md.misc.sidebar_mini_active == true) {
                    $('body').removeClass('sidebar-mini');
                    md.misc.sidebar_mini_active = false;

                    $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

                } else {

                    $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');

                    setTimeout(function () {
                        $('body').addClass('sidebar-mini');

                        md.misc.sidebar_mini_active = true;
                    }, 300);
                }

                // we simulate the window Resize so the charts will get updated in realtime.
                var simulateWindowResize = setInterval(function () {
                    window.dispatchEvent(new Event('resize'));
                }, 180);

                // we stop the simulation of Window Resize after the animations are completed
                setTimeout(function () {
                    clearInterval(simulateWindowResize);
                }, 1000);

            });
        });
    });
</script>

<script>
    $(document).ready(function () {
        var api_url = 'https://api.linkpreview.net'
        var key = '5b578yg9yvi8sogirbvegoiufg9v9g579gviuiub8' // not real
        /*console.log("in ready");*/
        $("#formSubmit").click(function () {
            /*console.log("in change function");*/

            var flag = false;
            $.ajax({
                url: '${contextPath}/emailApi' + "?value=" + document.getElementById("exampleEmail").value,
                async: false,
                contentType: "application/json",
                dataType: 'json',
                beforeSend: function (msg) {
                    /*console.log("in ajax change")*/
                },
                success: function (result) {
                    /*console.log("inside if :" + result['response']);*/
                    if (result['response'] == true) {
                        Swal.fire({
                            title: 'Duplicate email',
                            text: "Email already exist",
                            type: 'warning',
                            confirmButtonText: 'Ok'
                        });
                        flag = true;
                    }


                }
            })
            /*console.log("after if" + flag);*/
            if (flag)
                event.preventDefault();

        });
    });
</script>

<script>
    $(document).ready(function () {
        md.checkFullPageBackgroundImage();
        setTimeout(function () {
            // after 1000 ms we add the class animated to the login/register card
            $('.card').removeClass('card-hidden');
        }, 700);
    });
</script>
<%
    String message = (String) request.getAttribute("message");
    String type = (String) request.getAttribute("type");
    if (message != null)
        if (!message.equals("")) {
            if(type==null){
                type = "success";
            }

%>
<script>
    // Initialise Sweet Alert library
    Swal.fire({
        title: '${title}',
        text: "${message}",
        type: '<%=type%>',
        confirmButtonText: 'OK'
    });


</script>

<%} %>
<script>
    $('#forgotPass').click(function () {
        event.preventDefault();
        Swal.fire({
            title: 'Enter your email address',
            input: 'text',
            inputAttributes: {
                autocapitalize: 'off'
            },
            showCancelButton: true,
            confirmButtonText: 'Submit',
            showLoaderOnConfirm: true,
            preConfirm: (login) => {
                return fetch(`/api_access/forgotPass?email=` + login)
                    .then(response => {
                        if (!response.ok) {
                            throw new Error(response.statusText)
                        }
                        return response.json()
                    })
                    .catch(error => {
                        Swal.showValidationMessage(
                            'Request failed: ${error}'
                        )
                    })
            },
            allowOutsideClick: () => !Swal.isLoading()
        }).then((result) => {
            if (result.value) {
                /*console.log(result.value['response']);*/
                validUser = result.value['validUser'];
                /*console.log("validUser: " + validUser);*/
                if (validUser == true) {
                    Swal.fire({
                        title: `Check Email`,
                        text: 'Please check your email for reset password link',
                        type: 'success'
                        /*       imageUrl: result.value.avatar_url */
                    });
                } else {
                    Swal.fire({
                        title: `Check Email`,
                        text: 'Email not found! Please retry with a registered email or Register as new user',
                        type: 'error'
                        /*       imageUrl: result.value.avatar_url */
                    });
                }

            }
        });
    });

    var error = '${msg}';
    console.log(error);
    if (error != '') {
        Swal.fire({
            title: '${title}',
            text: '${msg}',
            type: '${type}',
            confirmButtonText: 'OK'
        });
    }

    var text = '';
    $('.dropdown-menu li a').click(function () {
        text = $(this).text();
        if (text === 'Advertiser')
            document.getElementById("modalHeading").innerHTML = "Register Advertiser";
        else
            document.getElementById("modalHeading").innerHTML = "Register Publisher";
    });

    function recaptchaCallback() {

    }

    $("#regSubmit").click(function () {
        $('#regSubmit').attr('disabled');
        var flag = "";
        // password and confirm_password validation
        var password = document.getElementById("pass");
        var confirm_password = document.getElementById("confirmPass");

        if (password.value !== confirm_password.value) {
            confirm_password.setCustomValidity("Passwords Don't Match");
        } else {
            confirm_password.setCustomValidity('');
        }
        $.ajax({
            url: '/emailByUser' + "?value=" + document.getElementById("email").value,
            async: false,
            contentType: "application/json",
            dataType: 'json',
            success: function (result) {
                if (result['response'] == true) {
                    Swal.fire({
                        title: 'Duplicate email',
                        text: "Email already exist",
                        type: 'warning',
                        confirmButtonText: 'Ok'
                    });
                    flag = true;
                } else {
                    flag = false;
                }
            }

        });
        /*console.log("after if" + flag);*/
        if (flag) {
            event.preventDefault();
        }

    });

    function saveDetails() {
        alert("Inside Savedetails")
        var myform = document.getElementById("saveDetails");
        var fd = new FormData(myform);
        alert(fd + " ");
        event.preventDefault();
        $.ajax({
            url: "/registrationCompany",
            data: fd,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            beforeSend: function () {


            },
            success: function (response) {
                Swal.fire({
                    type: 'success',
                    title: 'Your application has been submitted successfully',
                    showConfirmButton: false,
                    timer: 2500
                });
                /*location.reload();*/
                setTimeout(function () {
                    window.location.reload();
                }, 2500);
            },
            complete: function () {
                alert("Done in complete")
                // Hide image container

            },

            error: function (jqXHR) {
                var o = $.parseJSON(jqXHR.responseText);
                window.open('/adminConsole/error?status=' + o.status + '&message=' + o.error, "_self")
            }
        });
    }

    /*function saveDetails() {
        alert("Inside Savedetails")
        /!*var response = grecaptcha.getResponse();
        if (response.length === 0) {
            //reCaptcha not verified
            Swal.fire({
                title: 'Captcha failed ',
                text: "please verify you are human!",
                type: 'info',
                confirmButtonText: 'Ok'
            });
            event.preventDefault();
            return false;
        }*!/

        var myform = document.getElementById("saveDetails");
        var fd = new FormData(myform);

        alert(fd + " ");
        /!*if (text === 'Advertiser')
            url = "/adminConsole/registrationAdvertiser";
        else
            url = "/adminConsole/registrationPublisher";*!/
        event.preventDefault();
        $.ajax({
            url: "/adminConsole/registrationCompany",
            data: fd,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            beforeSend: function () {


            },
            success: function (response) {
                Swal.fire({
                    type: 'success',
                    title: 'Your application has been submitted successfully',
                    showConfirmButton: false,
                    timer: 2500
                });
                /!*location.reload();*!/
                setTimeout(function () {
                    window.location.reload();
                }, 2500);
            },
            complete: function () {
                // Hide image container

            },

            error: function (jqXHR) {
                var o = $.parseJSON(jqXHR.responseText);
                window.open('/adminConsole/error?status=' + o.status + '&message=' + o.error, "_self")
            }
        });
    }*/

    function showPassword(e, icon) {
        var x = document.getElementById(e);
        if (x.type === "password") {
            x.type = "text";
            $(icon).removeClass('fa fa-eye-slash').addClass('fa fa-eye');
        } else {
            x.type = "password";
            $(icon).removeClass('fa fa-eye').addClass('fa fa-eye-slash');
        }
    }

    function limitText(limitField, limitNum) {
        if (limitField.value.length > limitNum) {
            limitField.value = limitField.value.substring(0, limitNum);
            Swal.fire({
                title: 'Limit Validation',
                text: "Value should be between 0 - " + limitNum,
                type: 'info',
                confirmButtonText: 'Ok'
            });
        }
    }
</script>

</body>
</html>