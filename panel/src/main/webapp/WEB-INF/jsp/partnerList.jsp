`<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="com.attribution.panel.constants.UIConstants,java.util.List" %>
<%@ page import="com.attribution.panel.bean.*" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <link rel="apple-touch-icon" sizes="76x76" href="../../assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="../assets/img/adjar.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>
        AM | Manage Affiliates
    </title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport'/>
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- CSS Files -->
    <link href="../../assets/css/material-dashboard.css?v=2.1.0" rel="stylesheet"/>
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="../../assets/demo/demo.css" rel="stylesheet"/>
    <link href="../assets/demo/demo.css" rel="stylesheet"/>
    <link href="../assets/css/style-sheet-adjar.css?v=2.1.0" rel="stylesheet"/>
    <%--loader CSS Files--%>
    <link href="../assets/css/loader.css" rel="stylesheet"/>
    <%--responsive css file--%>
    <link href="../assets/css/responsive-modal-pages.css" rel="stylesheet"/>
    <style>
        .cardButton {
            position: absolute;
            font-size: 12px;
            height: 28px;
            padding: 6px 15px;
            margin-top: -15px;
            margin-left: 5px;
            font-weight: bold;
        }

        .iconTable {
            background-color: green;
            border-radius: 15px;
            padding: 4px 5px;
            color: white;
            height: 28px;
            width: 27px;
            font-size: 18px
        }
        #datatables_filterSearch {
            margin-left: 100px;
            margin-top: -35px;
        }

        .macroButton {
            margin-left: 15px;
        }

        .text-wrap{
            white-space:normal;
        }
        .width-200{
            width:200px;
        }
        .dataTables_filter{
            display: block !important;
        }
    </style>
</head>

<body class="<%=UIConstants.primaryTextColorClass%>"
      style="background-color:<%=UIConstants.primaryBackgroundColor%>;font-family:<%=UIConstants.primaryTextColorClass%>">
<div class="se-pre-con"></div>
<% String url = request.getContextPath(); %>
<div class="wrapper ">
    <jsp:include page="adminSidebar.jsp">
        <jsp:param value="" name="adv"/>
        <jsp:param value="" name="offers"/>
        <jsp:param value="active" name="allEvent"/>
    </jsp:include>
    <div class="main-panel">
        <!-- Navbar -->
        <jsp:include page="adminHeader.jsp"/>

        <!-- End Navbar -->
        <div class="content">
            <div class="container-fluid">
                <div class="pull-right" style="margin-top:12px;"><b class="textLine" style="margin-left:-165px">|</b>
                </div>
                <div class="cardDiv">
                    <div class="cardSize">
                        <i class="material-icons iconSize">assignment</i>
                    </div>
                    <h2 class="cardHeading">Partners</h2>
                </div>
                <div class="card <%=UIConstants.primaryTextColorClass%>"
                     style="background-color:<%=UIConstants.primaryCardBackground%>;font-family:<%=UIConstants.primaryFontFamily%>">
                    <%--<div class="card-header card-header-<%=UIConstants.primaryColorClass%> card-header-icon responsiveButton">
                        <!-- Button trigger modal -->
                        <button class="btn btn-<%=UIConstants.primaryColorClass%> filterButton pull-right" data-toggle="modal"
                                style="cursor: pointer;margin-top:-38px;-moz-transform: scale(0.94);"
                                data-target="#exampleModal" id="newAdv">
                            <i class="fa fa-plus" aria-hidden="true" data-toggle="modal" data-target="#exampleModal"
                               style="margin-left:0px;margin-top:-15px;width:auto;font-size:15px"><b
                                    style="margin-left:6px">Add Publisher&nbsp;</b></i>
                        </button>
                        <div class="row">
                            <div class="col-sm-12" style="margin-left:-158px">
                                <button
                                        class="btn btn-<%=UIConstants.primaryColorClass%> filterButton  pull-right"
                                        data-target="#exampleModalAd" data-toggle="modal" style="margin-top:-38px;-moz-transform: scale(0.94);">
                                    <i class="fa fa-search" aria-hidden="true"
                                       style="width:auto;font-size:15px;margin-left:0px"><b style="margin-left:6px">Search&nbsp;</b></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <!-- Modal -->
                    <div class="modal fade" id="exampleModalAd" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalLongTitle" aria-hidden="true" data-backdrop="static">
                        <div class="modal-dialog" role="document" id="exampleModalAd1">
                            <div class="card modal-content <%=UIConstants.primaryTextColorClass%> publisherContent"
                                 style="background-color: <%=UIConstants.primaryModalCardHeaderBackground%>; width: 450px">
                                <div class="modalClose">
                                    <i class="fa fa-times" data-dismiss="modal" aria-label="Close"></i>
                                </div>
                                <div class="card-header card-header-<%=UIConstants.primaryColorClass%> card-header-icon"
                                     style="height:70px">
                                    <div class="card-icon">
                                        <i class="material-icons">dashboard</i>
                                    </div>
                                    <h4 class="card-title <%=UIConstants.primaryTextColorClass%>"
                                        style="font-family: <%=UIConstants.primaryFontFamily%>; font-weight: bold"
                                        id="">Search Report</h4>
                                    <p class="card-category">Enter Information</p>
                                </div>
                                <div class="card-body publisherCard"
                                     style="font-family: <%=UIConstants.primaryFontFamily%>; width: 450px; background-color: <%=UIConstants.primaryCardModalBackground%>">
                                    <br>
                                    <div id="modalForm">
                                        <div class="row pubSearchFilter" style="margin-left:15px">
                                            <div class="togglebutton">
                                                <label class="<%=UIConstants.primaryTextColorClass%>"
                                                       style="font-family: <%=UIConstants.primaryFontFamily%>;margin-left:34px;font-weight:bold ">Account
                                                    Status:
                                                    <input type="checkbox"
                                                           id="toggleSearch"<%=true ? "checked" : "" %>/>
                                                    Disabled
                                                    <span class="toggle"></span>
                                                    Enabled
                                                </label>
                                            </div>
                                        </div>
                                        <br>
                                        <br>
                                        <div class="row publisherBottomLine"
                                             style="border-top:1px solid #423b3a;width:455px;margin-left:-20px;margin-top: -10px">
                                            <br>
                                            <div class="col-sm-12">
                                                <button type="button" id="searchRecord"
                                                        class="btn btn-<%=UIConstants.primaryColorClass%> btn-round pull-right submitButton"
                                                        data-dismiss="modal" style="margin-right:30px;margin-top:10px">
                                                    Apply
                                                </button>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        &lt;%&ndash;</form>&ndash;%&gt;
                                        <!-- Image loader -->
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    &lt;%&ndash;Modal 2&ndash;%&gt;
                    <div class="modal fade" id="exampleModal" tabindex="-1"
                         role="dialog" aria-hidden="true" data-backdrop="static">
                        <div class="modal-dialog " role="document">
                            <!-- 									<div class="modal-content" style="background-color: <%=UIConstants.primaryCardBackground%>;">
 -->
                            <div class="card modal-content <%=UIConstants.primaryTextColorClass%> contentPublisher"
                                 style="background-color:<%=UIConstants.primaryModalCardHeaderBackground%>;width:600px">
                                <div class="modalClose">
                                    <i class="fa fa-times" data-dismiss="modal" aria-label="Close"></i>
                                </div>
                                <div class="card-header card-header-<%=UIConstants.primaryColorClass%> card-header-icon"
                                     style="height:70px">
                                    <div class="card-icon">
                                        <i class="material-icons">dashboard</i>
                                    </div>
                                    <h4 class="card-title <%=UIConstants.primaryTextColorClass%>"
                                        style="font-family: <%=UIConstants.primaryFontFamily%>; font-weight: bold"
                                        id="modalHeading">Create Publisher -
                                    </h4>
                                    <p class="card-category">Complete your profile</p>

                                </div>

                                <div class="card-body cardPublisher"
                                     style="font-family: <%=UIConstants.primaryFontFamily%>; font-weight: bold;background-color:<%=UIConstants.primaryCardModalBackground%>;width:600px">
                                    <p>${message}</p>

                                    <div id="mymodel" style="margin-top:-30px">

                                        <form id="savePublisher" class="form-horizontal"
                                              onsubmit="saveGeneralDetails()">

                                            <div class="form-group">
                                                <input type="text" id="pubId" name="pubId" value=""
                                                       style="display:none">
                                                <input type="text" id="userId" name="userId" value=""
                                                       style="display:none">
                                            </div>
                                            <div class="row companyFieldSize">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="bmd-label-floating <%=UIConstants.primaryTextColorClass%>">Company* </label>
                                                        <input type="text" oninvalid="this.setCustomValidity('Please Enter Company Name')" oninput="setCustomValidity('')"
                                                               class="form-control <%=UIConstants.primaryTextColorClass%>"
                                                               required="true" name="company" id="company" onchange="limitText(this,255)" onkeypress="limitText(this,255)">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 jobTitle">
                                                    <div class="form-group">
                                                        <label class="bmd-label-floating <%=UIConstants.primaryTextColorClass%>">Job
                                                            Title</label>
                                                        <input type="text"
                                                               class="form-control <%=UIConstants.primaryTextColorClass%>"
                                                               name="jobTitle" id="jobTitle" onchange="limitText(this,255)" onkeypress="limitText(this,255)">
                                                    </div>
                                                </div>
                                                &lt;%&ndash;<div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="bmd-label-floating <%=UIConstants.primaryTextColorClass%>">Number of Employees</label>
                                                        <input type="number" class="form-control <%=UIConstants.primaryTextColorClass%>" name="numEmp" id="numEmp" >
                                                    </div>
                                                </div>&ndash;%&gt;
                                            </div>
                                            <div class="row emailFieldSize">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label class="bmd-label-floating <%=UIConstants.primaryTextColorClass%>">Email
                                                            address*</label>
                                                        <input type="email"
                                                               class="form-control <%=UIConstants.primaryTextColorClass%>" oninvalid="this.setCustomValidity('Please Enter Valid Email')" oninput="setCustomValidity('')"
                                                               required="true" name="email" id="email" onchange="limitText(this,255)" onkeypress="limitText(this,255)" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row passwordFieldSize" id="passwordField">
                                                <div class="col-sm-6 passwordFieldSize1">
                                                    <div class="form-group">
                                                        <label class="bmd-label-floating <%=UIConstants.primaryTextColorClass%>">Password*</label>
                                                        <input type="password" oninvalid="this.setCustomValidity('Please Enter Password')" oninput="setCustomValidity('')"
                                                               class="form-control <%=UIConstants.primaryTextColorClass%>"
                                                               required="true" name="pass" id="pass" onchange="limitText(this,255)" onkeypress="limitText(this,255)"
                                                               autocomplete="new-password">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 confirmPasswordField">
                                                    <div class="form-group">
                                                        <label class="bmd-label-floating <%=UIConstants.primaryTextColorClass%>">Confirm
                                                            Password*</label>
                                                        <input type="password"
                                                               class="form-control <%=UIConstants.primaryTextColorClass%>"
                                                               required="true" name="confirmPass" id="confirmPass" onchange="limitText(this,255)" onkeypress="limitText(this,255)">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row nameFieldSize">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="bmd-label-floating <%=UIConstants.primaryTextColorClass%>">First
                                                            Name*</label>
                                                        <input type="text" oninvalid="this.setCustomValidity('Please Enter First Name')" oninput="setCustomValidity('')"
                                                               class="form-control <%=UIConstants.primaryTextColorClass%>"
                                                               required="true" name="fname" id="fname" onchange="limitText(this,255)" onkeypress="limitText(this,255)">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 lastName">
                                                    <div class="form-group">
                                                        <label class="bmd-label-floating <%=UIConstants.primaryTextColorClass%>">Last
                                                            Name</label>
                                                        <input type="text"
                                                               class="form-control <%=UIConstants.primaryTextColorClass%>"
                                                               name="lname" id="lname" onchange="limitText(this,255)" onkeypress="limitText(this,255)">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-top:70px">
                                                <label class="bmd-label-floating <%=UIConstants.primaryTextColorClass%>"
                                                       style="margin-top:-55px;margin-left:30px;font-size:13px">Postback
                                                    URL</label>
                                                <div class="col-sm-12 macroButtonHeight">
                                                    <textarea class="textArea" id="pb" name="pb" onchange="limitText(this,1000)" onkeypress="limitText(this,1000)"></textarea><br>
                                                    <label class="<%=UIConstants.primaryTextColorClass%>"
                                                           style="margin-left:25px">Most Used URL tokens</label><br>
                                                    <a class="macroButton" onclick="trck('{PUB_CLICK_ID}')">{PUB_CLICK_ID}</a>
                                                    <a class="macroButton" onclick="trck('{AFFID}')">{AFFID}</a>
                                                    <a class="macroButton" onclick="trck('{SUB_AFFID}')">{SUB_AFFID}</a>
                                                    <a class="macroButton publisherDevice" onclick="trck('{DEVICE_ID}')">{DEVICE_ID}</a>
                                                    <a class="macroButton publisherS2Button" onclick="trck('{s2}')">{s2}</a>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="togglebutton">
                                                <label class="<%=UIConstants.primaryTextColorClass%>"
                                                       style="font-family: <%=UIConstants.primaryFontFamily%>; ">Account
                                                    Status:
                                                    <input type="checkbox" name="status" id="status">
                                                    Disabled
                                                    <span class="toggle"></span>
                                                    Enabled
                                                </label>

                                            </div>
                                            <br>
                                            <div class="card-collapse"
                                                 style="font-family: serif;background-color: <%=UIConstants.primaryCardModalBackground%> "
                                                 id="advancedOption">
                                                <div class="card-header <%=UIConstants.primaryTextColorClass%>"
                                                     role="tab" id="headingTwo"
                                                     style="font-family: serif;background-color:<%=UIConstants.primaryCardModalBackground%>;width:140px;margin-left:0px;margin-top:-30px;;border-color:<%=UIConstants.primaryCardModalBackground%>">
                                                    <h4 class="<%=UIConstants.primaryTextColorClass%>">
                                                        <a class="collapsed <%=UIConstants.primaryTextColorClass%>"
                                                           data-toggle="collapse" href="#collapseTwo"
                                                           aria-expanded="false" aria-controls="collapseTwo"
                                                           style="font-family: serif;font-weight: bold">
                                                            <i class="fa fa-sort-desc" aria-hidden="true"></i>Advanced
                                                            Option
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseTwo" class="collapse" role="tabpanel"
                                                     aria-labelledby="headingTwo" data-parent="#accordion">
                                                    <div class="card-body <%=UIConstants.primaryTextColorClass%>" style="margin-top:-20px">
                                                        <div class="row addressRow">
                                                            <div class="col-sm-12">
                                                                <div class="form-group">
                                                                    <label class="bmd-label-floating <%=UIConstants.primaryTextColorClass%>">Address1</label>
                                                                    <input type="text"
                                                                           class="form-control <%=UIConstants.primaryTextColorClass%>"
                                                                           name="addr1" id="addr1" onchange="limitText(this,255)" onkeypress="limitText(this,255)">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row addressRow">
                                                            <div class="col-sm-12">
                                                                <div class="form-group">
                                                                    <label class="bmd-label-floating <%=UIConstants.primaryTextColorClass%>">Address2</label>
                                                                    <input type="text"
                                                                           class="form-control <%=UIConstants.primaryTextColorClass%>"
                                                                           name="addr2" id="addr2" onchange="limitText(this,255)" onkeypress="limitText(this,255)">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row mobileIMIdNumber">
                                                            <div class="col-sm-4 numberInput">
                                                                <div class="form-group">
                                                                    <label class="bmd-label-floating <%=UIConstants.primaryTextColorClass%>">Mobile
                                                                        Number</label>
                                                                    <input type="text" oninvalid="this.setCustomValidity('Please Enter Valid Mobile Number')" oninput="setCustomValidity('')"
                                                                           class="form-control <%=UIConstants.primaryTextColorClass%>" onchange="limitText(this,255)" onkeypress="limitText(this,255)"
                                                                           name="phone" id="phone" pattern="^\+{0,2}([\-\. ])?(\(?\d{0,3}\))?([\-\. ])?\(?\d{0,3}\)?([\-\. ])?\d{3}([\-\. ])?\d{4}">
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4 <%=UIConstants.primaryTextColorClass%> imType"
                                                                 style="margin-top:5px;margin-left:-20px">
                                                                <div class="dropdown bootstrap-select show-tick show <%=UIConstants.primaryTextColorClass%>">
                                                                <select class="selectpicker"
                                                                        data-style="btn"
                                                                        title="IM TYPE" data-size="7" tabindex="-98"
                                                                        name="imType" id="imType" data-width="200px">
                                                                    <option value="1"> ICQ</option>
                                                                    <option value="2">Skype</option>
                                                                    <option value="3">Yahoo Messenger</option>
                                                                </select></div>
                                                            </div>


                                                            <div class="col-sm-4 imIdMargin" style="margin-left:20px">
                                                                <div class="form-group">
                                                                    <label class="bmd-label-floating <%=UIConstants.primaryTextColorClass%>">IM
                                                                        ID</label>
                                                                    <input type="text" onchange="limitText(this,255)" onkeypress="limitText(this,255)"
                                                                           class="form-control <%=UIConstants.primaryTextColorClass%>"
                                                                           name="imId" id="imId">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row publisherCity">
                                                            <div class="col-sm-4">
                                                                <div class="form-group">
                                                                    <label class="bmd-label-floating <%=UIConstants.primaryTextColorClass%>">City</label>
                                                                    <input type="text" onchange="limitText(this,255)" onkeypress="limitText(this,255)"
                                                                           class="form-control <%=UIConstants.primaryTextColorClass%>"
                                                                           name="city" id="city">
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4 CountryPublisher">
                                                                <div class="form-group">
                                                                    <label class="bmd-label-floating <%=UIConstants.primaryTextColorClass%>">Country</label>
                                                                    <input type="text" onchange="limitText(this,255)" onkeypress="limitText(this,255)"
                                                                           class="form-control <%=UIConstants.primaryTextColorClass%>"
                                                                           name="country" id="country">
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4 statePublisher">
                                                                <div class="form-group">
                                                                    <label class="bmd-label-floating <%=UIConstants.primaryTextColorClass%>">State/Region</label>
                                                                    <input type="text" onchange="limitText(this,255)" onkeypress="limitText(this,255)"
                                                                           class="form-control <%=UIConstants.primaryTextColorClass%>"
                                                                           name="state" id="state">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row postalCodeFax">
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label class="bmd-label-floating <%=UIConstants.primaryTextColorClass%>">Postal
                                                                        Code</label>
                                                                    <input type="number" onchange="limitText(this,255)" onkeypress="limitText(this,255)"
                                                                           class="form-control <%=UIConstants.primaryTextColorClass%>"
                                                                           name="zip" id="zip">
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6 faxPublisher">
                                                                <div class="form-group">
                                                                    <label class="bmd-label-floating <%=UIConstants.primaryTextColorClass%>">Fax</label>
                                                                    <input type="text" onchange="limitText(this,255)" onkeypress="limitText(this,255)"
                                                                           class="form-control <%=UIConstants.primaryTextColorClass%>"
                                                                           name="fax" id="fax">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row CutFactorOffset">
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label class="bmd-label-floating <%=UIConstants.primaryTextColorClass%>">Cut
                                                                        Factor</label>
                                                                    <input type="number"
                                                                           class="form-control <%=UIConstants.primaryTextColorClass%>"
                                                                           name="cutFactor" id="cutFactor" step="any">
                                                                </div>
                                                            </div>

                                                            <div class="col-sm-6 offsetPublisher">
                                                                <div class="form-group">
                                                                    <label class="bmd-label-floating <%=UIConstants.primaryTextColorClass%>">Offset</label>
                                                                    <input type="number"
                                                                           class="form-control <%=UIConstants.primaryTextColorClass%>"
                                                                           name="offset" step="any" id="offset">
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>

                                            <div class="row bottomLinePublisher"
                                                 style="border-top:1px solid #423b3a;width:600px;margin-left:-20px;margin-top: -10px">
                                                <br>
                                                <div class="col-sm-12">
                                                    <button type="submit"
                                                            class="btn btn-<%=UIConstants.primaryColorClass%> btn-round pull-right submitButton"
                                                            id="pubSubmit" style="margin-right:10px;margin-top:10px">
                                                        Submit
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                        <div style="margin-top:-30px">
                                            <i style="margin-left:10px">Note:Please fill * columns</i>
                                        </div>
                                    </div>
                                    <!-- Image loader -->
                                    <div class="mx-auto text-center" id="loader" style='display: none;'>
                                        <!--              <h6 class="<%=UIConstants.primaryTextColorClass%>">Loading... </h6>
 --> <img src='../assets/loaders/colorfulSpiralSpinner.gif' width='150px' height='150px'>
                                    </div>
                                    <!-- Image loader -->
                                </div>
                            </div>

                            <!-- 									</div>
-->
                        </div>
                    </div>

                    &lt;%&ndash;Activity&ndash;%&gt;
                    <div class="modal fade" tabindex="-1" role="dialog"
                          aria-labelledby="myLargeModalLabel" aria-hidden="true" data-backdrop="static"
                          style="margin-top:-80px;" id="activityModal">
                        <div class="modal-dialog modal-lg">
                            <div class="card modal-content <%=UIConstants.primaryTextColorClass%>  col-md-14"
                                 style="background-color: <%=UIConstants.primaryCardModalBackground%>; width: 1000px;">
                                <div class="modalClose">
                                    <i class="fa fa-times" data-dismiss="modal" aria-label="Close"></i>
                                </div>
                                <div class="card-header card-header-<%=UIConstants.primaryColorClass%> card-header-icon">
                                    <div class="card-icon">
                                        <i class="material-icons">dashboard</i>
                                    </div>
                                    <h3 class="card-title <%=UIConstants.primaryTextColorClass%>"
                                        style="font-family: <%=UIConstants.primaryFontFamily%>; font-weight: bold"
                                    >Activity Logs</h3>
                                    &lt;%&ndash;                                                                <p class="card-category">Enter Information</p>&ndash;%&gt;
                                </div>
                                <div class="card-body"
                                     style="font-family: <%=UIConstants.primaryFilterFontFamilyTable%>; font-weight: bold;background-color: <%=UIConstants.primaryCardModalBackground%>">
                                    <br>                                                                <div class="toolbar">
                                    <!--        Here you can write extra buttons/actions for the toolbar   -->
                                </div>
                                    <div class="container">
                                        <div class="material-datatables">
                                            <table id="datatablesActivity"
                                                   class="table table-no-bordered <%=UIConstants.primaryTextColorClass%>"
                                                   cellspacing="0" width="100%"
                                                   style="font-family: <%=UIConstants.primaryFontFamily%>;">
                                                <thead>
                                                <tr>
                                                    <th>
                                                        ID
                                                    </th>
                                                    <th>
                                                        Creation Time
                                                    </th>
                                                    <th >
                                                        Operation
                                                    </th>
                                                    <th>
                                                        Type
                                                    </th>
                                                    <th>
                                                        Updated Fields
                                                    </th>
                                                    <th>
                                                        User
                                                    </th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                    &lt;%&ndash;                                                            </div>&ndash;%&gt;
                                    <!-- end content-->
                                    &lt;%&ndash;                                                        </div>&ndash;%&gt;
                                    <!--  end card  -->
                                    &lt;%&ndash;                                                    </div>&ndash;%&gt;
                                    <!-- end col-md-12 -->
                                </div>
                            </div>
                        </div>
                    </div>
                    &lt;%&ndash;&ndash;%&gt;--%>


                    <div class="card-body" style="background-color:<%=UIConstants.primaryCardBackground%>">
                        <div class="material-datatables">
                            <table id="datatables"
                                   class="dataTable display nowrap <%=UIConstants.primaryTextColorClass%>"
                                   cellspacing="0" style="width:100%">
                                <thead class=" <%=UIConstants.primaryTextColorClass%>">
                                <tr>
                                    <th style="width:50px">
                                        ID
                                    </th>
                                    <th>
                                        Company
                                    </th>
                                    <th style="width:200px">
                                       Partner Name
                                    </th>
                                    <th>
                                        Email
                                    </th>
                                    <th>
                                        Status
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <% List<Partner> param = (List<Partner>) request.getAttribute("partner");
                                    int i = 1;
                                    for (Partner pub : param) {%>

                                <tr class="<%=UIConstants.primaryTextColorClass%>"
                                    style="background-color:<%=UIConstants.primaryCardBackground%>">
                                    <td>
                                        <div style="margin-left:60px"></div>
                                        <%=  pub.getId()%>
                                    </td>
                                    <td>
                                        <div style="margin-left:100px"></div>
                                        <a href="/adminConsole/getPublisherDetails/<%=  pub.getId()%> " target = "_blank" style="color:<%=UIConstants.primaryHyperLinkColor%>;font-weight:bold"><%= pub.getCompany()%></a>
                                    </td>
                                    <td>
                                        <div style="margin-left:160px"></div>
                                        <%=pub.getName()%> <%= pub.getName()%>
                                    </td>
                                    <td>
                                        <div style="margin-left:80px"></div>
                                        <%=  pub.getEmail()%>
                                    </td>
                                    <td>
                                        <div style="display:none"><%= pub.getStatus() %>
                                        </div>
                                        <form method="POST" action="/adminConsole/savepub">

                                            <div class="togglebutton">
                                                <label> <input type="checkbox" name="status"
                                                               id="toggle<%=i-1%>" <%=pub.getStatus() ? "checked" : "" %>
                                                               name="toggle1"
                                                               onclick="checkForm(<%=pub.getId() %>,<%=i-1%>)"/>
                                                    <span class="toggle btn-<%=UIConstants.primaryColorClass%>"></span>

                                                </label>
                                            </div>
                                        </form>
                                    </td>
                                </tr>
                                <%
                                        i++;
                                    }
                                %>

                                </tbody>
                            </table>
                        </div>

                    </div>


                </div>


            </div>
        </div>

        <!--  Core JS Files   -->
        <script src="../../assets/js/core/jquery.min.js"></script>
        <script src="../../assets/js/core/popper.min.js"></script>
        <script src="../../assets/js/core/bootstrap-material-design.min.js"></script>
        <script src="../../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
        <!-- Plugin for the momentJs  -->
        <script src="../../assets/js/plugins/moment.min.js"></script>
        <!--  Plugin for Sweet Alert -->
        <script src="../../assets/js/plugins/sweetalert2.js"></script>
        <!-- Forms Validations Plugin -->
        <script src="../../assets/js/plugins/jquery.validate.min.js"></script>
        <!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
        <script src="../../assets/js/plugins/jquery.bootstrap-wizard.js"></script>
        <!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
        <script src="../../assets/js/plugins/bootstrap-selectpicker.js"></script>
        <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
        <script src="../../assets/js/plugins/bootstrap-datetimepicker.min.js"></script>
        <!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
        <script src="../assets/js/plugins/jquery.dataTables.min.js"></script>
        <!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
        <script src="../../assets/js/plugins/bootstrap-tagsinput.js"></script>
        <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
        <script src="../../assets/js/plugins/jasny-bootstrap.min.js"></script>
        <!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
        <script src="../../assets/js/plugins/fullcalendar.min.js"></script>
        <!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
        <script src="../../assets/js/plugins/jquery-jvectormap.js"></script>
        <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
        <script src="../../assets/js/plugins/nouislider.min.js"></script>
        <!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
        <!-- Library for adding dinamically elements -->
        <script src="../../assets/js/plugins/arrive.min.js"></script>
        <!-- Chartist JS -->
        <script src="../../assets/js/plugins/chartist.min.js"></script>
        <!--  Notifications Plugin    -->
        <script src="../../assets/js/plugins/bootstrap-notify.js"></script>
        <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
        <script src="../../assets/js/material-dashboard.js?v=2.1.0" type="text/javascript"></script>
        <!-- Material Dashboard DEMO methods, don't include it in your project! -->
        <script src="../../assets/demo/demo.js"></script>
        <%--loader JS Files--%>
        <script src="../assets/js/loader.js"></script>
        <script>
            $(document).ready(function () {
                $().ready(function () {
                    $sidebar = $('.sidebar');

                    $sidebar_img_container = $sidebar.find('.sidebar-background');

                    $full_page = $('.full-page');

                    $sidebar_responsive = $('body > .navbar-collapse');

                    window_width = $(window).width();

                    fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

                    if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
                        if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
                            $('.fixed-plugin .dropdown').addClass('open');
                        }

                    }

                    $('.fixed-plugin a').click(function (event) {
                        // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
                        if ($(this).hasClass('switch-trigger')) {
                            if (event.stopPropagation) {
                                event.stopPropagation();
                            } else if (window.event) {
                                window.event.cancelBubble = true;
                            }
                        }
                    });

                    $('.fixed-plugin .active-color span').click(function () {
                        $full_page_background = $('.full-page-background');

                        $(this).siblings().removeClass('active');
                        $(this).addClass('active');

                        var new_color = $(this).data('color');

                        if ($sidebar.length != 0) {
                            $sidebar.attr('data-color', new_color);
                        }

                        if ($full_page.length != 0) {
                            $full_page.attr('filter-color', new_color);
                        }

                        if ($sidebar_responsive.length != 0) {
                            $sidebar_responsive.attr('data-color', new_color);
                        }
                    });

                    $('.fixed-plugin .background-color .badge').click(function () {
                        $(this).siblings().removeClass('active');
                        $(this).addClass('active');

                        var new_color = $(this).data('background-color');

                        if ($sidebar.length != 0) {
                            $sidebar.attr('data-background-color', new_color);
                        }
                    });

                    $('.fixed-plugin .img-holder').click(function () {
                        $full_page_background = $('.full-page-background');

                        $(this).parent('li').siblings().removeClass('active');
                        $(this).parent('li').addClass('active');


                        var new_image = $(this).find("img").attr('src');

                        if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
                            $sidebar_img_container.fadeOut('fast', function () {
                                $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
                                $sidebar_img_container.fadeIn('fast');
                            });
                        }

                        if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
                            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

                            $full_page_background.fadeOut('fast', function () {
                                $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
                                $full_page_background.fadeIn('fast');
                            });
                        }

                        if ($('.switch-sidebar-image input:checked').length == 0) {
                            var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
                            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

                            $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
                            $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
                        }

                        if ($sidebar_responsive.length != 0) {
                            $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
                        }
                    });

                    $('.switch-sidebar-image input').change(function () {
                        $full_page_background = $('.full-page-background');

                        $input = $(this);

                        if ($input.is(':checked')) {
                            if ($sidebar_img_container.length != 0) {
                                $sidebar_img_container.fadeIn('fast');
                                $sidebar.attr('data-image', '#');
                            }

                            if ($full_page_background.length != 0) {
                                $full_page_background.fadeIn('fast');
                                $full_page.attr('data-image', '#');
                            }

                            background_image = true;
                        } else {
                            if ($sidebar_img_container.length != 0) {
                                $sidebar.removeAttr('data-image');
                                $sidebar_img_container.fadeOut('fast');
                            }

                            if ($full_page_background.length != 0) {
                                $full_page.removeAttr('data-image', '#');
                                $full_page_background.fadeOut('fast');
                            }

                            background_image = false;
                        }
                    });

                    $('.switch-sidebar-mini input').change(function () {
                        $body = $('body');

                        $input = $(this);

                        if (md.misc.sidebar_mini_active == true) {
                            $('body').removeClass('sidebar-mini');
                            md.misc.sidebar_mini_active = false;

                            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

                        } else {

                            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');

                            setTimeout(function () {
                                $('body').addClass('sidebar-mini');

                                md.misc.sidebar_mini_active = true;
                            }, 300);
                        }

                        // we simulate the window Resize so the charts will get updated in realtime.
                        var simulateWindowResize = setInterval(function () {
                            window.dispatchEvent(new Event('resize'));
                        }, 180);

                        // we stop the simulation of Window Resize after the animations are completed
                        setTimeout(function () {
                            clearInterval(simulateWindowResize);
                        }, 1000);

                    });
                });
            });
        </script>
        <script>
            $(document).ready(function () {
                $('#datatables').DataTable({
                    "pagingType": "full_numbers",

                    "lengthMenu": [
                        [10, 25, 50, -1],
                        [10, 25, 50, "All"]
                    ],
                    "columnDefs": [
                        {"Searching": false, "orderable": false, "targets": 4},
                        {"Searching": false, "orderable": false, "targets": 5},
                        {"Searching": false, "orderable": false, "targets": 6},
                        {"orderable": false, "targets": 1},
                        {"Searching": false, "orderable": false, "targets": 2},
                        {"Searching": false, "orderable": false, "targets": 3},
                        {"Searching": false, "targets": 0}

                    ],
                    "order": [[0, "desc"]],
                    language: {
                        search: "_INPUT_",
                        searchPlaceholder: "Search records",
                    }
                });
                jQuery('.dataTable').wrap('<div class="dataTables_scroll" />');
                var table = $('#datatables').DataTable();
                /*search record click a button*/
                var test = '';
                $('#datatables_filterSearch input').unbind();
                $('#datatables_filterSearch input').keyup(function (e) {
                    test = $(this).val();
                });
                $('#searchRecord').click(function () {
                    toggleStatusBool = document.getElementById("toggleSearch").checked;
                    table.column(4).search(toggleStatusBool).draw();
                    table.search(test).draw();
                });
                // Edit record
                table.on('click', '.edit', function () {
                    $tr = $(this).closest('tr');
                    var data = table.row($tr).data();
                    alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
                });

                // Delete a record
                table.on('click', '.remove', function (e) {
                    $tr = $(this).closest('tr');
                    table.row($tr).remove().draw();
                    e.preventDefault();
                });

                //Like record
                table.on('click', '.like', function () {
                    alert('You clicked on Like button');
                });
            });
        </script>
        <script>
            var toggleStatusBool;
            var toggleId;
            var fetchId;

            function toggleSwitchAjax(toggleStatus) {

                Swal.fire({
                    title: 'Are you sure?',
                    text: "You want to " + toggleStatus + " integration!",
                    type: 'info',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes!'
                }).then((result) => {
                    if (result.value) {
                        Swal.fire({
                            title: 'Successfully ' + toggleStatus + 'd',
                            type: 'success',
                            confirmButtonText: 'OK!'

                        });
                        $.ajax({
                            url: "/adminConsole/statusEnablePub/" + fetchId + "/" + toggleStatusBool,
                            success: function (result) {

                            }
                        });
                    } else {
                        document.getElementById("toggle" + toggleId).checked = !toggleStatusBool;
                    }

                });
            }

            function checkForm(e, z) {
                fetchId = e;
                toggleId = z;
                toggleStatusBool = document.getElementById("toggle" + toggleId).checked;
                if (toggleStatusBool) {
                    toggleSwitchAjax("enable")
                } else {
                    toggleSwitchAjax("disable")
                }


            }
        </script>
        <script>
            var invokedSection = "";
            var data = "";

            function myFun(e) {
                document.getElementById("modalHeading").innerHTML = "Edit Publisher";
                /* document.getElementByid("email").disabled=true; */
                invokedSection = "update";
                $.ajax({
                    url: "/adminConsole/getPublisher/" + e,
                    type: "GET",
                    dataType: 'json',
                    beforeSend: function () {
                        $("#loader").show();
                        $("#mymodel").hide();
                    },
                    success: function (response) {
                        data = response.user.id;
                        document.getElementById("userId").value = response.user.id;
                        document.getElementById("pubId").value = response.publisher.id;
                        document.getElementById("company").value = response.publisher.company;
                        document.getElementById("jobTitle").value = response.publisher.jobTitle;
                        /*document.getElementById("numEmp").value = response.publisher.numEmp;*/
                        document.getElementById("email").value = response.user.email;
                        document.getElementById("pass").value = response.user.confirmPassword;
                        document.getElementById("confirmPass").value = response.user.confirmPassword;
                        $("#passwordField").hide();
                        document.getElementById("fname").value = response.publisher.fname;
                        document.getElementById("lname").value = response.publisher.lname;
                        document.getElementById("addr1").value = response.publisher.addr1;
                        document.getElementById("addr2").value = response.publisher.addr2;
                        document.getElementById("phone").value = response.publisher.phone;
                        document.getElementById("imId").value = response.publisher.imId;
                        document.getElementById("city").value = response.publisher.city;
                        document.getElementById("country").value = response.publisher.country;
                        document.getElementById("state").value = response.publisher.state;
                        document.getElementById("zip").value = response.publisher.zip;
                        document.getElementById("fax").value = response.publisher.fax;
                        document.getElementById("pb").value = response.publisher.pb;
                        document.getElementById("cutFactor").value = response.publisher.cutFactor;
                        document.getElementById("offset").value = response.publisher.offset;
                        /*  document.getElementById("imType").value = response.publisher.imType;  */
                        var flag = response.publisher.status;
                        if (flag)
                            document.getElementById("status").checked = true;
                        else
                            document.getElementById("status").checked = false;
                        $(":input").change();
                    },
                    complete: function () {
                        // Hide image container
                        $("#loader").hide();
                        $("#mymodel").show();

                    },
                    error: function (jqXHR) {
                        var o = $.parseJSON(jqXHR.responseText);
                        window.open('/adminConsole/error?status=' + o.status + '&message=' + o.error, "_self")
                    }
                });
            }

        </script>

        <script>
            function deleteNotification() {

                Swal.fire({
                    title: 'Please contact support!!',
                    type: 'info',
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'OK!'
                }).then((result) => {
                    if (result.value) {
                        Swal.fire({
                            title: 'Successfully ' + toggleStatus + 'd',
                            type: 'success',
                            confirmButtonText: 'OK!'

                        });

                    }
                });
            }
        </script>
        <script>
            var invokedSection = "";
            $("#newAdv").on("click", function () {
                //code here
                invokedSection = "create";
                $('#savePublisher')[0].reset();
                $(":input").change();
                $("#passwordField").show();
                // Hide image container
                $("#loader").hide();
                $("#mymodel").show();
            });
            $("#pubSubmit").click(function () {
                // password and confirm_password validation
                var password = document.getElementById("pass");
                var confirm_password = document.getElementById("confirmPass");

                if (password.value !== confirm_password.value) {
                    confirm_password.setCustomValidity("Passwords Don't Match");
                } else {
                    confirm_password.setCustomValidity('');
                }
                if (invokedSection == "create") {
                    $.ajax({
                        url: '/emailByUser' + "?value=" + document.getElementById("email").value,
                        async: false,
                        contentType: "application/json",
                        dataType: 'json',
                        success: function (result) {
                            if (result['response'] == true) {
                                Swal.fire({
                                    title: 'Duplicate email',
                                    text: "Email already exist",
                                    type: 'info',
                                    confirmButtonText: 'Ok'
                                });
                                flag = true;
                            } else {
                                flag = false;
                            }
                        }
                    })
                    if (flag) {
                        event.preventDefault();
                    }
                }

            });


            function saveGeneralDetails() {
                var myform = document.getElementById("savePublisher");
                var fd = new FormData(myform);
                event.preventDefault();
                $.ajax({
                    url: "/adminConsole/savepub",
                    data: fd,
                    cache: false,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    beforeSend: function () {


                    },
                    success: function (response) {
                        showNotification('top', 'left');
                        /*location.reload();*/
                        setTimeout(function () {
                            window.location.reload();
                        }, 1000);
                    },
                    complete: function () {
                        // Hide image container

                    },

                    error: function (jqXHR) {
                    var o = $.parseJSON(jqXHR.responseText);
                    window.open('/adminConsole/error?status='+o.status+'&message='+o.error,"_self")
                    }
                });
            }

            function showNotification(from, align) {

                $.notify({
                    icon: "add_alert",
                    message: "Your application has been submitted successfully"


                }, {
                    type: 'success',
                    timer: 3000,
                    placement: {
                        from: from,
                        align: align
                    }
                });

            }
        </script>
        <script type="text/javascript">
            var value = '';

            function trck(text) {
                $("#pb").insertAtCaret(text);
            }

            /* Needs JQuery */
            $(document).ready(function () {

                jQuery.fn.extend({
                    insertAtCaret: function (myValue) {
                        return this.each(function (i) {
                            if (document.selection) {
                                //For browsers like Internet Explorer
                                this.focus();
                                sel = document.selection.createRange();
                                sel.text = myValue;
                                this.focus();
                            } else if (this.selectionStart || this.selectionStart == '0') {
                                //For browsers like Firefox and Webkit based
                                var startPos = this.selectionStart;
                                var endPos = this.selectionEnd;
                                var scrollTop = this.scrollTop;
                                this.value = this.value.substring(0, startPos) + myValue + this.value.substring(endPos, this.value.length);
                                this.focus();
                                this.selectionStart = startPos + myValue.length;
                                this.selectionEnd = startPos + myValue.length;
                                this.scrollTop = scrollTop;
                            } else {
                                this.value += myValue;
                                this.focus();
                            }
                        })
                    }
                });
            });

            function limitText(limitField, limitNum) {
                if (limitField.value.length > limitNum) {
                    limitField.value = limitField.value.substring(0, limitNum);
                    Swal.fire({
                        title: 'Limit Validation',
                        text: "Value should be between 0 - "+limitNum,
                        type: 'info',
                        confirmButtonText: 'Ok'
                    });
                }
            }
        </script>
</body>

</html>