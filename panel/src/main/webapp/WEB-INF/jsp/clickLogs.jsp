<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page
        import="com.adstacks.attribution.bean.*,com.adstacks.attribution.constants.UIConstants,com.adstacks.attribution.enums.CountryEnum,com.adstacks.attribution.enums.OS" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Set" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<html lang="en">

<head>
    <meta charset="utf-8"/>
    <link rel="icon" type="image/png" href="../assets/img/adjar.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>Report</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport'/>
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- CSS Files -->
    <link href="../assets/css/material-dashboard.css?v=2.1.0" rel="stylesheet"/>
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="../assets/demo/demo.css" rel="stylesheet"/>
    <!-- <link rel="stylesheet" href="../docs/css/bootstrap-3.3.2.min.css" type="text/css"> -->
    <!-- <script type="text/javascript" src="../docs/js/jquery-2.1.3.min.js"></script> -->
    <script src="../assets/js/core/jquery.min.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
    <link href="../assets/css/style-sheet-adjar.css?v=2.1.0" rel="stylesheet"/>
    <script src="https://rawgit.com/dbrekalo/attire/master/dist/js/build.min.js"></script>
    <link rel="stylesheet" href="../assets/fastselect.min.css">
    <script src="../assets/fastselect.standalone.js"></script>
    <link href="../dist/css/select2.css" rel="stylesheet"/>
    <script src="../dist/js/select2.js"></script>
    <%--loader CSS Files--%>
    <link href="../assets/css/loader.css" rel="stylesheet"/>
   <link rel="stylesheet" type="text/css" href="../assets/css/daterangepicker.css"/>
    <%--responsive css file--%>
    <link href="../assets/css/responsive-modal-pages.css" rel="stylesheet"/>

    <style>
        .dataTables_length::after {
            top: -2px;
            right: 65px;
        }
        .swal2-input{
            background-color:<%=UIConstants.sweetAlertBgColor%> !important;
            color:<%=UIConstants.textColor%> !important;
            font-size:15px !important;
            margin-top:-5px !important;
        }
        .swal2-confirm, .swal2-cancel{
            height:30px !important;
            padding:5px 10px !important;
            margin-top:-15px !important;
        }
        .swal2-modal{
            background-color:<%=UIConstants.sweetAlertBgColor%> !important;
            color:<%=UIConstants.textColor%> !important;
            margin-top:100px !important;
            margin-left:40% !important;
        }
        .swal2-title{
            color:<%=UIConstants.textColor%> !important;
            margin-top:-10px !important;
        }
        .swal2-success-circular-line-right,
        .swal2-success-circular-line-left, .swal2-success-fix{
            background-color:<%=UIConstants.sweetAlertBgColor%> !important;
        }

        .dataTables_wrapper .dataTables_paginate {
            margin-top:5px !important;
        }
    </style>
</head>
<%
    String cp = request.getContextPath();
%>

<body class="" style="background-color: <%=UIConstants.primaryBackgroundColor%>;">
<div class="se-pre-con"></div>
<jsp:include page="adminSidebar.jsp">
    <jsp:param value="active" name="clickLogs"/>
    <jsp:param value="show" name="54esz"/>
</jsp:include>
<div class="main-panel">
    <!-- Navbar -->
    <jsp:include page="adminHeader.jsp"/>
    <!-- End Navbar -->
    <div class="content">
        <div class="container-fluid">
            <div class="cardDiv">
                <div class="cardSize">
                    <i class="material-icons iconSize">assignment</i>
                </div>
                <h2 class="cardHeading">Click Logs</h2>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card <%=UIConstants.primaryTextColorClass%>"
                         style="background-color: <%=UIConstants.primaryCardBackground%>;">
                        <div class="card-header card-header-warning card-header-icon clickHeader">
                            <div class="row pull-right clickReportrange" style="margin-top:11px;margin-left:-8px">
                                <div class="col-sm-8" style="">
                                    <!-- <i class="fa fa-caret-down"></i> -->
                                    <div class="text-dark reportrange" id="reportrange"
                                         style="margin-top: -53px;">
                                        <input readonly type="text"
                                               class="<%=UIConstants.primaryTextColorClass%> reportrangeinput"
                                               id="reportrangeinput"
                                               name="daterange"
                                               style="cursor: pointer; background-color: <%=UIConstants.primaryBackgroundColor%>; border:1px solid <%=UIConstants.borderColor%>; width:295px;padding:2px 6px">
                                    </div>
                                </div>
                                </div>
                                <div class="row pull-right" style="margin-top:-2px;margin-right:-65px">
                                    <div class="col-sm-7" style="">
                                        <button class="filterButton btn btn-<%=UIConstants.primaryColorClass%> btn-round pull-right" id="exportData" style="cursor: pointer;margin-top:-39px;height:28px;border-radius:5px;border:1.8px solid #fff">
                                            <i class="fa fa-download" aria-hidden="true" style="margin-left:0;margin-top:-15px;width:auto;font-size:16px"><b style="margin-left:5px;">Export</b>&nbsp;</i>
                                        <%--<button id="exportFilter" class="filterButton pull-right clickExportFilter" data-toggle="modal"
                                                style="cursor: pointer;margin-top:-39px;height:28px;border-radius:5px;border:1.8px solid #fff;margin-right:0"
                                                data-target="#exportModal">
                                            <i class="fa fa-search" aria-hidden="true" data-toggle="modal"
                                               style="margin-left:0px;margin-top:-5px;width:auto;font-size:16px"><b
                                                    style="margin-left:6px">Export&nbsp;</b></i>
                                        </button>--%>
                                    </div>

                                </div>
                            <div class="row pull-right clickOfferFilter" style="margin-top:-2px;margin-right:-10px">
                                <div class="col-sm-7" style="">

                                    <div class="col-sm-7" style="">

                                        <button id="offerFilter" class="btn btn-<%=UIConstants.primaryColorClass%> filterButton pull-right" data-toggle="modal"
                                                id="modalAddRule"
                                                style="cursor: pointer;margin-top:-39px;height:28px;border-radius:5px;border:1.8px solid #fff"
                                                data-target="#exampleModalAd">
                                            <i class="fa fa-search" aria-hidden="true" data-toggle="modal"
                                               data-target="#addRule"
                                               id="reset"
                                               style="margin-left:0px;margin-top:-16px;width:auto;font-size:16px"><b
                                                    style="margin-left:6px">Search&nbsp;</b></i>
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="card-body">
                            <div class="toolbar <%=UIConstants.primaryTextColorClass%>"
                                 style="font-family: <%=UIConstants.primaryFontFamily%>;">
                                <!--        Here you can write extra buttons/actions for the toolbar              -->

                                <!-- Modal -->
                                <div class="modal fade" id="exampleModalAd" tabindex="-1" role="dialog"
                                     aria-labelledby="exampleModalLongTitle" aria-hidden="true" data-backdrop="static">
                                    <div class="modal-dialog" role="document" id="exampleModalAd1">
                                        <div class="card modal-content clickContent <%=UIConstants.primaryTextColorClass%>  col-md-14"
                                             style="background-color: <%=UIConstants.primaryModalCardHeaderBackground%>; width: 520px;margin-left:25%">
                                            <div class="modalClose">
                                                <i class="fa fa-times" data-dismiss="modal" aria-label="Close"></i>
                                            </div>
                                            <div class="card-header card-header-<%=UIConstants.primaryColorClass%> card-header-icon" style="height:80px">
                                                <div class="card-icon">
                                                    <i class="material-icons">dashboard</i>
                                                </div>
                                                <h4 class="card-title <%=UIConstants.primaryTextColorClass%>"
                                                    style="font-family: <%=UIConstants.primaryFontFamily%>; font-weight: bold"
                                                    id="modalHeading">Search Report</h4>
                                                <p class="card-category">Enter Information</p>
                                            </div>
                                            <div class="card-body clickCard"
                                                 style="font-family: <%=UIConstants.primaryFilterFontFamilyTable%>; font-weight: bold; width: 520px; background-color: <%=UIConstants.primaryCardModalBackground%>">
                                                <br>
                                                <div id="modalForm">
                                                    <form id="formSubmit">

                                                        <div class="row">
                                                            <div class="col-sm-6" style="margin-top: 4px">
                                                                <label class="<%=UIConstants.primaryTextColorClass%>"
                                                                       style="font-family: <%=UIConstants.primaryFontFamily%>; font-weight: bold; margin-top:-5px; position: absolute">Select
                                                                    Offers </label> <i class="material-icons"
                                                                                       style="margin-top: -5px; margin-left: 115px; position: absolute;">playlist_add</i>&nbsp;

                                                            </div>
                                                            <div class="col-sm-6 clickSelectBox"
                                                                 style="margin-left:-105px;margin-top:-10px">
                                                                <select multiple id="offerListAjax"
                                                                        style="width:328px"></select>
                                                            </div>
                                                        </div>
                                                        <div class="row" style="margin-top:10px">
                                                            <div class="col-sm-6" style="margin-top: 5px">
                                                                <label class="<%=UIConstants.primaryTextColorClass%>"
                                                                       style="font-family: <%=UIConstants.primaryFontFamily%>; font-weight: bold; margin-left: 0px; position: absolute">Select
                                                                    Publishers </label> <i class="material-icons"
                                                                                           style="margin-top: -5px; margin-left: 115px; position: absolute;">person</i>&nbsp;
                                                            </div>
                                                            <div class="col-sm-6 clickSelectBox"
                                                                 style="margin-left:-105px;margin-top:-10px">
                                                                <select multiple id="publisherListAjax"
                                                                        style="width:328px"></select>
                                                            </div>
                                                        </div>
                                                        <div class="row" style="margin-top:10px">
                                                            <div class="col-sm-6" style="margin-top: 4px">
                                                                <label class="<%=UIConstants.primaryTextColorClass%>"
                                                                       style="font-family: <%=UIConstants.primaryFontFamily%>; font-weight: bold; margin-top: -1px; position: absolute">Select
                                                                    Advertisers </label> <i class="material-icons"
                                                                                            style="margin-top: -5px; margin-left: 115px; position: absolute;">person</i>&nbsp;
                                                            </div>
                                                            <div class="col-sm-6 clickSelectBox"
                                                                 style="margin-left:-105px;margin-top:-10px">
                                                                <select multiple id="advertiserListAjax"
                                                                        style="width:328px"></select>
                                                            </div>
                                                        </div>
                                                        <div class="row" style="margin-top:10px">
                                                            <div class="col-sm-5" style="margin-top:5px">
                                                                <label class="<%=UIConstants.primaryTextColorClass%>"
                                                                       style="font-family: <%=UIConstants.primaryFontFamily%>; font-weight: bold; margin-top: -3px; position: absolute">Search
                                                                    Click Id
                                                                </label> <i class="material-icons"
                                                                            style="margin-top: -5px; margin-left: 115px; position: absolute;">format_indent_increase</i>&nbsp;
                                                            </div>
                                                            <div class="col-sm-9"
                                                                 style="margin-left:175px;margin-top:-37px">
                                                                <input type="text" class="inputText" value=""
                                                                       id="searchUuid"
                                                                       style="color:<%=UIConstants.textColor%>;margin-left:-20px;width:320px">

                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-6" style="margin-top:35px">
                                                                <label class="<%=UIConstants.primaryTextColorClass%>"
                                                                       style="font-family: <%=UIConstants.primaryFontFamily%>; font-weight: bold; margin-top:-2px;position:absolute">Select
                                                                    Columns </label> <i class="fa fa-table"
                                                                                        style="margin-left: 120px"></i>
                                                            </div>
                                                            <div class="col-sm-6 dropdown bootstrap-select show-tick <%=UIConstants.primaryTextColorClass%> clickSelectBox"
                                                                 style="margin-top: 25px; margin-left: -105px">
                                                                <select class="multipleSelect"
                                                                        data-style="btn btn-warning"
                                                                        multiple="" title="Select Column" data-size="7"
                                                                        tabindex="-98" id="extSelect"
                                                                        data-width="325px">
                                                                    <!-- selected="selected" -->
                                                                    <option value="Offer Id" selected>Offer Id</option>
                                                                    <option value="Offer Name" selected>Offer Name</option>
                                                                    <option value="Click Id" selected>Click Id</option>
                                                                    <option value="Pub Click Id" selected>Pub Click Id</option>
                                                                    <option value="Publisher Id" selected>Publisher Id</option>
                                                                    <option value="Publisher Name" selected>Publisher Name</option>
                                                                    <option value="IP Address" selected>IP Address</option>
                                                                    <option value="Device Name">Device Name</option>
                                                                    <option value="Creation Date Time" selected>Creation Date Time
                                                                    </option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="row" style="border-top:1px solid #423b3a;width:520px;margin-left:-20px;margin-top:0">
                                                            <div class="col-sm-12">
                                                        <button type="button" id="submitForm"
                                                                class="btn btn-<%=UIConstants.primaryColorClass%> btn-round pull-right submitButton"
                                                                data-dismiss="modal" style="margin-top:10px;margin-right:20px">Apply
                                                        </button></div></div>
                                                        <div class="clearfix"></div>
                                                    </form>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <!--Export Modal -->
                                <div class="modal fade" id="exportModal" tabindex="-1" role="dialog"
                                     aria-labelledby="exampleModalLongTitle" aria-hidden="true" data-backdrop="static">
                                    <div class="modal-dialog" role="document" id="exportModal1">
                                        <div class="card modal-content clickContent <%=UIConstants.primaryTextColorClass%>  col-md-14"
                                             style="background-color: <%=UIConstants.primaryModalCardHeaderBackground%>; width: 520px;margin-left:25%">
                                            <div class="modalClose">
                                                <i class="fa fa-times" data-dismiss="modal" aria-label="Close"></i>
                                            </div>
                                            <div class="card-header card-header-<%=UIConstants.primaryColorClass%> card-header-icon" style="height:80% !important;">
                                                <div class="card-icon">
                                                    <i class="material-icons">dashboard</i>
                                                </div>
                                                <h4 class="card-title <%=UIConstants.primaryTextColorClass%>"
                                                    style="font-family: <%=UIConstants.primaryFontFamily%>; font-weight: bold"
                                                    id="exportModalHeading">Search Report</h4>
                                                <p class="card-category">Enter Information</p>
                                            </div>
                                            <div class="card-body clickCard"
                                                 style="font-family: <%=UIConstants.primaryFontFamilyTable%>; font-weight: bold; width: 520px; background-color: <%=UIConstants.primaryCardModalBackground%>;margin-top:10px">
                                                <br>
                                                <div id="exportModalForm">
                                                    <form id="exportFormSubmit">

                                                        <div class="row" style="margin-top:-10px">
                                                            <div class="col-sm-6" style="margin-top: 4px">
                                                                <label class="<%=UIConstants.primaryTextColorClass%>"
                                                                       style="font-family: <%=UIConstants.primaryFontFamily%>; font-weight: bold; margin-top:-5px; position: absolute">Select
                                                                    Offers </label> <i class="material-icons"
                                                                                       style="margin-top: -5px; margin-left: 115px; position: absolute;">playlist_add</i>&nbsp;

                                                            </div>
                                                            <div class="col-sm-6 clickSelectBox"
                                                                 style="margin-left:-105px;margin-top:-10px">
                                                                <select multiple id="exportOfferListAjax"
                                                                        style="width:315px" name="offerId"></select>
                                                            </div>
                                                        </div>
                                                        <div class="row" style="margin-top:10px">
                                                            <div class="col-sm-6" style="margin-top: 5px">
                                                                <label class="<%=UIConstants.primaryTextColorClass%>"
                                                                       style="font-family: <%=UIConstants.primaryFontFamily%>; font-weight: bold; margin-left: 0px; position: absolute">Select
                                                                    Publishers </label> <i class="material-icons"
                                                                                           style="margin-top: -5px; margin-left: 115px; position: absolute;">person</i>&nbsp;
                                                            </div>
                                                            <div class="col-sm-6 clickSelectBox"
                                                                 style="margin-left:-105px;margin-top:-10px">
                                                                <select multiple id="exportPublisherListAjax"
                                                                        style="width:315px" name="pubId"></select>
                                                            </div>
                                                        </div>
                                                        <div class="row" style="margin-top:10px">
                                                            <div class="col-sm-6" style="margin-top: 4px">
                                                                <label class="<%=UIConstants.primaryTextColorClass%>"
                                                                       style="font-family: <%=UIConstants.primaryFontFamily%>; font-weight: bold; margin-top: -5px; position: absolute">Select
                                                                    Date </label> <i class="fa fa-calendar" aria-hidden="true" style="margin-top:-2px; margin-left: 118px; position: absolute;"></i>
                                                            </div>
                                                            <div class="col-sm-6 reportrange"
                                                                 style="margin-left:-105px;margin-top:-7px">
                                                                <input readonly type="text"
                                                                       class="<%=UIConstants.primaryTextColorClass%> reportrangeinput"
                                                                       id="reportRangeInputExport"
                                                                       name="daterange"
                                                                       style="color: #fff; cursor: pointer; background-color: <%=UIConstants.primaryBackgroundColor%>; border:1px solid #ff9800; width:315px;padding:2px 14px;font-size:16px;border-radius:10px">
                                                            </div>
                                                        </div>

                                                        <br>
                                                        <div class="row" style="border-top:1px solid #423b3a;width:520px;margin-left:-20px;">
                                                            <div class="col-sm-12">
                                                        <button type="button" id="exportSubmitForm"
                                                                class="btn btn-<%=UIConstants.primaryColorClass%> btn-round pull-right submitButton"
                                                                data-dismiss="modal" onclick="submitExportForm()" style="margin-top:10px;margin-right:20px">Search
                                                        </button></div></div>
                                                        <div class="clearfix"></div>
                                                    </form>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <!-- </form> -->
                            </div>

                        </div>

                        <div id="tableBody" style="height:0;visibility:hidden"></div>
                        <div class="table-responsive" style="width:98%;margin-left:10px;margin-top:-20px">


                            <table id="datatables"
                                   class="display nowrap border-bottom row-border <%=UIConstants.primaryTextColorClass%>"
                                   style="font-family: <%=UIConstants.primaryFontFamily%>;" cellspacing="0" width="100%">
                                <thead>
                                <tr class="<%=UIConstants.primaryTextColorClass%>" style="font-weight: bold; ">
                                    <th style="font-weight:bold">Offer Id</th>
                                    <th style="font-weight:bold">Offer Name</th>
                                    <th style="font-weight:bold">Click Id</th>
                                    <th style="font-weight:bold">Publisher Click Id</th>
                                    <th style="font-weight:bold">Publisher Id</th>
                                    <th style="font-weight:bold">Publisher Name</th>
                                    <th style="font-weight:bold">IP Address</th>
                                    <th style="font-weight:bold">Device Name</th>
                                    <th style="font-weight:bold">Creation Date Time</th>
                                </tr>
                                </thead>

                            </table>
                        </div>
                    </div>
                </div>
                <!-- end content-->
            </div>
            <!--  end card  -->
        </div>
        <!-- end col-md-12 -->
    </div>
    <!-- end row -->
</div>

<jsp:include page="adminFooter.jsp"/>
<!--   Core JS Files   -->
<!-- multiselect -->

<script src="../assets/js/core/popper.min.js"></script>
<script src="../assets/js/core/bootstrap-material-design.min.js"></script>
<script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!-- Plugin for the momentJs  -->
<script src="../assets/js/plugins/moment.min.js"></script>
<!--  Plugin for Sweet Alert -->
<script src="../assets/js/plugins/sweetalert2.js"></script>
<!-- Forms Validations Plugin -->
<script src="../assets/js/plugins/jquery.validate.min.js"></script>
<!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
<script src="../assets/js/plugins/jquery.bootstrap-wizard.js"></script>
<!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script src="../assets/js/plugins/bootstrap-selectpicker.js"></script>
<!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
<script src="../assets/js/plugins/bootstrap-datetimepicker.min.js"></script>
<!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
<script src="../assets/js/plugins/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../assets/js/jquery.spring-friendly.js"></script>
<!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
<script src="../assets/js/plugins/bootstrap-tagsinput.js"></script>
<!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="../assets/js/plugins/jasny-bootstrap.min.js"></script>
<!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
<script src="../assets/js/plugins/fullcalendar.min.js"></script>
<!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
<script src="../assets/js/plugins/jquery-jvectormap.js"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="../assets/js/plugins/nouislider.min.js"></script>
<!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
<!-- Library for adding elements -->
<script src="../assets/js/plugins/arrive.min.js"></script>
<script src="../assets/js/plugins/chartist.min.js"></script>
<!--  Notifications Plugin    -->
<script src="../assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="../assets/js/material-dashboard.js?v=2.1.0" type="text/javascript"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="../assets/demo/demo.js"></script>
<!-- <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script> -->
<!-- <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script> -->
<!--   <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script> -->
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<script type="text/javascript">
</script>
<%--loader JS Files--%>
<script src="../assets/js/loader.js"></script>
<script>
    $(document).ready(function () {
        $().ready(function () {
            $sidebar = $('.sidebar');

            $sidebar_img_container = $sidebar.find('.sidebar-background');

            $full_page = $('.full-page');

            $sidebar_responsive = $('body > .navbar-collapse');

            window_width = $(window).width();

            fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

            if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
                if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
                    $('.fixed-plugin .dropdown').addClass('open');
                }

            }

            $('.fixed-plugin a').click(function (event) {
                // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
                if ($(this).hasClass('switch-trigger')) {
                    if (event.stopPropagation) {
                        event.stopPropagation();
                    } else if (window.event) {
                        window.event.cancelBubble = true;
                    }
                }
            });

            $('.fixed-plugin .active-color span').click(function () {
                $full_page_background = $('.full-page-background');

                $(this).siblings().removeClass('active');
                $(this).addClass('active');

                var new_color = $(this).data('color');

                if ($sidebar.length != 0) {
                    $sidebar.attr('data-color', new_color);
                }

                if ($full_page.length != 0) {
                    $full_page.attr('filter-color', new_color);
                }

                if ($sidebar_responsive.length != 0) {
                    $sidebar_responsive.attr('data-color', new_color);
                }
            });

            $('.fixed-plugin .background-color .badge').click(function () {
                $(this).siblings().removeClass('active');
                $(this).addClass('active');

                var new_color = $(this).data('background-color');

                if ($sidebar.length != 0) {
                    $sidebar.attr('data-background-color', new_color);
                }
            });

            $('.fixed-plugin .img-holder').click(function () {
                $full_page_background = $('.full-page-background');

                $(this).parent('li').siblings().removeClass('active');
                $(this).parent('li').addClass('active');


                var new_image = $(this).find("img").attr('src');

                if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
                    $sidebar_img_container.fadeOut('fast', function () {
                        $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
                        $sidebar_img_container.fadeIn('fast');
                    });
                }

                if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
                    var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

                    $full_page_background.fadeOut('fast', function () {
                        $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
                        $full_page_background.fadeIn('fast');
                    });
                }

                if ($('.switch-sidebar-image input:checked').length == 0) {
                    var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
                    var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

                    $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
                    $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
                }

                if ($sidebar_responsive.length != 0) {
                    $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
                }
            });

            $('.switch-sidebar-image input').change(function () {
                $full_page_background = $('.full-page-background');

                $input = $(this);

                if ($input.is(':checked')) {
                    if ($sidebar_img_container.length != 0) {
                        $sidebar_img_container.fadeIn('fast');
                        $sidebar.attr('data-image', '#');
                    }

                    if ($full_page_background.length != 0) {
                        $full_page_background.fadeIn('fast');
                        $full_page.attr('data-image', '#');
                    }

                    background_image = true;
                } else {
                    if ($sidebar_img_container.length != 0) {
                        $sidebar.removeAttr('data-image');
                        $sidebar_img_container.fadeOut('fast');
                    }

                    if ($full_page_background.length != 0) {
                        $full_page.removeAttr('data-image', '#');
                        $full_page_background.fadeOut('fast');
                    }

                    background_image = false;
                }
            });

            $('.switch-sidebar-mini input').change(function () {
                $body = $('body');

                $input = $(this);

                if (md.misc.sidebar_mini_active == true) {
                    $('body').removeClass('sidebar-mini');
                    md.misc.sidebar_mini_active = false;

                    $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

                } else {

                    $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');

                    setTimeout(function () {
                        $('body').addClass('sidebar-mini');

                        md.misc.sidebar_mini_active = true;
                    }, 300);
                }

                // we simulate the window Resize so the charts will get updated in realtime.
                var simulateWindowResize = setInterval(function () {
                    window.dispatchEvent(new Event('resize'));
                }, 180);

                // we stop the simulation of Window Resize after the animations are completed
                setTimeout(function () {
                    clearInterval(simulateWindowResize);
                }, 1000);

            });
        });
    });
</script>
<script>
    var dateRange = '';
    var filter = '';
    var filter1 = '';
    var offerId = 0;
    var table = $('#datatables').DataTable({
        ajax: "<%=BackendConstants.clickReport%>",
        serverSide: true,
        processing: true,
        scrollX: true,
        columns: [
            {
                data: 'offerId',
                render: function (data) {
                    offerId = data;
                    return ' <div style="margin-left:80px"></div>' + data;
                }
            },
            {
                data: 'offerName',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + '<a href="/adminConsole/viewoffer?id=' + offerId + '" target = "_blank" style="color:<%=UIConstants.primaryHyperLinkColor%>;font-weight:bold;">' + data + ' </a>';
                }
            },
            {
                data: 'uuid',
                render: function (data) {
                    return ' <div style="margin-left:100px"></div>' + '<a href="/adminConsole/getClickLogsDetails/' + data + '" target = "_blank" style="color:<%=UIConstants.primaryHyperLinkColor%>;font-weight:bold;">' + data + ' </a>';
                }
            },
            {
                data: 'pubClickId',
                render: function (data) {
                    return ' <div style="margin-left:80px"></div>' + data;
                }
            },
            {
                data: 'pubId',
                render: function (data) {
                    offerId = data;
                    return ' <div style="margin-left:80px"></div>' + data;
                }
            },
            {
                data: 'publisherName',
                render: function (data) {
                    offerId = data;
                    return ' <div style="margin-left:80px"></div>' + data;
                }
            },

            {
                data: 'ip',
                render: function (data) {
                    return ' <div style="margin-left:80px"></div>' + data;
                }
            },
            {
                data: 'deviceName',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },

            {
                data: 'creationDateTime',
                render: function (data) {
                    if (data != null) {
                        var date = new Date(data);
                        const options = {weekday: 'long', year: 'numeric', month: 'long', day: 'numeric'};
                        /*return '<div style="margin-left:160px"></div>' + date.toLocaleString('en-US', options) + " " + date.toLocaleTimeString('en-US');*/
                        return '<div style="margin-left:160px"></div>' + date.toLocaleString('en-GB');
                    } else
                        return '<div style="margin-left:160px"></div>NA';
                }
            }

        ],
        "scrollCollapse": true,
        scrollY: 520,
        ordering: true
    });
    $(window).bind('resize', function(e)
    {
        if (window.RT) clearTimeout(window.RT);
        window.RT = setTimeout(function()
        {
            table.ajax.reload(); /* false to get page from cache */
        }, 250);
    });
    $('.dataTables_filter input').unbind();
    $('.dataTables_filter input').keyup(function (e) {
        if (e.keyCode === 13) /* if enter is pressed */ {
            table.search($(this).val()).draw();
        }
    });

    $(function () {
        var start = moment().subtract(6, 'days').startOf('day');
        var end = moment().endOf('day');

        function cb(start, end) {
            dateRange = document.getElementById("reportrangeinput").value = start.format('YYYY-MM-DD HH:mm:ss') + ' - ' + end.format('YYYY-MM-DD HH:mm:ss');
            document.getElementById("reportRangeInputExport").value = start.format('YYYY-MM-DD HH:mm:ss') + ' - ' + end.format('YYYY-MM-DD HH:mm:ss');
            document.getElementById("reportrangeinput").value = start.format('YYYY-MM-DD HH:mm:ss') + ' - ' + end.format('YYYY-MM-DD HH:mm:ss');
            table.column(8).search(dateRange).draw();
            $('.btnDatatable').hide();
            $('#exportData').show();
        }

        $('.reportrange').daterangepicker({
            startDate: start,
            timePicker: true,
            timePicker24Hour: true,
            endDate: end,
            ranges: {
                'Today': [moment().startOf('day'), moment().endOf('day')],
                'Yesterday': [moment().subtract(1, 'days').startOf('day'), moment().subtract(1, 'days').endOf('day')],
                'Last 7 Days': [moment().subtract(6, 'days').startOf('day'), moment().endOf('day')],
                'Last 30 Days': [moment().subtract(29, 'days').startOf('day'), moment().endOf('day')],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        cb(start, end);

    });
    var offerIds = '';
    $('select#offerListAjax').change(function () {
        offerIds = '';
        $('select#offerListAjax option:selected').each(function () {
            offerIds += $(this).val() + ",";

        });
        offerIds = offerIds.substring(0, offerIds.length - 1);
    });
    var pubIds = '';
    $('select#publisherListAjax').change(function () {
        pubIds = '';
        $('select#publisherListAjax option:selected').each(function () {
            pubIds += $(this).val() + ",";

        });
        pubIds = pubIds.substring(0, pubIds.length - 1);
    });
    var advIds = '';
    $('select#advertiserListAjax').change(function () {
        advIds = '';
        $('select#advertiserListAjax option:selected').each(function () {
            advIds += $(this).val() + ",";

        });
        advIds = advIds.substring(0, advIds.length - 1);
    });
    var array=[];
    $('body').on('change', '#extSelect', function (e) {
        e.preventDefault();
        filter1 = '';
        array=[];
        var i, j;
        filter1 = $(this).val();
        for (i = 0; i < filter1.length; i++){
            array.push(filter1[i]);
        }
    });

    $(document).ready(function () {
        filter1 = '';
        array=[];
        filter1 = $("#extSelect").val();
        for (i = 0; i < filter1.length; i++){
            array.push(filter1[i]);
        }
    });


    table.columns().visible(false);
    table.columns([0, 1, 2, 3, 4, 5, 8]).visible(true);
    var uuid;
    $('#submitForm').click(function () {
        uuid = document.getElementById("searchUuid").value;
        var i, j;
        $("#exampleModalAd1").hide();
        if (filter1.length > 0) {
            table.columns().visible(false);
            for (i = 0; i < filter1.length; i++) {
                j = array.indexOf(array[i]);
                switch(filter1[i]) {
                    case 'Offer Id':table.columns(0).visible(true);;break;
                    case 'Offer Name':table.columns(1).visible(true);;break;
                    case 'AttributionClick.java Id':table.columns(2).visible(true);;break;
                    case 'Pub AttributionClick.java Id':table.columns(3).visible(true);;break;
                    case 'Publisher Id':table.columns(4).visible(true);;break;
                    case 'Publisher Name':table.columns(5).visible(true);;break;
                    case 'IP Address':table.columns(6).visible(true);;break;
                    case 'Device Name':table.columns(7).visible(true);;break;
                    case 'Creation Date Time':table.columns(8).visible(true);;break;
                }

            }
        }
        /*console.log("BEfore draw:" + advIds);*/
        table.column(2).search(uuid);
        table.column(4).search(pubIds);
        table.column(0).search(offerIds).draw();
        table.column(1).search(advIds).draw();
        $('.btnDatatable').hide();
        $('#exportData').show();
    });

    $('#offerFilter').click(function () {
        $("#exampleModalAd1").show();
    });


    $('select#publisherListAjax')
        .change(
            function () {
                pubFilter = '';
                $(
                    'select#publisherListAjax option:selected')
                    .each(function () {
                        pubFilter += $(this).val() + ",";
                    });
                pubFilter = pubFilter.substring(0, pubFilter.length - 1);
                /*console.log("pubFilter:" + pubFilter);*/
            });

    $('select#offerListAjax')
        .change(
            function () {
                offerFilter = '';
                $(
                    'select#offerListAjax option:selected')
                    .each(function () {
                        offerFilter += $(this).val() + ",";
                    });
                offerFilter = offerFilter.substring(0, offerFilter.length - 1);
                /*console.log("offerFilter:" + offerFilter);*/
            });


    $('#searchUuid').on('keyup', function () {
        table.search(this.value);
    });

    $("#offerFilter").click(function () {
        offerLoad('#exampleModalAd', '', '/adminConsole/infiniteScrollerOffers', '#offerListAjax');
        offerLoad('#exampleModalAd', '', '/adminConsole/infiniteScrollerAdvertiser', '#advertiserListAjax');
        offerLoad('#exampleModalAd', '', '/adminConsole/infiniteScrollerPublisher', '#publisherListAjax');
    });

    $("#exportFilter").click(function () {
        offerLoad('#exportModal', '', '/adminConsole/infiniteScrollerOffers', '#exportOfferListAjax');
        offerLoad('#exportModal', '', '/adminConsole/infiniteScrollerAdvertiser', '#exportPublisherListAjax');
        // offerLoad('#exampleModalAd', '', '/adminConsole/infiniteScrollerPublisher', '#advertiserListAjax');
    });


    function offerLoad(modalName, title, url, className) {
        var perPageRecord = 10;
        $(className).select2({
            dropdownParent: $(modalName),
            ajax: {
                url: url,
                type: 'GET',
                dataType: 'json',
                delay: 500,
                data: function (params) {
                    return {
                        search: params.term, // search term
                        page: params.page || 0,
                        size: perPageRecord//Per page records
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 0;
                    return {
                        results: data.results,
                        pagination: {
                            more: params.page < data.count_filtered - 1
                        }
                    };
                },
                cache: true
            },
            placeholder: title,
            minimumInputLength: 0
        });
    }

    function showNotification(from, align, type, message) {

        $.notify({
            icon: "add_alert",
            message: message,

        }, {
            type: type,
            timer: 3000,
            placement: {
                from: from,
                align: align
            }
        });
    }

</script>
<script>
    $(document).ready(function () {
        // initialise Datetimepicker and Sliders
        md.initFormExtendedDatetimepickers();
        if ($('.slider').length != 0) {
            md.initSliders();
        }
    });

    function submitExportForm() {
        console.log("Inside submitExportForm:")
        var formData = new FormData(document.getElementById('exportFormSubmit'));// yourForm: form selector
        console.log(formData);
        $.ajax({
            async: true,
            type: "POST",
            url: "/adminConsole/exportClicks",// where you wanna post
            data: formData,
            processData: false,
            contentType: false,
            error: function (jqXHR, textStatus, errorMessage) {
                var o = $.parseJSON(jqXHR.responseText);
                window.open('/adminConsole/error?status='+o.status+'&message='+o.error,"_self")
            },
            success: function (data) {
                console.log(data);
                document.getElementById("downloadNotificationUL").style.display = "inline";
                document.getElementById("downloadNotificationLink").href = data;
                // document.cookie = 'cookie1=test; expires=Sun, 1 Jan 2023 00:00:00 UTC; path=/'
                // localStorage.setItem("link", data);
                // showNotification('top', 'right', 'info', 'Your download link has been generated: ' + document.write("<a>data</a>"));
            }
        });
    }

</script>

<script>
    $('.multipleSelect').select2();

    $('#exportData').click(function(){
        Swal.fire({
            title: "Export Report Name",
            input: 'text',
            inputPlaceholder: 'Enter export report name',
            showCancelButton: true
        }).then((result) => {
            if (result.value) {
                exportAllData(result.value);
                $('.btnDatatable').hide();
                $('#exportData').show();
            }
        });


    });
    /*Exprot data functionality*/
    var search={};
    function exportAllData(exportName) {
        document.removeEventListener("mouseover", exportAllData);
        search['uuid'] = uuid;
        search['pubId'] = pubIds;
        search['offerId'] = offerIds;
        search['offerName'] =advIds;
        search['creationDateTime'] = dateRange;
        var title;
        $.ajax({
            type: "POST",
            url: "/adminConsole/exportClickDataCount",
            "destroy": true,
            "processing": true,
            "serverSide": true,
            data: search,
            success: function (response) {
                if(response.includes("export clicks older than 7 days"))
                exportNotification1(response)
                else
                exportNotification(search, response, exportName)
            }
        });
    }
    function exportNotification(search, title, exportName) {
        Swal.fire({
            title: title,
            type: 'success',
            confirmButtonText: 'OK!',
            showCancelButton: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "/adminConsole/exportClickData?columns=" + filter1+"&exportName="+exportName,
                    "destroy": true,
                    "processing": true,
                    "serverSide": true,
                    data: search,
                    success: function (response) {
                        /*location.reload();*/
                        setTimeout(function () {
                            window.open('/adminConsole/findAllCSVFile');
                            window.location.reload();
                        }, 1000);
                    }
                });
            }
        });
    }

    function exportNotification1(title) {
        Swal.fire({
            title: title,
            type: 'success',
            confirmButtonText: 'Create Ticket',
            showCancelButton: true
        }).then((result) => {
            if (result.value) {
                /*location.reload();*/
                setTimeout(function () {
                    window.open('/adminConsole/support');
                    window.location.reload();
                }, 1000);
            }
        });
    }

    function datatables1(dataTableVal) {
        var table1 = $(dataTableVal).DataTable({
            "sPagingType": "full_number",
            "lengthMenu": [
                [5, 10, 20, -1],
                [5, 10, 20, "All"]
            ],
            scrollX: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
            },
            dom: 'Blfrtip',
            buttons: [{
                extend: 'csv',
                text: '<i class="fa fa-download"></i>&nbsp;&nbsp;Export',
                title: 'Report',
                className: 'btnDatatable bg-transparent text-white',
                exportOptions: {
                    columns: ':visible', orthogonal: 'export'
                }
            }]
        });
    }
</script>

</body>

</html>