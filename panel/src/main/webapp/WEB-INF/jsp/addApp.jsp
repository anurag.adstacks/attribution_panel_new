<!DOCTYPE html>
<html lang="en">
<%@ page import="com.attribution.panel.constants.UIConstants" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<head>
    <meta charset="utf-8"/>
    <link rel="apple-touch-icon" sizes="76x76" href="../../asset/img/apple-icon.png">
    <link rel="icon" type="image/png" href="../../asset/img/logoAttribution.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>
        App
    </title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport'/>
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- CSS Files -->
    <link href="../../asset/css/material-dashboard.css?v=2.1.0" rel="stylesheet"/>
    <!-- Responsive -->
    <link href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="../asset/css/style-sheet-attribution.css" rel="stylesheet"/>
    <style>
        .pager div {
            float: left;
        }

        .items div {
            padding: 0;
            margin: 0
        }
    </style>
</head>

<body class="fixed-left blue_dark drawer drawer--right " data-theme='blue_dark' onload="searchPlatforms()"
      style="background-color:<%=UIConstants.primaryBackgroundColor%>;font-family:<%=UIConstants.primaryFontFamily%>;">
<div class="wrapper ">
    <jsp:include page="adminSidebar.jsp">
        <jsp:param value="active" name="addApps"/>
    </jsp:include>

    <div class="main-panel">
        <!-- Navbar -->
        <jsp:include page="adminHeader.jsp"/>
        <div class="content">

            <section class="addApps">

                <div class="container">

                    <div class="row" style="width: 90%;">
                        <div class="addApps-info" style="color: black; margin-top: 160px; margin-left: 20px;">

                            <p class="paragraph" style="color: #303654; font-size: 30px; ">Welcome to AppsFlyer!</p>
                            <p class="paragraph2"
                               style="">We are glad you are here.</p><br>
                            <p class="paragraph3"
                               style="color: #303654;">Lets get started by adding your first
                                app.</p>
                            <button class="addApp-button"
                                    style=" margin-left:10px; margin-top: 20px;  color: black; height: 50px; width: 100px; background-color: #303654; border: 2px solid black; border-radius: 10px;"
                                    data-toggle="modal" data-target=".addAppModal"
                                    type="button"><span class="addApps-buttonSpam"
                                                        style="color:whitesmoke;">Add App</span>
                            </button>
                            <a class="" href="${contextPath}/adminConsole/appRedirect">
                                <button class="addApp-button"
                                        style=" margin-left:30px; margin-top: 20px;  color: black; height: 50px; width: 100px; background-color: #303654; border: 2px solid black; border-radius: 10px;"
                                        type="button"><span class="addApps-buttonSpam"
                                                            style="color:whitesmoke;">Your Apps</span>
                                </button>
                            </a>

                        </div>

                        <div class="addApps-img" style="margin-left: 100px; margin-top: 20px;">
                            <figure><img src="../assets/img/addApp-image.jpg"
                                         style="height: 500px; width: 450px; margin-left: 100px;" alt="sing up image">
                            </figure>
                        </div>
                    </div>

                </div>


                <div class="modal fade addAppModal" style="margin-top: -60px;" tabindex="-1" role="dialog"
                     aria-labelledby="myLargeModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">

                            <div class="modal-header" style="">

                                <div class="row" style="width: 100%;">
                                    <h2 class="modal-title" style="color: black; " id="exampleModalLabel">Add an
                                        app</h2>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>

                            </div>

                            <div class="modal-body">

                                <h4 style="color: #303654; margin-left: 10px; ">Enter app details</h4>

                                <br>

                                <form id="saveDetails" onsubmit="saveDetails()">
                                    <div>

                                        <div class="form-group" style="width: 600px;">
                                            <label class="bmd-label-floating <%=UIConstants.primaryTextColorClass%> <%=UIConstants.primaryTextColorClass%>"
                                                   style="font-weight:bold;margin-left:10px;" for="appName">App
                                                Name*</label>
                                            <input type="text"
                                                   class="form-control <%=UIConstants.primaryTextColorClass%>"
                                                   id="appName" name="name"
                                                   required="true"
                                                   style="margin-left:10px;">
                                        </div>

                                        <br>

                                        <div class="form-group" style="width: 600px;">
                                            <label class="bmd-label-floating <%=UIConstants.primaryTextColorClass%> <%=UIConstants.primaryTextColorClass%>"
                                                   style="font-weight:bold;margin-left:10px;" for="appDescription">App
                                                Description</label>
                                            <input type="text"
                                                   class="form-control <%=UIConstants.primaryTextColorClass%>"
                                                   id="appDescription" name="Description"
                                                   required="true"
                                                   style="margin-left:10px;">
                                        </div>

                                        <br>

                                        <!--surround the select box with a "custom-select" DIV element. Remember to set the width:-->
                                        <div class="custom-selectPlatform" style="width:600px; margin-left:10px;">
                                            <label class="bmd-label-floating <%=UIConstants.primaryTextColorClass%> <%=UIConstants.primaryTextColorClass%>"
                                                   style="font-weight:bold;margin-left:0px;" for="appSelect">Select
                                                Platform:</label>
                                            <select id="appSelect" name="os">
                                                <option value="0">Select:</option>
                                                <option>ANDROID</option>
                                                <option>IOS</option>
                                                <option>Windows</option>
                                                <option>Web</option>
                                                <option>Roku</option>
                                                <option>Vizio</option>
                                                <option>Samsung</option>
                                                <option>LG</option>
                                            </select>
                                        </div>

                                        <br>

                                        <div class="form-group" style="width: 600px;">
                                            <label class="bmd-label-floating <%=UIConstants.primaryTextColorClass%> <%=UIConstants.primaryTextColorClass%>"
                                                   style="font-weight:bold;margin-left:10px;" for="appLink">App
                                                Link</label>
                                            <input type="text"
                                                   class="form-control <%=UIConstants.primaryTextColorClass%>"
                                                   id="appLink" name="Link"
                                                   required="true"
                                                   style="margin-left:10px;">
                                        </div>

                                        <br>

                                        <div class="input-group" style="width: 600px;">

                                            <label class="bmd-label-floating <%=UIConstants.primaryTextColorClass%> <%=UIConstants.primaryTextColorClass%>"
                                                   style="margin-left:10px;" for="appLink">Select App Logo: &nbsp;
                                                &nbsp;
                                            </label>

                                            <div class="input-group-prepend">
                                        <span class="input-group-text" id="socialMediaImage"
                                              style="border: 1px solid black; font: bold; background-color: grey; color: black; ">Upload</span>
                                            </div>

                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="inputGroupFile01"
                                                       name="appLogo"
                                                       aria-describedby="socialMediaImage">
                                                <label class="custom-file-label" for="inputGroupFile01"
                                                       style="border: 1px solid black;">Choose Logo</label>
                                            </div>
                                        </div>

                                        <br>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                                            </button>
                                            <button type="submit" class="btn btn-primary">Sunbmit</button>
                                        </div>

                                    </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>


            </section>

        </div>
    </div>
</div>

<!--   Core JS Files   -->
<script src="../../asset/js/core/jquery.min.js"></script>
<script src="../../asset/js/core/popper.min.js"></script>
<script src="../../asset/js/core/bootstrap-material-design.min.js"></script>
<script src="../../asset/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!-- Plugin for the momentJs  -->
<script src="../../asset/js/plugins/moment.min.js"></script>
<!--  Plugin for Sweet Alert -->
<script src="../../asset/js/plugins/sweetalert2.js"></script>
<!-- Forms Validations Plugin -->
<script src="../../asset/js/plugins/jquery.validate.min.js"></script>
<!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
<script src="../../asset/js/plugins/jquery.bootstrap-wizard.js"></script>
<!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script src="../../asset/js/plugins/bootstrap-selectpicker.js"></script>
<!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
<script src="../../asset/js/plugins/bootstrap-datetimepicker.min.js"></script>
<!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
<script src="../../asset/js/plugins/jquery.dataTables.min.js"></script>
<!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
<script src="../../asset/js/plugins/bootstrap-tagsinput.js"></script>
<!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="../../asset/js/plugins/jasny-bootstrap.min.js"></script>
<!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
<script src="../../asset/js/plugins/fullcalendar.min.js"></script>
<!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
<script src="../../asset/js/plugins/jquery-jvectormap.js"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="../../asset/js/plugins/nouislider.min.js"></script>
<!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
<!-- Library for adding dinamically elements -->
<script src="../../asset/js/plugins/arrive.min.js"></script>
<!--  Google Maps Plugin    -->
<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!-- Chartist JS -->
<script src="../../asset/js/plugins/chartist.min.js"></script>
<!--  Notifications Plugin    -->
<script src="../../asset/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="../../asset/js/material-dashboard.js?v=2.1.0" type="text/javascript"></script>
<script src="../asset/js/paginga.jquery.js"></script>
<script src="../asset/js/flexible.pagination.js"></script>


<script>
    function saveDetails() {
        var myform = document.getElementById("saveDetails");
        var fd = new FormData(myform);
        event.preventDefault();
        $.ajax({
            url: "/saveApp",
            data: fd,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            beforeSend: function () {
            },
            success: function (response) {
                showNotification('top', 'left', 'Your application has been submitted successfully');
                /*location.reload();*/
                setTimeout(function () {
                    window.location.reload();
                }, 1000);
            },
            complete: function () {
                // Hide image container

            },

            error: function () {
                console.log("Error");
            }
        });
    }

    function showNotification(from, align, message) {

        $.notify({
            icon: "add_alert",
            message: message
        }, {
            type: 'success',
            timer: 3000,
            placement: {
                from: from,
                align: align
            }
        });
    }
</script>

<%--<script>
    function appRedirect() {
        alert("hello ");
        $.ajax({
            url: "adminConsole/appRedirect"
        });
    }
</script>--%>


<%--<script>
    function searchPlatforms() {
        var flexiblePagination = $('#content').flexiblePagination({
            ajax: {
                url: '/getAppsListAjax',
                onSuccessCallBack: function (response) {
                    var platformListBody = "";
                    for (var i = 0; i < response.data.length; i++) {
                        var image="";
                        if(response.data[i].appLogo!=null)
                            image='<img src="/imageAppLogoDisplay?id=' + response.data[i].id + '" style="height:55px;width:60px;border-radius:10px;margin-top:8px"/>';
                        else
                            image='<img src="/asset/img/blank-profile-picture-973460__480.png" style="height:55px;width:60px;border-radius:10px;margin-top:8px"/>';
                        var icon = "";
                        if (response.data[i].os === 'ANDROID')
                            icon = '<i class="fa fa-android" aria-hidden="true" style="position:absolute;margin-top:40px;margin-left:10px"></i>'
                        else
                            icon = '<i class="fa fa-apple" style="position:absolute;margin-top:40px;margin-left:10px"></i>';
                        var name;
                        if(response.data[i].name.length>20)
                            name= '<span style="margin-right:20px" data-toggle="tooltip" title="' + response.data[i].name + '">' + response.data[i].name.substring(0, 20)+"..."+ '...</span>';
                        else
                            name=response.data[i].name
                        platformListBody +='<div class="col-lg-4 col-md-4">'
                            + '<div class="card" style= "background-color:#136AA0;border:1px solid black;border-radius:15px;height:150px;font-weight:bold;">'
                            + '<h4 class="card-title" style="margin-top:10px"><b style="color:white;margin-left:15px;font-size:22px;">'
                            + response.data[i].os
                            + '<div style="float: right;margin-top:0;margin-right:12px"><i class="material-icons" onclick="uploadDocumentFile(' + response.data[i].id + ')" data-toggle="modal" data-target="#uploadImageFile" style="color:white;font-size:20px;width: 20px;height: 20px;cursor: pointer;margin-right: 5px">upload</i>'
                            + '<i class="material-icons" style="color:white;font-size:20px;cursor: pointer" onclick="editApp('+response.data[i].id+')" data-toggle="modal" data-target="#exampleModalAd">app_registration</i></div>'
                            + '</b></h4>'
                            + '<a style="cursor: pointer" href="/adminConsole/dashboard?id=' + response.data[i].id + '"><h4 class="card-title" style="border-top:1px solid white;border-bottom:1px solid white;height:75px;margin-top:5px"><b style="color:#FFFFFF;margin-left:15px;font-size:22px;position: absolute;margin-top:10px">'
                            + '<div class="row" style="margin-top:-8px">'
                            + '<div class="img-container" style="margin-left:30px">'
                            + image+ icon + '</div> <div class="" style="border-left:1px solid #aaa;color:#fff;margin-left:130px;margin-top:-58px;"><div style="margin-left:20px;margin-top:10px;color:#FFFFFF">' + name + '</div>'
                            + '</div></div></div></a>'
                            + '</div></div>'
                            + '</b></h4>'
                            + '</div> </a>'
                            + '</div>';
                    }
                    return platformListBody;
                },
                onFailureCallBack: function (error) {
                    alert("Ajax Error  - See Console for details!");
                    console.log(error);
                }
            },
            itemsPerPage: 1000000000,
            itemSelector: 'div.items:visible',
            pagingControlsContainer: '#pagingControls',
            showingInfoSelector: '#showingInfo',
            css: {
                btnNumberingClass: 'btnNumbering',
                btnFirstClass: 'first',
                btnLastClass: 'last',
                btnNextClass: 'btn btn-sm nextPre',
                btnPreviousClass: 'btn btn-sm nextPre'
            }
        });
        flexiblePagination.pagingContainer = '#content';
        flexiblePagination.getController().onPageClick = function (pageNum, e) {
        };
    }

    function editApp(e){
        document.getElementById("modalHeading").innerHTML = "Edit App";
        $.ajax({
            url: "/findAppById?appId=" + e,
            contentType: 'json',
            type: 'GET',
            success: function (response) {
                try {
                    document.getElementById("appId").value =response.app.id;
                    document.getElementById("name").value =response.app.name;
                    document.getElementById("description").value =response.app.description;
                    document.getElementById("link").value =response.app.link;
                    document.getElementById("os").value =response.app.os;
                    $(":input").change();
                } catch (error) {
                    console.log(error);
                }
            },

            error: function (jqXHR) {
                var o = $.parseJSON(jqXHR.responseText);
                window.open('/adminConsole/error?status=' + o.status + '&message=' + o.error, "_self")
            }
        });
    }

    $("#newApp").on("click", function () {
        document.getElementById("modalHeading").innerHTML = "Create App";
        $('#saveDetails')[0].reset();
        $(":input").change();
    });

    function uploadImageDocument() {
        $("#submitDocument").prop('disabled', true);
        var myform = document.getElementById("uploadImageDocument");
        var fd = new FormData(myform);
        event.preventDefault();
        $.ajax({
            url: "/uploadImageDocument",
            data: fd,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            beforeSend: function () {


            },
            success: function (response) {
                showNotification('top', 'left', 'Your application has been submitted successfully');
                /*location.reload();*/
                setTimeout(function () {
                    window.location.reload();
                }, 1000);
            },
            complete: function () {
                // Hide image container

            },

            error: function (jqXHR) {
                var o = $.parseJSON(jqXHR.responseText);
                window.open('/adminConsole/error?status=' + o.status + '&message=' + o.error, "_self")
            }
        });
    }

    function uploadDocumentFile(e){
        document.getElementById("appId1").value=e;
        $.ajax({
            url: "/findDownloadImageFile?appId=" + e,
            contentType: 'json',
            type: 'GET',
            success: function (response) {
                var tableBodyResponse = "";
                tableBodyResponse += '<table  class="display nowrap datatables1" style="width:100%">'
                    + '<thead>'
                    + '<tr>'
                    + '<th>S.No.</th>'
                    + '<th>Name</th>'
                    + '<th>Action</th>'
                    + '</tr>'
                    + '</thead>'
                    + '<tbody>';
                for (var i = 0; i < response.uploadAppImage.length; i++) {
                    var serialNo=i+1;/*star.png*/
                    tableBodyResponse += '<tr>'
                        + '<td><div style="margin-left:40px"></div>' +serialNo+ '</td>'
                        + '<td><div style="margin-left:100px"></div>' + response.uploadAppImage[i].fileName +'</td>'
                        + '<td><a href="' + response.uploadAppImage[i].url + '" target="_blank" data-toggle = "tooltip" title = "Print" style="font-size:16px;margin-left: 3%;height: 30px;padding: 0 10px;border-radius: 10px;color: #ffffff"> <span class="glyphicon glyphicon-file icon-green" style="font-size: 23px;color:dodgerblue"></span><i class="material-icons" style="color: #0a6ebd">print</i></a></div></td>'
                        + '</tr>'
                }
                tableBodyResponse += '</tbody>'
                    + '</table>';
                document.getElementById("document").innerHTML = tableBodyResponse;
            },
            error: function (jqXHR) {
                /*var o = $.parseJSON(jqXHR.responseText);
                window.open('/adminConsole/error?status=' + o.status + '&message=' + o.error, "_self")*/
            }
        });
    }
</script>--%>


</body>

</html>