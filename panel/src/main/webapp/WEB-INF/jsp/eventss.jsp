<%@ page import="java.util.List" %>
<%@ page import="com.adstacks.attribution.constants.UIConstants" %>
<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html lang="en">

<head>
    <meta charset="utf-8"/>
    <link rel="apple-touch-icon" sizes="76x76" href="../../assets/img/apple-icon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <link rel="icon" type="image/png" href="../assets/img/logoAttribution.png">
    <title>
        Event
    </title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport'/>
    <link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- CSS Files -->
    <link href="../assets/css/material-dashboard.css?v=2.1.0" rel="stylesheet"/>
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="../assets/demo/demo.css" rel="stylesheet"/>
    <link rel="canonical" href="https://github.com/dbrekalo/fastselect/"/>
    <link href="../assets/css/style-sheet-adjar.css?v=2.1.0" rel="stylesheet"/>
    <!--  <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700,900&subset=latin,latin-ext' rel='stylesheet' type='text/css'> -->
    <!-- <link rel="stylesheet" href="https://rawgit.com/dbrekalo/attire/master/dist/css/build.min.css"> -->
    <script src="https://rawgit.com/dbrekalo/attire/master/dist/js/build.min.js"></script>
    <link rel="stylesheet" href="../assets/fastselect.min.css">
    <script src="../assets/fastselect.standalone.js"></script>
    <%--loader CSS Files--%>
    <link href="../assets/css/loader.css" rel="stylesheet"/>
    <%--    Tooltip textarea CSS --%>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote-lite.min.css" rel="stylesheet">
    <link href="../dist/css/select2.css" rel="stylesheet"/>
    <%--responsive css file--%>
    <link href="../assets/css/responsive-modal-pages.css" rel="stylesheet"/>

    <!--     Fonts and icons     -->
    <%--    <link rel="stylesheet" type="text/css"
              href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
        <!-- CSS Files -->
        <link href="../../assets/css/material-dashboard.css?v=2.1.0" rel="stylesheet"/>
        <link href="../assets/css/style-sheet-attribution.css?v=2.1.0" rel="stylesheet"/>
        &lt;%&ndash;loader CSS Files&ndash;%&gt;
        <link href="../assets/css/loader.css" rel="stylesheet"/>
        <!-- CSS Just for demo purpose, don't include it in your project -->
        <link href="../../assets/demo/demo.css" rel="stylesheet"/>
        <link rel="stylesheet" href="../assets/fastselect.min.css">
        <!-- Responsive -->
        <link href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css" rel="stylesheet"
              type="text/css"/>
        <link href="../dist/css/select2.css" rel="stylesheet"/>
        <link rel="stylesheet" type="text/css" href="../assets/css/daterangepicker.css"/>--%>

    <style>
        td, th {
            text-align: center;
        }

        td {
            border: 1px solid #615f5c;
        }

        th {
            border-top: 3px solid #615f5c;
            border-left: 1px solid #615f5c;
            border-right: 1px solid #615f5c;
        }

        th:last-child, td:last-child {
            border-right: none;
        }

        th:first-child, td:first-child {
            border-left: none;
        }
    </style>


</head>

<body class="fixed-left blue_dark drawer drawer--right " data-theme='blue_dark'
      style="background-color:<%=UIConstants.primaryBackgroundColor%>;font-family:<%=UIConstants.primaryFontFamily%>;"
      onload="getEventList()">
<div class="se-pre-con"></div>
<div class="wrapper ">
    <jsp:include page="integratedSidebar.jsp">
        <jsp:param value="active" name="event"/>
    </jsp:include>
    <div class="main-panel">
        <jsp:include page="adminHeader.jsp"/>
        <%-- <jsp:include page="adminHeader.jsp"/>--%>
        <!-- Navbar -->
        <%--<jsp:include page="adminHeader.jsp"/>--%>

        <!-- End Navbar -->
        <div class="content">
            <div class="container-fluid">
                <div class="cardDiv">
                    <div class="cardSize">
                        <i class="material-icons iconSize">assignment</i>
                    </div>
                    <h2 class="cardHeading <%=UIConstants.primaryTextColorClass%>" style="-moz-transform: scale(0.98);">
                        Event List</h2>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card <%=UIConstants.primaryTextColorClass%>"
                             style="background-color:<%=UIConstants.primaryCardBackground%>;font-family:<%=UIConstants.primaryFontFamily%>">
                            <div class="card-header card-header-<%=UIConstants.primaryColorClass%> card-header-icon responsiveButton">
                                <!-- Button trigger modal -->
                                <%--<sec:authorize access="hasRole('ADMIN')">--%>
                                <div class="row" style="">
                                    <div class="" style="margin-top:-40px; margin-left: 1300px;">
                                        <button class="filterButton btn btn-<%=UIConstants.primaryColorClass%> btn-round"
                                                data-toggle="modal"
                                                style="cursor: pointer;margin-top:0;padding:4px 10px;margin-left:10px"
                                                data-target="#signupModal" data-command="reset"
                                                id="reset">
                                            <i class="fa fa-plus" aria-hidden="true"
                                               style="margin-left:0px;margin-top:-15px;width:auto;font-size:14px"
                                               data-toggle="modal" data-target="#signupModal"></i><b
                                                style="margin-left:6px;">Add Event&nbsp;</b>
                                        </button>
                                        <%--<button class="filterButton pull-right" data-toggle="modal"
                                                data-target="#addEvent"
                                                id="newEvent" style="margin-top:-40px;-moz-transform: scale(0.92);">
                                            <i class="fa fa-plus" aria-hidden="true" data-toggle="modal"
                                               data-target="#addEvent"
                                               style="width:auto;font-size:15px;margin-left:0px;"><b
                                                    style="margin-left:6px;">Add Event</b></i>
                                        </button>--%>
                                    </div>
                                </div>
                                <%--</sec:authorize>--%>
                            </div>

                            <div class="modal fade" id="signupModal" tabindex="-1"
                                 role="dialog" aria-hidden="true" data-backdrop="static">
                                <div class="modal-dialog " role="document">
                                    <!-- 									<div class="modal-content" style="background-color: <%=UIConstants.primaryCardBackground%>;">
 -->
                                    <div class="card modal-content <%=UIConstants.primaryTextColorClass%> offerDetailsContent"
                                         style="background-color:<%=UIConstants.primaryModalCardHeaderBackground%>;width:540px">
                                        <div class="modalClose">
                                            <i class="fa fa-times" data-dismiss="modal"
                                               aria-label="Close"></i>
                                        </div>
                                        <div class="card-header card-header-<%=UIConstants.primaryColorClass%> card-header-icon"
                                             style="height:70px;">
                                            <div class="card-icon">
                                                <i class="material-icons">dashboard</i>
                                            </div>
                                            <h4 class="card-title <%=UIConstants.primaryTextColorClass%>"
                                                style="font-family: <%=UIConstants.primaryFontFamily%>; font-weight: bold; margin-top: 0px;"
                                                id="modalHeading">Create Event</h4>
                                            <p class="card-category" style="margin-top: 0px;">Enter Information</p>
                                            <!-- <p class="card-category">Enter Advertiser Info</p> -->
                                        </div>
                                        <div class="card-body offerDetailsCard"
                                             style="font-family: <%=UIConstants.primaryFontFamily%>; font-weight: bold;background-color:<%=UIConstants.primaryCardModalBackground%>;width:auto;font-size:14px">
                                            <p>${message}</p>
                                            <div style="" id="mymodel">

                                                <hr style="height: 5px;border-width:0;color:gray;background-color:#303654; margin-top: 2px;">

                                                <div class="navOfferDetails"
                                                     style="margin-top:14px;background-color:<%=UIConstants.primaryCardBackground%>;height:50px;">
                                                    <ul class="nav nav-pills nav-pills-<%=UIConstants.primaryColorClass%>  <%=UIConstants.primaryTextColorClass%> justify-content-center"
                                                        role="tablist">

                                                        <li class="nav-item">
                                                            <a class="nav-link active <%=UIConstants.textColor%>"
                                                               data-toggle="tab"
                                                               href="#preDefineEvent"
                                                               role="tablist" id="nav-link" style="font-size: 15px;">
                                                                Pre Define Event
                                                            </a>
                                                        </li>

                                                        <li class="nav-item">
                                                            <a class="nav-link <%=UIConstants.textColor%>"
                                                               data-toggle="tab"
                                                               href="#userDefineEvent"
                                                               role="tablist" id="nav-link" style="font-size: 15px;">
                                                                User Define Event
                                                            </a>
                                                        </li>

                                                    </ul>
                                                </div>

                                                <hr style="height: 5px;border-width:0;color:gray;background-color:#303654; margin-top: 13px;">


                                                <div class="tab-content tab-space">

                                                    <div class="tab-pane active" id="preDefineEvent">
                                                        <div class="row" style="width:100%;margin-left:0px"><br>
                                                            <div class="card <%=UIConstants.primaryTextColorClass%>"
                                                                 Style="font-family:<%=UIConstants.primaryFontFamily%>;background-color:<%=UIConstants.primaryCardBackground%>;margin-top:-25px;">
                                                                <div class="card-header card-header-warning card-header-text">

                                                                </div>
                                                                <div class="card-body <%=UIConstants.primaryTextColorClass%> infoTabCard"
                                                                     style="margin-left:0px;">
                                                                    <br>

                                                                    <form method="post" id="addPreEventForm"
                                                                          class="form-horizontal"
                                                                          onsubmit="addPreEvent()">
                                                                        <input type="text" name="eventId" id="eventId"
                                                                               style="display:none">

                                                                        <div class="row"
                                                                        <%--style="margin-top:0;margin-left:-5px"--%>>

                                                                            <div class="col-sm-3">
                                                                                <label class="col-form-label<%=UIConstants.primaryTextColorClass%>"
                                                                                       style="color: black; font-weight:bold;margin-left:-1px;margin-top:-9px">SELECT EVENT:</label>
                                                                            </div>

                                                                            <div class="col-sm-9"
                                                                                 style="color:<%=UIConstants.textColor%>;margin-left:-20px">

                                                                                <select class="ajaxPreDefineEvent"
                                                                                        id="ajaxPreDefineEvent"
                                                                                        name="ajaxPreDefineEvent"
                                                                                        multiple
                                                                                        data-size="7" <%--style="width:300px"--%>
                                                                                        style="color:<%=UIConstants.textColor%>; margin-left:-20px; width: 300px;"
                                                                                ></select>
                                                                            </div>

                                                                        </div>

                                                                        <br>
                                                                        <br>
                                                                        <br>

                                                                        <input type="hidden" name="appId"
                                                                               value="${app.id}">

                                                                        <div class="row offerDetailsBottomLine"
                                                                             style="border-top:1px solid #423b3a;width:490px;margin-left:-20px;margin-top: -10px">
                                                                            <br>
                                                                            <div class="col-sm-12">
                                                                                <button class="btn btn-<%=UIConstants.primaryColorClass%> btn-round pull-right submitButton"
                                                                                        type="submit"
                                                                                        style="padding:8px 10px;;margin-top:10px;margin-right:30px">
                                                                                    <b style="margin-left:0px">Save
                                                                                        Event</b>&nbsp;
                                                                                </button>
                                                                            </div>
                                                                        </div>


                                                                    </form>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="tab-pane" id="userDefineEvent">
                                                        <div class="row" style="width:100%;margin-left:0px"><br>
                                                            <div class="card <%=UIConstants.primaryTextColorClass%>"
                                                                 Style="font-family:<%=UIConstants.primaryFontFamily%>;background-color:<%=UIConstants.primaryCardBackground%>;margin-top:-25px;">
                                                                <div class="card-header card-header-warning card-header-text">

                                                                </div>

                                                                <div class="card-body <%=UIConstants.primaryTextColorClass%> infoTabCard"
                                                                     style="background-color:<%=UIConstants.primaryCardBackground%>">
                                                                    <br>

                                                                    <form method="post" id="addEventForm"
                                                                          class="form-horizontal"
                                                                          onsubmit="addEvent()">
                                                                        <input type="text" name="eventId" id="eventId"
                                                                               style="display:none">

                                                                        <div class="row">
                                                                            <div class="col-sm-3">
                                                                                <label class=" col-form-label <%=UIConstants.primaryTextColorClass%>"
                                                                                       style="font-weight:bold;margin-left:-1px;margin-top:-9px">Event
                                                                                    Name*:</label>
                                                                            </div>
                                                                            <div class="col-sm-9">
                                                                                <input type="text"
                                                                                       oninvalid="this.setCustomValidity('Please Enter Event Name')"
                                                                                       oninput="setCustomValidity('')"
                                                                                       class="inputText eventName"
                                                                                       id="eventName" name="eventName"
                                                                                       style="color:<%=UIConstants.textColor%>;margin-left:-20px"
                                                                                       onchange="limitText(this,255)"
                                                                                       onkeypress="limitText(this,255)"
                                                                                       required>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row rowMargin">
                                                                            <div class="col-md-3">

                                                                                <label class="col-form-label <%=UIConstants.primaryTextColorClass%>"
                                                                                       style="font-weight:bold;margin-left:-1px;margin-top:13px">Description:</label>

                                                                            </div>
                                                                            <div class="col-sm-9 eventDescription">
                                                                                <div class="form-group">
                                                                                <textarea type="text" rows="4"
                                                                                          class="textArea"
                                                                                          oninput="auto_grow(this)"
                                                                                          id="description"
                                                                                          name="description"
                                                                                          style="color:<%=UIConstants.textColor%>;margin-left:-20px;margin-top:12px;padding:6px 10px"
                                                                                          onchange="limitText(this,5000)"
                                                                                          onkeypress="limitText(this,5000)"></textarea>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row rowMargin">

                                                                            <div class="col-md-4">
                                                                                <label class="col-form-label label-checkbox <%=UIConstants.primaryTextColorClass%>"
                                                                                       style="font-weight:bold;margin-left:-1px">Event
                                                                                    Status*:</label>
                                                                            </div>

                                                                            <div class="col-sm-8 checkbox-radios radiosBox">
                                                                                <div class="col-sm-2 form-check form-check-inline">
                                                                                    <label class="form-check-label <%=UIConstants.primaryTextColorClass%>"
                                                                                           style="font-weight:bold;margin-left:-40px">
                                                                                        <input class="form-check-input"
                                                                                               type="radio"
                                                                                               id="status"
                                                                                               name="status"
                                                                                               value="0"
                                                                                               checked> Active
                                                                                        <span class="circle" style="margin-top:2px"> <span class="check"></span></span>
                                                                                    </label>
                                                                                </div>
                                                                                <div class="col-sm-3 form-check form-check-inline offerDetailsInactiveStatus">
                                                                                    <label class="form-check-label <%=UIConstants.primaryTextColorClass%>"
                                                                                           style="font-weight:bold;margin-left:30px">
                                                                                        <input class="form-check-input"
                                                                                               type="radio"
                                                                                               id="status1"
                                                                                               name="status"
                                                                                               value="1" >Inactive
                                                                                        <span class="circle" style="margin-top:2px"><span class="check"></span></span>
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <br>

                                                                        <div class="row eventModalRow">
                                                                            <div class="col-md-4">
                                                                                <label class="col-form-label <%=UIConstants.primaryTextColorClass%>"
                                                                                       style="font-weight:bold;margin-left:-1px">Revenue
                                                                                    Model*:</label>
                                                                            </div>
                                                                            <div class="col-lg-5 col-md-6 col-sm-3 revenueModelEvent"
                                                                                 style="margin-left:-40px">
                                                                                <div class="dropdown bootstrap-select show-tick show <%=UIConstants.primaryTextColorClass%>">
                                                                                    <select class="selectpicker <%=UIConstants.primaryTextColorClass%>"
                                                                                            id="revenueModel"
                                                                                            name="revenueModel"
                                                                                            data-size="7"
                                                                                            data-style="btn"
                                                                                            title="Single Select"
                                                                                            data-width="150px">
                                                                                        <option value="RPA"
                                                                                                selected>RPA
                                                                                        </option>
                                                                                        <option value="RPS">RPS
                                                                                        </option>
                                                                                        <option value="RPC">RPC
                                                                                        </option>
                                                                                        <option value="RPI">RPI
                                                                                        </option>
                                                                                        <option value="RPL">RPL
                                                                                        </option>
                                                                                        <option value="RPAS">
                                                                                            RPA+RPS
                                                                                        </option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="revenueModelEvent1">
                                                                                <div class="input-group-prepend"
                                                                                     style="margin-left:-20px">
                                                                                     <span class="input-group-text">
                                                                                      <i class="material-icons">monetization_on</i>
                                                                                      </span>
                                                                                </div>
                                                                                <div class="col-sm-4"
                                                                                     style="margin-left:-10px">
                                                                                    <div class="form-group">
                                                                                        <input type="text"
                                                                                               oninvalid="this.setCustomValidity('Please Enter Revenue')"
                                                                                               oninput="setCustomValidity('')"
                                                                                               class="form-control"
                                                                                               id="revenueModel1"
                                                                                               style="color:<%=UIConstants.textColor%>;width:100px"
                                                                                               name="revenueModel1"
                                                                                               required>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <br>

                                                                        <div class="card-collapse"
                                                                             style="font-family:<%=UIConstants.primaryFontFamily%>;background-color: <%=UIConstants.primaryCardModalBackground%>"
                                                                             id="advancedOption">
                                                                            <div class="card-header <%=UIConstants.primaryTextColorClass%>"
                                                                                 role="tab" id="headingTwo"
                                                                                 style="font-family:<%=UIConstants.primaryFontFamily%>;background-color:<%=UIConstants.primaryCardModalBackground%>;width:140px;margin-left:-1px;margin-top:-30px;;border-color:<%=UIConstants.primaryCardModalBackground%>">
                                                                                <h4 class="<%=UIConstants.primaryTextColorClass%>">
                                                                                    <a class="collapsed <%=UIConstants.primaryTextColorClass%>"
                                                                                       data-toggle="collapse"
                                                                                       href="#collapseTwo"
                                                                                       aria-expanded="false"
                                                                                       aria-controls="collapseTwo"
                                                                                       style="font-family:<%=UIConstants.primaryFontFamily%>;font-weight: bold">
                                                                                        <i class="fa fa-sort-desc"
                                                                                           aria-hidden="true"></i>Advanced
                                                                                        Option
                                                                                    </a>
                                                                                </h4>
                                                                            </div>
                                                                            <div id="collapseTwo" class="collapse"
                                                                                 role="tabpanel"
                                                                                 aria-labelledby="headingTwo"
                                                                                 data-parent="#accordion">
                                                                                <div class="card-body <%=UIConstants.primaryTextColorClass%>">

                                                                                    <div class="row"
                                                                                         style="margin-top:0px">
                                                                                        <label class="col-form-label eventTokenLabel <%=UIConstants.primaryTextColorClass%>"
                                                                                               style="font-weight:bold;margin-left:35px">Event
                                                                                            Token:</label>
                                                                                    </div>

                                                                                    <div class="row">
                                                                                        <div class="col-lg-6 col-md-6 col-sm-3 protocol"
                                                                                             style="margin-top:15px;margin-left:-100px;">
                                                                                            <div class="form-group">
                                                                                                <input type="text"
                                                                                                       class="inputText eventTokenInput"
                                                                                                       id="token"
                                                                                                       style="position:absolute;color:<%=UIConstants.textColor%>;margin-left:250px;width:160px;height:30px;margin-top:-42px"
                                                                                                       name="token"
                                                                                                       onchange="limitText(this,255)"
                                                                                                       onkeypress="limitText(this,255)">

                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="row eventVisibilityLabel">
                                                                                        <label class="col-form-label <%=UIConstants.primaryTextColorClass%>"
                                                                                               style="font-weight:bold;margin-left:40px;margin-top:-10px">Event
                                                                                            Visibility:</label>
                                                                                    </div>

                                                                                    <div class="row">
                                                                                        <div class="col-lg-4 col-md-6 col-sm-3 eventVisibility"
                                                                                             style="margin-left:12px;margin-top:-10px">
                                                                                            <label style="font-weight:bold">
                                                                                                <div class="dropdown bootstrap-select show-tick show <%=UIConstants.primaryTextColorClass%>">
                                                                                                    <select id="accessStatus"
                                                                                                            name="accessStatus"
                                                                                                            class="selectpicker <%=UIConstants.primaryTextColorClass%>"
                                                                                                            data-size="7"
                                                                                                            data-style="btn"
                                                                                                            title="Single Select">
                                                                                                        <option value="NeedApproval">
                                                                                                            NeedApproval
                                                                                                        </option>
                                                                                                        <option value="Public"
                                                                                                                selected>
                                                                                                            Public
                                                                                                        </option>
                                                                                                        <option value="Private">
                                                                                                            Private
                                                                                                        </option>
                                                                                                        <option value="Approved">
                                                                                                            Approved
                                                                                                        </option>
                                                                                                        <option value="Pending">
                                                                                                            Pending
                                                                                                        </option>
                                                                                                    </select></div>
                                                                                            </label>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="row conversionsRow">
                                                                                        <label class=" col-form-label <%=UIConstants.primaryTextColorClass%>"
                                                                                               style="font-weight:bold;margin-left:320px;margin-top:-100px">Multiple
                                                                                            Conversions<br>(With Same
                                                                                            Click
                                                                                            Id)</label>
                                                                                    </div>

                                                                                    <div class="row"
                                                                                         style="position:absolute;margin-top:-80px;margin-left:270px">
                                                                                        <div class="togglebutton col-lg-5 col-md-6 col-sm-3"
                                                                                             style="margin-top:22px;margin-left:50px">
                                                                                            <label class="<%=UIConstants.primaryTextColorClass%>"
                                                                                                   style="font-weight:bold">
                                                                                                <input type="checkbox"
                                                                                                       id="multiConv"
                                                                                                       name="multiConv">
                                                                                                <span class="toggle"></span>

                                                                                            </label>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="row"
                                                                                         style="margin-top:40px;margin-left:-27px;display: none"
                                                                                         id="partnersInEvent">
                                                                                        <label class="col-sm-3 col-form-label partnerLabel <%=UIConstants.primaryTextColorClass%>"
                                                                                               style="font-weight:bold;margin-top:-54px">Partners:</label>
                                                                                        <div class="col-lg-6"
                                                                                             style="margin-left:-107px;margin-top:-18px">
                                                                                            <select class="ajaxDropdownPartner"
                                                                                                    name="partnersInEvent"
                                                                                                    id="partnersInEvent1"
                                                                                                    multiple
                                                                                                    style="width:420px;height:auto">
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>

                                                                                </div>

                                                                            </div>
                                                                        </div>

                                                                        <br>

                                                                        <input type="hidden" name="appId"
                                                                               value="${app.id}">
                                                                        <div class="row offerDetailsBottomLine"
                                                                             style="border-top:1px solid #423b3a;width:490px;margin-left:-20px;margin-top: -10px">
                                                                            <br>
                                                                            <div class="col-sm-12">
                                                                                <button class="btn btn-<%=UIConstants.primaryColorClass%> btn-round pull-right submitButton"
                                                                                        type="submit"
                                                                                        style="padding:8px 10px;;margin-top:10px;margin-right:30px">
                                                                                    <b style="margin-left:0px">Save
                                                                                        Event</b>&nbsp;
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </form>

                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>

                                                <i class="col-lg-6"
                                                   style="position:absolute;margin-top:-15px">Note:Please
                                                    fill * columns</i>
                                            </div>
                                            <!-- Image loader -->
                                            <div class="mx-auto text-center" id="loader"
                                                 style='display: none;'>
                                                <!--              <h6 class="<%=UIConstants.primaryTextColorClass%>">Loading... </h6>
 --> <img src='../assets/loaders/spiral.gif' width='150px' height='150px'>
                                            </div>
                                            <!-- Image loader -->
                                        </div>

                                    </div>

                                    <!-- 									</div>
    -->
                                </div>


                            </div>


                                    <!-- 									</div>
    -->
                                </div>


                            </div>


                            </div>



                            <div class="card-body" style="background-color:<%=UIConstants.primaryCardBackground%>;">
                                <input type="text" value="" id="myInput" style="opacity: 0">
                                <div id="eventList" class="material-datatables" style="margin-top:-25px">
                                </div>
                            </div>


                        </div>
                    </div>
                </div>


            </div>

            <!-- <footer class="footer">

        </footer> -->
        </div>
    </div>
</div>

<%--Core JS Files--%>

<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<!-- <script src="../assets/js/core/jquery.min.js"></script> -->
<script src="../assets/js/core/popper.min.js"></script>
<script src="../assets/js/core/bootstrap-material-design.min.js"></script>
<script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!-- Plugin for the momentJs  -->
<script src="../assets/js/plugins/moment.min.js"></script>
<!--  Plugin for Sweet Alert -->
<script src="../assets/js/plugins/sweetalert2.js"></script>
<!-- Forms Validations Plugin -->
<script src="../assets/js/plugins/jquery.validate.min.js"></script>
<!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
<script src="../assets/js/plugins/jquery.bootstrap-wizard.js"></script>
<!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script src="../assets/js/plugins/bootstrap-selectpicker.js"></script>
<!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
<script src="../assets/js/plugins/bootstrap-datetimepicker.min.js"></script>
<!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
<script src="../assets/js/plugins/jquery.dataTables.min.js"></script>
<!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
<script src="../assets/js/plugins/bootstrap-tagsinput.js"></script>
<!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="../assets/js/plugins/jasny-bootstrap.min.js"></script>
<!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
<script src="../assets/js/plugins/fullcalendar.min.js"></script>
<!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
<script src="../assets/js/plugins/jquery-jvectormap.js"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="../assets/js/plugins/nouislider.min.js"></script>
<!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
<!-- Library for adding dinamically elements -->
<script src="../assets/js/plugins/arrive.min.js"></script>
<!--  Google Maps Plugin    -->
<%--<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>--%>
<!-- Chartist JS -->
<script src="../assets/js/plugins/chartist.min.js"></script>
<!--  Notifications Plugin    -->
<script src="../assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="../assets/js/material-dashboard.js?v=2.1.0" type="text/javascript"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="../assets/demo/demo.js"></script>
<%--loader JS Files--%>
<script src="../assets/js/loader.js"></script>
<%--Tooltip textArea JS--%>
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote-lite.min.js"></script>
<script src="../dist/js/select2.js"></script>

<script>

    function addEvent() {
        var myform = document.getElementById("addEventForm");
        var fd = new FormData(myform);
        alert("heello");
        event.preventDefault();
        $.ajax({
            url: "/event/saveNewEvent",
            data: fd,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            beforeSend: function () {


            },
            success: function (response) {
                showNotification('top', 'left', 'Event has been created.', 'success');
                getEventList();
                /*getBannerList();*/
                $('#signupModal').modal('hide');
            },
            complete: function () {
                // Hide image container

            },
            error: function (jqXHR) {
                var o = $.parseJSON(jqXHR.responseText);
                window.open('/adminConsole/error?status=' + o.status + '&message=' + o.error, "_self")
            }
        });
    }

    function addPreEvent() {
        var myform = document.getElementById("addPreEventForm");
        var fd = new FormData(myform);
        alert("heello");
        event.preventDefault();
        $.ajax({
            url: "/event/savePreNewEvent",
            data: fd,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            beforeSend: function () {


            },
            success: function (response) {
                showNotification('top', 'left', 'Event has been created.', 'success');
                getEventList();
                /*getBannerList();*/
                $('#signupModal').modal('hide');
            },
            complete: function () {
                // Hide image container

            },
            error: function (jqXHR) {
                var o = $.parseJSON(jqXHR.responseText);
                window.open('/adminConsole/error?status=' + o.status + '&message=' + o.error, "_self")
            }
        });
    }

    function showNotification(from, align, message, color) {

        $.notify({
            icon: "add_alert",
            message: message,
        }, {
            type: color,
            timer: 3000,
            placement: {
                from: from,
                align: align
            }
        });
    }
</script>

<script>
    var perPageRecord = 10;
    $(".ajaxDropdownPartner").select2({
        ajax: {
            url: '/adminConsole/findByPartnerCompany1',
            type: 'GET',
            dataType: 'json',
            delay: 500,
            data: function (params) {
                return {
                    search: params.term, // search term
                    page: params.page || 0,
                    size: perPageRecord//Per page records
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 0;
                return {
                    results: data.results,
                    pagination: {
                        more: params.page < data.count_filtered - 1
                    }
                };
            },
            cache: true

        },
        placeholder: "SELECT PUBLISHER",
        minimumInputLength: 0
    });
</script>

<script>

    var perPageRecord = 10;
    $(".ajaxPreDefineEvent").select2({
        ajax: {
            url: '/event/findByPreDefineEvent',
            type: 'GET',
            dataType: 'json',
            delay: 500,
            data: function (params) {
                return {
                    search: params.term, // search term
                    page: params.page || 0,
                    size: perPageRecord//Per page records
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 0;
                return {
                    results: data.results,
                    pagination: {
                        more: params.page < data.count_filtered - 1
                    }
                };
            },
            cache: true

        },
        placeholder: "SELECT EVENT",
        minimumInputLength: 0
    });

</script>

<script>

    function limitText(limitField, limitNum) {
        if (limitField.value.length > limitNum) {
            limitField.value = limitField.value.substring(0, limitNum);
            Swal.fire({
                title: 'Limit Validation',
                text: "Value should be between 0 - " + limitNum,
                type: 'info',
                confirmButtonText: 'Ok'
            });
        }
    }

    $('select#accessStatus').change(function () {
        alert("yoo ");
        var eventVisibility = $(this).val();
        if (eventVisibility == "NeedApproval")
            $('#partnersInEvent').show();
        else
            $('#partnersInEvent').hide();
    });

</script>

<script>

    function getEventList() {
        $.ajax({
            url: "/event/findByEventAjax?appId=" +${app.id},
            type: "GET",
            dataType: 'json',

            success: function (response) {
                var tableBodyResponse = "";
                tableBodyResponse += '<table  class="display nowrap datatables" style="width:100%">'
                    + '<thead>'
                    + '<tr>'
                    + '<th>Id</th>'
                    + '<th>Event Name</th>'
                    + '<th>Event Count</th>'
                    + '<th>Event Key</th>'
                    + '<th>App ID</th>'
                    + '<th>Action</th>'
                    + '</tr>'
                    + '</thead>'
                    + '<tbody>';
                for (var i = 0; i < response.eventList.length; i++) {

                    tableBodyResponse += '<tr>'
                        + '<td><div style="margin-left:40px"></div>' + response.eventList[i].id + '</td>'
                        + '<td><div></div>' + response.eventList[i].eventName + '</td>'
                        + '<td><div></div>' + response.eventList[i].eventCount + '</td>'
                        + '<td><div></div>' + response.eventList[i].eventKey + '</td>'
                        + '<td><div></div>' + ${app.id} +'</td>'
                        + '<td><a href="#" onclick="editEventInfo(' + response.eventList[i].id + ')" data-toggle="modal" data-target="#addEvent">'
                        + '<i class="material-icons iconTable">edit</i></a>'
                        + '<a href="#" onclick="deleteEvent(' + response.eventList[i].id + ')" style="margin-left:5px"><i class="material-icons iconTable" style="background-color:red;x">close</i></a></div></td>'
                        + '</tr>'
                }
                tableBodyResponse += '</tbody>'
                    + '</table>';
                document.getElementById("eventList").innerHTML = tableBodyResponse;
                var dataTableVal = ".datatables";
                datatables(dataTableVal);
            },

            error: function (jqXHR) {
                var o = $.parseJSON(jqXHR.responseText);
                window.open('/adminConsole/error?status=' + o.status + '&message=' + o.error, "_self")
            }
        });
    }

    function datatables(dataTableVal) {
        $(dataTableVal).DataTable({
            "sPagingType": "full_number",
            "lengthMenu": [
                [10, 20, -1],
                [10, 20, "All"]
            ],
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
            }
        });
        jQuery('.dataTable').wrap('<div class="dataTables_scroll" />');
        var table = $(dataTableVal).DataTable();

        // Edit record
        table.on('click', '.edit', function () {
            $tr = $(this).closest('tr');
            var data = table.row($tr).data();
            alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
        });

        // Delete a record
        table.on('click', '.remove', function (e) {
            $tr = $(this).closest('tr');
            table.row($tr).remove().draw();
            e.preventDefault();
        });

        //Like record
        table.on('click', '.like', function () {
            alert('You clicked on Like button');
        });
    }

    function editEventInfo(e) {
        document.getElementById("modalHeading").innerHTML = "Edit Event";
        $.ajax({
            url: "/event/getEventById/" + e,
            type: "GET",
            dataType: 'json',
            beforeSend: function () {

            },
            success: function (response) {
                document.getElementById("eventId").value = e;
                document.getElementById("eventName").value = response.event.eventName;
                /*$(":input").change();*/
            },
            complete: function () {
                // Hide image container

            },
            error: function (jqXHR) {
                var o = $.parseJSON(jqXHR.responseText);
                window.open('/adminConsole/error?status=' + o.status + '&message=' + o.error, "_self")
            }
        });
    }

    $("#newEvent").on("click", function () {
        document.getElementById("modalHeading").innerHTML = "Create Employee";
        $('#saveEvent')[0].reset();
    });

    function deleteEvent(e) {
        toggleSwitchAjax("delete", "delete", "/event/deleteEventById/", e, " Event!");
    }

    function toggleSwitchAjax(toggleStatus, confirmMessage, url, e, msg) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You want to " + toggleStatus + msg,
            type: 'info',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then((result) => {
            if (result.value) {
                Swal.fire({
                    title: 'Successfully ' + confirmMessage,
                    type: 'success',
                    confirmButtonText: 'OK!'
                });
                $.ajax({
                    url: url + e,
                    contentType: 'json',
                    type: 'GET',
                    success: function (response) {
                        setTimeout(function () {
                            window.location.reload();
                        }, 2000);
                    },
                    complete: function () {
                        // Hide image container
                        $("#loaderBulk").hide();
                        $("#modalFormBulk").show();

                    },

                    error: function (jqXHR) {
                        var o = $.parseJSON(jqXHR.responseText);
                        window.open('/adminConsole/error?status=' + o.status + '&message=' + o.error, "_self")
                    }
                });
            }
        });
    }


    function myFunction() {
        var x = document.getElementById("preDefined");
        var y = document.getElementById("userDefined");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }

        if (y.style.display === "none") {
            y.style.display = "block";
        } else {
            y.style.display = "none";
        }
    }


</script>
</body>

</html>