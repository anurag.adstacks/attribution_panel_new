<%@ page import="java.util.List" %>
<%@ page import="com.attribution.panel.constants.UIConstants" %>
<%@ page import="com.attribution.panel.bean.Event" %>
<%@ page import="com.attribution.panel.bean.Country" %>
<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html lang="en">

<head>
    <meta charset="utf-8"/>
    <link rel="apple-touch-icon" sizes="76x76" href="../../assets/img/apple-icon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <link rel="icon" type="image/png" href="../assets/img/logoAttribution.png">
    <title>
        User Event
    </title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport'/>
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- CSS Files -->
    <link href="../../assets/css/material-dashboard.css?v=2.1.0" rel="stylesheet"/>
    <link href="../assets/css/style-sheet-attribution.css?v=2.1.0" rel="stylesheet"/>
    <%--loader CSS Files--%>
    <link href="../assets/css/loader.css" rel="stylesheet"/>
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="../../assets/demo/demo.css" rel="stylesheet"/>
    <link rel="stylesheet" href="../assets/fastselect.min.css">
    <!-- Responsive -->
    <link href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="../dist/css/select2.css" rel="stylesheet"/>

    <link rel="stylesheet" type="text/css" href="../assets/css/daterangepicker.css"/>
    <style>
        td, th {
            text-align: center;
        }
        td {
            border: 1px solid #615f5c;
        }
        th{
            border-top: 3px solid #615f5c;
            border-left: 1px solid #615f5c;
            border-right: 1px solid #615f5c;
        }
        th:last-child, td:last-child {
            border-right: none;
        }
        th:first-child, td:first-child {
            border-left: none;
        }
    </style>


</head>

<body class="fixed-left blue_dark drawer drawer--right " data-theme='blue_dark'
      style="background-color:<%=UIConstants.primaryBackgroundColor%>;font-family:<%=UIConstants.primaryFontFamily%>;">
<div class="se-pre-con"></div>
<% String url = request.getContextPath(); %>
<div class="wrapper ">
    <jsp:include page="integratedSidebar.jsp">
        <jsp:param value="active" name="allEvent"/>
    </jsp:include>
    <div class="main-panel">
        <jsp:include page="adminHeader.jsp"/>
        <%-- <jsp:include page="adminHeader.jsp"/>--%>
        <!-- Navbar -->
        <%--<jsp:include page="adminHeader.jsp"/>--%>

        <!-- End Navbar -->
        <div class="content">
            <div class="container-fluid">
                <div class="cardDiv">
                    <div class="cardSize">
                        <i class="material-icons iconSize">assignment</i>
                    </div>
                    <h2 class="cardHeading <%=UIConstants.primaryTextColorClass%>" style="-moz-transform: scale(0.98);">Event Report</h2>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card <%=UIConstants.primaryTextColorClass%>"
                             style="background-color:<%=UIConstants.primaryCardBackground%>;font-family:<%=UIConstants.primaryFontFamily%>">
                            <div class="card-header card-header-<%=UIConstants.primaryColorClass%> card-header-icon responsiveButton">
                                <div class="row" style="margin-right: 220px;margin-top: 3px">
                                    <div class="col-sm-12">
                                        <button class="filterButton pull-right" data-toggle="modal"
                                                data-target="#eventReportSearch"
                                                style="margin-top:-40px;-moz-transform: scale(0.92);">
                                            <i class="fa fa-plus" aria-hidden="true" data-toggle="modal"
                                               data-target="#eventReportSearch"
                                               style="width:auto;font-size:15px;margin-left:0px;"><b
                                                    style="margin-left:6px;">Search</b></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="row pull-right" style="margin-top:-3px;margin-right:-30px">
                                    <div class="col-sm-7" style="">
                                        <!-- <i class="fa fa-caret-down"></i> -->
                                        <div class="<%=UIConstants.primaryTextColorClass%>" id="reportrange"
                                             style="margin-top: -40px;">
                                            <input readonly type="text"
                                                   class="<%=UIConstants.primaryTextColorClass%>"
                                                   id="reportrangeinput"
                                                   name="daterange"
                                                   style="cursor: pointer; background-color: <%=UIConstants.primaryBackgroundColor%>; border:1px solid <%=UIConstants.borderColor%>; width:240px;padding:3px 6px;font-size:12px;">
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="modal fade" id="eventReportSearch" tabindex="-1" role="dialog" aria-hidden="true"
                                 data-backdrop="static">
                                <div class="modal-dialog " role="document">
                                    <div class="card modal-content <%=UIConstants.primaryTextColorClass%>"
                                         style="background-color:<%=UIConstants.primaryModalCardHeaderBackground%>;width:420px">
                                        <div class="modalClose">
                                            <i class="fa fa-times" data-dismiss="modal" aria-label="Close"></i>
                                        </div>
                                        <div class="card-header card-header-<%=UIConstants.primaryColorClass%> card-header-icon"
                                             style="height:70px">
                                            <div class="cardDiv">
                                                <div class="cardSize">
                                                    <i class="material-icons iconSize" style="margin-left: 4px;margin-top: 5px">assignment</i>
                                                </div>
                                                <h4 class="card-title <%=UIConstants.primaryTextColorClass%>"
                                                    style="font-family: <%=UIConstants.primaryFontFamily%>; font-weight: bold;margin-left:58px">
                                                    Search Event Report</h4>
                                                <p class="card-category" style="margin-left:60px">Enter Report Information</p>
                                            </div>
                                        </div>
                                        <div class="card-body"
                                             style="font-family: <%=UIConstants.primaryFontFamily%>; font-weight: bold;background-color:<%=UIConstants.primaryCardModalBackground%>;width:420px;">
                                            <p>${message}</p>
                                            <br>
                                            <div style="">
                                                <div class="row" style="margin-top: -70px;margin-left: -1px">
                                                    <div class="col-md-12">
                                                        <label class="bmd-label-floating <%=UIConstants.primaryTextColorClass%>"
                                                               style="margin-left:0px;margin-top:48px;font-size:15px;">Select Event:</label>
                                                        <div style="margin-top:-35px;margin-left:100px">
                                                            <div class="dropdown bootstrap-select show-tick show <%=UIConstants.primaryTextColorClass%>"
                                                                 style="z-index: 5">
                                                                <select class="selectEvent" multiple style="width:270px;color:white;z-index: 5"
                                                                        id="eventIds">
                                                                    <%
                                                                        List<Event> eventList = (List<Event>) request.getAttribute("eventList");
                                                                        for (Event event : eventList) {
                                                                    %>
                                                                    <option value="<%=event.getId()%>">(<%=event.getId()%>) <%=event.getEventName()%>
                                                                    </option>
                                                                    <%}%>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row" style="margin-left:-3px;margin-top: -40px">
                                                    <div class="col-md-12">
                                                        <label class="bmd-label-floating <%=UIConstants.primaryTextColorClass%>"
                                                               style="margin-left:-13px;margin-top:48px;font-size:15px;">Select
                                                            Country:</label>
                                                        <div style="margin-top:-35px;margin-left:103px">
                                                            <select class="ajaxCountry" multiple style="width:270px;color:white"
                                                                    id="countriesCode">
                                                                <%
                                                                    List<Country> countriesList = (List<Country>) request.getAttribute("countries");
                                                                    for (Country country : countriesList) {
                                                                %>
                                                                <option value="<%=country.getShortName()%>">(<%=country.getShortName()%>) <%=country.getName()%>
                                                                </option>
                                                                <%}%>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row" style="margin-top:-40px;margin-left: 10px">
                                                    <div class="col-md-12">
                                                        <label class="bmd-label-floating <%=UIConstants.primaryTextColorClass%>"
                                                               style="margin-left:-13px;margin-top:48px;font-size:15px;">Select Device:</label>
                                                    <div style="margin-top:-35px;margin-left:103px">
                                                        <input type="text" class="inputText" value="" id="searchUuid" style="color:<%=UIConstants.textColor%>;margin-left:-15px;width:270px">

                                                    </div></div>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row"
                                                 style="border-top:1px solid #423b3a;width:420px;margin-left:-20px;margin-top: -10px">
                                                <div class="col-sm-12">
                                                    <button type="submit" class="submitButton pull-right"
                                                            id="searchEventReport" style="margin-right:20px;margin-top:15px">
                                                        Search
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Image loader -->
                                        <div class="mx-auto text-center" style='display: none;margin-top:-35px'>
                                            <!--              <h6 class="<%=UIConstants.primaryTextColorClass%>">Loading... </h6>
 --> <img src='../assets/loaders/colorfulSpiralSpinner.gif' width='150px' height='150px'>

                                        </div>
                                        <div style="margin-top:-30px !important;">
                                            <i style="background-color:<%=UIConstants.primaryCardModalBackground%>;"></i>
                                        </div>


                                        <!-- Image loader -->
                                    </div>

                                </div>
                                <!-- 									</div>
                -->
                            </div>
                            <div class="card-body">
                                <div class="table-responsive" style="margin-top:6px">
                                    <table id="datatables" class="display nowrap" cellspacing="0" width="100%"
                                           style="width:100%;background-color:<%=UIConstants.primaryCardBackground%>">
                                        <thead>
                                        <tr class="<%=UIConstants.primaryTextColorClass%>" style="font-weight: bold;">
                                            <th>S.No.</th>
                                            <th>Event Name</th>
                                            <th>Date</th>
                                            <th>Country</th>
                                            <th>IP</th>
                                            <th>Device</th>
                                            <th>OS</th>
                                            <th>Google_aid</th>
                                            <th>param1</th>
                                            <th>param2</th>
                                            <th>param3</th>
                                            <th>param4</th>
                                            <th>param5</th>
                                            <th>param6</th>
                                            <th>param7</th>
                                            <th>param8</th>
                                            <th>param9</th>
                                            <th>param10</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>

                            </div>


                        </div>
                    </div>
                </div>


            </div>

            <!-- <footer class="footer">

        </footer> -->
        </div>
    </div></div>
<!--   Core JS Files   -->
<script src="../../assets/js/core/jquery.min.js"></script>
<script src="../../assets/js/core/popper.min.js"></script>
<script src="../../assets/js/core/bootstrap-material-design.min.js"></script>
<script src="../../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!-- Plugin for the momentJs  -->
<script src="../../assets/js/plugins/moment.min.js"></script>
<!--  Plugin for Sweet Alert -->
<script src="../../assets/js/plugins/sweetalert2.js"></script>
<!-- Forms Validations Plugin -->
<script src="../../assets/js/plugins/jquery.validate.min.js"></script>
<!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
<script src="../../assets/js/plugins/jquery.bootstrap-wizard.js"></script>
<!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script src="../../assets/js/plugins/bootstrap-selectpicker.js"></script>
<!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
<script src="../../assets/js/plugins/bootstrap-datetimepicker.min.js"></script>
<!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
<script src="../assets/js/plugins/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../assets/js/jquery.spring-friendly.js"></script>
<!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
<script src="../../assets/js/plugins/bootstrap-tagsinput.js"></script>
<!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="../../assets/js/plugins/jasny-bootstrap.min.js"></script>
<!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
<script src="../../assets/js/plugins/fullcalendar.min.js"></script>
<!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
<script src="../../assets/js/plugins/jquery-jvectormap.js"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="../../assets/js/plugins/nouislider.min.js"></script>
<!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
<!-- Library for adding dinamically elements -->
<script src="../../assets/js/plugins/arrive.min.js"></script>
<!-- Chartist JS -->
<script src="../../assets/js/plugins/chartist.min.js"></script>
<!--  Notifications Plugin    -->
<script src="../../assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="../../assets/js/material-dashboard.js?v=2.1.0" type="text/javascript"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="../../assets/demo/demo.js"></script>
<script src="../assets/fastselect.standalone.js"></script>
<%--fixedheader--%>
<script src="https://cdn.datatables.net/fixedheader/3.1.6/js/dataTables.fixedHeader.min.js"
        type="text/javascript"></script>
<%--loader JS Files--%>
<script src="../assets/js/loader.js"></script>
<script src="../dist/js/select2.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<script>
    var table = $('#datatables').DataTable({
        ajax: '/event/getUserEventAjax?id=${appId}',
        serverSide: true,
        scrollX: true,
        processing: true,
        columns: [
            {
                data: 'id',
                orderable: false,
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            {
                data: 'eventName',
                orderable: false,
                render: function (data) {
                    return '<div style="margin-left:140px"></div>' + data;
                }
            },
            {
                data: 'creationDateTime',
                render: function (data, type, row) {
                    return '<div style="margin-left:100px"></div>'+moment(data).format('DD/MM/YYYY');
                }
            },
            {
                data: 'country',
                orderable: false,
                render: function (data) {
                    return '<div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'ip',
                orderable: false,
                render: function (data) {
                    return '<div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'deviceName',
                orderable: false,
                render: function (data) {
                    return '<div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'os',
                orderable: false,
                render: function (data) {
                    return '<div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'google_aid',
                orderable: false,
                render: function (data) {
                    return '<div style="margin-left:140px"></div>' + data;
                }
            },
            {
                data: 'param1',
                orderable: false,
                render: function (data) {
                    return '<div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'param2',
                orderable: false,
                render: function (data) {
                    return '<div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'param3',
                orderable: false,
                render: function (data) {
                    return '<div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'param4',
                orderable: false,
                render: function (data) {
                    return '<div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'param5',
                orderable: false,
                render: function (data) {
                    return '<div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'param6',
                orderable: false,
                render: function (data) {
                    return '<div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'param7',
                orderable: false,
                render: function (data) {
                    return '<div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'param8',
                orderable: false,
                render: function (data) {
                    return '<div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'param9',
                orderable: false,
                render: function (data) {
                    return '<div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'param10',
                orderable: false,
                render: function (data) {
                    return '<div style="margin-left:120px"></div>' + data;
                }
            }
        ],
        "scrollCollapse": true,
        scrollY: 620,
        "order": [[ 2, "desc" ]],
        "language": {

            "processing": "<img src='/assets/img/loader.gif'  width='100px' height='auto'/>"
        }
    });
    $(window).bind('resize', function (e) {
        if (window.RT) clearTimeout(window.RT);
        window.RT = setTimeout(function () {
            table.ajax.reload(); /* false to get page from cache */
        }, 250);
    });
    var dateRange;
    $(function () {
        var start = moment().subtract(6, 'days').startOf('day');
        var end = moment().endOf('day');

        function cb(start, end) {
            document.getElementById("reportrangeinput").value = start.format('YYYY-MM-DD HH:mm:ss') + ' - ' + end.format('YYYY-MM-DD HH:mm:ss');
            dateRange = document.getElementById("reportrangeinput").value;
            /*Populating dates in Counts boxes*/
            table.column(4).search(dateRange).draw();
        }

        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'Today': [moment().startOf('day'), moment().endOf('day')],
                'Yesterday': [moment().subtract(1, 'days').startOf('day'), moment().subtract(1, 'days').endOf('day')],
                'Last 7 Days': [moment().subtract(6, 'days').startOf('day'), moment().endOf('day')],
                'Last 30 Days': [moment().subtract(29, 'days').startOf('day'), moment().endOf('day')],
                'This Month': [moment().startOf('month').startOf('day'), moment().endOf('month').endOf('day')],
                'Last Month': [moment().subtract(1, 'month').startOf('month').startOf('day'), moment().subtract(1, 'month').endOf('month').endOf('day')]
            }
        }, cb);

        cb(start, end);
    });
    $('.selectEvent').select2({
        dropdownParent: "#eventReportSearch"
    });
    $('.ajaxCountry').select2({
        dropdownParent: "#eventReportSearch"
    });
    var eventIds = '';
    $('select#eventIds').change(function () {
        eventIds = '';
        $('select#eventIds option:selected').each(function () {
            eventIds += $(this).val() + ",";
        });
        eventIds = eventIds.substring(0, eventIds.length - 1);
    });
    var countriesCode = '';
    $('select#countriesCode').change(function () {
        countriesCode = '';
        $('select#countriesCode option:selected').each(function () {
            countriesCode += $(this).val() + ",";
        });
        countriesCode = countriesCode.substring(0, countriesCode.length - 1);
    });

    $('#searchEventReport').click(function () {
        table.column(1).search(eventIds).draw();
        table.column(3).search(countriesCode).draw();
        table.column(7).search(document.getElementById("searchUuid").value).draw();
        $('#eventReportSearch').modal('hide');
    });
</script>
</body>

</html>