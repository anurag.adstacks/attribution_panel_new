<%@ page import="com.attribution.panel.constants.UIConstants" %>
<!DOCTYPE html>
<%--<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<html lang="en">
<head>
    <meta http-equiv="refresh" content="86400; url=/login?logout">
    <style>
        :root {
            --primaryButtonColor: <%=UIConstants.primaryButtonColor%>;
            --borderColor: <%=UIConstants.borderColor%>;
            --textColor: <%=UIConstants.textColor%>;
            --arrowColor: <%=UIConstants.arrowColor%>;
            --hoverButtonBgColor: <%=UIConstants.hoverButtonBgColor%>;
            --dropDownHoverColor: <%=UIConstants.dropDownHoverColor%>;
            --dropDownSelectedColor: <%=UIConstants.dropDownSelectedColor%>;
            --dropDownMenuColor: <%=UIConstants.dropDownMenuColor%>;
            --paginatehoverTextColor: <%=UIConstants.paginateHoverTextColor%>;
            --paginatehoverBgColor: <%=UIConstants.paginateHoverBgColor%>;
            --tableIconBgColor: <%=UIConstants.tableIconBgColor%>;
            --tableIconTextColor: <%=UIConstants.tableIconTextColor%>;
            --primaryBackgroundColor: <%=UIConstants.primaryBackgroundColor%>;
            --primaryCardBackground: <%=UIConstants.primaryCardBackground%>;
            --bannerTextColor: <%=UIConstants.bannerTextColor%>;
            --bannerBgColor: <%=UIConstants.bannerBgColor%>;
            --sideBgColor: <%=UIConstants.sideBgColor%>;
            --scrollbarBgColor: <%=UIConstants.scrollbarBgColor%>;
            --primaryButtonTextColor: <%=UIConstants.primaryButtonTextColor%>;
            --chartTextColor: <%=UIConstants.chartTextColor%>;
            --chartBoxBgColor: <%=UIConstants.chartBoxBgColor%>;
            --primaryBootstrapColor: <%=UIConstants.primaryBootstrapColor%>;
            --dataPickerBgColor: <%=UIConstants.dataPickerBgColor%>;
        }

        .sidebar .sidebar-wrapper .active a {
            font-size: 13px !important;
            -moz-box-shadow: 1px 1px 3px 2px #ffffff;
            -webkit-box-shadow: 1px 1px 3px 2px #ffffff;
            box-shadow: 1px 1px 3px 2px #ffffff;
        }

        .sidebar .sidebar-wrapper .active i {
            color: var(--primaryButtonTextColor) !important;
        }
    </style>
    <script src="https://cdn.branch.io/assets/b1499015/static/common/lib.js?b1499015" type="text/javascript"></script>
    <script src="https://cdn.branch.io/assets/b1499015/static/main/prep.js?b1499015" type="text/javascript"></script>
    <script src="https://cdn.branch.io/assets/b1499015/static/main/app.js?b1499015" type="text/javascript"></script>
</head>


<div class="sidebar" data-color="sidebarColor" data-background-color="black"
     style="font-family:<%=UIConstants.primaryFontFamily%>;height:100%;">
    <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
    <div class="logo">
        <a href="#" class="simple-text logo-normal" style="height:60px">
            <p style="margin-left:44px;margin-top:2px"><%--<img src="../../assets/img/adja.png" style="width:50px;height:50px">--%>
                <b style="font-size:35px;font-family:'Times New Roman', Times, serif;text-transform: capitalize;margin-left:6px;margin-top:17px;position:absolute;">Attribution</b>
            </p>
        </a>
    </div>
    <div class="sidebar-wrapper" style="height:90%">
        <ul class="nav">
            <%--<li class="nav-item ${param.dash}">
                <a class="nav-link" href="${contextPath}/adminConsole/dashboard">
                    <i class="material-icons">dashboard</i>
                    <p>Dashboard</p>
                </a>
            </li>--%>
            <%--<sec:authorize access="hasRole('ADMIN') or hasRole('ADV_ACCESS')">--%>
            <li class="nav-item ${param.addApps}">
                <a class="nav-link" href="${contextPath}/adminConsole/addApp">
                    <i class="material-icons">content_paste</i>
                    <p>Add App</p>
                </a>
            </li>
            <%--</sec:authorize>--%>

            <li class="nav-item ${param.apps}">
                <a class="nav-link" href="${contextPath}/adminConsole/appRedirect">
                    <i class="material-icons">content_paste</i>
                    <p>Apps</p>
                </a>
            </li>

            <li class="nav-item ${param.configurations}">
                <a class="nav-link" href="${contextPath}/adminConsole/getConfiguration">
                    <i class="material-icons">content_paste</i>
                    <p>Configuration</p>
                </a>
            </li>

                <li class="nav-item ${param.allEvent}">
                    <a class="nav-link" href="${contextPath}/adminConsole/getpubs">
                        <i class="fa fa-address-card" aria-hidden="true" style="color:#FFFFFF"></i>
                        <p style="color:#FFFFFF;font-weight: bold">Partners</p>
                    </a>
                </li>

                <li class="nav-item ${param.clickLogs}">
                    <a class="nav-link" href="${contextPath}/adminConsole/getReport">
                        <i class="fa fa-address-card" aria-hidden="true" style="color:#FFFFFF"></i>
                        <p style="color:#FFFFFF;font-weight: bold">Reports</p>
                    </a>
                </li>

            <%--<li class="side-nav__category"><p style="color: whitesmoke; margin-left: 10px;"><b> Channels &amp;
                Links </b></p></li>

            <li class="nav-item ">
                <a class="nav-link" data-toggle="collapse" href="#apps">
                    <i class="material-icons">content_paste</i>
                    <p> Ads
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse ${param.appsTab}" id="apps">
                    <ul class="nav">
                        <li class="nav-item ${param.apps}">
                            <a class="nav-link" href="${contextPath}/adminConsole/getapps">
                                <span class="sidebar-mini"> AL </span>
                                <span class="sidebar-normal"> Analytics </span>
                            </a>
                        </li>

                        <li class="nav-item ${param.categories}">
                            <a class="nav-link" href="${contextPath}/category/getCategories">
                                <span class="sidebar-mini"> PM </span>
                                <span class="sidebar-normal"> Partner Management </span>
                            </a>
                        </li>

                        <li class="nav-item ${param.categories}">
                            <a class="nav-link" href="${contextPath}/category/getCategories">
                                <span class="sidebar-mini"> LK </span>
                                <span class="sidebar-normal"> Links </span>
                            </a>
                        </li>

                        <li class="nav-item ${param.categories}">
                            <a class="nav-link" href="${contextPath}/category/getCategories">
                                <span class="sidebar-mini"> FD </span>
                                <span class="sidebar-normal"> Fraud </span>
                            </a>
                        </li>

                        <li class="nav-item ${param.categories}">
                            <a class="nav-link" href="${contextPath}/category/getCategories">
                                <span class="sidebar-mini"> PM </span>
                                <span class="sidebar-normal"> SKSdNetwork </span>
                            </a>
                        </li>

                    </ul>
                </div>
            </li>

            <li class="nav-item ${param.aff}">
                <a class="nav-link" href="${contextPath}/adminConsole/getpubs">
                    <i class="material-icons">library_books</i>
                    <p>Journey</p>
                </a>
            </li>

            <li class="nav-item ${param.aff}">
                <a class="nav-link" href="${contextPath}/adminConsole/getpubs">
                    <i class="material-icons">library_books</i>
                    <p>Email</p>
                </a>
            </li>

            <li class="nav-item ${param.aff}">
                <a class="nav-link" href="${contextPath}/adminConsole/getpubs">
                    <i class="material-icons">library_books</i>
                    <p>Quick Links</p>
                </a>
            </li>

            <li class="side-nav__category"><p style="color: whitesmoke; margin-left: 10px;"><b> CROSS-CHANNEL
                ANALYTICS </b></p></li>

            <li class="nav-item ${param.aff}">
                <a class="nav-link" href="${contextPath}/adminConsole/getpubs">
                    <i class="material-icons">library_books</i>
                    <p>Sources</p>
                </a>
            </li>

            <li class="side-nav__category"><p style="color: whitesmoke; margin-left: 10px;"><b> EXPORTS
            </b></p></li>

            <li class="nav-item ${param.aff}">
                <a class="nav-link" href="${contextPath}/adminConsole/getpubs">
                    <i class="material-icons">library_books</i>
                    <p>Data Feeds</p>
                </a>
            </li>

            <li class="nav-item ${param.aff}">
                <a class="nav-link" href="${contextPath}/adminConsole/getpubs">
                    <i class="material-icons">library_books</i>
                    <p>Daily Exports</p>
                </a>
            </li>

            <li class="nav-item ${param.aff}">
                <a class="nav-link" href="${contextPath}/adminConsole/getpubs">
                    <i class="material-icons">library_books</i>
                    <p>Custom Exports</p>
                </a>
            </li>

            <li class="side-nav__category"><p style="color: whitesmoke; margin-left: 10px;"><b> CONFIGURE
            </b></p></li>

            <li class="nav-item ${param.aff}">
                <a class="nav-link" href="${contextPath}/adminConsole/getpubs">
                    <i class="material-icons">library_books</i>
                    <p>Configuration</p>
                </a>
            </li>

            <li class="nav-item ${param.aff}">
                <a class="nav-link" href="${contextPath}/adminConsole/getpubs">
                    <i class="material-icons">library_books</i>
                    <p>Account Settings</p>
                </a>
            </li>

            <li class="nav-item ${param.aff}">
                <a class="nav-link" href="${contextPath}/adminConsole/getpubs">
                    <i class="material-icons">library_books</i>
                    <p>Integration Status</p>
                </a>
            </li>

            <li class="nav-item ${param.aff}">
                <a class="nav-link" href="${contextPath}/adminConsole/getpubs">
                    <i class="material-icons">library_books</i>
                    <p>Test Devices</p>
                </a>
            </li>

            <li class="side-nav__category"><p style="color: whitesmoke; margin-left: 10px;"><b> TOOLS
            </b></p></li>

            &lt;%&ndash;<sec:authorize access="hasRole('ADMIN') or hasRole('PUB_ACCESS')">&ndash;%&gt;
            <li class="nav-item ${param.aff}">
                <a class="nav-link" href="${contextPath}/adminConsole/getpubs">
                    <i class="material-icons">library_books</i>
                    <p>Publishers</p>
                </a>
            </li>--%>

        </ul>
    </div>
</div>

</html>