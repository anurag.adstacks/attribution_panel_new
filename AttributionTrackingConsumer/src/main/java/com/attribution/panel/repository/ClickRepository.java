package com.attribution.panel.repository;

import com.attribution.panel.beans.AttributionClick;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClickRepository extends ElasticsearchRepository<AttributionClick, String> {

//    List<AttributionClick> findByIpLikeAndOfferIdOrderByCreationDateTimeDesc(String ip, Long offerId);
//    List<AttributionClick> findByOfferIdOrderByCreationDateTimeDesc(Long offerId);
//    List<AttributionClick> findByAgentAndIpAndOfferIdOrderByCreationDateTimeDesc(String agent, String ip, Long offerId);
//
//    List<AttributionClick> findByDeviceIdAndIpAndOfferIdOrderByCreationDateTimeDesc(AttributionDeviceId deviceId, String ip, Long offerId);
//
//    List<AttributionClick> findByIpAndOfferIdOrderByCreationDateTimeDesc(String s, long l);


//    List<Click> findByOfferIdAndGaidOrderByCreationDateTimeDesc(Long offerId, String gaid);//Not supported in es
}

