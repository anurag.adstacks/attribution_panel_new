//package com.attribution.panel.repository;
//
//import com.etreetech.panel.beans.CampaignHourlyRecord;
//import com.etreetech.panel.beans.CampaignHourlyRecordId;
//import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
//import org.springframework.stereotype.Repository;
//
//import java.util.List;
//
//@Repository
//public interface CampaignHourlyRecordRepository extends ElasticsearchRepository<CampaignHourlyRecord, CampaignHourlyRecordId> {
//
//
//    List<CampaignHourlyRecord> findById_SubAffiliate(String subAff);
//}
//
