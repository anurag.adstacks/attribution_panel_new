//package com.attribution.panel.repository;
//
//import com.etreetech.panel.beans.Conversion;
//import com.etreetech.panel.enums.ConversionStatus;
//import org.springframework.data.domain.Page;
//import org.springframework.data.domain.Pageable;
//import org.springframework.data.domain.Sort;
//import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
//import org.springframework.stereotype.Repository;
//
//import java.util.List;
//
//@Repository
//public interface ConversionRepository extends ElasticsearchRepository<Conversion, String> {
//
//    Conversion findByUuid(String uuid);
//
////	Conversion findByClickId(String id);
//
////	List<Conversion> findByClickId(String id);
//
////	List<Conversion> findByEventAndClick(EventDTO event, Click click);
//
//    List<Conversion> findByEvent_IdAndClickId(Long eventId, String clickId);
//
//    Page<Conversion> findByCreationDateTimeBetween(String startDate1, String endDate1, Pageable pageable);
//
//    //	@Query()
//    List<Conversion> findByClickId(String clickId, Sort sort);
//    List<Conversion> findByClickId(String clickId);
//    List<Conversion> findByClickIdOrderByCreationDateTimeDesc(String clickId);
//
//    Long countByConversionStatusNotAndOffer_OfferIdAndPublisher_PublisherId(ConversionStatus conversionStatus, Long offerId, Long publisherId);
//
//    Long countByConversionStatusNotAndOffer_OfferIdAndPublisher_PublisherIdAndCreationDateTimeBetween
//            (ConversionStatus conversionStatus, Long offerId, Long publisherId,String startDate,String endDate);
//
//    Long countByOffer_OfferIdAndPublisher_PublisherId(Long offerId, Long publisherId);
//
//    Long countByOffer_OfferIdAndPublisher_PublisherIdAndCreationDateTimeBetween(Long offerId, Long publisherId,String startDate,String endDate);
//
//    void deleteByUuid(String uuid);
//
//    void deleteByClickId(String clickId);
//
//    Conversion findFirstByClickIdOrderByCreationDateTimeDesc(String clickId);
//
//    Long countByConversionStatusNotAndOffer_OfferIdAndPublisher_PublisherIdAndEvent_Id(ConversionStatus conversionStatus, Long id, Long id1, Long eventId);
//
//    Long countByConversionStatusNotAndOffer_OfferIdAndPublisher_PublisherIdAndEvent_IdAndCreationDateTimeBetween
//            (ConversionStatus conversionStatus, Long id, Long id1, Long eventId,String startDate,String endDate);
//
//
//    Long countByOffer_OfferIdAndPublisher_PublisherIdAndEvent_Id(Long id, Long id1, Long eventId);
//
//    Long countByOffer_OfferIdAndPublisher_PublisherIdAndEvent_IdAndCreationDateTimeBetween(Long id, Long id1, Long eventId,
//                                                                                            String startDate,String endDate);
//
//    Float countByConversionStatusAndOffer_OfferIdAndPublisher_PublisherId(ConversionStatus approved, Long id, Long id1);
//
//    Float countByConversionStatusAndOffer_OfferIdAndPublisher_PublisherIdAndCreationDateTimeBetween(ConversionStatus approved, Long id, Long id1,
//                                                                                                    String startDate,String endDate);
//}
