package com.attribution.panel.config;


import com.attribution.panel.dto.*;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;

@Configuration
@RequiredArgsConstructor
public class KafkaConfig {



    private final KafkaProperties kafkaProperties;
//    private final ConsumerApplicationProperties consumerApplicationProperties;

//
//    @Bean
//    public NewTopic adviceTopic() {
//        return new NewTopic("click", 3, (short) 1);
//    }
//
//    @Bean
//    public NewTopic adviceTopic2() {
//        return new NewTopic("conversion", 3, (short) 1);
//    }
//
//    @Bean
//    public NewTopic adviceTopic3() {
//        return new NewTopic("report", 3, (short) 1);
//    }



    @Bean
    public ConsumerFactory<String, AttributionClickTrackingDTO> consumerFactory() {
        final JsonDeserializer<AttributionClickTrackingDTO> jsonDeserializer = new JsonDeserializer<>();
        jsonDeserializer.addTrustedPackages("*");
        return new DefaultKafkaConsumerFactory<>(
                kafkaProperties.buildConsumerProperties(), new StringDeserializer(), jsonDeserializer
        );
    }
    @Bean
    public ConsumerFactory<String, ConversionMapDTO> consumerFactoryConv() {
        final JsonDeserializer<ConversionMapDTO> jsonDeserializer = new JsonDeserializer<>();
        jsonDeserializer.addTrustedPackages("*");
        return new DefaultKafkaConsumerFactory<>(
                kafkaProperties.buildConsumerProperties(), new StringDeserializer(), jsonDeserializer
        );
    }
    @Bean
    public ConsumerFactory<String, AttributionAllReportTrackingDTO> consumerFactoryReport() {
        final JsonDeserializer<AttributionAllReportTrackingDTO> jsonDeserializer = new JsonDeserializer<>();
        jsonDeserializer.addTrustedPackages("*");
        return new DefaultKafkaConsumerFactory<>(
                kafkaProperties.buildConsumerProperties(), new StringDeserializer(), jsonDeserializer
        );
    }
    @Bean
    public ConsumerFactory<String, ImpressionTrackingDTO> consumerFactoryImpression() {
        final JsonDeserializer<ImpressionTrackingDTO> jsonDeserializer = new JsonDeserializer<>();
        jsonDeserializer.addTrustedPackages("*");
        return new DefaultKafkaConsumerFactory<>(
                kafkaProperties.buildConsumerProperties(), new StringDeserializer(), jsonDeserializer
        );
    }
    @Bean
    public ConsumerFactory<String, InstallReportDTO> consumerFactoryInstallReport() {
        final JsonDeserializer<InstallReportDTO> jsonDeserializer = new JsonDeserializer<>();
        jsonDeserializer.addTrustedPackages("*");
        return new DefaultKafkaConsumerFactory<>(
                kafkaProperties.buildConsumerProperties(), new StringDeserializer(), jsonDeserializer
        );
    }
    @Bean
    public ConsumerFactory<String, DailyActiveUsersDTO> consumerFactoryDailyActiveUsers() {
        final JsonDeserializer<DailyActiveUsersDTO> jsonDeserializer = new JsonDeserializer<>();
        jsonDeserializer.addTrustedPackages("*");
        return new DefaultKafkaConsumerFactory<>(
                kafkaProperties.buildConsumerProperties(), new StringDeserializer(), jsonDeserializer
        );
    }





    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, AttributionClickTrackingDTO> kafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, AttributionClickTrackingDTO> factory =
                new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory());

        return factory;
    }
    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, ConversionMapDTO> kafkaListenerContainerFactoryConv() {
        ConcurrentKafkaListenerContainerFactory<String, ConversionMapDTO> factory =
                new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactoryConv());

        return factory;
    }
    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, AttributionAllReportTrackingDTO> kafkaListenerContainerFactoryReport() {
        ConcurrentKafkaListenerContainerFactory<String, AttributionAllReportTrackingDTO> factory =
                new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactoryReport());

        return factory;
    }
    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, ImpressionTrackingDTO> kafkaListenerContainerFactoryImpression() {
        ConcurrentKafkaListenerContainerFactory<String, ImpressionTrackingDTO> factory =
                new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactoryImpression());

        return factory;
    }
    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, InstallReportDTO> kafkaListenerContainerFactoryInstallReport() {
        ConcurrentKafkaListenerContainerFactory<String, InstallReportDTO> factory =
                new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactoryInstallReport());

        return factory;
    }
    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, DailyActiveUsersDTO> kafkaListenerContainerFactoryDailyActiveUsers() {
        ConcurrentKafkaListenerContainerFactory<String, DailyActiveUsersDTO> factory =
                new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactoryDailyActiveUsers());

        return factory;
    }




//    @Value("${spring.kafka.topic.name}")
//    private String topicName;
//
//    @Value("${spring.kafka.topic-json.name}")
//    private String topicJsonName;

//    @Bean
//    public NewTopic attributionClickTopic(){
//        return TopicBuilder.name("click")
//                .build();
//    }

//    @Bean
//    public NewTopic javaguidesJsonTopic(){
//        return TopicBuilder.name("click")
//                .build();
//    }



}
