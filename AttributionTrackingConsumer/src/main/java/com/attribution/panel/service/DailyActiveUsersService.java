package com.attribution.panel.service;

import com.attribution.panel.beans.DailyActiveUsers;
import com.attribution.panel.beans.InstallReport;
import com.attribution.panel.cache.dto.AppTrackingDTO;
import com.attribution.panel.cache.dto.PartnerTrackingDTO;
import com.attribution.panel.cache.repository.AppTrackingDTORepository;
import com.attribution.panel.cache.repository.PartnerTrackingDTORepository;
import com.attribution.panel.constants.UrlConstants;
import com.attribution.panel.dto.DailyActiveUsersDTO;
import com.attribution.panel.elasticsearchRepo.DailyActiveUsersRepository;
import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CityResponse;
import com.maxmind.geoip2.record.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

@Service
@Transactional
public class DailyActiveUsersService {
    @Autowired
    DailyActiveUsersRepository dailyActiveUsersRepository;

    @Autowired
    AppTrackingDTORepository appTrackingDTORepository;

    @Autowired
    PartnerTrackingDTORepository partnerTrackingDTORepository;

    @Autowired
    AttributionAllReportService attributionAllReportService;

    @Autowired
    TrackUtilityService trackUtilityService;

    @Autowired
    InstallReportService installReportService;



    public DailyActiveUsers findByAppIDAndGaid(Long appID, String gaid, String creationDateTime) {
        return dailyActiveUsersRepository.findByAppIDAndGaid(appID, gaid, creationDateTime);
    }

    public void save(DailyActiveUsers dailyActiveUsers) {
        dailyActiveUsersRepository.save(dailyActiveUsers);
    }

    public void saveDailyActiveUsers(DailyActiveUsersDTO dailyActiveUsersDTO) throws IOException, GeoIp2Exception {


        String ip = null;
        Optional<AppTrackingDTO> app = appTrackingDTORepository.findById(dailyActiveUsersDTO.getAppID());
        Optional<PartnerTrackingDTO> partner = partnerTrackingDTORepository.findById(dailyActiveUsersDTO.getPartnerId());

        String date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").format(new Date());
        System.out.println("date is " + date);

        ip = "122.160.74.236";

        File database = new File(UrlConstants.DATABASE_CITY_PATH);
        File database1 = new File(UrlConstants.DATABASE_COUNTRY_PATH);
        DatabaseReader reader = new DatabaseReader.Builder(database).build();
        DatabaseReader reader1 = new DatabaseReader.Builder(database1).build();

//        File dbFile = new File(UrlConstants.DATABASE_CITY_PATH);

        // This creates the DatabaseReader object,
        // which should be reused across lookups.

//        DatabaseReader reader = new DatabaseReader.Builder(dbFile).build();

        // A IP Address
        InetAddress ipAddress = InetAddress.getByName(ip);
        System.out.println("InetAddress " + ipAddress);

        // Get City info
        CityResponse response = reader.city(ipAddress);

        // Country Info
        Country country = response.getCountry();
        System.out.println("Country IsoCode: " + country.getIsoCode()); // 'US'
        System.out.println("Country Name: " + country.getName()); // 'United States'
        System.out.println(country.getNames().get("zh-CN")); // '美国'

        Subdivision subdivision = response.getMostSpecificSubdivision();
        System.out.println("Subdivision Name: " + subdivision.getName()); // 'Minnesota'
        System.out.println("Subdivision IsoCode: " + subdivision.getIsoCode()); // 'MN'

        // City Info.
        City city = response.getCity();
        System.out.println("City Name: " + city.getName()); // 'Minneapolis'

        // Postal info
        Postal postal = response.getPostal();
        System.out.println(postal.getCode()); // '55455'

        // Geo Location info.
        Location location = response.getLocation();

        // Latitude
        System.out.println("Latitude: " + location.getLatitude()); // 44.9733

        // Longitude
        System.out.println("Longitude: " + location.getLongitude()); // -93.2323



        String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
        InstallReport installReport = installReportService.findByAppIdAndGaid(dailyActiveUsersDTO.getAppID() , dailyActiveUsersDTO.getGaid());
        InstallReport installReport1 = null;
        /*if(installReport!=null)
            installReport1=installReportService.findByAppIdAndGoogle_aidAndDate(app, google_aid, currentDate);
        System.out.println("installReport1 = "+installReport1);*/
        if(installReport!=null) {
            DailyActiveUsers dailyActiveUsers = findByAppIDAndGaid(dailyActiveUsersDTO.getAppID(), dailyActiveUsersDTO.getGaid(), currentDate);
            System.out.println("dailyActiveUsers  = "+dailyActiveUsers);
            if (dailyActiveUsers == null) {
                dailyActiveUsers = new DailyActiveUsers();
                dailyActiveUsers.setIp(ip);
                dailyActiveUsers.setCountry(country.getName());
                dailyActiveUsers.setDeviceName(dailyActiveUsersDTO.getDeviceName());
                dailyActiveUsers.setGaid(dailyActiveUsersDTO.getGaid());
                dailyActiveUsers.setOs(dailyActiveUsersDTO.getOs());
//                dailyActiveUsers.setApp(app);
                dailyActiveUsers.setAppID(dailyActiveUsersDTO.getAppID());
                dailyActiveUsers.setAppName(dailyActiveUsersDTO.getAppName());
                dailyActiveUsers.setPartnerId(dailyActiveUsersDTO.getPartnerId());
                dailyActiveUsers.setPartnerName(dailyActiveUsersDTO.getPartnerName());
                save(dailyActiveUsers);
                return ;
            } else
                return ;
        } else
            return ;

    }


//    public DailyActiveUsers findByAppAndGoogle_Aid(App app, String google_aid, String currentDate) {
//    return dailyActiveUsersRepository.findByAppAndGoogle_aid(app.getId(), google_aid, currentDate);
//    }
//
//    public void save(DailyActiveUsers dailyActiveUsers) {
//        dailyActiveUsersRepository.save(dailyActiveUsers);
//    }
//
//    public Long findByAppIDAndToday(Long appId, String currentDate) {
//        return dailyActiveUsersRepository.findByAppIDAndToday(appId, currentDate);
//    }

}
