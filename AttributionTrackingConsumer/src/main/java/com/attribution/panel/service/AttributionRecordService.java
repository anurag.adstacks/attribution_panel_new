package com.attribution.panel.service;


import com.attribution.panel.beans.AttributionRecord;
import com.attribution.panel.elasticsearchRepo.AttributionRecordRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AttributionRecordService {

    @Autowired
    AttributionRecordRepo attributionRecordRepo;

/*    public Optional<Object> findById(AttributionRecordId attributionRecordId) {
        System.out.println("haanji1 " + attributionRecordRepo.findById(attributionRecordId));
        return Optional.of(attributionRecordRepo.findById(attributionRecordId));
    }*/

    public void save(AttributionRecord attributionRecord) {
        attributionRecordRepo.save(attributionRecord);
    }
}
