package com.attribution.panel.service;

import com.attribution.panel.beans.DailyActiveUsers;
import com.attribution.panel.beans.Impression;
import com.attribution.panel.beans.InstallReport;
import com.attribution.panel.cache.dto.AppTrackingDTO;
import com.attribution.panel.cache.dto.PartnerTrackingDTO;
import com.attribution.panel.cache.repository.AppTrackingDTORepository;
import com.attribution.panel.cache.repository.PartnerTrackingDTORepository;
import com.attribution.panel.constants.UrlConstants;
import com.attribution.panel.dto.ImpressionTrackingDTO;
import com.attribution.panel.elasticsearchRepo.ImpressionRepository;
import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CityResponse;
import com.maxmind.geoip2.record.*;
import nl.basjes.parse.useragent.UserAgent;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
@Transactional
public class ImpressionService {
    @Autowired
    ImpressionRepository impressionRepository;

    @Autowired
    AppTrackingDTORepository appTrackingDTORepository;

    @Autowired
    PartnerTrackingDTORepository partnerTrackingDTORepository;

    @Autowired
    AttributionAllReportService attributionAllReportService;

    @Autowired
    TrackUtilityService trackUtilityService;

    @Autowired
    InstallReportService installReportService;

    @Autowired
    DailyActiveUsersService dailyActiveUsersService;


    public void save(Impression impression) {
        impressionRepository.save(impression);
    }

    public Optional<Impression> findById(String s) {
        return impressionRepository.findById(s);
    }

    public void saveImpression(ImpressionTrackingDTO impressionTrackingDTO) throws IOException, GeoIp2Exception {

        System.out.println("Inside Impression");
//        CountryEnum country = null;
        String ip = null;
        Optional<AppTrackingDTO> app = appTrackingDTORepository.findById(impressionTrackingDTO.getAppId());
        Optional<PartnerTrackingDTO> partner = partnerTrackingDTORepository.findById(impressionTrackingDTO.getPartnerId());

        String date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").format(new Date());
        System.out.println("date is " + date);

        ip = "122.160.74.236";

        File database = new File(UrlConstants.DATABASE_CITY_PATH);
        File database1 = new File(UrlConstants.DATABASE_COUNTRY_PATH);
        DatabaseReader reader = new DatabaseReader.Builder(database).build();
        DatabaseReader reader1 = new DatabaseReader.Builder(database1).build();

//        File dbFile = new File(UrlConstants.DATABASE_CITY_PATH);

        // This creates the DatabaseReader object,
        // which should be reused across lookups.

//        DatabaseReader reader = new DatabaseReader.Builder(dbFile).build();

        // A IP Address
        InetAddress ipAddress = InetAddress.getByName(ip);
        System.out.println("InetAddress " + ipAddress);

        // Get City info
        CityResponse response = reader.city(ipAddress);

        // Country Info
        Country country = response.getCountry();
        System.out.println("Country IsoCode: " + country.getIsoCode()); // 'US'
        System.out.println("Country Name: " + country.getName()); // 'United States'
        System.out.println(country.getNames().get("zh-CN")); // '美国'

        Subdivision subdivision = response.getMostSpecificSubdivision();
        System.out.println("Subdivision Name: " + subdivision.getName()); // 'Minnesota'
        System.out.println("Subdivision IsoCode: " + subdivision.getIsoCode()); // 'MN'

        // City Info.
        City city = response.getCity();
        System.out.println("City Name: " + city.getName()); // 'Minneapolis'

        // Postal info
        Postal postal = response.getPostal();
        System.out.println(postal.getCode()); // '55455'

        // Geo Location info.
        Location location = response.getLocation();

        // Latitude
        System.out.println("Latitude: " + location.getLatitude()); // 44.9733

        // Longitude
        System.out.println("Longitude: " + location.getLongitude()); // -93.2323

        String postals = String.valueOf(postal);
        Map<String, Object> map1 = new HashMap<>();
        map1.put("countryIsoCode", country.getIsoCode());
        map1.put("countryName", country.getName());
        map1.put("subdivisionIsoCode", subdivision.getIsoCode());
        map1.put("subdivisionName", subdivision.getName());
        map1.put("city", city.getName());
        map1.put("Latitude", location.getLatitude());
        map1.put("Longitude", location.getLongitude());
        map1.put("postal", postals);
        map1.put("ip", ip);

        UserAgent userAgent = trackUtilityService.getDeviceId(impressionTrackingDTO.getAgent());
        System.out.println("useragentsIsis " + userAgent.getUserAgentString());


        InstallReport installReport = installReportService.findByAppIdAndGaid(impressionTrackingDTO.getAppId(), impressionTrackingDTO.getGoogle_aid());
        System.out.println("InstallReport " + installReport);


        System.out.println("countryCode " + country.getIsoCode());

        String sectionId = RandomStringUtils.randomAlphanumeric(12);


        if (installReport != null) {
            Impression impression = new Impression();
            impression.setId(UUID.randomUUID().toString().replace("-", ""));
            impression.setIp(ip);
            impression.setCountry(country.getName());
            impression.setDeviceName(impressionTrackingDTO.getDeviceName());
            impression.setGoogle_aid(impressionTrackingDTO.getGoogle_aid());
            impression.setOs(impressionTrackingDTO.getOs());
            impression.setAppId(impressionTrackingDTO.getAppId());
            impression.setAppName(app.get().getName());
            impression.setPartnerId(impressionTrackingDTO.getPartnerId());
            impression.setPartnerName(partner.get().getName());
            impression.setCreationDateTime(impression.getCreationDateTime());
            save(impression);
            attributionAllReportService.saveImpresionDailyAllReportRecord(userAgent, map1, impression);
            String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
            DailyActiveUsers dailyActiveUsers = dailyActiveUsersService.findByAppIDAndGaid(impressionTrackingDTO.getAppId(), impressionTrackingDTO.getGoogle_aid(), currentDate);
            System.out.println("dailyActiveUsers  = " + dailyActiveUsers);
            if (dailyActiveUsers == null) {
                dailyActiveUsers = new DailyActiveUsers();
                dailyActiveUsers.setId(UUID.randomUUID().toString().replace("-", ""));
                dailyActiveUsers.setIp(ip);
                dailyActiveUsers.setCountry(country.getName());
                dailyActiveUsers.setDeviceName(impressionTrackingDTO.getDeviceName());
                dailyActiveUsers.setGaid(impressionTrackingDTO.getGoogle_aid());
                dailyActiveUsers.setOs(impressionTrackingDTO.getOs());
//                dailyActiveUsers.setApp(app);
                dailyActiveUsers.setAppID(impressionTrackingDTO.getAppId());
                dailyActiveUsers.setAppName(impressionTrackingDTO.getAppName());
                dailyActiveUsers.setPartnerId(impressionTrackingDTO.getPartnerId());
                dailyActiveUsers.setPartnerName(impressionTrackingDTO.getPartnerName());
                dailyActiveUsers.setCreationDateTime(date);
                dailyActiveUsersService.save(dailyActiveUsers);
//                List<Impression> impressions = (List<Impression>) impressionRepository.findAll();
                System.out.println("impressionService " + dailyActiveUsers);

//                model.put("response DAU", "Save Daily Active User");
            } else
//                model.put("response DAU", "Daily Active User Already Exist");
//            model.put("response Imp.", "Saved User Impression");
            return ;
        } else
            return ;

    }

    /*public Long findByAppIDAndStartBetweenEndDate(Long appId, String startDate, String endDate) {
        return impressionRepository.findByAppIDAndStartBetweenEndDate(appId, startDate, endDate);
    }

    public Long findByAppIDAndToday(Long appId, String currentDate) {
        return impressionRepository.findByAppIDAndToday(appId, currentDate);
    }

    public Impression findByAppAndGoogle_Aid(App app, String google_aid, String currentDate) {
        return impressionRepository.findByAppAndGoogle_aid(app.getId(), google_aid, currentDate);
    }*/

}
