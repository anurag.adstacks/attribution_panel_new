//package com.attribution.panel.service;
//
//import com.etreetech.panel.cache.dto.*;
//import com.etreetech.panel.cache.repository.AdvertiserTrackingDTORepository;
//import com.etreetech.panel.cache.repository.BlockSubAffiliateRepository;
//import com.etreetech.panel.cache.repository.BusinessRuleCacheRepository;
//import com.etreetech.panel.enums.ActionRule;
//import com.etreetech.panel.enums.Scope;
//import com.etreetech.panel.enums.StatusRule;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.web.client.RestTemplate;
//
//import java.util.List;
//import java.util.stream.Collectors;
//
//import static com.etreetech.panel.constants.DbAccessConstants.BASE_URL;
//
//@Service
//@Transactional
//@Slf4j
//public class BusinessRuleService {
//    @Autowired
//    BusinessRuleCacheRepository businessRuleCacheRepository;
//    @Autowired
//    AdvertiserTrackingDTORepository advertiserTrackingDTORepository;
//    @Autowired
//    BlockSubAffiliateRepository blockSubAffiliateRepository;
//    RestTemplate restTemplate = new RestTemplate();
//
//    public BusinessRuleCacheDTO fetchBusinessRules(List<StatusRule> statusRule, OfferTrackingDTO offer, PublisherTrackingDTO publisher) {
//        System.out.println("Inside fetch business rules");
//        BusinessRuleCacheDTO businessRule = null;
//        List<BusinessRuleCacheDTO> businessRules = null;
//        AdvertiserTrackingDTO advertiser = advertiserTrackingDTORepository.findById(offer.getAdvertiserId()).orElse(null);
//
//        List<BusinessRuleCacheDTO> businessRuleList = (List<BusinessRuleCacheDTO>) businessRuleCacheRepository.findAll();
////        log.info("bsRule:" + businessRuleList);
//
//        businessRules = businessRuleList.stream().filter(bsRule -> bsFilter(bsRule, statusRule, Scope.OFFER, false))
//                .collect(Collectors.toList());
//        for (BusinessRuleCacheDTO businessRuleCacheDTO : businessRules) {
//            if (isRuleApplicable(offer, publisher, businessRuleCacheDTO, Scope.OFFER, null)) {
//                return businessRuleCacheDTO;
//            }
//        }
//
//        businessRules = businessRuleList.stream().filter(bsRule -> bsFilter(bsRule, statusRule, Scope.ADVERTISER, false)).
//                collect(Collectors.toList());
//
//        for (BusinessRuleCacheDTO businessRuleCacheDTO : businessRules) {
//            if (isRuleApplicable(null, publisher, businessRuleCacheDTO, Scope.ADVERTISER, advertiser)) {
//                return businessRuleCacheDTO;
//            }
//        }
//
//        businessRules = businessRuleList.stream().filter(bsRule -> bsFilter(bsRule, statusRule, Scope.ALL, false)).
//                collect(Collectors.toList());
//
//        for (BusinessRuleCacheDTO businessRuleCacheDTO : businessRules) {
//            if (isRuleApplicable(null, publisher, businessRuleCacheDTO, Scope.ALL, null)) {
//                return businessRuleCacheDTO;
//            }
//        }
//
//        businessRules = businessRuleList.stream().filter(bsRule -> bsFilter(bsRule, statusRule, Scope.OFFER, true)).
//                collect(Collectors.toList());
//
//        for (BusinessRuleCacheDTO businessRuleCacheDTO : businessRules) {
//            if (isRuleApplicable(offer, null, businessRuleCacheDTO, Scope.OFFER, null)) {
//                return businessRuleCacheDTO;
//            }
//        }
//
//        businessRules = businessRuleList.stream().filter(bsRule -> bsFilter(bsRule, statusRule, Scope.ADVERTISER, true)).
//                collect(Collectors.toList());
//
//        for (BusinessRuleCacheDTO businessRuleCacheDTO : businessRules) {
//            if (isRuleApplicable(null, null, businessRuleCacheDTO, Scope.ADVERTISER, advertiser)) {
//                return businessRuleCacheDTO;
//            }
//        }
//
//        businessRules = businessRuleList.stream().filter(bsRule -> bsFilter(bsRule, statusRule, Scope.ALL, true)).
//                collect(Collectors.toList());
//
//        for (BusinessRuleCacheDTO businessRuleCacheDTO : businessRules) {
//            if (isRuleApplicable(null, null, businessRuleCacheDTO, Scope.ALL, null)) {
//                return businessRuleCacheDTO;
//            }
//        }
//
//        return businessRule;
//    }
//
//    public boolean isRuleApplicable(OfferTrackingDTO offerTrackingDTO, PublisherTrackingDTO publisherTrackingDTO,
//                                    BusinessRuleCacheDTO businessRuleCacheDTO, Scope scope, AdvertiserTrackingDTO advertiserTrackingDTO) {
//        boolean isRuleApplicable = false;
//        try {
//            switch (scope) {
//                case OFFER:
//                    if ((businessRuleCacheDTO.getOffer().contains(offerTrackingDTO.getId()))
//                            && (publisherTrackingDTO == null || businessRuleCacheDTO.getPub().contains(publisherTrackingDTO.getId()))) {
//                        isRuleApplicable = true;
//                    }
//                    break;
//                case ADVERTISER:
//                    if (businessRuleCacheDTO.getAdvert().contains(advertiserTrackingDTO.getId())
//                            && (publisherTrackingDTO == null || businessRuleCacheDTO.getPub().contains(publisherTrackingDTO.getId()))) {
//                        isRuleApplicable = true;
//                    }
//                    break;
//                case ALL:
////                    log.info("businessRuleCacheDTO:" + businessRuleCacheDTO.getPub());
//                    if (publisherTrackingDTO == null || businessRuleCacheDTO.getPub().contains(publisherTrackingDTO.getId())) {
//                        isRuleApplicable = true;
//                    }
//                    break;
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return isRuleApplicable;
//    }
//
//    public Boolean bsFilter(BusinessRuleCacheDTO businessRule, List<StatusRule> statusRule, Scope scope,
//                            Boolean isAllPub) {
//        boolean toIncludeRule = false;
//        try {
//            toIncludeRule = statusRule.contains(businessRule.getStatusRule())
//                    && businessRule.getScope().equals(scope)
//                    && businessRule.isAllPublishers() == isAllPub;
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return toIncludeRule;
//    }
//
//    //Function to pause offer,block campaign and block subaffiliate.
//    public void businessRuleAction(BusinessRuleCacheDTO bsRule, OfferTrackingDTO offer, PublisherTrackingDTO publisher, String subAff) {
//        ActionRule rule = bsRule.getActionRule();
//        log.info("In violation: " + BASE_URL);
//        switch (rule) {
//            case PAUSE_OFFER:
//                log.info("Pause offer");
//                restTemplate.getForEntity(BASE_URL + "/offer/pauseOffer?offerId=" + offer.getId()+ "&ruleId="+bsRule.getId(), String.class).getBody();
//                break;
//            case BLOCK_CAMPAIGN:
//                log.info("Block Campaign");
//                restTemplate.getForEntity(BASE_URL + "/campaign/blockCampaign?offerId=" + offer.getId() + "&pubId="
//                        + publisher.getId()+"&ruleId="+bsRule.getId(), String.class).getBody();
//                break;
//            case BlockSubAffiliate:
//                log.info("BlockSubAffiliate");
//                String id = "offerId:" + offer.getId() + "pubId:" + publisher.getId() + "subAff:" + subAff;
//                BlockSubAffiliate blockSubAffiliate = blockSubAffiliateRepository.findById(id).orElse(null);
//                if (blockSubAffiliate == null) {
//                    blockSubAffiliate = new BlockSubAffiliate();
//                    blockSubAffiliate.setId(id);
////                    blockSubAffiliate.setSubAff(subAff);
////                    blockSubAffiliate.setOfferId(offer.getId());
////                    blockSubAffiliate.setPubId(publisher.getId());
//                    blockSubAffiliate.setStatusRule(bsRule.getStatusRule());
//                    blockSubAffiliateRepository.save(blockSubAffiliate);
//                }
//                break;
//            default:
//                log.info("No rule");
//        }
//    }
//
//    public void businessRuleBlockSubAffiliateAction(BusinessRuleCacheDTO bsRule, OfferTrackingDTO offer, PublisherTrackingDTO publisher, String subAff) {
//        log.info("BlockSubAffiliate = "+BASE_URL);
//        restTemplate.getForEntity(BASE_URL + "/subAffiliate/blockSubAffiliate?offerId=" + offer.getId()+
//                "&pubId="+publisher.getId()+"&ruleId="+bsRule.getId()+"&subAff="+subAff, String.class).getBody();
//    }
//}
