package com.attribution.panel.service;

import com.attribution.panel.beans.AttributionClick;
import com.attribution.panel.beans.AttributionRecord;
import com.attribution.panel.beans.Attributionconversion;
import com.attribution.panel.beans.Impression;
import com.attribution.panel.cache.dto.AppTrackingDTO;
import com.attribution.panel.cache.dto.EventCacheDTO;
import com.attribution.panel.cache.dto.PartnerTrackingDTO;
import com.attribution.panel.cache.repository.AppTrackingDTORepository;
import com.attribution.panel.cache.repository.EventCacheDTORepository;
import com.attribution.panel.cache.repository.PartnerTrackingDTORepository;
import com.attribution.panel.constants.ElasticSearchConstants;
import com.attribution.panel.constants.UrlConstants;
import com.attribution.panel.dao.ReportQueryBuilder;
import com.attribution.panel.dto.ConversionMapDTO;
import com.attribution.panel.elasticsearchRepo.AttributionRecordRepo;
import com.attribution.panel.elasticsearchRepo.ClickRepo;
import com.attribution.panel.elasticsearchRepo.ConversionRepo;
import com.attribution.panel.enums.ConversionStatus;
import com.attribution.panel.enums.CountryEnum;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CityResponse;
import com.maxmind.geoip2.record.*;
import lombok.extern.slf4j.Slf4j;
import nl.basjes.parse.useragent.UserAgent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;


@Service
@Slf4j
@Transactional
public class ConversionsService extends TrackUtilityService {

    @Autowired
    Environment environment;

    @Autowired
    IpRequestService ipRequestService;

    @Autowired
    AppTrackingDTORepository appRepository;

    @Autowired
    PartnerTrackingDTORepository partnerRepository;

    @Autowired
    ConversionRepo conversionRepository;

    @Autowired
    AttributionRecordService attributionRecordService;

    @Autowired
    AttributionRecordRepo attributionRecordRepo;

    @Autowired
    ClickRepo clickRepository;

    @Autowired
    ClickService clickService;

//    @Autowired
//    AppService appService;

//    @Autowired
//    EventService eventService;

    @Autowired
    EventCacheDTORepository eventRepository;

    @Autowired
    ClickRepo clickRepo;

    @Autowired
    ReportQueryBuilder reportQueryBuilder;

    @Autowired
    AttributionAllReportService attributionAllReportService;


    public String getEnvironment() {
        String[] activeProfiles = environment.getActiveProfiles();
        return activeProfiles[0];
    }

    public void saveAttributionPostback(ConversionMapDTO payload) throws IOException, GeoIp2Exception {
        attributionPostback(payload.getAppId(), payload.getGaid(), payload.getToken(), payload.getAgent(), payload.getIp(), payload.getS1(), payload.isApk(), payload.getPId(), payload.getAgentRequest(), payload.getIpRequest());
    }

    public Map<String, String> attributionPostback(Long appId, String gaid, String token, String agent, String ip, String s1, boolean isApk, Long pId, String agentRequest, String ipRequest) throws IOException, GeoIp2Exception {

        System.out.println("Inside attributionPostback");

        Map<String, Object> map = new HashMap<>();
        CountryEnum countrys = null;
        Impression impression = null;

//        agent = request.getHeader("User-Agent");
//        System.out.println("agentif " + agent);

//        ip = ipRequestService.getClientIp(request);
        System.out.println("ip is  " + ip);

//        country = CountryEnum.valueOf(request.getHeader("CF-IPCountry"));
//        System.out.println("try country " + country);


//        map.put("date", new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").format(new Date()));

        String debug = "/attributionPostback?app_id=" + appId + "&p_id=" + pId + "&token=" + token + "&gaid=" + gaid + "&agent=" + agent
                + "&s1=" + s1 + "&isApk=" + isApk + "&ip=" + ip;


        String yoo = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").format(new Date());
        System.out.println("debugs " + debug);

        Attributionconversion conversions = new Attributionconversion();
        String date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").format(new Date());
        System.out.println("date is " + date);

        System.out.println("appId is " + appId);
        System.out.println("pId is " + pId);
        System.out.println("token is " + token);
        System.out.println("agent is " + agent);
        System.out.println("gaid is " + gaid);
        System.out.println("ip is " + ip);
        System.out.println("country is " + countrys);
        System.out.println("isApk is " + isApk);


        File database = new File(UrlConstants.DATABASE_CITY_PATH);
        File database1 = new File(UrlConstants.DATABASE_COUNTRY_PATH);
        DatabaseReader reader = new DatabaseReader.Builder(database).build();
        DatabaseReader reader1 = new DatabaseReader.Builder(database1).build();

//        File dbFile = new File(UrlConstants.DATABASE_CITY_PATH);

        // This creates the DatabaseReader object,
        // which should be reused across lookups.

//        DatabaseReader reader = new DatabaseReader.Builder(dbFile).build();

        // A IP Address
        InetAddress ipAddress = InetAddress.getByName(ip);
        System.out.println("InetAddress " + ipAddress);

        // Get City info
        CityResponse response = reader.city(ipAddress);

        // AttributionCountry Info
        Country country = response.getCountry();
        System.out.println("AttributionCountry IsoCode: " + country.getIsoCode()); // 'US'
        System.out.println("AttributionCountry Name: " + country.getName()); // 'United States'
        System.out.println(country.getNames().get("zh-CN")); // '美国'

        Subdivision subdivision = response.getMostSpecificSubdivision();
        System.out.println("Subdivision Name: " + subdivision.getName()); // 'Minnesota'
        System.out.println("Subdivision IsoCode: " + subdivision.getIsoCode()); // 'MN'

        // City Info.
        City city = response.getCity();
        System.out.println("City Name: " + city.getName()); // 'Minneapolis'

        // Postal info
        Postal postal = response.getPostal();
        System.out.println(postal.getCode()); // '55455'

        // Geo Location info.
        Location location = response.getLocation();

        // Latitude
        System.out.println("Latitude: " + location.getLatitude()); // 44.9733

        // Longitude
        System.out.println("Longitude: " + location.getLongitude()); // -93.2323

        String postals = String.valueOf(postal);

        Map<String, Object> map2 = new HashMap<>();
        map2.put("countryIsoCode", country.getIsoCode());
        map2.put("countryName", country.getName());
        map2.put("subdivisionIsoCode", subdivision.getIsoCode());
        map2.put("subdivisionName", subdivision.getName());
        map2.put("city", city.getName());
        map2.put("Latitude", location.getLatitude());
        map2.put("Longitude", location.getLongitude());
        map2.put("postal", postals);
        map2.put("ip", ip);


//        AttributionDeviceId deviceId = getDeviceId(agent);
        UserAgent userAgent = uaa.parse(agent);
        System.out.println("DeviceIdis = " + userAgent.getUserAgentString());


        String OperatingSystemNameVersion = (userAgent.getValue("OperatingSystemNameVersion") == null) ? "" : userAgent.getValue("OperatingSystemNameVersion");
        String DeviceName = (userAgent.getValue("DeviceName") == null) ? "" : userAgent.getValue("DeviceName");
        String AgentNameVersion = (userAgent.getValue("AgentNameVersion") == null) ? "" : userAgent.getValue("AgentNameVersion");
        String LayoutEngineNameVersion = (userAgent.getValue("LayoutEngineNameVersion") == null) ? "" : userAgent.getValue("LayoutEngineNameVersion");

//        AttributionClick attributionclick = new AttributionClick(OperatingSystemNameVersion, DeviceName, AgentNameVersion, LayoutEngineNameVersion);
//        System.out.println("clickAttribution1 " + attributionclick);

        AppTrackingDTO app = appRepository.findById(appId).orElse(null);
        System.out.println("app = " + app.getName());

        PartnerTrackingDTO partner = partnerRepository.findById(pId).orElse(null);
        System.out.println("Partner = " + partner.getName());

        AttributionClick click = null;
        Map<String, Object> map1 = new HashMap<>();
        map1.put("agent", agent);
        map1.put("ip", ip);
        map1.put("OperatingSystemNameVersion", OperatingSystemNameVersion);
        map1.put("DeviceName", DeviceName);
        map1.put("AgentNameVersion", AgentNameVersion);
        map1.put("LayoutEngineNameVersion", LayoutEngineNameVersion);
        map1.put("app_id", appId);

        System.out.println("insideIfCliNull9 " + map1);

        //Boolean value, if conversion held upon a previous click
        boolean isClickFound = true;

        /*click = restTemplate.postForEntity(BASE_URL + "/click/findFirstByDeviceIdAndIpAndOfferIdOrderByCreationDateTimeDesc",
                map1, Click.class).getBody();*/

        System.out.println("findFirstByDeviceIdAndIpAndOffer " + click);

        click = findFirstByDeviceIdAndIpAndOfferIdOrderByCreationDateTimeDesc(map1);  // will come from baseurl later on .

        System.out.println("data clickyo = " + click);

        if (isApk && click == null) {
            System.out.println("YoinHere  " + click);
            click = new AttributionClick(app, gaid, agent, ip, userAgent, "", s1, partner, date, country.getName());
            isClickFound = false;
        } else if (click == null) {
//            click = restTemplate.getForEntity(BASE_URL + "/click/findFirstByOfferIdAndGaidOrderByCreationDateTimeDesc?appId="
//                    + appId + "&gaid=" + gaid, Click.class).getBody();
            System.out.println("YoinHere1 ");
            isClickFound = false;
        }


        if (click == null) {
            System.out.println("insideIfClickNull " + click);
            click = new AttributionClick(app, gaid, agent, ip, userAgent, "", s1, partner, date, country.getName());
            isClickFound = false;
            System.out.println("insideIfClickNull2 " + click);
        }

        if (click.getGaid() == null && !gaid.isEmpty()) {
            click.setGaid(gaid);
            System.out.println("gaidEmpty " + gaid);
        }

//        if (countrys != null && click.getCountry() == null) {
//            click.setCountry(String.valueOf(country));
//            System.out.println("countryEmpty " + country);
//        }

        String clickId = click.getId();
        System.out.println("ClickID ID " + click.getId());
//        List<Attributionconversion> conversionList = conversionRepository.findByClickId(click.getId());

//        List<Attributionconversion> conversionList = conversionRepository.findByClickIdAndAppIdAndPartnerIdOrderByCreationDateTimeDesc(clickId, appId, pId);

        List<Attributionconversion> attributionconversions = reportQueryBuilder.findByClickIdAndAppIdAndPartnerIdOrderByCreationDateTimeDesc(clickId, appId, pId);


        System.out.println("conversionList " + attributionconversions);
        System.out.println("conversionList2 " + attributionconversions.size());
        System.out.println("debug is " + debug);

        Attributionconversion conversion = new Attributionconversion(debug, date, date, click); // es vale me useragent vala system add karna hai


        DateFormat dateFormat = new SimpleDateFormat(ElasticSearchConstants.chrField_HourTimestampPattern);
        String dayTimestamp = dateFormat.format(new Date());

        System.out.println("debugis1 " + conversion);
        if (ip.isEmpty() || agent.isEmpty()) {
            System.out.println("ip.isEmpty() " + ip);
            conversion.setConversionStatus(ConversionStatus.REJECTED);
            conversion.setConversionMessage("Attribution Failed");
        } else if (attributionconversions.size() == 0) {
            conversion.setConversionStatus(ConversionStatus.APPROVED);
            conversion.setPartnerId(partner.getId());
            conversion.setPartnerName(partner.getName());
            conversion.setAppId(app.getId());
            conversion.setAppName(app.getName());
//            conversion.setOffer(new OfferConversionDTO(offer.getId(), offer.getOfferName()));

            conversion = setEvent(token, click, conversion);
            System.out.println("conversion setEvent " + conversion);


//            conversion = multiEventCheck(conversion);
            System.out.println("conversion multiEventCheck " + conversion);

            saveOrUpdateClick(click, conversion, conversion.getConversionStatus(), s1, gaid);

            log.info("setConversionStatus:" + conversion.getConversionStatus());


//            conversion = passPostback(conversion, click, publisher, offerPostback);

//            Map<String, Object> map1 = new HashMap<>();
//            map1.put("conversion", conversion);
            System.out.println("isClickFound " + isClickFound);

//            saveConversionDailyRecord(conversion, click, dayTimestamp, isClickFound);
            attributionAllReportService.saveConversionDailyAllReportRecord(conversion, click, dayTimestamp, isClickFound, userAgent, map2, impression);
//            template.convertAndSend("app1-exchange", "CDR-routing-key", map1);
        }
        System.out.println("conversionRepository.save " + conversion);


        if (attributionconversions.size() == 0) {
            System.out.println("inside coversion save " + attributionconversions.size());
            conversionRepository.save(conversion);
        }

        Map<String, String> responseMap = new HashMap<>();
        responseMap.put("message", "Conversion Registered");
        responseMap.put("status", "true");
        return responseMap;

    }


    public void saveConversionDailyRecord(Attributionconversion conversion, AttributionClick attributionclick, String dayTimestamp, boolean isClickFound) {

        Optional<AppTrackingDTO> app = appRepository.findById(attributionclick.getAppID());
        System.out.println("apppsps is " + conversion.getEventID());
        System.out.println("apppsps is1 " + conversion.getEventName());
        System.out.println("apppsps is2 " + app.get().getName());


        try {
//            Conversion conversion = (Conversion) map.get("conversion");
//            log.info("clickId:" + conversion.getClickId());
//            String clickId = conversion.getClickId();
            System.out.println("saveConversionDailyRecord1 " + attributionclick);
            System.out.println("saveConversionDailyRecord " + conversion);


//            AttributionClick click = clickRepository.findById(conversion.getClickId()).orElse(null);
//            System.out.println("clickcount0  " + click);


//            App app = appRepository.findById(clickAttribution.getAppID()).orElse(null);

//            List<Attributionconversion> conversionList = clickRepository.findByClickId(clickAttribution.getId());
//            System.out.println("hwlloMrHowDoUdo2 " + conversionList);

            AttributionRecord attributionRecord = null;

            if (conversion.getId() == null) {
                System.out.println("horlyhourlu");
                attributionRecord = attributionRecordRepo.findByAppIdAndPartnerIdAndDayTimestamp(attributionclick.getAppID(), attributionclick.getPId(), dayTimestamp);
                System.out.println("horlyhourlu4 " + attributionRecord);
                if (attributionRecord == null) {
                    attributionRecord = new AttributionRecord(conversion, dayTimestamp, attributionclick, app);
                    saveAttributionRecord(conversion, attributionRecord, attributionclick, isClickFound);
                    System.out.println("horlyhourlu2" + attributionRecord);
                } else {
                    saveAttributionRecord(conversion, attributionRecord, attributionclick, isClickFound);
                    System.out.println("horlyhourlu3" + attributionRecord);
                }
            } else {
                attributionRecord = attributionRecordRepo.findByAppIdAndPartnerIdAndDayTimestamp(conversion.getAppId(), conversion.getPartnerId(), dayTimestamp);
                System.out.println("horlyhourlu8 " + attributionRecord);
                if (attributionRecord == null) {
                    attributionRecord = new AttributionRecord(conversion, dayTimestamp, attributionclick, app);
                    saveAttributionRecord(conversion, attributionRecord, attributionclick, isClickFound);
                    System.out.println("horlyhourlu5" + attributionRecord);
                } else {
                    saveAttributionRecord(conversion, attributionRecord, attributionclick, isClickFound);
                    System.out.println("horlyhourlu6" + attributionRecord);
                }
                System.out.println("horlyhourlu7" + attributionRecord);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void saveAttributionRecord(Attributionconversion conversion, AttributionRecord attributionRecord, AttributionClick attributionclick, boolean isClickFound) {


        String sDate = "Apr 1, 2023";
        String eDate = "Apr 5, 2023";
        Long count = null;
        System.out.println("thisIsSaveConversionDailyRecord0  " + conversion.getAppId() + " " + attributionclick.getPId());
//        Long count = reportQueryBuilder.countByAppIdAndPartnerIdAndCreationDateTimeBetween(attributionclick.getAppID(), attributionclick.getPId(), sDate, eDate );
        if (conversion.getId() == null) {
            count = reportQueryBuilder.countByAppIdAndPartnerIdAndCreationDateTimeBetween(attributionclick.getAppID(), attributionclick.getPId());
            System.out.println("thisIsSaveConversionDailyRecord00  " + count);
        } else {
            count = reportQueryBuilder.countByAppIdAndPartnerIdAndCreationDateTimeBetween(attributionclick.getAppID(), conversion.getPartnerId());
            System.out.println("thisIsSaveConversionDailyRecord000  " + count);
        }

        System.out.println("thisIsSaveConversionDailyRecord0000  " + count);

        try {
            if (conversion.getId() == null) {
                System.out.println("thisIsSaveConversionDailyRecord0  " + conversion.getConversionStatus() + attributionRecord.getApprovedConversions() + attributionRecord.getClickCount());
                attributionRecord.setClickCount(attributionRecord.getClickCount() + 1);
                double ac = attributionRecord.getApprovedConversions();
                double cc = attributionRecord.getClickCount();
                double ratio = ac / cc * 100;
                double ctr = cc / count * 100;
                System.out.println("clickcount4  " + ratio);
                attributionRecord.setRatio(ratio);
                attributionRecord.setCtr(ctr);
            } else {
                System.out.println("thisIsSaveConversionDailyRecord1  " + conversion.getConversionStatus() + attributionRecord.getApprovedConversions() + attributionRecord.getClickCount());
                if (!isClickFound) {
                    attributionRecord.setClickCount(attributionRecord.getClickCount() + 1);
                }
                double ac = attributionRecord.getApprovedConversions();
                double cc = attributionRecord.getClickCount();
                double ratio = ac / cc * 100;
                double ctr = cc / count * 100;
                System.out.println("clickcount4  " + ratio);
                attributionRecord.setRatio(ratio);
                attributionRecord.setCtr(ctr);
                System.out.println("clickcount3  " + (attributionRecord.getApprovedConversions() / attributionRecord.getClickCount()) * 100);
                switch (conversion.getConversionStatus()) {
                    case APPROVED:
                        attributionRecord.setApprovedConversions(attributionRecord.getApprovedConversions() + 1);
//                    attributionRecord.setApprovedRevenue(attributionRecord.getApprovedRevenue() + conversion.getEvent().getRevenue());
//                    attributionRecord.setApprovedPayout(attributionRecord.getApprovedPayout() + conversion.getEvent().getPayout());
                        System.out.println("hhihihihihihih" + attributionRecord);
                        break;
                    case PENDING:
                        attributionRecord.setPendingConversions(attributionRecord.getPendingConversions() + 1);
//                    attributionRecord.setPendingRevenue((attributionRecord.getPendingRevenue() + conversion.getEvent().getRevenue()));
                        System.out.println("hahahaahahaha");
                        break;
                    case CANCELLED:
                        attributionRecord.setCancelledConversions(attributionRecord.getCancelledConversions() + 1);
//                    attributionRecord.setCancelledRevenue(attributionRecord.getCancelledRevenue() + conversion.getEvent().getRevenue());
                        System.out.println("hahahaahahaha");
                        break;
                }
            }

            System.out.println("clickcount6  " + attributionRecord);
            attributionRecordService.save(attributionRecord);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void saveOrUpdateClick(AttributionClick click, Attributionconversion conversion, ConversionStatus conversionStatus, String s1, String gaid) {

        System.out.println("inside saveOrUpdateClick");
        System.out.println("clickis " + click);

//        if (!s1.isEmpty())
//            click.setS1(s1);
//        Long id = click.getAppID();
//        App app = appService.findById(id);
//        System.out.println("AppName is " + app.getName());
//
//        Long appId = click.getAppID();
//        System.out.println("appId is " + appId);
//
//        String appName = app.getName();
//        System.out.println("appName is " + appName);
//        click.setAppName(appName);
//        click.setAppID(appId);

//        ConversionStatus cs = click.getConversionStatus();
//        System.out.println("cs " + cs);

        click.setConversionStatus(ConversionStatus.APPROVED);

//        String partnerName = click.getPartner().getName();
//        System.out.println("appName is " + appName);


//        click.setAppName();
//        click.setGaid(gaid);
//        restTemplate.put(BASE_URL + "/click/save", click);
//        clickQueryBuilder.save(click);


        clickService.saveClick(click);

    }


    public AttributionClick findFirstByDeviceIdAndIpAndOfferIdOrderByCreationDateTimeDesc(@RequestBody Map map) {
//        log.info("Inside findFirstByDeviceIdAndIpAndOfferIdOrderByCreationDateTimeDesc");
        System.out.println("Inside findFirstByDeviceIdAndIpAndOfferIdOrderByCreationDateTimeDesc " + map);

        ObjectMapper objectMapper = new ObjectMapper();
        String agent = objectMapper.convertValue(map.get("agent"), String.class);
        String ip = objectMapper.convertValue(map.get("ip"), String.class);
        Long offerId = objectMapper.convertValue(map.get("app_id"), Long.class);
        String OperatingSystemNameVersion = objectMapper.convertValue(map.get("OperatingSystemNameVersion"), String.class);
        String DeviceName = objectMapper.convertValue(map.get("DeviceName"), String.class);
        String AgentNameVersion = objectMapper.convertValue(map.get("AgentNameVersion"), String.class);
        String LayoutEngineNameVersion = objectMapper.convertValue(map.get("LayoutEngineNameVersion"), String.class);


        System.out.println("DeviceIdAndIpAndOfferIdOrder " + ip + " " + agent + " " + offerId + " " + OperatingSystemNameVersion + " " + DeviceName + " " + AgentNameVersion + " " + LayoutEngineNameVersion);

        List<AttributionClick> clickList = clickRepo.findByAgentAndIpAndAppID(agent, ip, offerId);
        System.out.println("findByAgentAndIpAndAppIDOrderByCreationDateTimeDesc " + clickList);

        if (clickList.size() == 0) {
            clickList = clickRepo.findByOperatingSystemNameVersionAndDeviceNameAndAgentNameVersionAndLayoutEngineNameVersionAndIpAndAppID(OperatingSystemNameVersion, DeviceName, AgentNameVersion, LayoutEngineNameVersion, ip, offerId);
            System.out.println("findByDeviceIdAndIpAndAppIDOrderByCreationDateTimeDesc " + clickList);
        }
        if (clickList.size() == 0) {
            clickList = reportQueryBuilder.getCLickListForIPAndOfferId(ip, offerId.toString());
            System.out.println("getCLickListForIPAndOfferId " + clickList);
        }
        System.out.println("data Ip Address = " + clickList.size());

        AttributionClick clickDTO = null;
        UserAgent userAgent = uaa.parse(agent);
//        System.out.println("DeviceIdis = " + userAgent);

        if (!clickList.isEmpty()) {
            System.out.println("clickDTOyo " + clickList);
//            clickDTO = new AttributionClick(clickList.get(0), userAgent);
        }
        System.out.println("clickDTOyo1 " + clickDTO);
        if (clickList == null || clickList.isEmpty()) {
            System.out.println("Inside clickList.isEmpty");
            return null;
        } else {
            System.out.println("Inside clickList.isEmpty2 " + clickList);
            return clickList.get(0);
        }
    }


    public List<Attributionconversion> findAll() {
        return (List<Attributionconversion>) conversionRepository.findAll();
    }

    private Attributionconversion setEvent(String token, AttributionClick click, Attributionconversion conversion) {
        try {
//            Long offerId = click.getOfferId();
            Long appId = click.getAppID();
            log.info("appId:" + appId);
            System.out.println("getAppID " + appId);

            List<EventCacheDTO> event = eventRepository.findByAppsId(appId);
            log.info("event List:" + event);
            System.out.println("event " + event);

            String clickId = click.getId();
            conversion.setClickId(clickId);

            EventCacheDTO events = event.get(0); //setting initial event as default event
            log.info("eventYo :" + events);
            conversion.setEventID(events.getId());
            conversion.setEventName(events.getEventName());
//            conversion.setEvent( new Event(events));

            log.info("token:" + token);

            if (!token.equals("")) {
                System.out.println("tokenis " + token);

//                event = new Event(token, offer);
                long index = -1;
                EventCacheDTO newEvent = null;
                for (EventCacheDTO eventDTO : event) {
                    log.info("eventDTO: " + eventDTO);
                    if (eventDTO.getToken() != null && eventDTO.getToken().equals(token)) {
                        newEvent = eventDTO;
                        System.out.println("newEvent " + newEvent);
                    }
                }
                log.info("newEvent:" + newEvent);
                System.out.println("newEvent " + newEvent);

                if (newEvent != null) {
                    events = newEvent; //overriding default event by event found from token
                    System.out.println("yoEvent is " + newEvent + " newEvents " + events);
                    conversion.setEventID(events.getId());
                    conversion.setEventName(events.getEventName());
                } else {
                    conversion.setConversionStatus(ConversionStatus.REJECTED);
                    conversion.setConversionMessage("Invalid token");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conversion;
    }




//    public Attributionconversion multiEventCheck(Attributionconversion conversion) {
//        try {
//            Long event1 = conversion.getEventID();
//            Event event = eventService.findById(event1);
//            System.out.println("eventsisi " + event1);
//            String clickId = conversion.getClickId();
//            if (!event.isMultiConv()) { //if conversion not approved, skip check
//                // record conversions in case of multi conversion but don't pass
//                System.out.println("isMultiConv " + event.isMultiConv());
//                Map<String, Object> map = new HashMap<>();
//                map.put("event", event);
//                map.put("clickId", clickId);
//
//                Integer conversionSize = conversionRepository.findByEvent_IdAndClickId(event.getId(), clickId).size();
//                System.out.println("conversionSize " + conversionSize);
//
//
//                if (conversionSize > 0) {
//                    System.out.println("conversionSize " + conversionSize);
//                    log.info("Multi conversions not supported on event");
//                    conversion.setConversionStatus(ConversionStatus.CANCELLED);
//                    conversion.setConversionMessage("Event already converted");
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return conversion;
//    }


/*    private Conversion passPostback(Conversion conversion, Click click, PublisherTrackingDTO publisher, OfferPostbackCacheDTO offerPostback) {
        try {
            String publisherPostback = publisher.getPb(); //throws NPE(Null Ptr Exc) with empty postback
            //stop private Event
            EventDTO event = conversion.getEvent();
//        log.info("eventAccess:"+event);
            if (event.getAccessStatus() == AccessStatus.Private)
                conversion.setConversionDetails(ConversionStatus.CANCELLED, "Private Event");
            if (publisherPostback == null)
                conversion.setConversionDetails(ConversionStatus.CANCELLED, "No Publisher Postback");
            if (conversion.getConversionStatus() == ConversionStatus.APPROVED) {
                if(offerPostback!=null && publisher.getId().equals(offerPostback.getPubId()) && event.getId().equals(offerPostback.getEventId()))
                    conversion = sendPostbackPublisher(conversion, offerPostback.getPostbackUrl(), click.getPubClickId());
                else
                    conversion = sendPostbackPublisher(conversion, publisherPostback, click.getPubClickId());
            }
            log.info("conversion:" + conversion.getConversionStatus());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conversion;
    }*/

    /*public void saveConversionDailyRecord(Conversion conversion) {
        try {
//            Conversion conversion = (Conversion) map.get("conversion");
            log.info("clickId:" + conversion.getClickId());
            String clickId = conversion.getClickId();
            Click click = restTemplate.getForEntity(BASE_URL + "/click/findById?id=" + conversion.getClickId(), Click.class).getBody();

            Click click = clickQueryBuilder.findById(clickId);
            DateFormat dateFormat = new SimpleDateFormat(ElasticSearchConstants.chrField_HourTimestampPattern);
            String dayTimestamp = dateFormat.format(new Date());
            CampaignHourlyRecordId campaignHourlyRecordId = new CampaignHourlyRecordId(
                    click.getOfferId(),
                    click.getPubId(),
                    click.getSubAff().replaceAll("[{}]", ""),
                    dayTimestamp,
                    conversion.getEvent().getId()
            );
            OfferTrackingDTO offerTrackingDTO = offerTrackingDTORepository.findById(click.getOfferId()).orElse(null);
            CampaignHourlyRecord campaignHourlyRecord = campaignHourlyRecordRepository.findById(campaignHourlyRecordId).orElse(null);
            if (campaignHourlyRecord == null) {
                campaignHourlyRecord = new CampaignHourlyRecord(campaignHourlyRecordId, offerTrackingDTO.getAdvertiserId());
                campaignHourlyRecord.setClickCount(1);
            }
            switch (conversion.getConversionStatus()) {
                case APPROVED:
                    campaignHourlyRecord.setApprovedConversions(campaignHourlyRecord.getApprovedConversions() + 1);
                    campaignHourlyRecord.setApprovedRevenue(campaignHourlyRecord.getApprovedRevenue() + conversion.getEvent().getRevenue());
                    campaignHourlyRecord.setApprovedPayout(campaignHourlyRecord.getApprovedPayout() + conversion.getEvent().getPayout());
                    break;
                case PENDING:
                    campaignHourlyRecord.setPendingConversions(campaignHourlyRecord.getPendingConversions() + 1);
                    campaignHourlyRecord.setPendingRevenue((campaignHourlyRecord.getPendingRevenue() + conversion.getEvent().getRevenue()));
                    break;
                case CANCELLED:
                    campaignHourlyRecord.setCancelledConversions(campaignHourlyRecord.getCancelledConversions() + 1);
                    campaignHourlyRecord.setCancelledRevenue(campaignHourlyRecord.getCancelledRevenue() + conversion.getEvent().getRevenue());
                    break;
            }
            campaignHourlyRecordRepository.save(campaignHourlyRecord);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    /*private Conversion passPostback(Conversion conversion, Click click, PublisherTrackingDTO publisher, OfferPostbackCacheDTO offerPostback) {
        try {
            String publisherPostback = publisher.getPb(); //throws NPE(Null Ptr Exc) with empty postback
            //stop private Event
            EventDTO event = conversion.getEvent();
//        log.info("eventAccess:"+event);
            if (event.getAccessStatus() == AccessStatus.Private)
                conversion.setConversionDetails(ConversionStatus.CANCELLED, "Private Event");
            if (publisherPostback == null)
                conversion.setConversionDetails(ConversionStatus.CANCELLED, "No Publisher Postback");
            if (conversion.getConversionStatus() == ConversionStatus.APPROVED) {
                if(offerPostback!=null && publisher.getId().equals(offerPostback.getPubId()) && event.getId().equals(offerPostback.getEventId()))
                    conversion = sendPostbackPublisher(conversion, offerPostback.getPostbackUrl(), click.getPubClickId());
                else
                    conversion = sendPostbackPublisher(conversion, publisherPostback, click.getPubClickId());
            }
            log.info("conversion:" + conversion.getConversionStatus());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conversion;
    }*/

    /*public ResponseEntity<?> getPostbackResponse(String uuid, String token, HttpServletRequest request) {
        CountryEnum country = CountryEnum.ALL;
        //Fetching country from header only in production environment.
        if (getEnvironment().equals("prod")) {
            try {
                country = CountryEnum.valueOf(request.getHeader("CF-IPCountry"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        Map<String, Object> map = new HashMap<>();
        map.put("uuid", uuid);
        map.put("token", token);
        map.put("date", new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").format(new Date()));
        map.put("debug", "/pb?click_id=" + uuid + "&token=" + token);
        map.put("country",country);
        template.convertSendAndReceive
                ("app1-exchange", "Conversion-routing-key", map);
        ResponseEntity<?> response = ResponseEntity.ok().body("Conversion Registered");
        return response;
    }*/

    //    public void saveAttributionConversion(Map map) {
//        System.out.println("Map is " + map);
//        Long appId = (Long) map.get("app_id");
//        Long pId = (Long) map.get("p_id");
//        String token = (String) map.get("token");
//        String agent = (String) map.get("agent");
//        String gaid = (String) map.get("gaid");
//        String ip = (String) map.get("ip");
//        String s1 = (String) map.get("s1");
//        CountryEnum country = (CountryEnum) map.get("country");
//        Boolean isApk = (Boolean) map.get("isApk");
//        String debug = (String) map.get("debug");
////        String date = (String) map.get("date");
//        Attributionconversion conversions = new Attributionconversion();
//        String date = conversions.getCreationDateTime();
//        String date1 = conversions.getUpdateDateTime();
//        System.out.println("date is " + date);
//
//        System.out.println("appId is " + appId);
//        System.out.println("pId is " + pId);
//        System.out.println("token is " + token);
//        System.out.println("agent is " + agent);
//        System.out.println("gaid is " + gaid);
//        System.out.println("ip is " + ip);
//        System.out.println("country is " + country);
//        System.out.println("isApk is " + isApk);
//
//
//        AttributionDeviceId deviceId = getDeviceId(agent);
//
////        log.info("appId:" + appId + " token:" + token + " agent:" + agent + " gaid:" + gaid + " ip:" + ip + " s4:" + s4 + " s5:" + s5 + " isApk:" + isApk + " pId:" + pId);
//        System.out.println("DeviceIdis = " + deviceId);
//
//        App app = appRepository.findById(appId).orElse(null);
////        OfferTrackingDTO offer = offerTrackingDTORepository.findById(appId).orElse(null);
//        System.out.println("app = " + app.getName());
//
////        OfferPostbackCacheDTO offerPostback=offerPostbackDTORepository.findByOfferId(offer.getId());
//
//        Partner Partner = partnerRepository.findById(pId).orElse(null);
//        System.out.println("Partner = " + Partner.getName());
////        PublisherTrackingDTO publisher = publisherTrackingDTORepository.findById(pId).orElse(null);
//        AttributionClick click = null;
//        Map<String, Object> map1 = new HashMap<>();
//        map1.put("agent", agent);
//        map1.put("deviceId", deviceId);
//        map1.put("ip", ip);
//        map1.put("app_id", appId);
////        click = restTemplate.postForEntity(BASE_URL + "/click/findFirstByDeviceIdAndIpAndOfferIdOrderByCreationDateTimeDesc",
////                map1, Click.class).getBody();
////        System.out.println("data click = "+click);
////        if (isApk && click == null) {
////            click = new Click(app, gaid, deviceId, ip, agent, "apk_", s1, Partner, (String) map.get("date"));
////        } else if(click == null) {
////            click = restTemplate.getForEntity(BASE_URL + "/click/findFirstByOfferIdAndGaidOrderByCreationDateTimeDesc?appId="
////                    + appId + "&gaid=" + gaid, Click.class).getBody();
////        }
//
//        if (click == null) {
//            System.out.println("insideIfClickNull " + click);
//            click = new AttributionClick(app, gaid, deviceId, ip, agent, "", s1, Partner
//                    , date);
//            System.out.println("insideIfClickNull2 " + click);
//        }
//
//        if (!gaid.isEmpty() && click.getGaid().isEmpty()) {
//            click.setGaid(gaid);
//            System.out.println("gaidEmpty " + gaid);
//        }
//
//        if (country != null && click.getCountry() == null) {
//            click.setCountry(country);
//            System.out.println("countryEmpty " + country);
//        }
//
//        System.out.println("ClickID ID " + click.getId());
//
//        List<Attributionconversion> conversionList = conversionRepository.findByClickId(click.getId());
//        System.out.println("conversionList " + conversionList.size());
//        System.out.println("debug is " + debug);
//
//        Attributionconversion conversion = new Attributionconversion(debug, date, date1);
//        if (ip.isEmpty() || agent.isEmpty()) {
//            System.out.println("ip.isEmpty() " + ip);
//            conversion.setConversionStatus(ConversionStatus.REJECTED);
//            conversion.setConversionMessage("Attribution Failed");
//        } else if (conversionList.size() == 0) {
//            conversion.setConversionStatus(ConversionStatus.APPROVED);
//            conversion.setPartnerId(Partner.getId());
//            conversion.setPartnerName(Partner.getName());
////            conversion.setPublisher(new PublisherConversionDTO(publisher.getFname(), publisher.getId()));
//            conversion.setAppId(app.getId());
//            conversion.setAppName(app.getName());
////            conversion.setOffer(new OfferConversionDTO(offer.getId(), offer.getOfferName()));
//
//            conversion = setEvent(token, click, conversion);
//            System.out.println("conversion setEvent " + conversion);
//
//            conversion = multiEventCheck(conversion);
//            System.out.println("conversion multiEventCheck " + conversion);
//
//            saveOrUpdateClick(click, conversion, conversion.getConversionStatus(), s1, gaid);
//
//            log.info("setConversionStatus:" + conversion.getConversionStatus());
//
//
////            conversion = passPostback(conversion, click, publisher, offerPostback);
//
////            Map<String, Object> map1 = new HashMap<>();
////            map1.put("conversion", conversion);
////            saveConversionDailyRecord(conversion);
////            template.convertAndSend("app1-exchange", "CDR-routing-key", map1);
//        }
//        conversionRepository.save(conversion);
//        /*if (conversionList.size() == 0) {
//            System.out.println("inside coversion save " + conversionList.size());
//            conversionRepository.save(conversion);
//        }*/
//    }


}
