package com.attribution.panel.service;

import com.attribution.panel.beans.Impression;
import com.attribution.panel.beans.InstallGeoWise;
import com.attribution.panel.beans.InstallReport;
import com.attribution.panel.cache.dto.AppTrackingDTO;
import com.attribution.panel.cache.dto.PartnerTrackingDTO;
import com.attribution.panel.cache.repository.AppTrackingDTORepository;
import com.attribution.panel.cache.repository.PartnerTrackingDTORepository;
import com.attribution.panel.constants.UrlConstants;
import com.attribution.panel.dto.InstallReportDTO;
import com.attribution.panel.elasticsearchRepo.ImpressionRepository;
import com.attribution.panel.elasticsearchRepo.InstallReportRepository;
import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CityResponse;
import com.maxmind.geoip2.record.*;
import nl.basjes.parse.useragent.UserAgent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.*;


@Service
public class InstallReportService {

    @Autowired
    InstallReportRepository installReportRepository;

    @Autowired
    AppTrackingDTORepository appTrackingDTORepository;

    @Autowired
    PartnerTrackingDTORepository partnerTrackingDTORepository;

    @Autowired
    AttributionAllReportService attributionAllReportService;

    @Autowired
    TrackUtilityService trackUtilityService;

    @Autowired
    ImpressionRepository impressionRepository;

    @Autowired
    ImpressionService impressionService;

    @Autowired
    InstallGeoWiseService installGeoWiseService;


    public InstallReport findByAppIdAndGaid(Long appId, String google_aid) {
        return installReportRepository.findByAppIdAndGaid(appId, google_aid);
    }

    public void save(InstallReport installReport) {
        installReportRepository.save(installReport);
    }

    public List<InstallReport> findByCountryAndAppId(String country, Long appId) {
        return installReportRepository.findByCountryAndAppId(country, appId);
    }

    public void saveInstallReport(InstallReportDTO installReportDTO) throws IOException, GeoIp2Exception {
        String ip = null;
        Optional<AppTrackingDTO> app = appTrackingDTORepository.findById(installReportDTO.getAppId());
        Optional<PartnerTrackingDTO> partner = partnerTrackingDTORepository.findById(installReportDTO.getPartnerId());

        String date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").format(new Date());
        System.out.println("date is " + date);

        ip = "122.160.74.236";

        File database = new File(UrlConstants.DATABASE_CITY_PATH);
        File database1 = new File(UrlConstants.DATABASE_COUNTRY_PATH);
        DatabaseReader reader = new DatabaseReader.Builder(database).build();


//        File dbFile = new File(UrlConstants.DATABASE_CITY_PATH);

        // This creates the DatabaseReader object,
        // which should be reused across lookups.

//        DatabaseReader reader = new DatabaseReader.Builder(dbFile).build();

        // A IP Address
        InetAddress ipAddress = InetAddress.getByName(ip);
        System.out.println("InetAddress " + ipAddress);

        // Get City info
        CityResponse response = reader.city(ipAddress);

        // Country Info
        Country country = response.getCountry();
        System.out.println("Country IsoCode: " + country.getIsoCode()); // 'US'
        System.out.println("Country Name: " + country.getName()); // 'United States'
        System.out.println(country.getNames().get("zh-CN")); // '美国'

        Subdivision subdivision = response.getMostSpecificSubdivision();
        System.out.println("Subdivision Name: " + subdivision.getName()); // 'Minnesota'
        System.out.println("Subdivision IsoCode: " + subdivision.getIsoCode()); // 'MN'

        // City Info.
        City city = response.getCity();
        System.out.println("City Name: " + city.getName()); // 'Minneapolis'

        // Postal info
        Postal postal = response.getPostal();
        System.out.println(postal.getCode()); // '55455'

        // Geo Location info.
        Location location = response.getLocation();

        // Latitude
        System.out.println("Latitude: " + location.getLatitude()); // 44.9733

        // Longitude
        System.out.println("Longitude: " + location.getLongitude()); // -93.2323

        String postals = String.valueOf(postal);
        Map<String, Object> map1 = new HashMap<>();
        map1.put("countryIsoCode", country.getIsoCode());
        map1.put("countryName", country.getName());
        map1.put("subdivisionIsoCode", subdivision.getIsoCode());
        map1.put("subdivisionName", subdivision.getName());
        map1.put("city", city.getName());
        map1.put("Latitude", location.getLatitude());
        map1.put("Longitude", location.getLongitude());
        map1.put("postal", postals);
        map1.put("ip", ip);


        UserAgent userAgent = trackUtilityService.getDeviceId(installReportDTO.getAgent());
        System.out.println("useragentsIsis " + userAgent.getUserAgentString());


        System.out.println("countryCode " + country);

        InstallReport installReport = findByAppIdAndGaid(installReportDTO.getAppId(), installReportDTO.getGaid());
        System.out.println("InstallReport " + installReport);

        if (installReport == null) {
            installReport = new InstallReport();
            installReport.setId(UUID.randomUUID().toString().replace("-", ""));
            installReport.setIp(ip);
            installReport.setCountry(country.getName());
            installReport.setDeviceName(installReportDTO.getDeviceName());
            installReport.setGaid(installReportDTO.getGaid());
            installReport.setOs(installReportDTO.getOs());
//            installReport.setApp(app);
            installReport.setAppId(installReportDTO.getAppId());
            installReport.setAppName(installReportDTO.getAppName());
            installReport.setPartnerId(installReportDTO.getPartnerId());
            installReport.setPartnerName(installReportDTO.getPartnerName());
            installReport.setStatus(true);
            installReport.setCreationDateTime(date);
            save(installReport);

            Impression impression = new Impression();
            impression.setId(UUID.randomUUID().toString().replace("-", ""));
            impression.setIp(ip);
            impression.setCountry(country.getName());
            impression.setDeviceName(installReportDTO.getDeviceName());
            impression.setGoogle_aid(installReportDTO.getGaid());
            impression.setOs(installReportDTO.getOs());
            impression.setAppId(installReportDTO.getAppId());
            impression.setAppName(installReportDTO.getAppName());
            impression.setPartnerId(installReportDTO.getPartnerId());
            impression.setPartnerName(installReportDTO.getPartnerName());
            impression.setCreationDateTime(date);
//            impression.setApp(app);
            impressionService.save(impression);
            attributionAllReportService.saveImpresionDailyAllReportRecord(userAgent, map1, impression);
        } else {
            installReport.setStatus(true);
            save(installReport);
        }

        List<InstallReport> count1 = findByCountryAndAppId(country.getName(), installReportDTO.getAppId());

        System.out.println("size is " + count1.size());
        int count = count1.size();

        InstallGeoWise installGeoWise = installGeoWiseService.findByCountryCode(country.getIsoCode());
        if (installGeoWise == null) {
            installGeoWise = new InstallGeoWise();
            installGeoWise.setCountryCode(country.getIsoCode());
            installGeoWise.setCount(count);
            installGeoWise.setAppId(installReportDTO.getAppId());
            installReport.setAppName(installReportDTO.getAppName());
            installReport.setPartnerId(installReportDTO.getPartnerId());
            installReport.setPartnerName(installReportDTO.getPartnerName());

        } else {
            installGeoWise.setCount(count);
        }
        installGeoWiseService.save(installGeoWise);
        return ;
    }


}
