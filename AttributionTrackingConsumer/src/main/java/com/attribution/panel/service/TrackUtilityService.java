package com.attribution.panel.service;

import com.attribution.panel.dto.AttributionDeviceId;
import com.fasterxml.jackson.databind.ObjectMapper;
import nl.basjes.parse.useragent.UserAgent;
import nl.basjes.parse.useragent.UserAgentAnalyzer;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
class TrackUtilityService {
    static UserAgentAnalyzer uaa;
    static UserAgent userAgent;
    static ObjectMapper mapper;

    @PostConstruct
    public void initialize() {
        uaa = UserAgentAnalyzer.newBuilder().hideMatcherLoadStats().withCache(10000).build();
        userAgent = uaa.parse("PostmanRuntime/7.6.0");
        mapper = new ObjectMapper();
    }

    public UserAgent getDeviceId(String agent) {
        UserAgent userAgent = uaa.parse(agent);
        return userAgent;
    }
}
