//package com.attribution.panel.service;
//
//import com.etreetech.panel.beans.Click;
//import com.etreetech.panel.dto.ClickTrackingDTO;
//import com.etreetech.panel.dto.AttributionDeviceId;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.amqp.rabbit.annotation.RabbitListener;
//import org.springframework.amqp.rabbit.core.RabbitTemplate;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.core.env.Environment;
//import org.springframework.stereotype.Service;
//import org.springframework.web.client.RestTemplate;
//
//import javax.annotation.PostConstruct;
//import java.time.LocalDateTime;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//
//@Service
//@Slf4j
//public class TrackService extends TrackUtilityService {
//
//    @Autowired
//    AsyncService asyncService;
//    static ObjectMapper mapper;
//    @Autowired
//    Environment environment;
//    @Autowired
//    RabbitTemplate template;
//
//    public String getEnvironment() {
//        String[] activeProfiles = environment.getActiveProfiles();
//        return activeProfiles[0];
//    }
//
//    @PostConstruct
//    public void initialize() {
//        mapper = new ObjectMapper();
//    }
//
//    RestTemplate restTemplate = new RestTemplate();
//    @Autowired
//    ClickBulkIndexService clickBulkIndexService;
//
//    volatile List<Click> clickListForBulkSave = new ArrayList<>();
//    volatile LocalDateTime lastClickPersistDuration = LocalDateTime.now();
//    volatile Integer ifCount = 0;
//    volatile Integer bulkInsertCount = 0;
//    volatile Integer saveClickQueueCount = 0;
//
//    @RabbitListener(queues = "saveClick")
//    //Getting click from rabbitmq and saving click to elasticsearch.
//    public void saveClickQueue(Map<String, Object> map) {
//        try {
//            saveClickQueueCount++;
//            ClickTrackingDTO clickTrackingDTO = (ClickTrackingDTO) map.get("click");
//            Click click = new Click(clickTrackingDTO);
//            AttributionDeviceId deviceId = getDeviceId(clickTrackingDTO.getAgent());
//            click.setDeviceId(deviceId);
//            if (getEnvironment().equals("test")) {
//                asyncService.saveClick(click);
//            } else {
//                clickBulkIndexService.saveBulk(click);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//
///*    private synchronized void bulkInsert(Click click) {
//        bulkInsertCount++;
//        clickListForBulkSave.add(click);
//        if (clickListForBulkSave.size() == 200 || LocalDateTime.now().toEpochSecond(ZoneOffset.UTC) -
//                lastClickPersistDuration.toEpochSecond(ZoneOffset.UTC) > 30) {
//            ifCount++;
////            log.info("LocalDateTime Diff in Seconds:" +
////                    (LocalDateTime.now().toEpochSecond(ZoneOffset.UTC) - lastClickPersistDuration.toEpochSecond(ZoneOffset.UTC)));
////            restTemplate.put(BASE_URL + "/click/saveBulk", clickListForBulkSave);
//            clickService.saveBulk(clickListForBulkSave);
//            clickListForBulkSave.clear();
//            lastClickPersistDuration = LocalDateTime.now();
//        }
//    }*/
//
///*
//    @RabbitListener(queues = "saveCampaignHourlyRecord")
//    public void updateCHRRejectedClick(Map map) {
//        log.info("Inside updateCHRRejected click:");
//        Long offerId = (Long) map.get("offerId");
//        Long pubId = (Long) map.get("pubId");
//        String subAff = (String) map.get("subAff");
//        Offer offer = restTemplate.getForEntity(BASE_URL + "/offer/findById?id=" + offerId, Offer.class).getBody();
//        List<Event> events = offer.getEvents();
//        Event event = events.get(0);
//        String dayTimestamp = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
//        CampaignHourlyRecordId campaignHourlyRecordId = new CampaignHourlyRecordId(
//                offerId, pubId, subAff, dayTimestamp, event.getId());
//
//        CampaignHourlyRecord campaignHourlyRecord = restTemplate.postForEntity(BASE_URL + "/campaignHourlyRecord/findById", campaignHourlyRecordId, CampaignHourlyRecord.class).getBody();
//        if (campaignHourlyRecord == null) {
//            campaignHourlyRecord = new CampaignHourlyRecord(campaignHourlyRecordId, offer.getAdvertiserId());
//        }
//        campaignHourlyRecord.setRejectedClicks(campaignHourlyRecord.getRejectedClicks() + 1);
//        restTemplate.put(BASE_URL + "/campaignHourlyRecord/save", campaignHourlyRecord);
//    }
//*/
//
//    public void reset() {
//        ifCount = 0;
//        bulkInsertCount = 0;
//        saveClickQueueCount = 0;
//    }
//
//    public Map<String, Integer> count() {
//        Map<String, Integer> map = new HashMap<>();
//        map.put("ifCount:", ifCount);
//        map.put("bulkInsertCount:", bulkInsertCount);
//        map.put("saveClickQueueCount:", saveClickQueueCount);
//        return map;
//    }
//}
