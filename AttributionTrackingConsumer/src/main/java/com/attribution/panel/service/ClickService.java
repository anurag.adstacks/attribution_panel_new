package com.attribution.panel.service;

import com.attribution.panel.beans.AttributionClick;
import com.attribution.panel.dao.ClickQueryBuilder;
import com.attribution.panel.elasticsearchRepo.ClickRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class ClickService {

    @Autowired
    ClickRepo clickRepository;

    @Autowired
    ClickQueryBuilder clickQueryBuilders;

    volatile Integer bulkInsertCount = 0;
    volatile Integer saveClickQueueCount = 0;

    public List<AttributionClick> findAll() {
        return (List<AttributionClick>) clickRepository.findAll();
    }

    @Async("taskExecutor")
    public void saveClick(AttributionClick click) {
        System.out.println("saveClick  " + click);

        clickQueryBuilders.save(click);


        /*try {
            saveClickQueueCount++;
//            ClickTrackingDTO clickTrackingDTO = (ClickTrackingDTO) map.get("click");
//            AttributionClick clicks = new AttributionClick(click);
            DeviceId deviceId = getDeviceId(clickTrackingDTO.getAgent());
            click.setDeviceId(deviceId);
            if (getEnvironment().equals("test")) {
                asyncService.saveClick(click);
            } else {
                clickBulkIndexService.saveBulk(click);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }*/

    }



}
