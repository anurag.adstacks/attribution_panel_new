package com.attribution.panel.service;

import com.attribution.panel.beans.AttributionClick;
import com.attribution.panel.constants.ElasticSearchConstants;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.bulk.BackoffPolicy;
import org.elasticsearch.action.bulk.BulkProcessor;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.ByteSizeUnit;
import org.elasticsearch.common.unit.ByteSizeValue;
import org.elasticsearch.common.unit.TimeValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
//class for bulk indexing/persisting clicks in elasticsearch
public class ClickBulkIndexService {
    @Autowired
    @Qualifier("elasticClient")
    RestHighLevelClient client;
    DateFormat dateFormat = new SimpleDateFormat(ElasticSearchConstants.clickField_UpdationDateTime_Pattern);

    public ClickBulkIndexService() throws IOException {
    }

    BulkProcessor bulkProcessor = BulkProcessor.builder(
            (request, bulkListener) ->
                    client.bulkAsync(request, RequestOptions.DEFAULT, bulkListener),
            new BulkProcessor.Listener() {
                @Override
                public void beforeBulk(long executionId, BulkRequest request) {
                    int numberOfActions = request.numberOfActions();
//                    log.info("Executing bulk [{}] with {} requests::::",
//                            executionId, numberOfActions);
                }

                @Override
                public void afterBulk(long executionId, BulkRequest request,
                                      BulkResponse response) {
                    if (response.hasFailures()) {
//                        log.info("Bulk [{}] executed with failures", executionId);
                    } else {
//                        log.info("Bulk [{}] completed in {} milliseconds",
//                                executionId, response.getTook().getMillis());
                    }
                }

                @Override
                public void afterBulk(long executionId, BulkRequest request,
                                      Throwable failure) {
//                    log.info("Failed to execute bulk", failure);
                }
            })
            .setBulkActions(10000)
            .setBulkSize(new ByteSizeValue(5L, ByteSizeUnit.MB))
            .setConcurrentRequests(1)
            .setFlushInterval(TimeValue.timeValueSeconds(30L))
            .setBackoffPolicy(BackoffPolicy.constantBackoff(TimeValue.timeValueSeconds(1L), 3))
            .build();

    public void saveBulk(AttributionClick click) {
        click.setUpdateDateTime(dateFormat.format(new Date()));
        ObjectMapper objectMapper = new ObjectMapper();
        ZonedDateTime zd = ZonedDateTime.now(ZoneId.of("Asia/Kolkata"));
        String index = "";
        String pattern = ElasticSearchConstants.clickIndexNameDatePattern;
        if (zd.getHour() >= 0 && zd.getHour() <= 5) {
            index += zd.withHour(6).withMinute(0).withSecond(0)
                    .format(DateTimeFormatter.ofPattern(pattern));
        } else if (zd.getHour() >= 6 && zd.getHour() <= 11) {
            index += zd.withHour(12).withMinute(0).withSecond(0)
                    .format(DateTimeFormatter.ofPattern(pattern));

        } else if (zd.getHour() >= 12 && zd.getHour() <= 17) {
            index += zd.withHour(18).withMinute(0).withSecond(0)
                    .format(DateTimeFormatter.ofPattern(pattern));

        } else if (zd.getHour() >= 18 && zd.getHour() <= 23) {
            index += zd.plusDays(1).withHour(0).withMinute(0).withSecond(0).
                    format(DateTimeFormatter.ofPattern(pattern));
        }
        IndexRequest indexRequest = new IndexRequest(ElasticSearchConstants.clickIndexNamePrefix + index).
                id(click.getId()).
                source(objectMapper.convertValue(click, Map.class));
        bulkProcessor.add(indexRequest);
    }

    @PreDestroy
    public void close() {
        try {
            bulkProcessor.flush();
            bulkProcessor.awaitClose(10, TimeUnit.MINUTES);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
