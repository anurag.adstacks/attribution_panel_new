package com.attribution.panel.service;


import com.attribution.panel.beans.AttributionAllReport;
import com.attribution.panel.beans.AttributionClick;
import com.attribution.panel.beans.Attributionconversion;
import com.attribution.panel.beans.Impression;
import com.attribution.panel.cache.dto.AppTrackingDTO;
import com.attribution.panel.cache.dto.EventCacheDTO;
import com.attribution.panel.cache.repository.AppTrackingDTORepository;
import com.attribution.panel.cache.repository.EventCacheDTORepository;
import com.attribution.panel.cache.repository.PartnerTrackingDTORepository;
import com.attribution.panel.dao.ReportQueryBuilder;
import com.attribution.panel.elasticsearchRepo.AttributionAllReportRepo;
import com.attribution.panel.elasticsearchRepo.AttributionRecordRepo;
import com.attribution.panel.elasticsearchRepo.ClickRepo;
import com.attribution.panel.elasticsearchRepo.ConversionRepo;
import lombok.extern.slf4j.Slf4j;
import nl.basjes.parse.useragent.UserAgent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Map;
import java.util.Optional;

@Service
@Slf4j
@Transactional
public class AttributionAllReportService {

    @Autowired
    Environment environment;
    @Autowired
    IpRequestService ipRequestService;

    @Autowired
    AppTrackingDTORepository appRepository;

    @Autowired
    PartnerTrackingDTORepository partnerRepository;

    @Autowired
    ConversionRepo conversionRepository;

    @Autowired
    AttributionAllReportRepo attributionAllReportRepo;

    @Autowired
    AttributionRecordService attributionRecordService;

    @Autowired
    AttributionRecordRepo attributionRecordRepo;

    @Autowired
    ClickRepo clickRepository;

//    @Autowired
//    ClickService clickService;

//    @Autowired
//    AppService appService;

//    @Autowired
//    EventService eventService;

    @Autowired
    EventCacheDTORepository eventRepository;

    @Autowired
    ClickRepo clickRepo;

    @Autowired
    ReportQueryBuilder reportQueryBuilder;


    public void saveConversionDailyAllReportRecord(Attributionconversion conversion, AttributionClick attributionclick, String dayTimestamp, boolean isClickFound, UserAgent userAgent, Map deviceLocation, Impression impression) {

        System.out.println("apppsps is " + attributionclick.getAppID() + " " + conversion.getAppId());
        Optional<AppTrackingDTO> app = appRepository.findById(attributionclick.getAppID());
        Optional<EventCacheDTO> event = null;
        if (conversion.getId() != null) {
            event = eventRepository.findById(conversion.getEventID());
            System.out.println("apppsps is1 " + event.get().getEventName());
        }


        System.out.println("apppsps is " + conversion.getEventID());

        System.out.println("apppsps is2 " + app.get().getName());
        System.out.println("apppsps is3 " + impression);


        try {
//            Conversion conversion = (Conversion) map.get("conversion");
//            log.info("clickId:" + conversion.getClickId());
//            String clickId = conversion.getClickId();
            System.out.println("saveConversionDailyRecord1 " + attributionclick.getIp());
            System.out.println("saveConversionDailyRecord " + conversion);


//            AttributionClick click = clickRepository.findById(conversion.getClickId()).orElse(null);
//            System.out.println("clickcount0  " + click);


//            App app = appRepository.findById(clickAttribution.getAppID()).orElse(null);

//            List<Attributionconversion> conversionList = clickRepository.findByClickId(clickAttribution.getId());
//            System.out.println("hwlloMrHowDoUdo2 " + conversionList);

            AttributionAllReport attributionAllReport = null;

            if (conversion.getId() == null) {
                System.out.println("horlyhourlu");
//                attributionAllReport = attributionAllReportRepo.findByAppIdAndPartnerIdAndDayTimestamp(attributionclick.getAppID(), attributionclick.getPId(), dayTimestamp);
                attributionAllReport = new AttributionAllReport(conversion, dayTimestamp, attributionclick, app, userAgent, deviceLocation, event);
                saveAttributionAllReportRecord(conversion, attributionAllReport, attributionclick, isClickFound);
                System.out.println("horlyhourlu2" + attributionAllReport);
                System.out.println("horlyhourlu4 " + attributionAllReport);
                /*if (attributionAllReport == null) {
                    attributionAllReport = new AttributionAllReport(conversion, dayTimestamp, attributionclick, app, userAgent, deviceLocation, event);
                    saveAttributionAllReportRecord(conversion, attributionAllReport, attributionclick, isClickFound);
                    System.out.println("horlyhourlu2" + attributionAllReport);
                } else {
                    saveAttributionAllReportRecord(conversion, attributionAllReport, attributionclick, isClickFound);
                    System.out.println("horlyhourlu3" + attributionAllReport);
                }*/
            } else {
                attributionAllReport = new AttributionAllReport(conversion, dayTimestamp, attributionclick, app, userAgent, deviceLocation, event);
                saveAttributionAllReportRecord(conversion, attributionAllReport, attributionclick, isClickFound);
                System.out.println("horlyhourlu5" + attributionAllReport);
//                attributionAllReport = attributionAllReportRepo.findByAppIdAndPartnerIdAndDayTimestamp(conversion.getAppId(), conversion.getPartnerId(), dayTimestamp);
                System.out.println("horlyhourlu8 " + attributionAllReport);
                /*if (attributionAllReport == null) {
                    attributionAllReport = new AttributionAllReport(conversion, dayTimestamp, attributionclick, app, userAgent, deviceLocation, event);
                    saveAttributionAllReportRecord(conversion, attributionAllReport, attributionclick, isClickFound);
                    System.out.println("horlyhourlu5" + attributionAllReport);
                } else {
                    saveAttributionAllReportRecord(conversion, attributionAllReport, attributionclick, isClickFound);
                    System.out.println("horlyhourlu6" + attributionAllReport);
                }
                System.out.println("horlyhourlu7" + attributionAllReport);*/
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void saveImpresionDailyAllReportRecord(UserAgent userAgent, Map deviceLocation, Impression impression) {

        Optional<AppTrackingDTO> app = appRepository.findById(impression.getAppId());
        EventCacheDTO event = null;

//        event = eventService.findById(conversion.getEventID());
//        System.out.println("apppsps is1 " + event.getEventName());


//        System.out.println("apppsps is " + conversion.getEventID());

        System.out.println("apppsps is2 " + app.get().getName());
        System.out.println("apppsps is3 " + impression);

        System.out.println("horlyhourlu");
        AttributionAllReport attributionAllReport = null;

        attributionAllReport = new AttributionAllReport(app, userAgent, deviceLocation, impression);
//        saveAttributionAllReportRecord(conversion, attributionAllReport, attributionclick, isClickFound);
        System.out.println("horlyhourlu2" + attributionAllReport);

        save(attributionAllReport);


    }


    public void saveAttributionAllReportRecord(Attributionconversion conversion, AttributionAllReport attributionAllReport, AttributionClick attributionclick, boolean isClickFound) {


        String sDate = "Apr 1, 2023";
        String eDate = "Apr 5, 2023";
        Long count = null;
        System.out.println("thisIsSaveConversionDailyRecord0  " + conversion.getAppId() + " " + attributionclick.getPId());

        System.out.println("thisIsSaveConversionDailyRecord0000  " + count);

        System.out.println("clickcount6  " + attributionAllReport);
        save(attributionAllReport);

    }

    public void save(AttributionAllReport attributionAllReport) {
        attributionAllReportRepo.save(attributionAllReport);
    }


}
