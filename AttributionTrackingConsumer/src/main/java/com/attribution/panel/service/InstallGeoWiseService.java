package com.attribution.panel.service;

import com.attribution.panel.beans.InstallGeoWise;
import com.attribution.panel.elasticsearchRepo.InstallGeoWiseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class InstallGeoWiseService {
    @Autowired
    InstallGeoWiseRepository installGeoWiseRepository;

    public InstallGeoWise findByCountryCode(String country) {
        return installGeoWiseRepository.findByCountryCode(country);
    }

    public void save(InstallGeoWise installGeoWise) {
        installGeoWiseRepository.save(installGeoWise);
    }

    public List<InstallGeoWise> findByAll() {
        return (List<InstallGeoWise>) installGeoWiseRepository.findAll();
    }

    public List<InstallGeoWise> findByAppId(Long appId) {
        return installGeoWiseRepository.findByAppId(appId);
    }
}
