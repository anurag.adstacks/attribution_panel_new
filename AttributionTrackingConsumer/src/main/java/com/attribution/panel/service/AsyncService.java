//package com.attribution.panel.service;
//
//import com.etreetech.panel.beans.Click;
//import com.etreetech.panel.dao.ClickQueryBuilder;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.scheduling.annotation.Async;
//import org.springframework.stereotype.Service;
//
//@Service
//public class AsyncService {
//    @Autowired
//    ClickQueryBuilder clickQueryBuilder;
//    Logger log = LogManager.getLogger();
//
//    @Async("taskExecutor")
//    public void saveClick(Click click) {
//        try{
//            clickQueryBuilder.save(click);
//        }
//        catch (Exception e){
//            e.printStackTrace();
//        }
//    }
//
//}
