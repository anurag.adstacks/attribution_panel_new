package com.attribution.panel.dao;

import com.alibaba.fastjson.JSON;
import com.attribution.panel.beans.AttributionClick;
import com.attribution.panel.constants.ElasticSearchConstants;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@Slf4j
public class ClickQueryBuilder {
    @Autowired
    @Qualifier("elasticClient")
    RestHighLevelClient client;

    public void save(AttributionClick click) {
        ObjectMapper objectMapper = new ObjectMapper();
        System.out.println("Inside click save");
        log.info("Inside click save");
        IndexRequest indexRequest = new IndexRequest("attributionclick").
                id(click.getId()).
                source(objectMapper.convertValue(click, Map.class));
        try {
            IndexResponse indexResponse = client.index(indexRequest, RequestOptions.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public AttributionClick findById(String id) {
        BoolQueryBuilder query = QueryBuilders.boolQuery();
        query.must(QueryBuilders.termQuery("id", id));
        SearchRequest searchRequest = new SearchRequest(ElasticSearchConstants.clickIndexSearchPattern);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(query);
        searchRequest.source(searchSourceBuilder);
        SearchResponse response = null;
//        log.info("searchRequest:" + searchRequest.toString());
        try {
            response = client.search(searchRequest, RequestOptions.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
        }
        SearchHit[] searchHits = response.getHits().getHits();
//        log.info("response:" + response.toString());
        AttributionClick click = null;
        try {
            click = JSON.parseObject(searchHits[0].getSourceAsString(), AttributionClick.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return click;
    }


//    public Long countByOfferIdAndPubIdAndCreationDateTimeBetween(Long offerId, Long pubId, String startDate, String endDate) {
//        BoolQueryBuilder query = QueryBuilders.boolQuery();
//        query.must(QueryBuilders.termQuery("offerId", offerId));
//        query.must(QueryBuilders.termQuery("pubId", pubId));
//        query.must(QueryBuilders.rangeQuery("creationDateTime").gte(startDate).lte(endDate));
//        long count = 0L;
//        CountRequest countRequest = new CountRequest(ElasticSearchConstants.clickIndexSearchPattern);
//        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
//        searchSourceBuilder.query(query);
//        countRequest.source(searchSourceBuilder);
//        try {
//            CountResponse countResponse = client.count(countRequest, RequestOptions.DEFAULT);
//            count = countResponse.getCount();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return count;
//    }
//
//
//
//    public Map<String,Long> countClickByOfferIdPubIdGroupBySubAffAndCreationDateTimeBetween(Long offerId, Long pubId,
//                                                                                String startDate, String endDate) {
//        BoolQueryBuilder query = QueryBuilders.boolQuery();
//        query.must(QueryBuilders.termQuery("offerId", offerId));
//        query.must(QueryBuilders.termQuery("pubId", pubId));
//        query.must(QueryBuilders.rangeQuery("creationDateTime").gte(startDate).lte(endDate));
//
//        List<CompositeValuesSourceBuilder<?>> sources = new ArrayList<>();
//        sources.add(new TermsValuesSourceBuilder("SubAff").field("subAff.keyword"));
//
//        CompositeAggregationBuilder compositeAggregationBuilder = new CompositeAggregationBuilder("SubAffBlock", sources)
//                .size(10000);
//
//        SearchRequest searchRequest = new SearchRequest(ElasticSearchConstants.clickIndexSearchPattern);
//        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
//        searchSourceBuilder.query(query);
//        searchSourceBuilder.aggregation(compositeAggregationBuilder);
//        searchRequest.source(searchSourceBuilder);
//        SearchResponse response = null;
//        Map<String,Long> clickCountSubAffWise = new HashMap<>();
//        try {
//            response = client.search(searchRequest, RequestOptions.DEFAULT);
//            ParsedComposite internalComposite = response.getAggregations().get("SubAffBlock");//get aggregation bucket from name
//            for (ParsedComposite.ParsedBucket termBucket : internalComposite.getBuckets()) {//Loop through array of buckets returned
//                    Map<String, Object> map = termBucket.getKey();
//                    String subAff = (String) map.get("SubAff");
//                    Long docCount = termBucket.getDocCount();
//                    log.info("click key:"+subAff);
//                    log.info("docCount:"+docCount);
//                    clickCountSubAffWise.put(subAff,docCount);
//                }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return clickCountSubAffWise;
//    }


}
