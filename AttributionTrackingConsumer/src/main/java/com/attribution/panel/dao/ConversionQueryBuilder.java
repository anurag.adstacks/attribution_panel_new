//package com.attribution.panel.dao;
//
//import com.etreetech.panel.enums.ConversionStatus;
//import lombok.extern.slf4j.Slf4j;
//import org.elasticsearch.action.search.SearchRequest;
//import org.elasticsearch.action.search.SearchResponse;
//import org.elasticsearch.client.RequestOptions;
//import org.elasticsearch.client.RestHighLevelClient;
//import org.elasticsearch.index.query.BoolQueryBuilder;
//import org.elasticsearch.index.query.QueryBuilders;
//import org.elasticsearch.search.aggregations.bucket.composite.CompositeAggregationBuilder;
//import org.elasticsearch.search.aggregations.bucket.composite.CompositeValuesSourceBuilder;
//import org.elasticsearch.search.aggregations.bucket.composite.ParsedComposite;
//import org.elasticsearch.search.aggregations.bucket.composite.TermsValuesSourceBuilder;
//import org.elasticsearch.search.builder.SearchSourceBuilder;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.stereotype.Component;
//
//import java.io.IOException;
//import java.util.*;
//
//@Component
//@Slf4j
//public class ConversionQueryBuilder {
//    @Autowired
//    @Qualifier("elasticClient")
//    RestHighLevelClient client;
//
//    public Map<String, Long> countConversionByConversionStatus_OfferId_PubId_GroupBySubAffAndCreationDateTimeBetween(ConversionStatus conversionStatus,
//                                                                                                                     Long offerId, Long pubId,
//                                                                                                                     String startDate, String endDate) {
//        log.info("Inside count conversion.......");
//        BoolQueryBuilder query = QueryBuilders.boolQuery();
//        query.must(QueryBuilders.termQuery("offer.offerId", offerId));
//        query.must(QueryBuilders.termQuery("publisher.publisherId", pubId));
//        query.must(QueryBuilders.termsQuery("conversionStatus", Collections.singletonList(conversionStatus)));
//        query.must(QueryBuilders.rangeQuery("creationDateTime").gte(startDate).lte(endDate));
//
//        List<CompositeValuesSourceBuilder<?>> sources = new ArrayList<>();
//        sources.add(new TermsValuesSourceBuilder("SubAff").field("subAff.keyword"));
//
//        CompositeAggregationBuilder compositeAggregationBuilder = new CompositeAggregationBuilder("SubAffBlock", sources)
//                .size(10000);
//
//        SearchRequest searchRequest = new SearchRequest("conversion");
//        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
//        searchSourceBuilder.query(query);
//        searchSourceBuilder.aggregation(compositeAggregationBuilder);
//        searchRequest.source(searchSourceBuilder);
//        SearchResponse response = null;
//        Map<String, Long> conversionCountSubAffWise = new HashMap<>();
//        try {
//            response = client.search(searchRequest, RequestOptions.DEFAULT);
//            ParsedComposite internalComposite = response.getAggregations().get("SubAffBlock");//get aggregation bucket from name
//            for (ParsedComposite.ParsedBucket termBucket : internalComposite.getBuckets()) {//Loop through array of buckets returned
//                Map<String, Object> map = termBucket.getKey();
//                String subAff = (String) map.get("SubAff");
//                Long docCount = termBucket.getDocCount();
//                log.info("conv key:" + subAff);
//                log.info("count:" + docCount);
//                conversionCountSubAffWise.put(subAff, docCount);
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return conversionCountSubAffWise;
//    }
//
//}
