package com.attribution.panel.dao;

import com.alibaba.fastjson.JSON;
import com.attribution.panel.beans.AttributionClick;
import com.attribution.panel.beans.Attributionconversion;
import com.attribution.panel.constants.ElasticSearchConstants;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.core.CountRequest;
import org.elasticsearch.client.core.CountResponse;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Slf4j
public class ReportQueryBuilder {

    @Autowired
    @Qualifier("elasticClient")
    RestHighLevelClient client;


    public List<Attributionconversion> findByClickIdAndAppIdAndPartnerIdOrderByCreationDateTimeDesc(String clickId, Long appId, Long partnerId /*, String startDate, String endDate*/) {

        BoolQueryBuilder query = QueryBuilders.boolQuery();
        query.must(QueryBuilders.matchQuery("clickId", clickId));
        query.must(QueryBuilders.matchQuery("appId", String.valueOf(appId)));
        query.must(QueryBuilders.termsQuery("partnerId", String.valueOf(partnerId)));
        SearchRequest searchRequest = new SearchRequest(ElasticSearchConstants.conversionIndexSearchPattern);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(query);
        searchRequest.source(searchSourceBuilder);
        SearchResponse response = null;
        try {
            response = client.search(searchRequest, RequestOptions.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
        }

        SearchHit[] searchHits = response.getHits().getHits();
        List<Attributionconversion> attributionconversionList =
                Arrays.stream(searchHits)
                        .map(hit -> JSON.parseObject(hit.getSourceAsString(), Attributionconversion.class))
                        .collect(Collectors.toList());

        System.out.println("getCLickListForIPAndOfferId " + attributionconversionList);
        return attributionconversionList;
    }


    public Long countByAppIdAndPartnerIdAndCreationDateTimeBetween(Long appId, Long partnerId /*, String startDate, String endDate*/) {
        BoolQueryBuilder query = QueryBuilders.boolQuery();
        query.must(QueryBuilders.termQuery("appId", appId));
        query.must(QueryBuilders.termQuery("partnerId", partnerId));
//        query.must(QueryBuilders.rangeQuery("creationDateTime").gte(startDate).lte(endDate));

        long count = 0L;
        CountRequest countRequest = new CountRequest(ElasticSearchConstants.impressionIndexSearchPattern);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(query);
        countRequest.source(searchSourceBuilder);
        System.out.println("countRequest " + countRequest);
        try {
            CountResponse countResponse = client.count(countRequest, RequestOptions.DEFAULT);
            count = countResponse.getCount();
            System.out.println("countRequest1 " + count);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return count;
    }


    public List<AttributionClick> getCLickListForIPAndOfferId(String ip, String offerId) {
        BoolQueryBuilder query = QueryBuilders.boolQuery();
        query.must(QueryBuilders.matchQuery("ip", ip));
        query.must(QueryBuilders.termsQuery("appID", offerId));
        SearchRequest searchRequest = new SearchRequest(ElasticSearchConstants.clickIndexSearchPattern);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(query);
        searchRequest.source(searchSourceBuilder);
        SearchResponse response = null;
        try {
            response = client.search(searchRequest, RequestOptions.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
        }

        SearchHit[] searchHits = response.getHits().getHits();
        List<AttributionClick> clickList =
                Arrays.stream(searchHits)
                        .map(hit -> JSON.parseObject(hit.getSourceAsString(), AttributionClick.class))
                        .collect(Collectors.toList());

        System.out.println("getCLickListForIPAndOfferId " + clickList);
        return clickList;
    }



}
