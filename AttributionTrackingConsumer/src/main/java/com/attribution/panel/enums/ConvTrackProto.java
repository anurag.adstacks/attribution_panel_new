package com.attribution.panel.enums;

/**
 * AttributionCountry pojo class.
 */

public enum ConvTrackProto {
	pb,		//server postback url
	ifp,	//iframePixel
	ip		//imagePixel
}
