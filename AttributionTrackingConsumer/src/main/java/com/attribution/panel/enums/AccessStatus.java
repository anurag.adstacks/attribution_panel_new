package com.attribution.panel.enums;

public enum AccessStatus {
	Public,
	NeedApproval,
	Private,
	Approved,
	Pending
	
}
