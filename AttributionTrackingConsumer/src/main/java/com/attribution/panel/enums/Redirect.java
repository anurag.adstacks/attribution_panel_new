package com.attribution.panel.enums;

public enum Redirect {
    CAMPAIGN, SMART_LINK, GLOBAL_REDIRECT
}
