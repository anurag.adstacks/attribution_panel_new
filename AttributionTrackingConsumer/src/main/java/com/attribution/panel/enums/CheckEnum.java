package com.attribution.panel.enums;

import java.io.Serializable;

//Entity Created to update Campaign Hourly Record by using a single function
public enum CheckEnum implements Serializable {
    Tracking,
    Conversion
}
