package com.attribution.panel.enums;

public enum Interval {
	Daily,
	Weekly,
	Monthly
}
