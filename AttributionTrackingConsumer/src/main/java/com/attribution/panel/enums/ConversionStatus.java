package com.attribution.panel.enums;

public enum ConversionStatus {
	APPROVED, REJECTED, PENDING, CANCELLED
}
