package com.attribution.panel.beans;


import com.attribution.panel.cache.dto.AppTrackingDTO;
import com.attribution.panel.cache.dto.PartnerTrackingDTO;
import com.attribution.panel.dto.AttributionClickTrackingDTO;
import com.attribution.panel.enums.ConversionStatus;
import lombok.Data;
import lombok.NoArgsConstructor;
import nl.basjes.parse.useragent.UserAgent;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.UUID;

@Data
@NoArgsConstructor
@Document(indexName = "attributionclick",shards = 6,refreshInterval = "30s",replicas = 1)
@Component
public class AttributionClick implements Serializable {

    private String id;

    private String pClickId;  // yeah jab publisher humara link lgaiega toh yeah id uski taraf se aiieghi
    private String gaid; //android device id
    private String idfa; //iOS device id
    private String ip;    //IP address
    private String country;

    private long durationInMillis; //Calculate total duration of time in completing a request.

    private Long pId; //partnerID
    private String pName;

    private int pResp;

    private String agent; //user agent..

    //We can use P for Passed and A for Approved
    private String clickMessage; //message for click rejection or approval
    private boolean isGross;

    private String subPid;

    private ConversionStatus conversionStatus;

    private String source;


    private String s1;

    private Long appID;
    private String appName;

    private float revenue;

    private float payout;



//    private AttributionDeviceId deviceId;
    private String operatingSystemNameVersion;
    private String deviceName;
    private String agentNameVersion;
    private String layoutEngineNameVersion;


    private String devOs;

    //    @Field(type = FieldType.Date, format = DateFormat.date_optional_time)

    //    @Field(type = FieldType.Date, format = DateFormat.date_optional_time)

    private String creationDateTime;

    private String updateDateTime;

    public AttributionClick(AttributionClickTrackingDTO attributionClick) {

        System.out.println("List<AttributionClick> " + attributionClick.getAgent());

        this.id = UUID.randomUUID().toString().replace("-", "");
        this.pClickId = attributionClick.getPClickId();
        this.agent = attributionClick.getAgent();
        this.agent = attributionClick.getAgent();
        this.isGross = attributionClick.isGross();
        this.subPid = attributionClick.getSubPid();
        this.source = attributionClick.getSource();
        this.devOs = attributionClick.getDevOs();
        this.s1 = attributionClick.getS1();
        this.pId = attributionClick.getPId();
        this.pName = attributionClick.getPName();
        this.appID = attributionClick.getAppID();
        this.appName = attributionClick.getAppName();
        this.creationDateTime = attributionClick.getCreationDateTime();
        this.updateDateTime = attributionClick.getUpdateDateTime();
        this.ip = attributionClick.getIp();
        this.country = attributionClick.getCountry();
        this.clickMessage = attributionClick.getClickMessage();
        this.conversionStatus = attributionClick.getConversionStatus();
        this.durationInMillis = attributionClick.getDurationInMillis();
        this.gaid = attributionClick.getGaid();
        this.idfa = attributionClick.getIdfa();
        this.payout = attributionClick.getPayout();
        this.pResp = attributionClick.getPResp();
        this.revenue = attributionClick.getRevenue();
        this.operatingSystemNameVersion = attributionClick.getOperatingSystemNameVersion();
        this.deviceName = attributionClick.getDeviceName();
        this.agentNameVersion = attributionClick.getAgentNameVersion();
        this.layoutEngineNameVersion = attributionClick.getLayoutEngineNameVersion();

    }

    public AttributionClick(AppTrackingDTO app, String gaid, String agents, String ip, UserAgent agent, String s, String s1, PartnerTrackingDTO partner, String date, String country) {
        this.id = UUID.randomUUID().toString().replace("-", "");;
        this.pClickId = "pClickId";
        this.gaid = gaid;
        this.idfa = "idfa";
        this.ip = ip;
        this.country = country;
        this.durationInMillis = 1;
        this.pId = partner.getId();
        this.pName=partner.getName();
        this.pResp = 1;
        this.agent = agents;
        this.clickMessage = "Passed: APPROVED";
        this.isGross = true;
        this.subPid = subPid;
        this.conversionStatus = ConversionStatus.APPROVED;
        this.source = s;
        this.s1 = s1;
        this.appID = app.getId();
        this.appName = app.getName();
        this.revenue = revenue;
        this.payout = payout;
        this.operatingSystemNameVersion = (agent.getValue("OperatingSystemNameVersion")==null)?"":agent.getValue("OperatingSystemNameVersion");
        this.deviceName = (agent.getValue("DeviceName")==null)?"":agent.getValue("DeviceName");
        this.agentNameVersion = (agent.getValue("AgentNameVersion")==null)?"":agent.getValue("AgentNameVersion");
        this.layoutEngineNameVersion = (agent.getValue("LayoutEngineNameVersion")==null)?"":agent.getValue("LayoutEngineNameVersion");
        this.devOs = devOs;
        this.creationDateTime = date;
        this.updateDateTime = date;
    }


}
