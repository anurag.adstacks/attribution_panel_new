package com.attribution.panel.beans;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.stereotype.Component;

import java.util.Date;

@Data
@NoArgsConstructor
@Document(indexName = "dailyactiveusers",shards = 1,replicas = 1)
@Component
public class DailyActiveUsers {

//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Long id;

    private String id;
    private String ip;
    private String os;
    private String gaid;
    private String deviceName;

    private String country;

//    @OneToOne(fetch = FetchType.EAGER)
//    @ToString.Exclude
//    private App app;

    private Long appID;
    private String appName;

    private Long partnerId;
    private String partnerName;


    private String creationDateTime;
}
