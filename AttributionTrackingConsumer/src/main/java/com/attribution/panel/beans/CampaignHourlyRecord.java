//package com.attribution.panel.beans;
//
//import lombok.Data;
//import lombok.EqualsAndHashCode;
//import lombok.NoArgsConstructor;
//import lombok.ToString;
//import org.springframework.data.elasticsearch.annotations.Document;
//
//import java.io.Serializable;
//
//@Data
//@EqualsAndHashCode(onlyExplicitlyIncluded = true)
//@ToString(onlyExplicitlyIncluded = true)
//@NoArgsConstructor
//@Document(indexName = "campaignhourlyrecord",shards = 1)
//
//public class CampaignHourlyRecord implements Serializable {
//    private static final long serialversionUID =
//            129348931L;
//    @EqualsAndHashCode.Include
//    @ToString.Include
//    private CampaignHourlyRecordId id;
//
//    private int approvedConversions;
//    private int cancelledConversions;
//    private int pendingConversions;
//    private int rejectedClicks;
//    private float approvedRevenue;
//    private float cancelledRevenue;
//    private float pendingRevenue;
//    private float approvedPayout;
//
//    private Long advertiserId;
//
//    private long clickCount;
//
//    public CampaignHourlyRecord(CampaignHourlyRecordId campaignHourlyRecordId, Long advertiserId) {
//        this.id = campaignHourlyRecordId;
//        this.advertiserId = advertiserId;
//    }
//
//}
