//package com.attribution.panel.beans;
//
//import lombok.Data;
//
//import java.io.Serializable;
//
//@Data
//public class CampaignHourlyRecordId implements Serializable {
//    private static final long serialversionUID =
//            12934893712123L;
//    private Long offerId;
//    private Long pubId;
//    private String subAffiliate;
//    private String hourTimestamp;
//    private Long eventId;
//
//    public CampaignHourlyRecordId(Long offerId, Long pubId, String subAffiliate, String hourTimestamp,Long eventId) {
//        this.offerId = offerId;
//        this.pubId = pubId;
//        this.subAffiliate = subAffiliate;
//        this.hourTimestamp = hourTimestamp;
//        this.eventId = eventId;
//    }
//
//    public CampaignHourlyRecordId() {
//
//    }
//}
