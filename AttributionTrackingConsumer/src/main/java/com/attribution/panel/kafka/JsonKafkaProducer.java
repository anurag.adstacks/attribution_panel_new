//package com.attribution.panel.kafka;
//
//
//import com.attribution.panel.dto.AttributionClickTrackingDTO;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.kafka.core.KafkaTemplate;
//import org.springframework.kafka.support.KafkaHeaders;
//import org.springframework.messaging.Message;
//import org.springframework.messaging.support.MessageBuilder;
//import org.springframework.stereotype.Service;
//
//@Service
//public class JsonKafkaProducer {
//
////    @Value("${spring.kafka.topic-json.name}")
////    private String topicJsonName;
//
//    private static final Logger LOGGER = LoggerFactory.getLogger(JsonKafkaProducer.class);
//
//    private KafkaTemplate<String, AttributionClickTrackingDTO> kafkaTemplate;
//
//    public JsonKafkaProducer(KafkaTemplate<String, AttributionClickTrackingDTO> kafkaTemplate) {
//        this.kafkaTemplate = kafkaTemplate;
//    }
//
//    public void sendMessage(AttributionClickTrackingDTO attributionClickTrackingDTO){
//
//        LOGGER.info(String.format("Message sent -> %s", attributionClickTrackingDTO.toString()));
//
//        Message<AttributionClickTrackingDTO> message = MessageBuilder
//                .withPayload(attributionClickTrackingDTO)
//                .setHeader(KafkaHeaders.TOPIC, "click")
//                .build();
//
//        kafkaTemplate.send(message);
//    }
//
//}
