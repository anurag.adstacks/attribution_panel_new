package com.attribution.panel.kafka;


import com.attribution.panel.beans.AttributionClick;
import com.attribution.panel.dto.AttributionClickTrackingDTO;
import com.attribution.panel.dto.ImpressionTrackingDTO;
import com.attribution.panel.service.ImpressionService;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequiredArgsConstructor
public class JsonImpressionKafkaConsumer {

    @Autowired
    ImpressionService impressionService;


    @KafkaListener(topics = "impression",
            clientIdPrefix = "json",
            containerFactory = "kafkaListenerContainerFactory")
    public void consume(ConsumerRecord<String, ImpressionTrackingDTO> record,
                        @Payload ImpressionTrackingDTO payload) throws IOException, GeoIp2Exception {

//        logger.info("Received Payload: {} | Record: {}", payload, record);

        System.out.println("payload " + payload);
        System.out.println("record " + record);

        impressionService.saveImpression(payload);



    }




}
