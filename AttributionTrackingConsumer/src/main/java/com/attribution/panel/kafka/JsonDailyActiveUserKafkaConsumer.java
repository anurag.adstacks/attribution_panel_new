package com.attribution.panel.kafka;


import com.attribution.panel.dto.DailyActiveUsersDTO;
import com.attribution.panel.service.DailyActiveUsersService;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequiredArgsConstructor
public class JsonDailyActiveUserKafkaConsumer {

    @Autowired
    DailyActiveUsersService dailyActiveUsersService;


    @KafkaListener(topics = "dailyactiveusers", clientIdPrefix = "json", containerFactory = "kafkaListenerContainerFactory")
    public void consume(ConsumerRecord<String, DailyActiveUsersDTO> record,
                        @Payload DailyActiveUsersDTO payload) throws IOException, GeoIp2Exception {

//        logger.info("Received Payload: {} | Record: {}", payload, record);

        System.out.println("payload " + payload);
        System.out.println("record " + record);

        dailyActiveUsersService.saveDailyActiveUsers(payload);

    }

}
