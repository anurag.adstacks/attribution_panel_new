package com.attribution.panel.kafka;

import com.attribution.panel.beans.AttributionAllReport;
import com.attribution.panel.dto.AttributionAllReportTrackingDTO;
import com.attribution.panel.elasticsearchRepo.AttributionAllReportRepo;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequiredArgsConstructor
public class JsonReportKafkaConsumer {

    @Autowired
    AttributionAllReportRepo attributionAllReportRepo;

    private static final Logger LOGGER = LoggerFactory.getLogger(JsonReportKafkaConsumer.class);


    @KafkaListener(topics = "report",
            clientIdPrefix = "json",
            containerFactory = "kafkaListenerContainerFactory")
    public void consume(ConsumerRecord<String, AttributionAllReportTrackingDTO> record,
                        @Payload AttributionAllReportTrackingDTO payload) {

//        logger.info("Received Payload: {} | Record: {}", payload, record);

        System.out.println("payload " + payload);
        System.out.println("record " + record);


        LOGGER.info(String.format("Event message received -> %s", payload + " " + record));
//        System.out.println("attributionClick " + attributionClick.getAgent());

        AttributionAllReport attributionAllReport = new AttributionAllReport(payload);

        //        System.out.println("attributionClick1 " + attributionClick1);


        save(attributionAllReport);
    }

    public void save(AttributionAllReport attributionAllReport) {
        System.out.println("SavedReport ");
        attributionAllReportRepo.save(attributionAllReport);
    }


}
