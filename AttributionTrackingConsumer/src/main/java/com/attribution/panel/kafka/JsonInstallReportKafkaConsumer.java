package com.attribution.panel.kafka;

import com.attribution.panel.dto.InstallReportDTO;
import com.attribution.panel.service.InstallReportService;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequiredArgsConstructor
public class JsonInstallReportKafkaConsumer {

    @Autowired
    InstallReportService installReportService;


    @KafkaListener(topics = "installreport",
            clientIdPrefix = "json",
            containerFactory = "kafkaListenerContainerFactory")
    public void consume(ConsumerRecord<String, InstallReportDTO> record,
                        @Payload InstallReportDTO payload) throws IOException, GeoIp2Exception {

//        logger.info("Received Payload: {} | Record: {}", payload, record);

        System.out.println("payload " + payload);
        System.out.println("record " + record);

//        impressionService.saveImpression(payload);

        installReportService.saveInstallReport(payload);

    }

}
