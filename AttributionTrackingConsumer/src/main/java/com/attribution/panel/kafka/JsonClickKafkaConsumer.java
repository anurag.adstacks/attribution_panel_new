package com.attribution.panel.kafka;

import com.attribution.panel.beans.AttributionClick;
import com.attribution.panel.dao.ClickQueryBuilder;
import com.attribution.panel.dto.AttributionClickTrackingDTO;
import com.attribution.panel.repository.ClickRepository;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class JsonClickKafkaConsumer {

    @Autowired
    ClickQueryBuilder clickQueryBuilder;

    @Autowired
    ClickRepository clickRepository;

    private static final Logger LOGGER = LoggerFactory.getLogger(JsonClickKafkaConsumer.class);


    @KafkaListener(topics = "click",
            clientIdPrefix = "json",
            containerFactory = "kafkaListenerContainerFactory")
    public void consume(ConsumerRecord<String, AttributionClickTrackingDTO> record,
                        @Payload AttributionClickTrackingDTO payload) {

//        logger.info("Received Payload: {} | Record: {}", payload, record);

        System.out.println("payload " + payload);
        System.out.println("record " + record);


        LOGGER.info(String.format("Event message received -> %s", payload + " " + record));
//        System.out.println("attributionClick " + attributionClick.getAgent());

        AttributionClick attributionClick1 = new AttributionClick(payload);
//        System.out.println("attributionClick1 " + attributionClick1);


        clickQueryBuilder.save(attributionClick1);
        clickRepository.save(attributionClick1);

    }





//    private static final Logger LOGGER = LoggerFactory.getLogger(JsonClickKafkaConsumer.class);
//
//    @KafkaListener(topics = "${spring.kafka.topic-json.name}", groupId = "${spring.kafka.consumer.group-id}")
//    public void consume(AttributionClickTrackingDTO attributionClickTrackingDTO){
//        System.out.println("Json message recieved " + attributionClickTrackingDTO.getAgent());
//        LOGGER.info(String.format("Json message recieved -> %s", attributionClickTrackingDTO.toString()));
//    }



}