package com.attribution.panel.kafka;


import com.attribution.panel.cache.dto.AppTrackingDTO;
import com.attribution.panel.cache.repository.AppTrackingDTORepository;
import com.attribution.panel.dto.ConversionMapDTO;
import com.attribution.panel.service.ConversionsService;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
public class JsonConversionKafkaConsumer {

    @Autowired
    AppTrackingDTORepository appTrackingDTORepository;

    @Autowired
    ConversionsService conversionsService;


    private static final Logger LOGGER = LoggerFactory.getLogger(JsonClickKafkaConsumer.class);


    @KafkaListener(topics = "conversion",
            clientIdPrefix = "json",
            containerFactory = "kafkaListenerContainerFactory")
    public void consume(ConsumerRecord<String, ConversionMapDTO> record,
                        @Payload ConversionMapDTO payload) throws IOException, GeoIp2Exception {

//        Optional<AppTrackingDTO> app = appTrackingDTORepository.findById(1L);
//        System.out.println("Appis " + app);

        System.out.println("payload " + payload);
        System.out.println("record " + record);

        conversionsService.saveAttributionPostback(payload);
        LOGGER.info(String.format("Event message received -> %s", payload + " " + record));




    }

}
