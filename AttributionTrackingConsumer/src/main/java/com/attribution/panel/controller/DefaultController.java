package com.attribution.panel.controller;

import com.attribution.panel.beans.AttributionClick;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class DefaultController {
//    @Autowired
//    RabbitTemplate template;

//    @Autowired
//    TrackService trackService;
//    @Autowired
//    ClickBulkIndexService clickBulkIndexService;
    @Autowired
    Environment environment;

    public String getEnvironment() {
        String[] activeProfiles = environment.getActiveProfiles();
        return activeProfiles[0];
    }

    @GetMapping("/")
    public ResponseEntity<?> response() {
        return ResponseEntity.ok().body("DefaultTrack Consumer Controller Executed");
    }

//    @GetMapping("/reset")
//    public ResponseEntity<?> reset() {
//        trackService.reset();
////        clickService.reset();
//        return ResponseEntity.ok().body("Done");
//    }

//    @GetMapping("/count")
//    public Map<String, Integer> count() {
//        Map<String,Integer> map = trackService.count();
////        map.put("indexRequestCount",clickService.count());
//        return map;
//    }

    @GetMapping("/test")
    public ResponseEntity<String> test() {
        List<AttributionClick> clickList = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            AttributionClick click = new AttributionClick();
            click.setClickMessage("Batch test click" + i);
            clickList.add(click);
        }
//        clickRepository.saveAll(clickList);
//        template.convertAndSend
//                ("app1-exchange", "bsRule-routing-key","");
        return ResponseEntity.ok().body("test Track Consumer Controller Executed");
    }

    @GetMapping("/getActiveApplicationProperty")
    public ResponseEntity<?> getApplicationProp(){
        return ResponseEntity.ok().header("env",getEnvironment()).body(getEnvironment());
    }
}
