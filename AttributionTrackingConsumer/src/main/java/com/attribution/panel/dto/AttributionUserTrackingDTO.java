package com.attribution.panel.dto;


import com.attribution.panel.enums.RoleEnum;
import com.attribution.panel.enums.StateEnum;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;
import java.io.Serializable;
import java.util.Set;

//@RedisHash("userCache")
@Data
@NoArgsConstructor
public class AttributionUserTrackingDTO implements Serializable{

    @Id
    private Long id;

    private String fName;

    private String lName;

    private String email;

    private Set<RoleEnum> roles;

    private String imType;

    private String password;

    private StateEnum state;

    private String confirmPassword;


    //    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Long id;
//
//    private String fName;
//
//    private String lName;
//
//    @Column(unique = true)
//    private String email;
//
//    @ElementCollection(targetClass = RoleEnum.class)
//    @CollectionTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"))
//    @Enumerated(EnumType.STRING)
//    @Column(name = "role_id")
//    private Set<RoleEnum> roles;
//
//    private String imType;
//
//    private String password;
//
//    @Enumerated(EnumType.STRING)
//    private StateEnum state;
//
//    private String confirmPassword;

}
