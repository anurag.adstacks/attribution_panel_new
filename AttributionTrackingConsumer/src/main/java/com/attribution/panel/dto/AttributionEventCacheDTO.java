package com.attribution.panel.dto;

import com.attribution.panel.enums.AccessStatus;
import com.attribution.panel.enums.ConversionPoint;
import com.attribution.panel.enums.PayoutModel;
import com.attribution.panel.enums.RevenueModel;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;
import java.io.Serializable;
import java.util.List;

//@RedisHash("eventCache")
@Data
@NoArgsConstructor
public class AttributionEventCacheDTO implements Serializable {


    @Id
    private Long id;

    private String eventName;
    private String eventValue;
    private double revenue;
    private String eventRevenueCurrency;
    private double eventRevenueUsd;

    private String description;

    private boolean status;

    private String token; //Event token to be integrated in postback url

    private float payout;

    private AttributionAppTrackingDTO app;

    private boolean privateEvent;

    private RevenueModel revenueModel;

    private PayoutModel payoutModel;

    private AccessStatus accessStatus;

    private boolean multiConv;

    private String eventKey;

    private int eventCount;

    private ConversionPoint convPoint;


    private String creationDateTime;    //date


    private String updateDateTime;

    private List<AttributionPartnerTrackingDTO> partners;


//    @Id
//    private Long id;
//
//    private String eventName;
//    private String eventValue;
//    private double revenue;
//    private String eventRevenueCurrency;
//    private double eventRevenueUsd;
//
//    private String description;
//
//    private boolean status;
//
//    private String token; //Event token to be integrated in postback url
//
//    private float payout;
//
//    private List<Long> app;
//
//    private boolean privateEvent;
//
//    private RevenueModel revenueModel;
//
//    private PayoutModel payoutModel;
//
//    private AccessStatus accessStatus;
//
//    private boolean multiConv;
//
//    private String eventKey;
//
//    private int eventCount;
//
//    private ConversionPoint convPoint;
//
//
//    private String creationDateTime;    //date
//
//
//    private String updateDateTime;
//
//    private List<Long> partners;



    /*@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @JsonProperty("id")
    private Long id;

    //    @JsonProperty("event_name")
    private String eventName;
    private String eventValue;
    private double revenue;
    private String eventRevenueCurrency;
    private double eventRevenueUsd;

    //    @JsonProperty("description")
    private String description;

    //	@JsonIgnore
    private boolean status;

    //	@JsonProperty("e_tkn")
    @EqualsAndHashCode.Include
    private String token; //Event token to be integrated in postback url

    //	@JsonIgnore

    private float payout;

    //	@JsonIgnore
//    @Enumerated(EnumType.STRING)
//    private ConvTrackProto proto;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "app_id", nullable = false)
    @JsonIgnore
    @ToString.Exclude
    @EqualsAndHashCode.Include
    @NotFound(action = NotFoundAction.IGNORE)
    private AttributionAppTrackingDTO app;

    //	@JsonIgnore
    private boolean privateEvent;

    //	@JsonIgnore
    @Enumerated(EnumType.STRING)
    private RevenueModel revenueModel;

    @Enumerated(EnumType.STRING)
    private PayoutModel payoutModel;

    //	@JsonIgnore
    @Enumerated(EnumType.STRING)
    private AccessStatus accessStatus;

    //	@JsonIgnore
    private boolean multiConv;

    private String eventKey;

    private int eventCount;

    @Enumerated(EnumType.STRING)
    private ConversionPoint convPoint;


    private String creationDateTime;    //date


    private String updateDateTime;

    @ManyToMany(cascade = CascadeType.REFRESH)
    private List<AttributionPartnerTrackingDTO> partners;*/


    /*    @Id
    private Long id;
    private String eventName;
    private String description;
    private boolean status;
    private String token; //Event token to be integrated in postback url
    private float revenue;
    private float payout;
    private ConvTrackProto proto;
    @Indexed
    private Long offerId;
    private boolean privateEvent;
    private RevenueModel revenueModel;
    private PayoutModel payoutModel;
    private AccessStatus accessStatus;
    private boolean multiConv;
    private ConversionPoint convPoint;
    private String creationDateTime;    //date
    private String updateDateTime;*/


}
