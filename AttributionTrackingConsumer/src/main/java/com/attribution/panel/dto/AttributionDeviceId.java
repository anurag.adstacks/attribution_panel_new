package com.attribution.panel.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.Data;
import nl.basjes.parse.useragent.UserAgent;

import java.io.Serializable;

@Data
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class AttributionDeviceId implements Serializable {
    private String OperatingSystemNameVersion;
    private String DeviceName;
    private String AgentNameVersion;
    private String LayoutEngineNameVersion;

    public AttributionDeviceId(UserAgent agent) {
        this.OperatingSystemNameVersion = (agent.getValue("OperatingSystemNameVersion")==null)?"":agent.getValue("OperatingSystemNameVersion");
        this.DeviceName = (agent.getValue("DeviceName")==null)?"":agent.getValue("DeviceName");
        this.AgentNameVersion = (agent.getValue("AgentNameVersion")==null)?"":agent.getValue("AgentNameVersion");
        this.LayoutEngineNameVersion = (agent.getValue("LayoutEngineNameVersion")==null)?"":agent.getValue("LayoutEngineNameVersion");
    }

    public AttributionDeviceId() {
    }
}
