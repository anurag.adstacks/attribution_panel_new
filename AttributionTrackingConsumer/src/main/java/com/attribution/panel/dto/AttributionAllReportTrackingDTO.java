package com.attribution.panel.dto;


import com.attribution.panel.cache.dto.AppTrackingDTO;
import com.attribution.panel.cache.dto.EventCacheDTO;
import lombok.Data;
import lombok.NoArgsConstructor;
import nl.basjes.parse.useragent.UserAgent;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

@Data
@NoArgsConstructor
public class AttributionAllReportTrackingDTO implements Serializable {

    private String id;


    //app
    private String appVersion;
    private String sdkVersion;
    private Long appId;
    private String appName;
    private String appType;
    private String bundleId;


    // event
    private String eventTime;
    private String eventName;
    private String eventValue;
    private double eventRevenue;
    private String eventRevenueCurrency;
    private String eventRevenueUSD;


    //device
    private String att; // App Tracking Transparency,
    private String wifi;
    private String operator;
    private String carrier;
    private String language;
    private String androidId;
    private String imei;
    private String idfa;
    private String idfv;
    private String advertisingId;
    private String deviceModel;
    private String deviceType;
    private String deviceCategory;
    private String platform;
    private String osVersion;
    private String userAgent;
    private String deviceDownloadTime;
    private String oaid;
    private boolean isLAT;


    //device location
    private String region;
    private String countryCode;
    private String state;
    private String city;
    private String postalCode;
    private String ip;




    //Attribution
    private String attributedTouchType;
    private String dayTimestamp;
    //    private String attributedTouchTime;
//    private Date installTime;
//    private String costModel;
//    private String costValue;
//    private String subParam1;
//    private String subParam2;
//    private String subParam3;
//    private String subParam4;
//    private String customerUserId;
//    private boolean isRetargeting;
//    private String storeProductPage;
//    private String costCurrency;
//    private String eventSource;
    private Long partnerId;
    private String partnerName;

//    private String mediaSource;
//    private String channel;
//    private String keywords;
//    private String retargetingConversionType;
//    private boolean isPrimaryAttribution;
//    private String attributionLookback;
//    private String reengagementWindow;
//    private String matchType;
//    private String httpReferrer;
//    private String campaign;
//    private String campaignId;
//    private String adset;
//    private String adsetId;
//    private String ad;
//    private String originalUrl;
//    private String adId;
//    private String adType;
//    private String siteId;



    // fraud
//    private String rejectedReason;
//    private String rejectedReasonValue;
//    private String blockedReason;
//    private String blockedReasonRule;
//    private String blockedReasonValue;
//    private String blockedSubReason;



}



