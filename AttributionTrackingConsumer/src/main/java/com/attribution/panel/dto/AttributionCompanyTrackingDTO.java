package com.attribution.panel.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;
import java.io.Serializable;

@Data
@NoArgsConstructor
public class AttributionCompanyTrackingDTO implements Serializable {

    @Id
    private Long id;

    private String fName;

    private String lName;

    private String email;

    private String imType;

    private String password;

    private Boolean status;

    private String confirmPassword;



//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Long id;
//
//    private String fName;
//
//    private String lName;
//
//    @Column(unique = true)
//    private String email;
//
//    private String imType;
//
//    private String password;
//
//    private Boolean status;
//
//    private String confirmPassword;




    /*@Id
    private Long id;
    private String fname;
    private String lname;
    private String email;
    private String phone;
    private String imType;
    private String imId;
    private String fax;
    private String company;
    private String jobTitle;
    private Integer numEmp;
    private String addr1;
    private String addr2;
    private String city;
    private String country;
    private String state;
    private String zip;
    private Double payout;
    private float cr;
    private Double revenue;
    private long totalClicks;
    private long grossClicks;
    private long conversions;
    private String creationDateTime;
    private String updateDateTime;
    private Boolean status;
    private List<Long> approvedPubs;
    private List<Long> blockedPubs;
    private Integer userId;
    private String uuid;*/

}
