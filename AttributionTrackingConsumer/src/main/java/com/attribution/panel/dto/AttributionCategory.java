package com.attribution.panel.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

//@Entity
//@Data
//@NoArgsConstructor
//@AllArgsConstructor
@Data
@NoArgsConstructor
public class AttributionCategory {

    private Long id;

    private String name;

    private Date creationDateTime; // date

    private Date updateDateTime;

    private AttributionAppTrackingDTO app;

//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Long id;
//
//    @Column(unique = true)
//    private String name;
//
//    @CreationTimestamp
//    private Date creationDateTime; // date
//
//    @UpdateTimestamp
//    private Date updateDateTime;
//
//    @ManyToOne(fetch = FetchType.EAGER)
//    @JoinColumn(name = "app_id", nullable = false)
//    @JsonIgnore
//    @ToString.Exclude
//    @EqualsAndHashCode.Include
//    private AttributionAppTrackingDTO app;

}
