package com.attribution.panel.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;
import java.io.Serializable;

//@Entity
//@Data
@Data
@NoArgsConstructor
public class AttributionCountry implements Serializable {

    @Id
    public Long id;

    public String shortName;

    public String name;

    public Integer phoneCode;

//    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
//    @Fetch(FetchMode.SELECT)
//    private List<State> state;
}
