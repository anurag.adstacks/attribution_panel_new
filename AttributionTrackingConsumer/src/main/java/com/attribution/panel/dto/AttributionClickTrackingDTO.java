package com.attribution.panel.dto;

import com.attribution.panel.enums.ConversionStatus;
import lombok.Data;
import lombok.NoArgsConstructor;
import nl.basjes.parse.useragent.UserAgent;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

@Data
@NoArgsConstructor
public class AttributionClickTrackingDTO implements Serializable {

    private String id;

    private String pClickId;  // yeah jab publisher humara link lgaiega toh yeah id uski taraf se aiieghi

    private String gaid; //android device id




    private String idfa; //iOS device id

    private String ip;    //IP address

    private String country;

    private long durationInMillis; //Calculate total duration of time in completing a request.

    private Long pId; //partnerID

    private String pName;

    private int pResp;

    private String agent; //user agent

    //We can use P for Passed and A for Approved
    private String clickMessage; //message for click rejection or approval
    private boolean isGross;

    private String subPid;

    private ConversionStatus conversionStatus;

    private String source;

    private String s1;

    private Long appID;

    private String appName;

    private float revenue;

    private float payout;



    //    private AttributionDeviceId deviceId;
    private String operatingSystemNameVersion;

    private String deviceName;

    private String agentNameVersion;

    private String layoutEngineNameVersion;

    private String devOs;

    private String creationDateTime;

    private String updateDateTime;


//    private static final long serialVersionUID = 6529685098267757690L;

    /*private String id;
    private String pubClickId; //pub click id
    private String gaid; //android device id
    private String idfa; //iOS device id
    private String ip;    //IP address
    private CountryEnum country;
    private long durationInMillis;

    private String agent; //user agent
    private String clickMessage; //message for click rejection or approval
    private boolean isGross; //is gross click?
    private String subAff;
    private String source;
    private String s1;
    private String s2;
    private String s3;
    private String s4;
    private String s5;

    private Long pubId;
    private Long advertiserId;

    private int status;

    private ConversionStatus conversionStatus;

    private int pubResponse;

    private Long offerId;

    private float revenue;

    private float payout;

    private String creationDateTime;

    private String updateDateTime;*/


    @Override
    public String toString() {
        return "AttributionClickTrackingDTO{" +
                "id='" + id + '\'' +
                ", pClickId='" + pClickId + '\'' +
                ", gaid='" + gaid + '\'' +
                ", idfa='" + idfa + '\'' +
                ", ip='" + ip + '\'' +
                ", country='" + country + '\'' +
                ", durationInMillis=" + durationInMillis +
                ", pId=" + pId +
                ", pName='" + pName + '\'' +
                ", pResp=" + pResp +
                ", agent='" + agent + '\'' +
                ", clickMessage='" + clickMessage + '\'' +
                ", isGross=" + isGross +
                ", subPid='" + subPid + '\'' +
                ", conversionStatus=" + conversionStatus +
                ", source='" + source + '\'' +
                ", s1='" + s1 + '\'' +
                ", appID=" + appID +
                ", appName='" + appName + '\'' +
                ", revenue=" + revenue +
                ", payout=" + payout +
                ", operatingSystemNameVersion='" + operatingSystemNameVersion + '\'' +
                ", deviceName='" + deviceName + '\'' +
                ", agentNameVersion='" + agentNameVersion + '\'' +
                ", layoutEngineNameVersion='" + layoutEngineNameVersion + '\'' +
                ", devOs='" + devOs + '\'' +
                ", creationDateTime='" + creationDateTime + '\'' +
                ", updateDateTime='" + updateDateTime + '\'' +
                '}';
    }

    public AttributionClickTrackingDTO(String pClickId, String devOs, UserAgent agent, String agentt, String subPid, String s1, String source, AttributionPartnerTrackingDTO partner, Long pId,
                                       AttributionAppTrackingDTO app, boolean isGross,
                                       String ip, String country) {
        this.id = UUID.randomUUID().toString().replace("-", "");
        this.pClickId = pClickId;
        this.agent = agentt;
        this.isGross = isGross;
        this.subPid = subPid;
        this.source = source;
        this.devOs = devOs;
        this.s1 = s1;
        this.pId = pId;
        this.pName = partner.getName();
        this.appID = app.getId();
        this.appName = app.getName();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        this.creationDateTime = dateFormat.format(new Date());
        this.updateDateTime = dateFormat.format(new Date());
//        this.creationDateTime = getCreationDateTime();
//        this.updateDateTime = getUpdateDateTime();
        this.ip = ip;
        this.country = country;
        this.operatingSystemNameVersion = (agent.getValue("OperatingSystemNameVersion")==null)?"":agent.getValue("OperatingSystemNameVersion");
        this.deviceName = (agent.getValue("DeviceName")==null)?"":agent.getValue("DeviceName");
        this.agentNameVersion = (agent.getValue("AgentNameVersion")==null)?"":agent.getValue("AgentNameVersion");
        this.layoutEngineNameVersion = (agent.getValue("LayoutEngineNameVersion")==null)?"":agent.getValue("LayoutEngineNameVersion");
    }


    public AttributionClickTrackingDTO(AttributionAppTrackingDTO app, String gaid, String agents, String ip, UserAgent agent, String subPid, String s1, AttributionPartnerTrackingDTO partner, String date) {

        this.id = UUID.randomUUID().toString().replace("-", "");
        this.gaid = gaid;
        this.s1 = s1;
//        this.deviceId = deviceId;
//        this.deviceId = getDeviceId();
        this.agent = agents;
        this.ip = ip;
        this.subPid = subPid;
        this.pId = partner.getId();
        this.pName=partner.getName();
        this.appID = app.getId();
        this.appName = app.getName();
        this.isGross = true;
        this.clickMessage="Passed: APPROVED";
//        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
//        this.creationDateTime = dateFormat.format(new Date());
//        this.updateDateTime = dateFormat.format(new Date(date));
        this.creationDateTime=date;
        this.updateDateTime=date;
        this.operatingSystemNameVersion = (agent.getValue("OperatingSystemNameVersion")==null)?"":agent.getValue("OperatingSystemNameVersion");
        this.deviceName = (agent.getValue("DeviceName")==null)?"":agent.getValue("DeviceName");
        this.agentNameVersion = (agent.getValue("AgentNameVersion")==null)?"":agent.getValue("AgentNameVersion");
        this.layoutEngineNameVersion = (agent.getValue("LayoutEngineNameVersion")==null)?"":agent.getValue("LayoutEngineNameVersion");
    }


}
