package com.attribution.panel.elasticsearchRepo;

import com.attribution.panel.beans.InstallGeoWise;
import com.attribution.panel.dto.InstallGeoWiseDTO;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InstallGeoWiseRepository extends ElasticsearchRepository<InstallGeoWise, String> {
    InstallGeoWise findByCountryCode(String toString);

    List<InstallGeoWise> findByAppId(Long appId);
}
