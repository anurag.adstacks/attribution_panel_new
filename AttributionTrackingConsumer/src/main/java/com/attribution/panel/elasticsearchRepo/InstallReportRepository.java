package com.attribution.panel.elasticsearchRepo;

import com.attribution.panel.beans.InstallReport;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InstallReportRepository extends ElasticsearchRepository<InstallReport, String> {

    InstallReport findByAppIdAndGaid(Long appId, String google_aid);

    List<InstallReport> findByCountryAndAppId(String country, Long appId);


//    int findByCountryAndAppId(CountryEnum country, Long appId);

//    @Query(value = "select count(*) from install_report where app_id=:appId And creation_date_time BETWEEN :startDate AND :endDate", nativeQuery = true)
//    Long findByAppIDAndStartBetweenEndDate(@Param("appId") Long appId, @Param("startDate") String startDate, @Param("endDate") String endDate);
//    @Query(value = "select count(*) from install_report where app_id=:appId And country=:country", nativeQuery = true)
//    int findByCountryAndAppId(@Param("country") String country, @Param("appId") Long appId);
//    @Query(value = "SELECT HOUR(creation_date_time) hr, COUNT(DISTINCT id) count FROM install_report where app_id=:appId And creation_date_time between :startDate and :endDate GROUP BY hr;", nativeQuery = true)
//    List<String> findByAppIdAndHourCount(@Param("appId") Long appId, @Param("startDate") String startDate, @Param("endDate") String endDate);
//    @Query(value = "SELECT Date(creation_date_time), COUNT(*) FROM install_report where app_id=:appId And creation_date_time between :startDate and :endDate GROUP BY Date(creation_date_time);", nativeQuery = true)
//    List<String> findByAppIdAndDateWiseCount(@Param("appId") Long appId, @Param("startDate") String startDate, @Param("endDate") String endDate);
//    @Query(value = "select * from install_report where app_id=:appId And google_aid=:google_aid", nativeQuery = true)
//    InstallReportDTO findByAppIdAndGoogle_aid(@Param("appId") Long appId, @Param("google_aid") String google_aid);
//    @Query(value = "select * from install_report where app_id=:appId And google_aid=:google_aid And status=:status", nativeQuery = true)
//    InstallReportDTO findByAppIdAndGoogle_aidAndStatus(@Param("appId") Long appId, @Param("google_aid") String google_aid, @Param("status") boolean status);
//    @Query(value = "select count(*) from install_report where app_id=:appId And update_date_time BETWEEN :startDate AND :endDate", nativeQuery = true)
//    Long findActiveUserByAppIdAndStartBetweenEndDate(@Param("appId") Long appId, @Param("startDate") String startDate, @Param("endDate") String endDate);
//    @Query(value = "select count(*) from install_report where app_id=:appId", nativeQuery = true)
//    Long findByAppId(@Param("appId") Long appId);
//    @Query(value = "SELECT country, COUNT(*) as count FROM install_report where app_id=:appId And creation_date_time between :startDate and :endDate GROUP BY country;", nativeQuery = true)
//    List<String> findByAppIdAndCountryWiseCount(@Param("appId") Long appId, @Param("startDate") String startDate, @Param("endDate") String endDate);
//    @Query(value = "select * from install_report where app_id=:appId And google_aid=:google_aid And creation_date_time Like :currentDate%", nativeQuery = true)
//    InstallReportDTO findByAppIdAndGoogle_aidAndDate(@Param("appId") Long appId, @Param("google_aid") String google_aid, @Param("currentDate") String currentDate);
//

}
