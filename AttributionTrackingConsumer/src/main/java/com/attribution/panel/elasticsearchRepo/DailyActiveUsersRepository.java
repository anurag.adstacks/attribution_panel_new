package com.attribution.panel.elasticsearchRepo;

import com.attribution.panel.beans.DailyActiveUsers;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DailyActiveUsersRepository extends ElasticsearchRepository<DailyActiveUsers, String> {

    DailyActiveUsers findByAppIDAndGaid(Long appID, String gaid, String creationDateTime);

//    DailyActiveUsersDTO findByAppIDAndGoogle_Aid(Long appId, String google_aid, String currentDate);


//    @Query(value = "select * from daily_active_users where app_id=:appId And google_aid=:google_aid And creation_date_time Like :currentDate%", nativeQuery = true)
//    DailyActiveUsersDTO findByAppAndGoogle_aid(@Param("appId") Long appId, @Param("google_aid") String google_aid, @Param("currentDate") String currentDate);
//    @Query(value = "select count(*) from daily_active_users where app_id=:appId And creation_date_time Like :currentDate%", nativeQuery = true)
//    Long findByAppIDAndToday(@Param("appId") Long appId, @Param("currentDate") String currentDate);



}
