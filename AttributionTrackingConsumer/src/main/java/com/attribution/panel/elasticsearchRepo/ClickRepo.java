package com.attribution.panel.elasticsearchRepo;

import com.attribution.panel.beans.AttributionClick;
import com.attribution.panel.beans.Attributionconversion;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClickRepo extends ElasticsearchRepository<AttributionClick, String> {


    List<AttributionClick> findByAgentAndIpAndAppID(String agent, String ip, Long offerId);

    List<AttributionClick> findByOperatingSystemNameVersionAndDeviceNameAndAgentNameVersionAndLayoutEngineNameVersionAndIpAndAppID(String operatingSystemNameVersion, String deviceName, String agentNameVersion, String layoutEngineNameVersion, String ip, Long offerId);

    List<Attributionconversion> findByClickId(String id);


}
