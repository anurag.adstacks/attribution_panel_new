package com.attribution.panel.elasticsearchRepo;

import com.attribution.panel.beans.Attributionconversion;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface ConversionRepo extends ElasticsearchRepository<Attributionconversion, String> {


//    List<Attributionconversion> findByClickId(String id);

    List<Attributionconversion> findByClickIdAndAppIdAndPartnerIdOrderByCreationDateTimeDesc(String clickId, Long appId, Long pId);

    Map<Object, Object> findByEventIDAndClickId(Long id, String clickId);

}
