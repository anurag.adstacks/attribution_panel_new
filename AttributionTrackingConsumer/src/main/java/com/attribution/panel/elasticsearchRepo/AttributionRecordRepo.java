package com.attribution.panel.elasticsearchRepo;

import com.attribution.panel.beans.AttributionRecord;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface AttributionRecordRepo extends ElasticsearchRepository<AttributionRecord, String> {
    AttributionRecord findByAppIdAndPartnerIdAndDayTimestamp(Long appId, Long partnerId, String dayTimestamp);
}

