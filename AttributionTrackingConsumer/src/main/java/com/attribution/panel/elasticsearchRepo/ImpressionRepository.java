package com.attribution.panel.elasticsearchRepo;

import com.attribution.panel.beans.Impression;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ImpressionRepository extends ElasticsearchRepository<Impression, String> {


/*    @Query(value = "select count(*) from impression where app_id=:appId And creation_date_time BETWEEN :startDate AND :endDate", nativeQuery = true)
    Long findByAppIDAndStartBetweenEndDate(@Param("appId") Long appId, @Param("startDate") String startDate, @Param("endDate") String endDate);
    @Query(value = "select count(*) from impression where app_id=:appId And creation_date_time Like :currentDate%", nativeQuery = true)
    Long findByAppIDAndToday(@Param("appId") Long appId, @Param("currentDate") String currentDate);
    @Query(value = "select * from impression where app_id=:appId And google_aid=:google_aid And creation_date_time Like :currentDate%", nativeQuery = true)
    Impression findByAppAndGoogle_aid(@Param("appId") Long appId, @Param("google_aid") String google_aid, @Param("currentDate") String currentDate);*/


}
