package com.attribution.panel.cache.repository;

import com.attribution.panel.cache.dto.PartnerTrackingDTO;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PartnerTrackingDTORepository extends CrudRepository<PartnerTrackingDTO, Long> {



}
