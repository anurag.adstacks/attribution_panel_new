package com.attribution.panel.cache.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Id;
import java.io.Serializable;

//@Entity
//@Data
@RedisHash("countryCache")
@Data
@NoArgsConstructor
public class Country implements Serializable {

    @Id
    public Long id;

    public String shortName;

    public String name;

    public Integer phoneCode;

//    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
//    @Fetch(FetchMode.SELECT)
//    private List<State> state;
}
