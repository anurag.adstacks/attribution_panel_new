package com.attribution.panel.cache.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@RedisHash("partnerCache")
@Data
@NoArgsConstructor
public class PartnerTrackingDTO implements Serializable {

    @Id
    private Long id;

    private String name;

    private String email;

    private String phone;

    private String company;

    private String country;

    private String state;

    private Boolean status;

    private String postBack;

    private Date creationDateTime;    //date

    private Date updateDateTime;

    private String uuid = UUID.randomUUID().toString().replace("-", "");

    /*@Id
    private Long id;
    private String fname;
    private String lname;
    private String email;
    private String phone;
    private String imType;
    private String imId;
    private String fax;
    private String company;
    private String jobTitle;
    private Integer numEmp;
    private String addr1;
    private String addr2;
    private String city;
    private String country;
    private String state;
    private String zip;
    private Boolean status;
    private String pb;
    private float payout;
    private Float cutFactor;
    private Integer offset;
    private String uuid;
    private float revenue;
    private long totalClicks;
    private long grossClicks;
    private long conversions;
    private String creationDateTime;
    private String updateDateTime;
    //    private CampaignRule rule;
    private Integer userId;*/

}
