package com.attribution.panel.enums;

public enum AppStatus {

	Active,
	Pause,
	Archived,
	Pending

}
