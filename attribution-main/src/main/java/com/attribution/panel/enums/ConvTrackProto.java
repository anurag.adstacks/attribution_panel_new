package com.attribution.panel.enums;

/**
 * Country pojo class.
 */

public enum ConvTrackProto {
	pb,		//server postback url
	ifp,	//iframePixel
	ip		//imagePixel
}
