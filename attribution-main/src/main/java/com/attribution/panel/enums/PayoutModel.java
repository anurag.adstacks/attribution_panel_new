package com.attribution.panel.enums;

/**
 * Payout Model Enum.
 */

public enum PayoutModel {

	CPA,
	CPI,
	CPC,
	CPL,
	CPS,
	CPAS

}
