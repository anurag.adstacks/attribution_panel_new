package com.attribution.panel.enums;

public enum OS {
    ANDROID,
    IOS,
    Windows,
    Web,
    Roku,
    Vizio,
    Samsung,
    LG
}
