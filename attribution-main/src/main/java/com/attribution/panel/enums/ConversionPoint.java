package com.attribution.panel.enums;

/**
 * Payout Model Enum.
 */

public enum ConversionPoint {
	dep,	//deposit
	reg,	//registration
	pin,	//pin-submit	
	sms,	//SMS confirmation
	sale,	//sale offer/credit required
	/*@JsonProperty("Download and Install")*/
	install,//Download & Install
	soi,	//SOI
	doi,	//DOI
	email	//Email Submit
}
