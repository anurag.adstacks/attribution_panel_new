package com.attribution.panel.console.Controller;

import com.attribution.panel.bean.User;
import com.attribution.panel.console.service.*;
import com.attribution.panel.constants.BackendConstants;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(value = "/adminConsole")
@Slf4j
public class UserEventController {
    @Autowired
    UserEventService userEventService;
    @Autowired
    EventService eventService;
    @Autowired
    IpRequestService ipRequestService;
    @Autowired
    AppService appService;

    @Autowired
    BackendConstants backendConstants;

    RestTemplate restTemplate = new RestTemplate();

    @Autowired
    UserService userService;


    @RequestMapping(value = "/findByUserEmail/{value}", method = RequestMethod.GET)
    public Map<String, Object> findByUserEmail(@PathVariable("value") String value) {
        Map<String, Object> data = new HashMap<String, Object>();
        User user = userService.findUserByEmail(value);
        data.put("user", user);
        if (user == null) {
            data.put("response", false);
        } else {
            data.put("response", true);
        }
        return data;
    }

    @PostMapping(value = "/getUserEventAjax")
    public ResponseEntity<?> getUserEventAjax(@RequestBody Map<String, Object> input) {

        System.out.println("yoyoy " + input.get("id") + " bobo " + input.get("dataTablesInput"));

        ObjectMapper objectMapper = new ObjectMapper();
        Long appId = objectMapper.convertValue(input.get("id"), Long.class);
        DataTablesInput search = objectMapper.convertValue(input.get("dataTablesInput"), DataTablesInput.class);


        Map<String, Object> res = userEventService.listUserEventAjax(search, appId);

        return ResponseEntity.ok().body(res);
    }


//    @PostMapping(path = "/allReport")
//    public ResponseEntity<?> getAllReport(@RequestBody Map<String, Object> input) {
//        System.out.println("yoyoy " + input.get("id") + " bobo " + input.get("dataTablesInput"));
//
////        Integer appId = (Integer) input.get("id");
////        Object dataTablesInput = input.get("dataTablesInput");
//
//        ObjectMapper objectMapper = new ObjectMapper();
//        Long appId = objectMapper.convertValue(input.get("id"), Long.class);
//        DataTablesInput dataTablesInput = objectMapper.convertValue(input.get("dataTablesInput"), DataTablesInput.class);
//
//        Map<String, Object> res = reportService.reportAllReportAjax(appId, dataTablesInput);
//
//        return ResponseEntity.ok().body(res);
//    }



//
//
//    @RequestMapping(value = "/findByEventAndGoogle_aid/{appId}/{event}/{google_aid}", method = RequestMethod.GET)
//    public Long findByEventKey(@PathVariable("appId") Long appId,
//                               @PathVariable("event") Event event,
//                               @PathVariable("google_aid") String google_aid) {
//        System.out.println("findByEventAndGoogle_aid ");
//        return userEventService.findByEventAndGoogle_aid(appId, event, google_aid);
//    }
//
//
//    @PostMapping(path = "/saveUserEvent")
//    public void save(@RequestBody UserEventDTO eventDTO) {
//        UserEvent userEvent = new UserEvent(eventDTO);
//        userEventService.save(userEvent);
//    }


//    @GetMapping(value = "/saveUserEvent")
//    public ResponseEntity<?> saveUserEvent(@RequestParam(value = "eventKey") String eventKey,
//                                           @RequestParam(value = "appId") Long appId,
//                                           @RequestParam(value = "param1") String param1,
//                                           @RequestParam(value = "param2") String param2,
//                                           @RequestParam(value = "param3") String param3,
//                                           @RequestParam(value = "param4") String param4,
//                                           @RequestParam(value = "param5") String param5,
//                                           @RequestParam(value = "param6") String param6,
//                                           @RequestParam(value = "param7") String param7,
//                                           @RequestParam(value = "param8") String param8,
//                                           @RequestParam(value = "param9") String param9,
//                                           @RequestParam(value = "param10") String param10,
//                                           @RequestParam(value = "os") String os,
//                                           @RequestParam(value = "device") String device,
//                                           @RequestParam(value = "google_aid") String google_aid,
//                                           HttpServletRequest request, Map<String, Object> model) throws Exception {
//        CountryEnum country = null;
//        String ip = null;
//        Event event = eventService.findByEventKey(eventKey);
//        if (event != null) {
//            Long count = userEventService.findByEventAndGoogle_aid(appId, event, google_aid);
//            UserEvent userEvent = new UserEvent();
//            if (count == 0) {
//                App app = appService.findById(appId);
//                ip = ipRequestService.getClientIp(request);
//                String countryCode = request.getHeader("CF-IPCountry");
//                try {
//                    country = CountryEnum.valueOf(countryCode);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                userEvent.setEvent(event);
//                userEvent.setParam1(param1);
//                userEvent.setParam2(param2);
//                userEvent.setParam3(param3);
//                userEvent.setParam4(param4);
//                userEvent.setParam5(param5);
//                userEvent.setParam6(param6);
//                userEvent.setParam7(param7);
//                userEvent.setParam8(param8);
//                userEvent.setParam9(param9);
//                userEvent.setParam10(param10);
//                userEvent.setIp(ip);
//                userEvent.setCountry(country);
//                userEvent.setDeviceName(device);
//                userEvent.setGoogle_aid(google_aid);
//                userEvent.setOs(os);
//                userEvent.setApp(app);
//                userEventService.save(userEvent);
//                return ResponseEntity.ok().body("Saved User Event");
//            } else
//                return ResponseEntity.ok().body("User Event Already Exist");
//        } else
//            return ResponseEntity.ok().body("Event does not exist");
//    }

    /*@GetMapping(value = "/findByUserEventAjax")
    public ResponseEntity<?> findByEventAjax(@RequestParam(value = "appId") Long appId, @RequestParam(value = "dateRange") String dateRange) {
        Map<String, Object> data = new HashMap<String, Object>();
            App app=appService.findById(appId);
        Map<String, String> map = getDates(dateRange);
        String startDate = map.get("startDate");
        String endDate = map.get("endDate");
            List<UserEvent> userEventList=userEventService.findByAppIdAndStartBetweenEndDate(appId, startDate, endDate);
            data.put("userEventList", userEventList);
        return ResponseEntity.ok().body(data);
    }*/

//    public Map<String, String> getDates(String dateRange) {
//        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        SimpleDateFormat formatterForJsDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        String startDate = null;
//        String endDate = null;
//        if (dateRange.equals("")) {
//            endDate = formatter.format(new Date());
//            Calendar cal = Calendar.getInstance();
//            cal.add(Calendar.DATE, -7);
//            startDate = formatter.format(cal.getTime());
//        } else {
//            String[] startDateArr = dateRange.split(" - ");
//            try {
//                startDate = formatter.format(formatterForJsDate.parse(startDateArr[0]));
//                endDate = formatter.format(formatterForJsDate.parse(startDateArr[1]));
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//        Map<String, String> mp = new HashMap<>();
//        mp.put("startDate", startDate);
//        mp.put("endDate", endDate);
//        return mp;
//    }

//    @GetMapping("/getUserEventAjax")
//    public ResponseEntity<?> getOffersAjax(@RequestParam(value = "id", required = false, defaultValue = "0") Long appId, DataTablesInput search) {
//        // TODO Auto-generated method stub
//        Map<String, Object> res = userEventService.listUserEventAjax(search, appId);
//        return ResponseEntity.ok().body(res);
//    }

}
