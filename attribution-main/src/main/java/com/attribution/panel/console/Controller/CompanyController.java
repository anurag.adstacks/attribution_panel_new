package com.attribution.panel.console.Controller;

import com.attribution.panel.bean.Company;
import com.attribution.panel.bean.User;
import com.attribution.panel.console.dto.CompanyDTO;
import com.attribution.panel.console.dto.UserDTO;
import com.attribution.panel.console.service.CompanyService;
import com.attribution.panel.console.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/adminConsole")
@Slf4j
public class CompanyController {


    @Autowired
    CompanyService companyService;

    @Autowired
    UserService userService;

    BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();


    @PostMapping(path = "/registrationCompany")
    public CompanyDTO registrationCompany(@RequestBody CompanyDTO companyDTO) {

        Company company = new Company(companyDTO);
        companyService.save(company);


        return new CompanyDTO(company);
    }

    @PostMapping(path = "/registrationUser")
    public UserDTO registrationUser(@RequestBody UserDTO userDTO) {

        User user = new User(userDTO);
        userService.save(user);


        return new UserDTO(user);
    }



}
