package com.attribution.panel.console.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class InstallHoursDTO {
    private Integer hr;
    private Integer count;
}
