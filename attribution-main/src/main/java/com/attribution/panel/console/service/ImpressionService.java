package com.attribution.panel.console.service;

import com.attribution.panel.bean.Impression;
import com.attribution.panel.elasticsearchRepo.ImpressionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class ImpressionService {
    @Autowired
    ImpressionRepository impressionRepository;

    public void save(Impression impression) {
    impressionRepository.save(impression);
    }

    public Optional<Impression> findById(String s) {
        return impressionRepository.findById(s);
    }

    /*public Long findByAppIDAndStartBetweenEndDate(Long appId, String startDate, String endDate) {
        return impressionRepository.findByAppIDAndStartBetweenEndDate(appId, startDate, endDate);
    }

    public Long findByAppIDAndToday(Long appId, String currentDate) {
        return impressionRepository.findByAppIDAndToday(appId, currentDate);
    }

    public Impression findByAppAndGoogle_Aid(App app, String google_aid, String currentDate) {
        return impressionRepository.findByAppAndGoogle_aid(app.getId(), google_aid, currentDate);
    }*/

}
