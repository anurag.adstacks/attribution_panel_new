package com.attribution.panel.console.service;


import com.attribution.panel.bean.PreDefineEvent;
import com.attribution.panel.repository.PreDefineEventRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class PreDefineEventService {

    @Autowired
    PreDefineEventRepo preDefineEventRepo;

    public Page<PreDefineEvent> findByEventNameContainingIgnoreCaseOrId(String search, Long searchLong, Pageable pageRequest) {

        return preDefineEventRepo.findByEventNameContainingIgnoreCaseOrId(search,searchLong,pageRequest);

    }

    public List<PreDefineEvent> findByIdIn(List<Long> ajaxPreDefineEvent) {
        return preDefineEventRepo.findByIdIn(ajaxPreDefineEvent);
    }
}
