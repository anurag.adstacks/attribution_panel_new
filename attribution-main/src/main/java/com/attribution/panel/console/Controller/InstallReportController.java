package com.attribution.panel.console.Controller;

import com.attribution.panel.bean.*;
import com.attribution.panel.console.dto.AppDTO;
import com.attribution.panel.console.service.*;
import com.attribution.panel.constants.UrlConstants;
import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.model.CityResponse;
import com.maxmind.geoip2.record.Country;
import com.maxmind.geoip2.record.*;
import lombok.extern.slf4j.Slf4j;
import nl.basjes.parse.useragent.UserAgent;
import nl.basjes.parse.useragent.UserAgentAnalyzer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping(value = "/adminConsole")
@Slf4j
public class InstallReportController {
    @Autowired
    AppService appService;
    @Autowired
    IpRequestService ipRequestService;
    @Autowired
    InstallReportService installReportService;
    @Autowired
    InstallGeoWiseService installGeoWiseService;
    @Autowired
    ImpressionService impressionService;
    @Autowired
    PartnerService partnerService;

    @Autowired
    AttributionAllReportService attributionAllReportService;

    static UserAgentAnalyzer uaa;
    static UserAgent userAgent;

//  http://localhost:90980/install/saveInstall?appId=1&partnerId=2&os={os}&device={device}&google_aid={google_aid}


    @GetMapping(value = "/installCount")
    public ResponseEntity<?> installCount(@RequestParam(value = "appId") Long appId, @RequestParam(value = "dateRange") String dateRange) {
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String, String> map = getDates(dateRange);
        String startDate = map.get("startDate");
        String endDate = map.get("endDate");
        Long count = installReportService.findByAppId(appId);
        data.put("count", count);
        return ResponseEntity.ok().body(data);
    }

    @GetMapping("/findByAppIDAndStartBetweenEndDate")
    public Long installCount(@RequestParam(value = "id") Long appId, @RequestParam(value = "stDate") String startDate, @RequestParam(value = "enDate") String endDate, HttpServletRequest request) {
        System.out.println("inside findByAppIDAndStartBetweenEndDate " + appId + " " + startDate + " " + endDate);
//        Map<String, Object> data = new HashMap<String, Object>();
//        Map<String, String> map = getDates(dateRange);
//        String startDate = map.get("startDate");
//        String endDate = map.get("endDate");

//        Long count = installReportService.findByAppId(appId);
        Long count = installReportService.findByAppIDAndStartBetweenEndDate(appId, startDate, endDate);
        System.out.println("findByAppIDAndStartBetweenEndDate " + count);
        if (count != null || count == 0) {
            return count;
        } else return 0L;
    }


    public Map<String, String> getDates(String dateRange) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat formatterForJsDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String startDate = null;
        String endDate = null;
        if (dateRange.equals("")) {
            endDate = formatter.format(new Date());
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, -7);
            startDate = formatter.format(cal.getTime());
        } else {
            String[] startDateArr = dateRange.split(" - ");
            try {
                startDate = formatter.format(formatterForJsDate.parse(startDateArr[0]));
                endDate = formatter.format(formatterForJsDate.parse(startDateArr[1]));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Map<String, String> mp = new HashMap<>();
        mp.put("startDate", startDate);
        mp.put("endDate", endDate);
        return mp;
    }

//    @GetMapping(value = "/installCountWiseGeo")
//    public ResponseEntity<?> installCountWiseGeo(@RequestParam(value = "appId") Long appId, @RequestParam(value = "dateRange") String dateRange) {
//        Map<String, Object> data = new HashMap<String, Object>();
//        Map<String, String> map = getDates(dateRange);
//        String startDate = map.get("startDate");
//        String endDate = map.get("endDate");
//        App app = appService.findById(appId);
//        List<String> installHours = installReportService.findByAppIdAndCountryWiseCount(appId, startDate, endDate);
//        List<InstallGeoWiseDTO> installGeoWiseDTOS = new ArrayList<>();
//        for (int i = 0; i < installHours.size(); i++) {
//            String[] arr = installHours.get(i).split(",");
//            InstallGeoWiseDTO installGeoWiseDTO = new InstallGeoWiseDTO(arr[0], arr[1]);
//            installGeoWiseDTOS.add(installGeoWiseDTO);
//        }
//        List<InstallGeoWise> installGeoWise = installGeoWiseService.findByApp(app);
//        /*for(InstallGeoWise installGeoWise1:installGeoWise){
//            InstallGeoWiseDTO installGeoWiseDTO=new InstallGeoWiseDTO(installGeoWise1);
//            System.out.println("country = "+installGeoWiseDTO);
//            installGeoWiseDTOS.add(installGeoWiseDTO);
//        }*/
//        data.put("installGeoWise", installGeoWiseDTOS);
//        return ResponseEntity.ok().body(data);
//    }
//
//    @GetMapping(value = "/installCountWiseDate")
//    public ResponseEntity<?> installCountWiseDate(@RequestParam(value = "appId") Long appId, @RequestParam(value = "dateRange") String dateRange) throws ParseException {
//        Map<String, Object> data = new HashMap<String, Object>();
//        Map<String, String> map = getDates(dateRange);
//        String startDate = map.get("startDate");
//        String endDate = map.get("endDate");
//        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        Date date = dt.parse(startDate);
//        Date date1 = dt.parse(endDate);
//        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
//        String start1 = formatter.format(date);
//        String endDate1 = formatter.format(date1);
//        App app = appService.findById(appId);
//        List<String> installHours = new ArrayList<>();
//        List<String> hours = new ArrayList<>();
//        List<String> count = new ArrayList<>();
//        if (start1.equals(endDate1)) {
//            installHours = installReportService.findByAppIdAndHourCount(appId, startDate, endDate);
//            String timeStamp = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
//            int currentHour;
//            if (timeStamp.equals(start1))
//                currentHour = LocalDateTime.now().getHour();
//            else
//                currentHour = 24;
//            for (int j = 1; j <= currentHour; j++) {
//                hours.add(String.valueOf(j));
//                count.add("0");
//            }
//            for (int i = 0; i < installHours.size(); i++) {
//                String[] arr = installHours.get(i).split(",");
//                count.set(Integer.parseInt(arr[0]), arr[1]);
//            }
//        } else {
//            installHours = installReportService.findByAppIdDateWiseCount(appId, startDate, endDate);
//            // Parses the date
//            DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("dd/MM/YYYY");
//            LocalDate start = LocalDate.parse(start1);
//            LocalDate end = LocalDate.parse(endDate1);
//            List<String> totalDates = new ArrayList<>();
//            while (!start.isAfter(end)) {
//                hours.add(formatter1.format(start));
//                count.add("0");
//                start = start.plusDays(1);
//            }
//            for (int i = 0; i < installHours.size(); i++) {
//                String[] arr = installHours.get(i).split(",");
//                LocalDate dbDate = LocalDate.parse(arr[0]);
//                int pos = hours.indexOf(formatter1.format(dbDate));
//                count.set(pos, arr[1]);
//            }
//        }
//        data.put("hours", hours);
//        data.put("count", count);
//        return ResponseEntity.ok().body(data);
//    }
//
//    @GetMapping(value = "/updateInstallByRetention")
//    public ResponseEntity<?> updateInstallByRetention(@RequestParam(value = "status") boolean status,
//                                                      @RequestParam(value = "google_aid") String google_aid,
//                                                      @RequestParam(value = "appId") Long appId,
//                                                      HttpServletRequest request, Map<String, Object> model) throws Exception {
//        InstallReport installReport = null;
//        if (!status)
//            installReport = installReportService.findByAppIdAndGoogle_aidAndStatus(appId, google_aid, false);
//        if (installReport == null) {
//            installReport = installReportService.findByAppIdAndGoogle_aid(appId, google_aid);
//            installReport.setStatus(status);
//            String currentDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
//            SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//            Date date = dt.parse(currentDate);
//            installReport.setUpdateDateTime(date);
//            installReportService.save(installReport);
//        }
//        return ResponseEntity.ok().body("Updated User Install");
//    }
//
//    @GetMapping(value = "/retentionCount")
//    public ResponseEntity<?> updateInstallByRetention(@RequestParam(value = "appId") Long appId,
//                                                      HttpServletRequest request, Map<String, Object> model) throws Exception {
//        Map<String, Object> data = new HashMap<String, Object>();
//        Map<String, String> map = getDates("");
//        String startDate = map.get("startDate");
//        String endDate = map.get("endDate");
//        Long activeUserCount = installReportService.findActiveUserByAppIdAndStartBetweenEndDate(appId, startDate, endDate);
//        Long totalInstallCount = installReportService.findByAppId(appId);
//        double percentage = 0;
//        if (totalInstallCount != 0) {
//            percentage = (double) (activeUserCount * 100) / totalInstallCount;
//            percentage = Double.parseDouble(new DecimalFormat("##.##").format(percentage));
//        }
//        data.put("percentage", percentage);
//        return ResponseEntity.ok().body(data);
//    }

    @GetMapping(value = "/saveInstall")
    public ResponseEntity<?> saveData(@RequestParam(value = "appId") Long appId,
                                      @RequestParam(value = "partnerId") Long partnerId,
                                      @RequestParam(value = "os") String os,
                                      @RequestParam(value = "device") String device,
                                      @RequestParam(value = "google_aid") String google_aid,
                                      HttpServletRequest request, Map<String, Object> model) throws Exception {

        String ip = null;
        App app = appService.findById(appId);
        Partner partner = partnerService.findById(partnerId);
//        ip = ipRequestService.getClientIp(request);
//        String countryCode = request.getHeader("CF-IPCountry");

        String countryCode = "IN";
        System.out.println("countryCode " + countryCode);

        String date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").format(new Date());
        System.out.println("date is " + date);

        ip = "122.160.74.236";

        File database = new File(UrlConstants.DATABASE_CITY_PATH);
        File database1 = new File(UrlConstants.DATABASE_COUNTRY_PATH);
        DatabaseReader reader = new DatabaseReader.Builder(database).build();


//        File dbFile = new File(UrlConstants.DATABASE_CITY_PATH);

        // This creates the DatabaseReader object,
        // which should be reused across lookups.

//        DatabaseReader reader = new DatabaseReader.Builder(dbFile).build();

        // A IP Address
        InetAddress ipAddress = InetAddress.getByName(ip);
        System.out.println("InetAddress " + ipAddress);

        // Get City info
        CityResponse response = reader.city(ipAddress);

        // Country Info
        Country country = response.getCountry();
        System.out.println("Country IsoCode: " + country.getIsoCode()); // 'US'
        System.out.println("Country Name: " + country.getName()); // 'United States'
        System.out.println(country.getNames().get("zh-CN")); // '美国'

        Subdivision subdivision = response.getMostSpecificSubdivision();
        System.out.println("Subdivision Name: " + subdivision.getName()); // 'Minnesota'
        System.out.println("Subdivision IsoCode: " + subdivision.getIsoCode()); // 'MN'

        // City Info.
        City city = response.getCity();
        System.out.println("City Name: " + city.getName()); // 'Minneapolis'

        // Postal info
        Postal postal = response.getPostal();
        System.out.println(postal.getCode()); // '55455'

        // Geo Location info.
        Location location = response.getLocation();

        // Latitude
        System.out.println("Latitude: " + location.getLatitude()); // 44.9733

        // Longitude
        System.out.println("Longitude: " + location.getLongitude()); // -93.2323

        String postals = String.valueOf(postal);
        Map<String, Object> map1 = new HashMap<>();
        map1.put("countryIsoCode", country.getIsoCode());
        map1.put("countryName", country.getName());
        map1.put("subdivisionIsoCode", subdivision.getIsoCode());
        map1.put("subdivisionName", subdivision.getName());
        map1.put("city", city.getName());
        map1.put("Latitude", location.getLatitude());
        map1.put("Longitude", location.getLongitude());
        map1.put("postal", postals);
        map1.put("ip", ip);

        String agent = request.getHeader("User-Agent");
        UserAgent userAgent = uaa.parse(agent);

        /*try {
            country = CountryEnum.valueOf(countryCode);
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        System.out.println("countryCode " + country);

        InstallReport installReport = installReportService.findByAppIdAndGaid(appId, google_aid);
        System.out.println("InstallReport " + installReport);

        if (installReport == null) {
            installReport = new InstallReport();
            installReport.setIp(ip);
            installReport.setCountry(String.valueOf(country));
            installReport.setDeviceName(device);
            installReport.setGaid(google_aid);
            installReport.setOs(os);
//            installReport.setApp(app);
            installReport.setAppId(appId);
            installReport.setAppName(app.getName());
            installReport.setPartnerId(partnerId);
            installReport.setPartnerName(partner.getName());
            installReport.setStatus(true);
            installReportService.save(installReport);
            Impression impression = new Impression();
            impression.setId(UUID.randomUUID().toString().replace("-", ""));
            impression.setIp(ip);
            impression.setCountry(String.valueOf(country));
            impression.setDeviceName(device);
            impression.setGoogle_aid(google_aid);
            impression.setOs(os);
            impression.setAppId(appId);
            impression.setAppName(app.getName());
            impression.setPartnerId(partnerId);
            impression.setPartnerName(partner.getName());
            impression.setCreationDateTime(date);
//            impression.setApp(app);
            impressionService.save(impression);
            attributionAllReportService.saveImpresionDailyAllReportRecord(userAgent, map1, impression);
        } else {
            installReport.setStatus(true);
            installReportService.save(installReport);
        }

        List<InstallReport> count1 = installReportService.findByCountryAndAppId(String.valueOf(country), appId);

        System.out.println("size is " + count1);
        int count = count1.size();

        InstallGeoWise installGeoWise = installGeoWiseService.findByCountryCode(String.valueOf(country));
        if (installGeoWise == null) {
            installGeoWise = new InstallGeoWise();
            installGeoWise.setCountryCode(country.getIsoCode());
            installGeoWise.setCount(count);
            installGeoWise.setAppId(appId);
            installReport.setAppName(app.getName());
            installReport.setPartnerId(partnerId);
            installReport.setPartnerName(partner.getName());

        } else
            installGeoWise.setCount(count);
        installGeoWiseService.save(installGeoWise);
        return ResponseEntity.ok().body("Saved User Install");
    }


}
