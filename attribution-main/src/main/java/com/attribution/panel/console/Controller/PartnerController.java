package com.attribution.panel.console.Controller;


import com.attribution.panel.bean.Partner;
import com.attribution.panel.console.dto.PartnerDTO;
import com.attribution.panel.console.service.AppService;
import com.attribution.panel.console.service.EventService;
import com.attribution.panel.console.service.PartnerService;
import com.attribution.panel.console.service.UploadAppImageService;
import com.attribution.panel.console.dto.AjaxSelectDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping(value = "/adminConsole")
@Slf4j
public class PartnerController {

    @Autowired
    AppService appService;

    @Autowired
    UploadAppImageService uploadAppImageService;

    @Autowired
    PartnerService partnerService;

    @Autowired
    EventService eventService;




    @RequestMapping(value = "/findByPartnerCompany1", method = RequestMethod.GET)
    public Map<String, Object> findByPartnerCompany(@RequestParam(value = "search", defaultValue = "", required = false) String search,
                                                    @RequestParam("page") Integer page,
                                                    @RequestParam("size") Integer size) {
        System.out.println("pubListYo1 ");
        Long searchLong = null;
        try {
            searchLong = (search.equals("")) ? null : Long.parseLong(search);
        } catch (Exception e) {
        }
        Pageable pageRequest = PageRequest.of(page, size);
//        Page<Partner> pubList = partnerService.findByIdInAndCompanyContainingIgnoreCaseOrId(pubsIds,search, searchLong, pageRequest);
        Page<Partner> pubList = partnerService.findByCompanyContainingIgnoreCaseOrId(search, searchLong, pageRequest);

        System.out.println("pubListYo " + pubList);
        List<AjaxSelectDTO> selectDTOS = new ArrayList<>();
        for (Partner pub : pubList) {
            selectDTOS.add(new AjaxSelectDTO(pub));
        }
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("results", selectDTOS);
        map.put("count_filtered", pubList.getTotalPages());

        return map;
    }


    @RequestMapping(value = "/findAllPartner", method = RequestMethod.GET)
    public List<PartnerDTO> findByAll() {
        List<Partner> partner = partnerService.findAll();
        List<PartnerDTO> partnerDTO = new ArrayList<>();
        for (Partner partner1 : partner) {
            partnerDTO.add(new PartnerDTO(partner1));
        }
        System.out.println("InsidefindAllPartner " + partnerDTO);
        return partnerDTO;
    }


    @RequestMapping(value = "/findAllByPartnerId/{partnersInEvent}", method = RequestMethod.GET)
    public List<PartnerDTO> findAllByPartnerId(@PathVariable("partnersInEvent") List<Long> partnersInEvent) {
        List<Partner> partner = partnerService.findAllById(partnersInEvent);
        List<PartnerDTO> partnerDTO = new ArrayList<>();
        for (Partner partner1 : partner)
            partnerDTO.add(new PartnerDTO(partner1));
        return partnerDTO;
    }


    private Set<Partner> listToSet(List<Partner> pubList) {


        return null;
    }

}




//    @RequestMapping(value = "/findByPartnerCompany1", method = RequestMethod.GET)
//    public ResponseEntity<?>
//    findByPartnerCompany1(@RequestParam(value = "search", defaultValue = "", required = false) String search,
//                          @RequestParam("page") Integer page,
//                          @RequestParam("size") Integer size) {
//        System.out.println("pubListYo1 ");
//        Long searchLong = null;
//        try {
//            searchLong = (search.equals("")) ? null : Long.parseLong(search);
//        } catch (Exception e) {
//        }
//        Pageable pageRequest = PageRequest.of(page, size);
////        Page<Partner> pubList = partnerService.findByIdInAndCompanyContainingIgnoreCaseOrId(pubsIds,search, searchLong, pageRequest);
//        Page<Partner> pubList = partnerService.findByCompanyContainingIgnoreCaseOrId(search, searchLong, pageRequest);
//
//        System.out.println("pubListYo " + pubList);
//        List<AjaxSelectDTO> selectDTOS = new ArrayList<>();
//        for (Partner pub : pubList) {
//            selectDTOS.add(new AjaxSelectDTO(pub));
//        }
//        Map<String, Object> map = new LinkedHashMap<>();
//        map.put("results", selectDTOS);
//        map.put("count_filtered", pubList.getTotalPages());
//        return ResponseEntity.ok().body(map);
//    }
