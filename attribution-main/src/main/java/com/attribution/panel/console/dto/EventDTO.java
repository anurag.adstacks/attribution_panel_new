package com.attribution.panel.console.dto;

import com.attribution.panel.bean.App;
import com.attribution.panel.bean.Event;
import com.attribution.panel.bean.Partner;
import com.attribution.panel.enums.AccessStatus;
import com.attribution.panel.enums.ConversionPoint;
import com.attribution.panel.enums.PayoutModel;
import com.attribution.panel.enums.RevenueModel;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class EventDTO {

    private Long id;

    private String eventName;

    private String eventValue;

    private double revenue;

    private String eventRevenueCurrency;

    private double eventRevenueUsd;

    private String description;

    private boolean status;

    private String token; //Event token to be integrated in postback url

    private float payout;

    private Long appsId;

    private Long partnersId;

    private App app;

    private boolean privateEvent;

    private RevenueModel revenueModel;

    private PayoutModel payoutModel;

    private AccessStatus accessStatus;

    private boolean multiConv;

    private String eventKey;

    private int eventCount;

    private ConversionPoint convPoint;


    private String creationDateTime;    //date


    private String updateDateTime;

    private List<Partner> partners;




    public EventDTO(Event event) {
        this.id = event.getId();
        this.eventName = event.getEventName();
        this.eventValue = event.getEventValue();
        this.revenue = event.getRevenue();
        this.eventRevenueCurrency = event.getEventRevenueCurrency();
        this.eventRevenueUsd = event.getEventRevenueUsd();
        this.description = event.getDescription();
        this.status = event.isStatus();
        this.token = event.getToken();
        this.payout = event.getPayout();
        this.appsId = event.getAppsId();
        this.partnersId = event.getPartnersId();
        this.app = event.getApp();
        this.privateEvent = event.isPrivateEvent();
        this.revenueModel = event.getRevenueModel();
        this.payoutModel = event.getPayoutModel();
        this.accessStatus = event.getAccessStatus();
        this.multiConv = event.isMultiConv();
        this.eventKey = event.getEventKey();
        this.eventCount = event.getEventCount();
        this.convPoint = event.getConvPoint();
        this.creationDateTime = event.getCreationDateTime();
        this.updateDateTime = event.getUpdateDateTime();
        this.partners = event.getPartners();
    }

    public EventDTO(Event event, int eventCount){
        this.id=event.getId();
        this.eventName=event.getEventName();
        this.eventCount=eventCount;
        this.eventKey=event.getEventKey();
    }

}

