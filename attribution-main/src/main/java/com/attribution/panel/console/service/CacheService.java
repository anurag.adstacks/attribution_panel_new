package com.attribution.panel.console.service;

import com.attribution.panel.bean.App;
import com.attribution.panel.bean.Partner;
import com.attribution.panel.repository.AppRepository;
import com.attribution.panel.repository.PartnerRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class CacheService {
    @Autowired
    AppRepository offerRepository;
    @Autowired
    PartnerRepository publisherRepository;


    public App getOffer(Long offerId) {
        App app = offerRepository.findById(offerId).orElse(null);
        if (app != null) {
            return app;
        }
        System.out.println("getOffer1 "  + app);
        return app;
//        App app1 = offerRepository.findById(offerId).orElse(null);
//        return (app1 == null) ? null : new App(app1);
    }

    public Partner getPublisher(Long pubId) {
        Partner partner = publisherRepository.findById(pubId).orElse(null);
        if (partner != null) {
            return partner;
        }
        System.out.println("getOffer2 "  + partner);
        return partner;
//        Partner publisher = publisherRepository.findById(pubId).orElse(null);
//        return (publisher == null) ? null : new PublisherTrackingDTO(publisher);
    }

//    public CampaignTrackingDTO getCampaign(Long offerId, Long affId) {
//        String id = "offerId:" + offerId + "pubId:" + affId;
//        CampaignTrackingDTO campaignTrackingDTO = campaignTrackingDTORepository.findById(id).orElse(null);
//        if (campaignTrackingDTO != null) {
//            return campaignTrackingDTO;
//        }
//        Campaign campaign = campaignRepository.findById(new CampaignId(offerId, affId)).orElse(null);
//        return (campaign == null) ? null : new CampaignTrackingDTO(campaign);
//    }
//
//
//    public AdvertiserTrackingDTO getAdvertiser(Long advertiserId) {
//        AdvertiserTrackingDTO advertiserTrackingDTO = advertiserTrackingDTORepository.findById(advertiserId).orElse(null);
//        if (advertiserTrackingDTO != null) {
//            return advertiserTrackingDTO;
//        }
//        Advertiser advertiser = advertiserRepository.findById(advertiserId).orElse(null);
//        return (advertiser == null) ? null : new AdvertiserTrackingDTO(advertiser);
//    }

}
