package com.attribution.panel.console.service;

import com.attribution.panel.bean.Partner;
import com.attribution.panel.repository.PartnerRepository;
import com.attribution.panel.repository.UploadAppImageRepository;
import com.amazonaws.services.s3.AmazonS3;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class PartnerService {
    @Autowired
    private PartnerRepository partnerRepository;

    @Value("${do.space.bucket}")
    private String doSpaceBucket;

    @Autowired
    AmazonS3 s3Client;
    @Autowired
    UploadAppImageRepository uploadAppImageRepository;





    public Page<Partner> findByCompanyContainingIgnoreCaseOrId(String search, Long searchLong, Pageable pageRequest) {
        System.out.println("findByCompanyContainingIgnoreCaseOrId " + partnerRepository.findByCompanyContainingIgnoreCaseOrId(search,searchLong,pageRequest));
            return partnerRepository.findByCompanyContainingIgnoreCaseOrId(search,searchLong,pageRequest);
    }

    public List<Partner> findAll() {
        return partnerRepository.findAll();
    }

    public List<Partner> findAllById(List<Long> partnersInEvent) {
        return partnerRepository.findAllById(partnersInEvent);
    }

    public Partner findById(Long pId) {
        return partnerRepository.findById(pId).get();
    }

    public Partner findByEmail(String name) {
        Partner partner = partnerRepository.findByEmail(name);
        System.out.println("findByEmail " + partner);
        return partner;
    }
}

