package com.attribution.panel.console.dto;


import com.attribution.panel.bean.User;
import com.attribution.panel.enums.RoleEnum;
import com.attribution.panel.enums.StateEnum;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@NoArgsConstructor
public class UserDTO {

    private Long id;

    private String fName;

    private String lName;

    private String email;

    private Set<RoleEnum> roles;

    private String imType;

    private String password;

    private StateEnum state;

    private String confirmPassword;


    public UserDTO(User user) {

        this.id =user.getId();
        this.fName =user.getFName();
        this.lName =user.getLName();
        this.email =user.getEmail();
        this.imType =user.getImType();
        this.password =user.getPassword();
        this.state =user.getState();
        this.confirmPassword =user.getConfirmPassword();
    }
}
