package com.attribution.panel.console.dto;


import com.attribution.panel.bean.*;
import com.attribution.panel.enums.AppStatus;
import lombok.Data;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Data
public class AppDTO {

    private Long id;
    private String name;

    /*@Enumerated(EnumType.STRING)
    private OS os;*/

    private String devOS;
    private String description;

    private String appVersion;
    private String sdkVersion;

    private String appType;
    private String bundleId;  //The Apple bundle ID is a unique identifier associated with iOS apps.


    private String gameLink;

    private Date creationDateTime;

    private Date updateDateTime;

    private User user;

    private List<Partner> approvedPartner;

    private Long compId;

    private Set<Company> approvedCompany;

    private byte[] appLogo;

    private String previewUrl;

    private String trackingUrl;

    private String uuid = UUID.randomUUID().toString().replace("-", "");

    private Long allClicks = (long) 0;

    private Long grossClicks = (long) 0;

    private Long conversions = (long) 0;

    private List<Event> events;

    private List<Category> category;

    private AppStatus status; //

    public List<Country> countries;

    public AppDTO(App app) {
        this.id = app.getId();
        this.name = app.getName();
        this.devOS = app.getDevOS();
        this.description = app.getDescription();
        this.appVersion = app.getAppVersion();
        this.sdkVersion = app.getSdkVersion();
        this.appType = app.getAppType();
        this.bundleId = app.getBundleId();
        this.gameLink = app.getGameLink();
        this.creationDateTime = app.getCreationDateTime();
        this.updateDateTime = app.getUpdateDateTime();
        this.user = app.getUser();
        this.approvedPartner = app.getApprovedPartner();
        this.compId = app.getCompId();
        this.approvedCompany = app.getApprovedCompany();
        this.appLogo = app.getAppLogo();
        this.previewUrl = app.getPreviewUrl();
        this.trackingUrl = app.getTrackingUrl();
        this.uuid = app.getUuid();
        this.allClicks = app.getAllClicks();
        this.grossClicks = app.getGrossClicks();
        this.conversions = app.getConversions();
        this.events = app.getEvents();
        this.category = app.getCategory();
        this.status = app.getStatus();
        this.countries = app.getCountries();
    }


}
