package com.attribution.panel.console.service;

import com.attribution.panel.bean.Event;
import com.attribution.panel.bean.UserEvent;
import com.attribution.panel.console.dto.UserEventDTO;
import com.attribution.panel.repository.UserEventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.datatables.mapping.Column;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
@Transactional
public class UserEventService {
    @Autowired
    UserEventRepository userEventRepository;
    public void save(UserEvent userEvent) {
        userEventRepository.save(userEvent);
    }

//    public List<UserEvent> findByApp(App app) {
//        return userEventRepository.findByApp(app);
//    }

    public List<UserEvent> findByAppIdAndEvent(Long app, Event event) {
        return userEventRepository.findByAppIdAndEvent(app, event);
    }

    /*public List<UserEvent> findByAppIdAndStartBetweenEndDate(Long appId, String startDate, String endDate) {
        return userEventRepository.findByAppIdAndStartBetweenEndDate(appId, startDate, endDate);
    }*/

    public Long findByAppAndEventAndStartDateAndEndDateCount(Long app, Long id, String startDate, String endDate) {
        return userEventRepository.findByAppAndEventAndStartDateAndEndDateCount(app, id, startDate, endDate);
    }

    public Map<String, Object> listUserEventAjax(DataTablesInput search, Long appId){
        Column advertName = search.getColumn("ip");
        String dateRange=advertName.getSearch().getValue();
        Column eventName = search.getColumn("eventName");
        String eventName1=eventName.getSearch().getValue();
        Column country = search.getColumn("country");
        String countryCode=country.getSearch().getValue();
        Column google_aid = search.getColumn("google_aid");
        String deviceId=google_aid.getSearch().getValue();
        List<String> eventIds;
        if (eventName1.equals(""))
            eventIds = new ArrayList<String>();
        else
            eventIds = Arrays.asList(eventName1.split("\\,"));
        List<Long> eventIds1 = new ArrayList<Long>();
        for (String id :  eventIds) {
            eventIds1.add(Long.valueOf(id));
        }
        List<String> countryIds;
        if (countryCode.equals(""))
            countryIds = new ArrayList<String>();
        else
            countryIds = Arrays.asList(countryCode.split("\\,"));
        String[] datesString = dateRange.split(" - ");
        ZoneId zoneId = ZoneId.of("Asia/Kolkata"); // Make zone dynamic later according to timezone selected on UI
        ZonedDateTime startDate = null, endDate = null, now = ZonedDateTime.now(zoneId);
        if (dateRange.equals("")) {
            LocalDate today = now.toLocalDate();
            startDate = today.atStartOfDay(zoneId);
            endDate = today.atTime(23, 59, 59).atZone(zoneId);
        } else {
            startDate = LocalDate.parse(datesString[0], DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))
                    .atStartOfDay(zoneId);
            endDate = LocalDate.parse(datesString[1], DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))
                    .atTime(23, 59, 59).atZone(zoneId);
        }
        int first = search.getStart();
        int len = search.getLength();
        /*Order order = search.getOrder().get(0);*/
        Map<String, String> orderMap = new HashMap<String, String>();
        /*orderMap.put("orderColumnName", search.getColumns().get(order.getColumn()).getData());
        orderMap.put("orderType", order.getDir());*/
        Pageable pageable = getPageable(search);
        Map<String, Object> res = getUserEventAjax(search, first, len, pageable, startDate, endDate, appId, eventIds1, countryIds, deviceId);
        res.put("draw", search.getDraw());
        res.put("error", "");
        // log.info("response map: "+ res.toString());
        return res;
    }

    private Map<String, Object> getUserEventAjax(DataTablesInput tablesInput, int first, int len, Pageable pageable, ZonedDateTime startDate, ZonedDateTime endDate, Long appId, List<Long> eventIds, List<String> countryIds, String deviceId)  {
        Map<String, Object> res = new HashMap<String, Object>();
        Pageable pageable1 = PageRequest.of(first, len);
        List<UserEventDTO> userEventList= userEventRepository.findByAppIdAndStartBetweenEndDate(tablesInput , startDate, endDate, first, len, appId, eventIds, deviceId, countryIds);
        List<Long> count=userEventRepository.getUserEventFilterCount(appId, eventIds, deviceId, countryIds, startDate, endDate);
        res.put("recordsFiltered", count);
        res.put("recordsTotal", count);
        res.put("data", userEventList);
        return res;
    }

    public Pageable getPageable(DataTablesInput input) {
        int first = input.getStart();
        int len = input.getLength();
        int page = (int) (Math.ceil(first + 1) / len);
        return PageRequest.of(page, len);
    }

    public Long findByEventAndGoogle_aid(Long appId, Event event, String google_aid) {
        return userEventRepository.findByEventAndGoogle_aid(appId, event.getId(), google_aid);
    }
}
