package com.attribution.panel.console.service;

import com.attribution.panel.bean.User;
import com.attribution.panel.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service("userService")
@Transactional
public class UserService {
    @Qualifier("userRepository")
    @Autowired
    private UserRepository userRepository;

    public User findUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public User save(User user) {
        return userRepository.save(user);
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public List<User> getEmailByUser(String value) {
        List<User> email = userRepository.getEmailByUser(value);
        return email;
    }

    public void saveUser(User user) {
        userRepository.save(user);
    }
}
