package com.attribution.panel.console.dto;

import com.attribution.panel.bean.App;
import com.attribution.panel.bean.Partner;
import com.attribution.panel.bean.PreDefineEvent;
import lombok.Data;

@Data
public class AjaxSelectDTO {
    private Long id;
    private String text;

    public AjaxSelectDTO(App app) {
        this.id = app.getId();
        this.text = "(Id:" + id + ") " + app.getName();
    }

    public AjaxSelectDTO(Partner partner) {
        this.id = partner.getId();
        System.out.println(id);
        this.text = "(Id:" + id + ") " + partner.getCompany();
    }

    public AjaxSelectDTO(PreDefineEvent preDefineEvent) {
        this.id = preDefineEvent.getId();
        System.out.println(id + " " + preDefineEvent.getEventName());
        this.text = "(Id:" + id + ") " + preDefineEvent.getEventName();
    }

/*
    public AjaxSelectDTO(Advertiser adv) {
        this.id = adv.getId();
        this.text = "(Id:" + id + ") " + adv.getCompany();
    }*/

}
