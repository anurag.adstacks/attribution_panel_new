package com.attribution.panel.console.Controller;


import com.attribution.panel.bean.App;
import com.attribution.panel.bean.UploadAppImage;
import com.attribution.panel.console.service.AppService;
import com.attribution.panel.console.service.UploadAppImageService;
import com.attribution.panel.console.dto.AjaxSelectDTO;
import com.attribution.panel.console.dto.AppDTO;
import com.attribution.panel.console.dto.UploadAppImageFileDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

@RestController
@RequestMapping(value = "/adminConsole")
@Slf4j
public class AppController {
    @Autowired
    AppService appService;
    @Autowired
    UploadAppImageService uploadAppImageService;

    @RequestMapping(value = "/saveApps", method = RequestMethod.POST)
    public ResponseEntity<?> saveNewApp(@ModelAttribute("userFormData") AppDTO appData,
                                        @RequestParam("appLogo") MultipartFile file) throws IOException {
        App app = new App(appData);

        try {
            byte[] contents = file.getBytes();
            if (contents.length == 0) {
                App app1 = appService.findById(app.getId());
                app.setAppLogo(app1.getAppLogo());
            } else
                app.setAppLogo(contents);
        } catch (Exception e) {
            e.printStackTrace();
        }
        appService.save(app);
        return ResponseEntity.ok().body("ok");
    }

    @PostMapping(path = "/saveApp")
    public AppDTO save(@RequestBody AppDTO appData) {
        App app = new App(appData);
        appService.save(app);


        return new AppDTO(app);
    }


    @RequestMapping(value = "/findAllApp", method = RequestMethod.GET)
    public List<AppDTO> findByAll() {
        List<App> apps = appService.findAll();
        System.out.println("Appsis " + apps);
        List<AppDTO> appDTO = new ArrayList<>();
        for (App app : apps) {
            appDTO.add(new AppDTO(app));
        }
        System.out.println("InsideFindAllApp " + appDTO);
        return appDTO;
    }

    @RequestMapping(value = "/findByAppNames", method = RequestMethod.GET)
    public Map<String, Object> findByAppNames(@RequestParam(value = "search", defaultValue = "", required = false) String search,
                                              @RequestParam("page") Integer page,
                                              @RequestParam("size") Integer size) {
        System.out.println("findByOfferNames ");
        Long searchLong = null;
        try {
            searchLong = (search.equals("")) ? null : Long.parseLong(search);
        } catch (Exception e) {
        }
        Pageable pageRequest = PageRequest.of(page, size);
        Page<App> offerList = appService.findByNameContainingIgnoreCaseOrId(search, searchLong, pageRequest);
        System.out.println("samikshaOggfrs " + offerList);
        List<AjaxSelectDTO> selectDTOS = new ArrayList<>();
        for (App offer : offerList) {
            selectDTOS.add(new AjaxSelectDTO(offer));
        }
        System.out.println("findByAppNames " + selectDTOS);
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("results", selectDTOS);
        map.put("count_filtered", offerList.getTotalPages());
        return map;
    }


    @RequestMapping(value = "/getAppsListAjax", method = RequestMethod.GET)
    public Map<String, Object> getAppsAjax(@RequestParam(value = "search", defaultValue = "", required = false) String searchVal,
                                           @RequestParam("start") int start,
                                           @RequestParam("end") int end) {
        System.out.println("getAppsListAjax ");

        Map<String, Object> res = appService.listAppsAjax(searchVal, start, end);
        System.out.println("getAppsListAjax2 " + res);

        return res;
    }


    @RequestMapping(value = "/uploadImageDocument", method = RequestMethod.GET)
    public Map<String, Object> uploadImageDocument(@RequestParam(value = "documentImageFile", required = false) MultipartFile documentImageFile,
                                                   @RequestParam(value = "appId", required = false) Long appId) throws IOException {
        System.out.println("uploadImageDocument ");

        Map<String, Object> data = new HashMap<String, Object>();
        App app = appService.findById(appId);
        Map<String, Object> map = appService.saveFile(documentImageFile, app);

        return data;
    }

    @RequestMapping(value = "/findDownloadImageFile", method = RequestMethod.GET)
    public Map<String, Object> findDownloadIOFile(@RequestParam(value = "appId", required = false) Long appId) throws IOException {
        System.out.println("findDownloadIOFile ");

        Map<String, Object> data = new HashMap<String, Object>();
        App app=appService.findById(appId);
        List<UploadAppImage> uploadAppImages=uploadAppImageService.findByApp(app);
        List<UploadAppImageFileDTO> uploadImageFileDTOS=new ArrayList<>();
        for(UploadAppImage uploadAppImage:uploadAppImages){
            UploadAppImageFileDTO uploadIOFileDTO=new UploadAppImageFileDTO(uploadAppImage);
            uploadImageFileDTOS.add(uploadIOFileDTO);
        }

        data.put("uploadAppImage", uploadImageFileDTOS);

        System.out.println("findDownloadIOFile " + data);

        return data;
    }



/*    @RequestMapping(value = {"/adminConsole/appRedirect"},method = RequestMethod.GET)
    public String appRedirect(Principal principal, Map<String, Object> model){
        return "jsp/apps";
    }


    @RequestMapping(value = {"/adminConsole/getConfiguration"},method = RequestMethod.GET)
    public String getConfiguration(Principal principal, Map<String, Object> model){
        return "jsp/Configuration";
    }*/


    @RequestMapping(value = "/imageAppLogoDisplay", method = RequestMethod.GET)
    public void showImage(@RequestParam("id") long id, HttpServletResponse response, HttpServletRequest request)
            throws IOException {
        App user = appService.findById(id);
//        response.setContentType("image/jpeg, image/jpg, image/png, image/gif");
//        response.getOutputStream().write(user.getAppLogo());
        response.getOutputStream().close();
    }

//    @PostMapping("/getAppsListAjax")
//    public ResponseEntity<?> getAppsAjax(@RequestParam(value = "search", required = false, defaultValue = "") String searchVal,
//                                         @RequestParam("start") int start,
//                                         @RequestParam("end") int end, Map<String, Object> model) {
//        Map<String, Object> res = appService.listAppsAjax(searchVal, start, end);
//        return ResponseEntity.ok().body(res);
//    }

    @GetMapping("/findAppById")
    public AppDTO findAppById(@RequestParam(value = "id") Long appId, HttpServletRequest request) {
        App app = appService.findById(appId);
        System.out.println("InsideFindAppById " + app.getName());
        return new AppDTO(app);
    }

//    @RequestMapping(value = "/findByOfferId/{offerId}", method = RequestMethod.GET)
//    public OfferDTO findById ( @PathVariable("offerId") long offerId){
//        Offer offer = offerService.findById(offerId);
//        return new OfferDTO(offer);
//    }

//    @RequestMapping(value = "/uploadImageDocument", method = RequestMethod.POST)
//    public ResponseEntity<?> uploadIODocument(@RequestParam(value = "documentImageFile", required = false) MultipartFile documentImageFile,
//                                              @RequestParam(value = "appId", required = false) Long appId,
//                                              HttpServletRequest request, Map<String, Object> model) throws IOException {
//        System.out.println("Inside Upload Image Document");
//        Map<String, Object> data = new HashMap<String, Object>();
//        App app = appService.findById(appId);
//        Map<String, Object> map = appService.saveFile(documentImageFile, app);
//        return ResponseEntity.ok().body(data);
//
//    }

//    @GetMapping("/findDownloadImageFile")
//    public ResponseEntity<?> findDownloadIOFile(@RequestParam(value = "appId") Long appId, HttpServletRequest request) {
//        Map<String, Object> data = new HashMap<String, Object>();
//        App app = appService.findById(appId);
//        List<UploadAppImage> uploadAppImages = uploadAppImageService.findByApp(app);
//        List<UploadAppImageFileDTO> uploadImageFileDTOS = new ArrayList<>();
//        for (UploadAppImage uploadAppImage : uploadAppImages) {
//            UploadAppImageFileDTO uploadIOFileDTO = new UploadAppImageFileDTO(uploadAppImage);
//            uploadImageFileDTOS.add(uploadIOFileDTO);
//        }
//        data.put("uploadAppImage", uploadImageFileDTOS);
//        return ResponseEntity.ok().body(data);
//    }


}
