package com.attribution.panel.console.service;

import com.attribution.panel.bean.DailyActiveUsers;
import com.attribution.panel.elasticsearchRepo.DailyActiveUsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class DailyActiveUsersService {
    @Autowired
    DailyActiveUsersRepository dailyActiveUsersRepository;

    public DailyActiveUsers findByAppIDAndGaid(Long appId, String google_aid, String currentDate) {
        return dailyActiveUsersRepository.findByAppIDAndGaid(appId, google_aid, currentDate);
    }

    public void save(DailyActiveUsers dailyActiveUsers) {
        dailyActiveUsersRepository.save(dailyActiveUsers);
    }




//    public DailyActiveUsers findByAppAndGoogle_Aid(App app, String google_aid, String currentDate) {
//    return dailyActiveUsersRepository.findByAppAndGoogle_aid(app.getId(), google_aid, currentDate);
//    }
//
//    public void save(DailyActiveUsers dailyActiveUsers) {
//        dailyActiveUsersRepository.save(dailyActiveUsers);
//    }
//
//    public Long findByAppIDAndToday(Long appId, String currentDate) {
//        return dailyActiveUsersRepository.findByAppIDAndToday(appId, currentDate);
//    }

}
