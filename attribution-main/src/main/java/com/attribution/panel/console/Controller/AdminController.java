package com.attribution.panel.console.Controller;

import com.attribution.panel.bean.App;
import com.attribution.panel.bean.Country;
import com.attribution.panel.bean.Event;
import com.attribution.panel.bean.Partner;
import com.attribution.panel.console.service.AppService;
import com.attribution.panel.console.service.EventService;
import com.attribution.panel.console.service.PartnerService;
import com.attribution.panel.enums.ConversionStatus;
import com.attribution.panel.reports.service.ReportService;
import com.attribution.panel.repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/adminConsole")
public class AdminController {
    @Autowired
    EventService eventService;
    @Autowired
    AppService appService;
    @Autowired
    CountryRepository countryRepository;

    @Autowired
    PartnerService partnerService;

    @Autowired
    ReportService reportService;

    @GetMapping("/eventReport")
    public String eventReport(@RequestParam("id") Long id, Map<String, Object> model, HttpServletRequest request) {
        App app = appService.findById(id);
        List<App> appsList = appService.findAll();
        List<Event> eventList = eventService.findByApp(app);
        List<Country> countries = countryRepository.findAll();
        model.put("appsList", appsList);
        model.put("app", app);
        model.put("appId", app.getId());
        model.put("eventList", eventList);
        model.put("countries", countries);
        return "jsp/userEvent";
    }

    @GetMapping("/event")
    public String findEvent(@RequestParam("id") Long id, Map<String, Object> model, HttpServletRequest request) {
        App app = appService.findById(id);
        List<App> appsList = appService.findAll();
//        Partner partners= partnerService.findAll();
        model.put("appsList", appsList);
        model.put("app", app);
//        model.put("partnerId", partners.);
        return "jsp/events";
    }

    @RequestMapping(value = {"/appRedirect"}, method = RequestMethod.GET)
    public String appRedirect(Principal principal, Map<String, Object> model) {
        return "jsp/apps";
    }

    @RequestMapping(value = {"/apps"}, method = RequestMethod.GET)
    public String getApp(Principal principal, Map<String, Object> model) {
        return "jsp/apps";
    }


    @RequestMapping(value = {"/addApp"}, method = RequestMethod.GET)
    public String addApp(Principal principal, Map<String, Object> model) {
        return "jsp/addApp";
    }


    @RequestMapping(value = {"/getConfiguration"}, method = RequestMethod.GET)
    public String getConfiguration(Principal principal, Map<String, Object> model) {

        List<Partner> partners = partnerService.findAll();
        List<App> apps = appService.findAll();
        System.out.println("himank");
//        System.out.println("Partner are : -  " + apps);
//        List<Publisher> publishers;
//        if (request.isUserInRole("ROLE_EMPLOYEE")) {
//            Admin admin = restTemplate.getForEntity(backendConstants.baseUrlDbAccess + "/adminConsole/findAllAdminBYId/{email}", Admin.class, request.getUserPrincipal().getName()).getBody();
//            assert admin != null;
//            publishers = admin.getPublishers();
//        } else
//            publishers = restTemplate.exchange(backendConstants.baseUrlDbAccess + "/adminConsole/findAllPublisher",
//                    HttpMethod.GET, null, new ParameterizedTypeReference<List<Publisher>>() {
//                    }).getBody();
//        model.put("base", base);
        model.put("app", apps);
        model.put("partner", partners);

        return "jsp/Configuration";
    }

    @RequestMapping(value = {"/getpubs"}, method = RequestMethod.GET)
    public String listPubs(Map<String, Object> model, HttpServletRequest request) {
        List<Partner> partners;
        List<Long> partnersIds = new ArrayList<>();
        partners = partnerService.findAll();
//        List<Partner> partners =  partnerService.findAll();
        System.out.println("himank");
        System.out.println("Partner are : -  " + partners);
//        List<Publisher> publishers;
//        if (request.isUserInRole("ROLE_EMPLOYEE")) {
//            Admin admin = restTemplate.getForEntity(backendConstants.baseUrlDbAccess + "/adminConsole/findAllAdminBYId/{email}", Admin.class, request.getUserPrincipal().getName()).getBody();
//            assert admin != null;
//            publishers = admin.getPublishers();
//        } else
//            publishers = restTemplate.exchange(backendConstants.baseUrlDbAccess + "/adminConsole/findAllPublisher",
//                    HttpMethod.GET, null, new ParameterizedTypeReference<List<Publisher>>() {
//                    }).getBody();
//        model.put("base", base);
        model.put("partnersIds", partnersIds);
        model.put("partner", partners);

        return "jsp/partnerList";
    }

    @GetMapping("/dashboard")
    public String dashboard(@RequestParam("id") Long id, Map<String, Object> model, HttpServletRequest request) {
        App app = appService.findById(id);
        List<Event> eventList = eventService.findByApp(app);
        List<App> appsList = appService.findAll();
        model.put("appsList", appsList);
        model.put("app", app);
        model.put("eventList", eventList);
        return "jsp/dashboard";
    }

    @GetMapping("/getClickLogs")
    public String listClickLogs(
            @RequestParam(value = "daterange", required = false, defaultValue = "") String daterange,
            @RequestParam(value = "advertisers", required = false, defaultValue = "") List<String> advertIds,
            Map<String, Object> model) {

/*        ResponseEntity<List<App>> offers = restTemplate.exchange(backendConstants.baseUrlDbAccess + "/adminConsole/findAllApp",
                HttpMethod.GET, null, new ParameterizedTypeReference<List<App>>() {
                });
        ResponseEntity<List<Company>> responseEntity = restTemplate.exchange(backendConstants.baseUrlDbAccess + "/adminConsole/findAllCompany",
                HttpMethod.GET, null, new ParameterizedTypeReference<List<Company>>() {
                });
        List<Company> adverts = responseEntity.getBody();

        model.put("offerList", offers.getBody());
        model.put("adverts", adverts);*/

//        model.put("base", base);
        return "jsp/clickLogs";
    }


//    @GetMapping(value = "/getAllReport")
//    public String getAllReport(@RequestParam("id") Long appId, Map<String, Object> model) {
//        System.out.println("Inside getAll report:" + id);
//        App app = appService.findById(id);
//        System.out.println("Inside getAll report2:" + " " + model);
//        List<AllReportDTO> list = null;
////        list = reportService.reportAllAjax(new ReportFilterDTO(), id);
//
//        System.out.println("Inside getAll report2: " + list.size() + " " + list);
//        System.out.println(",reportFilterDTO.getPublisherName() ");
//
//        list.get(0).getAppId();
//
//
////        log.info("Inside getAll report:");
////        List<AllReportDTO> list = null;
////        RestTemplate restTemplate = new RestTemplate();
////        ResponseEntity<Object> responseEntity = restTemplate.postForEntity(backendConstants.baseUrlDbAccess + allReport, new ReportFilterDTO(), Object.class);
////        list = (List<AllReportDTO>) responseEntity.getBody();
//        List<App> appsList = appService.findAll();
//        model.put("appsList", appsList);
//        model.put("app", app);
//        model.put("list", list);
//        model.put("filters", new ReportFilterDTO());
//        return "jsp/allReport";
//    }


    @GetMapping("/getReport")
    public String getReport(Map<String, Object> model) {
        System.out.println("this is testing ");
        List<App> app = appService.findAll();
        System.out.println("this is testing " + app);
        List<Partner> partner = partnerService.findAll();
        System.out.println("this is testing " + partner);
        List<ConversionStatus> conversionStatus = Arrays.asList(ConversionStatus.values());
        model.put("publishers", partner);
        model.put("offerList", app);
        model.put("conversionStatus", conversionStatus);
//        model.put("base", base);
        return "jsp/report";
    }

    @GetMapping("/getAllReport")
    public String getAllReport(@RequestParam("id") Long appId, Map<String, Object> model) {
        System.out.println("this is testing 2 " + appId);
        App app = appService.findById(appId);
        List<Partner> partner = partnerService.findAll();
        System.out.println("this is testing 3 " + partner);
        List<ConversionStatus> conversionStatus = Arrays.asList(ConversionStatus.values());
        List<App> appsList = appService.findAll();
        model.put("app", app);
        model.put("appsList", appsList);
        model.put("partners", partner);
        return "jsp/allReport";
    }

}
