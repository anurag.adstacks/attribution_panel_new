//package com.adstacks.attribution.dto;
//
//import bean.com.attribution.panel.App;
//import bean.com.attribution.panel.Partner;
//import enums.com.attribution.panel.CountryEnum;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//
//import java.io.Serializable;
//import java.text.DateFormat;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.UUID;
//
//@Data
//@NoArgsConstructor
//public class ClickTrackingDTO implements Serializable {
//
//    private String id;
//
//    private String partnerClickId;
//    private String gaid; //android device id
//    private String idfa; //iOS device id
//    private String ip;    //IP address
//    private CountryEnum country;
//    private long durationInMillis;
//
//    private Long companyId;
//    private String companyName;
//
//    private Long partnerId;
//
//    private String partnerName;
//
//    private int partnerResponse;
//
//    private String agent; //user agent
//
//    //We can use P for Passed and A for Approved
//    private String clickMessage; //message for click rejection or approval
//    private boolean isGross;
//    private String subAff;
//    private String source;
//    private String s1;
//    private String s2;
//    private String s3;
//    private String s4;
//    private String s5;
//
//    private Long appId;
//
//    private float revenue;
//
//    private float payout;
//
//    private DeviceId deviceId;
//
//    //    @Field(type = FieldType.Date, format = DateFormat.date_optional_time)
//    private String creationDateTime;
//
//    //    @Field(type = FieldType.Date, format = DateFormat.date_optional_time)
//    private String updateDateTime;
//
//    public ClickTrackingDTO(
//            String partnerClickId, String gaid, String idfa, String agent, String subAff,
//            String s1, String s2, String s3, String s4, String s5, String source, Partner Partner,
//            App app, Long companyId, boolean isGross,
//            String ip, CountryEnum country
//    ) {
//        this.id = UUID.randomUUID().toString().replace("-", "");
//        this.partnerClickId = partnerClickId;
//        this.gaid = gaid;
//        this.idfa = idfa;
//        this.agent = agent;
//        this.isGross = isGross;
//        this.subAff = subAff;
//        this.source = source;
//        this.s1 = s1;
//        this.s2 = s2;
//        this.s3 = s3;
//        this.s4 = s4;
//        this.s5 = s5;
//        this.partnerId = Partner.getId();
//        this.appId = app.getId();
//        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
//        this.creationDateTime = dateFormat.format(new Date());
//        this.updateDateTime = dateFormat.format(new Date());
//        this.companyId = companyId;
//        this.ip = ip;
//        this.country = country;
//    }
//
//}
