package com.attribution.panel.console.service;

import com.attribution.panel.bean.App;
import com.attribution.panel.bean.UploadAppImage;
import com.attribution.panel.repository.UploadAppImageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UploadAppImageService {
    @Autowired
    UploadAppImageRepository uploadAppImageRepository;
    public List<UploadAppImage> findByApp(App app) {
        return uploadAppImageRepository.findByApp(app);
    }
}
