package com.attribution.panel.console.service;


import com.attribution.panel.bean.*;
import com.attribution.panel.elasticsearchRepo.AttributionAllReportRepo;
import com.attribution.panel.elasticsearchRepo.AttributionRecordRepo;
import com.attribution.panel.elasticsearchRepo.ClickRepo;
import com.attribution.panel.elasticsearchRepo.ConversionRepo;
import com.attribution.panel.reports.dao.ReportQueryBuilder;
import com.attribution.panel.repository.AppRepository;
import com.attribution.panel.repository.EventRepository;
import com.attribution.panel.repository.PartnerRepository;
import lombok.extern.slf4j.Slf4j;
import nl.basjes.parse.useragent.UserAgent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Map;

@Service
@Slf4j
@Transactional
public class AttributionAllReportService {

    @Autowired
    Environment environment;
    @Autowired
    IpRequestService ipRequestService;

    @Autowired
    AppRepository appRepository;

    @Autowired
    PartnerRepository partnerRepository;

    @Autowired
    ConversionRepo conversionRepository;

    @Autowired
    AttributionAllReportRepo attributionAllReportRepo;

    @Autowired
    AttributionRecordService attributionRecordService;

    @Autowired
    AttributionRecordRepo attributionRecordRepo;

    @Autowired
    ClickRepo clickRepository;

    @Autowired
    ClickService clickService;

    @Autowired
    AppService appService;

    @Autowired
    EventService eventService;

    @Autowired
    EventRepository eventRepository;

    @Autowired
    ClickRepo clickRepo;

    @Autowired
    ReportQueryBuilder reportQueryBuilder;


    public void saveConversionDailyAllReportRecord(AttributionConversion conversion, AttributionClick attributionclick, String dayTimestamp, boolean isClickFound, UserAgent userAgent, Map deviceLocation, Impression impression) {

        App app = appService.findById(attributionclick.getAppID());
        Event event = null;
        if (conversion.getId() != null) {
            event = eventService.findById(conversion.getEventID());
            System.out.println("apppsps is1 " + event.getEventName());
        }


        System.out.println("apppsps is " + conversion.getEventID());

        System.out.println("apppsps is2 " + app.getName());
        System.out.println("apppsps is3 " + impression);


        try {
//            Conversion conversion = (Conversion) map.get("conversion");
//            log.info("clickId:" + conversion.getClickId());
//            String clickId = conversion.getClickId();
            System.out.println("saveConversionDailyRecord1 " + attributionclick.getIp());
            System.out.println("saveConversionDailyRecord " + conversion);


//            AttributionClickDTO click = clickRepository.findById(conversion.getClickId()).orElse(null);
//            System.out.println("clickcount0  " + click);


//            App app = appRepository.findById(clickAttribution.getAppID()).orElse(null);

//            List<AttributionConversion> conversionList = clickRepository.findByClickId(clickAttribution.getId());
//            System.out.println("hwlloMrHowDoUdo2 " + conversionList);

            AttributionAllReport attributionAllReport = null;

            if (conversion.getId() == null) {
                System.out.println("horlyhourlu");
//                attributionAllReport = attributionAllReportRepo.findByAppIdAndPartnerIdAndDayTimestamp(attributionclick.getAppID(), attributionclick.getPId(), dayTimestamp);
                attributionAllReport = new AttributionAllReport(conversion, dayTimestamp, attributionclick, app, userAgent, deviceLocation, event);
                saveAttributionAllReportRecord(conversion, attributionAllReport, attributionclick, isClickFound);
                System.out.println("horlyhourlu2" + attributionAllReport);
                System.out.println("horlyhourlu4 " + attributionAllReport);
                /*if (attributionAllReport == null) {
                    attributionAllReport = new AttributionAllReportDTO(conversion, dayTimestamp, attributionclick, app, userAgent, deviceLocation, event);
                    saveAttributionAllReportRecord(conversion, attributionAllReport, attributionclick, isClickFound);
                    System.out.println("horlyhourlu2" + attributionAllReport);
                } else {
                    saveAttributionAllReportRecord(conversion, attributionAllReport, attributionclick, isClickFound);
                    System.out.println("horlyhourlu3" + attributionAllReport);
                }*/
            } else {
                attributionAllReport = new AttributionAllReport(conversion, dayTimestamp, attributionclick, app, userAgent, deviceLocation, event);
                saveAttributionAllReportRecord(conversion, attributionAllReport, attributionclick, isClickFound);
                System.out.println("horlyhourlu5" + attributionAllReport);
//                attributionAllReport = attributionAllReportRepo.findByAppIdAndPartnerIdAndDayTimestamp(conversion.getAppId(), conversion.getPartnerId(), dayTimestamp);
                System.out.println("horlyhourlu8 " + attributionAllReport);
                /*if (attributionAllReport == null) {
                    attributionAllReport = new AttributionAllReportDTO(conversion, dayTimestamp, attributionclick, app, userAgent, deviceLocation, event);
                    saveAttributionAllReportRecord(conversion, attributionAllReport, attributionclick, isClickFound);
                    System.out.println("horlyhourlu5" + attributionAllReport);
                } else {
                    saveAttributionAllReportRecord(conversion, attributionAllReport, attributionclick, isClickFound);
                    System.out.println("horlyhourlu6" + attributionAllReport);
                }
                System.out.println("horlyhourlu7" + attributionAllReport);*/
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void saveImpresionDailyAllReportRecord(UserAgent userAgent, Map deviceLocation, Impression impression) {

        App app = appService.findById(impression.getAppId());
        Event event = null;

//        event = eventService.findById(conversion.getEventID());
//        System.out.println("apppsps is1 " + event.getEventName());


//        System.out.println("apppsps is " + conversion.getEventID());

        System.out.println("apppsps is2 " + app.getName());
        System.out.println("apppsps is3 " + impression);

        System.out.println("horlyhourlu");
        AttributionAllReport attributionAllReport = null;

        attributionAllReport = new AttributionAllReport(app, userAgent, deviceLocation, impression);
//        saveAttributionAllReportRecord(conversion, attributionAllReport, attributionclick, isClickFound);
        System.out.println("horlyhourlu2" + attributionAllReport);

        save(attributionAllReport);


    }


    public void saveAttributionAllReportRecord(AttributionConversion conversion, AttributionAllReport attributionAllReport, AttributionClick attributionclick, boolean isClickFound) {


        String sDate = "Apr 1, 2023";
        String eDate = "Apr 5, 2023";
        Long count = null;
        System.out.println("thisIsSaveConversionDailyRecord0  " + conversion.getAppId() + " " + attributionclick.getPId());
//        Long count = reportQueryBuilder.countByAppIdAndPartnerIdAndCreationDateTimeBetween(attributionclick.getAppID(), attributionclick.getPId(), sDate, eDate );
/*        if (conversion.getId() == null) {
            count = reportQueryBuilder.countByAppIdAndPartnerIdAndCreationDateTimeBetween(attributionclick.getAppID(), attributionclick.getPId());
            System.out.println("thisIsSaveConversionDailyRecord00  " + count);
        } else {
            count = reportQueryBuilder.countByAppIdAndPartnerIdAndCreationDateTimeBetween(attributionclick.getAppID(), conversion.getPartnerId());
            System.out.println("thisIsSaveConversionDailyRecord000  " + count);
        }*/
//
        System.out.println("thisIsSaveConversionDailyRecord0000  " + count);

/*        try {
            if (conversion.getId() == null) {
                System.out.println("thisIsSaveConversionDailyRecord0  " + conversion.getConversionStatus() + attributionAllReport.getApprovedConversions() + attributionAllReport.getClickCount());
                attributionAllReport.setClickCount(attributionAllReport.getClickCount() + 1);
                double ac = attributionAllReport.getApprovedConversions();
                double cc = attributionAllReport.getClickCount();
                double ratio = ac / cc * 100;
                double ctr = cc / count * 100;
                System.out.println("clickcount4  " + ratio);
                attributionAllReport.setRatio(ratio);
                attributionAllReport.setCtr(ctr);
            } else {
                System.out.println("thisIsSaveConversionDailyRecord1  " + conversion.getConversionStatus() + attributionAllReport.getApprovedConversions() + attributionAllReport.getClickCount());
                if(!isClickFound){
                    attributionAllReport.setClickCount(attributionAllReport.getClickCount() + 1);
                }
                double ac = attributionAllReport.getApprovedConversions();
                double cc = attributionAllReport.getClickCount();
                double ratio = ac / cc * 100;
                double ctr = cc / count * 100;
                System.out.println("clickcount4  " + ratio);
                attributionAllReport.setRatio(ratio);
                attributionAllReport.setCtr(ctr);
                System.out.println("clickcount3  " + (attributionAllReport.getApprovedConversions()/attributionAllReport.getClickCount()) * 100);
                switch (conversion.getConversionStatus()) {
                    case APPROVED:
                        attributionAllReport.setApprovedConversions(attributionAllReport.getApprovedConversions() + 1);
//                    attributionAllReport.setApprovedRevenue(attributionAllReport.getApprovedRevenue() + conversion.getEvent().getRevenue());
//                    attributionAllReport.setApprovedPayout(attributionAllReport.getApprovedPayout() + conversion.getEvent().getPayout());
                        System.out.println("hhihihihihihih" + attributionAllReport);
                        break;
                    case PENDING:
                        attributionAllReport.setPendingConversions(attributionAllReport.getPendingConversions() + 1);
//                    attributionAllReport.setPendingRevenue((attributionAllReport.getPendingRevenue() + conversion.getEvent().getRevenue()));
                        System.out.println("hahahaahahaha");
                        break;
                    case CANCELLED:
                        attributionAllReport.setCancelledConversions(attributionAllReport.getCancelledConversions() + 1);
//                    attributionAllReport.setCancelledRevenue(attributionAllReport.getCancelledRevenue() + conversion.getEvent().getRevenue());
                        System.out.println("hahahaahahaha");
                        break;
                }
            }

            System.out.println("clickcount6  " + attributionAllReport);
            attributionAllReportService.save(attributionAllReport);

        } catch (Exception e) {
            e.printStackTrace();
        }*/

        System.out.println("clickcount6  " + attributionAllReport);
        save(attributionAllReport);

    }

    public void save(AttributionAllReport attributionAllReport) {
        attributionAllReportRepo.save(attributionAllReport);
    }

    // banana robusta

}
