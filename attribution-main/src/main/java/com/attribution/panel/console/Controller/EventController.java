package com.attribution.panel.console.Controller;

import com.attribution.panel.bean.App;
import com.attribution.panel.bean.Event;
import com.attribution.panel.bean.PreDefineEvent;
import com.attribution.panel.bean.UserEvent;
import com.attribution.panel.console.dto.AjaxSelectDTO;
import com.attribution.panel.console.dto.EventDTO;
import com.attribution.panel.console.service.*;
import com.attribution.panel.enums.AccessStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping(value = "/adminConsole")
@Slf4j
public class EventController {
    @Autowired
    EventService eventService;
    @Autowired
    AppService appService;
    @Autowired
    UserEventService userEventService;

    @Autowired
    PartnerService partnerService;

    @Autowired
    PreDefineEventService preDefineEventService;




    @PostMapping(path = "/saveEvent")
    public void save(@RequestBody EventDTO event) {
        Event event1 = new Event(event);
        eventService.save(event1);
    }



    @RequestMapping(value = "/savePreNewEvent/{ajaxPreDefineEvent}/{appId}", method = RequestMethod.GET)
    public void SavePreNewEvent(@PathVariable("ajaxPreDefineEvent") List<Long> ajaxPreDefineEvent, @PathVariable("appId") Long appId) {

        App app = appService.findById(appId);
        List<PreDefineEvent> preDefineEvents = preDefineEventService.findByIdIn(ajaxPreDefineEvent);

        System.out.println("hello hi bye bye1 " + preDefineEvents);

        for(int i = 0; i < preDefineEvents.size(); i++) {
            Event event = new Event();
            event.setEventName(preDefineEvents.get(i).getEventName());
            event.setEventValue(preDefineEvents.get(i).getEventValue());
            ;
            event.setRevenue(preDefineEvents.get(i).getRevenue());
            event.setEventRevenueCurrency(preDefineEvents.get(i).getEventRevenueCurrency());
            event.setEventRevenueUsd(preDefineEvents.get(i).getEventRevenueUsd());
            event.setDescription(preDefineEvents.get(i).getDescription());
            event.setStatus(preDefineEvents.get(i).isStatus()); // active - inactive
            event.setToken(preDefineEvents.get(i).getToken());
            event.setRevenueModel(preDefineEvents.get(i).getRevenueModel());

            event.setPayout(preDefineEvents.get(i).getPayout());
            event.setMultiConv(preDefineEvents.get(i).isMultiConv());
            event.setPayoutModel(preDefineEvents.get(i).getPayoutModel());
            event.setAccessStatus(preDefineEvents.get(i).getAccessStatus());
            event.setEventKey(UUID.randomUUID().toString().replace("-", ""));
            event.setApp(app);
            event.setEventCount(3);
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
            event.setCreationDateTime(dateFormat.format(new Date()));
            event.setUpdateDateTime(dateFormat.format(new Date()));

//            if (preDefineEvents.get(i).getAccessStatus().equals("NeedApproval")) {
//                List<Partner> pubList = partnerService.findAllById(partnersInEvent);
//                System.out.println("pubList " + pubList);
//                event.setPartners(pubList);
//            Set<Partner> yoo = listToSet(pubList);
//            }
//
//            if (accessStatus.equals("NeedApproval")) {
//                List<Partner> pubList = partnerService.findAllById(partnersInEvent);
//                System.out.println("pubList " + pubList);
//                event.setPartners(pubList);
////            Set<Partner> yoo = listToSet(pubList);
//            }

            System.out.println("hello hi bye bye2 " + preDefineEvents.get(i).isStatus());
            eventService.save(event);

        }


    }

    @RequestMapping(value = "/findByPreDefineEvent", method = RequestMethod.GET)
    public Map<String, Object> findByPreDefineEvent(@RequestParam(value = "search", defaultValue = "", required = false) String search,
                                               @RequestParam("page") Integer page,
                                               @RequestParam("size") Integer size) {
        System.out.println("pubListYo2 ");
        Long searchLong = null;
        try {
            searchLong = (search.equals("")) ? null : Long.parseLong(search);
        } catch (Exception e) {
        }
        Pageable pageRequest = PageRequest.of(page, size);
//        Page<Partner> pubList = partnerService.findByIdInAndCompanyContainingIgnoreCaseOrId(pubsIds,search, searchLong, pageRequest);
//        Page<Partner> pubList = partnerService.findByCompanyContainingIgnoreCaseOrId(search, searchLong, pageRequest);
        Page<PreDefineEvent> eventList = preDefineEventService.findByEventNameContainingIgnoreCaseOrId(search, searchLong, pageRequest);

        System.out.println("eventListYo " + eventList);
        List<AjaxSelectDTO> selectDTOS = new ArrayList<>();

        for (PreDefineEvent event : eventList) {
            selectDTOS.add(new AjaxSelectDTO(event));
        }

        Map<String, Object> map = new LinkedHashMap<>();
        map.put("results", selectDTOS);
        map.put("count_filtered", eventList.getTotalPages());

        return map;
    }


    @GetMapping(value = "/findByEventAjax")
    public Map<String, Object> findByEventAjax(@RequestParam(value = "appId") Long appId) {
        Map<String, Object> data = new HashMap<String, Object>();
        Calendar cal = Calendar.getInstance();
        App app = appService.findById(appId);
        List<Event> eventList = eventService.findByApp(app);
        List<EventDTO> eventDTOS = new ArrayList<>();
        for (Event event : eventList) {
            List<UserEvent> userEventList = userEventService.findByAppIdAndEvent(app.getId(), event);
            EventDTO eventDTO = new EventDTO(event, userEventList.size());
            eventDTOS.add(eventDTO);
        }
        data.put("eventList", eventDTOS);

        return data;
    }


    @GetMapping(value = "/findByEventAjax1")
    public Map<String, Object> findByEventAjax1(@RequestParam(value = "appId") Long appId,
                                              @RequestParam(value = "dateRange") String dateRange,
                                              @RequestParam(value = "selectEvent") String selectEvent
    ) {
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String, String> map = getDates(dateRange);
        String startDate = map.get("startDate");
        String endDate = map.get("endDate");
        Calendar cal = Calendar.getInstance();
        App app = appService.findById(appId);
        List<Event> eventList = new ArrayList<>();
        List<EventDTO> eventDTOS = new ArrayList<>();
        if (!selectEvent.isEmpty()) {
            String[] arr = selectEvent.split(",");
            List<Long> eventIds = new ArrayList<>();
            for (String id : arr) {
                eventIds.add(Long.valueOf(id));
            }
            eventList = eventService.findByAppAndIdIn(app, eventIds);
        } else {
            eventList = eventService.findByAppId(app.getId());
        }
        for (Event event : eventList) {
            Long count = userEventService.findByAppAndEventAndStartDateAndEndDateCount(app.getId(), event.getId(), startDate, endDate);
            EventDTO eventDTO = new EventDTO(event, Math.toIntExact(count));
            eventDTOS.add(eventDTO);
        }
        data.put("eventList", eventDTOS);
        data.put("event", eventList);

        return data;
    }


    @GetMapping(value = "/eventTracking")
    public ResponseEntity<?> attributionPostback(@RequestParam(value = "eventKey") String eventKey,
                                                 @RequestParam(value = "param1") String param1,
                                                 @RequestParam(value = "param2") String param2,
                                                 @RequestParam(value = "param3") String param3,
                                                 @RequestParam(value = "param4") String param4,
                                                 @RequestParam(value = "param5") String param5,
                                                 @RequestParam(value = "param6") String param6,
                                                 @RequestParam(value = "param7") String param7,
                                                 @RequestParam(value = "param8") String param8,
                                                 @RequestParam(value = "param9") String param9,
                                                 @RequestParam(value = "param10") String param10) {
        Map<String, Object> data = new HashMap<String, Object>();
        Event event = eventService.findByEventKey(eventKey);
        eventService.save(event);
        return ResponseEntity.ok().body("Save Event");
    }


    @RequestMapping(value = "/findByEventId/{eventId}", method = RequestMethod.GET)
    public Event findById(@PathVariable("eventId") Long eventId) {
        return eventService.findById(eventId);
    }


    @RequestMapping(value = "/findByEventKey/{eventKey}", method = RequestMethod.GET)
    public Event findByEventKey(@PathVariable("eventKey") String eventKey) {
        return eventService.findByEventKey(eventKey);
    }



    @RequestMapping(value = "/deleteEventById/{id}", method = RequestMethod.GET)
    public Void getDelete(@PathVariable("id") Long id) {
        eventService.deleteById(id);
        return null;
    }



    public Map<String, String> getDates(String dateRange) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat formatterForJsDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String startDate = null;
        String endDate = null;
        if (dateRange.equals("")) {
            endDate = formatter.format(new Date());
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, -7);
            startDate = formatter.format(cal.getTime());
        } else {
            String[] startDateArr = dateRange.split(" - ");
            try {
                startDate = formatter.format(formatterForJsDate.parse(startDateArr[0]));
                endDate = formatter.format(formatterForJsDate.parse(startDateArr[1]));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Map<String, String> mp = new HashMap<>();
        mp.put("startDate", startDate);
        mp.put("endDate", endDate);
        return mp;
    }




    @RequestMapping(value = "/findByAllEvent/{appId}", method = RequestMethod.GET)
    public List<Event> findByAllEvent(@PathVariable("appId") Long appId) {
        System.out.println("insideFindByAllEvent " + appId);
        List<Event> events=eventService.findByAppId(appId);
        List<Event> eventList=new ArrayList<>();
        for(Event event:events){
            if(event.getAccessStatus().equals(AccessStatus.Public))
                eventList.add(event);

            /*else if(event.getAccessStatus().equals(AccessStatus.NeedApproval) && event.getPublishers().isEmpty())
                eventList.add(event);
            else if(event.getAccessStatus().equals(AccessStatus.NeedApproval)){
                for(Publisher publisher:event.getPublishers()){
                    if(publisher.getId().equals(pubId))
                        eventList.add(event);
                }
            }*/

        }
        return events;
    }


}




//    @RequestMapping(value = "/findByPreDefineEvent", method = RequestMethod.GET)
//    public ResponseEntity<?> findByPreDefineEvent(@RequestParam(value = "search", defaultValue = "", required = false) String search,
//                                                  @RequestParam("page") Integer page,
//                                                  @RequestParam("size") Integer size) {
//        System.out.println("pubListYo2 ");
//        Long searchLong = null;
//        try {
//            searchLong = (search.equals("")) ? null : Long.parseLong(search);
//        } catch (Exception e) {
//        }
//        Pageable pageRequest = PageRequest.of(page, size);
////        Page<Partner> pubList = partnerService.findByIdInAndCompanyContainingIgnoreCaseOrId(pubsIds,search, searchLong, pageRequest);
////        Page<Partner> pubList = partnerService.findByCompanyContainingIgnoreCaseOrId(search, searchLong, pageRequest);
//        Page<PreDefineEvent> eventList = preDefineEventService.findByEventNameContainingIgnoreCaseOrId(search, searchLong, pageRequest);
//
//        System.out.println("eventListYo " + eventList);
//        List<AjaxSelectDTO> selectDTOS = new ArrayList<>();
//
//        for (PreDefineEvent event : eventList) {
//            selectDTOS.add(new AjaxSelectDTO(event));
//        }
//
//        Map<String, Object> map = new LinkedHashMap<>();
//        map.put("results", selectDTOS);
//        map.put("count_filtered", eventList.getTotalPages());
//
//        return ResponseEntity.ok().body(map);
//    }
//    @GetMapping("/getEventById/{eventId}")
//    public ResponseEntity<?> getEventById(@PathVariable("eventId") Long eventId, Map<String, Object> model) {
//        Map<String, Long> advParam = new HashMap<>();
//        Event event = eventService.findById(eventId);
//        model.put("event", event);
//        return ResponseEntity.ok().body(model);
//    }

//    @GetMapping(value = "/deleteEventById/{id}")
//    public ResponseEntity<?> getDelete(@PathVariable("id") Long id,
//                                       Map<String, Object> model) {
//        eventService.deleteById(id);
//        return ResponseEntity.ok().body("ok");
//    }

//@RequestMapping(value = "/saveEvent", method = RequestMethod.POST)
//    public ResponseEntity<?> saveLeave(@RequestParam(value = "appId") Long appId,
//                                       @RequestParam(value = "eventId") Long eventId,
//                                       @RequestParam(value = "eventName") String eventName,
//                                       HttpServletRequest request, Map<String, Object> model) {
//        App app = appService.findById(appId);
//        Event event = new Event();
//        if (eventId != null) {
//            event = eventService.findById(eventId);
//            event.setEventName(eventName);
//        } else {
//            String eventKey = RandomStringUtils.randomAlphanumeric(12);
//            event.setEventName(eventName);
//            event.setEventKey(eventKey);
//            event.setApp(app);
//        }
//        eventService.save(event);
//        return ResponseEntity.ok().body("Saved Event");
//    }
//@PostMapping(value = "/savePreNewEvent")
//public ResponseEntity<?> savePreNewEvent(
//        @RequestParam(value = "ajaxPreDefineEvent", required = false) List<Long> ajaxPreDefineEvent,
//        @RequestParam(value = "appId", required = false) Long appId
//) {
//    System.out.println("hello hi bye bye0 " + ajaxPreDefineEvent);
//
//    App app = appService.findById(appId);
//    List<PreDefineEvent> preDefineEvents = preDefineEventService.findByIdIn(ajaxPreDefineEvent);
//
//    System.out.println("hello hi bye bye1 " + preDefineEvents);
//
//    for(int i = 0; i < preDefineEvents.size(); i++) {
//        Event event = new Event();
//        event.setEventName(preDefineEvents.get(i).getEventName());
//        event.setEventValue(preDefineEvents.get(i).getEventValue());;
//        event.setRevenue(preDefineEvents.get(i).getRevenue());
//        event.setEventRevenueCurrency(preDefineEvents.get(i).getEventRevenueCurrency());
//        event.setEventRevenueUsd(preDefineEvents.get(i).getEventRevenueUsd());
//        event.setDescription(preDefineEvents.get(i).getDescription());
//        event.setStatus(preDefineEvents.get(i).isStatus()); // active - inactive
//        event.setToken(preDefineEvents.get(i).getToken());
//        event.setRevenueModel(preDefineEvents.get(i).getRevenueModel());
//
//        event.setPayout(preDefineEvents.get(i).getPayout());
//        event.setMultiConv(preDefineEvents.get(i).isMultiConv());
//        event.setPayoutModel(preDefineEvents.get(i).getPayoutModel());
//        event.setAccessStatus(preDefineEvents.get(i).getAccessStatus());
//        event.setEventKey(UUID.randomUUID().toString().replace("-", ""));
//        event.setApp(app);
//        event.setEventCount(3);
//        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
//        event.setCreationDateTime(dateFormat.format(new Date()));
//        event.setUpdateDateTime(dateFormat.format(new Date()));
//
////            if (preDefineEvents.get(i).getAccessStatus().equals("NeedApproval")) {
////                List<Partner> pubList = partnerService.findAllById(partnersInEvent);
////                System.out.println("pubList " + pubList);
////                event.setPartners(pubList);
////            Set<Partner> yoo = listToSet(pubList);
////            }
////
////            if (accessStatus.equals("NeedApproval")) {
////                List<Partner> pubList = partnerService.findAllById(partnersInEvent);
////                System.out.println("pubList " + pubList);
////                event.setPartners(pubList);
//////            Set<Partner> yoo = listToSet(pubList);
////            }
//
//        System.out.println("hello hi bye bye2 " + preDefineEvents.get(i).isStatus());
//        eventService.save(event);
//    }
//
//    return ResponseEntity.ok().body("ok");
//}


//    @PostMapping(value = "/saveNewEvent")
//    public ResponseEntity<?> saveNewEvent(
//            @RequestParam(value = "eventName", required = false) String eventName,
//            @RequestParam(value = "ajaxPreDefineEvent", required = false) String ajaxPreDefineEvent,
//            @RequestParam(value = "description", required = false) String description,
//            @RequestParam(value = "status", required = false) int status,
//            @RequestParam(value = "revenueModel", required = false) RevenueModel revenueModel,
//            @RequestParam(value = "revenueModel1", required = false) Float revenueModel1,
//            @RequestParam(value = "multiConv", required = false, defaultValue = "false") Boolean multiConv,
//            @RequestParam(value = "token", required = false) String token,
//            @RequestParam(value = "appId", required = false) Long appId,
//            @RequestParam(value = "accessStatus", required = false) String accessStatus,
//            @RequestParam(value = "partnersInEvent", required = false) List<Long> partnersInEvent) {
//        System.out.println("hello hi bye bye0 " + ajaxPreDefineEvent);
//        System.out.println("hello hi bye bye1 " + AccessStatus.valueOf(accessStatus));
//        System.out.println("hello hi bye bye2 " + description);
//        System.out.println("partnersInEvent " + partnersInEvent + " " + status);
//        App app = appService.findById(appId);
//        Event event = new Event();
//        event.setEventName(eventName);
//        event.setEventValue("101.1");;
//        event.setRevenue(revenueModel1);
//        event.setEventRevenueCurrency("INR");
//        event.setEventRevenueUsd(1.18);
//        event.setDescription(description);
//        event.setStatus(status == 0 ? true : false); // active - inactive
//        event.setToken(token);
//        event.setRevenueModel(revenueModel);
//        event.setPayout((float) 1.04);
//        event.setMultiConv(multiConv);
//        event.setPayoutModel(PayoutModel.CPI);
//        event.setAccessStatus(AccessStatus.valueOf(accessStatus));
//        event.setEventKey(UUID.randomUUID().toString().replace("-", ""));
//        event.setApp(app);
//        event.setEventCount(3);
//        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
//        event.setCreationDateTime(dateFormat.format(new Date()));
//        event.setUpdateDateTime(dateFormat.format(new Date()));
//
//        if (accessStatus.equals("NeedApproval")) {
//            List<Partner> pubList = partnerService.findAllById(partnersInEvent);
//            System.out.println("pubList " + pubList);
//            event.setPartners(pubList);
////            Set<Partner> yoo = listToSet(pubList);
//        }
//        eventService.save(event);
//        return ResponseEntity.ok().body("ok");
//    }
