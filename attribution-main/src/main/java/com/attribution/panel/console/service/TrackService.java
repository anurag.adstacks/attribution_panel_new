package com.attribution.panel.console.service;

import au.com.flyingkite.mobiledetect.UAgentInfo;
import com.attribution.panel.bean.*;
import com.attribution.panel.constants.ElasticSearchConstants;
import com.attribution.panel.constants.UrlConstants;
import com.attribution.panel.elasticsearchRepo.ClickRepo;
import com.attribution.panel.enums.AppStatus;
import com.attribution.panel.enums.CapsStatus;
import com.attribution.panel.enums.CountryEnum;
import com.attribution.panel.repository.AppRepository;
import com.attribution.panel.repository.CompanyRepo;
import com.attribution.panel.repository.PartnerRepository;
import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.model.CityResponse;
import com.maxmind.geoip2.record.Country;
import com.maxmind.geoip2.record.*;
import lombok.extern.slf4j.Slf4j;
import nl.basjes.parse.useragent.UserAgent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.net.InetAddress;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;

@Service
@Slf4j
public class TrackService extends TrackUtilityService {


//    @Autowired
//    KafkaProducer kafkaProducer;

    @Autowired
    ClickService clickService;

    @Autowired
    PartnerRepository partnerRepository;

    @Autowired
    AppRepository appRepository;

    @Autowired
    CompanyRepo companyRepo;

    @Autowired
    ClickRepo clickRepository;

    @Autowired
    Environment environment;

    @Autowired
    ConversionsService conversionsService;

    @Autowired
    IpRequestService ipRequestService;

    @Autowired
    AttributionAllReportService attributionAllReportService;


    public String getEnvironment() {
        String[] activeProfiles = environment.getActiveProfiles();
        return activeProfiles[0];
    }

    public String tracking(Long appId, Long pId, String pClickId, String devOs, String subPid, String source, String s1, HttpServletRequest request) throws Exception {
        LocalDateTime initRequestTime = LocalDateTime.now();
        Company company;
        App app;
//        CampaignTrackingDTO campaignTrackingDTO = null;
        Partner partner;
        LocalDateTime completedRequestTime = null;
        long durationInMillis = 0;
        Map<String, Object> map = null;
        AttributionClick click;
        String id = null;
        Long companyId = null;
        String agent = null;
        String ip = null;
        CountryEnum countrys = null;

        DateFormat dateFormat = new SimpleDateFormat(ElasticSearchConstants.chrField_HourTimestampPattern);
        String dayTimestamp = dateFormat.format(new Date());

        //fetch offer from cache
        try {
            System.out.println("appId = " + appId);
            app = appRepository.findById(appId).get();
            System.out.println("app = " + app.getName());
            if (app == null) {
                throw new Exception("App Not Found!");
            }
        } catch (Exception e) {
            throw new Exception("Offer Not Found!");
        }

        //fetch publisher from cache
        try {
            partner = partnerRepository.findById(pId).orElse(null);
            System.out.println("Partner = " + partner.getName() + " " + partner);
            if (partner == null) {
                throw new Exception("Partner Not Found!");
            }
        } catch (Exception e) {
            throw new Exception("Partner Not Found!");
        }

        //fetch campaign from cache
//id = "offerId:" + offerId + "pubId:" + affId;
//        campaignTrackingDTO = campaignTrackingDTORepository.findById(id).orElse(null);


        //fetch advertiser id from offer and thenA fetch advertiser from cache
        companyId = app.getCompId();
        System.out.println("companyId =aq" + companyId);
        try {
            company = companyRepo.findById(companyId).orElse(null);
            if (company == null) {
                throw new Exception("Company Not Found!");
            }
        } catch (Exception ex) {
            throw new Exception("Company Not Found!");
        }


//             agent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36";
//             agent = "Mozilla/5.0 (Linux; U; Windows 4.4.2; en-us; SCH-I535 Build/KOT49H) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30";
//             agent = "Mozilla/5.0 (X11; Linux x86_64) AppleWe.
//             bKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36";
//             agent = "Mozilla/5.0 (iPhone; CPU iPhone OS 13_7 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148";
        agent = "Mozilla/5.0 (Linux; Android 10; POCO X2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.101 Mobile Safari/537.36";


//        agent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36";

        String url = app.getTrackingUrl();
        System.out.println("url is + " + url);

//        agent = request.getHeader("User-Agent");
        System.out.println("agentif " + agent);

//        ip = ipRequestService.getClientIp(request);
        ip = "122.160.74.236";
        System.out.println("ip is  " + ip);

        //Fetching user agent,ip and country from header only in production environment.
//        if (getEnvironment().equals("test")) {
//            agent = request.getHeader("User-Agent");
//            System.out.println("agentif " + agent);
//        } else if (getEnvironment().equals("prod")) {
//            agent = request.getHeader("User-Agent");
//            System.out.println("agentElse " + agent);
//            ip = ipRequestService.getClientIp(request);
//            System.out.println("ip is  " + ip);
//            try {
//                country = CountryEnum.valueOf(request.getHeader("CF-IPCountry"));
//                System.out.println("try country " + country);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }


        File database = new File(UrlConstants.DATABASE_CITY_PATH);
        File database1 = new File(UrlConstants.DATABASE_COUNTRY_PATH);
        DatabaseReader reader = new DatabaseReader.Builder(database).build();
        DatabaseReader reader1 = new DatabaseReader.Builder(database1).build();

//        File dbFile = new File(UrlConstants.DATABASE_CITY_PATH);

        // This creates the DatabaseReader object,
        // which should be reused across lookups.

//        DatabaseReader reader = new DatabaseReader.Builder(dbFile).build();

        // A IP Address
        InetAddress ipAddress = InetAddress.getByName(ip);
        System.out.println("InetAddress " + ipAddress);

        // Get City info
        CityResponse response = reader.city(ipAddress);

        // Country Info
        Country country = response.getCountry();
        System.out.println("Country IsoCode: " + country.getIsoCode()); // 'US'
        System.out.println("Country Name: " + country.getName()); // 'United States'
        System.out.println(country.getNames().get("zh-CN")); // '美国'

        Subdivision subdivision = response.getMostSpecificSubdivision();
        System.out.println("Subdivision Name: " + subdivision.getName()); // 'Minnesota'
        System.out.println("Subdivision IsoCode: " + subdivision.getIsoCode()); // 'MN'

        // City Info.
        City city = response.getCity();
        System.out.println("City Name: " + city.getName()); // 'Minneapolis'

        // Postal info
        Postal postal = response.getPostal();
        System.out.println(postal.getCode()); // '55455'

        // Geo Location info.
        Location location = response.getLocation();

        // Latitude
        System.out.println("Latitude: " + location.getLatitude()); // 44.9733

        // Longitude
        System.out.println("Longitude: " + location.getLongitude()); // -93.2323

        String postals = String.valueOf(postal);

        Map<String, Object> map1 = new HashMap<>();
        map1.put("countryIsoCode", country.getIsoCode());
        map1.put("countryName", country.getName());
        map1.put("subdivisionIsoCode", subdivision.getIsoCode());
        map1.put("subdivisionName", subdivision.getName());
        map1.put("city", city.getName());
        map1.put("Latitude", location.getLatitude());
        map1.put("Longitude", location.getLongitude());
        map1.put("postal", postals);
        map1.put("ip", ip);


        //Creating a new click entity from above parameters.
//        click = new Click(clickId, gaid, idfa, agent, subAff, s1, s2, s3, s4, s5, source, Partner,
//                app, companyId, true, ip, country);

//        DeviceId deviceId = getDeviceId(agent);


        UserAgent userAgent = uaa.parse(agent);
        System.out.println("Partner = " + partner);


        click = new AttributionClick(pClickId, devOs, userAgent, agent, subPid, s1, source, partner, pId, app
                , true, ip, String.valueOf(country));

//        url = transformTrackingUrl(url, click);

//        System.out.println("abbe url  " + url);

        //Gross Check
        click = isGross(click, app, partner);
        //If gross is true redirect user to offer tracking url, else stop redirection and redirect to base url.
        if (click.isGross()) {
            url = transformTrackingUrl(url, click);
        } else {
            url = "yoo";
        }

        completedRequestTime = LocalDateTime.now();

        //Calculate total duration of time in completing a request.
        durationInMillis = Duration.between(initRequestTime, completedRequestTime).toMillis();

        click.setDurationInMillis(durationInMillis);
        click.setPId(pId);
//        click.setDeviceId(deviceId);

//        clickRepository.save(click);

        AttributionConversion attributionconversion = new AttributionConversion();
        Impression impression = null;

//        kafkaProducer.sendMessage(String.valueOf(click));

        if (click.isGross()) {
            boolean isClickFound = false;
//            conversionsService.saveConversionDailyRecord(attributionconversion, click, dayTimestamp, isClickFound);
            attributionAllReportService.saveConversionDailyAllReportRecord(attributionconversion, click, dayTimestamp, isClickFound, userAgent, map1, impression);
            System.out.println("Tracking Function ends2 " + click);
            clickService.saveClick(click);
        }


        //Sending click entity to rabbbitmq through map.
//        map = ImmutableMap.of("click", click);
//        template.convertAndSend("app1-exchange", "AttributionClickDTO.java-routing-key", map);


        //Setting all variables to null for faster garbage collection
        initRequestTime = null;
        company = null;
        app = null;
//        campaignTrackingDTO = null;
        partner = null;
        completedRequestTime = null;
        durationInMillis = 0;
        map = null;
        click = null;
        id = null;
        companyId = null;
        agent = null;
        ip = null;
        country = null;
        return url;
    }

    //Method to apply various rules on click and decide whether click is approved and user should be redirected to offerurl or not.
    private AttributionClick isGross(AttributionClick click, App app, Partner partner) {
        // CAPS
        CapsStatus capsStatus = CapsStatus.APPROVED;
        boolean isGross = true;
        String passed = "Passed: ";
        String rejected = "Rejected: ";
        click.setClickMessage(passed + capsStatus.name());

//        If company is disabled
//        isGross = checkCompanyActive(click, company, true);

//        If Partner is disabled/inactive
        if (isGross) {
            System.out.println("isGrossyo " + click.getAgent().toLowerCase());
            System.out.println("isGrossyo2 " + app.getAppType());
            isGross = checkPartnerActive(click, partner, true);
        }

        System.out.println("isGross 1 " + isGross);

        // If company has not approved Partner
        if (isGross) {
            isGross = checkApprovedPartner(click, app, partner, true);
        }

        System.out.println("isGross 2" + isGross);


//        Only active apps are passed
        if (isGross) {
            isGross = checkAppActive(click, app, true);
        }

        System.out.println("isGross 3" + isGross);

//      if non-public campaign is null or blocked
//        if (isGross) {
//            isGross = checkNonPublicCampaignIsNullOrBlocked(click, campaign, true, app);
//        }


        // check IP location match with app allowed locations
//        if (isGross) {
//            isGross = checkCountry(click, app, true);
//        }


        // check user agent OS match with app allowed OS
        if (isGross) {
            isGross = checkOs(click, app, true, rejected);
        }

        System.out.println("isGross 4 " + isGross);


        // RepeatedIP
        // check recent IP within set time from other clicks for same campaigns

        //Check caps
//        if (isGross) {
//            isGross = checkCaps(app, Partner, true, click);
//        }


        //subAffRule
//        if (isGross) {
//            isGross = subAffFilter(app, Partner, true, click);
//        }


        click.setGross(isGross);

        return click;
    }

//    public boolean checkCompanyActive(Click click, Company company, boolean isGross) {
//        System.out.println("company.getStatus " + company.getStatus());
//        if (company.getStatus() != null && !company.getStatus()) {
//            System.out.println("yoo");
//            isGross = false;
//            click.setClickMessage("Rejected: Company status Inactive");
//        }
//        return isGross;
//    }


    public boolean checkPartnerActive(AttributionClick click, Partner partner,
                                      boolean isGross) {
        System.out.println("Partner.getStatus " + partner.getStatus());
        if (partner.getStatus() != null && !partner.getStatus()) {
            System.out.println("yoo2");
            isGross = false;
            click.setClickMessage("Rejected: Publisher status Inactive");
        }
        return isGross;
    }

    public boolean checkApprovedPartner(AttributionClick click, App app,
                                        Partner partner, boolean isGross) {

        List<Long> ls = new ArrayList<>();
        Long ax = partner.getId();

        for (int i = 0; i < app.getApprovedPartner().size(); i++) {
            ls.add(app.getApprovedPartner().get(i).getId());
        }

        System.out.println("bhai list are " + partner.getId());

        if (app.getApprovedPartner() == null || !ls.contains(ax)) {

            System.out.println("yoo3");
            isGross = false;
            click.setClickMessage("Rejected: Partner not Approved");

        }

        return isGross;
    }

    public boolean checkAppActive(AttributionClick click, App app, boolean isGross) {
        System.out.println("app.getStatus " + app.getStatus());
        if (app.getStatus() != AppStatus.Active) {
            System.out.println("yoo4");
            isGross = false;
            click.setClickMessage("Rejected: Offer " + app.getStatus());
        }
        return isGross;
    }


//    public boolean checkNonPublicCampaignIsNullOrBlocked(Click click, CampaignTrackingDTO campaign, boolean isGross,
//                                                         OfferTrackingDTO offer) {
//        if (offer.getAccessStatus() != AccessStatus.Public && campaign == null) {
//            isGross = false;
//            click.setClickMessage("Rejected: Campaign not approved");
//        } else if (campaign != null && campaign.getCampaignStatus() != CampaignStatus.APPROVED) {
//            click.setClickMessage("Rejected: Campaign Status " + campaign.getCampaignStatus());
//            isGross = false;
//        }
//        return isGross;
//    }


    public boolean checkCountry(AttributionClick click, App app, boolean isGross) {
        if (app.getCountries() != null &&
                !app.getCountries().contains(CountryEnum.ALL) && !app.getCountries().contains(click.getCountry())) {
            isGross = false;
            click.setClickMessage("Rejected: Geo blocked");
        }
        return isGross;
    }


    public boolean checkOs(AttributionClick click, App app, boolean isGross, String rejected) {
        System.out.println("click.getAgent " + click.getAgent());
        if (click.getAgent() != null) {
            System.out.println("yoo5");
            try {
                String agent = click.getAgent().toLowerCase();
                System.out.println("agent is " + agent);
                // setup the class with the user agent
                UAgentInfo agentInfo = new UAgentInfo(agent, null);
                // check if mobile device
                boolean isMobileDevice = agentInfo.detectMobileQuick();
                System.out.println("isMobileDevice " + isMobileDevice);
                //or check if tablet
                boolean isTabletDevice = agentInfo.detectTierTablet();
                System.out.println("isTabletDevice " + isTabletDevice);

//            String agent = "Mozilla/5.0 (iPhone; CPU iPhone OS 13_7 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148";
//            String agent = "Mozilla/5.0 (Linux; Android 10; POCO X2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.101 Mobile Safari/537.36";


                String appType = (app.getAppType() != null) ? app.getAppType().toLowerCase() : "";
                System.out.println("appType " + appType);

                String devOs = (app.getDevOS() != null) ? app.getDevOS().toLowerCase() : null;
                System.out.println("devOs " + devOs);


                //Macintosh

                String devos = "";

                System.out.println("inside switch ");

                if (appType.equals("android")) {
                    System.out.println("inside android 1");
                    if (!agent.contains(" android ") || !agent.contains("android")) {
                        System.out.println("inside android 2");
                        isGross = false;
                        click.setClickMessage(rejected + "android blocked");
                    }
                } else if (appType.equals("iphone")) {
                    System.out.println("inside iPhone 1");
                    if (!agent.contains(" iphone ") || !agent.contains("iphone")) {
                        System.out.println("inside ios 2");
                        isGross = false;
                        click.setClickMessage(rejected + "iPhone blocked");
                    }
                } else if (appType.equals("windows")) {
                    System.out.println("inside Windows 1 " + agent);
                    if (!agent.contains(" windows ") || !agent.contains("windows")) {
                        System.out.println("inside Windows 2");
                        isGross = false;
                        click.setClickMessage(rejected + "Windows blocked");
                    }
                } else if (appType.equals("mac")) {
                    System.out.println("inside Macintosh 1");
                    if (!agent.contains(" mac ") || !agent.contains("mac")) {
                        System.out.println("inside Macintosh 2");
                        isGross = false;
                        click.setClickMessage(rejected + "Macintosh blocked");
                    }
                } else if (appType.equals("linux")) {
                    System.out.println("inside linux 1");
                    if (!agent.contains(" linux ") || !agent.contains("linux")) {
                        System.out.println("inside linux 2");
                        isGross = false;
                        click.setClickMessage(rejected + "Linux blocked");
                    }
                }

                System.out.println("check hue ");

//            assert appType != null;
                /*switch (appType) {
                    case "mobile,desktop":
                        System.out.println("yoo6" + appType);
                    case "all":
                        System.out.println("yoo4" + appType);
                        if (isMobileDevice || isTabletDevice) {
//                        assert devOs != null;
                            if (devOs.equals("android,ios") || devOs.equals("all")) {
                                if (!agent.contains("iphone") && !agent.contains("android")) {
                                    isGross = false;
                                    click.setClickMessage(rejected + "OS blocked");
                                }
                            } else {
                                if (devOs.equals("ios"))
                                    devOs = "iphone";
                                int index = agent.indexOf(devOs);
                                if (index < 0) {
                                    isGross = false;
                                    click.setClickMessage(rejected + "OS blocked");
                                }
                            }
                        }
                        break;
                    case "desktop":
                        if (isMobileDevice || isTabletDevice) {
                            isGross = false;
                            click.setClickMessage(rejected + "OS blocked");
                        }
                        break;
                    case "mobile":
//                    assert devOs != null;
                        if (devOs.equals("android,ios") || devOs.equals("all")) {
                            if (!agent.contains("iphone") && !agent.contains("android")) {
                                isGross = false;
                                click.setClickMessage(rejected + "OS blocked");
                            }
                        } else {
                            if (devOs.contains("ios"))
                                devOs = "iphone";
                            int index = agent.indexOf(devOs);
                            if (index < 0) {
                                isGross = false;
                                click.setClickMessage(rejected + "OS blocked");
                            }
                        }
                        break;
                    default:
                        break;
                }*/


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return isGross;
    }

    //Fetching list of caps according to appId if any cap is used then match if the cap has current offer and publisher
//    public boolean checkCaps(OfferTrackingDTO offer, PublisherTrackingDTO publisher, boolean isGross, Click click) {
//        List<CapCacheDTO> capCacheDTOList = capCacheDTORepository.findByOfferId(offer.getId());
//        for (CapCacheDTO capCacheDTO : capCacheDTOList) {
//            if (capCacheDTO.isUsed()) {
//                if (capCacheDTO.getPublishers() == null || (capCacheDTO.getPublishers() != null && capCacheDTO.getPublishers()
//                        .contains(publisher.getId()))) {
//                    isGross = false;
//                    click.setClickMessage("Rejected: " + capCacheDTO.getCapsType() + " cap expired");
//                    break;
//                }
//            }
//        }
//        return isGross;
//    }


    //Method to replace parameters according to our panel in redirecting url.
    public String transformTrackingUrl(String url, AttributionClick click) {

//        String deviceID = String.valueOf(click.getDeviceId());
        System.out.println("url is :-" + url);
        System.out.println("click Is  " + click);
//        System.out.println("deviceID Is  " + deviceID);

//        url = url.replaceAll("\\{CLICK_ID\\}", click.getClickId());
//        if (click.getGaid().contains("{") || click.getGaid().contains("}"))
//            click.setGaid("");


        url = url.replaceAll("\\{PID\\}", click.getPId().toString())
                .replaceAll("\\{P_CLICK_ID\\}", click.getPClickId())
                .replaceAll("\\{DEV_OS\\}", click.getDevOs())
                .replaceAll("\\{SUB_PID\\}", click.getSubPid())
                .replaceAll("\\{SOURCE\\}", click.getSource())
                .replaceAll("\\{APP_NAME\\}", click.getAppName())
                .replaceAll("\\{S1\\}", click.getS1());
        while (url.contains("{")) {
//            System.out.println("yoooo" +  url.indexOf("{"));
//            System.out.println("yoooo1" +  url.substring(url.indexOf("}") + 1));
            url = url.substring(0, url.indexOf("{")) + url.substring(url.indexOf("}") + 1);
        }
        System.out.println("bhai mere " + url);
        return url;
    }

    //SubAff filter to block clicks from particular subAffiliates
//    public boolean subAffFilter(OfferTrackingDTO offer, PublisherTrackingDTO Partner, boolean isGross,
//                                Click click) {
//        try {
//            String id = "offerId:" + offer.getId() + "pubId:" + Partner.getId() + "subAff:" + click.getSubAff();
//            BlockSubAffiliate blockSubAffiliate = blockSubAffiliateRepository.findById(id).orElse(null);
//            if (blockSubAffiliate != null) {
//                isGross = false;
//                click.setClickMessage("Rejected: Sub affiliate blocked");
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return isGross;
//    }


//    <div id="extraLargeModal" class="modal fade" tabindex="-1" role="dialog"
//                                 <%--aria-labelledby="exampleModalLongTitle" aria-hidden="true"
//    data-backdrop="static"--%> >
//                                <div class="modal-dialog modal-xl">
//                                    <div class="modal-content">
//                                        <div class="modal-header">
//                                            <h5 class="modal-title">Extra Large Modal</h5>
//                                            <button type="button" class="btn-close"
//    data-bs-dismiss="modal"></button>
//                                        </div>
//                                        <div class="modal-body">
//    <p>Add the fvionjnvdkndxdj to create this extra large modal.</p>
//                                        </div>
//                                        <div class="modal-footer">
//                                            <button type="button" class="btn btn-secondary"
//    data-bs-dismiss="modal">Cancel
//            </button>
//                                            <button type="button" class="btn btn-primary">OK</button>
//                                        </div>
//                                    </div>
//                                </div>
//                            </div>


}
