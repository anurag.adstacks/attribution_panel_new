package com.attribution.panel.console.service;

import com.attribution.panel.bean.App;
import com.attribution.panel.bean.UploadAppImage;
import com.attribution.panel.repository.AppRepository;
import com.attribution.panel.repository.UploadAppImageRepository;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class AppService {
    @Autowired
    private AppRepository appRepository;
    @Value("${do.space.bucket}")
    private String doSpaceBucket;
    @Autowired
    AmazonS3 s3Client;
    @Autowired
    UploadAppImageRepository uploadAppImageRepository;
    public void save(App app) {
        appRepository.save(app);
    }

    public Map<String, Object> listAppsAjax(String searchVal, int first, int last) {
        Map<String, Object> res = new HashMap<>();
        List<App> appList = appRepository.filtersAppsTable(first, last,searchVal);
        List<Long> count = appRepository.countAppsData(searchVal);
        res.put("data", appList);
        res.put("totalItems", count);
        return res;
    }

    public App findById(long id) {
        return appRepository.findById(id).get();
    }

    public List<App> findAll() {
        return appRepository.findAll();
    }

    public Map<String, Object> saveFile(MultipartFile documentImageFile, App app) throws IOException {
        Map<String, Object> map = new HashMap<>();
        String extension = FilenameUtils.getExtension(documentImageFile.getOriginalFilename());
        String nameFile = FilenameUtils.removeExtension(documentImageFile.getOriginalFilename());
        String key ="Attribution/App/Images/"+app.getName()+"/" + nameFile + "." + extension;
        saveImageToServer(documentImageFile, key);
        UploadAppImage uploadAppImage=new UploadAppImage();
        uploadAppImage.setFileName(nameFile);
        uploadAppImage.setFileType(extension);
        uploadAppImage.setUrl("https://app-data-store.sgp1.digitaloceanspaces.com/" + key);
        uploadAppImage.setApp(app);
        uploadAppImageRepository.save(uploadAppImage);
        map.put("status", true);
        map.put("message", "Uploaded Successful!");
        return map;
    }

    public void saveImageToServer(MultipartFile multipartFile, String key) throws IOException {
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentLength(multipartFile.getInputStream().available());

        if (multipartFile.getContentType() != null && !"".equals(multipartFile.getContentType())) {
            metadata.setContentType(multipartFile.getContentType());
        }

        s3Client.putObject(new PutObjectRequest(doSpaceBucket, key, multipartFile.getInputStream(), metadata)
                .withCannedAcl(CannedAccessControlList.PublicRead));

    }

    public Page<App> findByNameContainingIgnoreCaseOrId(String search, Long searchLong, Pageable pageRequest) {
        return appRepository.findByNameContainingIgnoreCaseOrId(search, searchLong, pageRequest);
    }

    public App getApp(Long appID) {
        System.out.println("apppsids " + appID);
        return appRepository.findById(appID).get();
    }
}
