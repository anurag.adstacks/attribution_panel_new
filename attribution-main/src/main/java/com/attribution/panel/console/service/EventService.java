package com.attribution.panel.console.service;

import com.attribution.panel.bean.App;
import com.attribution.panel.bean.Event;
import com.attribution.panel.repository.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class EventService {
    @Autowired
    EventRepository eventRepository;
    public Event findById(Long eventId) {
        return eventRepository.findById(eventId).get();
    }

    public void save(Event event) {
        eventRepository.save(event);
    }

    public List<Event> findAll() {
       return eventRepository.findAll();
    }

    public Event findByEventKey(String eventKey) {
        return eventRepository.findByEventKey(eventKey);
    }

    public List<Event> findByApp(App app) {
        return eventRepository.findByApp(app);
    }

    public void deleteById(Long id) {
        eventRepository.deleteById(id);
    }

    public List<Event> findByAppAndIdIn(App app, List<Long> eventIds) {
        return eventRepository.findByAppAndIdIn(app, eventIds);
    }

    public List<Event> findByAppId(Long id) {
        return eventRepository.findByAppId(id);
    }

}
