package com.attribution.panel.console.service;

import com.attribution.panel.bean.Company;
import com.attribution.panel.repository.CompanyRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service("companyService")
@Transactional
public class CompanyService {

    @Qualifier("companyRepo")
    @Autowired
    private CompanyRepo companyRepo;


    public void save(Company company) {
        companyRepo.save(company);
    }

}
