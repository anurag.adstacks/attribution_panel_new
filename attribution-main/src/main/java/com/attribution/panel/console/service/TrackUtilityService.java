package com.attribution.panel.console.service;

import com.attribution.panel.console.dto.DeviceId;
import com.fasterxml.jackson.databind.ObjectMapper;
import nl.basjes.parse.useragent.UserAgent;
import nl.basjes.parse.useragent.UserAgentAnalyzer;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public
class TrackUtilityService {
    public static UserAgentAnalyzer uaa;
    static UserAgent userAgent;
    static ObjectMapper mapper;

    @PostConstruct
    public void initialize() {
        uaa = UserAgentAnalyzer.newBuilder().hideMatcherLoadStats().withCache(10000).build();
        userAgent = uaa.parse("PostmanRuntime/7.6.0");
        mapper = new ObjectMapper();
    }

    public DeviceId getDeviceId(String agent) {
        UserAgent userAgent = uaa.parse(agent);
        DeviceId deviceId = new DeviceId(userAgent);
        return deviceId;
    }
}
