package com.attribution.panel.console.service;

import com.attribution.panel.bean.Country;
import com.attribution.panel.repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CountryService {
@Autowired
CountryRepository countryRepository;
	
	public List<Country> findAll(){
		return countryRepository.findAll();
	}

}
