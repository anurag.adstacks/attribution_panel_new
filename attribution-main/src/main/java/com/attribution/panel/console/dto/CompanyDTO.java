package com.attribution.panel.console.dto;

import com.attribution.panel.bean.Company;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CompanyDTO {
    private Long id;

    private String fName;

    private String lName;

    private String email;

    private String imType;

    private String password;

    private Boolean status;

    private String confirmPassword;

    public CompanyDTO(Company company) {
        this.id =company.getId();
        this.fName =company.getFName();
        this.lName =company.getLName();
        this.email =company.getEmail();
        this.imType =company.getImType();
        this.password =company.getPassword();
        this.status =company.getStatus();
        this.confirmPassword =company.getConfirmPassword();
    }

}
