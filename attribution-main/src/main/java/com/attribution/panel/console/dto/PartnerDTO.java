package com.attribution.panel.console.dto;

import com.attribution.panel.bean.Partner;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class PartnerDTO {

    private Long id;

    private String name;

    private String email;

    private String phone;

    private String company;

    private String country;

    private String state;

    private Boolean status;

    private String postBack;

    private Date creationDateTime;    //date

    private Date updateDateTime;

    private String uuid ;


    public PartnerDTO(Partner partner) {
        this.id = partner.getId();
        this.name = partner.getName();
        this.email = partner.getEmail();
        this.phone = partner.getPhone();
        this.company = partner.getCompany();
        this.country = partner.getCountry();
        this.state = partner.getState();
        this.status = partner.getStatus();
        this.postBack = partner.getPostBack();
        this.creationDateTime = partner.getCreationDateTime();
        this.updateDateTime = partner.getUpdateDateTime();
        this.uuid = partner.getUuid();
    }

}
