package com.attribution.panel.console.dao;



import com.attribution.panel.bean.App;

import java.util.List;

public interface AppRepositoryCustom {
    List<App> filtersAppsTable(int first, int last, String searchVal);

    List<Long> countAppsData(String searchVal);
}
