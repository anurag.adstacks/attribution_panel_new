package com.attribution.panel.console.dto;

import com.attribution.panel.bean.*;
import com.attribution.panel.enums.OS;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;
import java.util.Set;


@Data
@NoArgsConstructor
public class AppTrackingDTO {

    private Long id;
    private String name;
    private OS os;
    private String description;
    private String link;
    private String gameLink;

    private Date creationDateTime;

    private Date updateDateTime;

    private User user;

    private Set<Partner> approvedAppByPartner;

    private Long companyId;

    private Set<Company>comp;


    private byte[] appLogo;

    private String previewUrl;

    private String trackingUrl;

    private String uuid;

    private Long allClicks;

    private Long grossClicks;

    private Long conversions;

    private List<Event> events;

    private List<Category> category;

}
