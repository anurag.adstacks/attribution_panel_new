package com.attribution.panel.console.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import nl.basjes.parse.useragent.UserAgent;
import nl.basjes.parse.useragent.UserAgentAnalyzer;

import java.io.Serializable;

@Data
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class DeviceId implements Serializable {

//    static UserAgentAnalyzer uaa;
    private String OperatingSystemNameVersion;
    private String DeviceName;
    private String AgentNameVersion;
    private String LayoutEngineNameVersion;

    @JsonCreator
    public DeviceId(@JsonProperty("agent") UserAgent agent) {
        this.OperatingSystemNameVersion = (agent.getValue("OperatingSystemNameVersion")==null)?"":agent.getValue("OperatingSystemNameVersion");
        this.DeviceName = (agent.getValue("DeviceName")==null)?"":agent.getValue("DeviceName");
        this.AgentNameVersion = (agent.getValue("AgentNameVersion")==null)?"":agent.getValue("AgentNameVersion");
        this.LayoutEngineNameVersion = (agent.getValue("LayoutEngineNameVersion")==null)?"":agent.getValue("LayoutEngineNameVersion");
    }

//    public DeviceId getDeviceId(String agent) {
//        UserAgent userAgent = uaa.parse(agent);
//        DeviceId deviceId = new DeviceId(userAgent);
//        return deviceId;
//    }

}
