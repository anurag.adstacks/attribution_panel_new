package com.attribution.panel.console.Controller;

import com.attribution.panel.bean.Country;
import com.attribution.panel.console.dto.CountryDTO;
import com.attribution.panel.console.service.CountryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/adminConsole")
@Slf4j
public class CountryController {
    @Autowired
    CountryService countryService;

    @RequestMapping(value = "/findAllCountry", method = RequestMethod.GET)
    public List<CountryDTO> findByAll() {
        List<Country> country = countryService.findAll();
        List<CountryDTO> countryDTO = new ArrayList<>();
        for (Country country1 : country) {
            countryDTO.add(new CountryDTO(country1));
        }
        return countryDTO;
    }
}
