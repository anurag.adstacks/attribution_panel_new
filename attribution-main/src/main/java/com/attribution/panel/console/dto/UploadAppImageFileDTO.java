package com.attribution.panel.console.dto;

import com.attribution.panel.bean.UploadAppImage;
import lombok.Data;

import java.text.SimpleDateFormat;
@Data
public class UploadAppImageFileDTO {
    private Long id;
    private String fileName;
    private String fileType;
    private String appName;
    private String date;
    private String time;
    private String timeDurations;
    private String url;
    public UploadAppImageFileDTO(UploadAppImage uploadAppImage){
        this.id=uploadAppImage.getId();
        this.fileName=uploadAppImage.getFileName();
        this.fileType=uploadAppImage.getFileType();
        this.appName=uploadAppImage.getApp().getName();
        SimpleDateFormat date = new SimpleDateFormat("dd MMMM yyyy");
        SimpleDateFormat time = new SimpleDateFormat("hh:mm a");
        this.date=date.format(uploadAppImage.getCreationDateTime());
        this.time=time.format(uploadAppImage.getCreationDateTime());
        this.url=uploadAppImage.getUrl();
    }
}
