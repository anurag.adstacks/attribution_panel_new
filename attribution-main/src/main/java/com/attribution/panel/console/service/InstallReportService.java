package com.attribution.panel.console.service;

import com.attribution.panel.bean.InstallReport;
import com.attribution.panel.constants.ElasticSearchConstants;
import com.attribution.panel.elasticsearchRepo.InstallReportRepository;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.core.CountRequest;
import org.elasticsearch.client.core.CountResponse;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class InstallReportService {

    @Autowired
    InstallReportRepository installReportRepository;

    @Autowired
    @Qualifier("elasticClient")
    RestHighLevelClient client;

    public InstallReport findByAppIdAndGaid(Long appId, String google_aid) {
        return installReportRepository.findByAppIdAndGaid(appId, google_aid);
    }

    public void save(InstallReport installReport) {
        installReportRepository.save(installReport);
    }

    public List<InstallReport> findByCountryAndAppId(String country, Long appId) {
        return installReportRepository.findByCountryAndAppId(country, appId);
    }

    public Long findByAppId(Long appId) {

        List<InstallReport> yo =  installReportRepository.findByAppId(appId);
        System.out.println("yoes " + yo);
        return (long) yo.size();

    }


    public Long findByAppIDAndStartBetweenEndDate(Long appId, String startDate, String endDate) {
        String appID = appId.toString();
        BoolQueryBuilder query = QueryBuilders.boolQuery();
        query.must(QueryBuilders.termQuery("appId", appID));
//        query.must(QueryBuilders.termQuery("partnerId", partnerId));
        query.must(QueryBuilders.rangeQuery("creationDateTime").gte(startDate).lte(endDate));

        long count = 0L;
        CountRequest countRequest = new CountRequest(ElasticSearchConstants.installReprotIndexSearchPattern);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(query);
        countRequest.source(searchSourceBuilder);
        System.out.println("countRequest " + countRequest);
        try {
            CountResponse countResponse = client.count(countRequest, RequestOptions.DEFAULT);
            count = countResponse.getCount();
            System.out.println("countRequest1 " + count);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return count;
    }


//    public int findByCountryAndAppId(CountryEnum country, Long appId) {
//        return installReportRepository.findByCountryAndAppId(String.valueOf(country), appId);
//    }


//    public void save(InstallReport installReport) {
//        installReportRepository.save(installReport);
//    }
//
//    public Long findByAppIDAndStartBetweenEndDate(Long appId, String startDate, String endDate) {
//        return installReportRepository.findByAppIDAndStartBetweenEndDate(appId, startDate, endDate);
//    }
//
//
//    public int findByCountryAndAppId(CountryEnum country, Long appId) {
//        return installReportRepository.findByCountryAndAppId(String.valueOf(country), appId);
//    }
//
//    public List<String> findByAppIdAndHourCount(Long appId, String startDate, String endDate) {
//        return installReportRepository.findByAppIdAndHourCount(appId, startDate, endDate);
//    }
//
//    public List<String> findByAppIdDateWiseCount(Long appId, String startDate, String endDate) {
//        return installReportRepository.findByAppIdAndDateWiseCount(appId, startDate, endDate);
//    }
//
//    public InstallReport findByAppIdAndGoogle_aid(Long appId, String google_aid) {
//        return installReportRepository.findByAppIdAndGoogle_aid(appId, google_aid);
//    }
//
//    public InstallReport findByAppIdAndGoogle_aidAndStatus(Long appId, String google_aid, boolean status) {
//        return installReportRepository.findByAppIdAndGoogle_aidAndStatus(appId, google_aid, status);
//    }
//
//    public Long findActiveUserByAppIdAndStartBetweenEndDate(Long appId, String startDate, String endDate) {
//    return installReportRepository.findActiveUserByAppIdAndStartBetweenEndDate(appId, startDate, endDate);
//    }
//
//    public Long findByAppId(Long appId) {
//        return installReportRepository.findByAppId(appId);
//    }
//
//    public List<String> findByAppIdAndCountryWiseCount(Long appId, String s, String s1) {
//        return installReportRepository.findByAppIdAndCountryWiseCount(appId, s, s1);
//    }
//
//    public InstallReport findByAppIdAndGoogle_aidAndDate(App app, String google_aid, String currentDate) {
//        return installReportRepository.findByAppIdAndGoogle_aidAndDate(app.getId(), google_aid, currentDate);
//    }

}
