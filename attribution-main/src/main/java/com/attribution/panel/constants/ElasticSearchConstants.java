package com.attribution.panel.constants;

public class ElasticSearchConstants {

    public static final String clickField_CreationDateTime_Pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    public static final String clickField_UpdationDateTime_Pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";

    public static final String conversionField_CreationDateTime_Pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    public static final String conversionField_UpdationDateTime_Pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    public static final String conversionIndexSearchPattern = "attributionconversion*";

    public static final String attributionIndexSearchPattern = "attributionrecord*";
    public static final String attributionAllReportIndexSearchPattern = "attributionallreport*";

    public static final String chrField_HourTimestampPattern = "yyyy-MM-dd";


    public static final String clickIndexNamePrefix = "click";
    public static final String clickIndexNameDatePattern = "yyyy.MM.dd_HH.mm.ss";

    public static final String clickIndexSearchPattern = "attributionclick*";
    public static final String impressionIndexSearchPattern = "impression*";
    public static final String installReprotIndexSearchPattern = "installreport*";

    public static final String snapshotBackupRepository = "es_prod_backup";
    public static final String snapshotNamePrefix = "snapshot-";
    public static final String snapshotDatePattern = "yyyy.MM.dd_HH:mm:ss";


}
