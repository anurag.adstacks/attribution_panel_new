//package com.adstacks.attribution.clickRepository;
//
//import com.etreetech.panel.cache.dto.EventCacheDTO;
//import org.springframework.data.repository.CrudRepository;
//import org.springframework.stereotype.Repository;
//
//import java.util.List;
//
//@Repository
//public interface EventCacheDTORepository extends CrudRepository<EventCacheDTO, Long> {
//    List<EventCacheDTO> findByOfferId(Long offerId);
//}
