//package com.adstacks.attribution.clickRepository;
//
//import com.etreetech.panel.cache.dto.OfferTrackingDTO;
//import org.springframework.data.repository.CrudRepository;
//import org.springframework.stereotype.Repository;
//
//import java.util.List;
//
//@Repository
//public interface OfferTrackingDTORepository extends CrudRepository<OfferTrackingDTO, Long> {
//
//    List<OfferTrackingDTO> findAllByAdvertiserId(List<Long> advertiserIds);
//
//    List<OfferTrackingDTO> findByAdvertiserId(Long advertiserIds);
//}
