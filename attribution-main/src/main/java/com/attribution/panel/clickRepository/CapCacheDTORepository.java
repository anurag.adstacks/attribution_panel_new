//package com.adstacks.attribution.clickRepository;
//
//import com.etreetech.panel.cache.dto.CapCacheDTO;
//import org.springframework.data.repository.CrudRepository;
//import org.springframework.stereotype.Repository;
//
//import java.util.List;
//
//@Repository
//public interface CapCacheDTORepository extends CrudRepository<CapCacheDTO, Long> {
//    List<CapCacheDTO> findByOfferId(Long offerId);
//}
