//package com.adstacks.attribution.clickRepository;
//
//import com.etreetech.panel.cache.dto.CampaignTrackingDTO;
//import org.springframework.data.repository.CrudRepository;
//import org.springframework.stereotype.Repository;
//
//@Repository
//public interface CampaignTrackingDTORepository extends CrudRepository<CampaignTrackingDTO, String> {
//    CampaignTrackingDTO findFirstByOfferIdAndPubId(Long offerId, Long pubId);
//
//    CampaignTrackingDTO findByOfferId(Long offerId);
//}
