package com.attribution.panel.bean;


import com.attribution.panel.console.dto.UserDTO;
import com.attribution.panel.enums.RoleEnum;
import com.attribution.panel.enums.StateEnum;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String fName;

    private String lName;

    @Column(unique = true)
    private String email;

    @ElementCollection(targetClass = RoleEnum.class)
    @CollectionTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"))
    @Enumerated(EnumType.STRING)
    @Column(name = "role_id")
    private Set<RoleEnum> roles;

    private String imType;

    private String password;

    @Enumerated(EnumType.STRING)
    private StateEnum state;

    private String confirmPassword;


    public User(UserDTO user) {
        this.id =user.getId();
        this.fName =user.getFName();
        this.lName =user.getLName();
        this.email =user.getEmail();
        this.imType =user.getImType();
        this.password =user.getPassword();
        this.state =user.getState();
        this.confirmPassword =user.getConfirmPassword();
    }

}

