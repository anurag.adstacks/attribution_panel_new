package com.attribution.panel.bean;

import com.attribution.panel.console.dto.UserEventDTO;
import lombok.Data;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
public class UserEvent{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String param1;
    private String param2;
    private String param3;
    private String param4;
    private String param5;
    private String param6;
    private String param7;
    private String param8;
    private String param9;
    private String param10;
    private String ip;
    private String os;
    private String google_aid;

    private String deviceName;
    private String country;

    @OneToOne(fetch = FetchType.EAGER)
    @ToString.Exclude
    private Event event;

    private Long appId;

    @CreationTimestamp
    private Date creationDateTime;

    public UserEvent(UserEventDTO userEventDTO) {
        this.id = userEventDTO.getId();
        this.param1 = userEventDTO.getParam1();
        this.param2 = userEventDTO.getParam2();
        this.param3 = userEventDTO.getParam3();
        this.param4 = userEventDTO.getParam4();
        this.param5 = userEventDTO.getParam5();
        this.param6 = userEventDTO.getParam6();
        this.param7 = userEventDTO.getParam7();
        this.param8 = userEventDTO.getParam8();
        this.param9 = userEventDTO.getParam9();
        this.param10 = userEventDTO.getParam10();
        this.ip = userEventDTO.getIp();
        this.os = userEventDTO.getOs();
        this.google_aid = userEventDTO.getGoogle_aid();
        this.deviceName = userEventDTO.getDeviceName();
        this.country = userEventDTO.getCountry();
        this.event = userEventDTO.getEvent();
        this.appId = userEventDTO.getAppId();
        this.creationDateTime = userEventDTO.getCreationDateTime();
    }





}
