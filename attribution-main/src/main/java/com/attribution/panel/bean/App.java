package com.attribution.panel.bean;


import com.attribution.panel.console.dto.AppDTO;
import com.attribution.panel.enums.AppStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.UpdateTimestamp;
import org.javers.core.metamodel.annotation.DiffIgnore;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;


@Entity
@Data
@NoArgsConstructor
public class App {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

    /*@Enumerated(EnumType.STRING)
    private OS os;*/

    private String devOS;
    private String description;

    private String appVersion;
    private String sdkVersion;

    private String appType;
    private String bundleId;  //The Apple bundle ID is a unique identifier associated with iOS apps.


    private String gameLink;

    @CreationTimestamp
    private Date creationDateTime;

    @UpdateTimestamp
    private Date updateDateTime;

    @ManyToOne(fetch = FetchType.EAGER)
    private User user;

    @ManyToMany(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
    private List<Partner> approvedPartner;

    private Long compId;

    @ManyToMany(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
    private Set<Company> approvedCompany;


    @Lob
    private byte[] appLogo;

    private String previewUrl;

    private String trackingUrl;

    @Column(unique = true)
    @DiffIgnore
    private String uuid = UUID.randomUUID().toString().replace("-", "");

    @Column(columnDefinition = "bigint(20) default 0")
    private Long allClicks = (long) 0;

    @Column(columnDefinition = "bigint(20) default 0")
    private Long grossClicks = (long) 0;

    @Column(columnDefinition = "bigint(20) default 0")
    private Long conversions = (long) 0;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "app", orphanRemoval = true)
    @Fetch(FetchMode.SELECT)
    @JsonIgnoreProperties("app")
    @DiffIgnore
    private List<Event> events;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "app")
    @Fetch(FetchMode.SELECT)
    @JsonIgnoreProperties("app")
    @DiffIgnore
    private List<Category> category;

    @Enumerated(EnumType.STRING)
    private AppStatus status; //



    @ManyToMany(fetch = FetchType.LAZY)
    @Enumerated(EnumType.STRING)
    public List<Country> countries;


    public App(Long appId, String appName) {
        this.id = appId;
        this.name = appName;
    }

    public App(AppDTO app) {
        this.id = app.getId();
        this.name = app.getName();
        this.devOS = app.getDevOS();
        this.description = app.getDescription();
        this.appVersion = app.getAppVersion();
        this.sdkVersion = app.getSdkVersion();
        this.appType = app.getAppType();
        this.bundleId = app.getBundleId();
        this.gameLink = app.getGameLink();
        this.creationDateTime = app.getCreationDateTime();
        this.updateDateTime = app.getUpdateDateTime();
        this.user = app.getUser();
        this.approvedPartner = app.getApprovedPartner();
        this.compId = app.getCompId();
        this.approvedCompany = app.getApprovedCompany();
        this.appLogo = app.getAppLogo();
        this.previewUrl = app.getPreviewUrl();
        this.trackingUrl = app.getTrackingUrl();
        this.uuid = app.getUuid();
        this.allClicks = app.getAllClicks();
        this.grossClicks = app.getGrossClicks();
        this.conversions = app.getConversions();
        this.events = app.getEvents();
        this.category = app.getCategory();
        this.status = app.getStatus();
        this.countries = app.getCountries();
    }


    /*@JoinTable(name = "app_approved_partner", joinColumns = {
            @JoinColumn(name = "app_id")}, inverseJoinColumns = {@JoinColumn(name = "approved_partner_id")})
    @Fetch(FetchMode.SELECT)
    @ToString.Exclude
    @DiffIgnore*/


}
