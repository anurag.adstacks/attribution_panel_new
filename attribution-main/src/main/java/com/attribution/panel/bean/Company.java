package com.attribution.panel.bean;


import com.attribution.panel.console.dto.CompanyDTO;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
public class Company {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String fName;

    private String lName;

    @Column(unique = true)
    private String email;

//    @ElementCollection(targetClass = RoleEnum.class)
//    @CollectionTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"))
//    @Enumerated(EnumType.STRING)
//    @Column(name = "role_id")
//    private Set<RoleEnum> roles;

    private String imType;

    private String password;

    private Boolean status;

//    @Enumerated(EnumType.STRING)
//    private StateEnum state;

    private String confirmPassword;

    public Company(CompanyDTO companyDTO) {
        this.id =companyDTO.getId();
        this.fName =companyDTO.getFName();
        this.lName =companyDTO.getLName();
        this.email =companyDTO.getEmail();
        this.imType =companyDTO.getImType();
        this.password =companyDTO.getPassword();
        this.status =companyDTO.getStatus();
        this.confirmPassword =companyDTO.getConfirmPassword();
    }

}
