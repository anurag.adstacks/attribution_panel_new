package com.attribution.panel.bean;


import lombok.Data;
import lombok.NoArgsConstructor;
import nl.basjes.parse.useragent.UserAgent;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Data
@NoArgsConstructor
@Document(indexName = "attributionallreport", shards = 1, replicas = 1)
@Component
public class AttributionAllReport implements Serializable {

    private String id;


    //app
    private String appVersion;
    private String sdkVersion;
    private Long appId;
    private String appName;
    private String appType;
    private String bundleId;


    // event
    private String eventTime;
    private String eventName;
    private String eventValue;
    private double eventRevenue;
    private String eventRevenueCurrency;
    private String eventRevenueUSD;


    //device
    private String att; // App Tracking Transparency,
    private String wifi;
    private String operator;
    private String carrier;
    private String language;
    private String androidId;
    private String imei;
    private String idfa;
    private String idfv;
    private String advertisingId;
    private String deviceModel;
    private String deviceType;
    private String deviceCategory;
    private String platform;
    private String osVersion;
    private String userAgent;
    private String deviceDownloadTime;
    private String oaid;
    private boolean isLAT;


    //device location
    private String region;
    private String countryCode;
    private String state;
    private String city;
    private String postalCode;
    private String ip;




    //Attribution
    private String attributedTouchType;
    private String dayTimestamp;
    //    private String attributedTouchTime;
//    private Date installTime;
//    private String costModel;
//    private String costValue;
//    private String subParam1;
//    private String subParam2;
//    private String subParam3;
//    private String subParam4;
//    private String customerUserId;
//    private boolean isRetargeting;
//    private String storeProductPage;
//    private String costCurrency;
//    private String eventSource;
    private Long partnerId;
    private String partnerName;
//    private String mediaSource;
//    private String channel;
//    private String keywords;
//    private String retargetingConversionType;
//    private boolean isPrimaryAttribution;
//    private String attributionLookback;
//    private String reengagementWindow;
//    private String matchType;
//    private String httpReferrer;
//    private String campaign;
//    private String campaignId;
//    private String adset;
//    private String adsetId;
//    private String ad;
//    private String originalUrl;
//    private String adId;
//    private String adType;
//    private String siteId;



    // fraud
//    private String rejectedReason;
//    private String rejectedReasonValue;
//    private String blockedReason;
//    private String blockedReasonRule;
//    private String blockedReasonValue;
//    private String blockedSubReason;

    public AttributionAllReport(AttributionConversion conversion, String dayTimestamp, AttributionClick attributionclick, App app, UserAgent userAgent, Map deviceLocation, Event event) {

        this.id = UUID.randomUUID().toString().replace("-", "");
        this.appVersion = app.getAppVersion();
        this.sdkVersion = app.getSdkVersion();
        this.appId = app.getId();
        this.appName = app.getName();
        this.appType = app.getAppType();
        this.bundleId = app.getBundleId();

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

        if (conversion.getId() != null) {
            this.eventTime = dateFormat.format(new Date());
            this.eventName = event.getEventName();
            this.eventValue = event.getEventValue();
            this.eventRevenue = event.getRevenue();
            this.eventRevenueCurrency = event.getEventRevenueCurrency();
            this.eventRevenueUSD = String.valueOf(event.getEventRevenueUsd());
        }

        // device
        this.att = "att";
        this.wifi = "wifi";
        this.operator = "operator";
        this.carrier = "carrier";
        this.language = "language";
        this.androidId = "androidId";
        this.imei = "imei";
        this.idfa = "idfa";
        this.idfv = "idfv";
        this.advertisingId = "advertisingId";
        this.deviceModel = (userAgent.getValue("DeviceName") == null) ? "" : userAgent.getValue("DeviceName");
        this.deviceType = (userAgent.getValue("DeviceClass") == null) ? "" : userAgent.getValue("DeviceClass");
        this.deviceCategory = (userAgent.getValue("OperatingSystemClass") == null) ? "" : userAgent.getValue("OperatingSystemClass");
        this.platform = (userAgent.getValue("DeviceClass") == null) ? "" : userAgent.getValue("DeviceClass");
        this.osVersion = "osVersion";
        this.userAgent = String.valueOf(userAgent);
        this.deviceDownloadTime = dateFormat.format(new Date());
        this.dayTimestamp = dateFormat.format(new Date());
        this.oaid = "oaid";
        this.isLAT = true;

        // device location
        this.region = "region";
        this.countryCode = (String) deviceLocation.get("countryIsoCode");
        this.state = (String) deviceLocation.get("subdivisionName");
        this.city = (String) deviceLocation.get("city");
        this.postalCode = (String) deviceLocation.get("postal");
        this.ip = (String) deviceLocation.get("ip");

        //
        if (conversion.getId() == null) {
            this.attributedTouchType = "Click";
        } else {
            this.attributedTouchType = "Conversion";
        }
        this.partnerId = attributionclick.getPId();
        this.partnerName = attributionclick.getPName();

/*// Attribution
        this.attributedTouchType = "attributedTouchType";
        this.attributedTouchTime = "attributedTouchTime";
//        this.installTime = "installTime";
        this.costModel = "costModel";
        this.costValue = "costValue";
        this.subParam1 = "subParam1";
        this.subParam2 = "subParam2";
        this.subParam3 = "subParam3";
        this.subParam4 = "subParam4";
        this.customerUserId = "customerUserId";
        this.isRetargeting = true;
        this.storeProductPage = "storeProductPage";
        this.costCurrency = "costCurrency";
        this.eventSource = "eventSource";
        this.partnerId = attributionclick.getPId();
        this.partnerName = attributionclick.getPName();
        this.mediaSource = "mediaSource";
        this.channel = "channel";
        this.keywords = "keywords";
        this.retargetingConversionType = "retargetingConversionType";
        this.isPrimaryAttribution = true;
        this.attributionLookback = "attributionLookback";
        this.reengagementWindow = "reengagementWindow";
        this.matchType = "matchType";
        this.httpReferrer = "httpReferrer";
        this.campaign = "campaign";
        this.campaignId = "campaignId";
        this.adset = "adset";
        this.adsetId = "adsetId";
        this.ad = "ad";
        this.originalUrl = "originalUrl";
        this.adId = "adId";
        this.adType = "adType";
        this.siteId = "siteId";

        this.rejectedReason = "rejectedReason";
        this.rejectedReasonValue = "rejectedReasonValue";
        this.blockedReason = "blockedReason";
        this.blockedReasonRule = "blockedReasonRule";
        this.blockedReasonValue = "blockedReasonValue";
        this.blockedSubReason = "blockedSubReason";*/

    }

    public AttributionAllReport(AttributionAllReport attribution, AttributionClick click) {

        this.id = attribution.getId();
        this.appVersion = attribution.getAppVersion();
        this.sdkVersion = attribution.getSdkVersion();
        this.appId = attribution.getAppId();
        this.appName = attribution.getAppName();
        this.appType = attribution.getAppType();
        this.bundleId = attribution.getBundleId();


// event
        this.eventTime = attribution.getEventTime();
        this.eventName = attribution.getEventName();
        this.eventValue = attribution.getEventValue();
        this.eventRevenue = attribution.getEventRevenue();
        this.eventRevenueCurrency = attribution.getEventRevenueCurrency();
        this.eventRevenueUSD = attribution.getEventRevenueUSD();


        // device
        this.att = attribution.getAtt();
        this.wifi = attribution.getWifi();
        this.operator = attribution.getOperator();
        this.carrier = attribution.getCarrier();
        this.language = attribution.getLanguage();
        this.androidId = attribution.getAndroidId();
        this.imei = attribution.getImei();
        this.idfa = attribution.getIdfa();
        this.idfv = attribution.getIdfv();
        this.advertisingId = attribution.getAdvertisingId();
        this.deviceModel = attribution.getDeviceModel();
        this.deviceType = attribution.getDeviceType();
        this.deviceCategory = attribution.getDeviceCategory();
        this.platform = attribution.getPlatform();
        this.osVersion = attribution.getOsVersion();
        this.userAgent = attribution.getUserAgent();
        this.deviceDownloadTime = attribution.getDeviceDownloadTime();
        this.dayTimestamp = attribution.getDayTimestamp();
        this.oaid = attribution.getOaid();
        this.isLAT = attribution.isLAT();

        // device location
        this.region = attribution.getRegion();
        this.countryCode = attribution.getCountryCode();
        this.state = attribution.getState();
        this.city = attribution.getCity();
        this.postalCode = attribution.getPostalCode();
        this.ip = attribution.getIp();

        //
        this.attributedTouchType = attribution.getAttributedTouchType();
        this.partnerId = attribution.getPartnerId();
        this.partnerName = attribution.getPartnerName();

    }

    public AttributionAllReport(App app,UserAgent userAgent, Map deviceLocation, Impression impression) {

        this.id = UUID.randomUUID().toString().replace("-", "");
        this.appVersion = app.getAppVersion();
        this.sdkVersion = app.getSdkVersion();
        this.appId = app.getId();
        this.appName = app.getName();
        this.appType = app.getAppType();
        this.bundleId = app.getBundleId();

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

        /*this.eventTime = dateFormat.format(new Date());
        this.eventName = null;
        this.eventValue = null;
        this.eventRevenue = Double.parseDouble(null);
        this.eventRevenueCurrency = null;
        this.eventRevenueUSD = null;*/


        // device
        this.att = "att";
        this.wifi = "wifi";
        this.operator = "operator";
        this.carrier = "carrier";
        this.language = "language";
        this.androidId = "androidId";
        this.imei = "imei";
        this.idfa = "idfa";
        this.idfv = "idfv";
        this.advertisingId = "advertisingId";
        this.deviceModel = (userAgent.getValue("DeviceName") == null) ? "" : userAgent.getValue("DeviceName");
        this.deviceType = (userAgent.getValue("DeviceClass") == null) ? "" : userAgent.getValue("DeviceClass");
        this.deviceCategory = (userAgent.getValue("OperatingSystemClass") == null) ? "" : userAgent.getValue("OperatingSystemClass");
        this.platform = (userAgent.getValue("DeviceClass") == null) ? "" : userAgent.getValue("DeviceClass");
        this.osVersion = "osVersion";
        this.userAgent = String.valueOf(userAgent);
        this.deviceDownloadTime = dateFormat.format(new Date());
        this.dayTimestamp = dateFormat.format(new Date());
        this.oaid = "oaid";
        this.isLAT = true;

        // device location
        this.region = "region";
        this.countryCode = (String) deviceLocation.get("countryIsoCode");
        this.state = (String) deviceLocation.get("subdivisionName");
        this.city = (String) deviceLocation.get("city");
        this.postalCode = (String) deviceLocation.get("postal");
        this.ip = (String) deviceLocation.get("ip");

        this.attributedTouchType = "Impression";

        this.partnerId = impression.getPartnerId();
        this.partnerName = impression.getPartnerName();

/*// Attribution
        this.attributedTouchType = "attributedTouchType";
        this.attributedTouchTime = "attributedTouchTime";
//        this.installTime = "installTime";
        this.costModel = "costModel";
        this.costValue = "costValue";
        this.subParam1 = "subParam1";
        this.subParam2 = "subParam2";
        this.subParam3 = "subParam3";
        this.subParam4 = "subParam4";
        this.customerUserId = "customerUserId";
        this.isRetargeting = true;
        this.storeProductPage = "storeProductPage";
        this.costCurrency = "costCurrency";
        this.eventSource = "eventSource";
        this.partnerId = attributionclick.getPId();
        this.partnerName = attributionclick.getPName();
        this.mediaSource = "mediaSource";
        this.channel = "channel";
        this.keywords = "keywords";
        this.retargetingConversionType = "retargetingConversionType";
        this.isPrimaryAttribution = true;
        this.attributionLookback = "attributionLookback";
        this.reengagementWindow = "reengagementWindow";
        this.matchType = "matchType";
        this.httpReferrer = "httpReferrer";
        this.campaign = "campaign";
        this.campaignId = "campaignId";
        this.adset = "adset";
        this.adsetId = "adsetId";
        this.ad = "ad";
        this.originalUrl = "originalUrl";
        this.adId = "adId";
        this.adType = "adType";
        this.siteId = "siteId";

        this.rejectedReason = "rejectedReason";
        this.rejectedReasonValue = "rejectedReasonValue";
        this.blockedReason = "blockedReason";
        this.blockedReasonRule = "blockedReasonRule";
        this.blockedReasonValue = "blockedReasonValue";
        this.blockedSubReason = "blockedSubReason";*/

    }
}



