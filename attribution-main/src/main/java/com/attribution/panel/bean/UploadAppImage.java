package com.attribution.panel.bean;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
public class UploadAppImage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String fileName;
    private String fileType;
    private String Url;
    @OneToOne(fetch = FetchType.EAGER)
    @ToString.Exclude
    private App app;
    @CreationTimestamp
    private Date creationDateTime;
}
