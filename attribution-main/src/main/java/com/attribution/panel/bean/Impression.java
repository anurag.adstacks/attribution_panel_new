package com.attribution.panel.bean;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Data
@NoArgsConstructor
@Document(indexName = "impression",shards = 1,replicas = 1)
@Component
public class Impression implements Serializable {

    private String id;

    private String ip;
    private String os;
    private String google_aid;
    private String deviceName;
    private String sectionId;
    private int sectionNum;

    private String country;

    private Long appId;
    private String appName;

    private Long partnerId;
    private String partnerName;

//    private App app;

    private String creationDateTime;


}
