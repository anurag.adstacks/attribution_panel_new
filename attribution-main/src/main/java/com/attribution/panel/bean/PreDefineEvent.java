
package com.attribution.panel.bean;

import com.attribution.panel.enums.AccessStatus;
import com.attribution.panel.enums.ConversionPoint;
import com.attribution.panel.enums.PayoutModel;
import com.attribution.panel.enums.RevenueModel;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "PreDefineEvent")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor
public class PreDefineEvent implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @JsonProperty("id")
    private Long id;

    //    @JsonProperty("event_name")
    private String eventName;
    private String eventValue;
    private double revenue;
    private String eventRevenueCurrency;
    private double eventRevenueUsd;

    //    @JsonProperty("description")
    private String description;

    //	@JsonIgnore
    private boolean status;

    //	@JsonProperty("e_tkn")
    @EqualsAndHashCode.Include
    private String token; //Event token to be integrated in postback url

    private float payout;

    //	@JsonIgnore
//    @Enumerated(EnumType.STRING)
//    private ConvTrackProto proto;

//    @ManyToOne(fetch = FetchType.EAGER)
//    @JoinColumn(name = "app_id", nullable = false)
//    @JsonIgnore
//    @ToString.Exclude
//    @EqualsAndHashCode.Include
//    @NotFound(action = NotFoundAction.IGNORE)
//    private App app;

    //	@JsonIgnore
    private boolean privateEvent;

    //	@JsonIgnore
    @Enumerated(EnumType.STRING)
    private RevenueModel revenueModel;

    @Enumerated(EnumType.STRING)
    private PayoutModel payoutModel;

    //	@JsonIgnore
    @Enumerated(EnumType.STRING)
    private AccessStatus accessStatus;

    //	@JsonIgnore
    private boolean multiConv;

    private int eventCount;

    @Enumerated(EnumType.STRING)
    private ConversionPoint convPoint;


}




/*import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.util.Date;
@Entity
@Data
public class Event {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String eventName;
    private String eventKey;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "app_id", nullable = false)
    @JsonIgnore
    @ToString.Exclude
    @EqualsAndHashCode.Include
    @NotFound(action = NotFoundAction.IGNORE)
    private App app;
}*/
