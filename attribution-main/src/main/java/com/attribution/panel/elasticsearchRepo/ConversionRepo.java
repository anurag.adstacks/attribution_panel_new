package com.attribution.panel.elasticsearchRepo;

import com.attribution.panel.bean.AttributionConversion;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface ConversionRepo extends ElasticsearchRepository<AttributionConversion, String> {


//    List<AttributionConversion> findByClickId(String id);

    List<AttributionConversion> findByClickIdAndAppIdAndPartnerIdOrderByCreationDateTimeDesc(String clickId, Long appId, Long pId);

    Map<Object, Object> findByEventIDAndClickId(Long id, String clickId);

}
