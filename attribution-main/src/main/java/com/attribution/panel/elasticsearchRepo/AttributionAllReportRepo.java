package com.attribution.panel.elasticsearchRepo;

import com.attribution.panel.bean.AttributionAllReport;
import com.attribution.panel.bean.AttributionRecord;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface AttributionAllReportRepo extends ElasticsearchRepository<AttributionAllReport, String> {

    AttributionRecord findByAppIdAndPartnerIdAndDayTimestamp(Long appId, Long partnerId, String dayTimestamp);


}


