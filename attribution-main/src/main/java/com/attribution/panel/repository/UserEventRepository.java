package com.attribution.panel.repository;

import com.attribution.panel.bean.Event;
import com.attribution.panel.bean.UserEvent;
import com.attribution.panel.console.dao.UserEventRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserEventRepository extends JpaRepository<UserEvent, Long>, UserEventRepositoryCustom {
//    List<UserEvent> findByApp(App app);

    List<UserEvent> findByAppIdAndEvent(Long app, Event event);
    /*@Query(value = "SELECT * FROM user_event where app_id=:appId And creation_date_time between :startDate and :endDate limit :first,:len", nativeQuery = true)
    List<UserEvent> findByAppIdAndStartBetweenEndDate(@Param("appId") Long appId, @Param("startDate") String startDate, @Param("endDate") String endDate, @Param("first") int first, @Param("len") int len);*/
    @Query(value = "SELECT count(*) FROM user_event where app_id=:appId And event_id=:eventId And creation_date_time between :startDate and :endDate", nativeQuery = true)
    Long findByAppAndEventAndStartDateAndEndDateCount(@Param("appId") Long appId, @Param("eventId") Long eventId, @Param("startDate") String startDate, @Param("endDate") String endDate);
    /*@Query(value = "SELECT count(*) FROM user_event where app_id=:appId And creation_date_time between :startDate and :endDate", nativeQuery = true)
    Long getUserEventFilterCount(@Param("appId") Long appId, @Param("startDate") String startDate, @Param("endDate") String endDate);*/

    @Query(value = "Select count(*) FROM user_event where app_id=:app_id And event_id=:eventId And google_aid=:google_aid", nativeQuery = true)
    Long findByEventAndGoogle_aid(@Param("app_id") Long app_id, @Param("eventId") Long eventId, @Param("google_aid") String google_aid);

}
