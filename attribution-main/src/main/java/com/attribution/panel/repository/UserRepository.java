package com.attribution.panel.repository;

import com.attribution.panel.bean.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    List<User> findAll();

    User findByEmail(String email);

    @Query(value = "select * from  User where  email=?;", nativeQuery = true)
    List<User> getEmailByUser(String value);
}
