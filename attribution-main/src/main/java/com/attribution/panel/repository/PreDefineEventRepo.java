package com.attribution.panel.repository;

import com.attribution.panel.bean.PreDefineEvent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PreDefineEventRepo extends JpaRepository<PreDefineEvent, Long> {
    Page<PreDefineEvent> findByEventNameContainingIgnoreCaseOrId(String search, Long searchLong, Pageable pageRequest);

    List<PreDefineEvent> findByIdIn(List<Long> ajaxPreDefineEvent);

}
