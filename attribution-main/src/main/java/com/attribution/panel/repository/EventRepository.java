package com.attribution.panel.repository;

import com.attribution.panel.bean.App;
import com.attribution.panel.bean.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EventRepository extends JpaRepository<Event, Long> {
    Event findByEventKey(String eventKey);

    List<Event> findByApp(App app);

    List<Event> findByAppAndIdIn(App app, List<Long> eventIds);
    @Query(value = "select * from event where app_id=:appId limit 0, 8", nativeQuery = true)
    List<Event> findByOfferId(@Param("appId") Long id);


    List<Event> findByAppId(Long appId);
}
