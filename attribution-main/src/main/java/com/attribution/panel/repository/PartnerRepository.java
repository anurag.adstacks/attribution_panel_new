package com.attribution.panel.repository;

import com.attribution.panel.bean.Partner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PartnerRepository extends JpaRepository<Partner, Long> {


    Page<Partner> findByCompanyContainingIgnoreCaseOrId(String search, Long searchLong, Pageable pageRequest);

    Partner findByEmail(String name);

}
