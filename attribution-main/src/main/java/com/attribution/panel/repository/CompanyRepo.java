package com.attribution.panel.repository;

import com.attribution.panel.bean.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CompanyRepo extends JpaRepository<Company, Long> {


}
