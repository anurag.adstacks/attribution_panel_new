package com.attribution.panel.repository;


import com.attribution.panel.bean.App;
import com.attribution.panel.console.dao.AppRepositoryCustom;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AppRepository extends JpaRepository<App, Long>, AppRepositoryCustom {

    Page<App> findByNameContainingIgnoreCaseOrId(String search, Long searchLong, Pageable pageRequest);

}