package com.attribution.panel.cache.dto;//package com.attribution.panel.cache.dto;
//
//import com.attribution.panel.enums.AccessStatus;
//import com.attribution.panel.enums.CapScope;
//import com.attribution.panel.enums.CapsType;
//import com.attribution.panel.enums.Redirect;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//import org.springframework.data.redis.core.RedisHash;
//import org.springframework.data.redis.core.index.Indexed;
//
//import javax.persistence.EnumType;
//import javax.persistence.Enumerated;
//import java.util.Set;
//
//@RedisHash("capCache")
//@Data
//@NoArgsConstructor
//public class CapCacheDTO {
//
//    private Long id;
//    @Enumerated(EnumType.STRING)
//    private CapsType capsType;
//    @Indexed
//    private Long offerId;
//    private Set<Long> events;
//    private Set<Long> publishers;
//    @Enumerated(EnumType.STRING)
//    private AccessStatus capVisibility;
//    @Enumerated(EnumType.STRING)
//    private CapScope capScope;
//    private Integer daily;
//    private Integer monthly;
//    private Integer lifetime;
//    private boolean pausePublisherCampaign;
//    @Enumerated(EnumType.STRING)
//    private Redirect redirect;
//    private boolean isUsed;
//
//
//}
//
