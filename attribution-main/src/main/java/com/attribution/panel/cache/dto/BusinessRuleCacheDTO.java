package com.attribution.panel.cache.dto;//package com.attribution.panel.cache.dto;
//
//import com.attribution.panel.enums.ActionRule;
//import com.attribution.panel.enums.Scope;
//import com.attribution.panel.enums.StatusRule;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//import org.springframework.data.redis.core.RedisHash;
//
//import javax.persistence.Id;
//import java.util.List;
//
//@RedisHash("businessRuleCache")
//@Data
//@NoArgsConstructor
//public class BusinessRuleCacheDTO {
//
//    @Id
//    private Long id;
//    private String name;
//    private List<Long> offer;
//    private List<Long> pub;
//    private List<Long> advert;
//    private StatusRule statusRule;//TYPE OF RULE ~ CR,CONVERSION,CUT FACTOR, SUB-AFFILIATE BLOCK
//    private Long numClicks;
//    private Long conversions;
//    private String cutFactorPercent;
//    private Long thresholdConversion;
//    private Float crPercent;
//    private String subPubIds;
//    private ActionRule actionRule;    //ACTION PERFORMED - PAUSE OFFER/ BLOCK CAMPAIGN
//    private boolean isAllPublishers;
//    private Scope scope;
//    private String event;
//
//}
