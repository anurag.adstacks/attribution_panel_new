//package com.adstacks.attribution.cache.repository;
//
//import com.attribution.panel.cache.dto.AppTrackingDTO;
//import org.springframework.data.repository.CrudRepository;
//import org.springframework.stereotype.Repository;
//
//import java.util.List;
//
//@Repository
//public interface AppTrackingDTORepository extends CrudRepository<AppTrackingDTO, Long> {
//
//    List<AppTrackingDTO> findAllByAdvertiserId(List<Long> advertiserIds);
//
//    List<AppTrackingDTO> findByAdvertiserId(Long advertiserIds);
//}
