package com.attribution.panel.cache.dto;

import com.attribution.panel.bean.Category;
import com.attribution.panel.bean.Country;
import com.attribution.panel.bean.*;
import com.attribution.panel.enums.AppStatus;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;

//@RedisHash("appCache")
@Data
@NoArgsConstructor
public class AppTrackingDTO implements Serializable {

    @Id
    private Long id;

    private String name;

    /*@Enumerated(EnumType.STRING)
    private OS os;*/

    private String devOS;
    private String description;

    private String appVersion;
    private String sdkVersion;

    private String appType;
    private String bundleId;  //The Apple bundle ID is a unique identifier associated with iOS apps.


    private String gameLink;

    private Date creationDateTime;

    private Date updateDateTime;

    private User user;

    private List<Partner> approvedPartner;

    private Long compId;

    private Set<Company> approvedCompany;

    private byte[] appLogo;

    private String previewUrl;

    private String trackingUrl;

    private String uuid = UUID.randomUUID().toString().replace("-", "");

    private Long allClicks = 0L;

    private Long grossClicks = 0L;

    private Long conversions = 0L;

    private List<Event> events;

    private List<Category> category;

    private AppStatus status; //

    public List<Country> countries;

    public AppTrackingDTO(App app) {
        this.id = app.getId();
        this.name = app.getName();
        this.devOS = app.getDevOS();
        this.description = app.getDescription();
        this.appVersion = app.getAppVersion();
        this.sdkVersion = app.getSdkVersion();
        this.appType = app.getAppType();
        this.bundleId = app.getBundleId();
        this.gameLink = app.getGameLink();
        this.creationDateTime = app.getCreationDateTime();
        this.updateDateTime = app.getUpdateDateTime();
        this.user = app.getUser();
        this.approvedPartner = app.getApprovedPartner();
        this.compId = app.getCompId();
        this.approvedCompany = app.getApprovedCompany();
        this.appLogo = app.getAppLogo();
        this.previewUrl = app.getPreviewUrl();
        this.trackingUrl = app.getTrackingUrl();
        this.uuid = app.getUuid();
        this.allClicks = app.getAllClicks();
        this.grossClicks = app.getGrossClicks();
        this.conversions = app.getConversions();
        this.events = app.getEvents();
        this.category = app.getCategory();
        this.status = app.getStatus();
        this.countries = app.getCountries();
    }




//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Long id;
//    private String name;
//
//    /*@Enumerated(EnumType.STRING)
//    private OS os;*/
//
//    private String devOS;
//    private String description;
//
//    private String appVersion;
//    private String sdkVersion;
//
//    private String appType;
//    private String bundleId;  //The Apple bundle ID is a unique identifier associated with iOS apps.
//
//
//    private String gameLink;
//
//    @CreationTimestamp
//    private Date creationDateTime;
//
//    @UpdateTimestamp
//    private Date updateDateTime;
//
//    @ManyToOne(fetch = FetchType.EAGER)
//    private UserTrackingDTO user;
//
//    @ManyToMany(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
//    private List<PartnerDTO> approvedPartner;
//
//    private Long compId;
//
//    @ManyToMany(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
//    private Set<CompanyDTO> approvedCompany;
//
//
//    @Lob
//    private byte[] appLogo;
//
//    private String previewUrl;
//
//    private String trackingUrl;
//
//    @Column(unique = true)
//    @DiffIgnore
//    private String uuid = UUID.randomUUID().toString().replace("-", "");
//
//    @Column(columnDefinition = "bigint(20) default 0")
//    private Long allClicks = (long) 0;
//
//    @Column(columnDefinition = "bigint(20) default 0")
//    private Long grossClicks = (long) 0;
//
//    @Column(columnDefinition = "bigint(20) default 0")
//    private Long conversions = (long) 0;
//
//    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "app", orphanRemoval = true)
//    @Fetch(FetchMode.SELECT)
//    @JsonIgnoreProperties("app")
//    @DiffIgnore
//    private List<EventCacheDTO> events;
//
//    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "app")
//    @Fetch(FetchMode.SELECT)
//    @JsonIgnoreProperties("app")
//    @DiffIgnore
//    private List<Category> category;
//
//    @Enumerated(EnumType.STRING)
//    private AppStatus status; //
//
//
//
//    @ManyToMany(fetch = FetchType.LAZY)
//    @Enumerated(EnumType.STRING)
//    public List<Country> countries;

}
