package com.attribution.panel.cache.dto;//package com.attribution.panel.cache.dto;
//
//import com.attribution.panel.enums.CampaignStatus;
//import com.attribution.panel.enums.PayoutModel;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//import org.springframework.data.annotation.Id;
//import org.springframework.data.redis.core.RedisHash;
//import org.springframework.data.redis.core.index.Indexed;
//
//import java.io.Serializable;
//
//@RedisHash("campaignCache")
//@Data
//@NoArgsConstructor
//public class CampaignTrackingDTO implements Serializable {
//    @Id
//    private String id;
//    @Indexed
//    private Long offerId;
//    @Indexed
//    private Long pubId;
//    private PayoutModel payoutModel;
//    private float payout;
//    private float revenue;
//    private long totalClicks;
//    private long grossClicks;
//    private long conversions;
//    private String trackingUrl;
//    private String pubName;
//    private String offerName;
//    private String advName;
//    private float cr;
//    private String creationDateTime;
//    private String updateDateTime;
//    private String uuid;
//    private CampaignStatus campaignStatus;
//    private String publisherMessage;
//
//}
