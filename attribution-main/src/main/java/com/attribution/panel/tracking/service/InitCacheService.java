package com.attribution.panel.tracking.service;

import com.attribution.panel.bean.App;
import com.attribution.panel.bean.Company;
import com.attribution.panel.bean.Event;
import com.attribution.panel.bean.Partner;
import com.attribution.panel.cache.dto.AppTrackingDTO;
import com.attribution.panel.cache.dto.CompanyTrackingDTO;
import com.attribution.panel.cache.dto.EventCacheDTO;
import com.attribution.panel.cache.dto.PartnerTrackingDTO;
import com.attribution.panel.repository.AppRepository;
import com.attribution.panel.repository.CompanyRepo;
import com.attribution.panel.repository.EventRepository;
import com.attribution.panel.repository.PartnerRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


@Service
@Slf4j
@Transactional
public class InitCacheService {

    @Autowired
    InitCacheService initCacheService;

    @Autowired
    AppRepository offerRepository;

    @Autowired
    CompanyRepo advertiserRepository;

    @Autowired
    PartnerRepository publisherRepository;

//    @Autowired
//    IpRangeInfoRepository ipRangeInfoRepository;

    @Autowired
    EventRepository eventRepository;

//    @Autowired
//    CacheUpdateService cacheUpdateService;


    public List<AppTrackingDTO> offerList() {
        List<App> offerList = Collections.EMPTY_LIST;
        try {
            offerList = offerRepository.findAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("InitCacheService " + offerList);
        List<AppTrackingDTO> offerTrackingDTOList = new ArrayList<>();
        AppTrackingDTO offerTrackingDTO = null;
        for (App offer : offerList) {
            offerTrackingDTO = new AppTrackingDTO(offer);
            offerTrackingDTOList.add(offerTrackingDTO);
        }
        System.out.println("InitCacheService2 " + offerList);
        return offerTrackingDTOList;
    }


    public List<CompanyTrackingDTO> advertiserList() {
        List<Company> advertiserList = Collections.EMPTY_LIST;
        try {
            advertiserList = advertiserRepository.findAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        List<CompanyTrackingDTO> advertiserTrackingDTOList = new ArrayList<>();
        CompanyTrackingDTO advertiserTrackingDTO = null;
        for (Company advertiser : advertiserList) {
            advertiserTrackingDTO = new CompanyTrackingDTO(advertiser);
            advertiserTrackingDTOList.add(advertiserTrackingDTO);
        }
        return advertiserTrackingDTOList;
    }


    public List<PartnerTrackingDTO> publisherList() {
        List<Partner> publisherList = Collections.EMPTY_LIST;
        try {
            publisherList = publisherRepository.findAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        List<PartnerTrackingDTO> publisherTrackingDTOList = new ArrayList<>();
        PartnerTrackingDTO publisherTrackingDTO = null;
        for (Partner publisher : publisherList) {
            publisherTrackingDTO = new PartnerTrackingDTO(publisher);
            publisherTrackingDTOList.add(publisherTrackingDTO);
        }
        return publisherTrackingDTOList;
    }

//    @Transactional(noRollbackFor = RuntimeException.class)
    public List<EventCacheDTO> eventList() {
        List<Event> events = Collections.EMPTY_LIST;
        try {
            events = eventRepository.findAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        List<EventCacheDTO> eventCacheDTOS = new ArrayList<>();
        EventCacheDTO eventCacheDTO = null;
        for (Event event : events) {
            eventCacheDTO = new EventCacheDTO(event);
            eventCacheDTOS.add(eventCacheDTO);
        }
        System.out.println("yooevent " + eventCacheDTOS);
        return eventCacheDTOS;
    }

}
