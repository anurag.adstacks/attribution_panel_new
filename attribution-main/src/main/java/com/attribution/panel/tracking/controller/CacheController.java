package com.attribution.panel.tracking.controller;

import com.attribution.panel.cache.dto.AppTrackingDTO;
import com.attribution.panel.cache.dto.CompanyTrackingDTO;
import com.attribution.panel.cache.dto.EventCacheDTO;
import com.attribution.panel.cache.dto.PartnerTrackingDTO;
import com.attribution.panel.repository.AppRepository;
import com.attribution.panel.repository.CompanyRepo;
import com.attribution.panel.repository.EventRepository;
import com.attribution.panel.repository.PartnerRepository;
import com.attribution.panel.tracking.service.InitCacheService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Slf4j
@RequestMapping(value = "/adminConsole/cache")
public class CacheController {

    @Autowired
    InitCacheService initCacheService;

    @Autowired
    AppRepository offerRepository;

    @Autowired
    CompanyRepo advertiserRepository;

    @Autowired
    PartnerRepository publisherRepository;

//    @Autowired
//    IpRangeInfoRepository ipRangeInfoRepository;

    @Autowired
    EventRepository eventRepository;


    @GetMapping("/offers")
    public List<AppTrackingDTO> offers() {
        return initCacheService.offerList();
    }

    @GetMapping("/publishers")
    public List<PartnerTrackingDTO> publishers() {
        return initCacheService.publisherList();
    }

    @GetMapping("/advertisers")
    public List<CompanyTrackingDTO> advertisers() {
        return initCacheService.advertiserList();
    }

    @GetMapping("/events")
    public List<EventCacheDTO> events() {
        return initCacheService.eventList();
    }


}