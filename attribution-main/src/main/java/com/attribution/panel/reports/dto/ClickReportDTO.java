//package com.adstacks.attribution.reports.dto;
//
//import com.etreetech.panel.beans.Click;
//import com.etreetech.panel.cache.dto.OfferTrackingDTO;
//import com.etreetech.panel.cache.dto.PublisherTrackingDTO;
//import lombok.Data;
//import lombok.extern.slf4j.Slf4j;
//
//@Data
//@Slf4j
//public class ClickReportDTO {
//    private String uuid;
//    private String pubClickId;
//    private String offerName;
//    private Long offerId;
//    private String deviceName;
//    private String creationDateTime;
//    private String publisherName;
//    private Long pubId;
//    private String ip;
//    private Long advertiserId;
//
//    public ClickReportDTO(Click click, OfferTrackingDTO offerTrackingDTO, PublisherTrackingDTO publisherTrackingDTO) {
//        this.uuid = click.getId();
//        this.pubClickId = click.getPubClickId();
//        this.deviceName = click.getDeviceId().getDeviceName();
//        this.creationDateTime = click.getCreationDateTime();
//        this.ip = click.getIp();
//        this.offerId=click.getOfferId();
//        this.offerName=(offerTrackingDTO!=null)?offerTrackingDTO.getOfferName():null;
//        this.pubId=click.getPubId();
//        this.publisherName=(publisherTrackingDTO !=null)?publisherTrackingDTO.getFname():null;
//        this.advertiserId=click.getAdvertiserId();
//    }
//}
