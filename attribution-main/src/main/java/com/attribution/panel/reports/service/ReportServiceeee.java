//package com.adstacks.attribution.reports.service;
//
//import com.etreetech.panel.beans.Click;
//import com.etreetech.panel.beans.Conversion;
//import com.etreetech.panel.cache.dto.AdvertiserTrackingDTO;
//import com.etreetech.panel.cache.dto.OfferTrackingDTO;
//import com.etreetech.panel.cache.dto.PublisherTrackingDTO;
//import com.etreetech.panel.cache.service.CacheService;
//import com.etreetech.panel.dao.ClickQueryBuilder;
//import com.etreetech.panel.reports.dao.ReportQueryBuilder;
//import com.etreetech.panel.reports.dto.AllReportDTO;
//import com.etreetech.panel.reports.dto.ClickReportDTO;
//import com.etreetech.panel.reports.dto.ConversionReportDTO;
//import com.etreetech.panel.reports.dto.ReportFilterDTO;
//import com.etreetech.panel.repository.ConversionRepository;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.domain.PageRequest;
//import org.springframework.data.domain.Pageable;
//import org.springframework.data.domain.Sort;
//import org.springframework.data.jpa.datatables.mapping.Column;
//import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
//import org.springframework.data.jpa.datatables.mapping.Order;
//import org.springframework.stereotype.Service;
//
//import java.text.SimpleDateFormat;
//import java.util.*;
//import java.util.stream.Collectors;
//
//import static java.util.Arrays.stream;
//
//@Service
//@Slf4j
//public class ReportServiceeee {
//    @Autowired
//    ConversionRepository conversionRepository;
//    @Autowired
//    ClickQueryBuilder clickQueryBuilder;
//    @Autowired
//    ReportQueryBuilder reportQueryBuilder;
//    @Autowired
//    CacheService cacheService;
//
//    public Map<String, Object> reportConversionAjax(DataTablesInput input) {
//        Map<String, String> map = getDates(input, "creationDateTime");
//        String startDate = map.get("startDate");
//        String endDate = map.get("endDate");
//        List<String> offerArr = getOfferArray(input, "conversionStatus");
//        List<String> pubArr = getPublisherArray(input, "conversionMessage");
//        List<String> conversionArr = getConversionStatusArray(input, "eventName");
//        List<String> advArr = getPublisherArray(input, "uuid");
//        Pageable pageable = getPageableConversion(input, "conversion");
////        log.info("Conversion array:" + conversionArr);
////        log.info("Pub array:" + pubArr);
////        log.info("Offer array:" + offerArr);
////        log.info("Start Date :" + startDate + " End Date:" + endDate);
//        Map<String, Object> res = getConversionReport(offerArr, pubArr, advArr, startDate, endDate, pageable, conversionArr, input);
//        res.put("draw", input.getDraw());
//        res.put("error", "");
//        return res;
//    }
//
//    public Map<String, Object> reportClickAjax(DataTablesInput input) {
//        Map<String, String> map = getDates(input, "creationDateTime");
//        String startDate = map.get("startDate");
//        String endDate = map.get("endDate");
//        List<String> offerArr = getOfferArray(input, "offerId");
//        List<String> pubArr = getPublisherArray(input, "pubId");
//        List<String> advArr = getAdvertiserArray(input, "offerName");
////        System.out.println("advIds ="+advArr);
//        Column uuidSearch = input.getColumn("uuid");
//        String uuid = uuidSearch.getSearch().getValue();
//        Pageable pageable = getPageableClick(input, "click");
////        log.info("Pub array:" + pubArr);
////        log.info("Offer array:" + offerArr);
////        log.info("ClickId:" + uuid);
////        log.info("Start Date :" + startDate + " End Date:" + endDate);
//        Map<String, Object> res = getClickReport(offerArr, pubArr, startDate, endDate, pageable, uuid, advArr, input);
//        res.put("draw", input.getDraw());
//        res.put("error", "");
//        return res;
//    }
//
//    private List<String> getAdvertiserArray(DataTablesInput input, String column) {
//        Column advertiser = input.getColumn(column);
//        List<String> advArray = new ArrayList<>();
//        String advId = "";
//        if (!advertiser.getSearch().getValue().equals("")) {
//            advId = advertiser.getSearch().getValue();
//            advArray = stream(advId.split(",")).collect(Collectors.toList());
//        }
//        return advArray;
//    }
//
//    public Set<AllReportDTO> reportAllAjax(ReportFilterDTO reportFilterDTO) {
//        Map<String, String> map = getDates((reportFilterDTO.getDaterange() == null) ? "" : reportFilterDTO.getDaterange());
//        String startDate = map.get("startDate");
//        String endDate = map.get("endDate");
//        List<String> offerArr = reportFilterDTO.getOffers();
//        List<String> pubArr = reportFilterDTO.getPublishers();
//        List<String> advArr = reportFilterDTO.getAdvertisers();
//        String subAffiliate = reportFilterDTO.getSubAffiliate();
//        List<String> groupByList = getGroupByList(reportFilterDTO.getGroupBy());
//        Set<AllReportDTO> allReportDTOSet = null;
//        try {
//            allReportDTOSet = reportQueryBuilder.allReportCHR(startDate, endDate, pubArr, offerArr, advArr, groupByList, subAffiliate);
//            allReportDTOSet = populateFieldsInList(allReportDTOSet, groupByList);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return allReportDTOSet;
//    }
//
//    public List<String> getGroupByList(List<String> groupBy) {
//        List<String> groupByList = new ArrayList<>();
//        if (groupBy == null) {
//            groupByList.add("Offer");
//        } else {
//            return groupBy;
//        }
//        return groupByList;
//    }
//
//
//    public Set<AllReportDTO> populateFieldsInList(Set<AllReportDTO> allReportDTOSet, List<String> groupByList) throws Exception {
//        for (AllReportDTO allReportDTO : allReportDTOSet) {
//            for (String group : groupByList) {
//
//                switch (group) {
//                    case "Offer":
//                        OfferTrackingDTO offer = cacheService.getOffer(allReportDTO.getOfferId());
//                        AdvertiserTrackingDTO adv = cacheService.getAdvertiser(offer.getAdvertiserId());
//                        allReportDTO.setOfferName(offer.getOfferName());
//                        allReportDTO.setAdvertiserId(offer.getAdvertiserId());
//                        allReportDTO.setAdvertiserName(adv.getFname());
//                        break;
//                    case "Advertiser":
//                        AdvertiserTrackingDTO advert = cacheService.getAdvertiser(allReportDTO.getAdvertiserId());
//                        allReportDTO.setAdvertiserName(advert.getFname());
//                        break;
//                    case "Publisher":
//                        allReportDTO.setPublisherName(cacheService.getPublisher(allReportDTO.getPublisherId()).getFname());
//                        break;
//                    case "Sub_Aff":
//                        break;
//                }
//
//            }
//        }
//        return allReportDTOSet;
//    }
//
//    public Map<String, String> getDates(DataTablesInput input, String column) {
//        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
//        SimpleDateFormat formatterForJsDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        Column dateRange = input.getColumn(column);
//        String dateFromUI = "";
//        if (dateRange != null) {
//            dateFromUI = dateRange.getSearch().getValue();
//        }
//        String startDate = null;
//        String endDate = null;
//        if (dateFromUI.equals("")) {
//            endDate = formatter.format(new Date());
//            Calendar cal = Calendar.getInstance();
//            cal.add(Calendar.DATE, -7);
//            startDate = formatter.format(cal.getTime());
//        } else {
//            String[] startDateArr = dateFromUI.split(" - ");
//            try {
//                startDate = formatter.format(formatterForJsDate.parse(startDateArr[0]));
//                endDate = formatter.format(formatterForJsDate.parse(startDateArr[1]));
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//        Map<String, String> mp = new HashMap<>();
//        mp.put("startDate", startDate);
//        mp.put("endDate", endDate);
//        return mp;
//    }
//
//    public Map<String, String> getDates(String dateFromUI) {
//        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
//        SimpleDateFormat formatterForJsDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        String startDate = null;
//        String endDate = null;
//        if (dateFromUI.equals("")) {
//            endDate = formatter.format(new Date());
//            Calendar cal = Calendar.getInstance();
//            cal.add(Calendar.DATE, -7);
//            startDate = formatter.format(cal.getTime());
//        } else {
//            String[] startDateArr = dateFromUI.split(" - ");
//            try {
//                startDate = formatter.format(formatterForJsDate.parse(startDateArr[0]));
//                endDate = formatter.format(formatterForJsDate.parse(startDateArr[1]));
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//        Map<String, String> mp = new HashMap<>();
//        mp.put("startDate", startDate);
//        mp.put("endDate", endDate);
//        return mp;
//    }
//
//    public List<String> getOfferArray(DataTablesInput input, String column) {
//        Column offer = input.getColumn(column);
//        List<String> offerArray = new ArrayList<>();
//        String offerId = "";
//        if (!offer.getSearch().getValue().equals("")) {
//            offerId = offer.getSearch().getValue();
//            offerArray = stream(offerId.split(",")).collect(Collectors.toList());
//        }
//        return offerArray;
//    }
//
//    public Pageable getPageableConversion(DataTablesInput input, String reportType) {
//        Map<String, String> orderMap = new HashMap<String, String>();
//        Order order = input.getOrder().get(0);
//        orderMap.put("orderColumnName", input.getColumns().get(order.getColumn()).getData());
//        orderMap.put("orderType", order.getDir());
//        int first = input.getStart();
//        int len = input.getLength();
//        int page = (int) (Math.ceil(first + 1) / len);
//        Pageable pageable;
//        String orderValue = orderMap.get("orderType");
//        if (orderValue.equals("desc")) {
//            switch (orderMap.get("orderColumnName")) {
//                case "offerId":
//                    pageable = PageRequest.of(page, len, Sort.by("offer.offerId").descending());
//                    break;
//                case "pubId":
//                    pageable = PageRequest.of(page, len, Sort.by("publisher.publisherId").descending());
//                    break;
//                default:
//                    pageable = PageRequest.of(page, len, Sort.by(orderMap.get("orderColumnName")).descending());
//                    break;
//            }
//        } else {
//            switch (orderMap.get("orderColumnName")) {
//                case "offerId":
//                    pageable = PageRequest.of(page, len, Sort.by("offer.offerId").ascending());
//                    break;
//                case "pubId":
//                    pageable = PageRequest.of(page, len, Sort.by("publisher.publisherId").ascending());
//                    break;
//                default:
//                    pageable = PageRequest.of(page, len, Sort.by(orderMap.get("orderColumnName")).ascending());
//                    break;
//            }
//        }
//        return pageable;
//    }
//
//    public Pageable getPageableClick(DataTablesInput input, String reportType) {
//        Map<String, String> orderMap = new HashMap<String, String>();
//        Order order = input.getOrder().get(0);
//        orderMap.put("orderColumnName", input.getColumns().get(order.getColumn()).getData());
//        orderMap.put("orderType", order.getDir());
//        int first = input.getStart();
//        int len = input.getLength();
//        int page = (int) (Math.ceil(first + 1) / len);
//        Pageable pageable;
//        String orderValue = orderMap.get("orderType");
//        if (orderValue.equals("desc")) {
//            switch (orderMap.get("orderColumnName")) {
//                case "offerId":
//                    pageable = PageRequest.of(page, len, Sort.by("offerId").descending());
//                    break;
//                case "pubId":
//                    pageable = PageRequest.of(page, len, Sort.by("pubId").descending());
//                    break;
//                default:
//                    pageable = PageRequest.of(page, len, Sort.by(orderMap.get("orderColumnName")).descending());
//                    break;
//            }
//        } else {
//            switch (orderMap.get("orderColumnName")) {
//                case "offerId":
//                    pageable = PageRequest.of(page, len, Sort.by("offerId").ascending());
//                    break;
//                case "pubId":
//                    pageable = PageRequest.of(page, len, Sort.by("pubId").ascending());
//                    break;
//                default:
//                    pageable = PageRequest.of(page, len, Sort.by(orderMap.get("orderColumnName")).ascending());
//                    break;
//            }
//        }
//        return pageable;
//    }
//
//    public List<String> getConversionStatusArray(DataTablesInput input, String column) {
//        Column conversionStatus = input.getColumn(column);
//        List<String> conversionArray = new ArrayList<>();
//        String conv = "";
//        if (!conversionStatus.getSearch().getValue().equals("")) {
//            conv = conversionStatus.getSearch().getValue();
//            conversionArray = stream(conv.split(",")).collect(Collectors.toList());
//            conversionArray = conversionArray.stream().collect(Collectors.toList());
//        }
//        return conversionArray;
//    }
//
//    public List<String> getPublisherArray(DataTablesInput input, String column) {
//        Column publisher = input.getColumn(column);
//        List<String> pubArray = new ArrayList<>();
//        String pubId = "";
//        if (!publisher.getSearch().getValue().equals("")) {
//            pubId = publisher.getSearch().getValue();
//            pubArray = stream(pubId.split(",")).collect(Collectors.toList());
//        }
//        return pubArray;
//    }
//
//    public Map<String, Object> getConversionReport(List<String> offerArr, List<String> pubArr, List<String> advArr, String startDate, String endDate, Pageable pageable,
//                                                   List<String> conversionArr, DataTablesInput input) {
//        List<ConversionReportDTO> conversionReportDTOS = new ArrayList<>();
//        Map<String, Object> map = reportQueryBuilder.conversionReport(startDate, endDate, pubArr, advArr, offerArr, conversionArr, pageable, input);
//        List<Conversion> conversionList = (List<Conversion>) map.get("conversionList");
//
//        Long count = (Long) map.get("count");
//
//        for (Conversion conversion : conversionList) {
//            Click click = null;
//            AdvertiserTrackingDTO advertiserTrackingDTO=null;
//            if (conversion.getClickId() != null) {
//                try {
//                    click = clickQueryBuilder.findById(conversion.getClickId());
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//            if (click.getPubId() != null) {
//                advertiserTrackingDTO = cacheService.getAdvertiser(conversion.getAdvertiserId());
//            }
//            conversionReportDTOS.add(new ConversionReportDTO(conversion, click,advertiserTrackingDTO));
//        }
//        Map<String, Object> res = new HashMap<String, Object>();
//        res.put("recordsFiltered", count);
//        res.put("recordsTotal", conversionRepository.count());
//        res.put("data", conversionReportDTOS);
//        return res;
//    }
//
//
//
//    public Map<String, Object> getClickReport(List<String> offerArr, List<String> pubArr, String startDate, String endDate, Pageable pageable, String uuid,
//                                              List<String> advArr, DataTablesInput input) {
//        List<ClickReportDTO> clickReportDTOS = new ArrayList<>();
//        Map<String, Object> map = reportQueryBuilder.clickReport(startDate, endDate, pubArr, offerArr, pageable, uuid, advArr, input);
//        List<Click> clickList = (List<Click>) map.get("clickList");
//        Long count = (Long) map.get("count");
//        for (Click click : clickList) {
//            OfferTrackingDTO offerTrackingDTO = null;
//            PublisherTrackingDTO publisherTrackingDTO = null;
//            try {
//                if (click.getOfferId() != null) {
//                    offerTrackingDTO = cacheService.getOffer(click.getOfferId());
//                }
//                if (click.getPubId() != null) {
//                    publisherTrackingDTO = cacheService.getPublisher(click.getPubId());
//                }
//                clickReportDTOS.add(new ClickReportDTO(click, offerTrackingDTO, publisherTrackingDTO));
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//        }
//        Map<String, Object> res = new HashMap<String, Object>();
//        res.put("recordsFiltered", count);
//        res.put("recordsTotal", clickQueryBuilder.count());
//        res.put("data", clickReportDTOS);
//        return res;
//    }
//
//    /*public Map<String, Object> getConversionExportData(String startDate, String endDate, List<String> offerArr, List<String> pubArr,
//                                                       List<String> conversionArr) throws IOException {
//        List<ConversionReportDTO> conversionReportDTOS = new ArrayList<>();
//        List<SearchHit[]> map = reportQueryBuilder.conversionExportDataList(startDate, endDate, pubArr, offerArr, conversionArr);
//
//        *//*List<SearchHit[]> conversionList = (List<SearchHit[]>) map.get("conversionList");
//        Long count = (Long) map.get("count");
//        for (Conversion conversion : conversionList) {
//            Click click = null;
//            if (conversion.getClickId() != null)
//                click = clickQueryBuilder.findById(conversion.getClickId());
//            conversionReportDTOS.add(new ConversionReportDTO(conversion, click));
//        }*//*
//        Map<String, Object> res = new HashMap<String, Object>();
//
//        res.put("data", conversionReportDTOS);
//        return res;
//    }*/
//
///*    public List<SearchHit[]> getClickExportData(String startDate, String endDate, List<String> offerArr, List<String> pubArr,
//                                                       List<String> advArr, String uuid) throws IOException {
//        List<ClickReportDTO> clickReportDTOS = new ArrayList<>();
//        List<SearchHit[]> clickList = reportQueryBuilder.clickExportDataList(startDate, endDate, pubArr, offerArr, advArr, uuid);
//      *//*  for (Click click : clickList) {
//            OfferTrackingDTO offerTrackingDTO = null;
//            PublisherTrackingDTO publisherTrackingDTO = null;
//            try {
//                if (click.getOfferId() != null) {
//                    offerTrackingDTO = cacheService.offerCache(click.getOfferId());
//                }
//                if (click.getPubId() != null) {
//                    publisherTrackingDTO = cacheService.pubCache(click.getPubId());
//                }
//                clickReportDTOS.add(new ClickReportDTO(click, offerTrackingDTO, publisherTrackingDTO));
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }*//*
//        return clickList;
//    }*/
//}
