//package com.adstacks.attribution.reports.controller;
//
//import com.etreetech.panel.beans.Click;
//import com.etreetech.panel.beans.Conversion;
//import com.etreetech.panel.cache.dto.AdvertiserTrackingDTO;
//import com.etreetech.panel.cache.dto.OfferTrackingDTO;
//import com.etreetech.panel.cache.dto.PublisherTrackingDTO;
//import com.etreetech.panel.cache.service.CacheService;
//import com.etreetech.panel.dao.ClickQueryBuilder;
//import com.etreetech.panel.reports.dao.ReportQueryBuilder;
//import com.etreetech.panel.reports.dto.AllReportDTO;
//import com.etreetech.panel.reports.dto.ClickDetailsDTO;
//import com.etreetech.panel.reports.dto.ConversionDetailsDTO;
//import com.etreetech.panel.reports.dto.ReportFilterDTO;
//import com.etreetech.panel.reports.service.ReportServiceeee;
//import com.etreetech.panel.repository.ClickRepository;
//import com.etreetech.panel.repository.ConversionRepository;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.Arrays;
//import java.util.List;
//import java.util.Map;
//import java.util.Set;
//import java.util.stream.Collectors;
//
//@RestController
//@RequestMapping(value = "/reports")
//@Slf4j
//public class ReportControllerrr {
//    @Autowired
//    ReportServiceeee reportService;
//    @Autowired
//    ClickQueryBuilder clickQueryBuilder;
//    @Autowired
//    CacheService cacheService;
//    @Autowired
//    ConversionRepository conversionRepository;
//    @Autowired
//    ReportQueryBuilder reportQueryBuilder;
//    @Autowired
//    ClickRepository clickRepository;
//
//    @PostMapping(path = "/conversionReport", consumes = "application/json", produces = "application/json")
//    public ResponseEntity<?> getConversionReport(@RequestBody DataTablesInput input) {
//        Map<String, Object> res = reportService.reportConversionAjax(input);
//        return ResponseEntity.ok().body(res);
//    }
//
//    @PostMapping(path = "/clickReport", consumes = "application/json", produces = "application/json")
//    public ResponseEntity<?> getClickReport(@RequestBody DataTablesInput input) {
//        Map<String, Object> res = reportService.reportClickAjax(input);
//        return ResponseEntity.ok().body(res);
//    }
//
//    @PostMapping(path = "/allReport")
//    public Set<AllReportDTO> getAllReport(@RequestBody ReportFilterDTO reportFilterDTO) {
//        Set<AllReportDTO> res = reportService.reportAllAjax(reportFilterDTO);
//        return res;
//    }
//
//    @GetMapping("/getClickLogsDetails")
//    public ClickDetailsDTO getClickLogsDetails(@RequestParam("id") String id) {
//        Click click = clickQueryBuilder.findById(id);
//        if (click == null) {
//            return new ClickDetailsDTO();
//        }
//        PublisherTrackingDTO publisherTrackingDTO = cacheService.getPublisher(click.getPubId());
//        OfferTrackingDTO offerTrackingDTO = cacheService.getOffer(click.getOfferId());
//        AdvertiserTrackingDTO advertiserTrackingDTO = cacheService.getAdvertiser(offerTrackingDTO.getAdvertiserId());
//        ClickDetailsDTO clickDetailsDTO = new ClickDetailsDTO(click, offerTrackingDTO, advertiserTrackingDTO, publisherTrackingDTO);
//        return clickDetailsDTO;
//    }
//
//    @GetMapping("/getConversionDetails")
//    public ConversionDetailsDTO getConversionDetails(@RequestParam("id") String id) {
//        Conversion conversion = conversionRepository.findByUuid(id);
//        if (conversion == null) {
//            return new ConversionDetailsDTO();
//        }
//        Click click = clickQueryBuilder.findById(conversion.getClickId());
//        OfferTrackingDTO offerTrackingDTO = cacheService.getOffer(click.getOfferId());
//        AdvertiserTrackingDTO advertiserTrackingDTO = cacheService.getAdvertiser(offerTrackingDTO.getAdvertiserId());
//        ConversionDetailsDTO conversionDetailsDTO = new ConversionDetailsDTO(conversion, click, advertiserTrackingDTO);
//        return conversionDetailsDTO;
//    }
//
//    //To be completed
//    @GetMapping("/getExportClickList")
//    public List<Click> getExportClickList(String daterange, String offerId, String pubId) {
//        Map map = reportService.getDates(daterange);
//        String startDate = (String) map.get("startDate");
//        String endDate = (String) map.get("endDate");
//        List<String> offerIds = Arrays.stream(offerId.split(",")).collect(Collectors.toList());
//        List<String> pubIds = Arrays.stream(pubId.split(",")).collect(Collectors.toList());
//        List<Click> clickList = reportQueryBuilder.getCLickList(startDate, endDate, pubIds, offerIds);
//        return clickList;
//    }
//
//    /*Export data functionality of conversion report*/
//    /*@RequestMapping(value = "/findConversionData", method = RequestMethod.GET)
//    public Map<String, Object> findByConversionData(@RequestParam(value = "date", defaultValue = "", required = false) String date,
//                                                    @RequestParam(value = "offerId", defaultValue = "", required = false) String offerId,
//                                                    @RequestParam(value = "pubId", defaultValue = "", required = false) String pubId,
//                                                    @RequestParam(value = "conversionStatus", defaultValue = "", required = false) String conversionStatus) throws IOException {
//
//        List<String> offerIds = Arrays.asList(offerId.split(","));
//        List<String> pubIds = Arrays.asList(pubId.split(","));
//        List<String> conversionsStatus = Arrays.asList(conversionStatus.split(","));
//
//        offerIds = offerIds.stream().filter(x -> (!x.equals(""))).collect(Collectors.toList());
//        pubIds = pubIds.stream().filter(x -> (!x.equals(""))).collect(Collectors.toList());
//        conversionsStatus = conversionsStatus.stream().filter(x -> (!x.equals(""))).collect(Collectors.toList());
//
//        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
//        SimpleDateFormat formatterForJsDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        String startDate = null;
//        String endDate = null;
//        if (date.equals("")) {
//            endDate = formatter.format(new Date());
//            Calendar cal = Calendar.getInstance();
//            cal.add(Calendar.DATE, -7);
//            startDate = formatter.format(cal.getTime());
//        } else {
//            String[] startDateArr = date.split(" - ");
//            try {
//                startDate = formatter.format(formatterForJsDate.parse(startDateArr[0]));
//                endDate = formatter.format(formatterForJsDate.parse(startDateArr[1]));
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//        Map<String, Object> map = reportService.getConversionExportData(startDate, endDate, offerIds, pubIds, conversionsStatus);
//        return map;
//    }*/
//
//}
