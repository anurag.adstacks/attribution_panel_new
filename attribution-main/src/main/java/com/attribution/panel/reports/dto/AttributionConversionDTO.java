package com.attribution.panel.reports.dto;

import com.attribution.panel.enums.ConversionStatus;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.UUID;


@Data
@NoArgsConstructor
public class AttributionConversionDTO implements Serializable {

//    @Id
    private String id;

    private Long appId;
    private String appName;

    private Long partnerId;
    private String partnerName;

    private String clickId;

    private String partnerPostbackResponse;
    private Integer partnerPostbackResponseCode;

    private Long eventID;
    private String eventName;

//    @Field(type = FieldType.Date, format = DateFormat.date_optional_time)
    private String creationDateTime;

//    @Field(type = FieldType.Date, format = DateFormat.date_optional_time)
    private String updateDateTime;

    //@Field(type = FieldType.Text,fielddata = true)

    private ConversionStatus conversionStatus;

//    private long pId;

    private String subPid;

    private String debug;

    private String conversionMessage;

//    private String uuid = UUID.randomUUID().toString().replace("-", "");

    private String partnerPostback;

    public AttributionConversionDTO(AttributionConversionDTO conversion, AttributionClickDTO click) {

        this.appId = conversion.getAppId();
        this.appName = conversion.getAppName();
        this.clickId = conversion.getClickId();
        this.creationDateTime = conversion.getCreationDateTime();
        this.conversionStatus = conversion.getConversionStatus();
        this.partnerId =conversion.getPartnerId();
        this.partnerName = conversion.getPartnerName();
        this.id = conversion.getId();
    }

    public void setConversionDetails(ConversionStatus conversionStatus, String conversionMessage) {
        this.conversionStatus = conversionStatus;
        this.conversionMessage = conversionMessage;
    }


    public AttributionConversionDTO(String debug, String creationDateTime, String updateDateTime, AttributionClickDTO attributionclick) {

        this.id = UUID.randomUUID().toString().replace("-", "");
        this.debug = debug;
        this.creationDateTime = String.valueOf(creationDateTime);
        this.updateDateTime = String.valueOf(updateDateTime);
        this.clickId = attributionclick.getId();

    }

}
