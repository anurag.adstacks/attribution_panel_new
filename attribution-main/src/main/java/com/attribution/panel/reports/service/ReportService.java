package com.attribution.panel.reports.service;


import com.attribution.panel.bean.*;
import com.attribution.panel.console.dto.AllReportDTO;
import com.attribution.panel.console.dto.ReportFilterDTO;
import com.attribution.panel.console.service.AppService;
import com.attribution.panel.console.service.CacheService;
import com.attribution.panel.console.service.PartnerService;
import com.attribution.panel.elasticsearchRepo.ConversionRepo;
import com.attribution.panel.reports.dao.ClickQueryBuilder;
import com.attribution.panel.reports.dao.ReportQueryBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.datatables.mapping.Column;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.Order;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.Arrays.stream;

@Service
@Slf4j
public class ReportService {

    @Autowired
    ReportQueryBuilder reportQueryBuilder;

    @Autowired
    ClickQueryBuilder clickQueryBuilder;

    @Autowired
    ConversionRepo conversionRepo;

    @Autowired
    AppService appService;

    @Autowired
    PartnerService partnerService;

    @Autowired
    CacheService cacheService;

    public Map<String, Object> reportConversionAjax(DataTablesInput input) {
        System.out.println("reportConversionAjax " + input);
        Map<String, String> map = getDates(input, "creationDateTime");
        String startDate = map.get("startDate");
        String endDate = map.get("endDate");
        List<String> offerArr = getOfferArray(input, "appId");
        List<String> pubArr = getPublisherArray(input, "partnerId");
        List<String> conversionArr = getConversionStatusArray(input, "conversionStatus");
        Pageable pageable = getPageableConversion(input, "conversion");
        System.out.println("getPageableConversionB " + pageable);
        System.out.println("reportConversionAjax2 " + map);
//        log.info("Conversion array:" + conversionArr);
//        log.info("Pub array:" + pubArr);
//        log.info("Offer array:" + offerArr);
//        log.info("Start Date :" + startDate + " End Date:" + endDate);
        Map<String, Object> res = getConversionReport(offerArr, pubArr, startDate, endDate, pageable, conversionArr, input);
        System.out.println("reportConversionAjax3 " + res);
        res.put("draw", input.getDraw());
        res.put("error", "");
        return res;
    }

    public Map<String, Object> reportClickAjax(DataTablesInput input) {
        System.out.println("reportClickAjax0 " + input);
        Map<String, String> map = getDates(input, "dayTimestamp");
        String startDate = map.get("startDate");
        String endDate = map.get("endDate");
        System.out.println("reportClickAjax1 " + input);
//        List<String> offerArr = getOfferArray(input, "appIds");
        List<String> offerArr = new ArrayList<>();
//        List<String> pubArr = getPublisherArray(input, "pIds");
        List<String> pubArr = new ArrayList<>();
        System.out.println("reportClickAjax2 ="+offerArr + " " + pubArr + map);
        Column uuidSearch = input.getColumn("id");
        String uuid = uuidSearch.getSearch().getValue();
        Pageable pageable = getPageableClick(input, "click");
//        log.info("Pub array:" + pubArr);
//        log.info("Offer array:" + offerArr);
//        log.info("ClickId:" + uuid);
//        log.info("Start Date :" + startDate + " End Date:" + endDate);
        Map<String, Object> res = getClickReport(offerArr, pubArr, startDate, endDate, pageable, uuid, input);
        res.put("draw", input.getDraw());
        res.put("error", "");
        return res;
    }

    public Map<String, Object> reportAllReportAjax(Long appId, DataTablesInput input) {

        System.out.println("reportAllReportAjax " + input + appId);
        Map<String, String> map = getDates(input, "dayTimestamp");
        String startDate = map.get("startDate");
        String endDate = map.get("endDate");
        System.out.println("reportAllReportAjax0 " + startDate + endDate);
        List<String> offerArr = getOfferArray(input, "attributedTouchType");
        System.out.println("attributedTouchTypeSelect " + offerArr);
        List<String> pubArr = getPublisherArray(input, "partnerId");
//        List<String> conversionArr = getConversionStatusArray(input, "conversionStatus");
        Pageable pageable = getPageableConversion(input, "attribution");
        System.out.println("reportAllReportAjax1 " + pageable);
        System.out.println("reportAllReportAjax2 " + map);

//        List<AllReportDTO> list = null;
//        list = reportService.reportAllAjax(new ReportFilterDTO());
//        System.out.println("Inside getAll report: " + list.size() + " " + list
//        );

//        log.info("Conversion array:" + conversionArr);
//        log.info("Pub array:" + pubArr);
//        log.info("Offer array:" + offerArr);
//        log.info("Start Date :" + startDate + " End Date:" + endDate);
        Map<String, Object> res = getAllReportAjax(appId, offerArr, pubArr, startDate, endDate, pageable, input);
        System.out.println("reportAllReportAjax3 " + res.size());
        res.put("draw", input.getDraw());
        res.put("error", "");
        return res;

    }

    private Map<String, Object> getAllReportAjax(Long appIds, List<String> offerArr, List<String> pubArr, String startDate, String endDate, Pageable pageable, DataTablesInput input) {

        System.out.println("getConversionReportInside ");
        List<AttributionAllReport> attributionData = new ArrayList<>();
        Map<String, Object> map = reportQueryBuilder.allReportAjax(appIds, startDate, endDate, pubArr, offerArr, pageable, input);
        System.out.println("reportQueryBuilder.conversionReport " + map.size());
        List<AttributionAllReport> attributionList = (List<AttributionAllReport>) map.get("attributionList");
        Long count = (Long) map.get("count");
        System.out.println("reportQueryBuilder.conversionReport2 " + attributionList.size());

        for (AttributionAllReport attribution : attributionList) {
            AttributionClick click = null;

            if (attribution.getId() != null) {
                try {
//                    click = clickQueryBuilder.findById(conversion.getClickId());
                    System.out.println("reportQueryBuilder.conversionReport1 " + click);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            attributionData.add(new AttributionAllReport(attribution, click));
            System.out.println("reportQueryBuilder.conversionReport2 " + attributionData.size());
        }
        System.out.println("reportQueryBuilder.conversionReport3 " + attributionData.size());
        Map<String, Object> res = new HashMap<String, Object>();
        res.put("recordsFiltered", count);
        res.put("recordsTotal", conversionRepo.count());
        res.put("data", attributionData);
        return res;

    }


    public Map<String, Object> getConversionReport(List<String> offerArr, List<String> pubArr, String startDate, String endDate, Pageable pageable,
                                                   List<String> conversionArr, DataTablesInput input) {
        System.out.println("getConversionReportInside ");
        List<AttributionConversion> conversionReportDTOS = new ArrayList<>();
        Map<String, Object> map = reportQueryBuilder.conversionReport(startDate, endDate, pubArr, offerArr, conversionArr, pageable, input);
        System.out.println("reportQueryBuilder.conversionReport " + map);
        List<AttributionConversion> conversionList = (List<AttributionConversion>) map.get("conversionList");
        Long count = (Long) map.get("count");

        for (AttributionConversion conversion : conversionList) {
            AttributionClick click = null;

            if (conversion.getClickId() != null) {
                try {
//                    click = clickQueryBuilder.findById(conversion.getClickId());
                    System.out.println("reportQueryBuilder.conversionReport1 " + click);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            conversionReportDTOS.add(new AttributionConversion(conversion, click));
            System.out.println("reportQueryBuilder.conversionReport2 " + conversionReportDTOS.size());
        }
        System.out.println("reportQueryBuilder.conversionReport3 " + conversionReportDTOS);
        Map<String, Object> res = new HashMap<String, Object>();
        res.put("recordsFiltered", count);
        res.put("recordsTotal", conversionRepo.count());
        res.put("data", conversionReportDTOS);
        return res;
    }

    public Map<String, Object> getClickReport(List<String> offerArr, List<String> pubArr, String startDate, String endDate, Pageable pageable, String uuid, DataTablesInput input) {
        List<AttributionClick> clickReportDTOS = new ArrayList<>();
        Map<String, Object> map = reportQueryBuilder.clickReport(startDate, endDate, pubArr, offerArr, pageable, uuid, input);
        System.out.println("getClickReport " + map);
        List<AttributionClick> clickList = (List<AttributionClick>) map.get("clickList");
        Long count = (Long) map.get("count");
        for (AttributionClick click : clickList) {
            App offerTrackingDTO = null;
            Partner publisherTrackingDTO = null;
            try {
                if (click.getAppID() != null) {
                    offerTrackingDTO = appService.findById(click.getAppID());
                    System.out.println("getClickReport2 " + offerTrackingDTO);
//                    offerTrackingDTO = cacheService.getOffer(click.getAppID());
                }
                if (click.getPId() != null) {
                    publisherTrackingDTO = partnerService.findById(click.getPId());
//                    publisherTrackingDTO = cacheService.getPublisher(click.getPId());
                    System.out.println("getClickReport2 " + offerTrackingDTO);
                }

                clickReportDTOS.add(new AttributionClick(click, offerTrackingDTO, publisherTrackingDTO));
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        System.out.println("clickReportDTOS " +clickReportDTOS);
        Map<String, Object> res = new HashMap<String, Object>();
        res.put("recordsFiltered", count);
        res.put("recordsTotal", clickQueryBuilder.count());
        res.put("data", clickReportDTOS);
        return res;
    }

    public Map<String, String> getDates(DataTablesInput input, String column) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        SimpleDateFormat formatterForJsDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Column dateRange = input.getColumn(column);
        String dateFromUI = "";
        if (dateRange != null) {
            dateFromUI = dateRange.getSearch().getValue();
        }
        String startDate = null;
        String endDate = null;

        System.out.println("dateFromUI " + dateRange + " " + dateFromUI);

        if (dateFromUI.equals("")) {
            endDate = formatter.format(new Date());
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, -7);
            startDate = formatter.format(cal.getTime());
            System.out.println("startDate " + startDate + " " + endDate);
        } else {
            System.out.println("startDate " + startDate + " " + endDate);
            String[] startDateArr = dateFromUI.split(" - ");
            try {
                startDate = formatter.format(formatterForJsDate.parse(startDateArr[0]));
                endDate = formatter.format(formatterForJsDate.parse(startDateArr[1]));
                System.out.println("startDate2 " + startDate + " " + endDate);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Map<String, String> mp = new HashMap<>();
        mp.put("startDate", startDate);
        mp.put("endDate", endDate);
        return mp;
    }

    public List<String> getOfferArray(DataTablesInput input, String column) {
        System.out.println("getOfferArray " + input + " " + column);
        Column offer = input.getColumn(column);
        List<String> offerArray = new ArrayList<>();
        System.out.println("getOfferArray3 " + offer);
        String offerId = "";
        if (!offer.getSearch().getValue().equals("")) {
            System.out.println("getOfferArray1 " + offer.getSearch().getValue());
            offerId = offer.getSearch().getValue();
            offerArray = stream(offerId.split(",")).collect(Collectors.toList());
        }
        System.out.println("getOfferArray2 " + offerArray);
        return offerArray;
    }

    public List<String> getPublisherArray(DataTablesInput input, String column) {
        System.out.println("getPublisherArray " + input + " " + column);
        Column publisher = input.getColumn(column);
        List<String> pubArray = new ArrayList<>();
        /*String pubId = "";
        if (!publisher.getSearch().getValue().equals("")) {
            System.out.println("getPublisherArray2 " + publisher.getSearch().getValue());
            pubId = publisher.getSearch().getValue();
            pubArray = stream(pubId.split(",")).collect(Collectors.toList());
        }
        System.out.println("getPublisherArray3 " + pubArray);*/
        return pubArray;
    }

    public List<String> getConversionStatusArray(DataTablesInput input, String column) {
        System.out.println("getConversionStatusArray " + input);
        Column conversionStatus = input.getColumn(column);
        List<String> conversionArray = new ArrayList<>();
        String conv = "";
        if (!conversionStatus.getSearch().getValue().equals("")) {
            System.out.println("getConversionStatusArray2 " + input);
            conv = conversionStatus.getSearch().getValue();
            conversionArray = stream(conv.split(",")).collect(Collectors.toList());
            conversionArray = conversionArray.stream().collect(Collectors.toList());
        }
        System.out.println("getConversionStatusArray3 " + input);
        return conversionArray;
    }

    public Pageable getPageableConversion(DataTablesInput input, String reportType) {
        System.out.println("getPageableConversion " + input + " " + reportType);
        Map<String, String> orderMap = new HashMap<String, String>();
        Order order = input.getOrder().get(0);
        orderMap.put("orderColumnName", input.getColumns().get(order.getColumn()).getData());
        orderMap.put("orderType", order.getDir());
        int first = input.getStart();
        int len = input.getLength();
        int page = (int) (Math.ceil(first + 1) / len);
        Pageable pageable;
        String orderValue = orderMap.get("orderType");
        if (orderValue.equals("desc")) {
            System.out.println("getPageableConversion2 ");
            switch (orderMap.get("orderColumnName")) {
                case "appId":
                    pageable = PageRequest.of(page, len, Sort.by("appId").descending());
                    break;
                case "partnerId":
                    pageable = PageRequest.of(page, len, Sort.by("partnerId").descending());
                    break;
                default:
                    pageable = PageRequest.of(page, len, Sort.by(orderMap.get("orderColumnName")).descending());
                    break;
            }
        } else {
            System.out.println("getPageableConversion3 ");
            switch (orderMap.get("orderColumnName")) {
                case "appId":
                    pageable = PageRequest.of(page, len, Sort.by("appId").ascending());
                    break;
                case "partnerId":
                    pageable = PageRequest.of(page, len, Sort.by("partnerId").ascending());
                    break;
                default:
                    pageable = PageRequest.of(page, len, Sort.by(orderMap.get("orderColumnName")).ascending());
                    break;
            }
        }
        System.out.println("getPageableConversion4 " + pageable);
        return pageable;
    }

    public Pageable getPageableClick(DataTablesInput input, String reportType) {
        System.out.println("getPageableClick " + input);
        Map<String, String> orderMap = new HashMap<String, String>();
        Order order = input.getOrder().get(0);
        orderMap.put("orderColumnName", input.getColumns().get(order.getColumn()).getData());
        orderMap.put("orderType", order.getDir());
        int first = input.getStart();
        int len = input.getLength();
        int page = (int) (Math.ceil(first + 1) / len);
        Pageable pageable;
        String orderValue = orderMap.get("orderType");
        if (orderValue.equals("desc")) {
            System.out.println("getPageableClick1 ");
            switch (orderMap.get("orderColumnName")) {
                case "appID":
                    pageable = PageRequest.of(page, len, Sort.by("appID").descending());
                    break;
                case "pId":
                    pageable = PageRequest.of(page, len, Sort.by("pId").descending());
                    break;
                default:
                    pageable = PageRequest.of(page, len, Sort.by(orderMap.get("orderColumnName")).descending());
                    break;
            }
        } else {
            System.out.println("getPageableClick2 " );
            switch (orderMap.get("orderColumnName")) {
                case "appID":
                    pageable = PageRequest.of(page, len, Sort.by("appID").ascending());
                    break;
                case "pId":
                    pageable = PageRequest.of(page, len, Sort.by("pId").ascending());
                    break;
                default:
                    pageable = PageRequest.of(page, len, Sort.by(orderMap.get("orderColumnName")).ascending());
                    break;
            }
        }
        System.out.println("getPageableClick3 " + pageable);
        return pageable;
    }


    public List<AllReportDTO> reportAllAjax(ReportFilterDTO reportFilterDTO, Long appId ) {
        System.out.println("reportFilterDTOSamiksha  "+reportFilterDTO);
//        Map<String, String> map = getDates((reportFilterDTO.getDaterange() == null) ? " " : reportFilterDTO.getDaterange().toString());
//        System.out.println("reportAllAjax0"+ map);
//        String startDate = map.get("startDate");
        String startDate = "";
//        String endDate = map.get("endDate");
        String endDate = "";
//        List<String> offerArr = reportFilterDTO.getOffers();
//        List<String> pubArr = reportFilterDTO.getPublishers();
        List<String> pubArr = reportFilterDTO.getPartners();
        System.out.println("reportAllAjax2 "+ pubArr);
//        List<String> advArr = reportFilterDTO.getAdvertisers();

//        String subAffiliate = reportFilterDTO.getSubAffiliate();

        List<String> groupByList = getGroupByList(reportFilterDTO.getGroupBy());
        System.out.println("reportAllAjax3 "+ groupByList);

        List<AllReportDTO> allReportDTOSet = null;

        try {
            allReportDTOSet = reportQueryBuilder.allReportCHR(startDate, endDate, pubArr, groupByList, appId);
            System.out.println("reportAllAjax4 "+ allReportDTOSet);

            allReportDTOSet = populateFieldsInList(allReportDTOSet, groupByList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return allReportDTOSet;
    }

    public List<AllReportDTO> reportAllFilterAjax(String daterange, Long appId, List<String> partners, List<String> groupBy, List<String> columns ) {
        System.out.println("reportFilterDTOSamiksha  "+ daterange + appId + partners + groupBy + columns);
//        Map<String, String> map = getDates((reportFilterDTO.getDaterange() == null) ? " " : reportFilterDTO.getDaterange().toString());
//        System.out.println("reportAllAjax0"+ map);
//        String startDate = map.get("startDate");
        String startDate = "";
//        String endDate = map.get("endDate");
        String endDate = "";
//        List<String> offerArr = reportFilterDTO.getOffers();
//        List<String> pubArr = reportFilterDTO.getPublishers();
//        List<String> pubArr = reportFilterDTO.getPartners();
//        System.out.println("reportAllAjax2 "+ pubArr);
//        List<String> advArr = reportFilterDTO.getAdvertisers();

//        String subAffiliate = reportFilterDTO.getSubAffiliate();

        List<String> groupByList = getGroupByList(groupBy);
        System.out.println("reportAllAjax3 "+ groupByList);
        List<AllReportDTO> allReportDTOSet = null;
        try {
            allReportDTOSet = reportQueryBuilder.allReportCHR(startDate, endDate, partners, groupByList, appId);
            System.out.println("reportAllAjax4 "+ allReportDTOSet);

            allReportDTOSet = populateFieldsInList(allReportDTOSet, groupByList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return allReportDTOSet;
    }

    public List<AllReportDTO> populateFieldsInList(List<AllReportDTO> allReportDTOSet, List<String> groupByList) throws Exception {
        System.out.println("populateFieldsInList " + groupByList);
        for (AllReportDTO allReportDTO : allReportDTOSet) {
            for (String group : groupByList) {

                switch (group) {
                    case "App":
                        App app = cacheService.getOffer(allReportDTO.getAppId());
                        System.out.println("populateFieldsInList1 " + app.getName());
                        allReportDTO.setAppName(app.getName());
                        break;
                    case "Partner":
                        allReportDTO.setPartnerName(cacheService.getPublisher(allReportDTO.getPartnerId()).getName());
                        System.out.println("populateFieldsInList2 " + cacheService.getPublisher(allReportDTO.getPartnerId()).getName());
                        break;
                }

            }
        }
        return allReportDTOSet;
    }


//    public  List<AllReportDTO> reportAllAjax(ReportFilterDTO reportFilterDTO) {
//        System.out.println("reportFilterDTOSamiksha"+reportFilterDTO);
//        List<String> offerArrY =  reportFilterDTO.getOffers();
//        System.out.println("ALLiswell"+offerArrY);
//        List<String> pubArr =  reportFilterDTO.getPublishers();
//        System.out.println("ALLiswell11"+pubArr);
//        List<String> smartArr = reportFilterDTO.getSmartLinks();
//        System.out.println("ALLiswell22"+smartArr);
////        String subAffiliate = reportFilterDTO.getSubAffiliate();
//        List<String> groupByList = getGroupByList(reportFilterDTO.getGroupBy());
//        System.out.println("ALLiswell22"+groupByList);
//        List<AllReportDTO> allReportDTOSet = null;
//        try {
//            allReportDTOSet = reportQueryBuilder.allReportCHR(pubArr, offerArrY, smartArr, groupByList);
//            System.out.println("ALLiswell22456678" +allReportDTOSet);
//
//
////            allReportDTOSet = populateFieldsInList(allReportDTOSet, groupByList);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return allReportDTOSet;
//    }


    public Map<String, String> getDates(String dateFromUI) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        SimpleDateFormat formatterForJsDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String startDate = null;
        String endDate = null;
        if (dateFromUI.equals("")) {
            endDate = formatter.format(new Date());
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, -7);
            startDate = formatter.format(cal.getTime());
        } else {
            String[] startDateArr = dateFromUI.split(" - ");
            try {
                startDate = formatter.format(formatterForJsDate.parse(startDateArr[0]));
                endDate = formatter.format(formatterForJsDate.parse(startDateArr[1]));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Map<String, String> mp = new HashMap<>();
        mp.put("startDate", startDate);
        mp.put("endDate", endDate);
        return mp;
    }

    public List<String> getGroupByList(List<String> groupBy) {
        List<String> groupByList = new ArrayList<>();
        if (groupBy == null) {
            groupByList.add("App");
        } else {
            return groupBy;
        }
        return groupByList;
    }



    public void modifyFilterReportDTO(ReportFilterDTO reportFilterDTO) {

//        List<String> appList = reportFilterDTO.getApps();
//        if(appList!=null && !appList.isEmpty()) {
//            System.out.println("modifyFilterReportDTO " + appList);
//            List<App> appName = new ArrayList<>();
//            for (String app : appList) {
//                Long id= Long.parseLong(app);
//                App app1 = appService.findById(id);
//                appName.add(app1);
//
////                appName.add(restTemplate.getForObject(backendConstants.baseUrlDbAccess +"/adminConsole/findByOfferId/{appId}",Offer.class, id));
//            }
//            System.out.println("modifyFilterReportDTO2 " + appName);
//            reportFilterDTO.setAppName(appName);
//        }

        List<String> partnerList = reportFilterDTO.getPartners();
        if(partnerList!=null && !partnerList.isEmpty()) {
            System.out.println("modifyFilterReportDTO3 " + partnerList);
            List<Partner> partnerName = new ArrayList<>();
            for (String partner : partnerList) {
                Long partnerId=Long.parseLong(partner);
                Partner partner1 = partnerService.findById(partnerId);
                partnerName.add(partner1);
//                partnerName.add(restTemplate.getForObject(backendConstants.baseUrlDbAccess +"/adminConsole/findByPubId/{partnerId}",Pub.class, partnerId));
            }
            System.out.println("modifyFilterReportDTO3 " + partnerName);
            reportFilterDTO.setPartnerName(partnerName);
        }

    }



//
//    public void modifyFilterReportDTO(ReportFilterDTO reportFilterDTO) {
////        System.out.println("WELCOME"+reportFilterDTO);
//        List<String> smartLinkList = reportFilterDTO.getSmartLinks();
////        System.out.println("smartLinkLists="+smartLinkList);
//        if(smartLinkList!=null && !smartLinkList.isEmpty()) {
//            List<SmartLink> smartLinkName = new ArrayList<>();
////            System.out.println("smartLinkName"+smartLinkName);
//            for (String smart : smartLinkList) {
//                Long id=Long.parseLong(smart.trim());
//                SmartLink smartLink=smartLinkService.findBySmartLinkId(id);
////                System.out.print("smartlinkTassddftggh"+smartLink);
////                SmartLink smartLink = restTemplate.getForObject(backendConstants.baseUrlDbAccess +"/adminConsole/findByAdvertiserId/{advId}",Advertiser.class, id);
//                smartLinkName.add(smartLink);
////                System.out.print("smartlinkTassddftggh "+smartLinkName.add(smartLink));
//            }
//            reportFilterDTO.setSmartLinkName(smartLinkName);
//        }
////
////        List<String> advertiserList = reportFilterDTO.getAdvertisers();
////        if(advertiserList!=null && !advertiserList.isEmpty()) {
////            List<Advertiser> advertiserName = new ArrayList<>();
////            for (String adv : advertiserList) {
////                Long id=Long.parseLong(adv.trim());
////                Advertiser advertiser = restTemplate.getForObject(backendConstants.baseUrlDbAccess +"/adminConsole/findByAdvertiserId/{advId}",Advertiser.class, id);
////                advertiserName.add(advertiser);
////            }
////            reportFilterDTO.setAdvertiserName(advertiserName);
////        }
//        List<String> offerList = reportFilterDTO.getOffers();
//        System.out.print("oferrsssAre "+offerList);
//        if(offerList!=null && !offerList.isEmpty()) {
//            List<Offer> offerName = new ArrayList<>();
//            for (String offer : offerList) {
//                Long offerId= Long.parseLong(offer);
//                Offer offers=offerService.findByOfferId(offerId);
//                System.out.println("OffersName"+ offers);
//                offerName.add(offers);
//                System.out.print("smartlinkTassddftggh "+ offerName.add(offers));
////                offerName.add(restTemplate.getForObject(backendConstants.baseUrlDbAccess +"/adminConsole/findByOfferId/{offerId}",Offer.class, id));
//            }
//            reportFilterDTO.setOfferName(offerName);
//        }
//
//        List<String> pubList = reportFilterDTO.getPublishers();
//        System.out.println("Publisherpublisher"+ pubList);
//        if(pubList!=null && !pubList.isEmpty()) {
//            List<Publisher> publisherName = new ArrayList<>();
//            for (String pub : pubList) {
//                Long pubId=Long.parseLong(pub);
//                Publisher publisher=publisherService.findById(pubId);
//                System.out.println("Publisherpublisher"+ publisher);
////                SmartLink smartLink = restTemplate.getForObject(backendConstants.baseUrlDbAccess +"/adminConsole/findByAdvertiserId/{advId}",Advertiser.class, id);
//                publisherName.add(publisher);
//                System.out.print("publishersNamesdfgg "+publisherName.add(publisher));
////                publisherName.add(restTemplate.getForObject(backendConstants.baseUrlDbAccess +"/adminConsole/findByPublisherId/{pubId}",Publisher.class, pubId));
//            }
//            reportFilterDTO.setPublisherName(publisherName);
//        }
//    }



//    public Pageable getPageableConversion(DataTablesInput input, String reportType) {
//        System.out.println("getPageableConversion " + input + " " + reportType);
//        Map<String, String> orderMap = new HashMap<String, String>();
//        Order order = input.getOrder().get(0);
//        orderMap.put("orderColumnName", input.getColumns().get(order.getColumn()).getData());
//        orderMap.put("orderType", order.getDir());
//        int first = input.getStart();
//        int len = input.getLength();
//        int page = (int) (Math.ceil(first + 1) / len);
//        Pageable pageable;
//        String orderValue = orderMap.get("orderType");
//        if (orderValue.equals("desc")) {
//            System.out.println("getPageableConversion2 ");
//            switch (orderMap.get("orderColumnName")) {
//                case "appId":
//                    pageable = PageRequest.of(page, len, Sort.by("app.appId").descending());
//                    break;
//                case "partnerId":
//                    pageable = PageRequest.of(page, len, Sort.by("Partner.partnerId").descending());
//                    break;
//                default:
//                    pageable = PageRequest.of(page, len, Sort.by(orderMap.get("orderColumnName")).descending());
//                    break;
//            }
//        } else {
//            System.out.println("getPageableConversion3 ");
//            switch (orderMap.get("orderColumnName")) {
//                case "appId":
//                    pageable = PageRequest.of(page, len, Sort.by("app.appId").ascending());
//                    break;
//                case "partnerId":
//                    pageable = PageRequest.of(page, len, Sort.by("Partner.partnerId").ascending());
//                    break;
//                default:
//                    pageable = PageRequest.of(page, len, Sort.by(orderMap.get("orderColumnName")).ascending());
//                    break;
//            }
//        }
//        System.out.println("getPageableConversion4 " + pageable);
//        return pageable;
//    }


}
