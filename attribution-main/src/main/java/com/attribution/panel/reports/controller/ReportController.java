package com.attribution.panel.reports.controller;


import com.attribution.panel.console.service.AppService;
import com.attribution.panel.console.service.ClickService;
import com.attribution.panel.console.service.ConversionsService;
import com.attribution.panel.console.service.PartnerService;
import com.attribution.panel.reports.service.ReportService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/reports")
public class ReportController {


    @Autowired
    ConversionsService conversionsService;

    @Autowired
    AppService appService;

    @Autowired
    PartnerService partnerService;

    @Autowired
    ClickService clickService;

    @Autowired
    ReportService reportService;


//    http://localhost:9099/reports/allReport?appId=1&input=DataTablesInput(draw=1,%20start=0,%20length=10,%20search=Search(value=,%20regex=false),%20order=[Order(column=0,%20dir=asc)],%20columns=[Column(data=appVersion,%20name=,%20searchable=true,%20orderable=true,%20search=Search(value=,%20regex=false)),%20Column(data=sdkVersion,%20name=,%20searchable=true,%20orderable=true,%20search=Search(value=,%20regex=false)),%20Column(data=appId,%20name=,%20searchable=true,%20orderable=true,%20search=Search(value=,%20regex=false)),%20Column(data=appName,%20name=,%20searchable=true,%20orderable=true,%20search=Search(value=,%20regex=false)),%20Column(data=appType,%20name=,%20searchable=true,%20orderable=true,%20search=Search(value=,%20regex=false)),%20Column(data=bundleId,%20name=,%20searchable=true,%20orderable=true,%20search=Search(value=,%20regex=false)),%20Column(data=eventTime,%20name=,%20searchable=true,%20orderable=true,%20search=Search(value=,%20regex=false)),%20Column(data=eventName,%20name=,%20searchable=true,%20orderable=true,%20search=Search(value=,%20regex=false)),%20Column(data=eventValue,%20name=,%20searchable=true,%20orderable=true,%20search=Search(value=,%20regex=false)),%20Column(data=eventRevenue,%20name=,%20searchable=true,%20orderable=true,%20search=Search(value=,%20regex=false)),%20Column(data=eventRevenueCurrency,%20name=,%20searchable=true,%20orderable=true,%20search=Search(value=,%20regex=false)),%20Column(data=eventRevenueUSD,%20name=,%20searchable=true,%20orderable=true,%20search=Search(value=,%20regex=false)),%20Column(data=att,%20name=,%20searchable=true,%20orderable=true,%20search=Search(value=,%20regex=false)),%20Column(data=wifi,%20name=,%20searchable=true,%20orderable=true,%20search=Search(value=,%20regex=false)),%20Column(data=operator,%20name=,%20searchable=true,%20orderable=true,%20search=Search(value=,%20regex=false)),%20Column(data=carrier,%20name=,%20searchable=true,%20orderable=true,%20search=Search(value=,%20regex=false)),%20Column(data=language,%20name=,%20searchable=true,%20orderable=true,%20search=Search(value=,%20regex=false)),%20Column(data=androidId,%20name=,%20searchable=true,%20orderable=true,%20search=Search(value=,%20regex=false)),%20Column(data=imei,%20name=,%20searchable=true,%20orderable=true,%20search=Search(value=,%20regex=false)),%20Column(data=idfa,%20name=,%20searchable=true,%20orderable=true,%20search=Search(value=,%20regex=false)),%20Column(data=idfv,%20name=,%20searchable=true,%20orderable=true,%20search=Search(value=,%20regex=false)),%20Column(data=advertisingId,%20name=,%20searchable=true,%20orderable=true,%20search=Search(value=,%20regex=false)),%20Column(data=deviceModel,%20name=,%20searchable=true,%20orderable=true,%20search=Search(value=,%20regex=false)),%20Column(data=deviceType,%20name=,%20searchable=true,%20orderable=true,%20search=Search(value=,%20regex=false)),%20Column(data=deviceCategory,%20name=,%20searchable=true,%20orderable=true,%20search=Search(value=,%20regex=false)),%20Column(data=platform,%20name=,%20searchable=true,%20orderable=true,%20search=Search(value=,%20regex=false)),%20Column(data=osVersion,%20name=,%20searchable=true,%20orderable=true,%20search=Search(value=,%20regex=false)),%20Column(data=userAgent,%20name=,%20searchable=true,%20orderable=true,%20search=Search(value=,%20regex=false)),%20Column(data=deviceDownloadTime,%20name=,%20searchable=true,%20orderable=true,%20search=Search(value=,%20regex=false)),%20Column(data=oaid,%20name=,%20searchable=true,%20orderable=true,%20search=Search(value=,%20regex=false)),%20Column(data=isLAT,%20name=,%20searchable=true,%20orderable=true,%20search=Search(value=,%20regex=false)),%20Column(data=region,%20name=,%20searchable=true,%20orderable=true,%20search=Search(value=,%20regex=false)),%20Column(data=countryCode,%20name=,%20searchable=true,%20orderable=true,%20search=Search(value=,%20regex=false)),%20Column(data=state,%20name=,%20searchable=true,%20orderable=true,%20search=Search(value=,%20regex=false)),%20Column(data=city,%20name=,%20searchable=true,%20orderable=true,%20search=Search(value=,%20regex=false)),%20Column(data=postalCode,%20name=,%20searchable=true,%20orderable=true,%20search=Search(value=,%20regex=false)),%20Column(data=ip,%20name=,%20searchable=true,%20orderable=true,%20search=Search(value=,%20regex=false)),%20Column(data=attributedTouchType,%20name=,%20searchable=true,%20orderable=true,%20search=Search(value=,%20regex=false)),%20Column(data=dayTimestamp,%20name=,%20searchable=true,%20orderable=true,%20search=Search(value=,%20regex=false)),%20Column(data=partnerId,%20name=,%20searchable=true,%20orderable=true,%20search=Search(value=,%20regex=false)),%20Column(data=partnerName,%20name=,%20searchable=true,%20orderable=true,%20search=Search(value=,%20regex=false))])


//    @RequestMapping(value = "/allReport", method = RequestMethod.GET)
//    public Map<String, Object> getAllReport(@RequestBody DataTablesInput dataTablesInput) {
//        System.out.println("Inside get getAllReportAjax " + /*appId*/  " " + dataTablesInput);
//
////        Map<String, Object> res = reportService.reportAllReportAjax(appId, dataTablesInput);
//        System.out.println();
//
//        Map<String, Object> res = null;
//        return res;
//    }

    @RequestMapping(value = "/allReportTest", method = RequestMethod.GET)
    public Map<String, Object> getAllReportTest(@RequestParam("appId") Long appId,
                                                @RequestBody DataTablesInput dataTablesInput) {

//        DataTablesInput input = draw=1, start=0, length=10, search=Search(value=, regex=false), order=[Order(column=0, dir=asc)], columns=[Column(data=appVersion, name=, searchable=true, orderable=true, search=Search(value=, regex=false)), Column(data=sdkVersion, name=, searchable=true, orderable=true, search=Search(value=, regex=false)), Column(data=appId, name=, searchable=true, orderable=true, search=Search(value=, regex=false)), Column(data=appName, name=, searchable=true, orderable=true, search=Search(value=, regex=false)), Column(data=appType, name=, searchable=true, orderable=true, search=Search(value=, regex=false)), Column(data=bundleId, name=, searchable=true, orderable=true, search=Search(value=, regex=false)), Column(data=eventTime, name=, searchable=true, orderable=true, search=Search(value=, regex=false)), Column(data=eventName, name=, searchable=true, orderable=true, search=Search(value=, regex=false)), Column(data=eventValue, name=, searchable=true, orderable=true, search=Search(value=, regex=false)), Column(data=eventRevenue, name=, searchable=true, orderable=true, search=Search(value=, regex=false)), Column(data=eventRevenueCurrency, name=, searchable=true, orderable=true, search=Search(value=, regex=false)), Column(data=eventRevenueUSD, name=, searchable=true, orderable=true, search=Search(value=, regex=false)), Column(data=att, name=, searchable=true, orderable=true, search=Search(value=, regex=false)), Column(data=wifi, name=, searchable=true, orderable=true, search=Search(value=, regex=false)), Column(data=operator, name=, searchable=true, orderable=true, search=Search(value=, regex=false)), Column(data=carrier, name=, searchable=true, orderable=true, search=Search(value=, regex=false)), Column(data=language, name=, searchable=true, orderable=true, search=Search(value=, regex=false)), Column(data=androidId, name=, searchable=true, orderable=true, search=Search(value=, regex=false)), Column(data=imei, name=, searchable=true, orderable=true, search=Search(value=, regex=false)), Column(data=idfa, name=, searchable=true, orderable=true, search=Search(value=, regex=false)), Column(data=idfv, name=, searchable=true, orderable=true, search=Search(value=, regex=false)), Column(data=advertisingId, name=, searchable=true, orderable=true, search=Search(value=, regex=false)), Column(data=deviceModel, name=, searchable=true, orderable=true, search=Search(value=, regex=false)), Column(data=deviceType, name=, searchable=true, orderable=true, search=Search(value=, regex=false)), Column(data=deviceCategory, name=, searchable=true, orderable=true, search=Search(value=, regex=false)), Column(data=platform, name=, searchable=true, orderable=true, search=Search(value=, regex=false)), Column(data=osVersion, name=, searchable=true, orderable=true, search=Search(value=, regex=false)), Column(data=userAgent, name=, searchable=true, orderable=true, search=Search(value=, regex=false)), Column(data=deviceDownloadTime, name=, searchable=true, orderable=true, search=Search(value=, regex=false)), Column(data=oaid, name=, searchable=true, orderable=true, search=Search(value=, regex=false)), Column(data=isLAT, name=, searchable=true, orderable=true, search=Search(value=, regex=false)), Column(data=region, name=, searchable=true, orderable=true, search=Search(value=, regex=false)), Column(data=countryCode, name=, searchable=true, orderable=true, search=Search(value=, regex=false)), Column(data=state, name=, searchable=true, orderable=true, search=Search(value=, regex=false)), Column(data=city, name=, searchable=true, orderable=true, search=Search(value=, regex=false)), Column(data=postalCode, name=, searchable=true, orderable=true, search=Search(value=, regex=false)), Column(data=ip, name=, searchable=true, orderable=true, search=Search(value=, regex=false)), Column(data=attributedTouchType, name=, searchable=true, orderable=true, search=Search(value=, regex=false)), Column(data=dayTimestamp, name=, searchable=true, orderable=true, search=Search(value=, regex=false)), Column(data=partnerId, name=, searchable=true, orderable=true, search=Search(value=, regex=false)), Column(data=partnerName, name=, searchable=true, orderable=true, search=Search(value=, regex=false))];
        System.out.println("Inside get getAllReportAjax " + appId + " " + dataTablesInput);
//        DataTablesInput dataTablesInput1 = dataTablesInput;

        Map<String, Object> res = reportService.reportAllReportAjax(appId, dataTablesInput);
        System.out.println();

        return res;
    }

    @PostMapping(path = "/allReport")
    public ResponseEntity<?> getAllReport(@RequestBody Map<String, Object> input) {
        System.out.println("yoyoy " + input.get("id") + " bobo " + input.get("dataTablesInput"));

//        Integer appId = (Integer) input.get("id");
//        Object dataTablesInput = input.get("dataTablesInput");

        ObjectMapper objectMapper = new ObjectMapper();
        Long appId = objectMapper.convertValue(input.get("id"), Long.class);
        DataTablesInput dataTablesInput = objectMapper.convertValue(input.get("dataTablesInput"), DataTablesInput.class);

        Map<String, Object> res = reportService.reportAllReportAjax(appId, dataTablesInput);

        return ResponseEntity.ok().body(res);
    }

//    @PostMapping(path = "/allReport", consumes = "application/json", produces = "application/json")
//    public ResponseEntity<?> getAllReport(@RequestBody Map<String, Object> input) {
////        Map<String, Object> res = reportService.reportClickAjax(input);
//        Object res = null;
//        return ResponseEntity.ok().body(res);
//    }


    @RequestMapping(value = "/conversionReport", method = RequestMethod.GET)
    public Map<String, Object> getConversionReport(@RequestParam(value = "input", required = false) DataTablesInput input) {
        System.out.println("Inside get getAllReportAjax " + input);

        System.out.println("Inside get Conversion report");

        Map<String, Object> res = reportService.reportConversionAjax(input);

        System.out.println("getConversionReport " + res);

        return res;
    }

    @RequestMapping(value = "/clickReport", method = RequestMethod.GET)
    public Map<String, Object> getClickReport(@RequestParam(value = "input", required = false) DataTablesInput input) {

        System.out.println("Inside get Click report yooooo" + input);

        Map<String, Object> res = reportService.reportClickAjax(input);

        System.out.println("getConversionReport " + res);

        return res;
    }

    @GetMapping("/addFilterForm")
    public ResponseEntity<?> addFilterForm(@RequestParam("id") Long id, DataTablesInput input) {
        System.out.println("Inside get Conversion report" + id + " " + input);

//        Map<String, Object> res = reportService.reportAllFilterAjax(input);

//        String result = apiGet(backendConstants.baseUrlDbAccess + conversionReport, input);
        return ResponseEntity.ok().body(input);
    }

}


//    @GetMapping(value = "/getAllReports")
//    public String getAllReport(ReportFilterDTO reportFilterDTO, Map<String, Object> model) {
//        System.out.println("Inside getAll report:" + reportFilterDTO + " " + model);
//        List<AllReportDTO> list = null;
//        list = reportService.reportAllAjax(new ReportFilterDTO());
//        System.out.println("Inside getAll report: " + list.size() + " " + list
//        );
//        System.out.println(",reportFilterDTO.getPublisherName()  ");
//
//        model.put("list", list);
//        model.put("filters", new ReportFilterDTO());
//        model.put("apps", reportFilterDTO.getAppName());
//        model.put("partners", reportFilterDTO.getPartnerName());
//        model.put("groupBy", reportFilterDTO.getGroupBy());
//        return "jsp/allReports";
//    }


//    @GetMapping("/conversionReport")
//    public ResponseEntity<?> getConversionReport(DataTablesInput input) {
//        System.out.println("Inside get Conversion report");
//
//        Map<String, Object> res = reportService.reportConversionAjax(input);
//
////        String result = apiGet(backendConstants.baseUrlDbAccess + conversionReport, input);
//        return ResponseEntity.ok().body(res);
//    }

//    @GetMapping("/clickReport")
//    public ResponseEntity<?> getClickReport(DataTablesInput input) {
//        System.out.println("Inside get Click report yooooo" + input);
//
//        Map<String, Object> res = reportService.reportClickAjax(input);
//        return ResponseEntity.ok().body(res);
//    }


//    @GetMapping("/getAllReportAjax")
//    public ResponseEntity<?> getAllReportAjax(@RequestParam("id") Long appId,
//                                              @RequestParam(value = "attributedTouchTypeSelect", required = false) String attributedTouchTypeSelect, DataTablesInput input) {
//        System.out.println("Inside get getAllReportAjax " + appId + " " + attributedTouchTypeSelect + " " + input);
//
//        Map<String, Object> res = reportService.reportAllReportAjax(appId, input);
//
////        String result = apiGet(backendConstants.baseUrlDbAccess + conversionReport, input);
//        return ResponseEntity.ok().body(res);
//    }


//    @PostMapping("/allReport")
//    public String getAllReport(@RequestParam("id") Long id, ReportFilterDTO reportFilterDTO, Map<String, Object> model, Principal principal, HttpServletRequest request) {
//        System.out.println("lisiOfLosy" + reportFilterDTO);
//        App app = appService.findById(id);
//        List<AllReportDTO> list = reportService.reportAllAjax(reportFilterDTO , id);
//        System.out.println("lisiOfLosy" + list);
//
//
////        RestTemplate restTemplate = new RestTemplate();
////        ResponseEntity<Object> responseEntity = restTemplate.postForEntity(backendConstants.baseUrlDbAccess + allReport, reportFilterDTO, Object.class);
////        List<AllReportDTO> list = (List<AllReportDTO>) responseEntity.getBody();
//
//        reportService.modifyFilterReportDTO(reportFilterDTO);
//
//        if (request.isUserInRole("ROLE_PARTNER")) {
//            String name = principal.getName();
//            Partner partner = partnerService.findByEmail(name);
////            Publisher publisher = restTemplate.getForObject(backendConstants.baseUrlDbAccess + "/adminConsole/findByPublisherEmail/{name}", Publisher.class, name);
//            model.put("partner", partner);
//        }
//
//        List<App> appsList = appService.findAll();
//        model.put("appsList", appsList);
//        model.put("app", app);
//        model.put("list", list);
//        model.put("filters", reportFilterDTO);
//        model.put("apps", reportFilterDTO.getAppName());
//        model.put("partners", reportFilterDTO.getPartnerName());
//        model.put("groupBy", reportFilterDTO.getGroupBy());
//        return "jsp/allReport";
//    }


/*    @PostMapping(path = "/addFilterForm")
    public List<AllReportDTO> addFilterForm(@RequestBody ReportFilterDTO reportFilterDTO, Map<String, Object> model, Principal principal, HttpServletRequest request) {
        System.out.println("hello hi bye bye1 " + reportFilterDTO);
//        Set<AllReportDTO> res = reportService.reportAllAjax(reportFilterDTO);
        return (List<AllReportDTO>) reportFilterDTO;
    }*/


/*    @PostMapping("/addFilterForm")
    public String addFilterForm(ReportFilterDTO reportFilterDTO, Map<String, Object> model, Principal principal, HttpServletRequest request) {

        System.out.println("hello hi bye bye1 " + reportFilterDTO + " " + model + " " + principal);

        return "yoo";
    }*/


//    @PostMapping(value = "/addFilterForm")
//    public String addFilterForm(@RequestParam(value = "daterange", required = false) String daterange,
//                                @RequestParam(value = "appsId", required = false) Long apps,
//                                @RequestParam(value = "partners", required = false) List<String> partners,
//                                @RequestParam(value = "groupBy", required = false) List<String> groupBy,
//                                @RequestParam(value = "columns", required = false) List<String> columns, Map<String, Object> model) {
//        System.out.println("hello hi bye bye0 " + daterange + " apps " + apps);
////        System.out.println("hello hi bye bye1 " + reportFilterDTO);
//        System.out.println("hello hi bye bye2 " + partners + " " + groupBy);
//        System.out.println("partnersInEvent " + columns );
//
//
//        App app = appService.findById(apps);
//        System.out.println("Inside getAll report2:" + " " + model);
//        List<AllReportDTO> list = null;
//        list = reportService.reportAllFilterAjax(daterange, apps, partners, groupBy, columns);
//        System.out.println("Inside getAll report2: " + list.size() + " " + list);
//        System.out.println(",reportFilterDTO.getPublisherName() ");
//
//        list.get(0).getAppId();
//
//
////        log.info("Inside getAll report:");
////        List<AllReportDTO> list = null;
////        RestTemplate restTemplate = new RestTemplate();
////        ResponseEntity<Object> responseEntity = restTemplate.postForEntity(backendConstants.baseUrlDbAccess + allReport, new ReportFilterDTO(), Object.class);
////        list = (List<AllReportDTO>) responseEntity.getBody();
//        List<App> appsList = appService.findAll();
//        model.put("appsList", appsList);
//        model.put("app", app);
//        model.put("list", list);
//        model.put("filters", new ReportFilterDTO());
//
////        return ResponseEntity.ok().body("ok");
//        return "jsp/allReport";
//    }