//package com.adstacks.attribution.reports.dto;
//
//import com.etreetech.panel.beans.Click;
//import com.etreetech.panel.beans.Conversion;
//import com.etreetech.panel.cache.dto.AdvertiserTrackingDTO;
//import com.etreetech.panel.enums.ConversionStatus;
//import lombok.Data;
//
//@Data
//public class ConversionReportDTO {
//    private String creationDateTime;
//    private ConversionStatus conversionStatus;
//    private String conversionMessage;
//    private String eventName;
//    private String uuid;//conversion
//    private String clickUuid;
//    private Long id;//panel click id
//
//    public ConversionReportDTO(String creationDateTime, ConversionStatus conversionStatus, String conversionMessage, String eventName, String clickUuid,
//                               String pubClickId, String gaid, String idfa, String ip, String agent,
//                               String clickMessage, boolean isGross, String subAff, String source, String s1, String s2,
//                               String s3, String s4, String s5, Long offerId, String offerName, Long pubId, String pubName, String uuid) {
//        this.creationDateTime = creationDateTime;
//        this.conversionStatus = conversionStatus;
//        this.conversionMessage = conversionMessage;
//        this.eventName = eventName;
//        this.clickUuid = clickUuid;
//        this.pubClickId = pubClickId;
//        this.gaid = gaid;
//        this.idfa = idfa;
//        this.ip = ip;
//        this.agent = agent;
//        this.clickMessage = clickMessage;
//        this.isGross = isGross;
//        this.subAff = subAff;
//        this.source = source;
//        this.s1 = s1;
//        this.s2 = s2;
//        this.s3 = s3;
//        this.s4 = s4;
//        this.s5 = s5;
//        this.offerId = offerId;
//        this.offerName = offerName;
//        this.pubId = pubId;
//        this.pubName = pubName;
//        this.uuid = uuid;
//    }
//
//    private String pubClickId; //pub click id
//    private String gaid; //android device id
//    private String idfa; //iOS device id
//    private String ip;    //IP address
//    private String agent; //user agent
//    private String clickMessage; //message for click rejection or approval
//    private Boolean isGross; //is gross click?
//    private String subAff;
//    private String source;
//    private String s1;
//    private String s2;
//    private String s3;
//    private String s4;
//    private String s5;
//    private Long offerId;
//    private String offerName;
//    private Long pubId;
//    private String pubName;
//    private Long advId;
//    private String advName;
//
//    public ConversionReportDTO(Conversion conversion, Click click, AdvertiserTrackingDTO advertiserTrackingDTO) {
//        this.creationDateTime = conversion.getCreationDateTime();
//        this.conversionStatus = conversion.getConversionStatus();
//        this.conversionMessage = conversion.getConversionMessage();
//        this.eventName = (conversion.getEvent()!=null)?conversion.getEvent().getEventName():null;
//        this.clickUuid = conversion.getClickId();
//        this.pubClickId = (click!=null)?click.getPubClickId():null;
//        this.gaid = (click!=null)?click.getGaid():null;
//        this.idfa = (click!=null)?click.getIdfa():null;
//        this.ip = (click!=null)?click.getIp():null;
//        this.agent = (click!=null)?click.getAgent():null;
//        this.clickMessage = (click!=null)?click.getClickMessage():null;
//        this.isGross = (click!=null)?click.isGross():null;
//        this.subAff = (click!=null)?click.getSubAff():null;
//        this.source = (click!=null)?click.getSource():null;
//        this.s1 = (click!=null)?click.getS1():null;
//        this.s2 = (click!=null)?click.getS2():null;
//        this.s3 = (click!=null)?click.getS3():null;
//        this.s4 = (click!=null)?click.getS4():null;
//        this.s5 = (click!=null)?click.getS5():null;
//        this.offerId = (conversion.getEvent()!=null)?conversion.getOffer().getOfferId():null;
//        this.offerName = (conversion.getEvent()!=null)?conversion.getOffer().getOfferName():null;
//        this.pubId = (conversion.getEvent()!=null)?conversion.getPublisher().getPublisherId():null;
//        this.pubName = (conversion.getEvent()!=null)?conversion.getPublisher().getPublisherName():null;
//        this.uuid = conversion.getUuid();
//        this.advId=conversion.getAdvertiserId();
//        this.advName=(advertiserTrackingDTO!=null)?advertiserTrackingDTO.getCompany():null;
//    }
//}
