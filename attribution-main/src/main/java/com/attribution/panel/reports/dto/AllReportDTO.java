package com.attribution.panel.reports.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class AllReportDTO {

    private long click;
    private long approvedConversions;
    private long cancelledConversions;
    private long pendingConversions;
    private long grossConversions;

    private double approvedRevenue;
    private double cancelledRevenue;
    private double pendingRevenue;
    private double grossRevenue;
    private int rejectedClicks;

    private double approvedPayout;

    private String offerName;
    private String advertiserName;
    private String publisherName;
    @EqualsAndHashCode.Include
    private long advertiserId;

    @EqualsAndHashCode.Include
    private long offerId;
    @EqualsAndHashCode.Include
    private long publisherId;
    @EqualsAndHashCode.Include
    private String subAffiliate;

    private Date dayTimestamp;

    public AllReportDTO(AllReportDTO allReportDTO) {
        this.approvedConversions = allReportDTO.approvedConversions;
        this.cancelledConversions = allReportDTO.cancelledConversions;
        this.pendingConversions = allReportDTO.pendingConversions;
        this.approvedRevenue = allReportDTO.approvedRevenue;
        this.cancelledRevenue = allReportDTO.cancelledRevenue;
        this.pendingRevenue = allReportDTO.pendingRevenue;
        this.approvedPayout = allReportDTO.approvedPayout;
        this.click = allReportDTO.getClick();
        this.offerName = null;
        this.advertiserName = null;
        this.publisherName = null;
        this.subAffiliate = allReportDTO.getSubAffiliate();
        this.offerId = allReportDTO.getOfferId();
        this.publisherId = allReportDTO.getPublisherId();
        this.advertiserId = allReportDTO.getAdvertiserId();
        this.rejectedClicks=allReportDTO.getRejectedClicks();
    }
}
