package com.attribution.panel.reports.dto;

import lombok.Data;

import java.util.List;

@Data
public class ReportFilterDTO {
    private List<String> advertisers;
    private List<String> offers;
    private List<String> publishers;
    private String subAffiliate;
    private List<String> groupBy;
    private String daterange;
}
