//package com.adstacks.attribution.reports.dto;
//
//import com.etreetech.panel.beans.Click;
//import com.etreetech.panel.beans.DeviceId;
//import com.etreetech.panel.cache.dto.AdvertiserTrackingDTO;
//import com.etreetech.panel.cache.dto.OfferTrackingDTO;
//import com.etreetech.panel.cache.dto.PublisherTrackingDTO;
//import com.etreetech.panel.enums.CountryEnum;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//
//@Data
//@NoArgsConstructor
//public class ClickDetailsDTO {
//
//        private String id;
//
//        private String pubClickId;
//        private String gaid; //android device id
//        private String idfa; //iOS device id
//        private String ip;    //IP address
//
//        private String agent; //user agent
//
//        //We can use P for Passed and A for Approved
//        private String clickMessage; //message for click rejection or approval
//        private boolean isGross;
//        private String subAff;
//        private String source;
//        private String s1;
//        private String s2;
//        private String s3;
//        private String s4;
//        private String s5;
//
//
//        private Long pubId;
//
//        private int pubResponse;
//
//        private Long offerId;
//
//        private Long advertiserId;;
//        private String offerName;
//        private  String advertiserName;
//        private String publisherName;
//        private CountryEnum country;
//        private float revenue;
//
//        private float payout;
//
//        private DeviceId deviceId;
//
//        private String creationDateTime;
//
//        public ClickDetailsDTO(Click click, OfferTrackingDTO offerTrackingDTO, AdvertiserTrackingDTO advertiserTrackingDTO, PublisherTrackingDTO publisherTrackingDTO) {
//                this.id = click.getId();
//                this.pubClickId = click.getPubClickId();
//                this.gaid = click.getGaid();
//                this.idfa = click.getIdfa();
//                this.ip = click.getIp();
//                this.agent = click.getAgent();
//                this.clickMessage = click.getClickMessage();
//                this.isGross = click.isGross();
//                this.subAff = click.getSubAff();
//                this.source = click.getSource();
//                this.s1 = click.getS1();
//                this.s2 = click.getS2();
//                this.s3 = click.getS3();
//                this.s4 = click.getS4();
//                this.s5 = click.getS5();
//                this.pubId = click.getPubId();
//                this.pubResponse = click.getPubResponse();
//                this.offerId = click.getOfferId();
//                this.advertiserId = advertiserTrackingDTO.getId();
//                this.offerName = offerTrackingDTO.getOfferName();
//                this.advertiserName = advertiserTrackingDTO.getFname();
//                this.publisherName = publisherTrackingDTO.getFname();
//                this.revenue = click.getRevenue();
//                this.payout = click.getPayout();
//                this.deviceId = click.getDeviceId();
//                this.creationDateTime = click.getCreationDateTime();
//                this.updateDateTime = click.getUpdateDateTime();
//                this.country=click.getCountry();
//        }
//
//        private String updateDateTime;
//
//}
