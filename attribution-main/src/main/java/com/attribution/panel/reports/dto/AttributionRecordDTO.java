package com.attribution.panel.reports.dto;


import com.attribution.panel.bean.App;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.UUID;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString(onlyExplicitlyIncluded = true)
@NoArgsConstructor
public class AttributionRecordDTO implements Serializable {

    private String id;

    //app
    private String appVersion;
    private String sdkVersion;
    private Long appId;
    private String appName;
    private String appType;
    private String bundleId;
    private Long partnerId;
    private String partnerName;



    // conversion data
    private int approvedConversions;
    private int cancelledConversions;
    private int pendingConversions;
    private int rejectedClicks;
    private float approvedRevenue;
    private float cancelledRevenue;
    private float pendingRevenue;
    private float approvedPayout;
    private long clickCount;
    private String dayTimestamp;
    private double ratio;
    private double ctr; //click through rate


    public AttributionRecordDTO(AttributionConversionDTO conversion, String dayTimestamp, AttributionClickDTO attributionclick, App app) throws Exception {
        System.out.println("YoInHereAttributionRecord " + attributionclick.getClickMessage());
//        this.id = attributionRecordId;

        this.id = UUID.randomUUID().toString().replace("-", "");

        this.appId = attributionclick.getAppID();
        this.appName = attributionclick.getAppName();
        this.appType = app.getAppType();
        this.bundleId = app.getBundleId();
        this.appVersion = app.getAppVersion();
        this.sdkVersion = app.getSdkVersion();


        this.partnerId = attributionclick.getPId();
        this.partnerName = attributionclick.getPName();
        this.dayTimestamp = dayTimestamp;
//        System.out.println("YoInHereAttributionRecord2" + (attributionRecord.getApprovedConversions() / attributionRecord.getClickCount()) * 100);
//        this.ratio = (attributionRecord.getApprovedConversions() / attributionRecord.getClickCount()) * 100;
        //

    }


    public AttributionRecordDTO(AttributionRecordDTO attribution, AttributionClickDTO click) {

        this.id = attribution.getId();
        this.appId = attribution.getAppId();
        this.appName = attribution.getAppName();
        this.partnerId = attribution.getPartnerId();
        this.partnerName = attribution.getPartnerName();
        this.approvedConversions = attribution.getApprovedConversions();
        this.cancelledConversions = attribution.getCancelledConversions();
        this.pendingConversions = attribution.getPendingConversions();
        this.rejectedClicks = attribution.getRejectedClicks();
        this.approvedRevenue = attribution.getApprovedRevenue();
        this.cancelledRevenue = attribution.getCancelledRevenue();
        this.pendingRevenue = attribution.getPendingRevenue();
        this.approvedPayout = attribution.getApprovedPayout();
        this.clickCount = attribution.getClickCount();
        this.ratio = attribution.getRatio();
        this.ctr = attribution.getCtr();
        this.dayTimestamp = attribution.getDayTimestamp();


    }
}
