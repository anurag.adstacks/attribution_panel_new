//package com.adstacks.attribution.reports.controller;
//
//import com.etreetech.panel.reports.service.ReportExportService;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//
//import java.io.IOException;
//
//@RestController
//@RequestMapping(value = "/reports")
//@Slf4j
//public class ReportExportController {
//    @Autowired
//    ReportExportService reportExportService;
//
//
//
//    /*Export count functionality of click report*/
//    @RequestMapping(value = "/findClickExportDataCount", method = RequestMethod.GET)
//    public Long findByClickExportDataCount(@RequestParam(value = "date", defaultValue = "", required = false) String date,
//                                      @RequestParam(value = "offerId", defaultValue = "", required = false) String offerId,
//                                      @RequestParam(value = "pubId", defaultValue = "", required = false) String pubId,
//                                      @RequestParam(value = "advId", defaultValue = "", required = false) String advId,
//                                      @RequestParam(value = "uuid") String uuid) throws IOException {
//        return reportExportService.count(date, offerId, pubId, advId, uuid);
//    }
//
//    /*Export count functionality of conversion report*/
//    @RequestMapping(value = "/findConversionExportDataCount", method = RequestMethod.GET)
//    public Long findByConversionExportDataCount(@RequestParam(value = "date", defaultValue = "", required = false) String date,
//                                                @RequestParam(value = "offerId", defaultValue = "", required = false) String offerId,
//                                                @RequestParam(value = "pubId", defaultValue = "", required = false) String pubId,
//                                                @RequestParam(value = "conversionStatus", defaultValue = "", required = false) String conversionStatus) throws IOException {
//        return reportExportService.countConversionData(date, offerId, pubId, conversionStatus);
//    }
//}
