//package com.adstacks.attribution.reports.service;
//
//import com.alibaba.fastjson.JSON;
//import com.etreetech.panel.beans.CSVFile;
//import com.etreetech.panel.beans.Click;
//import com.etreetech.panel.cache.service.CacheService;
//import com.etreetech.panel.constants.BackendConstants;
//import com.etreetech.panel.enums.CSVType;
//import com.etreetech.panel.reports.dao.ReportQueryBuilder;
//import com.etreetech.panel.repository.CSVRepository;
//import com.opencsv.CSVWriter;
//import lombok.extern.slf4j.Slf4j;
//import org.elasticsearch.search.SearchHit;
//import org.springframework.amqp.rabbit.annotation.RabbitListener;
//import org.springframework.amqp.rabbit.core.RabbitTemplate;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.io.File;
//import java.io.FileWriter;
//import java.io.IOException;
//import java.text.SimpleDateFormat;
//import java.util.*;
//import java.util.stream.Collectors;
//
//@Service
//@Transactional
//@Slf4j
//public class ReportExportService {
//    @Autowired
//    CSVRepository csvRepository;
//
//    @Autowired
//    RabbitTemplate rabbitTemplate;
//
//    @Autowired
//    ReportServiceeee reportService;
//
//    @Autowired
//    BackendConstants backendConstants;
//
//    @Autowired
//    CacheService cacheService;
//
//    @Autowired
//    ReportQueryBuilder reportQueryBuilder;
//
//    @RabbitListener(queues = "exportData")
//    private void exportCsv(Map<String, Object> map) throws IOException {
//        log.info("Inside exportData Listener::");
//        String startDate = (String) map.get("startDate");
//        String endDate = (String) map.get("endDate");
//        String clickId = (String) map.get("clickId");
//        String path = (String) map.get("path");
//        String columns = (String) map.get("columns");
//        Long csvFileId = (Long) map.get("csvFileId");
//        List<String> offerIds = (List<String>) map.get("offerIds");
//        List<String> pubIds = (List<String>) map.get("pubIds");
//        List<String> advIds = (List<String>) map.get("advIds");
//
//        List<SearchHit[]> data = reportQueryBuilder.clickExportDataList(startDate, endDate, offerIds, pubIds, advIds, clickId);
//        System.out.println(data.size());
//        File file = new File(path);
//        try {
//            // create FileWriter object with file as parameter
//            FileWriter outputfile = new FileWriter(file);
//
//            // create CSVWriter with ';' as separator
//            CSVWriter writer = new CSVWriter(outputfile, ';',
//                    CSVWriter.NO_QUOTE_CHARACTER,
//                    CSVWriter.DEFAULT_ESCAPE_CHARACTER,
//                    CSVWriter.DEFAULT_LINE_END);
//            writer.writeNext(new String[]{columns});
//            addValue(data, columns, writer);
//            // closing writer connection
//            writer.close();
//        } catch (IOException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//        log.info("Inside save saveFile ");
//        CSVFile csvFile = csvRepository.findById(csvFileId).orElse(null);
//        csvFile.setCsvStatus(CSVType.Completed);
//        csvFile.setFileDownloadUri(path);
//        csvRepository.save(csvFile);
//    }
//
//    private void addValue(List<SearchHit[]> data, String columns, CSVWriter writer) {
//        for (SearchHit[] searchHits : data) {
//            for (SearchHit hit : searchHits) {
//                Click click = JSON.parseObject(hit.getSourceAsString(), Click.class);
//                String str1 = null;
//                if (columns.contains("Offer Id"))
//                    str1 = click.getOfferId() + ",";
//                if (columns.contains("Offer Name"))
//                    str1 += cacheService.getOffer(click.getOfferId()).getOfferName() + ",";
//                if (columns.contains("Click Id"))
//                    str1 += click.getId() + ",";
//                if (columns.contains("Pub Click Id"))
//                    str1 += click.getPubClickId() + ",";
//                if (columns.contains("Publisher Id"))
//                    str1 += click.getPubId() + ",";
//                if (columns.contains("Publisher Name"))
//                    str1 += cacheService.getPublisher(click.getPubId()).getFname() + ",";
//                if (columns.contains("IP Address"))
//                    str1 += click.getIp() + ",";
//                if (columns.contains("Device Name"))
//                    str1 += click.getDeviceId().getDeviceName() + ",";
//                if (columns.contains("Creation Date Time"))
//                    str1 += click.getCreationDateTime();
//                writer.writeNext(new String[]{str1});
//            }
//        }
//    }
//
//    public Long count(String date, String offerId, String pubId, String advId, String uuid) {
//        List<String> offerIds = Arrays.asList(offerId.split(","));
//        List<String> pubIds = Arrays.asList(pubId.split(","));
//        List<String> advIds = Arrays.asList(advId.split(","));
//        offerIds = offerIds.stream().filter(x -> (!x.equals(""))).collect(Collectors.toList());
//        pubIds = pubIds.stream().filter(x -> (!x.equals(""))).collect(Collectors.toList());
//        advIds = advIds.stream().filter(x -> (!x.equals(""))).collect(Collectors.toList());
//        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
//        SimpleDateFormat formatterForJsDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        String startDate = null;
//        String endDate = null;
//        if (date.equals("")) {
//            endDate = formatter.format(new Date());
//            Calendar cal = Calendar.getInstance();
//            cal.add(Calendar.DATE, -7);
//            startDate = formatter.format(cal.getTime());
//        } else {
//            String[] startDateArr = date.split(" - ");
//            try {
//                startDate = formatter.format(formatterForJsDate.parse(startDateArr[0]));
//                endDate = formatter.format(formatterForJsDate.parse(startDateArr[1]));
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//        return reportQueryBuilder.clickExportDataListCount(startDate, endDate, offerIds, pubIds, advIds, uuid);
//
//    }
//
//    public Long countConversionData(String date, String offerId, String pubId, String conversionStatus) {
//        List<String> offerIds = Arrays.asList(offerId.split(","));
//        List<String> pubIds = Arrays.asList(pubId.split(","));
//        List<String> conversionsStatus = Arrays.asList(conversionStatus.split(","));
//        offerIds = offerIds.stream().filter(x -> (!x.equals(""))).collect(Collectors.toList());
//        pubIds = pubIds.stream().filter(x -> (!x.equals(""))).collect(Collectors.toList());
//        conversionsStatus = conversionsStatus.stream().filter(x -> (!x.equals(""))).collect(Collectors.toList());
//        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
//        SimpleDateFormat formatterForJsDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        String startDate = null;
//        String endDate = null;
//        if (date.equals("")) {
//            endDate = formatter.format(new Date());
//            Calendar cal = Calendar.getInstance();
//            cal.add(Calendar.DATE, -7);
//            startDate = formatter.format(cal.getTime());
//        } else {
//            String[] startDateArr = date.split(" - ");
//            try {
//                startDate = formatter.format(formatterForJsDate.parse(startDateArr[0]));
//                endDate = formatter.format(formatterForJsDate.parse(startDateArr[1]));
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//        return reportQueryBuilder.conversionExportDataListCount(startDate, endDate, offerIds, pubIds,conversionsStatus);
//
//    }
//
//
//}
