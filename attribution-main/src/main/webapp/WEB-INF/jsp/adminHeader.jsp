<%@ page import="com.attribution.panel.constants.UIConstants" %>
<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
    <div class="container-fluid">
        <div class="navbar-wrapper">
            <div class="navbar-minimize">
                <%--<button id="minimizeSidebar" class="btn btn-<%=UIConstants.primaryColorClass%> btn-just-icon btn-fab btn-round">
                    <i class="material-icons text_align-center visible-on-sidebar-regular">view_list</i>
                    <i class="material-icons design_bullet-list-67 visible-on-sidebar-mini">view_list</i>
                </button>--%>
            </div>
            <%--<a class="navbar-brand text-white" href="#pablo" ></a>--%>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index"
                aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-end">
            <ul class="navbar-nav">
                <li class="nav-item dropdown" data-toggle="tooltip" data-html="true" title="Log Out">
                    <%--<a class="nav-link" href="#pablo" onclick="getUser()"
                       aria-haspopup="true" aria-expanded="false">
                        <i class="material-icons <%=UIConstants.primaryTextColorClass%>">power_settings_new</i><b>Log Out</b>
                        <p class="d-lg-none d-md-block">
                            Account
                        </p>
                    </a>--%>
                        <a class="nav-link" id="logOut" onclick="getUser()" aria-haspopup="true" aria-expanded="false" style="height:38px;background: red;border-radius:15px;padding: 9px 10px;font-size: 16px;color:#FFFFFF;cursor: pointer;right:20px">
                            <i class="material-icons">power_settings_new</i><b id="logOutLabel">&nbsp;Log Out&nbsp;&nbsp;</b>
                        </a>
                    <form id="logoutForm" method="POST" action="${contextPath}/logout">
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                    </form>
                </li>
            </ul>

        </div>
    </div>
</nav>
<script>
    function getUser(){
        document.forms['logoutForm'].submit()
    }
</script>