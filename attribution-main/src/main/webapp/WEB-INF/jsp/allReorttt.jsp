<!DOCTYPE html>
<%@ page import="com.attribution.panel.constants.UIConstants" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<html lang="en">

<head>
    <meta charset="utf-8"/>
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="../assets/img/adjar.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>Report</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport'/>
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- CSS Files -->
    <link href="../assets/css/material-dashboard.css?v=2.1.0" rel="stylesheet"/>
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="../assets/demo/demo.css" rel="stylesheet"/>
    <link href="../assets/css/style-sheet-adjar.css?v=2.1.0" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="../assets/css/daterangepicker.css"/>
    <script src="../assets/js/core/jquery.min.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
    <script src="https://rawgit.com/dbrekalo/attire/master/dist/js/build.min.js"></script>
    <link rel="stylesheet" href="../assets/fastselect.min.css">
    <script src="../assets/fastselect.standalone.js"></script>
    <%--loader CSS Files--%>
    <link href="../assets/css/loader.css" rel="stylesheet"/>
    <%--responsive css file--%>
    <link href="../assets/css/responsive-modal-pages.css" rel="stylesheet"/>
    <%--loader JS Files--%>
    <script src="../assets/js/loader.js"></script>
    <link href="../dist/css/select2.css" rel="stylesheet"/>
    <script src="../dist/js/select2.js"></script>
    <style>
        .dataTables_length::after {
            top: -2px;
            right: 65px;
        }

        .dataTables_sizing {
            display: none;
        }

        .dataTables_length {
            margin-left: 135px;
        }

        .swal2-input {
            background-color: <%=UIConstants.sweetAlertBgColor%> !important;
            color: <%=UIConstants.textColor%> !important;
            font-size: 15px !important;
            margin-top: -5px !important;
        }

        .swal2-confirm, .swal2-cancel {
            height: 30px !important;
            padding: 5px 10px !important;
            margin-top: -15px !important;
        }

        .swal2-modal {
            background-color: <%=UIConstants.sweetAlertBgColor%> !important;
            color: <%=UIConstants.textColor%> !important;
            margin-top: 100px !important;
            margin-left: 40% !important;
        }

        .swal2-title {
            color: <%=UIConstants.textColor%> !important;
            margin-top: -10px !important;
        }

        .swal2-success-circular-line-right,
        .swal2-success-circular-line-left, .swal2-success-fix {
            background-color: <%=UIConstants.sweetAlertBgColor%> !important;
        }
    </style>
</head>
<%
    String cp = request.getContextPath();
%>

<body class="" style="background-color: <%=UIConstants.primaryBackgroundColor%>;">
<div class="se-pre-con"></div>
<jsp:include page="adminSidebar.jsp">
    <jsp:param value="active" name="conversionReport"/>
    <jsp:param value="show" name="reportsTab"/>

</jsp:include>
<div class="main-panel">
    <!-- Navbar -->
    <jsp:include page="adminHeader.jsp"/>
    <!-- End Navbar -->

    <div class="content">
        <div class="container-fluid">
            <div class="cardDiv">
                <div class="cardSize">
                    <i class="material-icons iconSize">assignment</i>
                </div>
                <h2 class="cardHeading">Conversion Report</h2>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card <%=UIConstants.primaryTextColorClass%>"
                         style="background-color: <%=UIConstants.primaryCardBackground%>;">
                        <div class="card-header card-header-<%=UIConstants.primaryColorClass%> card-header-icon conversionHeader">
                            <div class="row pull-right conversionReportRange"
                                 style="margin-top:11px;margin-right:-20px">
                                <div class="col-sm-7" style="">
                                    <!-- <i class="fa fa-caret-down"></i> -->
                                    <div class="text-dark" id="reportrange" style="margin-top: -53px;">
                                        <input readonly type="text" class="<%=UIConstants.primaryTextColorClass%>"
                                               id="reportrangeinput"
                                               name="daterange"
                                               style="cursor: pointer; background-color: <%=UIConstants.primaryBackgroundColor%>; border:1px solid <%=UIConstants.borderColor%>; width:295px;padding:2px 6px">
                                    </div>
                                </div>

                            </div>
                            <div class="row pull-right" style="margin-top:-2px;margin-right:-60px">
                                <div class="col-sm-7">
                                    <button class="btn btn-<%=UIConstants.primaryColorClass%> filterButton pull-right"
                                            data-toggle="modal"
                                            id="offerFilter"
                                            style="cursor: pointer;margin-top:-39px;height:28px;border-radius:5px;border:1.8px solid #fff"
                                            data-target="#exampleModalAd">
                                        <i class="fa fa-search" aria-hidden="true" data-toggle="modal"
                                           data-target="#addRule"
                                           id="reset"
                                           style="margin-left:0px;margin-top:-16px;width:auto;font-size:16px"><b
                                                style="margin-left:6px">Search&nbsp;</b></i>
                                    </button>
                                </div>
                            </div>
                            <div class="row pull-right" style="margin-top:-2px;margin-right:44px">
                                <div class="col-sm-7" style="">
                                    <button class="filterButton btn btn-<%=UIConstants.primaryColorClass%> btn-round pull-right"
                                            id="exportData"
                                            style="cursor: pointer;margin-top:-39px;height:28px;border-radius:5px;border:1.8px solid #fff">
                                        <i class="fa fa-download" aria-hidden="true"
                                           style="margin-left:0px;margin-top:-15px;width:auto;font-size:16px"><b
                                                style="margin-left:5px;">Export</b>&nbsp;</i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="toolbar <%=UIConstants.primaryTextColorClass%>"
                                 style="font-family: <%=UIConstants.primaryFontFamily%>;">
                                <!--        Here you can write extra buttons/actions for the toolbar              -->

                                <!-- Modal -->
                                <div class="modal fade" id="exampleModalAd" tabindex="-1" role="dialog"
                                     aria-labelledby="exampleModalLongTitle" aria-hidden="true" data-backdrop="static">


                                    <div class="modal-dialog" role="document" id="exampleModalAd1">
                                        <div class="card modal-content conversionContent <%=UIConstants.primaryTextColorClass%>  col-md-14"
                                             style="background-color: <%=UIConstants.primaryModalCardHeaderBackground%>; width: 520px;">
                                            <div class="modalClose">
                                                <i class="fa fa-times" data-dismiss="modal" aria-label="Close"></i>
                                            </div>
                                            <div class="card-header card-header-<%=UIConstants.primaryColorClass%> card-header-icon"
                                                 style="height:75px">
                                                <div class="card-icon">
                                                    <i class="material-icons">dashboard</i>
                                                </div>
                                                <h4 class="card-title <%=UIConstants.primaryTextColorClass%>"
                                                    style="font-family: <%=UIConstants.primaryFontFamily%>; font-weight: bold"
                                                    id="modalHeading">Search Report</h4>
                                                <p class="card-category">Enter Information</p>
                                            </div>
                                            <div class="card-body conversionCard"
                                                 style="font-family: <%=UIConstants.primaryFilterFontFamilyTable%>; font-weight: bold; width: 520px; background-color: <%=UIConstants.primaryCardModalBackground%>">
                                                <br>
                                                <div id="modalForm">
                                                    <form id="formSubmit">

                                                        <div class="row" style="margin-top:-10px">
                                                            <div class="col-sm-6" style="margin-top: 5px">
                                                                <label class="<%=UIConstants.primaryTextColorClass%>"
                                                                       style="font-family: <%=UIConstants.primaryFontFamily%>; font-weight: bold; margin-top: -3px; position: absolute">Select
                                                                    Offers </label> <i class="material-icons"
                                                                                       style="margin-top: -5px; margin-left: 115px; position: absolute;">person</i>&nbsp;
                                                            </div>
                                                            <div class="col-sm-6 conversionSelectBox"
                                                                 style="margin-left:-105px;margin-top:-7px">
                                                                <select multiple id="offerListAjax"
                                                                        style="width:328px"></select>
                                                            </div>
                                                        </div>
                                                        <div class="row" style="margin-top:10px">
                                                            <div class="col-sm-6" style="margin-top: 5px">
                                                                <label class="<%=UIConstants.primaryTextColorClass%>"
                                                                       style="font-family: <%=UIConstants.primaryFontFamily%>; font-weight: bold; margin-top:-1px; position: absolute">Select
                                                                    Publishers </label> <i class="material-icons"
                                                                                           style="margin-top: -5px; margin-left: 115px; position: absolute;">person</i>&nbsp;
                                                            </div>
                                                            <div class="col-sm-6 conversionSelectBox"
                                                                 style="margin-left:-105px;margin-top:-7px">
                                                                <select multiple id="publisherListAjax"
                                                                        style="width:328px"></select>
                                                            </div>
                                                        </div>
                                                        <div class="row" style="margin-top:10px">
                                                            <div class="col-sm-6" style="margin-top: 5px">
                                                                <label class="<%=UIConstants.primaryTextColorClass%>"
                                                                       style="font-family: <%=UIConstants.primaryFontFamily%>; font-weight: bold; margin-top: -3px; position: absolute">Select
                                                                    Advertisers </label> <i class="material-icons"
                                                                                            style="margin-top: -5px; margin-left: 115px; position: absolute;">person</i>&nbsp;
                                                            </div>
                                                            <div class="col-sm-6 conversionSelectBox"
                                                                 style="margin-left:-105px;margin-top:-7px">
                                                                <select multiple id="advertiserListAjax"
                                                                        style="width:328px"></select>
                                                            </div>
                                                        </div>
                                                        <div class="row" style="margin-top:15px">
                                                            <div class="col-sm-6" style="margin-top: 0px">
                                                                <label class="<%=UIConstants.primaryTextColorClass%>"
                                                                       style="font-family: <%=UIConstants.primaryFontFamily%>; font-weight: bold; margin-top:-1px; position: absolute">Select
                                                                    Status </label> <i class="material-icons"
                                                                                       style="margin-top: -5px; margin-left: 115px; position: absolute;">person</i>&nbsp;
                                                            </div>
                                                            <div class="col-sm-6 conversionSelectBox"
                                                                 style="margin-left:-105px;margin-top:-13px">
                                                                <select class="multipleSelect" multiple
                                                                        id="conversionStatus"
                                                                        data-style="btn btn-primary btn-round"
                                                                        name="conversionStatus" data-width="330px">
                                                                    <c:forEach var="conversionStatus"
                                                                               items="${conversionStatus}">
                                                                        <option value="${conversionStatus}">${conversionStatus}</option>
                                                                    </c:forEach>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="row" style="margin-top:-15px">
                                                            <div class="col-sm-6" style="margin-top:30px">
                                                                <label class="<%=UIConstants.primaryTextColorClass%>"
                                                                       style="font-family: <%=UIConstants.primaryFontFamily%>; font-weight: bold; margin-top:-1px;position:absolute">Select
                                                                    Columns </label> <i class="fa fa-table"
                                                                                        style="margin-left:120px"></i>
                                                            </div>
                                                            <div class="col-sm-6 dropdown bootstrap-select show-tick <%=UIConstants.primaryTextColorClass%> conversionSelectBox"
                                                                 style="margin-top: 21px; margin-left: -105px">
                                                                <select class="multipleSelect"
                                                                        data-style="btn btn-warning"
                                                                        multiple="" title="Select Column" data-size="7"
                                                                        tabindex="-98" id="extSelect"
                                                                        data-width="330px">
                                                                    <!-- selected="selected" -->
                                                                    <option value="Offer Id" selected>Offer Id</option>
                                                                    <option value="Offer Name" selected>offer Name
                                                                    </option>
                                                                    <option value="Conversion Message">Conversion
                                                                        Message
                                                                    </option>
                                                                    <option value="Event Name">Event Name</option>
                                                                    <option value="Click Id" selected>Click Id</option>
                                                                    <option value="Pub Click Id">Pub Click Id</option>
                                                                    <option value="Gaid">Gaid</option>
                                                                    <option value="Idfa">Idfa</option>
                                                                    <option value="IP">IP</option>
                                                                    <option value="Agent">Agent</option>
                                                                    <option value="Click Message">Click Message</option>
                                                                    <!-- <option value="13" selected>isGross</option> -->
                                                                    <option value="SubAff">SubAff</option>
                                                                    <option value="Source">Source</option>
                                                                    <option value="S1">S1</option>
                                                                    <option value="S2">S2</option>
                                                                    <option value="S3">S3</option>
                                                                    <option value="S4">S4</option>
                                                                    <option value="S5">S5</option>
                                                                    <option value="Creation Date Time" selected>Creation
                                                                        Date Time
                                                                    </option>
                                                                    <option value="Conversion Status" selected>
                                                                        Conversion Status
                                                                    </option>
                                                                    <option value="Pub Id" selected>Publisher Id
                                                                    </option>
                                                                    <option value="Pub Name" selected>Publisher Name
                                                                    </option>
                                                                    <option value="adv Id">Advertiser Id</option>
                                                                    <option value="adv Name">Advertiser Name</option>
                                                                    <option value="Conversion Id" selected>Conversion
                                                                        Id
                                                                    </option>

                                                                </select>
                                                            </div>
                                                        </div>


                                                        <div class="filterBottomLine"
                                                             style="border-top:1px solid #423b3a;width:520px;margin-left:-20px;">

                                                            <div class="col-sm-12">
                                                                <button type="button" id="submitForm"
                                                                        class="btn btn-<%=UIConstants.primaryColorClass%> btn-round pull-right submitButton"
                                                                        data-dismiss="modal"
                                                                        style="margin-top:10px;margin-right:20px">Apply
                                                                </button>
                                                            </div>

                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </form>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <!-- </form> -->
                            </div>

                        </div>
                        <div class="row" style="position:absolute;margin-top:58px;margin-left:120px;z-index:5">
                            <div class="col-sm-2" style="">
                                <button class="filterButton btn btn-<%=UIConstants.primaryColorClass%> btn-round pull-right"
                                        id="approvedData"
                                        style="cursor: pointer;margin-top:-39px;height:28px;border-radius:5px;border:1.8px solid #fff">
                                    <i class="fa fa-check" aria-hidden="true"
                                       style="margin-left:0px;margin-top:-15px;width:auto;font-size:16px"><b
                                            style="margin-left:5px;">Approve</b>&nbsp;</i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body" id="tableBody" style="display: none">
                        </div>

                        <div class="table-responsive" style="width:98%;margin-left:10px;margin-top:-20px">

                            <table id="datatables"
                                   class="display nowrap <%=UIConstants.primaryTextColorClass%>"
                                   style="font-family: <%=UIConstants.primaryFontFamily%>;width:100%" cellspacing="0">

                                <thead>
                                <tr class="<%=UIConstants.primaryTextColorClass%>" style="font-weight: bold; ">
                                    <th>ID</th>
                                    <th class="no-sort" style="width:40px;position:relative">
                                        <div class="list checkLeft" style="margin-top:-13px;position: absolute">
                                            <label>
                                                <input type="checkbox" id="checkAll" name="check" value="TITLE"
                                                       style="margin-top:-6px;margin-left:15px"
                                                       cellpadding="10">
                                                <i></i>
                                            </label>
                                        </div>
                                    </th>
                                    <th style="font-weight:bold">offer Id</th>
                                    <th style="font-weight:bold">offer Name</th>
                                    <th style="font-weight:bold">Conversion Message</th>
                                    <th style="font-weight:bold">Event Name</th>
                                    <th style="font-weight:bold;width:120px">Click Id</th>
                                    <%--<th style="font-weight:bold">Tid</th>--%>
                                    <th style="font-weight:bold">Pub Click Id</th>
                                    <th style="font-weight:bold">Gaid</th>
                                    <th style="font-weight:bold">Idfa</th>
                                    <th style="font-weight:bold">IP</th>
                                    <%--<th style="font-weight:bold">Affid</th>--%>
                                    <th style="font-weight:bold">Agent</th>
                                    <th style="font-weight:bold">Click Message</th>
                                    <!-- <th style="font-weight:bold">isGross</th> -->
                                    <th style="font-weight:bold">SubAff</th>
                                    <th style="font-weight:bold">Source</th>
                                    <th style="font-weight:bold">S1</th>
                                    <th style="font-weight:bold">S2</th>
                                    <th style="font-weight:bold">S3</th>
                                    <th style="font-weight:bold">S4</th>
                                    <th style="font-weight:bold">S5</th>
                                    <th style="font-weight:bold">Creation Date Time</th>
                                    <th style="font-weight:bold">Conversion Status</th>
                                    <th style="font-weight:bold">Publisher Id</th>
                                    <th style="font-weight:bold">Publisher Name</th>
                                    <th style="font-weight:bold">Advertiser Id</th>
                                    <th style="font-weight:bold">Advertiser Name</th>
                                    <th style="font-weight:bold">Conversion Id</th>

                                </tr>
                                </thead>

                            </table>
                        </div>

                    </div>
                </div>
                <!-- end content-->
            </div>
            <!--  end card  -->
        </div>
        <!-- end col-md-12 -->
    </div>


    <!-- end row -->
</div>

<jsp:include page="adminFooter.jsp"/>
<!--   Core JS Files   -->
<!-- multiselect -->

<script src="../assets/js/core/popper.min.js"></script>
<script src="../assets/js/core/bootstrap-material-design.min.js"></script>
<script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!-- Plugin for the momentJs  -->
<script src="../assets/js/plugins/moment.min.js"></script>
<!--  Plugin for Sweet Alert -->
<script src="../assets/js/plugins/sweetalert2.js"></script>
<!-- Forms Validations Plugin -->
<script src="../assets/js/plugins/jquery.validate.min.js"></script>
<!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
<script src="../assets/js/plugins/jquery.bootstrap-wizard.js"></script>
<!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script src="../assets/js/plugins/bootstrap-selectpicker.js"></script>
<!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
<script src="../assets/js/plugins/bootstrap-datetimepicker.min.js"></script>
<!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
<script src="../assets/js/plugins/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../assets/js/jquery.spring-friendly.js"></script>
<!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
<script src="../assets/js/plugins/bootstrap-tagsinput.js"></script>
<!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="../assets/js/plugins/jasny-bootstrap.min.js"></script>
<!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
<script src="../assets/js/plugins/fullcalendar.min.js"></script>
<!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
<script src="../assets/js/plugins/jquery-jvectormap.js"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="../assets/js/plugins/nouislider.min.js"></script>
<!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
<!-- Library for adding elements -->
<script src="../assets/js/plugins/arrive.min.js"></script>
<script src="../assets/js/plugins/chartist.min.js"></script>
<!--  Notifications Plugin    -->
<script src="../assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="../assets/js/material-dashboard.js?v=2.1.0" type="text/javascript"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="../assets/demo/demo.js"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<script>
    $(document).ready(function () {
        $().ready(function () {
            $sidebar = $('.sidebar');

            $sidebar_img_container = $sidebar.find('.sidebar-background');

            $full_page = $('.full-page');

            $sidebar_responsive = $('body > .navbar-collapse');

            window_width = $(window).width();

            fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

            if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
                if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
                    $('.fixed-plugin .dropdown').addClass('open');
                }

            }

            $('.fixed-plugin a').click(function (event) {
                // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
                if ($(this).hasClass('switch-trigger')) {
                    if (event.stopPropagation) {
                        event.stopPropagation();
                    } else if (window.event) {
                        window.event.cancelBubble = true;
                    }
                }
            });

            $('.fixed-plugin .active-color span').click(function () {
                $full_page_background = $('.full-page-background');

                $(this).siblings().removeClass('active');
                $(this).addClass('active');

                var new_color = $(this).data('color');

                if ($sidebar.length != 0) {
                    $sidebar.attr('data-color', new_color);
                }

                if ($full_page.length != 0) {
                    $full_page.attr('filter-color', new_color);
                }

                if ($sidebar_responsive.length != 0) {
                    $sidebar_responsive.attr('data-color', new_color);
                }
            });

            $('.fixed-plugin .background-color .badge').click(function () {
                $(this).siblings().removeClass('active');
                $(this).addClass('active');

                var new_color = $(this).data('background-color');

                if ($sidebar.length != 0) {
                    $sidebar.attr('data-background-color', new_color);
                }
            });

            $('.fixed-plugin .img-holder').click(function () {
                $full_page_background = $('.full-page-background');

                $(this).parent('li').siblings().removeClass('active');
                $(this).parent('li').addClass('active');


                var new_image = $(this).find("img").attr('src');

                if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
                    $sidebar_img_container.fadeOut('fast', function () {
                        $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
                        $sidebar_img_container.fadeIn('fast');
                    });
                }

                if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
                    var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

                    $full_page_background.fadeOut('fast', function () {
                        $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
                        $full_page_background.fadeIn('fast');
                    });
                }

                if ($('.switch-sidebar-image input:checked').length == 0) {
                    var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
                    var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

                    $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
                    $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
                }

                if ($sidebar_responsive.length != 0) {
                    $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
                }
            });

            $('.switch-sidebar-image input').change(function () {
                $full_page_background = $('.full-page-background');

                $input = $(this);

                if ($input.is(':checked')) {
                    if ($sidebar_img_container.length != 0) {
                        $sidebar_img_container.fadeIn('fast');
                        $sidebar.attr('data-image', '#');
                    }

                    if ($full_page_background.length != 0) {
                        $full_page_background.fadeIn('fast');
                        $full_page.attr('data-image', '#');
                    }

                    background_image = true;
                } else {
                    if ($sidebar_img_container.length != 0) {
                        $sidebar.removeAttr('data-image');
                        $sidebar_img_container.fadeOut('fast');
                    }

                    if ($full_page_background.length != 0) {
                        $full_page.removeAttr('data-image', '#');
                        $full_page_background.fadeOut('fast');
                    }

                    background_image = false;
                }
            });

            $('.switch-sidebar-mini input').change(function () {
                $body = $('body');

                $input = $(this);

                if (md.misc.sidebar_mini_active == true) {
                    $('body').removeClass('sidebar-mini');
                    md.misc.sidebar_mini_active = false;

                    $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

                } else {

                    $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');

                    setTimeout(function () {
                        $('body').addClass('sidebar-mini');

                        md.misc.sidebar_mini_active = true;
                    }, 300);
                }

                // we simulate the window Resize so the charts will get updated in realtime.
                var simulateWindowResize = setInterval(function () {
                    window.dispatchEvent(new Event('resize'));
                }, 180);

                // we stop the simulation of Window Resize after the animations are completed
                setTimeout(function () {
                    clearInterval(simulateWindowResize);
                }, 1000);

            });
        });
    });
</script>

<script>
    var dateRange = '';
    var startDate = '';
    var filter = '';
    var filter1 = '';
    let offerFilter = '';
    let pubFilter = '';
    let advFilter = '';
    let conversionFilter = '';
    var offerId = 0;
    var search = {};
    var status = '';
    var id = 0;
    var table = $('#datatables').DataTable({
        ajax: "<%=BackendConstants.conversionReport%>",
        serverSide: true,
        processing: true,
        scrollX: true,
        columns: [
            {
                data: 'uuid',
                render: function (data) {
                    if (data != null)
                        id = data;
                    return '<div style="margin-left:50px"></div>' + id;

                }
            },
            {
                data: null,
                orderable: false,
                render: function (data, type, row) {
                    return '<div class="list columnCheckBox" style="margin-top:-14px;margin-left:15px;width:40px"><label><input  id="' + id + '" name="bulkActionCheckBoxes" type="checkbox" /><i></i></label></div>'
                }
            },
            {
                data: 'offerId',
                render: function (data) {
                    offerId = data;
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'offerName',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + '<a href="/adminConsole/viewoffer?id=' + offerId + '" target = "_blank" style="color:<%=UIConstants.primaryHyperLinkColor%>;font-weight:bold">' + data + ' </a>';
                }
            },
            {
                data: 'conversionMessage',
                render: function (data) {
                    return ' <div style="margin-left:180px"></div>' + data;
                }

            },
            {
                data: 'eventName',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },

            {
                data: 'clickUuid',
                render: function (data) {
                    return ' <div style="margin-left:140px"></div>' + '<a href="/adminConsole/getClickLogsDetails/' + data + '" target = "_blank" style="color:<%=UIConstants.primaryHyperLinkColor%>;font-weight:bold">' + data + ' </a>';
                }
            },

            {
                data: 'pubClickId',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'gaid',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'idfa',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            }, {
                data: 'ip',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            }, {
                data: 'agent',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            }, {
                data: 'clickMessage',
                render: function (data) {
                    return ' <div style="margin-left:140px"></div>' + data;
                }
            },
            // {
            //   data: 'isGross',
            // },
            {
                data: 'subAff', render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            }, {
                data: 'source',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 's1',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 's2',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 's3',
                render: function (data) {
                    return ' <div style="margin-left:80px"></div>' + data;
                }
            },
            {
                data: 's4',
                render: function (data) {
                    return ' <div style="margin-left:80px"></div>' + data;
                }
            },
            {
                data: 's5',
                render: function (data) {
                    return ' <div style="margin-left:80px"></div>' + data;
                }
            },
            {
                data: 'creationDateTime',
                render: function (data) {
                    if (data != null) {
                        var date = new Date(data);
                        const options = {weekday: 'long', year: 'numeric', month: 'long', day: 'numeric'};
                        return '<div style="margin-left:160px"></div>' + date.toLocaleString('en-US', options) + " " + date.toLocaleTimeString('en-US');
                    } else
                        return '<div style="margin-left:160px"></div>NA';
                }

            },
            {
                data: 'conversionStatus',
                render: function (data) {
                    return ' <div style="margin-left:180px"></div>' + data;
                }

            },
            {
                data: 'pubId',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'pubName',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'advId',
                render: function (data) {
                    offerId = data;
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'advName',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + '<a href="/adminConsole/viewoffer?id=' + offerId + '" target = "_blank" style="color:<%=UIConstants.primaryHyperLinkColor%>;font-weight:bold">' + data + ' </a>';
                }
            },
            {
                data: 'uuid',
                render: function (data, type, row) {
                    return '<a href="/adminConsole/getConversionDetails/' + data + '" target = "_blank" style="color:<%=UIConstants.primaryHyperLinkColor%>;font-weight:bold">' + data + ' </a>';
                }
            }
        ],
        "scrollCollapse": true,
        scrollY: 520,
        ordering: true
    });

    $(window).bind('resize', function (e) {
        if (window.RT) clearTimeout(window.RT);
        window.RT = setTimeout(function () {
            table.ajax.reload(); /* false to get page from cache */
        }, 250);
    });
    $('.dataTables_filter input').unbind();
    $('.dataTables_filter input').keyup(function (e) {
        if (e.keyCode === 13) /* if enter is pressed */ {
            table.search($(this).val()).draw();
        }
    });
    $('.sorting').click(function () {
        document.addEventListener("mouseover", exportAllData);
    })
    $(function () {
        var start = moment().subtract(6, 'days').startOf('day');
        var end = moment().endOf('day');

        function cb(start, end) {
            alert("dates " + start + " " + end);
            startDate = start;
            dateRange = document.getElementById("reportrangeinput").value = start.format('YYYY-MM-DD HH:mm:ss') + ' - ' + end.format('YYYY-MM-DD HH:mm:ss');
            document.getElementById("reportrangeinput").value = start.format('YYYY-MM-DD HH:mm:ss') + ' - ' + end.format('YYYY-MM-DD HH:mm:ss');
            /*console.log("dateRange: " + dateRange);*/
            table.column(20).search(dateRange).draw();
            alert("dates2 " + dateRange + " " + startDate + " " + start + " " + end);
            $('.btnDatatable').hide();
            $('#exportData').show();
        }

        $('#reportrange').daterangepicker({
            startDate: start,
            timePicker: true,
            timePicker24Hour: true,
            endDate: end,
            ranges: {
                'Today': [moment().startOf('day'), moment().endOf('day')],
                'Yesterday': [moment().subtract(1, 'days').startOf('day'), moment().subtract(1, 'days').endOf('day')],
                'Last 7 Days': [moment().subtract(6, 'days').startOf('day'), moment().endOf('day')],
                'Last 30 Days': [moment().subtract(29, 'days').startOf('day'), moment().endOf('day')],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        cb(start, end);

    });


    $('body').on('change', '#extSelect', function (e) {
        e.preventDefault();
        filter1 = '';
        var i, j;
        filter1 = $(this).val();
    });

    $(document).ready(function () {
        filter1 = '';
        filter1 = $("#extSelect").val();
    });


    table.columns().visible(false);
    table.columns([1, 2, 3, 6, 20, 21, 22, 23, 26]).visible(true);
    $('#submitForm').click(function () {
        var i, j;
        $("#exampleModalAd1").hide();
        /*console.log(filter1.length);*/
        if (filter1.length > 0) {
            alert("filter1.length");
            table.columns().visible(false);
            table.columns([1]).visible(true);
            for (i = 0; i < filter1.length; i++) {
                switch (filter1[i]) {
                    case 'Offer Id':
                        table.columns(2).visible(true);
                        break;
                    case 'Offer Name':
                        table.columns(3).visible(true);
                        break;
                    case 'Conversion Message':
                        table.columns(4).visible(true);
                        break;
                    case 'Event Name':
                        table.columns(5).visible(true);
                        break;
                    case 'Click Id':
                        table.columns(6).visible(true);
                        break;
                    case 'Pub Click Id':
                        table.columns(7).visible(true);
                        break;
                    case 'Gaid':
                        table.columns(8).visible(true);
                        break;
                    case 'Idfa':
                        table.columns(9).visible(true);
                        break;
                    case 'IP':
                        table.columns(10).visible(true);
                        break;
                    case 'Agent':
                        table.columns(11).visible(true);
                        break;
                    case 'Click Message':
                        table.columns(12).visible(true);
                        break;
                    case 'SubAff':
                        table.columns(13).visible(true);
                        break;
                    case 'Source':
                        table.columns(14).visible(true);
                        break;
                    case 'S1':
                        table.columns(15).visible(true);
                        break;
                    case 'S2':
                        table.columns(16).visible(true);
                        break;
                    case 'S3':
                        table.columns(17).visible(true);
                        break;
                    case 'S4':
                        table.columns(18).visible(true);
                        break;
                    case 'S5':
                        table.columns(19).visible(true);
                        break;
                    case 'Creation Date Time':
                        table.columns(20).visible(true);
                        break;
                    case 'Conversion Status':
                        table.columns(21).visible(true);
                        break;
                    case 'Pub Id':
                        table.columns(22).visible(true);
                        break;
                    case 'Pub Name':
                        table.columns(23).visible(true);
                        break;
                    case 'adv Id':
                        table.columns(24).visible(true);
                        break;
                    case 'adv Name':
                        table.columns(25).visible(true);
                        break;
                    case 'Conversion Id':
                        table.columns(26).visible(true);
                        break;
                }

            }
        }
        table.column(0).search(advFilter);
        table.column(21).search(offerFilter);
        table.column(4).search(pubFilter);
        table.column(5).search(conversionFilter).draw();
        $('.btnDatatable').hide();
        $('#exportData').show();
    });

    $('#offerFilter').click(function () {
        $("#exampleModalAd1").show();
    });


    $('select#publisherListAjax')
        .change(
            function () {
                pubFilter = '';
                $(
                    'select#publisherListAjax option:selected')
                    .each(function () {
                        pubFilter += $(this).val() + ",";
                    });
                pubFilter = pubFilter.substring(0, pubFilter.length - 1);
                /*console.log("pubFilter:" + pubFilter);*/
            });

    $('select#advertiserListAjax')
        .change(
            function () {
                advFilter = '';
                $(
                    'select#advertiserListAjax option:selected')
                    .each(function () {
                        advFilter += $(this).val() + ",";
                    });
                advFilter = advFilter.substring(0, advFilter.length - 1);
                /*console.log("pubFilter:" + pubFilter);*/
            });

    $('select#offerListAjax')
        .change(
            function () {
                offerFilter = '';
                $(
                    'select#offerListAjax option:selected')
                    .each(function () {
                        offerFilter += $(this).val() + ",";
                    });
                offerFilter = offerFilter.substring(0, offerFilter.length - 1);
            });


    $('select#conversionStatus')
        .change(
            function () {
                conversionFilter = '';
                $(
                    'select#conversionStatus option:selected')
                    .each(function () {
                        conversionFilter += $(this).val() + ",";
                    });
                conversionFilter = conversionFilter.substring(0, conversionFilter.length - 1);
            });


    $('#searchbox').on('keyup', function () {
        table.search(this.value).draw();
        /*console.log(this.value);*/
    });

</script>

<script>
    $(document).ready(function () {
        // initialise Datetimepicker and Sliders
        md.initFormExtendedDatetimepickers();
        if ($('.slider').length != 0) {
            md.initSliders();
        }
    });
</script>

<script>
    $('.multipleSelect').select2();

    $("#offerFilter").click(function () {
        offerLoad('#exampleModalAd', '', '/adminConsole/infiniteScrollerOffers', '#offerListAjax');
        offerLoad('#exampleModalAd', '', '/adminConsole/infiniteScrollerPublisher', '#publisherListAjax');
        offerLoad('#exampleModalAd', '', '/adminConsole/infiniteScrollerAdvertiser', '#advertiserListAjax');

    });

    function offerLoad(modalName, title, url, className) {
        var perPageRecord = 10;
        $(className).select2({
            dropdownParent: $(modalName),
            ajax: {
                url: url,
                type: 'GET',
                dataType: 'json',
                delay: 500,
                data: function (params) {
                    return {
                        search: params.term, // search term
                        page: params.page || 0,
                        size: perPageRecord//Per page records
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 0;
                    return {
                        results: data.results,
                        pagination: {
                            more: params.page < data.count_filtered - 1
                        }
                    };
                },
                cache: true
            },
            placeholder: title,
            minimumInputLength: 0
        });
    }

    $('#exportData').click(function () {
        Swal.fire({
            title: "Export Report Name",
            input: 'text',
            inputPlaceholder: 'Enter export report name',
            showCancelButton: true
        }).then((result) => {
            if (result.value) {
                exportAllData(result.value);
                $('.btnDatatable').hide();
                $('#exportData').show();
            }
        });
    });

    /*Exprot data functionality*/
    function exportAllData(exportName) {
        document.removeEventListener("mouseover", exportAllData);
        search['conversionStatus'] = offerFilter;
        search['conversionMessage'] = pubFilter;
        search['eventName'] = conversionFilter;
        search['creationDateTime'] = dateRange;
        // search['name'] = "report";
        $.ajax({
            type: "POST",
            url: "/adminConsole/exportConversionDataCount",
            "destroy": true,
            "processing": true,
            "serverSide": true,
            data: search,
            success: function (response) {
                if (response.includes("export clicks older than 7 days"))
                    exportNotification1(response)
                else
                    exportNotification(search, response, exportName)
            }
        });
    }

    function exportNotification(search, title, exportName) {
        Swal.fire({
            title: title,
            type: 'success',
            confirmButtonText: 'OK!',
            showCancelButton: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "/adminConsole/exportConversionData?columns=" + filter1 + "&exportName=" + exportName,
                    "destroy": true,
                    "processing": true,
                    "serverSide": true,
                    data: search,
                    success: function (response) {
                        /*location.reload();*/
                        setTimeout(function () {
                            window.open('/adminConsole/findAllCSVFile');
                            window.location.reload();
                        }, 1000);
                    }
                });
            }
        });
    }

    function exportNotification1(title) {
        Swal.fire({
            title: title,
            type: 'success',
            confirmButtonText: 'Create Ticket',
            showCancelButton: true
        }).then((result) => {
            if (result.value) {
                /*location.reload();*/
                setTimeout(function () {
                    window.open('/adminConsole/support');
                    window.location.reload();
                }, 1000);
            }
        });
    }

    /*function datatables1(dataTableVal) {
        var table1= $(dataTableVal).DataTable({
            "sPagingType": "full_number",
            "lengthMenu": [
                [5, 10, 20, -1],
                [5, 10, 20, "All"]
            ],
            scrollX:true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
            },
            dom: 'Blfrtip',
            buttons : [{
                extend: 'csv',text: '<i class="fa fa-download"></i>&nbsp;&nbsp;Export',title: 'Report',className: 'btnDatatable bg-transparent text-white',
                exportOptions: {
                    columns: ':visible', orthogonal: 'export'
                }
            }]
        });
        table1.columns().visible(false);
        table1.columns([3, 4, 5, 7, 8]).visible(true);
        if(filter1.length!=0) {
            table1.columns().visible(false);
            /!*search Table*!/
            for (i = 0; i < filter1.length; i++) {
                j = filter1[i];
                table1.columns(j).visible(true);
            }
        }

    }*/

    $("#checkAll").click(
        function () {
            $("[name='bulkActionCheckBoxes']").not(this).prop(
                'checked', this.checked);
        });
    var offerIdsToUpdate = [];

    //function to return checked offer ids
    function getOfferIds() {
        $("[name='bulkActionCheckBoxes']").each(function () {
            var $this = $(this);
            if ($this.is(":checked")) {
                offerIdsToUpdate.push($this.attr("id"));
            }
        });
        return offerIdsToUpdate;
    }

    $('#approvedData').click(function () {
        var offersId = getOfferIds();
        toggleSwitchAjax("approve", "approved", "/adminConsole/updateConversionStatus?id=" + offersId, " the conversions !");
    });

    function toggleSwitchAjax(toggleStatus, confirmMessage, url, msg) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You want to " + toggleStatus + msg,
            type: 'info',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then((result) => {
            if (result.value) {
                Swal.fire({
                    title: 'Successfully ' + confirmMessage,
                    type: 'success',
                    confirmButtonText: 'OK!'
                });
                $.ajax({
                    url: url,
                    contentType: 'json',
                    type: 'POST',
                    success: function (response) {
                        setTimeout(function () {
                            window.location.reload();
                        }, 1000);
                    },
                    complete: function () {
                        // Hide image container
                        $("#loaderBulk").hide();
                        $("#modalFormBulk").show();

                    },

                    error: function (jqXHR) {
                        var o = $.parseJSON(jqXHR.responseText);
                        window.open('/adminConsole/error?status=' + o.status + '&message=' + o.error, "_self")
                    }
                })
            }
        })
    }
</script>

</body>

</html>