<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page
        import="com.adstacks.attribution.bean.*,com.attribution.panel.constants.UIConstants,com.attribution.panel.enums.CountryEnum,com.attribution.panel.enums.OS" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Set" %>
<%@ page import="com.attribution.panel.bean.Partner" %>
<%@ page import="com.attribution.panel.bean.App" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="../../asset/img/logoAttribution.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>
        Configuration
    </title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport'/>
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- CSS Files -->
    <link href="../assets/css/material-dashboard.css?v=2.1.0" rel="stylesheet"/>
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="../assets/demo/demo.css" rel="stylesheet"/>
    <link rel="canonical" href="https://github.com/dbrekalo/fastselect/"/>
    <link href="../assets/css/style-sheet-adjar.css?v=2.1.0" rel="stylesheet"/>
    <!--  <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700,900&subset=latin,latin-ext' rel='stylesheet' type='text/css'> -->
    <!-- <link rel="stylesheet" href="https://rawgit.com/dbrekalo/attire/master/dist/css/build.min.css"> -->
    <script src="https://rawgit.com/dbrekalo/attire/master/dist/js/build.min.js"></script>
    <link rel="stylesheet" href="../assets/fastselect.min.css">
    <script src="../assets/fastselect.standalone.js"></script>
    <%--loader CSS Files--%>
    <link href="../assets/css/loader.css" rel="stylesheet"/>
    <%--    Tooltip textarea CSS --%>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote-lite.min.css" rel="stylesheet">
    <link href="../dist/css/select2.css" rel="stylesheet"/>
    <%--responsive css file--%>
    <link href="../assets/css/responsive-modal-pages.css" rel="stylesheet"/>
    <link href="../assets/css/summary.css" rel="stylesheet"/>


    <style>
        #nav-link {
            border: 1px solid<%=UIConstants.borderColor%>;
            height: 28px;
            font-size: 12px;
            font-weight: 600;
            padding: 2px 10px;
            margin-top: 10px;
        }

        .macroButton {
            padding: 7px 10px;
        }

        .no-border {
            border: 0;
            box-shadow: none; /* You may want to include this as bootstrap applies these styles too */
        }

        .dataTables_length .custom-select option {
            color: var(--textColor);
        }

        .dataTables_info {
            display: none;
            margin-top: -10px;
        }

        .cardButton {
            padding: 4px 10px;

        }

        #tableBody .dataTables_filter {
            margin-right: 40px !important;
        }

        @media screen and (min-width: 1366px) {
            .tab-contentTargeting {
                margin-left: 10px;
            }
        }

        @media screen and (min-width: 2000px) {
            .tab-contentTargeting {
                margin-left: 50px
            }
        }

        .progress {
            height: 5px;
        }

        #progressBar1 {
            color: <%=UIConstants.textColor%>;
            margin-top: -45px;
            margin-left: 2px;
        }

        #bannerList .dataTables_length {
            margin-left: 148px;
            margin-top: 6px;
        }


    </style>


</head>

<body class="text-white"
      style="background-color:<%=UIConstants.primaryBackgroundColor%>;font-family:<%=UIConstants.primaryTextColorClass%>"
      onload="/*allPublisherList(),approvePublisherList(),getEventList(),getBlockedList(),approvePublisherListAndApprovedCampaign(),getBannerList(),getTragetingList(),getCapsList(),getCategories(),getOfferPostback()*/">
<%--
<div class="se-pre-con"></div>
--%>


<div class="wrapper ">
    <jsp:include page="adminSidebar.jsp">
        <jsp:param value="active" name="configurations"/>
    </jsp:include>
    <div class="main-panel">
        <!-- Navbar -->
        <jsp:include page="adminHeader.jsp"/>
        <!-- End Navbar -->
        <div class="content">
            <div class="<%--container-fluid containerFluidSize--%>">
                <%--<div class="cardDiv">
                    <div class="cardSize">
                        <i class="material-icons iconSize" style="margin-left:5px">assignment</i>
                    </div>
                    <h2 class="cardHeading">Manage Offer Details</h2>
                </div>--%>
                <div class="<%--row--%>">

                    <div class="<%--col-md-12--%>">

                        <hr style="height: 5px;border-width:0;color:gray;background-color:#303654; margin-top: 2px;">

                        <div class="navOfferDetails"
                             style="margin-top:14px;background-color:<%=UIConstants.primaryCardBackground%>;height:50px;">
                            <ul class="nav nav-pills nav-pills-<%=UIConstants.primaryColorClass%>  <%=UIConstants.primaryTextColorClass%> justify-content-center"
                                role="tablist">

                                <li class="nav-item">
                                    <a class="nav-link active <%=UIConstants.textColor%>" data-toggle="tab"
                                       href="#general"
                                       role="tablist" id="nav-link" style="font-size: 15px;">
                                        GENERAL
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link <%=UIConstants.textColor%>" data-toggle="tab"
                                       href="#integratedPartners"
                                       role="tablist" id="nav-link" style="font-size: 15px;">
                                        INTEGRATED-PARTNERS
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link <%=UIConstants.textColor%>" data-toggle="tab"
                                       href="#approvedAgencies"
                                       role="tablist" id="nav-link" style="font-size: 15px;">
                                        APPROVED AGENCIES
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link <%=UIConstants.textColor%>" data-toggle="tab"
                                       href="#appSettings"
                                       role="tablist" id="nav-link" style="font-size: 15px;">
                                        APP SETTINGS
                                    </a>
                                </li>

                            </ul>
                        </div>

                        <hr style="height: 5px;border-width:0;color:gray;background-color:#303654; margin-top: 13px;">

                        <div class="tab-content tab-space">

                            <div class="tab-pane active" id="general">
                                <div class="row" style="width:100%;margin-left:0px"><br>
                                    <div class="card <%=UIConstants.primaryTextColorClass%>"
                                         Style="font-family:<%=UIConstants.primaryFontFamily%>;background-color:<%=UIConstants.primaryCardBackground%>;margin-top:-25px;">
                                        <div class="card-header card-header-warning card-header-text">

                                        </div>
                                        <div class="card-body <%=UIConstants.primaryTextColorClass%> infoTabCard"
                                             style="margin-left:0px;">

                                            <br>

                                            <form role="form" class="screen settings-form form-horizontal">

                                                <div class="collapsible-section"
                                                     data-testid="toggle_required_redirects"
                                                     style="border: 2px solid #303654">

                                                    <a id="yoo" class="nav-link" data-toggle="collapse"
                                                       href="#requiredRedirects"
                                                       style="font-size: 25px; color: #303654">
                                                        <i class="upside-down fa fa-chevron-right expand-icon"></i>
                                                        Required Redirects
                                                    </a>

                                                    <div class="collapse ${param.appsTab}" id="requiredRedirects"
                                                         data-testid="collapsible-section-body">
                                                        <div class="inner">
                                                            <div class="link-settings">

                                                                <div class="row">

                                                                    <div class="defauftURL"><img id="webURLimg"
                                                                                                 class="icon-stroke-settings"
                                                                                                 src="../assets/img/Blue_globe_icon.svg">
                                                                        <h3 id="h3URL">Default URL </h3>
                                                                    </div>

                                                                    <div class="formInput">
                                                                        <div class="row">
                                                                            <div class="formText">
                                                                                <div class="formText2">
                                                                                    <div class="formText3"
                                                                                         style="margin-top: 10px; width: 235px;">

                                                                                        <div class="arrowText">
                                                                                            Take the user here, if no
                                                                                            redirect is defined.
                                                                                        </div>

                                                                                        <svg version="1.1" width="90%"
                                                                                             height="51px" id="Layer_1"
                                                                                             xmlns="http://www.w3.org/2000/svg"
                                                                                             x="0px" y="0px"
                                                                                             viewBox="0 0 612 130">
                                                                                            <line fill="none"
                                                                                                  stroke="#666"
                                                                                                  stroke-width="8"
                                                                                                  stroke-linecap="round"
                                                                                                  stroke-miterlimit="10"
                                                                                                  x1="17.4" y1="76.9"
                                                                                                  x2="592.4"
                                                                                                  y2="76.9"></line>
                                                                                            <line fill="none"
                                                                                                  stroke="#666"
                                                                                                  stroke-width="8"
                                                                                                  stroke-linecap="round"
                                                                                                  stroke-miterlimit="10"
                                                                                                  x1="553.1" y1="34.5"
                                                                                                  x2="594.8"
                                                                                                  y2="76.1"></line>
                                                                                            <line fill="none"
                                                                                                  stroke="#666"
                                                                                                  stroke-width="8"
                                                                                                  stroke-linecap="round"
                                                                                                  stroke-miterlimit="10"
                                                                                                  x1="553.1" y1="115.1"
                                                                                                  x2="594.8"
                                                                                                  y2="76.9"></line>
                                                                                        </svg>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="formLabel">
                                                                                <div class="row">
                                                                                    <div class="input-control" style="    margin-left: 150px;
    margin-top: -15px;"
                                                                                         data-testid="default_url">
                                                                                        <div class=""
                                                                                             data-testid="title">
                                                                                            <div><label
                                                                                                    class="webLabelURL"
                                                                                                    style="color: black">Default
                                                                                                URL</label></div>
                                                                                            <div class="label__description"
                                                                                                 data-testid="description"
                                                                                                 style="font-size: 20px">
                                                                                                Your fallback URL for
                                                                                                mobile devices that do
                                                                                                not have a specified
                                                                                                redirect.
                                                                                            </div>
                                                                                        </div>
                                                                                        <input class="webLabelURL"
                                                                                               style="width: 100%; height: 40px; font-size: 18px;"
                                                                                               name="web_url"
                                                                                               placeholder=""
                                                                                               type="text"
                                                                                               data-testid="default_url-input"
                                                                                               value="https:"><span
                                                                                            class="input-control__footer h_d-f"
                                                                                            data-testid="default_url__footer"></span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                                <hr style="height: 3px;border-width:0;color:gray;background-color:lightgray; margin-top: 10px;">

                                                                <div class="row">

                                                                    <div class="defauftURL"><img id="webURLimg"
                                                                                                 class="icon-stroke-settings"
                                                                                                 src="../assets/img/android-svgrepo-com.svg">
                                                                        <h3 id="h3URL">Android
                                                                            Redirects </h3>

                                                                        <div class="checkbox androidCheckBox">
                                                                            <label><input type="checkbox"
                                                                                          id="android_app" value="true"
                                                                                          checked=""><span
                                                                                    style="margin-left: 10px; color: black; font-size: 15px;">I have an Android App</span></label>
                                                                        </div>

                                                                    </div>

                                                                    <div class="formInput">

                                                                        <div class="row">
                                                                            <div class="formText">
                                                                                <div class="formText2">
                                                                                    <div class="formText3"
                                                                                         style="margin-top: 25px; width: 235px; margin-left: 55px;">

                                                                                        <svg version="1.1" width="90%"
                                                                                             height="51px" id="Layer_1"
                                                                                             xmlns="http://www.w3.org/2000/svg"
                                                                                             x="0px" y="0px"
                                                                                             viewBox="0 0 612 130">
                                                                                            <line fill="none"
                                                                                                  stroke="#666"
                                                                                                  stroke-width="8"
                                                                                                  stroke-linecap="round"
                                                                                                  stroke-miterlimit="10"
                                                                                                  x1="17.4" y1="76.9"
                                                                                                  x2="592.4"
                                                                                                  y2="76.9"></line>
                                                                                            <line fill="none"
                                                                                                  stroke="#666"
                                                                                                  stroke-width="8"
                                                                                                  stroke-linecap="round"
                                                                                                  stroke-miterlimit="10"
                                                                                                  x1="553.1" y1="34.5"
                                                                                                  x2="594.8"
                                                                                                  y2="76.1"></line>
                                                                                            <line fill="none"
                                                                                                  stroke="#666"
                                                                                                  stroke-width="8"
                                                                                                  stroke-linecap="round"
                                                                                                  stroke-miterlimit="10"
                                                                                                  x1="553.1" y1="115.1"
                                                                                                  x2="594.8"
                                                                                                  y2="76.9"></line>
                                                                                        </svg>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="formLabel">
                                                                                <div class="row">
                                                                                    <div class="input-control"
                                                                                         style="margin-top: 10px;"
                                                                                         data-testid="default_url">
                                                                                        <div class=""
                                                                                             data-testid="title">
                                                                                            <div><label
                                                                                                    class="webLabelURL"
                                                                                                    style="color: black">Android
                                                                                                URIiii Scheme</label>
                                                                                            </div>
                                                                                        </div>
                                                                                        <input class="androidLabelURL"
                                                                                               style="width: 225%; height: 40px; font-size: 18px;"
                                                                                               name="web_url"
                                                                                               placeholder=""
                                                                                               type="text"
                                                                                               data-testid="default_url-input"
                                                                                               value="https:"><span
                                                                                            class="input-control__footer h_d-f"
                                                                                            data-testid="default_url__footer"></span>
                                                                                    </div>
                                                                                </div>

                                                                                <div>
                                                                                    <div class="row">

                                                                                        <div class="h_mb-20 androidLineURL">
                                                                                            <div class="text-center"
                                                                                                 style="height: 105px;">
                                                                                                <svg version="1.1"
                                                                                                     width="35px"
                                                                                                     height="100%"
                                                                                                     id="Layer_1"
                                                                                                     xmlns="http://www.w3.org/2000/svg"
                                                                                                     x="0px" y="0px"
                                                                                                     viewBox="0 0 85.6 294">
                                                                                                    <line fill="none"
                                                                                                          stroke="#666"
                                                                                                          stroke-width="5"
                                                                                                          stroke-linecap="round"
                                                                                                          stroke-miterlimit="10"
                                                                                                          x1="40.7"
                                                                                                          y1="2.5"
                                                                                                          x2="40.7"
                                                                                                          y2="288.7"></line>
                                                                                                    <line fill="none"
                                                                                                          stroke="#666"
                                                                                                          stroke-width="5"
                                                                                                          stroke-linecap="round"
                                                                                                          stroke-miterlimit="10"
                                                                                                          x1="83.1"
                                                                                                          y1="249.4"
                                                                                                          x2="41.4"
                                                                                                          y2="291"></line>
                                                                                                    <line fill="none"
                                                                                                          stroke="#666"
                                                                                                          stroke-width="5"
                                                                                                          stroke-linecap="round"
                                                                                                          stroke-miterlimit="10"
                                                                                                          x1="2.5"
                                                                                                          y1="249.4"
                                                                                                          x2="40.7"
                                                                                                          y2="291"></line>
                                                                                                </svg>
                                                                                            </div>
                                                                                            <div class="arrow-text-vertical text-center"
                                                                                                 style="font-size:20px;margin-left:-20px;">
                                                                                                If the above fails, fall
                                                                                                back to the app store or
                                                                                                custom link to download
                                                                                                the app.
                                                                                            </div>
                                                                                        </div>

                                                                                    </div>

                                                                                    <br><br>

                                                                                    <div class="row <%--h_mt-30--%>">
                                                                                        <div class="form-group">
                                                                                            <div>
                                                                                                <div><input type="radio"
                                                                                                            name=""
                                                                                                            id="autocomplete_android"
                                                                                                            style="margin-left:150px;margin-top:20px;"><label
                                                                                                        class="control-label h_mx-10"
                                                                                                        for="autocomplete_android"
                                                                                                        style="color:black;margin-left:5px;font-size:18px">Google
                                                                                                    Play
                                                                                                    Search</label>
                                                                                                    <input
                                                                                                            type="radio"
                                                                                                            name=""
                                                                                                            id="autocomplete_android_custom"
                                                                                                            value="1"
                                                                                                            checked=""
                                                                                                            style="margin-left:20px"><label
                                                                                                            class="control-label h_mx-10"
                                                                                                            for="autocomplete_android_custom"
                                                                                                            style="color:black;margin-left:5px;font-size:18px">Custom
                                                                                                        URL</label>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div>
                                                                                                <div>
                                                                                                    <div class="input-control"
                                                                                                         data-testid="android-url-fallback"
                                                                                                         style="margin-top:10px;margin-left:150px;">
                                                                                                        <input class="input-control__input h_bdrs-2px h_w-100"
                                                                                                               name="android_url"
                                                                                                               placeholder="A URL to fall back to when the app is not installed"
                                                                                                               type="text"
                                                                                                               data-testid="android-url-fallback-input"
                                                                                                               value=""
                                                                                                               style="width: 620px !important; height: 40px; font-size: 20px;"><span
                                                                                                            class="input-control__footer h_d-f"
                                                                                                            data-testid="android-url-fallback__footer"></span>
                                                                                                    </div>
                                                                                                    <div class="input-control"
                                                                                                         data-testid="android_package_name"
                                                                                                         style="margin-top:10px;margin-left:150px;width:222%">
                                                                                                        <input class="input-control__input h_bdrs-2px h_w-100"
                                                                                                               placeholder="Android Package Name"
                                                                                                               type="text"
                                                                                                               data-testid="android_package_name-input"
                                                                                                               value="com.zomogames.SurvivalGames"><span
                                                                                                            class="input-control__footer h_d-f"
                                                                                                            data-testid="android_package_name__footer"><span
                                                                                                            class="input-control__note h_ta-start"><div
                                                                                                            class="h_c-warning"
                                                                                                            style="color:red; margin-top: 5px">For redirects to work on Chrome, please enter your app's package name (e.g. com.company.appname)</div></span></span>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                </div>

                                                                            </div>
                                                                        </div>

                                                                    </div>

                                                                </div>

                                                                <div>
                                                                    <div class="row">
                                                                        <div class="col-lg-2">
                                                                            <div data-testid="app_links_enabled-checkbox"
                                                                                 class="check_box">
                                                                                <div><input class="check_box--input"
                                                                                            style="margin-left: 34px; background-size: 100px;"
                                                                                            id="app_links_enabled-checkbox"
                                                                                            name="check" type="checkbox"
                                                                                            checked=""><label
                                                                                        style="color:black; margin-left: 20px; font-size: 15px"
                                                                                        ;
                                                                                        class="check_box--label"
                                                                                        for="app_links_enabled-checkbox"><span
                                                                                        class="check_box--label-text"><span
                                                                                        class="check_box--popover-label">Enable App Links<%--<div
                                                                                        class="tooltip-info-icon-container"
                                                                                        data-tooltipped=""
                                                                                        aria-describedby="tippy-tooltip-57"
                                                                                        style="display: inline;"><i
                                                                                        class="material-icons tooltip-icon h_va-u" style="margin-top: 10px;">info_outline</i></div>--%></span></span></label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-10">
                                                                            <div class="row">
                                                                                <div class="col-xs-offset-3 col-xs-9 h_pstart-40">
                                                                                    <div class="row">
                                                                                        <div class="input-control"
                                                                                             data-testid="sha256_cert_fingerprints">
                                                                                            <div class="h_mb-5"
                                                                                                 data-testid="title">
                                                                                                <div><label
                                                                                                        class="h_mend-5 h_mb-0"
                                                                                                        style="margin-left:420px;color:black;margin-top:30px;font-size:20px">SHA256
                                                                                                    Cert
                                                                                                    Fingerprints</label>
                                                                                                </div>
                                                                                            </div>
                                                                                            <input class="input-control__input "
                                                                                                   type="text"
                                                                                                   style="margin-left:420px;color:black;margin-top:5px;font-size:20px; width: 625px; height: 40px; "
                                                                                                   data-testid="sha256_cert_fingerprints-input"
                                                                                                   value="3C:9C:FF:EA:93:66:2C:B0:86:C6:AC:D3:6F:DC:90:B7:38:B8:C5:9D:8E:75:64:E9:40:A6:B1:CC:65:9F:26:F4">
                                                                                            <span
                                                                                                    class="input-control__footer"
                                                                                                    data-testid="sha256_cert_fingerprints__footer"
                                                                                                    style="margin-left:420px;width:600px"></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <br>

                                                                <hr style="height: 3px;border-width:0;color:gray;background-color:lightgray; margin-top: 10px;">

                                                                <br>

                                                                <div class="row">
                                                                    <div class="">

                                                                        <div class="defauftURL"><img id="webURLimg"
                                                                                                     class="icon-stroke-settings"
                                                                                                     src="../assets/img/ic_apple.svg">
                                                                            <h3 id="h3URL">iOS Redirects </h3>
                                                                        </div>

                                                                        <div class="checkbox"
                                                                             style="margin-left: 50px; margin-top: 25px;">
                                                                            <label><input type="checkbox" id="ios_app"
                                                                                          value=""><span
                                                                                    style="margin-left: 10px;">I have an iOS App</span></label>
                                                                        </div>

                                                                    </div>
                                                                    <div class="">
                                                                        <div class="row">
                                                                            <div class="text-center">
                                                                                <div class="">
                                                                                    <div style="margin-top: 10px; width: 200px; margin-left:50px;">

                                                                                        <div class="arrow-text">Take
                                                                                            users to this URL.
                                                                                        </div>

                                                                                        <svg version="1.1" width="90%"
                                                                                             height="51px" id="Layer_1"
                                                                                             xmlns="http://www.w3.org/2000/svg"
                                                                                             x="0px" y="0px"
                                                                                             viewBox="0 0 612 130">
                                                                                            <line fill="none"
                                                                                                  stroke="#666"
                                                                                                  stroke-width="5"
                                                                                                  stroke-linecap="round"
                                                                                                  stroke-miterlimit="10"
                                                                                                  x1="17.4" y1="76.9"
                                                                                                  x2="592.4"
                                                                                                  y2="76.9"></line>
                                                                                            <line fill="none"
                                                                                                  stroke="#666"
                                                                                                  stroke-width="5"
                                                                                                  stroke-linecap="round"
                                                                                                  stroke-miterlimit="10"
                                                                                                  x1="553.1" y1="34.5"
                                                                                                  x2="594.8"
                                                                                                  y2="76.1"></line>
                                                                                            <line fill="none"
                                                                                                  stroke="#666"
                                                                                                  stroke-width="5"
                                                                                                  stroke-linecap="round"
                                                                                                  stroke-miterlimit="10"
                                                                                                  x1="553.1" y1="115.1"
                                                                                                  x2="594.8"
                                                                                                  y2="76.9"></line>
                                                                                        </svg>

                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="">
                                                                                <div class="row">
                                                                                    <div class="input-control"
                                                                                         data-testid="ios_url">
                                                                                        <div class=""
                                                                                             data-testid="title">
                                                                                            <div><label
                                                                                                    style="color:black; font-size: 18px;">iOS
                                                                                                URL</label></div>
                                                                                            <div class="label__description"
                                                                                                 data-testid="description">
                                                                                                <div style="width: 80%;">
                                                                                                    If blank, users
                                                                                                    will be redirected
                                                                                                    to the Default URL:
                                                                                                    <a class="standard-link standard-link__active-link undefined"
                                                                                                       data-testid="default-url-link"
                                                                                                       href="https://play.google.com/store/apps/details?id=com.zomogames.SurvivalGames&amp;hl=en"
                                                                                                       target="_blank"
                                                                                                       rel="noopener noreferrer">https://play.google.com/store/apps/details?id=com.zomogames.SurvivalGames&amp;hl=en</a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <input class="webLabelURL"
                                                                                               style="width: 80%; height: 40px; font-size: 18px;"
                                                                                               name="web_url"
                                                                                               placeholder=""
                                                                                               type="text"
                                                                                               data-testid="default_url-input"
                                                                                               value="https:"><span
                                                                                            class="input-control__footer h_d-f"
                                                                                            data-testid="default_url__footer"></span>
                                                                                    </div>
                                                                                </div>

                                                                                <br>
                                                                                <br>

                                                                                <div class="row">
                                                                                    <div class="input-control"
                                                                                         data-testid="ios_team_id">
                                                                                        <div class=""
                                                                                             data-testid="title">
                                                                                            <div><label
                                                                                                    class=""
                                                                                                    style="color: black; font-size: 18px">Apple
                                                                                                App Prefix</label>
                                                                                                <div class="tooltip-info-icon-container"
                                                                                                     data-tooltipped=""
                                                                                                     aria-describedby="tippy-tooltip-59"
                                                                                                     style="display: inline;">
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <input class="input-control__input"
                                                                                               style="width: 200%; height: 40px; font-size: 18px;"
                                                                                               name="ios_team_id"
                                                                                               type="text"
                                                                                               data-testid="ios_team_id-input"
                                                                                               value=""><span
                                                                                            class="input-control__footer h_d-f"
                                                                                            data-testid="ios_team_id__footer"></span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <br>
                                                                <br>

                                                                <hr style="height: 3px;border-width:0;color:gray;background-color:lightgray; margin-top: 10px;">


                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <br>

                                                <div class="collapsible-section" data-testid="toggle_social_media"
                                                     style="border: 2px solid #303654">

                                                    <a id="yoo1" class="nav-link" data-toggle="collapse"
                                                       href="#socialMediaPreview"
                                                       style="font-size: 25px; color: #303654">
                                                        <i class="upside-down fa fa-chevron-right expand-icon"></i>
                                                        Social Media Preview
                                                    </a>

                                                    <div class="collapse ${param.appsTab}" id="socialMediaPreview"
                                                         data-testid="collapsible-section-body">
                                                        <div class="inner">

                                                            <div class="tipbox-container"
                                                                 style="background-color: lightgray; border: 2px solid #303654; margin-left: 16px; margin-right: 16px;  margin-top: 10px;">
                                                                <div class="tipbox"><span class="tipbox-text"
                                                                                          style="font-size: 18px;">Use these to customize how the link to your install page will look like when shared or re-shared on social media.</span>
                                                                </div>
                                                            </div>

                                                            <div class="link-settings">
                                                                <div class="row">
                                                                    <div class="col-lg-4">

                                                                        <div class=""
                                                                             style="margin-left: 100px; margin-top: 40px;">
                                                                            <div class="form-group">
                                                                                <label for="socialMediaTitle">Link Title
                                                                                    *</label>
                                                                                <input id="socialMediaTitle" type="text"
                                                                                       name="socialMediaTitle"
                                                                                       class="form-control">
                                                                                <div class="help-block with-errors"></div>
                                                                            </div>
                                                                        </div>

                                                                        <%-- <div class="input-control"
                                                                              data-testid="og_title">
                                                                             <div class="titlee" data-testid="title">
                                                                                 <div><label class="">Link
                                                                                     Title</label></div>
                                                                             </div>
                                                                             <input class="input-control__input h_bdrs-2px h_w-100"
                                                                                    type="text"
                                                                                    data-testid="og_title-input"
                                                                                    value=""><span
                                                                                 class="input-control__footer h_d-f"
                                                                                 data-testid="og_title__footer"></span>
                                                                         </div>--%>

                                                                        <div class=""
                                                                             style="margin-left: 100px; margin-top: 40px;">
                                                                            <div class="form-group">
                                                                                <label for="socialMediaDescription">Description
                                                                                    *</label>
                                                                                <input id="socialMediaDescription"
                                                                                       type="text"
                                                                                       name="socialMediaDescription"
                                                                                       class="form-control">
                                                                                <div class="help-block with-errors"></div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="input-group"
                                                                             style="margin-left: 100px; margin-top: 40px; ">
                                                                            <div class="input-group-prepend">
                                                                                <span class="input-group-text"
                                                                                      id="socialMediaImage"
                                                                                      style="border: 1px solid black; font: bold; background-color: grey; color: black; ">Upload</span>
                                                                            </div>
                                                                            <div class="custom-file">
                                                                                <input type="file"
                                                                                       class="custom-file-input"
                                                                                       id="inputGroupFile01"
                                                                                       name="socialMediaImage"
                                                                                       aria-describedby="socialMediaImage">
                                                                                <label class="custom-file-label"
                                                                                       for="inputGroupFile01"
                                                                                       style="border: 1px solid black;">Choose
                                                                                    file</label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <%--<div class="col-md-7">
                                                                        <div class="og-container undefined">
                                                                            <div class="og-wrapper2">
                                                                                <div class="og-user clearfix">
                                                                                    <div class="og-user__wrapper">
                                                                                        <div class="og-user__photo"></div>
                                                                                        <div class="og-user__info clearfix">
                                                                                            <div class="facebook-color brand">
                                                                                                <span>Your brand</span>
                                                                                            </div>
                                                                                            <div class="attribute">1 hr
                                                                                                ·
                                                                                                <i class="material-icons public-icon">public</i>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="og-user__text">Check out
                                                                                    this great app <a
                                                                                            class="standard-link standard-link__active-link facebook-color"
                                                                                            data-testid="preview-url"
                                                                                            href="https://62o21.test-app.link/sample-link"
                                                                                            target="_blank"
                                                                                            rel="noopener noreferrer">https://62o21.test-app.link/sample-link</a>
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                <a class="standard-link standard-link__active-link og-wrapper"
                                                                                   data-testid="og-wrapper"
                                                                                   href="https://62o21.test-app.link/sample-link"
                                                                                   target="_blank"
                                                                                   rel="noopener noreferrer">
                                                                                    <div class="og-preview">Preview
                                                                                    </div>
                                                                                    <div class="og-image"
                                                                                         data-testid="preview-og-image"></div>
                                                                                    <div class="og-text">
                                                                                        <div class="og-title h_ell">This
                                                                                            is a title example
                                                                                        </div>
                                                                                        <div class="og-description h_line-clamp">
                                                                                            This is a description
                                                                                            example
                                                                                        </div>
                                                                                        <div class="og-url">
                                                                                            https://www.domain-name.com
                                                                                        </div>
                                                                                    </div>
                                                                                </a></div>
                                                                            <div class="og-action"><span
                                                                                    class="facebook-color share"><i
                                                                                    class="material-icons share-icon">thumb_up</i> Like </span><span
                                                                                    class="facebook-color share"><i
                                                                                    class="material-icons share-icon">mode_comment</i> Comment </span><span
                                                                                    class="facebook-color share"><i
                                                                                    class="material-icons share-icon reply">reply</i> Share </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>--%>
                                                                </div>
                                                                <br>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                <br>


                                                <div class="h_mt-20">
                                                    <button type="button" disabled=""
                                                            class="button button-type-primary button-size-standard"
                                                            data-testid="btn-link-settings__save">Save
                                                    </button>
                                                </div>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane justify-content-center" id="integratedPartners">
                                <div class="row" style="width:100%;margin-left:0px"><br>
                                    <div class="card <%=UIConstants.primaryTextColorClass%>"
                                         Style="font-family:<%=UIConstants.primaryFontFamily%>;background-color:<%=UIConstants.primaryCardBackground%>;margin-top:-25px;">
                                        <div class="card-header card-header-warning card-header-text">

                                        </div>

                                        <div class="card-body <%=UIConstants.primaryTextColorClass%> infoTabCard"
                                             style="background-color:<%=UIConstants.primaryCardBackground%>">
                                            <div class="material-datatables">
                                                <table id="datatable"
                                                       class="dataTable display nowrap <%=UIConstants.primaryTextColorClass%>"
                                                       cellspacing="0" style="width:100%">
                                                    <thead class=" <%=UIConstants.primaryTextColorClass%>">
                                                    <tr>
                                                        <th style="width:50px">
                                                            ID
                                                        </th>
                                                        <th>
                                                            Company
                                                        </th>
                                                        <th style="width:200px">
                                                            Publisher Name
                                                        </th>
                                                        <th>
                                                            Email
                                                        </th>
                                                        <th>
                                                            Status
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <% List<Partner> param = (List<Partner>) request.getAttribute("partner");
                                                        int i = 1;
                                                        for (Partner pub : param) {%>

                                                    <tr class="<%=UIConstants.primaryTextColorClass%>"
                                                        style="background-color:<%=UIConstants.primaryCardBackground%>">
                                                        <td>
                                                            <div style="margin-left:60px"></div>
                                                            <%=  pub.getId()%>
                                                        </td>
                                                        <td>
                                                            <div style="margin-left:100px"></div>
                                                            <a href="/adminConsole/getPublisherDetails/<%=  pub.getId()%> "
                                                               target="_blank"
                                                               style="color:<%=UIConstants.primaryHyperLinkColor%>;font-weight:bold"><%= pub.getCompany()%>
                                                            </a>
                                                        </td>
                                                        <td>
                                                            <div style="margin-left:160px"></div>
                                                            <%=pub.getName()%> <%= pub.getName()%>

                                                        </td>
                                                        <td>
                                                            <div style="margin-left:80px"></div>
                                                            <%=  pub.getEmail()%>
                                                        </td>
                                                        <td>
                                                            <div style="display:none"><%= pub.getStatus() %>
                                                            </div>
                                                            <form method="POST" action="/adminConsole/savepub">

                                                                <div class="togglebutton">
                                                                    <label> <input type="checkbox" name="status"
                                                                                   id="toggle<%=i-1%>" <%=pub.getStatus() ? "checked" : "" %>
                                                                                   name="toggle1"
                                                                                   onclick="checkForm(<%=pub.getId() %>,<%=i-1%>)"/>
                                                                        <span class="toggle btn-<%=UIConstants.primaryColorClass%>"></span>

                                                                    </label>
                                                                </div>
                                                            </form>
                                                        </td>
                                                    </tr>
                                                    <%
                                                            i++;
                                                        }
                                                    %>

                                                    </tbody>
                                                </table>
                                            </div>

                                        </div>

                                        <%--<div class="card-body <%=UIConstants.primaryTextColorClass%> infoTabCard"
                                             style="margin-left:0px;"><br>

                                            <!-- End Navbar -->

                                        </div>--%>
                                    </div>
                                </div>

                            </div>

                            <div class="tab-pane justify-content-center" id="approvedAgencies">
                                <div class="row" style="width:100%;margin-left:0px"><br>
                                    <div class="card <%=UIConstants.primaryTextColorClass%>"
                                         Style="font-family:<%=UIConstants.primaryFontFamily%>;background-color:<%=UIConstants.primaryCardBackground%>;margin-top:-25px;">
                                        <div class="card-header card-header-warning card-header-text">

                                        </div>

                                        <div class="card-body <%=UIConstants.primaryTextColorClass%> infoTabCard"
                                             style="margin-left:0px;">


                                            <% List<App> app = (List<App>) request.getAttribute("app"); %>

                                            <h3>Approved Agencies</h3>

                                            <br>

                                            <div class="row"
                                                 style="margin-left:20px;margin-top:-8px;width:100%;!important;">

                                                <label class="bmd-label-floating <%=UIConstants.primaryTextColorClass%> <%=UIConstants.primaryTextColorClass%>"
                                                       style="font-size: 20px; " for="selectEvent">Select
                                                    Agencie:
                                                </label>
                                                <select id="selectAgencie"
                                                        style="width: 240px; height: 40px; margin-left: 15px; "
                                                        name="selectEvent">
                                                    <%for (App apps : app) { %>
                                                    <option value="<%=apps.getName()%>"><%=apps.getName()%>
                                                    </option>
                                                    <%} %>
                                                </select>

                                            </div>

                                            <br>
                                            <br>
                                            <br>

                                            <div class="table-responsive" style="margin-top:-26px">
                                                <table id="datatables" class="display" width="100%"
                                                       style="width:100%;background-color:<%=UIConstants.primaryCardBackground%>">
                                                    <thead>
                                                    <tr class="<%=UIConstants.primaryTextColorClass%>"
                                                        style="font-weight: bold;">
                                                        <th>Agencie Name</th>
                                                        <th>Agencie Email</th>
                                                    </tr>
                                                </table>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane justify-content-center" id="appSettings">
                                <div class="row" style="width:100%;margin-left:0px"><br>
                                    <div class="card <%=UIConstants.primaryTextColorClass%>"
                                         Style="font-family:<%=UIConstants.primaryFontFamily%>;background-color:<%=UIConstants.primaryCardBackground%>;margin-top:-25px;">
                                        <div class="card-header card-header-warning card-header-text">

                                        </div>
                                        <div class="card-body <%=UIConstants.primaryTextColorClass%> infoTabCard"
                                             style="margin-left:0px;">

                                            <br>

                                            <div class="section">

                                                <form>

                                                    <div class="row">
                                                        <div style="width: 50%; margin-left: 20px;">
                                                            <h2>SDK AUTHENTICATION</h2>
                                                            <p style="font-size: 20px;">Key to authenticate events sent
                                                                by the app or S2S to the
                                                                platform.</p>
                                                        </div>
                                                        <div style="width: 51%; margin-left: 20px;">
                                                            <input type="text" value="StackExchange WordPress" readonly
                                                                   id="myInput" style="width: 270px; height: 40px;">
                                                            <button style="height: 40px; width: 80px"
                                                                    onclick="copyText()">Copy text
                                                            </button>
                                                        </div>

                                                    </div>

                                                    <br>

                                                    <hr style="height: 3px;border-width:0;color:gray;background-color:lightgray; margin-top: 10px;">

                                                    <br>

                                                    <div class="row">

                                                        <div style="width: 90%; margin-left: 20px;">
                                                            <h2>Analytics</h2>
                                                            <h3>Define loyal user trigger</h3>
                                                            <p style="font-size: 20px;">A loyal user, by default, is a
                                                                user who opens the app 3 or more times. </p>
                                                        </div>

                                                        <div style="width: 90%; margin-left: 20px; font-size: 20px;">

                                                            <label class="bmd-label-floating <%=UIConstants.primaryTextColorClass%> <%=UIConstants.primaryTextColorClass%>"
                                                                   style="font-size: 20px; " for="selectEvent">Select
                                                                Event:</label>
                                                            <select id="selectEvent"
                                                                    style="width: 240px; height: 40px; margin-left: 15px; "
                                                                    name="selectEvent">
                                                                <option value="0">Select:</option>
                                                                <option>ANDROID</option>
                                                                <option>IOS</option>
                                                                <option>Windows</option>
                                                            </select>

                                                        </div>

                                                        <div style="width: 90%; margin-left: 20px; font-size: 20px; margin-top: 80px;">

                                                            <div>

                                                                <h4>Minimum time between sessions.</h4>

                                                                <div id="group1">

                                                                    <label class="bmd-label-floating <%=UIConstants.primaryTextColorClass%> <%=UIConstants.primaryTextColorClass%>"
                                                                           style="font-size: 20px; margin-left: 10px;"
                                                                           for="forRadio1">Minute:</label>
                                                                    <input type="radio" value="val1" id="forRadio1"
                                                                           name="group1">

                                                                    <label class="bmd-label-floating <%=UIConstants.primaryTextColorClass%> <%=UIConstants.primaryTextColorClass%>"
                                                                           style="font-size: 20px; margin-left: 10px;"
                                                                           for="forRadio2">Hours:</label>
                                                                    <input type="radio" value="val2" id="forRadio2"
                                                                           name="group1">

                                                                    <select id="selectTime"
                                                                            style="width: 240px; height: 40px; margin-left: 15px; "
                                                                            name="selectEvent">
                                                                        <option>1</option>
                                                                        <option>2</option>
                                                                        <option>5</option>
                                                                        <option>10</option>
                                                                        <option>15</option>
                                                                        <option>20</option>
                                                                        <option>30</option>
                                                                        <option>45</option>
                                                                    </select>

                                                                </div>


                                                            </div>

                                                        </div>

                                                    </div>

                                                    <br>

                                                    <hr style="height: 3px;border-width:0;color:gray;background-color:lightgray; margin-top: 10px;">

                                                    <br>

                                                    <div class="row">
                                                        <div style="width: 50%; margin-left: 20px;">
                                                            <h2>App store country</h2>
                                                            <h4>Change store country</h4>
                                                            <p style="font-size: 20px;">Select the app store country the
                                                                user is directed to.</p>
                                                        </div>
                                                        <div style="width: 51%; margin-left: 20px;">
                                                            <label class="bmd-label-floating <%=UIConstants.primaryTextColorClass%> <%=UIConstants.primaryTextColorClass%>"
                                                                   style="font-size: 20px; " for="selectCountry">Select
                                                                Country:</label>
                                                            <select id="selectCountry"
                                                                    style="width: 240px; height: 40px; margin-left: 15px; "
                                                                    name="selectCountry">
                                                                <option value="0">Select:</option>
                                                                <option>India</option>
                                                                <option>Pakistan</option>
                                                                <option>Nepal</option>
                                                            </select>
                                                        </div>

                                                    </div>

                                                    <br>

                                                    <hr style="height: 3px;border-width:0;color:gray;background-color:lightgray; margin-top: 10px;">

                                                    <br>

                                                    <div class="row">
                                                        <div style="width: 50%; margin-left: 20px;">
                                                            <h2>Localization</h2>
                                                            <p style="font-size: 20px;">App data is reported using the
                                                                timezone and currency selected.</p>
                                                        </div>
                                                        <div style="width: 51%; margin-left: 20px;">
                                                            <label class="bmd-label-floating <%=UIConstants.primaryTextColorClass%> <%=UIConstants.primaryTextColorClass%>"
                                                                   style="font-size: 20px; " for="selectTimezone">Timezone:</label>
                                                            <select id="selectTimezone"
                                                                    style="width: 240px; height: 40px; margin-left: 15px; "
                                                                    name="selectTimezone">
                                                                <option value="0">Select:</option>
                                                                <option>India-Kolkata</option>
                                                                <option>Pakistan-Lahore</option>
                                                                <option>Nepal</option>
                                                            </select>

                                                            <br>
                                                            <br>

                                                            <label class="bmd-label-floating <%=UIConstants.primaryTextColorClass%> <%=UIConstants.primaryTextColorClass%>"
                                                                   style="font-size: 20px; " for="selectCurrency">Currency:</label>
                                                            <select id="selectCurrency"
                                                                    style="width: 240px; height: 40px; margin-left: 15px; "
                                                                    name="selectCurrency">
                                                                <option value="0">Select:</option>
                                                                <option>INR</option>
                                                                <option>Dollar</option>
                                                                <option>Nepal</option>
                                                            </select>

                                                        </div>

                                                    </div>

                                                    <br>

                                                    <hr style="height: 3px;border-width:0;color:gray;background-color:lightgray; margin-top: 10px;">

                                                    <br>

                                                    <div class="row">
                                                        <div style="width: 50%; margin-left: 20px;">
                                                            <h2>Attribution</h2>
                                                            <h4>Re-attribution window</h4>
                                                            <p style="font-size: 20px;">Select the number of days after
                                                                the first install that reinstalls aren't attributed as
                                                                new installs.</p>
                                                        </div>
                                                        <div style="width: 51%; margin-left: 20px;">
                                                            <select id=""
                                                                    style="width: 240px; height: 40px; margin-left: 15px; "
                                                                    name="selectTimezone">
                                                                <option value="0">Select:</option>
                                                                <option>India-Kolkata</option>
                                                                <option>Pakistan-Lahore</option>
                                                                <option>Nepal</option>
                                                            </select>

                                                            <br>
                                                            <br>


                                                        </div>

                                                    </div>

                                                    <br>

                                                    <hr style="height: 3px;border-width:0;color:gray;background-color:lightgray; margin-top: 10px;">

                                                    <br>

                                                </form>

                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>


                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<%--

<div class="card-body">
                                <div class="table-responsive" style="margin-top:-26px">
                                    <table id="datatables" class="display" width="100%"
                                           style="width:100%;background-color:<%=UIConstants.primaryCardBackground%>">
                                        <thead>
                                        <tr class="<%=UIConstants.primaryTextColorClass%>" style="font-weight: bold;">
                                            <th>Offer Id</th>
                                            <th>OfferName</th>
                                            <th>Advertiser</th>
                                            <th>Tracking Url</th>
                                            <th>Creation Date Time</th>
                                            <th>Status</th>
                                            <th>Access Status</th>
                                            <th>Device</th>
                                            <th>OS</th>

<th>Action</th>
</tr>
</table>
</div>
</div>

var offerId = '';
var table = $('#datatables').DataTable({
ajax: '/getOffersAjax?id=' +${advsIds},
serverSide: true,
processing: true,
"order": [[0, "desc"]],
columns: [
{
data: 'id',
render: function (data) {
offerId = data;
return '<div style="margin-left:80px"></div>' + offerId;

}
},
{
data: 'offerName',
orderable: false,
render: function (data) {
var offerName = data;
if (data.length > 40) {
offerName = offerName.substring(0, 40);
var offerData = '<span style="margin-right:20px" data-toggle="tooltip" title="' + data + '">' + offerName + '...</span>';
return '<div style="margin-left:120px"></div><a href="/adminConsole/viewoffer?id=' + offerId + '" target = "_blank" style="color:<%=UIConstants.primaryHyperLinkColor%>;font-weight:bold">' + offerData + '</a>' + '<button class="btnCopy" value=' + data + ' style="margin-left:-20px"><i class="fa fa-files-o" aria-hidden="true"></i></button>';
} else
return '<div style="margin-left:120px"></div><a href="/adminConsole/viewoffer?id=' + offerId + '" target = "_blank" style="color:<%=UIConstants.primaryHyperLinkColor%>;font-weight:bold">' + data + '</a>';

}
},
{
data: 'company',
orderable: false,
render: function (data) {
var company_name = data;
if (data.length > 20) {
company_name = company_name.substring(0, 20);
data = '<span style="margin-right:20px" data-toggle="tooltip" title="' + data + '">' + company_name + '... </span>';
}
return ' <div style="margin-left:110px"></div>' + data;

}
},
{
data: 'trackingUrl',
orderable: false,
render: function (data) {
var trackingUrl = data;
if (data.length > 40) {
trackingUrl = trackingUrl.substring(0, 40);
data = '<span style="margin-right:20px" data-toggle="tooltip" title="' + data + '">' + previewUrl + '...<button class="btnCopy" value=' + data + '><i class="fa fa-files-o" aria-hidden="true"></i></button> </span>';
}
return ' <div style="margin-left:110px"></div>' + data;

}
},
{
data: 'creationDateTime',
orderable: false,
render: function (data) {
if (data != null) {
var date = new Date(data);
return '<div style="margin-left:100px"></div>' + date.toUTCString('en-GB');
} else
return '<div style="margin-left:150px"></div>NA';
}

},
{
data: 'status',
orderable: false,
render: function (data) {
return ' <div style="margin-left:60px"></div>' + data;
}
},
{
data: 'accessStatus',
orderable: false,
render: function (data) {
return '<div style="margin-left:150px"></div>' + data;
}
},
{
data: 'offerType',
orderable: false,
render: function (data) {
return '<div style="margin-left:150px"></div>' + data;

}
},

{
data: 'selectOS',
orderable: false,
render: function (data) {
return '<div style="margin-left:150px"></div>' + data;

}
},
{
data: 'Action',
orderable: false,
render: function (data) {
return '<a href="#" onclick="editOffer(' + offerId + ')" data-toggle="modal"data-target="#exampleModal"><i class="material-icons iconTable">edit</i></a>';

}
}

],
"scrollCollapse": true,
scrollX: true,
scrollY: 520,
"processing": "<img src='/assets/img/loader.gif'  width='100px' height='auto'/>"
});



var device = '';
$('select#offerType').change(function () {
$(".mobileDesktopT .fstChoiceRemove").click()
$(".desktopT .fstChoiceRemove").click()
$(".mobileT .fstChoiceRemove").click()
device = $(this).val();
selectOS('T');
});
$('select#device').change(function () {
$('.selectOsF').val('').trigger("change");
device = $(this).val();
selectOS('F');
});
$(document).ready(function () {
selectOS('T');
selectOS('F');
});

function selectOS(e) {
$(".mobileDesktop" + e).hide();
$(".desktop" + e).hide();
$(".mobile" + e).hide();
if (device == "Mobile")
$(".mobile" + e).show();
else if (device == "Desktop")
$(".desktop" + e).show();
else
$(".mobileDesktop" + e).show();
}


<div class="col-md-6">
    <label class="bmd-label-floating LabelNameOS <%=UIConstants.primaryTextColorClass%>"
           style="font-size:15px;margin-top:10px">Select
        OS:</label>
    <div class="row device" style="margin-top:-13px">
        <div class="col-sm-3" style="margin-top:-5px">
            <div class="mobileDesktopT">
                <select class="multipleSelect selectOsT"
                        multiple
                        data-size="7"
                        data-style="btn"
                        placeholder="All OS"
                        name="selectOS"
                        id="selectOs">
                    <option value="Android">Android</option>
                    <option value="iOS">iOS</option>
                    <option value="Windows">Windows</option>
                    <option value="Mac">Mac</option>
                    <option value="Linux">Linux</option>
                </select></div>
            <div class="mobileT">
                <select class="multipleSelect selectOsT"
                        multiple
                        data-size="7"
                        data-style="btn"
                        placeholder="All OS"
                        name="selectOS">
                    <option value="Android">Android</option>
                    <option value="iOS">iOS</option>
                </select></div>
            <div class="desktopT">
                <select class="multipleSelect selectOsT"
                        multiple
                        data-size="7"
                        id="selectOS"
                        data-style="btn"
                        placeholder="All OS"
                        name="selectOS">
                    <option value="Windows">Windows</option>
                    <option value="Mac">Mac</option>
                    <option value="Linux">Linux</option>
                </select></div>

        </div>
    </div>
</div>
--%>


<!--   Core JS Files   -->
<!-- <script src="https://code.jquery.com/jquery-3.3.1.js"></script>  -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<!-- <script src="../assets/js/core/jquery.min.js"></script> -->
<script src="../assets/js/core/popper.min.js"></script>
<script src="../assets/js/core/bootstrap-material-design.min.js"></script>
<script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!-- Plugin for the momentJs  -->
<script src="../assets/js/plugins/moment.min.js"></script>
<!--  Plugin for Sweet Alert -->
<script src="../assets/js/plugins/sweetalert2.js"></script>
<!-- Forms Validations Plugin -->
<script src="../assets/js/plugins/jquery.validate.min.js"></script>
<!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
<script src="../assets/js/plugins/jquery.bootstrap-wizard.js"></script>
<!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script src="../assets/js/plugins/bootstrap-selectpicker.js"></script>
<!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
<script src="../assets/js/plugins/bootstrap-datetimepicker.min.js"></script>
<!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
<script src="../assets/js/plugins/jquery.dataTables.min.js"></script>
<!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
<script src="../assets/js/plugins/bootstrap-tagsinput.js"></script>
<!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="../assets/js/plugins/jasny-bootstrap.min.js"></script>
<!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
<script src="../assets/js/plugins/fullcalendar.min.js"></script>
<!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
<script src="../assets/js/plugins/jquery-jvectormap.js"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="../assets/js/plugins/nouislider.min.js"></script>
<!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
<!-- Library for adding dinamically elements -->
<script src="../assets/js/plugins/arrive.min.js"></script>
<!--  Google Maps Plugin    -->
<%--<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>--%>
<!-- Chartist JS -->
<script src="../assets/js/plugins/chartist.min.js"></script>
<!--  Notifications Plugin    -->
<script src="../assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="../assets/js/material-dashboard.js?v=2.1.0" type="text/javascript"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="../assets/demo/demo.js"></script>
<%--loader JS Files--%>
<script src="../assets/js/loader.js"></script>
<%--Tooltip textArea JS--%>
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote-lite.min.js"></script>
<script src="../dist/js/select2.js"></script>


<script>
    // Add the following code if you want the name of the file appear on select
    $(".custom-file-input").on("change", function () {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
</script>


<script>
    function copyText() {
        var copyText = document.getElementById("myInput");
        copyText.select();
        document.execCommand("Copy");
        alert("Copied the text: " + copyText.value);
        event.preventDefault();
    }
</script>



</body>
</html>