<%@ page import="com.attribution.panel.constants.UIConstants" %>
<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <link rel="apple-touch-icon" sizes="76x76" href="asset/img/apple-icon.png">
    <link rel="icon" type="image/png" href="../asset/img/logoAttribution.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>
        User Login
    </title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport'/>
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- CSS Files -->
    <link href="asset/css/material-dashboard.css?v=2.1.0" rel="stylesheet"/>
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <!-- <link href="assets/demo/demo.css" rel="stylesheet" /> -->
    <style>
        :root {
            --primaryButtonColor: <%=UIConstants.primaryButtonColor%>;
            --borderColor: <%=UIConstants.borderColor%>;
            --textColor: <%=UIConstants.textColor%>;
            --arrowColor: <%=UIConstants.arrowColor%>;
            --hoverButtonBgColor: <%=UIConstants.hoverButtonBgColor%>;
            --dropDownHoverColor: <%=UIConstants.dropDownHoverColor%>;
            --dropDownSelectedColor: <%=UIConstants.dropDownSelectedColor%>;
            --dropDownMenuColor: <%=UIConstants.dropDownMenuColor%>;
            --paginatehoverTextColor: <%=UIConstants.paginateHoverTextColor%>;
            --paginatehoverBgColor: <%=UIConstants.paginateHoverBgColor%>;
            --tableIconBgColor: <%=UIConstants.tableIconBgColor%>;
            --tableIconTextColor: <%=UIConstants.tableIconTextColor%>;
            --primaryBackgroundColor: <%=UIConstants.primaryBackgroundColor%>;
            --primaryCardBackground: <%=UIConstants.primaryCardBackground%>;
            --bannerTextColor: <%=UIConstants.bannerTextColor%>;
            --bannerBgColor: <%=UIConstants.bannerBgColor%>;
            --sideBgColor: <%=UIConstants.sideBgColor%>;
            --scrollbarBgColor: <%=UIConstants.scrollbarBgColor%>;
            --primaryButtonTextColor: <%=UIConstants.primaryButtonTextColor%>;
            --chartTextColor: <%=UIConstants.chartTextColor%>;
            --chartBoxBgColor: <%=UIConstants.chartBoxBgColor%>;
            --primaryBootstrapColor: <%=UIConstants.primaryBootstrapColor%>;
            --dataPickerBgColor: <%=UIConstants.dataPickerBgColor%>;
        }

        .nav-link {
            cursor: pointer;
        }


        .dropdown-menu .nav-link:hover {
            background-color: var(--primaryButtonColor);
            color: var(--primaryButtonTextColor) !important;
        }

        .swal2-popup {
            font-size: 0.7rem !important;
        }

        .navbar-toggler {
            display: none;
        }

        .modal-backdrop {
            display: none;
        }

        .form-control {
            color: <%=UIConstants.textColor%>;
            font-weight: 400;
        }

        /*close modal style*/
        .modalClose {
            position: absolute;
            top: 0;
            right: 0;
            height: 45px;
            width: 50px;
            background-color: <%=UIConstants.primaryButtonColor%>;
            line-height: 40px;
            color: var(--primaryButtonTextColor);
            font-size: 18px;
            border-radius: 0 5px 0 50px;
            padding-left: 25px !important;
            cursor: pointer;
            z-index: 5;
        }

        .bmd-label-floating {
            color: <%=UIConstants.textColor%> !important;
        }

        .bmd-form-group .form-control {
            border-bottom: 1px solid var(--textColor);
        }
    </style>
</head>

<body class="">
<!-- Navbar -->
<jsp:include page="loginHeader.jsp"/>
<!-- End Navbar -->
<div class="wrapper wrapper-full-page">
    <div class="page-header login-page header-filter"
         style="background-image: url('${contextPath}/assets/img/loginBackground.jpg');background-size: cover; background-position:center;background-color:var(--primaryBackgroundColor);">
        <!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->
        <div class="container" <%--style="zoom:80%;margin-top:10px; -moz-transform: scale(1);"--%>
             style="margin-top:10px;">
            <div class="row">
                <div class="ml-auto mr-auto" style="width:350px">
                    <form class="form" method="POST" action="${contextPath}/login">
                        <div class="card card-login card-hidden">
                            <div class="card-header card-header-<%=UIConstants.primaryColorClass%> text-center">
                                <h4 class="card-title">Login</h4>
                            </div>
                            <div class="card-body">
                                <p class="card-description text-center"
                                   style="color:<%=UIConstants.textColor%>;font-weight: bold">Enter credentials</p>
                                <!--                   <span class="bmd-form-group">
                                                    <div class="input-group">
                                                      <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                          <i class="material-icons">face</i>
                                                        </span>
                                                      </div>
                                                      <input type="text" class="form-control" placeholder="First Name...">
                                                    </div>
                                                  </span> -->
                                <span class="bmd-form-group">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="material-icons">email</i>
                        </span>
                      </div>
                      <input type="text" class="form-control" placeholder="Email" name="email">
                    </div>
                  </span>
                                <span class="bmd-form-group">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="material-icons">lock_outline</i>
                        </span>
                      </div>
                      <input type="password" name="password" id="loginPassword" class="form-control"
                             placeholder="Password">
                      <i class="fa fa-eye-slash" aria-hidden="true"
                         style="position:absolute;margin-top:30px;margin-left:94%;cursor:pointer;z-index:5"
                         id="iconLoginPass" onclick="showPassword('loginPassword','#iconLoginPass')"></i>
                    </div>
                  </span>
                                <span class="bmd-form-group">
                  <br>

                                    <!-- <div class="input-group"> -->

                      	<p class="text-center text-danger font-weight-bold font-italic">${error}</p>


                                    <!-- </div> -->
                  </span>
                                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                            </div>
                            <div class="row" style="margin-left:130px;margin-top:-15px">
                                <button class="btn btn-<%=UIConstants.primaryColorClass%> btn-round" type="submit"
                                        style="font-size:15px;height:30px;padding:4px 20px;border:1px solid var(--borderColor)">
                                    Submit
                                </button>
                            </div>
                            <br>
                        </div>

                    </form>

                </div>
            </div>

        </div>

    </div>
    <div class="modal fade" id="signupModal" tabindex="-1" role="dialog" data-backdrop="static"
         style="margin-left:39%;margin-top:-60px">
        <div class="modal-dialog modal-signup" role="document">
            <div class="card modal-content"
                 style="width:315px;background-color:<%=UIConstants.primaryModalCardHeaderBackground%>">
                <div class="modalClose">
                    <i class="fa fa-times" data-dismiss="modal" aria-label="Close"></i>
                </div>
                <div class="card-header card-header-<%=UIConstants.primaryColorClass%> card-header-icon"
                     style="/*zoom:80%;*/height:80px">
                    <div class="card-icon" style="margin-left:-10px">
                        <i class="material-icons">dashboard</i>
                    </div>
                    <h4 class="card-title <%=UIConstants.primaryTextColorClass%>" id="modalHeading"
                        style="font-family: <%=UIConstants.primaryFontFamily%>; font-weight: bold;">
                        Register</h4>
                    <p class="card-category">&nbsp;Enter Information</p>
                </div>
                <div class="card-body"
                     style="font-family: <%=UIConstants.primaryFontFamily%>; font-weight: bold;background-color:<%=UIConstants.primaryCardModalBackground%>;width:315px">
                    <br>
                    <div class="modal-body">
                        <div class="row">
                            <div style="margin-left:-28px;margin-top:-45px">
                                <form id="saveDetails" class="form-horizontal" onsubmit="saveDetails()">
                                    <div class="card-body ">
                                        <div class="form-group" <%--style="zoom:80%"--%>>
                                            <label class="bmd-label-floating"> First Name*</label>
                                            <input type="text" name="fname"
                                                   class="form-control <%=UIConstants.primaryTextColorClass%>"
                                                   oninvalid="this.setCustomValidity('Please Enter First Name')"
                                                   oninput="setCustomValidity('')" onchange="limitText(this,255)"
                                                   autocomplete="off"
                                                   onkeypress="limitText(this,255)" style="font-weight:bold" required>
                                        </div>
                                        <div class="form-group" <%--style="zoom:80%"--%>>
                                            <label class="bmd-label-floating"> Last Name*</label>
                                            <input type="text" name="lname"
                                                   class="form-control <%=UIConstants.primaryTextColorClass%>"
                                                   oninvalid="this.setCustomValidity('Please Enter Last Name')"
                                                   oninput="setCustomValidity('')" onchange="limitText(this,255)"
                                                   autocomplete="off"
                                                   onkeypress="limitText(this,255)" style="font-weight:bold" required>
                                        </div>
                                        <div class="form-group" <%--style="zoom:80%"--%>>
                                            <label class="bmd-label-floating"> Email Address*</label>
                                            <input type="email" name="email"
                                                   class="form-control <%=UIConstants.primaryTextColorClass%>"
                                                   oninvalid="this.setCustomValidity('Please Enter Valid Email')"
                                                   oninput="setCustomValidity('')" onchange="limitText(this,255)"
                                                   onkeypress="limitText(this,255)" autocomplete="off"
                                                   pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$"
                                                   id="email" style="font-weight:bold" required>
                                        </div>
                                        <div class="form-group" <%--style="zoom:80%"--%>>
                                            <label class="bmd-label-floating"> Password*</label>
                                            <input type="password" name="pass"
                                                   oninvalid="this.setCustomValidity('Please Enter Password')"
                                                   oninput="setCustomValidity('')"
                                                   class="form-control <%=UIConstants.primaryTextColorClass%>"
                                                   onchange="limitText(this,255)" onkeypress="limitText(this,255)"
                                                   id="pass" pattern=".{6,}" title="At least 6 or more characters"
                                                   autocomplete="off" name="password"
                                                   style="font-weight:bold" required>
                                            <i class="fa fa-eye-slash <%=UIConstants.primaryTextColorClass%>"
                                               id="iconPass"
                                               aria-hidden="true"
                                               style="position:absolute;margin-top:-20px;margin-left:94%;cursor:pointer"
                                               onclick="showPassword('pass','#iconPass')"></i>
                                        </div>
                                        <div class="form-group" <%--style="zoom:80%"--%>>
                                            <label class="bmd-label-floating"> Confirm Password*</label>
                                            <input type="password" autocomplete="off"
                                                   class="form-control <%=UIConstants.primaryTextColorClass%>"
                                                   onchange="limitText(this,255)" onkeypress="limitText(this,255)"
                                                   id="confirmPass" style="font-weight:bold" required>
                                            <i class="fa fa-eye-slash <%=UIConstants.primaryTextColorClass%>" id="icon"
                                               aria-hidden="true"
                                               style="position:absolute;margin-top:-20px;margin-left:94%;cursor:pointer"
                                               onclick="showPassword('confirmPass','#icon')"></i>
                                        </div>
                                        <div class="form-group" <%--style="zoom:80%"--%>>
                                            <label class="bmd-label-floating"> Company*</label>
                                            <input type="text" name="company" autocomplete="off"
                                                   class="form-control text-<%=UIConstants.primaryTextColorClass%>"
                                                   oninvalid="this.setCustomValidity('Please Enter Company Name')"
                                                   oninput="setCustomValidity('')" onchange="limitText(this,255)"
                                                   onkeypress="limitText(this,255)" style="font-weight:bold" required>
                                        </div>
                                        <div class="category form-category <%=UIConstants.primaryTextColorClass%>"
                                        <%--style="zoom:80%"--%>>* Required fields
                                        </div>
                                        <div class="form-check" style="/*zoom:80%;*/margin-top:10px">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" value="" checked>
                                                <span class="form-check-sign"><span class="check"></span></span><b
                                                    style="color:<%=UIConstants.textColor%>">I agree to the </b><a
                                                    href="#something"
                                                    style="color:<%=UIConstants.primaryHyperLinkColor%>;font-weight: bold;">terms
                                                and conditions</a>.
                                            </label>
                                        </div>
                                    </div>
                                    <div class="g-recaptcha" data-callback="recaptchaCallback"
                                         data-sitekey="6LfqLLUUAAAAABhTS_WGjaT38rEOhp7oZKPxnnTu"
                                         style="transform:scale(0.8);-webkit-transform:scale(0.8);transform-origin:0 0;-webkit-transform-origin:0 0;margin-left:18px;"></div>
                                    <div class="modal-footer justify-content-center" style="margin-top:10px">
                                        <button type="submit" id="regSubmit"
                                                class="btn btn-<%=UIConstants.primaryColorClass%> btn-round"
                                                style="height:24px;padding:3px 10px;border:1px solid <%=UIConstants.borderColor%>">
                                            Submit
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--   Core JS Files   -->
    <script src="assets/js/core/jquery.min.js"></script>
    <script src="assets/js/core/popper.min.js"></script>
    <script src="assets/js/core/bootstrap-material-design.min.js"></script>
    <script src="assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
    <!-- Forms Validations Plugin -->
    <script src="assets/js/plugins/jquery.validate.min.js"></script>
    <!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
    <!--  Plugin for Sweet Alert -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <!-- <script src="../assets/js/plugins/sweetalert2.js"></script> -->
    <!--  Notifications Plugin    -->
    <script src="assets/js/plugins/bootstrap-notify.js"></script>
    <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="assets/js/material-dashboard.js?v=2.1.0" type="text/javascript"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>

    <script>
        $(document).ready(function () {
            $().ready(function () {
                $sidebar = $('.sidebar');

                $sidebar_img_container = $sidebar.find('.sidebar-background');

                $full_page = $('.full-page');

                $sidebar_responsive = $('body > .navbar-collapse');

                window_width = $(window).width();

                fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

                if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
                    if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
                        $('.fixed-plugin .dropdown').addClass('open');
                    }

                }

                $('.fixed-plugin a').click(function (event) {
                    // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
                    if ($(this).hasClass('switch-trigger')) {
                        if (event.stopPropagation) {
                            event.stopPropagation();
                        } else if (window.event) {
                            window.event.cancelBubble = true;
                        }
                    }
                });

                $('.fixed-plugin .active-color span').click(function () {
                    $full_page_background = $('.full-page-background');

                    $(this).siblings().removeClass('active');
                    $(this).addClass('active');

                    var new_color = $(this).data('color');

                    if ($sidebar.length != 0) {
                        $sidebar.attr('data-color', new_color);
                    }

                    if ($full_page.length != 0) {
                        $full_page.attr('filter-color', new_color);
                    }

                    if ($sidebar_responsive.length != 0) {
                        $sidebar_responsive.attr('data-color', new_color);
                    }
                });

                $('.fixed-plugin .background-color .badge').click(function () {
                    $(this).siblings().removeClass('active');
                    $(this).addClass('active');

                    var new_color = $(this).data('background-color');

                    if ($sidebar.length != 0) {
                        $sidebar.attr('data-background-color', new_color);
                    }
                });

                $('.fixed-plugin .img-holder').click(function () {
                    $full_page_background = $('.full-page-background');

                    $(this).parent('li').siblings().removeClass('active');
                    $(this).parent('li').addClass('active');


                    var new_image = $(this).find("img").attr('src');

                    if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
                        $sidebar_img_container.fadeOut('fast', function () {
                            $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
                            $sidebar_img_container.fadeIn('fast');
                        });
                    }

                    if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
                        var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

                        $full_page_background.fadeOut('fast', function () {
                            $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
                            $full_page_background.fadeIn('fast');
                        });
                    }

                    if ($('.switch-sidebar-image input:checked').length == 0) {
                        var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
                        var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

                        $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
                        $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
                    }

                    if ($sidebar_responsive.length != 0) {
                        $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
                    }
                });

                $('.switch-sidebar-image input').change(function () {
                    $full_page_background = $('.full-page-background');

                    $input = $(this);

                    if ($input.is(':checked')) {
                        if ($sidebar_img_container.length != 0) {
                            $sidebar_img_container.fadeIn('fast');
                            $sidebar.attr('data-image', '#');
                        }

                        if ($full_page_background.length != 0) {
                            $full_page_background.fadeIn('fast');
                            $full_page.attr('data-image', '#');
                        }

                        background_image = true;
                    } else {
                        if ($sidebar_img_container.length != 0) {
                            $sidebar.removeAttr('data-image');
                            $sidebar_img_container.fadeOut('fast');
                        }

                        if ($full_page_background.length != 0) {
                            $full_page.removeAttr('data-image', '#');
                            $full_page_background.fadeOut('fast');
                        }

                        background_image = false;
                    }
                });

                $('.switch-sidebar-mini input').change(function () {
                    $body = $('body');

                    $input = $(this);

                    if (md.misc.sidebar_mini_active == true) {
                        $('body').removeClass('sidebar-mini');
                        md.misc.sidebar_mini_active = false;

                        $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

                    } else {

                        $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');

                        setTimeout(function () {
                            $('body').addClass('sidebar-mini');

                            md.misc.sidebar_mini_active = true;
                        }, 300);
                    }

                    // we simulate the window Resize so the charts will get updated in realtime.
                    var simulateWindowResize = setInterval(function () {
                        window.dispatchEvent(new Event('resize'));
                    }, 180);

                    // we stop the simulation of Window Resize after the animations are completed
                    setTimeout(function () {
                        clearInterval(simulateWindowResize);
                    }, 1000);

                });
            });
        });
    </script>

    <script>
        $(document).ready(function () {
            var api_url = 'https://api.linkpreview.net'
            var key = '5b578yg9yvi8sogirbvegoiufg9v9g579gviuiub8' // not real
            /*console.log("in ready");*/
            $("#formSubmit").click(function () {
                /*console.log("in change function");*/

                var flag = false;
                $.ajax({
                    url: '${contextPath}/emailApi' + "?value=" + document.getElementById("exampleEmail").value,
                    async: false,
                    contentType: "application/json",
                    dataType: 'json',
                    beforeSend: function (msg) {
                        /*console.log("in ajax change")*/
                    },
                    success: function (result) {
                        /*console.log("inside if :" + result['response']);*/
                        if (result['response'] == true) {
                            Swal.fire({
                                title: 'Duplicate email',
                                text: "Email already exist",
                                type: 'warning',
                                confirmButtonText: 'Ok'
                            });
                            flag = true;
                        }


                    }
                })
                /*console.log("after if" + flag);*/
                if (flag)
                    event.preventDefault();

            });
        });
    </script>

    <script>
        $(document).ready(function () {
            md.checkFullPageBackgroundImage();
            setTimeout(function () {
                // after 1000 ms we add the class animated to the login/register card
                $('.card').removeClass('card-hidden');
            }, 700);
        });
    </script>
        <%
    	String message = (String) request.getAttribute("message");
    	String type = (String) request.getAttribute("type");
    	if (message != null)
    		if (!message.equals("")) {
    			if(type==null){
    				type = "success";
    			}

    %>
    <script>
        // Initialise Sweet Alert library
        Swal.fire({
            title: '${title}',
            text: "${message}",
            type: '<%=type%>',
            confirmButtonText: 'OK'
        });


    </script>

        <%} %>
    <script>
        $('#forgotPass').click(function () {
            event.preventDefault();
            Swal.fire({
                title: 'Enter your email address',
                input: 'text',
                inputAttributes: {
                    autocapitalize: 'off'
                },
                showCancelButton: true,
                confirmButtonText: 'Submit',
                showLoaderOnConfirm: true,
                preConfirm: (login) => {
                    return fetch(`/api_access/forgotPass?email=` + login)
                        .then(response => {
                            if (!response.ok) {
                                throw new Error(response.statusText)
                            }
                            return response.json()
                        })
                        .catch(error => {
                            Swal.showValidationMessage(
                                'Request failed: ${error}'
                            )
                        })
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                if (result.value) {
                    /*console.log(result.value['response']);*/
                    validUser = result.value['validUser'];
                    /*console.log("validUser: " + validUser);*/
                    if (validUser == true) {
                        Swal.fire({
                            title: `Check Email`,
                            text: 'Please check your email for reset password link',
                            type: 'success'
                            /*       imageUrl: result.value.avatar_url */
                        });
                    } else {
                        Swal.fire({
                            title: `Check Email`,
                            text: 'Email not found! Please retry with a registered email or Register as new user',
                            type: 'error'
                            /*       imageUrl: result.value.avatar_url */
                        });
                    }

                }
            });
        });

        var error = '${msg}';
        console.log(error);
        if (error != '') {
            Swal.fire({
                title: '${title}',
                text: '${msg}',
                type: '${type}',
                confirmButtonText: 'OK'
            });
        }

        var text = '';
        $('.dropdown-menu li a').click(function () {
            text = $(this).text();
            if (text === 'Advertiser')
                document.getElementById("modalHeading").innerHTML = "Register Advertiser";
            else
                document.getElementById("modalHeading").innerHTML = "Register Publisher";
        });

        function recaptchaCallback() {

        }

        $("#regSubmit").click(function () {
            $('#regSubmit').attr('disabled');
            var flag = "";
            // password and confirm_password validation
            var password = document.getElementById("pass");
            var confirm_password = document.getElementById("confirmPass");

            if (password.value !== confirm_password.value) {
                confirm_password.setCustomValidity("Passwords Don't Match");
            } else {
                confirm_password.setCustomValidity('');
            }
            $.ajax({
                url: '/emailByUser' + "?value=" + document.getElementById("email").value,
                async: false,
                contentType: "application/json",
                dataType: 'json',
                success: function (result) {
                    if (result['response'] == true) {
                        Swal.fire({
                            title: 'Duplicate email',
                            text: "Email already exist",
                            type: 'warning',
                            confirmButtonText: 'Ok'
                        });
                        flag = true;
                    } else {
                        flag = false;
                    }
                }

            });
            /*console.log("after if" + flag);*/
            if (flag) {
                event.preventDefault();
            }

        });

        function saveDetails() {
            var response = grecaptcha.getResponse();
            if (response.length === 0) {
                //reCaptcha not verified
                Swal.fire({
                    title: 'Captcha failed ',
                    text: "please verify you are human!",
                    type: 'info',
                    confirmButtonText: 'Ok'
                });
                event.preventDefault();
                return false;
            }
            myform = document.getElementById("saveDetails");
            var fd = new FormData(myform);
            var url = '';
            if (text === 'Advertiser')
                url = "/adminConsole/registrationAdvertiser";
            else
                url = "/adminConsole/registrationPublisher";
            event.preventDefault();
            $.ajax({
                url: url,
                data: fd,
                cache: false,
                processData: false,
                contentType: false,
                type: 'POST',
                beforeSend: function () {


                },
                success: function (response) {
                    Swal.fire({
                        type: 'success',
                        title: 'Your application has been submitted successfully',
                        showConfirmButton: false,
                        timer: 2500
                    });
                    /*location.reload();*/
                    setTimeout(function () {
                        window.location.reload();
                    }, 2500);
                },
                complete: function () {
                    // Hide image container

                },

                error: function (jqXHR) {
                    var o = $.parseJSON(jqXHR.responseText);
                    window.open('/adminConsole/error?status=' + o.status + '&message=' + o.error, "_self")
                }
            });
        }

        function showPassword(e, icon) {
            var x = document.getElementById(e);
            if (x.type === "password") {
                x.type = "text";
                $(icon).removeClass('fa fa-eye-slash').addClass('fa fa-eye');
            } else {
                x.type = "password";
                $(icon).removeClass('fa fa-eye').addClass('fa fa-eye-slash');
            }
        }

        function limitText(limitField, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
                Swal.fire({
                    title: 'Limit Validation',
                    text: "Value should be between 0 - " + limitNum,
                    type: 'info',
                    confirmButtonText: 'Ok'
                });
            }
        }
    </script>
</body>

</html>