<%@ page import="com.attribution.panel.constants.UIConstants" %>
<%@ page import="com.attribution.panel.bean.Event" %>
<%@ page import="java.util.List" %>
<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<html lang="en">

<head>

    <meta charset="utf-8"/>
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <link rel="icon" type="image/png" href="../assets/img/logoAttribution.png">

    <title>
        Dashboard
    </title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport'/>

    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- CSS Files -->
    <link href="../assets/css/material-dashboard.css?v=2.1.0" rel="stylesheet"/>
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link rel="stylesheet" type="text/css" href="../assets/css/daterangepicker.css"/>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../assets/charts/dark/styles.css">
    <link href="../assets/css/style-sheet-attribution.css?v=2.1.0" rel="stylesheet"/>
    <%--loader CSS Files--%>
    <link href="../assets/css/loader.css" rel="stylesheet"/>
    <%--    <link href="../assets/css/chart.css" rel="stylesheet"/>--%>
    <%--Map Charts CS File--%>
    <link href="../assets/css/StephanWagner.css" rel="stylesheet"/>
    <link href="../dist/css/select2.css" rel="stylesheet"/>

    <style>
        .nav-linkdashboard {
            border: 1px solid<%=UIConstants.borderColor%>;
            height: 28px;
            font-size: 12px;
            padding: 3px 10px !important;
            margin-top: 10px;
            cursor: pointer;
        }

        .apexcharts-xaxis-label, .apexcharts-yaxis-label {
            font-weight: bold;
        }

        .apexcharts-yaxis-title-text {
            display: none;
        }

        .apexcharts-legend-text {
            color: #000000 !important;
            font-weight: bold !important;
        }

        /*#geoWiseInstall{
            !*height: 550px;
            width: 50%;
            margin-left: 0 !important;
            margin-right: 0 !important;*!
        }*/
        .noData {
            margin-left: 45%;
        }
    </style>
</head>

<body class="" style="background-color:<%=UIConstants.primaryBackgroundColor%>;">

<div class="se-pre-con"></div>

<div class="wrapper ">

    <jsp:include page="integratedSidebar.jsp">
        <jsp:param value="active" name="dash"/>
    </jsp:include>

    <div class="main-panel">
        <!-- Navbar -->
        <jsp:include page="adminHeader.jsp"/>
        <!-- End Navbar -->
        <div class="content">

            <div class="container-fluid">
                <div class="main">

                    <div class="card-header card-header-<%=UIConstants.primaryColorClass%> card-header-icon"
                         style="background:transparent;border:0px solid">
                        <div class="row pull-right" style="margin-top:11px;margin-right:-30px">
                            <div class="col-sm-7" style="">
                                <!-- <i class="fa fa-caret-down"></i> -->
                                <div class="<%=UIConstants.primaryTextColorClass%>" id="reportrange"
                                     style="margin-top: -40px;">
                                    <input readonly type="text"
                                           class="<%=UIConstants.primaryTextColorClass%>"
                                           id="reportrangeinput"
                                           name="daterange"
                                           style="cursor: pointer; background-color: <%=UIConstants.primaryBackgroundColor%>; border:1px solid <%=UIConstants.borderColor%>; width:180px;padding:3px 6px;font-size:14px;">
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="row sparkboxes mt-4">
                        <div class="col-md-3">
                            <div class="box box2">
                                <div class="details" style="margin-top: -8px">
                                    <h4 id="installCount"></h4>
                                    <h6 style="text-transform: capitalize;">User Acquisition</h6>
                                    <h6 id="dRange2"></h6>
                                    <div style="margin-top: 18px"></div>
                                </div>
                                <%--                                <div id="spark2"></div>--%>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="box box2">
                                <div class="details" style="margin-top: -8px">
                                    <h4 id="impressionCount"></h4>
                                    <h6 style="text-transform: capitalize;">User Impressions</h6>
                                    <h6 id="dRange3"></h6>
                                    <div style="margin-top: 18px"></div>
                                </div>
                                <%--                                <div id="spark2"></div>--%>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="box box2">
                                <div class="details" style="margin-top: -8px">
                                    <h4 id="DAUCount">0</h4>
                                    <h6 style="text-transform: capitalize;">DAU By Impression</h6>
                                    <h6 id="dRange4">Today</h6>
                                    <div style="margin-top: 18px"></div>
                                </div>
                                <%--                                <div id="spark2"></div>--%>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="box box1">
                                <div class="details" style="margin-top: -8px">
                                    <h4 id="retentionCount">0</h4>
                                    <h6 style="text-transform: capitalize;">User Retention</h6>
                                    <h6>Last 7 Days</h6>
                                    <div style="margin-top: 18px"></div>

                                </div>
                                <%--                                <div id="spark1"></div>--%>
                            </div>
                        </div>
                    </div>

                    <br>

                    <div class="row mt-4">
                        <div class="col-md-12">
                            <div class="cardDiv">
                                <div class="cardSize">
                                    <i class="material-icons iconSize">install_mobile</i>
                                </div>
                                <h2 class="cardHeading" style="margin-top:15px">Top Date Wise Installs</h2>
                            </div>
                            <br>
                            <div id="installDateWise" style="margin-top: -10px;"></div>
                        </div>
                    </div>

                    <div class="row mt-4">

                        <div class="col-md-12">
                            <div class="cardDiv">
                                <div class="cardSize">
                                    <i class="material-icons iconSize">event_note</i>
                                </div>
                                <h2 class="cardHeading" style="margin-top:15px">Top Events</h2>
                                <div>
                                    <button type="submit" class="submitButton pull-right" data-toggle="modal"
                                            data-target="#showEventName"
                                            style="margin-right:20px;margin-top:5px">Select Event
                                    </button>
                                </div>
                            </div>
                            <br>
                            <div id="eventCount" style="margin-top: -10px"></div>
                        </div>

                    </div>

                    <div class="row mt-4">
                        <div class="col-md-5">
                            <div class="cardDiv">
                                <div class="cardSize">
                                    <i class="material-icons iconSize">install_mobile</i>
                                </div>
                                <h2 class="cardHeading" style="margin-top:15px">Top Geo Wise Installs</h2>
                            </div>
                            <br><br>
                            <div id="geoWiseInstall" style="margin-top: -10px;"></div>
                        </div>
                        <div class="col-md-7">
                            <br><br><br>
                            <div id="geoWiseInstallMap"></div>
                        </div>
                        <div class="noData" style="display:none">NO DATA</div>
                    </div>

                    <div class="modal fade" id="showEventName" tabindex="-1" role="dialog" aria-hidden="true"
                         data-backdrop="static">
                        <div class="modal-dialog " role="document">
                            <div class="card modal-content <%=UIConstants.primaryTextColorClass%> contentAdvertiser"
                                 style="background-color:<%=UIConstants.primaryModalCardHeaderBackground%>;width:420px">
                                <div class="modalClose">
                                    <i class="fa fa-times" data-dismiss="modal" aria-label="Close"></i>
                                </div>
                                <div class="card-header card-header-<%=UIConstants.primaryColorClass%> card-header-icon"
                                     style="height:70px">
                                    <div class="cardDiv">
                                        <div class="cardSize">
                                            <i class="material-icons iconSize" style="margin-left: 4px;margin-top: 5px">assignment</i>
                                        </div>
                                        <h4 class="card-title <%=UIConstants.primaryTextColorClass%>"
                                            style="font-family: <%=UIConstants.primaryFontFamily%>; font-weight: bold;margin-left:58px">
                                            Event Name</h4>
                                        <p class="card-category" style="margin-left:60px">Select Event</p>
                                    </div>
                                </div>
                                <div class="card-body"
                                     style="font-family: <%=UIConstants.primaryFontFamily%>; font-weight: bold;background-color:<%=UIConstants.primaryCardModalBackground%>;width:420px;">
                                    <p>${message}</p>
                                    <br>
                                    <div style="">
                                        <div class="row" style="margin-top: -70px">
                                            <div class="col-md-12">
                                                <label class="bmd-label-floating <%=UIConstants.primaryTextColorClass%>"
                                                       style="margin-left:5px;margin-top:48px;font-size:15px;">Add
                                                    Columns:</label>
                                                <div style="margin-top:5px;margin-left:-8px">
                                                    <div class="dropdown bootstrap-select show-tick show <%=UIConstants.primaryTextColorClass%>"
                                                         style="z-index: 5">
                                                        <select class="selectEvent" multiple
                                                                style="width:390px;color:white;z-index: 5"
                                                                id="selectEvent">
                                                            <option></option>
                                                            <% List<Event> eventList = (List<Event>) request.getAttribute("eventList");
                                                                for (Event event : eventList) {%>
                                                            <option value="<%=event.getId()%>">(<%=event.getId()%>
                                                                ) <%=event.getEventName()%>
                                                            </option>
                                                            <%}%>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row"
                                             style="border-top:1px solid #423b3a;width:420px;margin-left:-20px;margin-top: -10px">
                                            <div class="col-sm-12">
                                                <button type="submit" class="submitButton pull-right"
                                                        id="searchEvent" style="margin-right:20px;margin-top:15px">
                                                    Search
                                                </button>
                                            </div>
                                        </div>
                                        <br>
                                    </div>
                                    <!-- Image loader -->
                                    <div class="mx-auto text-center" style='display: none;margin-top:-35px'>
                                        <!--              <h6 class="<%=UIConstants.primaryTextColorClass%>">Loading... </h6>
 --> <img src='../assets/loaders/colorfulSpiralSpinner.gif' width='150px' height='150px'>

                                    </div>
                                    <div style="margin-top:-30px !important;">
                                        <i style="background-color:<%=UIConstants.primaryCardModalBackground%>;"></i>
                                    </div>


                                    <!-- Image loader -->
                                </div>

                            </div>
                            <!-- 									</div>
          -->
                        </div>
                    </div>

                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<!--   Core JS Files   -->
<!-- multiselect -->


<script src="../assets/js/core/jquery.min.js"></script>
<script src="../assets/js/core/popper.min.js"></script>
<script src="../assets/js/core/bootstrap-material-design.min.js"></script>
<script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!-- Plugin for the momentJs  -->
<script src="../assets/js/plugins/moment.min.js"></script>
<!--  Plugin for Sweet Alert -->
<script src="../assets/js/plugins/sweetalert2.js"></script>
<!-- Forms Validations Plugin -->
<script src="../assets/js/plugins/jquery.validate.min.js"></script>
<!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
<script src="../assets/js/plugins/jquery.bootstrap-wizard.js"></script>
<!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script src="../assets/js/plugins/bootstrap-selectpicker.js"></script>
<!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
<script src="../assets/js/plugins/bootstrap-datetimepicker.min.js"></script>
<!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
<script src="../assets/js/plugins/jquery.dataTables.min.js"></script>
<!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
<script src="../assets/js/plugins/bootstrap-tagsinput.js"></script>
<!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="../assets/js/plugins/jasny-bootstrap.min.js"></script>
<!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
<script src="../assets/js/plugins/fullcalendar.min.js"></script>
<!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
<script src="../assets/js/plugins/jquery-jvectormap.js"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="../assets/js/plugins/nouislider.min.js"></script>
<!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
<!-- Library for adding dinamically elements -->
<script src="../assets/js/plugins/arrive.min.js"></script>
<!-- Chartist JS -->
<script src="../assets/js/plugins/chartist.min.js"></script>
<!--  Notifications Plugin    -->
<script src="../assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="../assets/js/material-dashboard.js?v=2.1.0" type="text/javascript"></script>
<script src="../assets/fastselect.standalone.js"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="../assets/demo/demo.js"></script>
<script src="https://cdn.jsdelivr.net/npm/apexcharts@latest"></script>
<script src="../assets/charts/dark/scripts.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<!-- Initialize the plugin: -->
<%--loader JS Files--%>
<script src="../assets/js/loader.js"></script>
<script type="text/javascript" src="../assets/js/chart.js"></script>
<script src="https://cdn.jsdelivr.net/npm/svg-pan-zoom@3.6.1/dist/svg-pan-zoom.min.js"></script>
<%--Map Charts Js File--%>
<script src="../assets/js/mapChart.js"></script>
<script src="../dist/js/select2.js"></script>
<script>
    var dateRange;
    $(function () {
        var start = moment().subtract(6, 'days').startOf('day');
        var end = moment().endOf('day');

        function cb(start, end) {
            document.getElementById("reportrangeinput").value = start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY');
            dateRange = start.format('YYYY-MM-DD HH:mm:ss') + ' - ' + end.format('YYYY-MM-DD HH:mm:ss');
            /*Populating dates in Counts boxes*/
            document.getElementById("dRange2").innerHTML = start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY');
            document.getElementById("dRange3").innerHTML = start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY');
            getInstallCounts()
            getImpressionCounts()
            installCountByDate()
            installCount()
            topEvent("date")
            $("#geoWiseInstallMap").load(location.href + " #geoWiseInstallMap");
            installCountMap()
        }

        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'Today': [moment().startOf('day'), moment().endOf('day')],
                'Yesterday': [moment().subtract(1, 'days').startOf('day'), moment().subtract(1, 'days').endOf('day')],
                'Last 7 Days': [moment().subtract(6, 'days').startOf('day'), moment().endOf('day')],
                'Last 30 Days': [moment().subtract(29, 'days').startOf('day'), moment().endOf('day')],
                'This Month': [moment().startOf('month').startOf('day'), moment().endOf('month').endOf('day')],
                'Last Month': [moment().subtract(1, 'month').startOf('month').startOf('day'), moment().subtract(1, 'month').endOf('month').endOf('day')]
            }
        }, cb);

        cb(start, end);
    });

    function getInstallCounts() {
        $.ajax({
            url: "/install/installCount?appId=${app.id}&dateRange=" + dateRange,
            type: "GET",
            dataType: 'json',
            success: function (response) {
                document.getElementById("installCount").innerHTML = response.count;
            },
            error: function () {
                console.log("Error");
            }
        });
    }

    function getImpressionCounts() {
        $.ajax({
            url: "/impression/count?appId=${app.id}&dateRange=" + dateRange,
            type: "GET",
            dataType: 'json',
            success: function (response) {
                document.getElementById("impressionCount").innerHTML = response.count;
            },
            error: function () {
                console.log("Error");
            }
        });
    }

    getDAUByCounts();

    function getDAUByCounts() {
        $.ajax({
            url: "/dailyActiveUsers/findDUAByAppId?appId=${app.id}",
            type: "GET",
            dataType: 'json',
            success: function (response) {
                document.getElementById("DAUCount").innerHTML = response.count;
            },
            error: function () {
                console.log("Error");
            }
        });
    }

    getRetentionCounts();

    function getRetentionCounts() {
        $.ajax({
            url: "/install/retentionCount?appId=${app.id}",
            type: "GET",
            dataType: 'json',
            success: function (response) {
                document.getElementById("retentionCount").innerHTML = response.percentage + '%';
            },
            error: function () {
                console.log("Error");
            }
        });
    }

    $('#searchEvent').click(function () {
        $('#showEventName').modal('hide');
        topEvent("search")
    });
    $('.selectEvent').select2({});
    var selectEvent = '';
    $('select#selectEvent').change(function () {
        selectEvent = '';
        $('select#selectEvent option:selected').each(function () {
            selectEvent += $(this).val() + ",";
        });
        selectEvent = selectEvent.substring(0, selectEvent.length - 1);
    });

    function topEvent(e) {
        $.ajax({
            url: "/event/findByEventAjax1?appId=${app.id}&dateRange=" + dateRange + "&selectEvent=" + selectEvent,
            type: "GET",
            dataType: 'json',
            success: function (response) {
                for (var i = 0; i < response.event.length; i++) {
                    // Set selected
                    $("#selectEvent").select2("trigger", "select", {
                        data: {
                            id: response.event[i].id,
                            text: " (Id:" + response.event[i].id + ") " + response.event[i].eventName
                        }
                    });
                }
                var countryCode = [];
                var count = [];
                for (var i = 0; i < response.eventList.length; i++) {
                    countryCode[i] = response.eventList[i].eventName;
                    count[i] = response.eventList[i].eventCount;
                }
                var options = {
                    series: [{
                        name: 'Count',
                        data: count
                    }],
                    chart: {
                        type: 'bar',
                        height: 350
                    },
                    fill: {
                        colors: ['#1A73E8', '#B32824']
                    },
                    plotOptions: {
                        bar: {
                            colors: {
                                ranges: [{
                                    from: -100,
                                    to: -46,
                                    color: '#F15B46'
                                }, {
                                    from: -45,
                                    to: 0,
                                    color: '#FEB019'
                                }]
                            },
                            columnWidth: '80%',
                        }
                    },
                    dataLabels: {
                        enabled: false,
                    },
                    yaxis: {
                        title: {
                            text: 'Growth',
                        },
                        labels: {
                            style: {
                                colors: '#000000'
                            },
                            formatter: function (y) {
                                return y.toFixed(0);
                            }
                        }
                    },
                    xaxis: {
                        categories: countryCode,
                        labels: {
                            style: {
                                colors: '#000000'
                            },
                            rotate: -90
                        }
                    },
                };
                var chart = new ApexCharts(document.querySelector("#eventCount"), options);
                $("#eventCount").empty();
                chart.render();
            },

            error: function () {
                console.log("Error");
            }
        });
    }

    function installCountByDate() {
        $.ajax({
            url: "/install/installCountWiseDate?appId=${app.id}&dateRange=" + dateRange,

            type: "GET",
            dataType: 'json',

            success: function (response) {
                var dateAndHours = [];
                var count = [];
                for (var i = 0; i < response.count.length; i++) {
                    count[i] = response.count[i];
                    dateAndHours[i] = response.hours[i];
                }
                var optionsLine = {
                    chart: {
                        height: 360,
                        type: 'line',
                        zoom: {
                            enabled: false
                        },
                        dropShadow: {
                            enabled: true,
                            top: 3,
                            left: 2,
                            blur: 4,
                            opacity: 1,
                        }
                    },
                    stroke: {
                        curve: 'smooth',
                        width: 2
                    },
                    colors: ['<%=UIConstants.chartLineColor1%>', '<%=UIConstants.chartLineColor%>'],
                    series: [{
                        name: "Install",
                        data: count
                    }

                    ],
                    title: {
                        text: '',
                        align: 'left',
                        offsetY: 25,
                        offsetX: 20
                    },

                    markers: {
                        size: 6,
                        strokeWidth: 0,
                        hover: {
                            size: 9
                        }
                    },
                    grid: {
                        show: true
                    },
                    yaxis: {
                        labels: {
                            style: {
                                colors: '#000000'
                            },
                        }
                    },
                    xaxis: {
                        categories: dateAndHours,
                        labels: {
                            style: {
                                colors: '#000000'
                            }
                        }
                    },
                    legend: {
                        position: 'top',
                        horizontalAlign: 'right',
                        offsetY: -20
                    }
                }

                var chartLine = new ApexCharts(document.querySelector('#installDateWise'), optionsLine);
                $("#installDateWise").empty();
                chartLine.render()
            },

            error: function () {
                console.log("Error");
            }
        });
    }

    function installCount() {
        $.ajax({
            url: "/install/installCountWiseGeo?appId=${app.id}&dateRange=" + dateRange,
            type: "GET",
            dataType: 'json',

            success: function (response) {
                if (response.installGeoWise.length == 0) {
                    // document.getElementById("offerChart").style.marginLeft = "50% !important"display:none
                    // document.getElementById("offerChart").innerHTML = "NO DATA";
                    $('.noData').show();
                    document.getElementById("geoWiseInstall").style.display = "none";
                } else {
                    $('.noData').hide();
                    document.getElementById("geoWiseInstall").style.display = "block";
                    var data = [];
                    for (var i = 0; i < response.installGeoWise.length; i++) {
                        var obj = {
                            y: response.installGeoWise[i].count,
                            indexLabel: response.installGeoWise[i].country
                        };
                        data.push(obj);
                    }

                    var chart = new CanvasJS.Chart("geoWiseInstall",
                        {
                            backgroundColor: "<%=UIConstants.primaryBackgroundColor%>",
                            fontColor: "<%=UIConstants.textColor%>",
                            animationEnabled: true,
                            colorSet: "colorSet2",
                            /*title: {
                                text: 'Top Geo By User Acquisition',
                                fontColor: "<%=UIConstants.textColor%>",
                                fontSize: 30,


                            },*/
                            toolTip: {
                                fontColor: "<%=UIConstants.textColor%>",
                                backgroundColor: "<%=UIConstants.primaryBackgroundColor%>",
                            },
                            legend: {
                                fontColor: "<%=UIConstants.textColor%>",
                                fontFamily: "'Lucida Console', Monaco, monospace",
                                fontSize: 14,
                                cursor: "pointer",
                            },
                            data: [
                                {
                                    type: "pie",
                                    percentFormatString: "#0.##",
                                    toolTipContent: "{indexLabel}: {y}",
                                    cursor: "pointer",
                                    indexLabelFontColor: "<%=UIConstants.textColor%>",
                                    showInLegend: true,
                                    legendText: "{indexLabel}:#percent%",
                                    dataPoints: data,
                                    fontColor: "<%=UIConstants.textColor%>",
                                }
                            ]
                        });
                    chart.render();


                }
            },

            error: function () {
                console.log("Error");
            }
        });
    }

    function installCountMap() {
        /*console.log("inside top10ConversionOfferWise fn");*/
        $.ajax({
            url: "/install/installCountWiseGeo?appId=${app.id}&dateRange=" + dateRange,
            type: "GET",
            dataType: 'json',

            success: function (response) {
                if (response.installGeoWise.length == 0) {
                    $('.noData').show();
                    document.getElementById("geoWiseInstallMap").style.display = "none";
                } else {
                    $('.noData').hide();
                    document.getElementById("geoWiseInstallMap").style.display = "block";
                    var data = {};
                    for (var i = 0; i < response.installGeoWise.length; i++) {
                        data[response.installGeoWise[i].countryCode] = {gdp: response.installGeoWise[i].count}
                    }
                    new svgMap({
                        targetElementID: 'geoWiseInstallMap',
                        data: {
                            data: {
                                gdp: {
                                    name: 'Total Install Count',
                                    format: '{0}',
                                    thousandSeparator: ',',
                                },
                            },
                            applyData: 'gdp',
                            values: data
                        }
                    });

                }
            },

            error: function () {
                console.log("Error");
            }
        });
    }
</script>
</body>

</html>
