
<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.etreetech.panel.constants.UIConstants" %>
<%@ page import="com.etreetech.panel.reports.ClickDetailsDTO" %>
<%@ page import="com.etreetech.panel.beans.Click" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<html lang="en">

<head>
    <meta charset="utf-8"/>
    <link rel="apple-touch-icon" sizes="76x76" href="../../assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="../../assets/img/adjar.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>
        Click Details
    </title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport'/>
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- CSS Files -->
    <link href="../../assets/css/material-dashboard.css?v=2.1.0" rel="stylesheet"/>
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="../../assets/demo/demo.css" rel="stylesheet"/>
    <link href="../../assets/css/style-sheet-adjar.css?v=2.1.0" rel="stylesheet" />
    <%--responsive css file--%>
    <link href="../../assets/css/responsive-modal-pages.css" rel="stylesheet"/>
    <style>
        td,th{
            border-bottom:1px solid <%=UIConstants.textColor%> !important;
        }
    </style>
</head>


<body class="<%=UIConstants.primaryTextColorClass%>"
      style="background-color:<%=UIConstants.primaryBackgroundColor%>;font-family:<%=UIConstants.primaryFontFamily%>;">
<% String url = request.getContextPath(); %>
<div class="wrapper ">
    <jsp:include page="adminSidebar.jsp"/>
    <div class="main-panel">
        <!-- Navbar -->
        <jsp:include page="adminHeader.jsp"/>

        <!-- End Navbar -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6" style="margin-top:7px">
                        <div class="cardDiv">
                            <div class="cardSize">
                                <i class="material-icons iconSize" >assignment</i>
                            </div>
                            <h2 class="cardHeading">Basic Details</h2>
                        </div>
                        <div class="card <%=UIConstants.primaryTextColorClass%>"
                             style="background-color:<%=UIConstants.primaryCardBackground%>;font-family:<%=UIConstants.primaryFontFamily%>">

                            <div class="card-body ">
                                <p> <span style = "color:<%=UIConstants.headingTextColor%>;font-weight:bold;">Campaign:</span> (ID:${click.offerId}) &nbsp;${click.offerName}</p>
                                <p> <span style = "color:<%=UIConstants.headingTextColor%>;font-weight:bold;">Publisher:</span>(ID:${click.pubId}) &nbsp;${click.publisherName}</p>
                                <p> <span style = "color:<%=UIConstants.headingTextColor%>;font-weight:bold;">Advertiser:</span>(ID:${click.advertiserId}) &nbsp;${click.advertiserName}</p>
                                <p> <span style = "color:<%=UIConstants.headingTextColor%>;font-weight:bold;">Click ID:</span> ${click.id}</p>
                                <p> <span style = "color:<%=UIConstants.headingTextColor%>;font-weight:bold;">Click Timestamp:</span> &nbsp;<strong class="clcikCreateTime"></strong><%--${click.creationDateTime}--%></p>
                                <%ClickDetailsDTO click= (ClickDetailsDTO) request.getAttribute("click");%>
                                <p> <span style = "color:<%=UIConstants.headingTextColor%>;font-weight:bold;">Is Gross:</span>&nbsp;<%if(click.isGross() || click.getClickMessage().equals("Passed: APPROVED")){%>true<%}else{%>false<%}%></p>
                                <p> <span style = "color:<%=UIConstants.headingTextColor%>;font-weight:bold;">Click Message:</span> &nbsp;<%if(click.getClickMessage()==null){%>NA<%}else{%><%=click.getClickMessage()%><%}%></p><br>

                            </div>
                            <div class="card-footer text-right">

                            </div>

                        </div>
                    </div>
                    <div class="col-md-6 deviceInfo" style="margin-top:30px">
                        <div class="cardDiv">
                            <div class="cardSize">
                                <i class="fa fa-mobile" aria-hidden="true" style="font-size: 36px;margin-left:13px;margin-top:3px"></i>
                            </div>
                            <h2 class="cardHeading">Device Info</h2>
                        </div>
                        <div class="card <%=UIConstants.primaryTextColorClass%>"
                             style="background-color:<%=UIConstants.primaryCardBackground%>;font-family:<%=UIConstants.primaryFontFamily%>">
                            <div class="card-body ">
                                <p><span style = "color:<%=UIConstants.headingTextColor%>;font-weight:bold;">Click IP:</span> ${click.ip}</p>
                                <p><span style = "color:<%=UIConstants.headingTextColor%>;font-weight:bold;">State:</span> NA</p>
                                <p><span style = "color:<%=UIConstants.headingTextColor%>;font-weight:bold;">City:</span> NA</p>
                                <p><span style = "color:<%=UIConstants.headingTextColor%>;font-weight:bold;">Country:</span> ${click.country}</p>
                                <%--                                <p>ISP :</p>--%>
                                <p><span style = "color:<%=UIConstants.headingTextColor%>;font-weight:bold;">OS:</span> ${click.deviceId.operatingSystemNameVersion}</p>
                                <p><span style = "color:<%=UIConstants.headingTextColor%>;font-weight:bold;">Browser:</span> ${click.deviceId.agentNameVersion}</p>
                                <p><span style = "color:<%=UIConstants.headingTextColor%>;font-weight:bold;">Device:</span> ${click.deviceId.deviceName} </p>
                            </div>
                            <div class="card-footer text-right">

                            </div>

                        </div>
                    </div>
                </div><br><br><br>

                <div class="row">
                    <div class="col-md-6" style="margin-top:-65px">
                        <div class="cardDiv">
                            <div class="cardSize">
                                <i class="fa fa-map-marker" style="font-size:28px;margin-left:13px;margin-top:6px"></i>
                            </div>
                            <h2 class="cardHeading" style="margin-top:1px">Track Params</h2>
                        </div>
                        <div class="card <%=UIConstants.primaryTextColorClass%>"
                             style="background-color:<%=UIConstants.primaryCardBackground%>;font-family:<%=UIConstants.primaryFontFamily%>">

                            <div class="card-body ">
                                <p><span style = "color:<%=UIConstants.headingTextColor%>;font-weight:bold;">Id :</span> ${click.id}</p>
                                <p><span style = "color:<%=UIConstants.headingTextColor%>;font-weight:bold;">Idfa :</span> ${click.idfa}</p>
                                <p><span style = "color:<%=UIConstants.headingTextColor%>;font-weight:bold;">Gaid :</span> ${click.gaid}</p>
                                <p><span style = "color:<%=UIConstants.headingTextColor%>;font-weight:bold;">S1 :</span> ${click.s1}</p>
                                <p><span style = "color:<%=UIConstants.headingTextColor%>;font-weight:bold;">S2 :</span> ${click.s2}</p>
                                <p><span style = "color:<%=UIConstants.headingTextColor%>;font-weight:bold;">S3 :</span> ${click.s3}</p>
                                <p><span style = "color:<%=UIConstants.headingTextColor%>;font-weight:bold;">S4 :</span> ${click.s4}</p>
                                <p><span style = "color:<%=UIConstants.headingTextColor%>;font-weight:bold;">S5 :</span> ${click.s5}</p>
                                <p><span style = "color:<%=UIConstants.headingTextColor%>;font-weight:bold;">Source :</span> ${click.source}</p>
                            </div>
                            <div class="card-footer text-right">

                            </div>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="cardDiv">
                            <div class="cardSize">
                                <i class="fa fa-dollar" style="font-size:28px;margin-left:13px;margin-top:6px"></i>
                            </div>
                            <h2 class="cardHeading" style="margin-top:1px">Payout & Revenue</h2>
                        </div>
                        <div class="card <%=UIConstants.primaryTextColorClass%>"
                             style="background-color:<%=UIConstants.primaryCardBackground%>;font-family:<%=UIConstants.primaryFontFamily%>">
                            <div class="card-body ">
                                <p><span style = "color:<%=UIConstants.headingTextColor%>;font-weight:bold;">Payout :</span> ${click.payout}</p>
                                <p><span style = "color:<%=UIConstants.headingTextColor%>;font-weight:bold;">Revenue :</span> ${click.revenue}</p>
                                <p><span style = "color:<%=UIConstants.headingTextColor%>;font-weight:bold;">Currency :</span> <i>USD</i></p><br>
                                <h4 class="card-title <%=UIConstants.primaryTextColorClass%>"
                                    style="font-family:font-family:<%=UIConstants.primaryFontFamily%>; font-weight: bold">
                                    Smart Payout

                                </h4>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead class="<%=UIConstants.primaryTextColorClass%>">
                                        <th>
                                            Click
                                        </th>
                                        <th>
                                            Revenue
                                        </th>
                                        <th>
                                            Payout
                                        </th>

                                        </thead>
                                        <tbody>

                                        <tr>
                                            <td>
                                                <i class="fa fa-globe" aria-hidden="true" style="font-size: 28px"></i>
                                            </td>
                                            <td style="font-size: 20px">
                                                ${click.revenue}
                                            </td>
                                            <td style="font-size: 20px">
                                                ${click.payout}
                                            </td>

                                        </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="card-footer text-right">

                            </div>

                        </div>
                    </div>
                </div>

                <!-- <footer class="footer">

            </footer> -->
            </div>
        </div>
    </div>
    <!--   Core JS Files   -->
    <script src="../../assets/js/core/jquery.min.js"></script>
    <script src="../../assets/js/core/popper.min.js"></script>
    <script src="../../assets/js/core/bootstrap-material-design.min.js"></script>
    <script src="../../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
    <!-- Plugin for the momentJs  -->
    <script src="../../assets/js/plugins/moment.min.js"></script>
    <!--  Plugin for Sweet Alert -->
    <script src="../../assets/js/plugins/sweetalert2.js"></script>
    <!-- Forms Validations Plugin -->
    <script src="../../assets/js/plugins/jquery.validate.min.js"></script>
    <!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
    <script src="../../assets/js/plugins/jquery.bootstrap-wizard.js"></script>
    <!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
    <script src="../../assets/js/plugins/bootstrap-selectpicker.js"></script>
    <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
    <script src="../../assets/js/plugins/bootstrap-datetimepicker.min.js"></script>
    <!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
    <script src="../../assets/js/plugins/jquery.dataTables.min.js"></script>
    <!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
    <script src="../../assets/js/plugins/bootstrap-tagsinput.js"></script>
    <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
    <script src="../../assets/js/plugins/jasny-bootstrap.min.js"></script>
    <!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
    <script src="../../assets/js/plugins/fullcalendar.min.js"></script>
    <!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
    <script src="../../assets/js/plugins/jquery-jvectormap.js"></script>
    <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
    <script src="../../assets/js/plugins/nouislider.min.js"></script>
    <!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
    <!-- Library for adding dinamically elements -->
    <script src="../../assets/js/plugins/arrive.min.js"></script>
    <!-- Chartist JS -->
    <script src="../../assets/js/plugins/chartist.min.js"></script>
    <!--  Notifications Plugin    -->
    <script src="../../assets/js/plugins/bootstrap-notify.js"></script>
    <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="../../assets/js/material-dashboard.js?v=2.1.0" type="text/javascript"></script>
    <!-- Material Dashboard DEMO methods, don't include it in your project! -->
    <script src="../../assets/demo/demo.js"></script>


    <script>
        $(document).ready(function () {
            $().ready(function () {
                $sidebar = $('.sidebar');

                $sidebar_img_container = $sidebar.find('.sidebar-background');

                $full_page = $('.full-page');

                $sidebar_responsive = $('body > .navbar-collapse');

                window_width = $(window).width();

                fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

                if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
                    if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
                        $('.fixed-plugin .dropdown').addClass('open');
                    }

                }

                $('.fixed-plugin a').click(function (event) {
                    // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
                    if ($(this).hasClass('switch-trigger')) {
                        if (event.stopPropagation) {
                            event.stopPropagation();
                        } else if (window.event) {
                            window.event.cancelBubble = true;
                        }
                    }
                });

                $('.fixed-plugin .active-color span').click(function () {
                    $full_page_background = $('.full-page-background');

                    $(this).siblings().removeClass('active');
                    $(this).addClass('active');

                    var new_color = $(this).data('color');

                    if ($sidebar.length != 0) {
                        $sidebar.attr('data-color', new_color);
                    }

                    if ($full_page.length != 0) {
                        $full_page.attr('filter-color', new_color);
                    }

                    if ($sidebar_responsive.length != 0) {
                        $sidebar_responsive.attr('data-color', new_color);
                    }
                });

                $('.fixed-plugin .background-color .badge').click(function () {
                    $(this).siblings().removeClass('active');
                    $(this).addClass('active');

                    var new_color = $(this).data('background-color');

                    if ($sidebar.length != 0) {
                        $sidebar.attr('data-background-color', new_color);
                    }
                });

                $('.fixed-plugin .img-holder').click(function () {
                    $full_page_background = $('.full-page-background');

                    $(this).parent('li').siblings().removeClass('active');
                    $(this).parent('li').addClass('active');


                    var new_image = $(this).find("img").attr('src');

                    if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
                        $sidebar_img_container.fadeOut('fast', function () {
                            $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
                            $sidebar_img_container.fadeIn('fast');
                        });
                    }

                    if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
                        var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

                        $full_page_background.fadeOut('fast', function () {
                            $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
                            $full_page_background.fadeIn('fast');
                        });
                    }

                    if ($('.switch-sidebar-image input:checked').length == 0) {
                        var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
                        var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

                        $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
                        $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
                    }

                    if ($sidebar_responsive.length != 0) {
                        $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
                    }
                });

                $('.switch-sidebar-image input').change(function () {
                    $full_page_background = $('.full-page-background');

                    $input = $(this);

                    if ($input.is(':checked')) {
                        if ($sidebar_img_container.length != 0) {
                            $sidebar_img_container.fadeIn('fast');
                            $sidebar.attr('data-image', '#');
                        }

                        if ($full_page_background.length != 0) {
                            $full_page_background.fadeIn('fast');
                            $full_page.attr('data-image', '#');
                        }

                        background_image = true;
                    } else {
                        if ($sidebar_img_container.length != 0) {
                            $sidebar.removeAttr('data-image');
                            $sidebar_img_container.fadeOut('fast');
                        }

                        if ($full_page_background.length != 0) {
                            $full_page.removeAttr('data-image', '#');
                            $full_page_background.fadeOut('fast');
                        }

                        background_image = false;
                    }
                });

                $('.switch-sidebar-mini input').change(function () {
                    $body = $('body');

                    $input = $(this);

                    if (md.misc.sidebar_mini_active == true) {
                        $('body').removeClass('sidebar-mini');
                        md.misc.sidebar_mini_active = false;

                        $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

                    } else {

                        $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');

                        setTimeout(function () {
                            $('body').addClass('sidebar-mini');

                            md.misc.sidebar_mini_active = true;
                        }, 300);
                    }

                    // we simulate the window Resize so the charts will get updated in realtime.
                    var simulateWindowResize = setInterval(function () {
                        window.dispatchEvent(new Event('resize'));
                    }, 180);

                    // we stop the simulation of Window Resize after the animations are completed
                    setTimeout(function () {
                        clearInterval(simulateWindowResize);
                    }, 1000);

                });
            });
        });
    </script>
    <script>

        $(document).ready(function () {
            $('#datatables').DataTable({
                "pagingType": "full_numbers",
                "lengthMenu": [
                    [5, 10, 20, -1],
                    [5, 10, 20, "All"]
                ],

                language: {
                    search: "_INPUT_",
                    searchPlaceholder: "Search records",
                }
            });

            var table = $('#datatables').DataTable();

            // Edit record
            table.on('click', '.edit', function () {
                $tr = $(this).closest('tr');
                var data = table.row($tr).data();
                alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
            });

            // Delete a record
            table.on('click', '.remove', function (e) {
                $tr = $(this).closest('tr');
                table.row($tr).remove().draw();
                e.preventDefault();
            });

            //Like record
            table.on('click', '.like', function () {
                alert('You clicked on Like button');
            });
        });

        function formatDate() {
            let conversionDate = new Date(Date.parse("${conversion.creationDateTime}"));
            let clickDate = new Date(Date.parse("${click.creationDateTime}"));
            var difference = (conversionDate - clickDate) / 1000;
            $("#clickToConversion").html("<span style = \"color:#F29D26;\">AttributionClickDTO.java to Conversion Time:</span> " + difference +" s");
            $("#conversionCreateTime").html(conversionDate);
            const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
            $(".clcikCreateTime").html(clickDate.toLocaleString('en-US',options)+" "+clickDate.toLocaleTimeString('en-US'));

        }

        formatDate();
    </script>

</body>

</html>