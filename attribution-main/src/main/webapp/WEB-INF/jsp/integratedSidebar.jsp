<%@ page import="com.attribution.panel.constants.UIConstants" %>
<!DOCTYPE html>
<%--<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<html lang="en">
<meta http-equiv="refresh" content="86400; url=/login?logout">
<link href="../../assets/css/style-sheet-attribution.css?v=2.1.0" rel="stylesheet"/>
<style>
    :root {
        --primaryButtonColor: <%=UIConstants.primaryButtonColor%>;
        --borderColor: <%=UIConstants.borderColor%>;
        --textColor: <%=UIConstants.textColor%>;
        --arrowColor: <%=UIConstants.arrowColor%>;
        --hoverButtonBgColor: <%=UIConstants.hoverButtonBgColor%>;
        --dropDownHoverColor: <%=UIConstants.dropDownHoverColor%>;
        --dropDownSelectedColor: <%=UIConstants.dropDownSelectedColor%>;
        --dropDownMenuColor: <%=UIConstants.dropDownMenuColor%>;
        --paginatehoverTextColor: <%=UIConstants.paginateHoverTextColor%>;
        --paginatehoverBgColor: <%=UIConstants.paginateHoverBgColor%>;
        --tableIconBgColor: <%=UIConstants.tableIconBgColor%>;
        --tableIconTextColor: <%=UIConstants.tableIconTextColor%>;
        --primaryBackgroundColor: <%=UIConstants.primaryBackgroundColor%>;
        --primaryCardBackground: <%=UIConstants.primaryCardBackground%>;
        --bannerTextColor: <%=UIConstants.bannerTextColor%>;
        --bannerBgColor: <%=UIConstants.bannerBgColor%>;
        --sideBgColor: <%=UIConstants.sideBgColor%>;
        --scrollbarBgColor: <%=UIConstants.scrollbarBgColor%>;
        --primaryButtonTextColor: <%=UIConstants.primaryButtonTextColor%>;
        --chartTextColor: <%=UIConstants.chartTextColor%>;
        --chartBoxBgColor: <%=UIConstants.chartBoxBgColor%>;
        --primaryBootstrapColor: <%=UIConstants.primaryBootstrapColor%>;
        --dataPickerBgColor: <%=UIConstants.dataPickerBgColor%>;
    }

    /*.sidebar .sidebar-wrapper .active a{
        font-size:13px !important;
        -moz-box-shadow: 1px 1px 3px 2px #ffffff;
        -webkit-box-shadow: 1px 1px 3px 2px #ffffff;
        box-shadow:1px 1px 3px 2px #ffffff;
    }
    .sidebar .sidebar-wrapper .active i{
        color: var(--primaryButtonTextColor) !important;
    }*/
    #sidebarTextColor a, #sidebarTextColor i {
        color: #ffffff;
    }

    .appSelectBox .bootstrap-select .btn, .appSelectBox .filter-option-inner-inner {
        color: white !important;
    }

    .appSelectBox .filter-option {
        border-color: #AAAAAA !important;
    }

</style>
</head>
<div class="sidebar" data-color="sidebarColor"
     style="font-family:<%=UIConstants.primaryFontFamily%>;height:100%;background-color:#272b30">
    <div class="logo">
        <a href="#" class="simple-text logo-normal" style="height:40px">
            <p style="font-size:30px;margin-left:30px;margin-top:10px"><b>Apps</b><b style="color:#FD9811">Track</b></p>
        </a>
    </div>
    <div class="logo" style="margin-top:0;height: 70px">
        <div style="margin-top:-10px"><a href="${contextPath}/adminConsole/apps"><img class="appIcon"
                                                                                      src='/imageAppLogoDisplay?id=${app.id}'
                                                                                      style="height:40px;width:45px;border-radius:10px;margin-top:10px;margin-left:20px"/>
            <c:set var="string1" value="${app.name}"/>
            <c:set var="appName" value="${fn:substring(string1, 0, 17)}"/>
            <div title="${app.name}"
                 style="margin-top:-32px;color:white;margin-left:75px;font-weight: bold;font-size: 18px">${appName}</div>
        </a></div>
        <br>
        <div class="appSelectBox" style="margin-left:10px">
            <div class="dropdown bootstrap-select show-tick show <%=UIConstants.primaryTextColorClass%>">
                <select class="selectpicker" data-style="btn bg-transparent" data-width="235px" title="Change App"
                        data-live-search="true" onchange="location = this.value;">
                    <option title="Change App" style="display:none"></option>
                    <c:forEach items="${appsList}" var="appsList">
                        <option value='/adminConsole/event?id=${appsList.id}'
                                data-thumbnail="/adminConsole/event?id=${appsList.id}">
                            <c:set var="string1" value="${appsList.name}"/>
                            <c:set var="appName" value="${fn:substring(string1, 0, 15)}"/>
                                ${appName}...
                        </option>
                    </c:forEach>
                </select>
            </div>
        </div>
    </div>
    <br><br>
    <div class="sidebar-wrapper" style="height:90%;z-index:1">
        <ul class="nav">
            <li class="nav-item ${param.dash}">
                <a class="nav-link" href="${contextPath}/adminConsole/dashboard?id=${app.id}">
                    <i class="fa fa-line-chart" aria-hidden="true" style="color:#FFFFFF"></i>
                    <p style="color:#FFFFFF;font-weight: bold">DashBoard</p>
                </a>
            </li>
            <li class="nav-item ${param.event}">
                <a class="nav-link" href="${contextPath}/adminConsole/event?id=${app.id}">
                    <i class="material-icons" style="color:#FFFFFF">library_books</i>
                    <p style="color:#FFFFFF;font-weight: bold">Event</p>
                </a>
            </li>
            <li class="nav-item ${param.allEvent}">
                <a class="nav-link" href="${contextPath}/adminConsole/eventReport?id=${app.id}">
                    <i class="fa fa-address-card" aria-hidden="true" style="color:#FFFFFF"></i>
                    <p style="color:#FFFFFF;font-weight: bold">Event Report</p>
                </a>
            </li>

            <li class="nav-item ${param.allReport}">
                <a class="nav-link" href="${contextPath}/adminConsole/getAllReport?id=${app.id}">
                    <i class="fa fa-address-card" aria-hidden="true" style="color:#FFFFFF"></i>
                    <p style="color:#FFFFFF;font-weight: bold">All Reports</p>
                </a>
            </li>
        </ul>
    </div>
</div>
</html>