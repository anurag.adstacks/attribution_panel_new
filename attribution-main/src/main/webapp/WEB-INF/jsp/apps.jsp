<!DOCTYPE html>
<html lang="en">
<%@ page import="com.attribution.panel.constants.UIConstants" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<head>

    <meta charset="utf-8"/>
    <link rel="apple-touch-icon" sizes="76x76" href="../../asset/img/apple-icon.png">
    <link rel="icon" type="image/png" href="../../asset/img/logoAttribution.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>
        App
    </title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport'/>
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- CSS Files -->
    <link href="../../asset/css/material-dashboard.css?v=2.1.0" rel="stylesheet"/>
    <!-- Responsive -->
    <link href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="../asset/css/style-sheet-attribution.css" rel="stylesheet"/>
    <style>
        .pager div {
            float: left;
        }

        .items div {
            padding: 0;
            margin: 0
        }
    </style>
</head>

<body class="fixed-left blue_dark drawer drawer--right " data-theme='blue_dark' onload="searchPlatforms()"
      style="background-color:<%=UIConstants.primaryBackgroundColor%>;font-family:<%=UIConstants.primaryFontFamily%>;">
<div class="wrapper ">
    <jsp:include page="adminSidebar.jsp">
        <jsp:param value="active" name="apps"/>
    </jsp:include>
    <div class="main-panel">
        <!-- Navbar -->
        <jsp:include page="adminHeader.jsp"/>
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 ml-auto mr-auto text-center <%=UIConstants.primaryTextColorClass%>">
                        <h2 class="title <%=UIConstants.primaryTextColorClass%>"
                            style="font-family: 'Times New Roman', Times, serif; font-weight: bold;text-transform: capitalize;margin-top: -10px">
                            Application Integration</h2>
                    </div>
                </div>
                <div class="row" style="margin-left:15px;margin-top:10px">
                    <div>
                        <div class="input-group no-border">
                            <input class="searchBox form-control <%=UIConstants.primaryTextColorClass%>" placeholder="Search..."></div>
                        <div id="showingInfo" class="well" style="color:<%=UIConstants.textColor%>;margin-left: 0 !important;margin-top: 20px !important;"></div>
                    </div>

                </div>
                <div class="row" style="margin-top:-20px">
                <div class="col-lg-12 col-md-4 addAppsButton">
                    <button class="filterButton pull-right" data-target="#exampleModalAd"
                            data-toggle="modal" id="newApp" style="margin-top:-28px;">
                        <i class="fa fa-plus" aria-hidden="true" style="width:auto;font-size:15px;margin-left:0px"><b
                                style="margin-left:6px;">ADD APP&nbsp;</b></i>
                    </button>
                    <div>
                    </div>
                </div>
                </div>
            </div>

            <div class="row" id="content"></div>
            <div id="pagingControls" style="float:right;"></div>

            <div style="margin-top:40px"></div>
            <!-- Modal -->
            <div class="modal fade" id="exampleModalAd" tabindex="-1" role="dialog" aria-hidden="true"
                 data-backdrop="static">
                <div class="modal-dialog " role="document">
                    <div class="card modal-content <%=UIConstants.primaryTextColorClass%>"
                         style="background-color:<%=UIConstants.primaryModalCardHeaderBackground%>;width:420px">
                        <div class="modalClose">
                            <i class="fa fa-times" data-dismiss="modal" aria-label="Close"></i>
                        </div>
                        <div class="card-header card-header-<%=UIConstants.primaryColorClass%> card-header-icon"
                             style="height:70px">
                            <div class="cardDiv">
                                <div class="cardSize">
                                    <i class="material-icons iconSize" style="margin-left: 4px;margin-top: 5px">assignment</i>
                                </div>
                                <h4 class="card-title <%=UIConstants.primaryTextColorClass%>"
                                    style="font-family: <%=UIConstants.primaryFontFamily%>; font-weight: bold;margin-left:58px"
                                    id="modalHeading">Create App</h4>
                                <p class="card-category" style="margin-left:60px">Enter Application Information</p>
                            </div>

                        </div>
                        <div class="card-body"
                             style="font-family: <%=UIConstants.primaryFontFamily%>; width: 420px; background-color: <%=UIConstants.primaryCardModalBackground%>">
                            <br>
                            <div id="modalForm">
                                <form id="saveDetails" onsubmit="saveDetails()">
                                    <div style="margin-left:-20px;margin-top:-30px;">
                                        <input type="text" name="id" value="0" id="appId"
                                               style="display:none">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label class=" col-form-label <%=UIConstants.primaryTextColorClass%>"
                                                       style="font-weight:bold;margin-left:37px;margin-top:-20px">App
                                                    Name *</label>
                                            </div>
                                            <div class="col-sm-12">
                                                <input type="text" class="inputText" id="name" name="name"
                                                       style="margin-left:20px;margin-top:2px;width:380px;height:45px"
                                                       required>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-top:-10px">
                                            <div class="col-md-12">
                                                <label class="col-form-label <%=UIConstants.primaryTextColorClass%>"
                                                       style="font-weight:bold;margin-left:35px;margin-top:13px">App
                                                    Description</label>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <textarea type="text" rows="4" class="textArea"
                                                                          id="description"
                                                                          name="description"
                                                                          style="margin-left:20px;margin-top:-5px;padding:6px 10px;width:380px"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-top:-20px">
                                            <div class="col-md-12">
                                                <label class="col-form-label <%=UIConstants.primaryTextColorClass%>"
                                                       style="font-weight:bold;margin-left:35px;margin-top:13px">App
                                                    Link</label>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                                <textarea type="text" rows="4" class="textArea"
                                                                          id="link"
                                                                          name="link"
                                                                          style="margin-left:20px;margin-top:-5px;padding:6px 10px;width:380px"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-top:-20px">
                                            <div class="col-md-12">
                                                <label class="col-form-label <%=UIConstants.primaryTextColorClass%>" style="font-weight:bold;margin-left:35px;margin-top:13px">Game Link</label>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <textarea type="text" rows="4" class="textArea" id="gameLink" name="gameLink" style="margin-left:20px;margin-top:-5px;padding:6px 10px;width:380px"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-left:14px">
                                            <div col="col-sm-12" style="margin-left:40px;margin-top:-4px">
                                                <label class=" col-form-label <%=UIConstants.primaryTextColorClass%>"
                                                       style="font-weight:bold;position:absolute">Select
                                                    OS *:</label>
                                                <div class="selectOS" style="margin-left:94px;margin-top:1px">
                                                    <div class="dropdown bootstrap-select show-tick show <%=UIConstants.primaryTextColorClass%>">
                                                        <select id="os" name="os"
                                                                class="selectpicker <%=UIConstants.primaryTextColorClass%>"
                                                                data-size="7" data-style="btn"
                                                                title="Select OS" data-width="172px">
                                                            <option value="ANDROID">ANDROID</option>
                                                            <option value="IOS">IOS</option>
                                                        </select></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-top: 5px">
                                            <label class=" col-form-label <%=UIConstants.primaryTextColorClass%>"
                                                   style="font-weight:bold;margin-left:40px;margin-top:0;position:absolute">Select
                                                App Logo:</label>
                                            <div class="col-sm-6" style="margin-left:150px;margin-top:15px">
                                                <input type="file" id="appLogo" name="appLogo">
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row" style="border-top:1px solid #423b3a;width:420px;margin-left:0px;margin-top: -10px">
                                            <div class="col-sm-12">
                                                <button type="submit" class="submitButton pull-right"
                                                        style="margin-right:20px;margin-top:15px">Save
                                                </button></div></div>
                                        <%--</form>--%>
                                        <!-- Image loader -->
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>

            </div>

            <div class="modal fade" id="uploadImageFile" tabindex="-1" role="dialog" aria-hidden="true"
                 data-backdrop="static">
                <div class="modal-dialog " role="document">
                    <div class="card modal-content <%=UIConstants.primaryTextColorClass%>"
                         style="background-color:<%=UIConstants.primaryButtonColor%>;width:500px;margin-left:-140px;border-radius:10px">
                        <div class="modalClose" style="margin-right:3.1px;margin-top:1px">
                            <i class="fa fa-times" data-dismiss="modal" aria-label="Close" style="color: #ffffff"></i>
                        </div>
                        <div class="card-header card-header-<%=UIConstants.primaryColorClass%> card-header-icon"
                             style="height:50px">
                            <div class="card-icon" style="margin-top: 0;margin-left: -15px">
                                <i class="fa fa-file-text" aria-hidden="true" style="font-size: 20px;"></i>
                            </div>
                            <h4 class="card-title text-white" style="font-family: <%=UIConstants.primaryFontFamily%>; font-weight: bold;margin-left:4%;position: absolute">Upload And Download Image</h4>
                            <%--<p class="card-category">in list <a id="user" style="color: #000000"></a></p>--%>

                        </div>
                        <div class="card-body"
                             style="font-family: <%=UIConstants.primaryFontFamily%>; font-weight: bold;background-color:#F4F5F7;width:500px;border-bottom-left-radius:10px;border-bottom-right-radius:10px">
                            <p>${message}</p>
                            <div>
                                <form id="uploadImageDocument" class="form-horizontal"
                                      onsubmit="uploadImageDocument()">
                                    <div class="form-group">
                                        <input type="text" id="appId1" name="appId" value=""
                                               style="display:none">
                                    </div>

                                    <i class="fa  fa-cloud-upload" style="margin-top:-8px;position: absolute;margin-left: 20px;font-size:20px"><b style="margin-left:15px;font-size:16px">Upload Image</b></i><br>
                                    <div style="margin-left:10px;;margin-top: 10px">
                                        <label for="documentImageFile" style="margin-left: 45px;color:<%=UIConstants.textColor%>;font-weight: 550">Select a file:</label>
                                        <input type="file" id="documentImageFile" name="documentImageFile" required>
                                    </div><br><br>
                                    <div class="row" style="margin-top:-40px;">
                                        <div class="col-sm-12">
                                            <button type="submit" class="submitButton pull-left"
                                                    id="submitDocument" style="margin-left:55px;margin-top:15px">Save File
                                            </button></div></div><br>
                                </form>
                                <i class="fa fa-cloud-download" style="margin-top:-8px;position: absolute;margin-left: 20px;font-size: 20px"><b style="margin-left:15px;font-size:16px">Download Image</b></i>
                                <div id="document" style="margin-left:20px;margin-top: 30px"></div><br><br><br>
                            </div>


                            <!-- Image loader -->
                        </div>

                    </div>
                    <!-- 									</div>
            -->
                </div>
            </div>

        </div>
    </div>
</div>

<!--   Core JS Files   -->
<script src="../../asset/js/core/jquery.min.js"></script>
<script src="../../asset/js/core/popper.min.js"></script>
<script src="../../asset/js/core/bootstrap-material-design.min.js"></script>
<script src="../../asset/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!-- Plugin for the momentJs  -->
<script src="../../asset/js/plugins/moment.min.js"></script>
<!--  Plugin for Sweet Alert -->
<script src="../../asset/js/plugins/sweetalert2.js"></script>
<!-- Forms Validations Plugin -->
<script src="../../asset/js/plugins/jquery.validate.min.js"></script>
<!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
<script src="../../asset/js/plugins/jquery.bootstrap-wizard.js"></script>
<!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script src="../../asset/js/plugins/bootstrap-selectpicker.js"></script>
<!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
<script src="../../asset/js/plugins/bootstrap-datetimepicker.min.js"></script>
<!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
<script src="../../asset/js/plugins/jquery.dataTables.min.js"></script>
<!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
<script src="../../asset/js/plugins/bootstrap-tagsinput.js"></script>
<!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="../../asset/js/plugins/jasny-bootstrap.min.js"></script>
<!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
<script src="../../asset/js/plugins/fullcalendar.min.js"></script>
<!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
<script src="../../asset/js/plugins/jquery-jvectormap.js"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="../../asset/js/plugins/nouislider.min.js"></script>
<!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
<!-- Library for adding dinamically elements -->
<script src="../../asset/js/plugins/arrive.min.js"></script>
<!--  Google Maps Plugin    -->
<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!-- Chartist JS -->
<script src="../../asset/js/plugins/chartist.min.js"></script>
<!--  Notifications Plugin    -->
<script src="../../asset/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="../../asset/js/material-dashboard.js?v=2.1.0" type="text/javascript"></script>
<script src="../asset/js/paginga.jquery.js"></script>
<script src="../asset/js/flexible.pagination.js"></script>


<script>
    function saveDetails() {
        var myform = document.getElementById("saveDetails");
        var fd = new FormData(myform);
        event.preventDefault();
        $.ajax({
            url: "/saveApp",
            data: fd,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            beforeSend: function () {
            },
            success: function (response) {
                showNotification('top', 'left', 'Your application has been submitted successfully');
                /*location.reload();*/
                setTimeout(function () {
                    window.location.reload();
                }, 1000);
            },
            complete: function () {
                // Hide image container

            },

            error: function () {
                console.log("Error");
            }
        });
    }
    function showNotification(from, align, message) {

        $.notify({
            icon: "add_alert",
            message: message
        }, {
            type: 'success',
            timer: 3000,
            placement: {
                from: from,
                align: align
            }
        });
    }
</script>


<script>
    function searchPlatforms() {
        var flexiblePagination = $('#content').flexiblePagination({
            ajax: {
                url: '/getAppsListAjax',
                onSuccessCallBack: function (response) {
                    var platformListBody = "";
                    for (var i = 0; i < response.data.length; i++) {
                        var image="";
                        if(response.data[i].appLogo!=null)
                            image='<img src="/imageAppLogoDisplay?id=' + response.data[i].id + '" style="height:55px;width:60px;border-radius:10px;margin-top:8px"/>';
                        else
                            image='<img src="/asset/img/blank-profile-picture-973460__480.png" style="height:55px;width:60px;border-radius:10px;margin-top:8px"/>';
                        var icon = "";
                        if (response.data[i].os === 'ANDROID')
                            icon = '<i class="fa fa-android" aria-hidden="true" style="position:absolute;margin-top:40px;margin-left:10px"></i>'
                        else
                            icon = '<i class="fa fa-apple" style="position:absolute;margin-top:40px;margin-left:10px"></i>';
                        var name;
                        if(response.data[i].name.length>20)
                            name= '<span style="margin-right:20px" data-toggle="tooltip" title="' + response.data[i].name + '">' + response.data[i].name.substring(0, 20)+"..."+ '...</span>';
                        else
                            name=response.data[i].name
                        platformListBody +='<div class="col-lg-4 col-md-4">'
                            + '<div class="card" style= "background-color:#136AA0;border:1px solid black;border-radius:15px;height:150px;font-weight:bold;">'
                            + '<h4 class="card-title" style="margin-top:10px"><b style="color:white;margin-left:15px;font-size:22px;">'
                            + response.data[i].os
                            + '<div style="float: right;margin-top:0;margin-right:12px"><i class="material-icons" onclick="uploadDocumentFile(' + response.data[i].id + ')" data-toggle="modal" data-target="#uploadImageFile" style="color:white;font-size:20px;width: 20px;height: 20px;cursor: pointer;margin-right: 5px">upload</i>'
                            + '<i class="material-icons" style="color:white;font-size:20px;cursor: pointer" onclick="editApp('+response.data[i].id+')" data-toggle="modal" data-target="#exampleModalAd">app_registration</i></div>'
                            + '</b></h4>'
                            + '<a style="cursor: pointer" href="/adminConsole/dashboard?id=' + response.data[i].id + '"><h4 class="card-title" style="border-top:1px solid white;border-bottom:1px solid white;height:75px;margin-top:5px"><b style="color:#FFFFFF;margin-left:15px;font-size:22px;position: absolute;margin-top:10px">'
                            + '<div class="row" style="margin-top:-8px">'
                            + '<div class="img-container" style="margin-left:30px">'
                            + image+ icon + '</div> <div class="" style="border-left:1px solid #aaa;color:#fff;margin-left:130px;margin-top:-58px;"><div style="margin-left:20px;margin-top:10px;color:#FFFFFF">' + name + '</div>'
                            + '</div></div></div></a>'
                            + '</div></div>'
                            + '</b></h4>'
                            + '</div> </a>'
                            + '</div>';
                    }
                    return platformListBody;
                },
                onFailureCallBack: function (error) {
                    alert("Ajax Error  - See Console for details!");
                    console.log(error);
                }
            },
            itemsPerPage: 1000000000,
            itemSelector: 'div.items:visible',
            pagingControlsContainer: '#pagingControls',
            showingInfoSelector: '#showingInfo',
            css: {
                btnNumberingClass: 'btnNumbering',
                btnFirstClass: 'first',
                btnLastClass: 'last',
                btnNextClass: 'btn btn-sm nextPre',
                btnPreviousClass: 'btn btn-sm nextPre'
            }
        });
        flexiblePagination.pagingContainer = '#content';
        flexiblePagination.getController().onPageClick = function (pageNum, e) {
        };
    }

    function editApp(e){
        document.getElementById("modalHeading").innerHTML = "Edit App";
        $.ajax({
            url: "/findAppById?appId=" + e,
            contentType: 'json',
            type: 'GET',
            success: function (response) {
                try {
                    document.getElementById("appId").value =response.app.id;
                    document.getElementById("name").value =response.app.name;
                    document.getElementById("description").value =response.app.description;
                    document.getElementById("link").value =response.app.link;
                    document.getElementById("os").value =response.app.os;
                    $(":input").change();
                } catch (error) {
                    console.log(error);
                }
            },

            error: function (jqXHR) {
                var o = $.parseJSON(jqXHR.responseText);
                window.open('/adminConsole/error?status=' + o.status + '&message=' + o.error, "_self")
            }
        });
    }

    $("#newApp").on("click", function () {
        document.getElementById("modalHeading").innerHTML = "Create App";
        $('#saveDetails')[0].reset();
        $(":input").change();
    });

    function uploadImageDocument() {
        $("#submitDocument").prop('disabled', true);
        var myform = document.getElementById("uploadImageDocument");
        var fd = new FormData(myform);
        event.preventDefault();
        $.ajax({
            url: "/uploadImageDocument",
            data: fd,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            beforeSend: function () {


            },
            success: function (response) {
                showNotification('top', 'left', 'Your application has been submitted successfully');
                /*location.reload();*/
                setTimeout(function () {
                    window.location.reload();
                }, 1000);
            },
            complete: function () {
                // Hide image container

            },

            error: function (jqXHR) {
                var o = $.parseJSON(jqXHR.responseText);
                window.open('/adminConsole/error?status=' + o.status + '&message=' + o.error, "_self")
            }
        });
    }

    function uploadDocumentFile(e){
        document.getElementById("appId1").value=e;
        $.ajax({
            url: "/findDownloadImageFile?appId=" + e,
            contentType: 'json',
            type: 'GET',
            success: function (response) {
                var tableBodyResponse = "";
                tableBodyResponse += '<table  class="display nowrap datatables1" style="width:100%">'
                    + '<thead>'
                    + '<tr>'
                    + '<th>S.No.</th>'
                    + '<th>Name</th>'
                    + '<th>Action</th>'
                    + '</tr>'
                    + '</thead>'
                    + '<tbody>';
                for (var i = 0; i < response.uploadAppImage.length; i++) {
                    var serialNo=i+1;/*star.png*/
                    tableBodyResponse += '<tr>'
                        + '<td><div style="margin-left:40px"></div>' +serialNo+ '</td>'
                        + '<td><div style="margin-left:100px"></div>' + response.uploadAppImage[i].fileName +'</td>'
                        + '<td><a href="' + response.uploadAppImage[i].url + '" target="_blank" data-toggle = "tooltip" title = "Print" style="font-size:16px;margin-left: 3%;height: 30px;padding: 0 10px;border-radius: 10px;color: #ffffff"> <span class="glyphicon glyphicon-file icon-green" style="font-size: 23px;color:dodgerblue"></span><i class="material-icons" style="color: #0a6ebd">print</i></a></div></td>'
                        + '</tr>'
                }
                tableBodyResponse += '</tbody>'
                    + '</table>';
                document.getElementById("document").innerHTML = tableBodyResponse;
            },
            error: function (jqXHR) {
                /*var o = $.parseJSON(jqXHR.responseText);
                window.open('/adminConsole/error?status=' + o.status + '&message=' + o.error, "_self")*/
            }
        });
    }
</script>
</body>

</html>