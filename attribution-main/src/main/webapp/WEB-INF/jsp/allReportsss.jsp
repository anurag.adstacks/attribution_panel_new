<%@ page import="java.util.List" %>
<%@ page import="com.attribution.panel.constants.UIConstants" %>
<%@ page import="com.attribution.panel.console.dto.ReportFilterDTO" %>
<%@ page
        import="com.adstacks.attribution.bean.*,com.attribution.panel.constants.UIConstants,com.attribution.panel.enums.CountryEnum,com.attribution.panel.enums.OS" %>
<%@ page import="javafx.scene.control.Alert" %>
<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html lang="en">

<head>
    <meta charset="utf-8"/>
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="../assets/img/adjar.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>Report</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport'/>
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- CSS Files -->
    <link href="../assets/css/material-dashboard.css?v=2.1.0" rel="stylesheet"/>
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="../assets/demo/demo.css" rel="stylesheet"/>
    <link href="../assets/css/style-sheet-adjar.css?v=2.1.0" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="../assets/css/daterangepicker.css"/>
    <script src="../assets/js/core/jquery.min.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
    <script src="https://rawgit.com/dbrekalo/attire/master/dist/js/build.min.js"></script>
    <script src="../assets/fastselect.standalone.js"></script>
    <link rel="stylesheet" href="../assets/fastselect.min.css">
    <%--loader CSS Files--%>
    <link href="../assets/css/loader.css" rel="stylesheet"/>
    <%--loader JS Files--%>
    <script src="../assets/js/loader.js"></script>
    <link href="../dist/css/select2.css" rel="stylesheet"/>
    <%--responsive css file--%>
    <link href="../../assets/css/responsive-modal-pages.css" rel="stylesheet"/>
    <style>
        .dataTables_length::after {
            top: -2px;
            right: 65px;
        }

        .dataTables_paginate {
            margin-top: -30px !important;
        }

        .dataTables_info {
            margin-left: 20px;
        }

        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            padding: 3px 10px !important;
        }

        .hide {
            display: none;
        }

        .dataTables_wrapper .dt-buttons {
            position: absolute;
            float: none;
        !important;
            text-align: left !important;
            margin-top: -83px;
            margin-right: 53%;

        }

        @media screen and (min-width: 576px) and (max-width: 767px) {
            .dt-buttons {
                width: 285px !important;
                margin-top: -108px !important;
                margin-left: -115px;

            }

            .filterButtonRow1 {
                margin-right: -75px !important;
            }

            .reportRange {
                position: absolute;
                right: 50px !important;
                top: -5px;

            }

            .navbar {
                z-index: 1 !important;
            }
        }

        @media screen and (max-width: 575px) {

            .filterButtonRow1 {
                margin-left: 300px !important;
                margin-top: -2px !important;
                float: left !important;
            }

            .dt-buttons {
                margin-right: 100px !important;
                margin-top: -85px !important;
            }

        }

        @media screen and (min-width: 768px) and (max-width: 812px) {
            .dt-buttons {
                width: 285px !important;
                margin-top: -75px !important;
                margin-left: -110px;
            }

            .filterButtonRow1 {
                margin-right: -75px !important;
            }
        }

        @media screen and (min-width: 813px) and (max-width: 1039px) {
            .dt-buttons {
                width: 285px !important;
                left: 200px !important;
            }

            #reportrangeinput {
                width: 245px !important;
                font-size: 12px;
            }

            .filterButtonRow1 {
                margin-right: -340px !important;
            }
        }

        @media screen and (min-width: 1040px) {
            .dt-buttons {
                width: 285px !important;
                left: 200px !important;
            }
        }

    </style>
</head>
<%
    String cp = request.getContextPath();
%>

<body class="" style="background-color: <%=UIConstants.primaryBackgroundColor%>;overflow-x:hidden">
<div class="wrapper">

    <%--<jsp:include page="adminSidebar.jsp">
        <jsp:param value="active" name="allReport"/>
        <jsp:param value="show" name="reportsTab"/>
    </jsp:include>--%>

    <jsp:include page="adminSidebar.jsp">
        <jsp:param value="active" name="allReport"/>
    </jsp:include>



    <div class="main-panel">
        <!-- Navbar -->

        <!-- End Navbar -->
        <div class="content">
            <div class="container-fluid">
                <div class="cardDiv">
                    <div class="cardSize">
                        <i class="material-icons iconSize">assignment</i>
                    </div>
                    <h2 class="cardHeading">All Report</h2>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card <%=UIConstants.primaryTextColorClass%>"
                             style="background-color: <%=UIConstants.primaryCardBackground%>;">
                            <div class="card-header card-header-warning card-header-icon allReportHeader">

                                <div class="row pull-right reportRange" style="margin-top:10px;margin-right:10px">
                                    <div class="col-sm-9">
                                        <!-- <i class="fa fa-caret-down"></i> -->
                                        <div class=" form-group float-right text-dark" id="reportrange"
                                             style="margin-top: -53px;">
                                            <input readonly type="text"
                                                   class="<%=UIConstants.primaryTextColorClass%>"
                                                   id="reportrangeinput"
                                                   name="daterange"
                                                   value="${filters.daterange}"
                                                   style="cursor: pointer; background-color: <%=UIConstants.primaryBackgroundColor%>; border:1px solid <%=UIConstants.borderColor%>;border-radius:5px; width: 295px;padding:2px 6px">
                                        </div>
                                    </div>
                                </div>

                                <div class="row pull-right filterButtonRow1"
                                     style="margin-top:-4px;margin-right:-380px">
                                    <div class="col-sm-7" style="">
                                        <button class="btn btn-<%=UIConstants.primaryColorClass%> filterButton pull-right"
                                                data-toggle="modal" id="modalAddRule"
                                                style="cursor: pointer;margin-top:-39px;height:28px;border-radius:5px;border:1.8px solid #aaa"
                                                data-target="#exampleModalAd">
                                            <i class="fa fa-search" aria-hidden="true" data-toggle="modal"
                                               data-target="#addRule"
                                               id="reset"
                                               style="margin-left:0px;margin-top:-16px;width:auto;font-size:16px"><b
                                                    style="margin-left:6px">Search&nbsp;</b></i>
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <c:set var="data" value="${groupBy}" scope="request"/>
                            <% List<String> stringList = (List<String>) request.getAttribute("groupBy");%>

                            <div class="card-body">
                                <div class="toolbar <%=UIConstants.primaryTextColorClass%>"
                                     style="font-family: <%=UIConstants.primaryFontFamily%>;">
                                    <!--        Here you can write extra buttons/actions for the toolbar              -->

                                    <!-- Modal -->
                                    <div class="modal fade" id="exampleModalAd" tabindex="-1" role="dialog"
                                         aria-labelledby="exampleModalLongTitle" aria-hidden="true"
                                         data-backdrop="static">
                                        <div class="modal-dialog" role="document" id="exampleModalAd1">
                                            <div class="card modal-content <%=UIConstants.primaryTextColorClass%> allReportContent"
                                                 style="background-color: <%=UIConstants.primaryModalCardHeaderBackground%>; width: 600px;margin-left:20%">
                                                <div class="modalClose">
                                                    <i class="fa fa-times" data-dismiss="modal" aria-label="Close"></i>
                                                </div>

                                                <div class="card-header card-header-<%=UIConstants.primaryColorClass%> card-header-icon"
                                                     style="height:80px">
                                                    <div class="card-icon">
                                                        <i class="material-icons">dashboard</i>
                                                    </div>
                                                    <h4 class="card-title <%=UIConstants.primaryTextColorClass%>"
                                                        style="font-family: <%=UIConstants.primaryFontFamily%>; font-weight: bold"
                                                        id="modalHeading1">Search Report</h4>
                                                    <p class="card-category">Enter Information</p>
                                                </div>

                                                <div class="card-body allReportCard"
                                                     style="font-family: <%=UIConstants.primaryFilterFontFamilyTable%>; width: 600px; background-color: <%=UIConstants.primaryCardModalBackground%>">
                                                    <div id="modalForm">

                                                        <form id="formSubmit" method="post"
                                                              action="/adminConsole/allReport">

                                                            <input type="hidden" name="daterange" id="dateHiddenField">

                                                            <div class="row">
                                                                <div class="col-sm-6" style="margin-top: 9px">
                                                                    <label class="<%=UIConstants.primaryTextColorClass%>"
                                                                           style="font-family: <%=UIConstants.primaryFontFamily%>; font-weight: bold; margin-left: 0px; position: absolute">Select
                                                                        App </label><i class="fa fa-list"
                                                                                       style="margin-left: 118px;font-size:16px"></i>
                                                                </div>
                                                                <div class="col-lg-6 allReportSearchOffers"
                                                                     style="margin-left:-150px;margin-top:-2px">
                                                                    <select class="multipleSelect" multiple
                                                                            data-width="380px" id="appSelect"
                                                                            name="apps">
                                                                        <c:forEach var="apps" items="${apps}">
                                                                            <option value="${apps.id}" selected>
                                                                                (Id:${apps.id})${apps.name}</option>
                                                                        </c:forEach>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <%--                                                        <sec:authorize access="!hasRole('PUBLISHER')">--%>
                                                            <div class="row">
                                                                <div class="col-sm-6" style="margin-top: 9px">
                                                                    <label class="<%=UIConstants.primaryTextColorClass%>"
                                                                           style="font-family: <%=UIConstants.primaryFontFamily%>; font-weight: bold; margin-left: 0px; position: absolute">Select
                                                                        Partner </label> <i class="material-icons"
                                                                                            style="margin-top: -5px; margin-left: 115px; position: absolute;">person</i>&nbsp;
                                                                </div>
                                                                <div class="col-lg-6"
                                                                     style="margin-left:-150px;margin-top:-2px">
                                                                    <select class="multipleSelect" multiple
                                                                            data-width="380px" id="partnerSelect"
                                                                            name="partners">
                                                                        <c:forEach var="partner" items="${partners}">
                                                                            <option value="${partner.id}" selected>
                                                                                (Id:${partner.id})${partner.company}</option>
                                                                        </c:forEach>
                                                                    </select>

                                                                </div>
                                                            </div>
                                                            <br>
                                                            <%--                                                        </sec:authorize>--%>
                                                            <%--                                                        <sec:authorize access="hasRole('PUBLISHER')">--%>
                                                            <input type="hidden" name="partners"
                                                                   value="${partner.id}">
                                                            <br>
                                                            <%--                                                        </sec:authorize>--%>

                                                            <br>

                                                            <%--                                                        <sec:authorize access="!hasRole('PUBLISHER')">--%>
                                                            <div class="row">
                                                                <div class="col-sm-6" style="margin-top: 9px">
                                                                    <label class="<%=UIConstants.primaryTextColorClass%>"
                                                                           style="font-family: <%=UIConstants.primaryFontFamily%>; font-weight: bold; margin-left: 0px; position: absolute">Group
                                                                        BY</label> <i class="fa fa-users"
                                                                                      style="margin-left: 118px;font-size:16px"></i>
                                                                </div>
                                                                <div class="col-lg-6 allReportGroupBy"
                                                                     style="margin-left:-150px;margin-top:-1px">
                                                                    <select class="multipleSelect" multiple id="groupBy"
                                                                            data-width="380px"
                                                                            name="groupBy">
                                                                        <option value="App" <%=(stringList == null || stringList.contains("App")) ? "selected" : ""%>>
                                                                            App
                                                                        </option>
                                                                        <option value="Partner" <%=(stringList != null && stringList.contains("Partner")) ? "selected" : ""%>>
                                                                            Partner
                                                                        </option>

                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <%--                                                        </sec:authorize>--%>

                                                            <div class="row">
                                                                <div class="col-lg-6"
                                                                     style="margin-left:-103px;margin-top:0">
                                                                    <div class="col-sm-6"
                                                                         style="margin-top: 9px;margin-left: 85px">
                                                                        <label class="<%=UIConstants.primaryTextColorClass%>"
                                                                               style="font-family: <%=UIConstants.primaryFontFamily%>; font-weight: bold; margin-left: 0px; position: absolute">Select
                                                                            Columns</label> <i class="fa fa-table"
                                                                                               style="margin-left: 123px"></i>
                                                                    </div>
                                                                    <div class="col-lg-6 allReportDataColumns"
                                                                         style="margin-left:90%;margin-top:-33px">
                                                                        <select class="multipleSelect <%=UIConstants.primaryTextColorClass%>"
                                                                                data-style="btn btn-warning"
                                                                                multiple="" title="Select Column"
                                                                                data-size="7"
                                                                                data-width="380px"
                                                                                tabindex="-98" id="extSelect"
                                                                                name="columns">
                                                                            <!-- selected="selected" -->
                                                                            <%
                                                                                ReportFilterDTO reportFilterDTO = (ReportFilterDTO) request.getAttribute("filters");
                                                                                System.out.println("ReportFilterDT0O0201 " + reportFilterDTO);
                                                                                List<String> columns = reportFilterDTO.getColumns();
                                                                                System.out.println("columns00201 " + columns);
                                                                            %>

                                                                            <option value="7" <%=(columns.contains("7") || columns.isEmpty()) ? "selected" : ""%>>
                                                                                Click
                                                                            </option>
                                                                            <%--                                                                        <sec:authorize access="!hasRole('PUBLISHER')">--%>
                                                                            <option value="8" <%=(columns.contains("8") || columns.isEmpty()) ? "selected" : ""%>>
                                                                                Conversion
                                                                            </option>
                                                                            <%--                                                                        </sec:authorize>--%>
                                                                            <%--                                                                        <sec:authorize access="hasRole('PUBLISHER')">--%>
                                                                            <option value="16" <%=(columns.contains("16") || columns.isEmpty()) ? "selected" : ""%>>
                                                                                Ratio
                                                                            </option>
                                                                            <%--                                                                        </sec:authorize>--%>
                                                                            <%--                                                                        <sec:authorize access="!hasRole('PUBLISHER')">--%>
                                                                            <option value="9" <%=(columns.contains("9")) ? "selected" : ""%>>
                                                                                Cancelled
                                                                                Conversion
                                                                            </option>
                                                                            <option value="10" <%=(columns.contains("10")) ? "selected" : ""%>>
                                                                                Pending
                                                                                Conversion
                                                                            </option>
                                                                            <option value="11" <%=(columns.contains("11") || columns.isEmpty()) ? "selected" : ""%>>
                                                                                Gross Conversion
                                                                            </option>
                                                                            <option value="12"<%=(columns.contains("12")) ? "selected" : ""%> >
                                                                                Approved Revenue
                                                                            </option>
                                                                            <option value="13" <%=(columns.contains("13")) ? "selected" : ""%> >
                                                                                Cancelled
                                                                                Revenue
                                                                            </option>
                                                                            <option value="14" <%=(columns.contains("14")) ? "selected" : ""%> >
                                                                                Pending Revenue
                                                                            </option>
                                                                            <option value="15" <%=(columns.contains("15")) ? "selected" : ""%> >
                                                                                Gross Revenue
                                                                            </option>
                                                                            <%--                                                                        </sec:authorize>--%>
                                                                            <option value="16" <%=(columns.contains("16") || columns.isEmpty()) ? "selected" : ""%> >
                                                                                Ratio
                                                                            </option>
                                                                            <option value="17"<%=(columns.contains("17")) ? "selected" : ""%> >
                                                                                Day Timestamp
                                                                            </option>
                                                                            <option value="18"<%=(columns.contains("18")) ? "selected" : ""%> >
                                                                                Rejected Clicks
                                                                            </option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row bottomLineAllReport"
                                                                 style="border-top:1px solid #423b3a;width:600px;margin-left:-20px;margin-top:0">
                                                                <div class="col-sm-12">
                                                                    <button type="submit" id="filterSubmitButton"
                                                                            class="btn btn-<%=UIConstants.primaryColorClass%> btn-round pull-right submitButton"
                                                                            style="margin-right:35px;margin-top:10px">
                                                                        Apply
                                                                    </button>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </form>
                                                        <!-- Image loader -->
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                    <!-- </form> -->
                                </div>

                            </div>


                            <%--<div class="table-responsive allReportTable" style="width:98%;margin-left:10px\
                            +xea>
                                <table id="datatables"
                                       class="dataTable display nowrap <%=UIConstants.primaryTextColorClass%>"
                                       style="font-family: <%=UIConstants.primaryFontFamily%>;width:100%"
                                       cellspacing="0">
                                    <thead>--%>

                            <div class="table-responsive allReportTable" style="width:98%;margin-left:10px">
                                <table id="datatables" class="dataTable display nowrap <%=UIConstants.primaryTextColorClass%>"
                                       style="font-family: <%=UIConstants.primaryFontFamily%>;width:100%" cellspacing="0">
                                    <thead>
                                    <tr class="<%=UIConstants.primaryTextColorClass%>" style="font-weight: bold; ">
                                        <c:if test="${fn:length(data)==0 || fn:contains(data, 'App')}">
                                            <th style="font-weight:bold;" <%=(stringList != null && !stringList.contains("App")) ? "class= hide" : ""%>>
                                                App ID
                                            </th>
                                            <th style="font-weight:bold;" <%=(stringList != null && !stringList.contains("App")) ? "class= hide" : ""%>>
                                                App Name
                                            </th>
                                        </c:if>

                                        <%--<c:if test="${fn:contains(data, 'Smartlink')}">
                                            <th style="font-weight:bold;" <%=(stringList != null && !stringList.contains("SmartLink")) ? "class= hide" : ""%>>
                                                Smartlink ID
                                            </th>
                                            <th style="font-weight:bold;" <%=(stringList != null && !stringList.contains("SmartLink")) ? "class= hide" : ""%>>
                                                Smartlink Name
                                            </th>
                                        </c:if>--%>

                                        <c:if test="${fn:contains(data, 'Partner')}">
                                            <th style="font-weight:bold" <%=(stringList != null && !stringList.contains("Partner")) ? "class= hide" : ""%>>
                                                Partner Id
                                            </th>
                                            <th style="font-weight:bold" <%=(stringList != null && !stringList.contains("Partner")) ? "class= hide" : ""%>>
                                                Partner Name
                                            </th>
                                        </c:if>

                                        <th style="font-weight:bold" <%=(!columns.contains("7") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                            Click
                                        </th>
                                        <%--                                    <sec:authorize access="!hasRole('PUBLISHER')">--%>
                                        <th style="font-weight:bold" <%=(!columns.contains("8") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                            Approved Conversion
                                        </th>
                                        <%--                                    </sec:authorize>--%>
                                        <%--                                    <sec:authorize access="hasRole('PUBLISHER')">--%>
                                        <th style="font-weight:bold" <%=(!columns.contains("8") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                            Conversion
                                        </th>
                                        <%--                                    </sec:authorize>--%>
                                        <%--                                    <sec:authorize access="!hasRole('PUBLISHER')">--%>
                                        <th style="font-weight:bold" <%=(!columns.contains("9") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                            Cancelled Conversion
                                        </th>
                                        <th style="font-weight:bold" <%=(!columns.contains("10") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                            Pending Conversion
                                        </th>
                                        <th style="font-weight:bold" <%=(!columns.contains("11") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                            Gross Conversion
                                        </th>

                                        <th style="font-weight:bold" <%=(!columns.contains("12") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                            Approved Revenue
                                        </th>
                                        <th style="font-weight:bold" <%=(!columns.contains("13") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                            Cancelled Revenue
                                        </th>
                                        <th style="font-weight:bold" <%=(!columns.contains("14") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                            Pending Revenue
                                        </th>
                                        <th style="font-weight:bold" <%=(!columns.contains("15") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                            Gross Revenue
                                        </th>
                                        <%--                                    </sec:authorize>--%>
                                        <%--                                    <sec:authorize access="!hasRole('PUBLISHER')">--%>
                                        <th style="font-weight:bold" <%=(!columns.contains("16") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                            Ratio
                                        </th>
                                        <%--                                    </sec:authorize>--%>
                                        <%--                                    <sec:authorize access="hasRole('PUBLISHER')">--%>
                                        <th style="font-weight:bold" <%=(!columns.contains("16") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                            Payout
                                        </th>
                                        <%--                                    </sec:authorize>--%>

                                        <th style="font-weight:bold" <%=(!columns.contains("17") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                            Day Timestamp
                                        </th>
                                        <th style="font-weight:bold" <%=(!columns.contains("18") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                            Rejected Clicks
                                        </th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <c:forEach var="responseList" items="${list}">
                                        <tr>
                                            <c:if test="${fn:length(data)==0 || fn:contains(data, 'App')}">
                                                <td <%=(stringList != null && !stringList.contains("App")) ? "class= hide" : ""%>>
                                                    <div style="margin-left:150px"></div>
                                                        ${responseList.appId}</td>
                                                <td <%=(stringList != null && !stringList.contains("App")) ? "class= hide" : ""%>>
                                                    <div style="margin-left:100px"></div>
                                                    <a href='/adminConsole/viewoffer?id=${responseList.appId}'
                                                       target="_blank"
                                                       style="color:<%=UIConstants.primaryHyperLinkColor%>;font-weight:bold;">${responseList.appName}</a>
                                                </td>
                                            </c:if>

                                                <%--<c:if test="${fn:contains(data, 'SmartLinks')}">
                                                    <td <%=(stringList != null && !stringList.contains("SmartLinks")) ? "class= hide" : ""%>>
                                                        <div style="margin-left:150px"></div>
                                                            ${responseList.smartLinkId}</td>
                                                    <td <%=(stringList != null && !stringList.contains("SmartLinks")) ? "class= hide" : ""%>>
                                                        <div style="margin-left:150px"></div>
                                                            ${responseList.smartLinkName}</td>
                                                </c:if>--%>

                                            <c:if test="${fn:contains(data, 'Partner')}">
                                                <td <%=(stringList != null && !stringList.contains("Partner")) ? "class= hide" : ""%>>
                                                    <div style="margin-left:150px"></div>
                                                        ${responseList.partnerId}</td>
                                                <td <%=(stringList != null && !stringList.contains("Partner")) ? "class= hide" : ""%>>
                                                    <div style="margin-left:150px"></div>
                                                        ${responseList.partnerName}</td>
                                            </c:if>

                                            <td <%=(!columns.contains("7") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                                <div style="margin-left:150px"></div>
                                                    ${responseList.click}</td>
                                            <td <%=(!columns.contains("8") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                                <div style="margin-left:190px"></div>
                                                    ${responseList.approvedConversions}</td>

                                                <%--                                        <sec:authorize access="!hasRole('PUBLISHER')">--%>
                                            <td <%=(!columns.contains("9") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                                <div style="margin-left:200px"></div>
                                                    ${responseList.cancelledConversions}</td>
                                            <td <%=(!columns.contains("10") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                                <div style="margin-left:190px"></div>
                                                    ${responseList.pendingConversions}</td>
                                            <td <%=(!columns.contains("11") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                                <div style="margin-left:190px"></div>
                                                    ${responseList.grossConversions}</td>
                                            <td <%=(!columns.contains("12") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                                <div style="margin-left:190px"></div>
                                                    ${responseList.approvedRevenue}</td>
                                            <td <%=(!columns.contains("13") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                                <div style="margin-left:190px"></div>
                                                    ${responseList.cancelledRevenue}</td>
                                            <td <%=(!columns.contains("14") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                                <div style="margin-left:190px"></div>
                                                    ${responseList.pendingRevenue}</td>
                                            <td <%=(!columns.contains("15") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                                <div style="margin-left:190px"></div>
                                                    ${responseList.grossRevenue}</td>
                                                <%--                                        </sec:authorize>--%>
                                            <td <%=(!columns.contains("16") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                                <div style="margin-left:190px"></div>
                                                    ${responseList.ratio}</td>
                                            <td <%=(!columns.contains("17") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                                <div style="margin-left:190px"></div>
                                                    ${responseList.dayTimestamp}</td>
                                            <td <%=(!columns.contains("18") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                                <div style="margin-left:190px"></div>
                                                    ${responseList.rejectedClicks}</td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>

<!--   Core JS Files   -->
<!-- multiselect -->

<%--<script src="../assets/js/core/popper.min.js"></script>--%>
<%--<script src="../assets/js/core/bootstrap-material-design.min.js"></script>--%>
<%--<script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>--%>
<%--<!-- Plugin for the momentJs  -->--%>
<%--<script src="../assets/js/plugins/moment.min.js"></script>--%>
<%--<!--  Plugin for Sweet Alert -->--%>
<%--<script src="../assets/js/plugins/sweetalert2.js"></script>--%>
<%--<!-- Forms Validations Plugin -->--%>
<%--<script src="../assets/js/plugins/jquery.validate.min.js"></script>--%>
<%--<!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->--%>
<%--<script src="../assets/js/plugins/jquery.bootstrap-wizard.js"></script>--%>
<%--<!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->--%>
<%--<script src="../assets/js/plugins/bootstrap-selectpicker.js"></script>--%>
<%--<!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->--%>
<%--<script src="../assets/js/plugins/bootstrap-datetimepicker.min.js"></script>--%>
<%--<!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->--%>
<%--<script src="../assets/js/plugins/jquery.dataTables.min.js"></script>--%>
<%--<script type="text/javascript" src="../assets/js/jquery.spring-friendly.js"></script>--%>
<%--<!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->--%>
<%--<script src="../assets/js/plugins/bootstrap-tagsinput.js"></script>--%>
<%--<!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->--%>
<%--<script src="../assets/js/plugins/jasny-bootstrap.min.js"></script>--%>
<%--<!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->--%>
<%--<script src="../assets/js/plugins/fullcalendar.min.js"></script>--%>
<%--<!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->--%>
<%--<script src="../assets/js/plugins/jquery-jvectormap.js"></script>--%>
<%--<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->--%>
<%--<script src="../assets/js/plugins/nouislider.min.js"></script>--%>
<%--<!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->--%>
<%--<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>--%>
<%--<!-- Library for adding elements -->--%>
<%--<script src="../assets/js/plugins/arrive.min.js"></script>--%>
<%--<script src="../assets/js/plugins/chartist.min.js"></script>--%>
<%--<!--  Notifications Plugin    -->--%>
<%--<script src="../assets/js/plugins/bootstrap-notify.js"></script>--%>
<%--<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->--%>
<%--<script src="../assets/js/material-dashboard.js?v=2.1.0" type="text/javascript"></script>--%>
<%--<!-- Material Dashboard DEMO methods, don't include it in your project! -->--%>
<%--<script src="../assets/demo/demo.js"></script>--%>
<%--<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>--%>
<%--<script src="../dist/js/select2.js"></script>--%>


<script src="../assets/js/core/popper.min.js"></script>
<script src="../assets/js/core/bootstrap-material-design.min.js"></script>
<script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!-- Plugin for the momentJs  -->
<script src="../assets/js/plugins/moment.min.js"></script>
<!--  Plugin for Sweet Alert -->
<script src="../assets/js/plugins/sweetalert2.js"></script>
<!-- Forms Validations Plugin -->
<script src="../assets/js/plugins/jquery.validate.min.js"></script>
<!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
<script src="../assets/js/plugins/jquery.bootstrap-wizard.js"></script>
<!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script src="../assets/js/plugins/bootstrap-selectpicker.js"></script>
<!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
<script src="../assets/js/plugins/bootstrap-datetimepicker.min.js"></script>
<!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
<script src="../assets/js/plugins/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../assets/js/jquery.spring-friendly.js"></script>
<!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
<script src="../assets/js/plugins/bootstrap-tagsinput.js"></script>
<!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="../assets/js/plugins/jasny-bootstrap.min.js"></script>
<!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
<script src="../assets/js/plugins/fullcalendar.min.js"></script>
<!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
<script src="../assets/js/plugins/jquery-jvectormap.js"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="../assets/js/plugins/nouislider.min.js"></script>
<!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
<!-- Library for adding elements -->
<script src="../assets/js/plugins/arrive.min.js"></script>
<script src="../assets/js/plugins/chartist.min.js"></script>
<!--  Notifications Plugin    -->
<script src="../assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="../assets/js/material-dashboard.js?v=2.1.0" type="text/javascript"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="../assets/demo/demo.js"></script>

<script type="text/javascript"
        src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<script>
    $(document).ready(function () {
        $().ready(function () {
            $sidebar = $('.sidebar');

            $sidebar_img_container = $sidebar.find('.sidebar-background');

            $full_page = $('.full-page');

            $sidebar_responsive = $('body > .navbar-collapse');

            window_width = $(window).width();

            fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

            if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
                if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
                    $('.fixed-plugin .dropdown').addClass('open');
                }

            }

            $('.fixed-plugin a').click(function (event) {
                // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
                if ($(this).hasClass('switch-trigger')) {
                    if (event.stopPropagation) {
                        event.stopPropagation();
                    } else if (window.event) {
                        window.event.cancelBubble = true;
                    }
                }
            });

            $('.fixed-plugin .active-color span').click(function () {
                $full_page_background = $('.full-page-background');

                $(this).siblings().removeClass('active');
                $(this).addClass('active');

                var new_color = $(this).data('color');

                if ($sidebar.length != 0) {
                    $sidebar.attr('data-color', new_color);
                }

                if ($full_page.length != 0) {
                    $full_page.attr('filter-color', new_color);
                }

                if ($sidebar_responsive.length != 0) {
                    $sidebar_responsive.attr('data-color', new_color);
                }
            });

            $('.fixed-plugin .background-color .badge').click(function () {
                $(this).siblings().removeClass('active');
                $(this).addClass('active');

                var new_color = $(this).data('background-color');

                if ($sidebar.length != 0) {
                    $sidebar.attr('data-background-color', new_color);
                }
            });

            $('.fixed-plugin .img-holder').click(function () {
                $full_page_background = $('.full-page-background');

                $(this).parent('li').siblings().removeClass('active');
                $(this).parent('li').addClass('active');


                var new_image = $(this).find("img").attr('src');

                if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
                    $sidebar_img_container.fadeOut('fast', function () {
                        $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
                        $sidebar_img_container.fadeIn('fast');
                    });
                }

                if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
                    var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

                    $full_page_background.fadeOut('fast', function () {
                        $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
                        $full_page_background.fadeIn('fast');
                    });
                }

                if ($('.switch-sidebar-image input:checked').length == 0) {
                    var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
                    var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

                    $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
                    $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
                }

                if ($sidebar_responsive.length != 0) {
                    $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
                }
            });

            $('.switch-sidebar-image input').change(function () {
                $full_page_background = $('.full-page-background');

                $input = $(this);

                if ($input.is(':checked')) {
                    if ($sidebar_img_container.length != 0) {
                        $sidebar_img_container.fadeIn('fast');
                        $sidebar.attr('data-image', '#');
                    }

                    if ($full_page_background.length != 0) {
                        $full_page_background.fadeIn('fast');
                        $full_page.attr('data-image', '#');
                    }

                    background_image = true;
                } else {
                    if ($sidebar_img_container.length != 0) {
                        $sidebar.removeAttr('data-image');
                        $sidebar_img_container.fadeOut('fast');
                    }

                    if ($full_page_background.length != 0) {
                        $full_page.removeAttr('data-image', '#');
                        $full_page_background.fadeOut('fast');
                    }

                    background_image = false;
                }
            });

            $('.switch-sidebar-mini input').change(function () {
                $body = $('body');

                $input = $(this);

                if (md.misc.sidebar_mini_active == true) {
                    $('body').removeClass('sidebar-mini');
                    md.misc.sidebar_mini_active = false;

                    $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

                } else {

                    $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');

                    setTimeout(function () {
                        $('body').addClass('sidebar-mini');

                        md.misc.sidebar_mini_active = true;
                    }, 300);
                }

                // we simulate the window Resize so the charts will get updated in realtime.
                var simulateWindowResize = setInterval(function () {
                    window.dispatchEvent(new Event('resize'));
                }, 180);

                // we stop the simulation of Window Resize after the animations are completed
                setTimeout(function () {
                    clearInterval(simulateWindowResize);
                }, 1000);

            });
        });

        var table = $('#datatables').DataTable({
            "pagingType": "full_numbers",
            "order": [[0, "desc"]],
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records"
            },
            dom: 'Blfrtip',
            buttons: [{
                extend: 'pdf', className: 'btnDatatable bg-transparent <%=UIConstants.textColor%>',
                exportOptions: {
                    columns: ':visible'
                }
            },
                {
                    extend: 'print', className: 'btnDatatable bg-transparent <%=UIConstants.textColor%>',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'csv', className: 'btnDatatable bg-transparent <%=UIConstants.textColor%>',
                    exportOptions: {
                        columns: ':visible', orthogonal: 'export'
                    }
                }, {
                    extend: 'excel', className: 'btnDatatable bg-transparent <%=UIConstants.textColor%>',
                    exportOptions: {
                        columns: ':visible'
                    }
                }]
        });
        jQuery('.dataTable').wrap('<div class="dataTables_scroll" />');
    });


    // var dateRange = '';
    var appFilter = '';
    var partnerFilter = '';
    var groupByFilter = '';
    var filter1 = '';

    var dateFromServer = "<%=reportFilterDTO.getDaterange()%>";
    /*console.log("dateFromServer:" + dateFromServer);*/

    $(function () {
        alert("this is report ranging");
        var start = (dateFromServer == "null" || dateFromServer == "") ? moment().subtract(6, 'days').startOf('day') :
            new Date(dateFromServer.split(" - ")[0]);
        var end = (dateFromServer == "null" || dateFromServer == "") ? moment().endOf('day') :
            new Date(dateFromServer.split(" - ")[1]);

        function cb(start, end) {
            alert("this is report range");
            document.getElementById("reportrangeinput").value = start.format('YYYY-MM-DD HH:mm:ss') + ' - ' + end.format('YYYY-MM-DD HH:mm:ss');
            dateFilter();
        }

        $('#reportrange').daterangepicker({

            startDate: start,
            endDate: end,
            ranges: {
                'Today': [moment().startOf('day'), moment().endOf('day')],
                'Yesterday': [moment().subtract(1, 'days').startOf('day'), moment().subtract(1, 'days').endOf('day')],
                'Last 7 Days': [moment().subtract(6, 'days').startOf('day'), moment().endOf('day')],
                'Last 30 Days': [moment().subtract(29, 'days').startOf('day'), moment().endOf('day')],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);
        cb(start, end);
        dateFilter();

    });

    document.getElementById("dateHiddenField").value = document.getElementById("reportrangeinput").value;

    function dateFilter() {
        alert("this is ree");
        /*console.log("Inside dateFilterWithFormData");*/
        /*console.log(document.getElementById("reportrangeinput").value);*/
        document.getElementById("dateHiddenField").value = document.getElementById("reportrangeinput").value;
        document.getElementById("formSubmit").submit();
    }


    $('body').on('change', '#extSelect', function (e) {
        alert("hello2");
        console.log("Inside filter1")
        e.preventDefault();
        filter1 = '';
        filter1 = $(this).val();
    });


    $('#click').click(function () {
        $("#exampleModalAd1").show();
    });


</script>
<script>


    $('select#smartLinkSelect')
        .change(
            function () {
                alert("okayji");
                smartLinkFilter = '';
                $(
                    'select#smartLinkSelect option:selected')
                    .each(function () {
                        smartLinkFilter += $(this).val() + ",";
                    });
                smartLinkFilter = smartLinkFilter.substring(0, smartLinkFilter.length - 1);

                alert("smartLinkFilter:" + smartLinkFilter);
            });


    $('select#appSelect')
        .change(
            function () {
                appFilter = '';
                $(
                    'select#appSelect option:selected')
                    .each(function () {
                        appFilter += $(this).val() + ",";
                    });
                appFilter = appFilter.substring(0, appFilter.length - 1);
                alert("smartLinkFilter: " + appFilter);
                /*console.log("appFilter:" + appFilter);*/
            });


    $('select#partnerSelect')
        .change(
            function () {
                alert("saltre");
                partnerFilter = '';
                $(
                    'select#partnerSelect option:selected')
                    .each(function () {
                        partnerFilter += $(this).val() + ",";
                    });
                partnerFilter = partnerFilter.substring(0, partnerFilter.length - 1);
                alert("okjijsp " + partnerFilter);
                /*console.log("partnerFilter:" + partnerFilter);*/
            });
    $('select#groupBy')
        .change(
            function () {
                alert("satlkhj");
                groupByFilter = '';
                $(
                    'select#groupBy option:selected')
                    .each(function () {
                        groupByFilter += $(this).val() + ",";
                    });
                groupByFilter = groupByFilter.substring(0, groupByFilter.length - 1);
                alert("okjijsp2 " + groupByFilter);
                /*console.log("groupByFilter:" + groupByFilter);*/
            });
</script>
<script>
    $(document).ready(function () {
        alert("extSelect");
        filter1 = '';
        filter1 = $("#extSelect").val();
        // initialise Datetimepicker and Sliders
        md.initFormExtendedDatetimepickers();
        if ($('.slider').length != 0) {
            md.initSliders();
        }
    });
</script>

<script>
    $('#groupBy').select2();
    $('#extSelect').select2();

    offerLoad('#exampleModalAd', '', '/findByOfferNames', '#appSelect');
    offerLoad('#exampleModalAd', '', '/adminConsole/findByPartnerCompany1', '#partnerSelect');

    function offerLoad(modalName, title, url, className) {
        alert("offerLoad");
        /*console.log("Inside offerLoad:" + className);*/
        var perPageRecord = 10;
        $(className).select2({
            dropdownParent: $(modalName),
            ajax: {
                url: url,
                type: 'GET',
                dataType: 'json',
                delay: 500,
                data: function (params) {
                    alert("helloo");
                    return {
                        search: params.term, // search term
                        page: params.page || 0,
                        size: perPageRecord//Per page records
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 0;
                    return {
                        results: data.results,
                        pagination: {
                            more: params.page < data.count_filtered - 1
                        }
                    };
                },
                cache: true
            },
            placeholder: title,
            minimumInputLength: 0
        });
    }

    $('.multiSelect').select2({});

</script>
</body>

</html>