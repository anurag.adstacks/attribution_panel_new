<%@ page import="com.attribution.panel.constants.UIConstants" %>
<%@ page import="com.attribution.panel.console.dto.ReportFilterDTO" %>
<%@ page import="java.util.List" %>
<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html lang="en">

<head>


    <meta charset="utf-8"/>
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="../../asset/img/logoAttribution.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>All Report</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport'/>
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- CSS Files -->
    <link href="../assets/css/material-dashboard.css?v=2.1.0" rel="stylesheet"/>
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="../assets/demo/demo.css" rel="stylesheet"/>
    <link href="../assets/css/style-sheet-adjar.css?v=2.1.0" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="../assets/css/daterangepicker.css"/>

    <%--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">--%>
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <link rel="stylesheet" href="../assets/charts/dark/styles.css">
    <link href="../assets/css/style-sheet-attribution.css?v=2.1.0" rel="stylesheet"/>

    <link rel="stylesheet" href="../assets/fastselect.min.css">
    <%--loader CSS Files--%>
    <link href="../assets/css/loader.css" rel="stylesheet"/>
    <link href="../dist/css/select2.css" rel="stylesheet"/>
    <%--responsive css file--%>
    <link href="../../assets/css/responsive-modal-pages.css" rel="stylesheet"/>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"
            integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
            crossorigin="anonymous"></script>


    <style>
        .dataTables_length::after {
            top: -2px;
            right: 65px;
        }

        .dataTables_paginate {
            margin-top: -30px !important;
        }

        .dataTables_info {
            margin-left: 20px;
        }

        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            padding: 3px 10px !important;
        }

        .hide {
            display: none;
        }

        .dataTables_wrapper .dt-buttons {
            position: absolute;
            float: none;
        !important;
            text-align: left !important;
            margin-top: -83px;
            margin-right: 53%;

        }

        @media screen and (min-width: 576px) and (max-width: 767px) {
            .dt-buttons {
                width: 285px !important;
                margin-top: -108px !important;
                margin-left: -115px;

            }

            .filterButtonRow1 {
                margin-right: -75px !important;
            }

            .reportRange {
                position: absolute;
                right: 50px !important;
                top: -5px;

            }

            .navbar {
                z-index: 1 !important;
            }
        }

        @media screen and (max-width: 575px) {

            .filterButtonRow1 {
                margin-left: 300px !important;
                margin-top: -2px !important;
                float: left !important;
            }

            .dt-buttons {
                margin-right: 100px !important;
                margin-top: -85px !important;
            }

        }

        @media screen and (min-width: 768px) and (max-width: 812px) {
            .dt-buttons {
                width: 285px !important;
                margin-top: -75px !important;
                margin-left: -110px;
            }

            .filterButtonRow1 {
                margin-right: -75px !important;
            }
        }

        @media screen and (min-width: 813px) and (max-width: 1039px) {
            .dt-buttons {
                width: 285px !important;
                left: 200px !important;
            }

            #reportrangeinput {
                width: 245px !important;
                font-size: 12px;
            }

            .filterButtonRow1 {
                margin-right: -340px !important;
            }
        }

        @media screen and (min-width: 1040px) {
            .dt-buttons {
                width: 285px !important;
                left: 200px !important;
            }
        }

    </style>


    <%-- style for checkboxes --%>
    <style>
        body,
        html {
            margin: 0;
            padding: 0;
        }

        . card-bodys {
            display: flex;
            align-items: center;
            justify-content: center;
            height: 100vh;
            background: #FFEFBA;
            /* fallback for old browsers */
            background: -webkit-linear-gradient(to right, #FFFFFF, #FFEFBA);
            /* Chrome 10-25, Safari 5.1-6 */
            background: linear-gradient(to right, #FFFFFF, #FFEFBA);
            /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */

        }

        .containers {
            max-width: 1440px;
            font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
            font-size: 13px;
        }

        ul.ks-cboxtags {
            list-style: none;
            padding: 20px;
        }

        ul.ks-cboxtags li {
            display: inline;
        }

        ul.ks-cboxtags li label {
            display: inline-block;
            background-color: rgba(255, 255, 255, .9);
            border: 2px solid rgba(139, 139, 139, .3);
            color: #adadad;
            border-radius: 25px;
            white-space: nowrap;
            margin: 3px 0px;
            -webkit-touch-callout: none;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            -webkit-tap-highlight-color: transparent;
            transition: all .2s;
        }

        ul.ks-cboxtags li label {
            padding: 8px 12px;
            cursor: pointer;
        }

        ul.ks-cboxtags li label::before {
            display: inline-block;
            font-style: normal;
            font-variant: normal;
            text-rendering: auto;
            -webkit-font-smoothing: antialiased;
            font-family: "Font Awesome 5 Free";
            font-weight: 900;
            font-size: 12px;
            padding: 2px 6px 2px 2px;
            content: "\f067";
            transition: transform .3s ease-in-out;
        }

        ul.ks-cboxtags li input[type="checkbox"]:checked + label::before {
            content: "\f00c";
            transform: rotate(-360deg);
            transition: transform .3s ease-in-out;
        }

        ul.ks-cboxtags li input[type="checkbox"]:checked + label {
            border: 2px solid #1bdbf8;
            background-color: #12bbd4;
            color: #fff;
            transition: all .2s;
        }

        ul.ks-cboxtags li input[type="checkbox"] {
            display: block;
        }

        ul.ks-cboxtags li input[type="checkbox"] {
            position: absolute;
            opacity: 0;
        }

        ul.ks-cboxtags li input[type="checkbox"]:focus + label {
            border: 2px solid #e9a1ff;
        }
    </style>


</head>

<%
    String cp = request.getContextPath();
%>

<body class="" style="background-color: <%=UIConstants.primaryBackgroundColor%>;overflow-x:hidden">
<%--<sec:authorize access="!hasRole('PUBLISHER')">
    <jsp:include page="adminSidebar.jsp">
        <jsp:param value="active" name="allReport"/>
        <jsp:param value="show" name="reportsTab"/>
    </jsp:include>
</sec:authorize>
<sec:authorize access="hasRole('PUBLISHER')">
    <jsp:include page="publisherSidebar.jsp">
        <jsp:param value="active" name="allReport"/>
    </jsp:include>
</sec:authorize>--%>
<div class="main-panel">
    <jsp:include page="integratedSidebar.jsp">
        <jsp:param value="active" name="allReport"/>
    </jsp:include>

    <!-- Navbar -->
    <%--<sec:authorize access="!hasRole('PUBLISHER')">
    <jsp:include page="adminHeader.jsp"/>
    </sec:authorize>
    <sec:authorize access="hasRole('PUBLISHER')">
    <jsp:include page="publisherHeader.jsp"/>
    </sec:authorize>--%>
    <!-- End Navbar -->
    <div class="wrapper ">


        <div class="content">
            <div class="container-fluid">
                <div class="cardDiv">
                    <div class="cardSize">
                        <i class="material-icons iconSize">assignment</i>
                    </div>
                    <h2 class="cardHeading">All Report</h2>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card <%=UIConstants.primaryTextColorClass%>"
                             style="background-color: <%=UIConstants.primaryCardBackground%>;">

                            <%--<div class="card-header card-header-warning card-header-icon allReportHeader">

                                <div class="row pull-right reportRange" style="margin-top:10px;margin-right:10px">
                                    <div class="col-sm-9">
                                        <!-- <i class="fa fa-caret-down"></i> -->
                                        <div class=" form-group float-right text-dark" id="reportrange"
                                             style="margin-top: -53px;">
                                            <input readonly type="text"
                                                   class="<%=UIConstants.primaryTextColorClass%>"
                                                   id="reportrangeinput"
                                                   name="daterange"
                                            &lt;%&ndash;value="${filters.daterange}"&ndash;%&gt;
                                                   style="cursor: pointer; background-color: <%=UIConstants.primaryBackgroundColor%>; border:1px solid <%=UIConstants.borderColor%>;border-radius:5px; width: 295px;padding:2px 6px">
                                        </div>
                                    </div>

                                </div>

                                <div class="row pull-right filterButtonRow1"
                                     style="margin-top:-4px;margin-right:-380px">
                                    <div class="col-sm-7" style="">
                                        <button class="btn btn-<%=UIConstants.primaryColorClass%> filterButton pull-right"
                                                id="appFilter"
                                                data-toggle="modal"
                                                style="cursor: pointer;margin-top:-39px;height:28px;border-radius:5px;border:1.8px solid #aaa"
                                                data-target="#exampleModalAd">
                                            <i class="fa fa-search" aria-hidden="true" data-toggle="modal"
                                               data-target="#addRule"
                                               id="reset"
                                               style="margin-left:0px;margin-top:-16px;width:auto;font-size:12px"><b
                                                    style="margin-left:6px">Search&nbsp;</b></i>
                                        </button>
                                    </div>
                                </div>

                            </div>--%>

                            <div class="card-header card-header-warning card-header-icon allReportHeader">

                                <div class="row pull-right reportRange" style="margin-top:10px;margin-right:80px">
                                    <div class="col-sm-9">
                                        <!-- <i class="fa fa-caret-down"></i> -->
                                        <div class=" form-group float-right text-dark" id="reportrange"
                                             style="margin-top: -53px;">
                                            <input readonly type="text"
                                                   class="<%=UIConstants.primaryTextColorClass%>"
                                                   id="reportrangeinput"
                                                   name="daterange"
                                            <%--value="${filters.daterange}"--%>
                                                   style="cursor: pointer; background-color: <%=UIConstants.primaryBackgroundColor%>; border:1px solid <%=UIConstants.borderColor%>;border-radius:5px; width: 295px;padding:2px 6px">
                                        </div>
                                    </div>

                                </div>

                                <div class="row pull-right filterButtonRow1"
                                     style="margin-top:-4px;margin-right:-430px">
                                    <div class="col-sm-7" style="">
                                        <button class="btn btn-<%=UIConstants.primaryColorClass%> filterButton pull-right"
                                                id="appFilter"
                                                data-toggle="modal"
                                                style="cursor: pointer;margin-top:-39px;height:28px;border-radius:5px;border:1.8px solid #aaa"
                                                data-target="#exampleModalAd">
                                            <i class="fa fa-search" aria-hidden="true" data-toggle="modal"
                                               data-target="#addRule"
                                               id="reset"
                                               style="margin-left:0px;margin-top:-16px;width:auto;font-size:16px"><b
                                                    style="margin-left:6px">Search&nbsp;</b></i>
                                        </button>
                                    </div>
                                </div>

                            </div>

                            <c:set var="data" value="${groupBy}" scope="request"/>
                            <% List<String> stringList = (List<String>) request.getAttribute("groupBy");%>


                            <div class="card-body">
                                <div class="toolbar <%=UIConstants.primaryTextColorClass%>"
                                     style="font-family: <%=UIConstants.primaryFontFamily%>;">
                                    <!--        Here you can write extra buttons/actions for the toolbar              -->

                                    <!-- Modal -->
                                    <div class="modal fade" id="exampleModalAd" tabindex="-1" role="dialog"
                                         aria-labelledby="exampleModalLongTitle" aria-hidden="true"
                                         data-backdrop="static">
                                        <div class="modal-dialog modal-lg" role="document" id="exampleModalAd1">
                                            <div class="card modal-content <%=UIConstants.primaryTextColorClass%> allReportContent"
                                                 style="background-color: <%=UIConstants.primaryModalCardHeaderBackground%>; /*width: 1200px;*/  ">
                                                <div class="modalClose">
                                                    <i class="fa fa-times" data-dismiss="modal" aria-label="Close"></i>
                                                </div>
                                                <div class="card-header card-header-<%=UIConstants.primaryColorClass%> card-header-icon"
                                                     style="height:80px">
                                                    <div class="card-icon">
                                                        <i class="material-icons">dashboard</i>
                                                    </div>
                                                    <h4 class="card-title <%=UIConstants.primaryTextColorClass%>"
                                                        style="font-family: <%=UIConstants.primaryFontFamily%>; font-weight: bold"
                                                        id="modalHeading1">Search Report</h4>
                                                    <p class="card-category">Enter Information</p>
                                                </div>
                                                <div class="card-body allReportCard"
                                                     style="font-family: <%=UIConstants.primaryFilterFontFamilyTable%>; /*width: 1200px;*/ background-color: <%=UIConstants.primaryCardModalBackground%>">
                                                    <div id="modalForm">

                                                        <form id="formSubmit">

                                                            <input type="hidden" name="daterange" id="dateHiddenField">


                                                            <input type="text" name="appsId"
                                                                   value=${app.id} id="appSelect"
                                                                   style="display:none">

                                                            <div class=""
                                                                 style="position: relative; height: 30px;">
                                                                <label class="<%=UIConstants.primaryTextColorClass%>"
                                                                       style="font-family: <%=UIConstants.primaryFontFamily%>; font-size: 30px; position: absolute; top: 50%; left: 50%; transform: translate(-50% , -50%); ">Select
                                                                    Columns</label>
                                                            </div>

                                                            <br>

                                                            <hr style="height: 5px;border-width:0;color:gray;background-color:#303654; margin-top: 2px;">

                                                            <div class="navOfferDetails"
                                                                 style="margin-top:14px;background-color:<%=UIConstants.primaryCardBackground%>;height:50px;">
                                                                <ul class="nav nav-pills nav-pills-<%=UIConstants.primaryColorClass%>  <%=UIConstants.primaryTextColorClass%> justify-content-center"
                                                                    role="tablist">

                                                                    <li class="nav-item">
                                                                        <a class="nav-link active <%=UIConstants.textColor%>"
                                                                           data-toggle="tab"
                                                                           href="#selectAppss"
                                                                           role="tablist" id="nav-link"
                                                                           style="font-size: 15px;">
                                                                            App
                                                                        </a>
                                                                    </li>

                                                                    <li class="nav-item">
                                                                        <a class="nav-link <%=UIConstants.textColor%>"
                                                                           data-toggle="tab"
                                                                           href="#selectEvents"
                                                                           role="tablist" id="nav-link"
                                                                           style="font-size: 15px;">
                                                                            Event
                                                                        </a>
                                                                    </li>

                                                                    <li class="nav-item">
                                                                        <a class="nav-link <%=UIConstants.textColor%>"
                                                                           data-toggle="tab"
                                                                           href="#selectDevices"
                                                                           role="tablist" id="nav-link"
                                                                           style="font-size: 15px;">
                                                                            Device
                                                                        </a>
                                                                    </li>

                                                                    <li class="nav-item">
                                                                        <a class="nav-link <%=UIConstants.textColor%>"
                                                                           data-toggle="tab"
                                                                           href="#selectDeviceLoc"
                                                                           role="tablist" id="nav-link"
                                                                           style="font-size: 15px;">
                                                                            Device Location
                                                                        </a>
                                                                    </li>

                                                                    <li class="nav-item">
                                                                        <a class="nav-link <%=UIConstants.textColor%>"
                                                                           data-toggle="tab"
                                                                           href="#selectAttributions"
                                                                           role="tablist" id="nav-link"
                                                                           style="font-size: 15px;">
                                                                            Attribution
                                                                        </a>
                                                                    </li>

                                                                    <li class="nav-item">
                                                                        <a class="nav-link <%=UIConstants.textColor%>"
                                                                           data-toggle="tab"
                                                                           href="#selectFraud"
                                                                           role="tablist" id="nav-link"
                                                                           style="font-size: 15px;">
                                                                            Fraud
                                                                        </a>
                                                                    </li>

                                                                    <li class="nav-item">
                                                                        <a class="nav-link <%=UIConstants.textColor%>"
                                                                           data-toggle="tab"
                                                                           href="#selectConversion"
                                                                           role="tablist" id="nav-link"
                                                                           style="font-size: 15px;">
                                                                            Conversion
                                                                        </a>
                                                                    </li>


                                                                </ul>
                                                            </div>

                                                            <br>
                                                            <br>

                                                            <hr style="height: 5px;border-width:0;color:gray;background-color:#303654; margin-top: 13px;">


                                                            <div class="tab-content tab-space">

                                                                <div class="tab-pane active" id="selectAppss">
                                                                    <div class="row" style="width:100%;margin-left:0px">
                                                                        <br>
                                                                        <div class="card <%=UIConstants.primaryTextColorClass%>"
                                                                             Style="font-family:<%=UIConstants.primaryFontFamily%>;background-color:<%=UIConstants.primaryCardBackground%>;margin-top:-25px;">
                                                                            <div class="card-header card-header-warning card-header-text">

                                                                            </div>
                                                                            <div class="card-bodys <%=UIConstants.primaryTextColorClass%> infoTabCard"
                                                                                 style="margin-left:0px;">
                                                                                <br>

                                                                                <div class="containers">
                                                                                    <ul class="ks-cboxtags">
                                                                                        <li><input type="checkbox"
                                                                                                   id="appVersion"
                                                                                                   value="App Version"><label
                                                                                                for="appVersion">App
                                                                                            Version</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="sdkVersion"
                                                                                                   value="SDK Version"
                                                                                        ><label
                                                                                                for="sdkVersion">SDK
                                                                                            Version</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="appId"
                                                                                                   value="App Id"><label
                                                                                                for="appId">App
                                                                                            ID</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="appName"
                                                                                                   value="App Name"
                                                                                                   checked><label
                                                                                                for="appName">App
                                                                                            Name</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="appType"
                                                                                                   value="App Type"
                                                                                        ><label
                                                                                                for="appType">App
                                                                                            Type</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="bundleId"
                                                                                                   value="Bundle Id"><label
                                                                                                for="bundleId">Bundle
                                                                                            Id</label>
                                                                                        </li>
                                                                                    </ul>

                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="tab-pane" id="selectEvents">
                                                                    <div class="row" style="width:100%;margin-left:0px">
                                                                        <br>
                                                                        <div class="card <%=UIConstants.primaryTextColorClass%>"
                                                                             Style="font-family:<%=UIConstants.primaryFontFamily%>;background-color:<%=UIConstants.primaryCardBackground%>;margin-top:-25px;">
                                                                            <div class="card-header card-header-warning card-header-text">

                                                                            </div>

                                                                            <div class="card-body <%=UIConstants.primaryTextColorClass%> infoTabCard"
                                                                                 style="background-color:<%=UIConstants.primaryCardBackground%>">
                                                                                <br>

                                                                                <div class="containers">
                                                                                    <ul class="ks-cboxtags">
                                                                                        <li><input type="checkbox"
                                                                                                   id="eventTime"
                                                                                                   value="Event Time"><label
                                                                                                for="eventTime">Event
                                                                                            Time</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="eventName"
                                                                                                   value="Event Name"
                                                                                        ><label
                                                                                                for="eventName">Event
                                                                                            Name</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="eventValue"
                                                                                                   value="Event Value"><label
                                                                                                for="eventValue">Event
                                                                                            Value</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="eventRevenue"
                                                                                                   value="Event Revenue"
                                                                                                   checked><label
                                                                                                for="eventRevenue">Event
                                                                                            Revenue</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="eventRevenueCurrency"
                                                                                                   value="Event Revenue Currency"
                                                                                        ><label
                                                                                                for="eventRevenueCurrency">Event
                                                                                            Revenue Currency</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="eventRevenueUsd"
                                                                                                   value="Event Revenue USD"><label
                                                                                                for="eventRevenueUsd">Event
                                                                                            Revenue USD</label>
                                                                                        </li>
                                                                                    </ul>

                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                                <div class="tab-pane" id="selectDevices">
                                                                    <div class="row" style="width:100%;margin-left:0px">
                                                                        <br>
                                                                        <div class="card <%=UIConstants.primaryTextColorClass%>"
                                                                             Style="font-family:<%=UIConstants.primaryFontFamily%>;background-color:<%=UIConstants.primaryCardBackground%>;margin-top:-25px;">
                                                                            <div class="card-header card-header-warning card-header-text">

                                                                            </div>

                                                                            <div class="card-body <%=UIConstants.primaryTextColorClass%> infoTabCard"
                                                                                 style="background-color:<%=UIConstants.primaryCardBackground%>">
                                                                                <br>

                                                                                <div class="containers">
                                                                                    <ul class="ks-cboxtags">
                                                                                        <li><input type="checkbox"
                                                                                                   id="att"
                                                                                                   value="ATT"><label
                                                                                                for="att">ATT</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="wifi"
                                                                                                   value="WIFI"
                                                                                        ><label
                                                                                                for="wifi">WIFI</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="operator"
                                                                                                   value="Operator"><label
                                                                                                for="operator">Operator</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="carrier"
                                                                                                   value="Carrier"
                                                                                                   checked><label
                                                                                                for="carrier">Carrier</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="language"
                                                                                                   value="Language"
                                                                                        ><label
                                                                                                for="language">Language</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="androidId"
                                                                                                   value="Android Id"><label
                                                                                                for="androidId">Android
                                                                                            Id</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="imei"
                                                                                                   value="IMEI"
                                                                                        ><label
                                                                                                for="imei">IMEI</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="idfa"
                                                                                                   value="IDFA"><label
                                                                                                for="idfa">IDFA</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="idfv"
                                                                                                   value="IDFV"
                                                                                        ><label
                                                                                                for="idfv">IDFV</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="advertisingId"
                                                                                                   value="Advertising Id"><label
                                                                                                for="advertisingId">Advertising
                                                                                            Id</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="deviceModel"
                                                                                                   value="Device Model"
                                                                                        ><label
                                                                                                for="deviceModel">Device
                                                                                            Model</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="deviceType"
                                                                                                   value="Device Type"><label
                                                                                                for="deviceType">Device
                                                                                            Type</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="deviceCategory"
                                                                                                   value="Device Category"
                                                                                        ><label
                                                                                                for="deviceCategory">Device
                                                                                            Category</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="platform"
                                                                                                   value="Platform"><label
                                                                                                for="platform">Platform</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="osVersion"
                                                                                                   value="OS Version"
                                                                                        ><label
                                                                                                for="osVersion">OS
                                                                                            Version</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="userAgent"
                                                                                                   value="User Agent"><label
                                                                                                for="userAgent">User
                                                                                            Agent</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="deviceDownloadTime"
                                                                                                   value="Device Download Time"
                                                                                        ><label
                                                                                                for="deviceDownloadTime">Device
                                                                                            Download Time</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="oaid"
                                                                                                   value="OAID"><label
                                                                                                for="oaid">OAID</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="isLat"
                                                                                                   value="is LAT"><label
                                                                                                for="isLat">is
                                                                                            LAT</label>
                                                                                        </li>
                                                                                    </ul>

                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                                <div class="tab-pane" id="selectDeviceLoc">
                                                                    <div class="row" style="width:100%;margin-left:0px">
                                                                        <br>
                                                                        <div class="card <%=UIConstants.primaryTextColorClass%>"
                                                                             Style="font-family:<%=UIConstants.primaryFontFamily%>;background-color:<%=UIConstants.primaryCardBackground%>;margin-top:-25px;">
                                                                            <div class="card-header card-header-warning card-header-text">

                                                                            </div>

                                                                            <div class="card-body <%=UIConstants.primaryTextColorClass%> infoTabCard"
                                                                                 style="background-color:<%=UIConstants.primaryCardBackground%>">
                                                                                <br>

                                                                                <div class="containers">
                                                                                    <ul class="ks-cboxtags">
                                                                                        <li><input type="checkbox"
                                                                                                   id="region"
                                                                                                   value="Region"><label
                                                                                                for="region">Region</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="countryCode"
                                                                                                   value="Country Code"
                                                                                        ><label
                                                                                                for="countryCode">Country
                                                                                            Code</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="state"
                                                                                                   value="State"><label
                                                                                                for="state">State</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="city"
                                                                                                   value="City" checked><label
                                                                                                for="city">City</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="postalCode"
                                                                                                   value="Postal Code"
                                                                                        ><label
                                                                                                for="postalCode">Postal
                                                                                            Code</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="ip"
                                                                                                   value="IP"><label
                                                                                                for="ip">IP</label>
                                                                                        </li>
                                                                                    </ul>

                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                                <div class="tab-pane" id="selectAttributions">
                                                                    <div class="row" style="width:100%;margin-left:0px">
                                                                        <br>
                                                                        <div class="card <%=UIConstants.primaryTextColorClass%>"
                                                                             Style="font-family:<%=UIConstants.primaryFontFamily%>;background-color:<%=UIConstants.primaryCardBackground%>;margin-top:-25px;">
                                                                            <div class="card-header card-header-warning card-header-text">

                                                                            </div>

                                                                            <div class="card-body <%=UIConstants.primaryTextColorClass%> infoTabCard"
                                                                                 style="background-color:<%=UIConstants.primaryCardBackground%>">
                                                                                <br>

                                                                                <div class="containers">
                                                                                    <ul class="ks-cboxtags">
                                                                                        <li><input type="checkbox"
                                                                                                   id="attributedTouchType"
                                                                                                   value="Attributed Touch Type"><label
                                                                                                for="attributedTouchType">Attributed
                                                                                            Touch Type</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="attributedTouchTime"
                                                                                                   value="Attributed Touch Time"
                                                                                        ><label
                                                                                                for="attributedTouchTime">Attributed
                                                                                            Touch Time</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="installTime"
                                                                                                   value="Install Time"><label
                                                                                                for="installTime">Install
                                                                                            Time</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="costModel"
                                                                                                   value="Cost Model"
                                                                                        ><label
                                                                                                for="costModel">Cost
                                                                                            Model</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="costValue"
                                                                                                   value="Cost Value"
                                                                                        ><label
                                                                                                for="costValue">Cost
                                                                                            Value</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="subParam1"
                                                                                                   value="Sub Param 1"><label
                                                                                                for="subParam1">Sub
                                                                                            Param 1</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="subParam2"
                                                                                                   value="Sub Param 2"><label
                                                                                                for="subParam2">Sub
                                                                                            Param 2</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="subParam3"
                                                                                                   value="Sub Param 3"><label
                                                                                                for="subParam3">Sub
                                                                                            Param 3</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="subParam4"
                                                                                                   value="Sub Param 4"><label
                                                                                                for="subParam4">Sub
                                                                                            Param 4</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="customerUserId"
                                                                                                   value="Customer User Id"
                                                                                        ><label
                                                                                                for="customerUserId">Customer
                                                                                            User Id</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="isRetargeting"
                                                                                                   value="is Retargeting"><label
                                                                                                for="isRetargeting">is
                                                                                            Retargeting</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="storeProductPage"
                                                                                                   value="Store Product Page"
                                                                                        ><label
                                                                                                for="storeProductPage">Store
                                                                                            Product Page</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="costCurrency"
                                                                                                   value="Cost Currency"><label
                                                                                                for="costCurrency">Cost
                                                                                            Currency</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="eventSource"
                                                                                                   value="Event Source"
                                                                                        ><label
                                                                                                for="eventSource">Event
                                                                                            Source</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="partner"
                                                                                                   value="Partner"
                                                                                                   checked><label
                                                                                                for="partner">Partner</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="mediaSource"
                                                                                                   value="Media Source"
                                                                                        ><label
                                                                                                for="mediaSource">Media
                                                                                            Source</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="channel"
                                                                                                   value="Channel"><label
                                                                                                for="channel">Channel</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="keywords"
                                                                                                   value="Keywords"
                                                                                        ><label
                                                                                                for="keywords">Keywords</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="retargetingConversionType"
                                                                                                   value="Retargeting Conversion Type"><label
                                                                                                for="retargetingConversionType">Retargeting
                                                                                            Conversion Type</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="isPrimaryAttribution"
                                                                                                   value="Is Primary Attribution"
                                                                                        ><label
                                                                                                for="isPrimaryAttribution">Is
                                                                                            Primary Attribution</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="attributionLookback"
                                                                                                   value="Attribution Lookback"><label
                                                                                                for="attributionLookback">Attribution
                                                                                            Lookback</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="reengagementWindow"
                                                                                                   value="Reengagement Window"><label
                                                                                                for="reengagementWindow">Reengagement
                                                                                            Window</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="matchType"
                                                                                                   value="Match Type"><label
                                                                                                for="matchType">Match
                                                                                            Type</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="httpReferrer"
                                                                                                   value="HTTP Referrer"
                                                                                        ><label
                                                                                                for="httpReferrer">HTTP
                                                                                            Referrer</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="campaign"
                                                                                                   value="Campaign"><label
                                                                                                for="campaign">Campaign</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="campaignId"
                                                                                                   value="Campaign Id"
                                                                                        ><label
                                                                                                for="campaignId">Campaign
                                                                                            Id</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="adset"
                                                                                                   value="Adset"
                                                                                        ><label
                                                                                                for="adset">Adset</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="adsetId"
                                                                                                   value="Adset Id"><label
                                                                                                for="adsetId">Adset
                                                                                            Id</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="ad"
                                                                                                   value="Ad"><label
                                                                                                for="ad">Ad</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="originalUrl"
                                                                                                   value="Original Url"><label
                                                                                                for="originalUrl">Original
                                                                                            Url</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="adId"
                                                                                                   value="Ad Id"><label
                                                                                                for="adId">Ad Id</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="adType"
                                                                                                   value="Ad Type"
                                                                                        ><label
                                                                                                for="adType">Ad
                                                                                            Type</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="siteId"
                                                                                                   value="Site Id"><label
                                                                                                for="siteId">Site
                                                                                            Id</label>
                                                                                        </li>

                                                                                    </ul>

                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                                <div class="tab-pane" id="selectFraud">
                                                                    <div class="row" style="width:100%;margin-left:0px">
                                                                        <br>
                                                                        <div class="card <%=UIConstants.primaryTextColorClass%>"
                                                                             Style="font-family:<%=UIConstants.primaryFontFamily%>;background-color:<%=UIConstants.primaryCardBackground%>;margin-top:-25px;">
                                                                            <div class="card-header card-header-warning card-header-text">

                                                                            </div>

                                                                            <div class="card-body <%=UIConstants.primaryTextColorClass%> infoTabCard"
                                                                                 style="background-color:<%=UIConstants.primaryCardBackground%>">
                                                                                <br>

                                                                                <div class="containers">
                                                                                    <ul class="ks-cboxtags">
                                                                                        <li><input type="checkbox"
                                                                                                   id="rejectedReason"
                                                                                                   value="Rejected Reason"><label
                                                                                                for="rejectedReason">Rejected
                                                                                            Reason</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="rejectedReasonValue"
                                                                                                   value="Rejected Reason Value"
                                                                                        ><label
                                                                                                for="rejectedReasonValue">Rejected
                                                                                            Reason Value</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="blockedReason"
                                                                                                   value="Blocked Reason"><label
                                                                                                for="blockedReason">Blocked
                                                                                            Reason</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="blockedReasonRule"
                                                                                                   value="Blocked Reason Rule"
                                                                                                   checked><label
                                                                                                for="blockedReasonRule">Blocked
                                                                                            Reason Rule</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="blockedReasonValue"
                                                                                                   value="Blocked Reason Value"
                                                                                        ><label
                                                                                                for="blockedReasonValue">Blocked
                                                                                            Reason Value</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="blockedSubReason"
                                                                                                   value="Blocked Sub Reason"><label
                                                                                                for="blockedSubReason">Blocked
                                                                                            Sub Reason</label>
                                                                                        </li>
                                                                                    </ul>

                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                                <div class="tab-pane" id="selectConversion">
                                                                    <div class="row" style="width:100%;margin-left:0px">
                                                                        <br>
                                                                        <div class="card <%=UIConstants.primaryTextColorClass%>"
                                                                             Style="font-family:<%=UIConstants.primaryFontFamily%>;background-color:<%=UIConstants.primaryCardBackground%>;margin-top:-25px;">
                                                                            <div class="card-header card-header-warning card-header-text">

                                                                            </div>

                                                                            <div class="card-body <%=UIConstants.primaryTextColorClass%> infoTabCard"
                                                                                 style="background-color:<%=UIConstants.primaryCardBackground%>">
                                                                                <br>

                                                                                <div class="containers">
                                                                                    <ul class="ks-cboxtags">
                                                                                        <li><input type="checkbox"
                                                                                                   id="approvedConversions"
                                                                                                   value="Approved Conversions"><label
                                                                                                for="approvedConversions">Approved
                                                                                            Conversions</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="cancelledConversions"
                                                                                                   value="Cancelled Conversions"><label
                                                                                                for="cancelledConversions">Cancelled
                                                                                            Conversions</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="pendingConversions"
                                                                                                   value="Pending Conversions"><label
                                                                                                for="pendingConversions">Pending
                                                                                            Conversions</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="rejectedClicks"
                                                                                                   value="Rejected Clicks"><label
                                                                                                for="rejectedClicks">Rejected
                                                                                            Clicks</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="approvedRevenue"
                                                                                                   value="Approved Revenue"><label
                                                                                                for="approvedRevenue">Approved
                                                                                            Revenue</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="cancelledRevenue"
                                                                                                   value="Cancelled Revenue"><label
                                                                                                for="cancelledRevenue">Cancelled
                                                                                            Revenue</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="pendingRevenue"
                                                                                                   value="Pending Revenue"><label
                                                                                                for="pendingRevenue">Pending
                                                                                            Revenue</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="approvedPayout"
                                                                                                   value="Approved Payout"><label
                                                                                                for="approvedPayout">Approved
                                                                                            Payout</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="clickCount"
                                                                                                   value="Click Count"><label
                                                                                                for="clickCount">Click
                                                                                            Count</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="ratio"
                                                                                                   value="Ratio"><label
                                                                                                for="ratio">Ratio</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="ctr"
                                                                                                   value="CTR"><label
                                                                                                for="ctr">CTR</label>
                                                                                        </li>
                                                                                    </ul>

                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                            </div>

                                                            <br>

                                                            <div class="row filterModalRow">

                                                                <div class="">
                                                                    <label class="col-form-label <%=UIConstants.primaryTextColorClass%>"
                                                                           style="font-weight:bold;margin-left: 10px">Select
                                                                        Attributed Touch Type *:</label>
                                                                </div>

                                                                <div class="attributedTouchTypeModel"
                                                                     style="margin-left:20px; width: 50%;">
                                                                    <div class="dropdown bootstrap-select show-tick show <%=UIConstants.primaryTextColorClass%>">
                                                                        <select class="selectpicker <%=UIConstants.primaryTextColorClass%>"
                                                                                id="attributedTouchTypeSelect"
                                                                                name="attributedTouchTypeSelect"
                                                                                data-size="7"
                                                                                data-style="btn"
                                                                                title="Single Select"
                                                                                data-width="250px">
                                                                            <option value="all"
                                                                                    selected>ALL
                                                                            </option>
                                                                            <option value="click">CLICK
                                                                            </option>
                                                                            <option value="conversion">CONVERSION
                                                                            </option>
                                                                            <option value="impression">IMPRESSION
                                                                            </option>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                            </div>

                                                            <br>

                                                            <%--<div class="row">

                                                                <div class="col-sm-6" style="margin-top: 9px">
                                                                    <label class="<%=UIConstants.primaryTextColorClass%>"
                                                                           style="font-family: <%=UIConstants.primaryFontFamily%>; font-weight: bold; margin-left: 0px; position: absolute">Select
                                                                        Partner </label> <i class="material-icons"
                                                                                            style="margin-top: -5px; margin-left: 115px; position: absolute;">person</i>&nbsp;
                                                                </div>
                                                                <div class="col-sm-6 conversionSelectBox"
                                                                     style="margin-left:-105px;margin-top:-7px">
                                                                    <select class="partnerSelect"
                                                                            id="partnerSelect"
                                                                            name="partners"
                                                                            multiple
                                                                            data-size="7" style="width:300px"
                                                                            style="color:<%=UIConstants.textColor%>; "
                                                                    ></select>
                                                                </div>

                                                            </div>

                                                            <br>

                                                            <div class="row">
                                                                <div class="col-lg-6"
                                                                     style="margin-left:-103px;margin-top:0">
                                                                    <div class="col-sm-6"
                                                                         style="margin-top: 9px;margin-left: 85px">
                                                                        <label class="<%=UIConstants.primaryTextColorClass%>"
                                                                               style="font-family: <%=UIConstants.primaryFontFamily%>; font-weight: bold; margin-left: 0px; position: absolute">Select
                                                                            Columns</label> <i class="fa fa-table"
                                                                                               style="margin-left: 123px"></i>
                                                                    </div>
                                                                    <div class="col-lg-6 allReportDataColumns"
                                                                         style="margin-left:90%;margin-top:-33px">
                                                                        <select class="multipleSelect <%=UIConstants.primaryTextColorClass%>"
                                                                                data-style="btn btn-warning"
                                                                                multiple="" title="Select Column"
                                                                                data-size="7"
                                                                                data-width="380px"
                                                                                tabindex="-98" id="extSelect"
                                                                                name="columns">
                                                                            <!-- selected="selected" -->
                                                                            <option value="App Id">App Id
                                                                            </option>
                                                                            <option value="App Name">App Name
                                                                            </option>
                                                                            <option value="Partner Id" selected>Partner
                                                                                Id
                                                                            </option>
                                                                            <option value="Partner Name" selected>
                                                                                Partner Name
                                                                            </option>
                                                                            <option value="Click" selected>Click
                                                                            </option>
                                                                            <option value="Approved Conversion"
                                                                                    selected>Approved Conversion
                                                                            </option>
                                                                            <option value="Cancelled Conversion">
                                                                                Cancelled Conversion
                                                                            </option>
                                                                            <option value="Pending Conversion">
                                                                                Pending Conversion
                                                                            </option>
                                                                            <option value="Gross Conversion">
                                                                                Gross Conversion
                                                                            </option>
                                                                            <option value="Approved Revenue">
                                                                                Approved Revenue
                                                                            </option>
                                                                            <option value="Cancelled Revenue">
                                                                                Cancelled Revenue
                                                                            </option>
                                                                            <option value="Pending Revenue">
                                                                                Pending Revenue
                                                                            </option>
                                                                            <option value="Gross Revenue">
                                                                                Gross Revenue
                                                                            </option>
                                                                            <option value="Ratio" selected>
                                                                                Ratio
                                                                            </option>
                                                                            <option value="CTR" selected>
                                                                                CTR
                                                                            </option>
                                                                            <option value="Day Timestamp">
                                                                                Day Timestamp
                                                                            </option>
                                                                            <option value="Rejected Clicks">
                                                                                Rejected Clicks
                                                                            </option>


                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>--%>

                                                            <div class="row bottomLineAllReport">
                                                                <div class="col-sm-12">

                                                                    <button type="button" id="submitForm"
                                                                            class="btn btn-<%=UIConstants.primaryColorClass%> btn-round pull-right submitButton"
                                                                            data-dismiss="modal"
                                                                            style="margin-top:10px;margin-right:20px">
                                                                        Apply
                                                                    </button>

                                                                </div>
                                                            </div>

                                                            <div class="clearfix"></div>

                                                        </form>

                                                        <!-- Image loader -->
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <!-- </form> -->
                                </div>

                            </div>


                            <div class="card-body" id="tableBody" style="display: none">
                            </div>

                            <div class="table-responsive" style="width:98%;margin-left:10px;margin-top:-20px">

                                <table id="datatables"
                                       class="display nowrap <%=UIConstants.primaryTextColorClass%>"
                                       style="font-family: <%=UIConstants.primaryFontFamily%>;width:100%"
                                       cellspacing="0">
                                    <thead>
                                    <tr class="<%=UIConstants.primaryTextColorClass%>" style="font-weight: bold; ">
                                        <th style="font-weight:bold">App Version</th>
                                        <th style="font-weight:bold">SDK Version</th>
                                        <th style="font-weight:bold">App Id</th>
                                        <th style="font-weight:bold">App Name</th>
                                        <th style="font-weight:bold">App Type</th>
                                        <th style="font-weight:bold">Bundle Id</th>

                                        <th style="font-weight:bold">Event Time</th>
                                        <th style="font-weight:bold">Event Name</th>
                                        <th style="font-weight:bold">Event Value</th>
                                        <th style="font-weight:bold">Event Revenue</th>
                                        <th style="font-weight:bold">Event Revenue Currency</th>
                                        <th style="font-weight:bold">Event Revenue USD</th>

                                        <th style="font-weight:bold">ATT</th>
                                        <th style="font-weight:bold">WIFI</th>
                                        <th style="font-weight:bold">Operator</th>
                                        <th style="font-weight:bold">Carrier</th>
                                        <th style="font-weight:bold">Language</th>
                                        <th style="font-weight:bold">Android Id</th>
                                        <th style="font-weight:bold">IMEI</th>
                                        <th style="font-weight:bold">IDFA</th>
                                        <th style="font-weight:bold">IDFV</th>
                                        <th style="font-weight:bold">Advertising Id</th>
                                        <th style="font-weight:bold">Device Model</th>
                                        <th style="font-weight:bold">Device Type</th>
                                        <th style="font-weight:bold">Device Category</th>
                                        <th style="font-weight:bold">Platform</th>
                                        <th style="font-weight:bold">OS Version</th>
                                        <th style="font-weight:bold">User Agent</th>
                                        <th style="font-weight:bold">Device Download Time</th>
                                        <th style="font-weight:bold">OAID</th>
                                        <th style="font-weight:bold">is LAT</th>

                                        <th style="font-weight:bold">Region</th>
                                        <th style="font-weight:bold">Country Code</th>
                                        <th style="font-weight:bold">State</th>
                                        <th style="font-weight:bold">City</th>
                                        <th style="font-weight:bold">Postal Code</th>
                                        <th style="font-weight:bold">IP</th>

                                        <th style="font-weight:bold">Attributed Touch Type</th>
                                        <th style="font-weight:bold">Attributed Touch Time</th>
                                        <th style="font-weight:bold">Partner Id</th>
                                        <th style="font-weight:bold">Partner Name</th>


                                        <%--<th style="font-weight:bold">Attributed Touch Type</th>
                                        <th style="font-weight:bold">Attributed Touch Time</th>
                                        <th style="font-weight:bold">Install Time</th>
                                        <th style="font-weight:bold">Cost Model</th>
                                        <th style="font-weight:bold">Cost Value</th>
                                        <th style="font-weight:bold">Sub Param 1</th>
                                        <th style="font-weight:bold">Sub Param 2</th>
                                        <th style="font-weight:bold">Sub Param 3</th>
                                        <th style="font-weight:bold">Sub Param 4</th>
                                        <th style="font-weight:bold">Customer User Id</th>
                                        <th style="font-weight:bold">is Retargeting</th>
                                        <th style="font-weight:bold">Store Product Page</th>
                                        <th style="font-weight:bold">Cost Currency</th>
                                        <th style="font-weight:bold">Event Source</th>
                                        <th style="font-weight:bold">Partner</th>
                                        <th style="font-weight:bold">Media Source</th>
                                        <th style="font-weight:bold">Channel</th>
                                        <th style="font-weight:bold">Keywords</th>
                                        <th style="font-weight:bold">Retargeting Conversion Type</th>
                                        <th style="font-weight:bold">Is Primary Attribution</th>
                                        <th style="font-weight:bold">Attribution Lookback</th>
                                        <th style="font-weight:bold">Reengagement Window</th>
                                        <th style="font-weight:bold">Match Type</th>
                                        <th style="font-weight:bold">HTTP Referrer</th>
                                        <th style="font-weight:bold">Campaign</th>
                                        <th style="font-weight:bold">Campaign Id</th>
                                        <th style="font-weight:bold">Adset</th>
                                        <th style="font-weight:bold">Adset Id</th>
                                        <th style="font-weight:bold">Ad</th>
                                        <th style="font-weight:bold">Original Url</th>
                                        <th style="font-weight:bold">Ad Id</th>
                                        <th style="font-weight:bold">Ad Type</th>
                                        <th style="font-weight:bold">Site Id</th>

                                        <th style="font-weight:bold">Rejected Reason</th>
                                        <th style="font-weight:bold">Rejected Reason Value</th>
                                        <th style="font-weight:bold">Blocked Reason</th>
                                        <th style="font-weight:bold">Blocked Reason Rule</th>
                                        <th style="font-weight:bold">Blocked Reason Value</th>
                                        <th style="font-weight:bold">Blocked Sub Reason</th>

                                        <th style="font-weight:bold">Approved Conversions</th>
                                        <th style="font-weight:bold">Cancelled Conversions</th>
                                        <th style="font-weight:bold">Pending Conversions</th>
                                        <th style="font-weight:bold">Rejected Clicks</th>
                                        <th style="font-weight:bold">Approved Revenue</th>
                                        <th style="font-weight:bold">Cancelled Revenue</th>
                                        <th style="font-weight:bold">Pending Revenue</th>
                                        <th style="font-weight:bold">Approved Payout</th>
                                        <th style="font-weight:bold">Click Count</th>
                                        <th style="font-weight:bold">Ratio</th>
                                        <th style="font-weight:bold">CTR</th>--%>

                                    </tr>
                                    </thead>

                                </table>
                            </div>

                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>


<jsp:include page="adminFooter.jsp"/>


<%--<!--   Core JS Files   -->
<!-- multiselect -->
<script src="../assets/js/core/jquery.min.js"></script>
<script src="../assets/js/core/popper.min.js"></script>
<script src="../assets/js/core/bootstrap-material-design.min.js"></script>
<script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!-- Plugin for the momentJs  -->
<script src="../assets/js/plugins/moment.min.js"></script>
<!--  Plugin for Sweet Alert -->
<script src="../assets/js/plugins/sweetalert2.js"></script>
<!-- Forms Validations Plugin -->
<script src="../assets/js/plugins/jquery.validate.min.js"></script>
<!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
<script src="../assets/js/plugins/jquery.bootstrap-wizard.js"></script>
<!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script src="../assets/js/plugins/bootstrap-selectpicker.js"></script>
<!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
<script src="../assets/js/plugins/bootstrap-datetimepicker.min.js"></script>
<!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
<script src="../assets/js/plugins/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../assets/js/jquery.spring-friendly.js"></script>
<!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
<script src="../assets/js/plugins/bootstrap-tagsinput.js"></script>
<!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="../assets/js/plugins/jasny-bootstrap.min.js"></script>
<!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
<script src="../assets/js/plugins/fullcalendar.min.js"></script>
<!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
<script src="../assets/js/plugins/jquery-jvectormap.js"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="../assets/js/plugins/nouislider.min.js"></script>
<!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
<!-- Library for adding elements -->
<script src="../assets/js/plugins/arrive.min.js"></script>
<script src="../assets/js/plugins/chartist.min.js"></script>
<!--  Notifications Plugin    -->
<script src="../assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="../assets/js/material-dashboard.js?v=2.1.0" type="text/javascript"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="../assets/demo/demo.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="../dist/js/select2.js"></script>--%>


<!--   Core JS Files   -->
<!-- multiselect -->
<script src="../assets/js/core/jquery.min.js"></script>
<script src="../assets/js/core/popper.min.js"></script>
<script src="../assets/js/core/bootstrap-material-design.min.js"></script>
<script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!-- Plugin for the momentJs  -->
<script src="../assets/js/plugins/moment.min.js"></script>
<!--  Plugin for Sweet Alert -->
<script src="../assets/js/plugins/sweetalert2.js"></script>
<!-- Forms Validations Plugin -->
<script src="../assets/js/plugins/jquery.validate.min.js"></script>
<!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
<script src="../assets/js/plugins/jquery.bootstrap-wizard.js"></script>
<!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script src="../assets/js/plugins/bootstrap-selectpicker.js"></script>
<!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
<script src="../assets/js/plugins/bootstrap-datetimepicker.min.js"></script>
<!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
<script src="../assets/js/plugins/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../assets/js/jquery.spring-friendly.js"></script>
<!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
<script src="../assets/js/plugins/bootstrap-tagsinput.js"></script>
<!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="../assets/js/plugins/jasny-bootstrap.min.js"></script>
<!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
<script src="../assets/js/plugins/fullcalendar.min.js"></script>
<!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
<script src="../assets/js/plugins/jquery-jvectormap.js"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="../assets/js/plugins/nouislider.min.js"></script>
<!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
<!-- Library for adding elements -->
<script src="../assets/js/plugins/arrive.min.js"></script>
<script src="../assets/js/plugins/chartist.min.js"></script>
<!--  Notifications Plugin    -->
<script src="../assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="../assets/js/material-dashboard.js?v=2.1.0" type="text/javascript"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="../assets/demo/demo.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="../dist/js/select2.js"></script>
<script src="https://kit.fontawesome.com/9a8202f52c.js" crossorigin="anonymous"></script>
<script src="https://use.fontawesome.com/8f1e7b48d4.js"></script>


<script>
    window.onresize = function () {
        alert("Inside Modal css");
        var modal = document.getElementById("exampleModalAd");
        var top = (window.innerHeight - modal.offsetHeight) / 2;
        var left = (window.innerWidth - modal.offsetWidth) / 2;
        modal.style.top = top + "px";
        modal.style.left = left + "px";
    }
</script>

<script>


    $(document).ready(function () {
        $().ready(function () {
            $sidebar = $('.sidebar');

            $sidebar_img_container = $sidebar.find('.sidebar-background');

            $full_page = $('.full-page');

            $sidebar_responsive = $('body > .navbar-collapse');

            window_width = $(window).width();

            fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

            if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
                if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
                    $('.fixed-plugin .dropdown').addClass('open');
                }

            }

            $('.fixed-plugin a').click(function (event) {
                // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
                if ($(this).hasClass('switch-trigger')) {
                    if (event.stopPropagation) {
                        event.stopPropagation();
                    } else if (window.event) {
                        window.event.cancelBubble = true;
                    }
                }
            });

            $('.fixed-plugin .active-color span').click(function () {
                $full_page_background = $('.full-page-background');

                $(this).siblings().removeClass('active');
                $(this).addClass('active');

                var new_color = $(this).data('color');

                if ($sidebar.length != 0) {
                    $sidebar.attr('data-color', new_color);
                }

                if ($full_page.length != 0) {
                    $full_page.attr('filter-color', new_color);
                }

                if ($sidebar_responsive.length != 0) {
                    $sidebar_responsive.attr('data-color', new_color);
                }
            });

            $('.fixed-plugin .background-color .badge').click(function () {
                $(this).siblings().removeClass('active');
                $(this).addClass('active');

                var new_color = $(this).data('background-color');

                if ($sidebar.length != 0) {
                    $sidebar.attr('data-background-color', new_color);
                }
            });

            $('.fixed-plugin .img-holder').click(function () {
                $full_page_background = $('.full-page-background');

                $(this).parent('li').siblings().removeClass('active');
                $(this).parent('li').addClass('active');


                var new_image = $(this).find("img").attr('src');

                if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
                    $sidebar_img_container.fadeOut('fast', function () {
                        $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
                        $sidebar_img_container.fadeIn('fast');
                    });
                }

                if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
                    var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

                    $full_page_background.fadeOut('fast', function () {
                        $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
                        $full_page_background.fadeIn('fast');
                    });
                }

                if ($('.switch-sidebar-image input:checked').length == 0) {
                    var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
                    var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

                    $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
                    $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
                }

                if ($sidebar_responsive.length != 0) {
                    $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
                }
            });

            $('.switch-sidebar-image input').change(function () {
                $full_page_background = $('.full-page-background');

                $input = $(this);

                if ($input.is(':checked')) {
                    if ($sidebar_img_container.length != 0) {
                        $sidebar_img_container.fadeIn('fast');
                        $sidebar.attr('data-image', '#');
                    }

                    if ($full_page_background.length != 0) {
                        $full_page_background.fadeIn('fast');
                        $full_page.attr('data-image', '#');
                    }

                    background_image = true;
                } else {
                    if ($sidebar_img_container.length != 0) {
                        $sidebar.removeAttr('data-image');
                        $sidebar_img_container.fadeOut('fast');
                    }

                    if ($full_page_background.length != 0) {
                        $full_page.removeAttr('data-image', '#');
                        $full_page_background.fadeOut('fast');
                    }

                    background_image = false;
                }
            });

            $('.switch-sidebar-mini input').change(function () {
                $body = $('body');

                $input = $(this);

                if (md.misc.sidebar_mini_active == true) {
                    $('body').removeClass('sidebar-mini');
                    md.misc.sidebar_mini_active = false;

                    $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

                } else {

                    $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');

                    setTimeout(function () {
                        $('body').addClass('sidebar-mini');

                        md.misc.sidebar_mini_active = true;
                    }, 300);
                }

                // we simulate the window Resize so the charts will get updated in realtime.
                var simulateWindowResize = setInterval(function () {
                    window.dispatchEvent(new Event('resize'));
                }, 180);

                // we stop the simulation of Window Resize after the animations are completed
                setTimeout(function () {
                    clearInterval(simulateWindowResize);
                }, 1000);

            });
        });

    });

</script>

<script>

    /*function getDatatable() {*/

        var dateRange = '';
        var startDate = '';
        var filter = '';
        var filter1 = '';
        let appFilter = '';
        let pubFilter = '';
        let advFilter = '';
        let conversionFilter = '';
        var offerId = 0;
        var pubId = 0;
        var search = {};
        var status = '';
        var id = 0;

        var table = $('#datatables').DataTable({
            ajax: "/adminConsole/getAllReportAjax?id=${app.id}",
            serverSide: true,
            processing: true,
            scrollX: true,
            columns: [
                {
                    data: 'appVersion',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'sdkVersion',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'appId',
                    render: function (data) {
                        offerId = data;
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'appName',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + '<a href="/adminConsole/viewoffer?id=' + offerId + '" target = "_blank" style="color:<%=UIConstants.primaryHyperLinkColor%>;font-weight:bold">' + data + ' </a>';
                    }
                },
                {
                    data: 'appType',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'bundleId',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },


                {
                    data: 'eventTime',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'eventName',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'eventValue',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'eventRevenue',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'eventRevenueCurrency',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'eventRevenueUSD',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },


                {
                    data: 'att',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'wifi',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'operator',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'carrier',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'language',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'androidId',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'imei',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'idfa',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'idfv',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'advertisingId',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'deviceModel',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'deviceType',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'deviceCategory',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'platform',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'osVersion',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'userAgent',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'deviceDownloadTime',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'oaid',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'isLAT',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },

                {
                    data: 'region',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'countryCode',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'state',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'city',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'postalCode',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'ip',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },

                {
                    data: 'attributedTouchType',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'dayTimestamp',
                    render: function (data) {
                        if (data != null) {
                            var date = new Date(data);
                            const options = {weekday: 'long', year: 'numeric', month: 'long', day: 'numeric'};
                            return '<div style="margin-left:160px"></div>' + date.toLocaleString('en-US', options) + " " + date.toLocaleTimeString('en-US');
                        } else
                            return '<div style="margin-left:160px"></div>NA';
                    }
                },
                {
                    data: 'partnerId',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'partnerName',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                }



                /*{
                    data: 'attributedTouchType',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'attributedTouchTime',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'installTime',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'costModel',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'costValue',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'subParam1',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'subParam2',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'subParam3',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'subParam4',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'customerUserId',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'isRetargeting',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'storeProductPage',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'costCurrency',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'eventSource',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'partnerId',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'partnerName',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'mediaSource',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'channel',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'keywords',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'retargetingConversionType',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'isPrimaryAttribution',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'attributionLookback',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'reengagementWindow',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'matchType',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'httpReferrer',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'campaign',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'campaignId',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'adset',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'adsetId',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'ad',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'originalUrl',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'adId',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'adType',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'siteId',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },


                {
                    data: 'rejectedReason',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'rejectedReasonValue',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'blockedReason',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'blockedReasonRule',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'blockedReasonValue',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'blockedSubReason',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },


                {
                    data: 'approvedConversions',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'cancelledConversions',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'pendingConversions',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'rejectedClicks',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'approvedRevenue',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'cancelledRevenue',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'pendingRevenue',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'approvedPayout',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'clickCount',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'dayTimestamp',
                    render: function (data) {
                        if (data != null) {
                            var date = new Date(data);
                            const options = {weekday: 'long', year: 'numeric', month: 'long', day: 'numeric'};
                            return '<div style="margin-left:160px"></div>' + date.toLocaleString('en-US', options) + " " + date.toLocaleTimeString('en-US');
                        } else
                            return '<div style="margin-left:160px"></div>NA';
                    }

                },
                {
                    data: 'ratio',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                },
                {
                    data: 'ctr',
                    render: function (data) {
                        return ' <div style="margin-left:120px"></div>' + data;
                    }
                }*/

            ],
            "scrollCollapse": true,
            scrollY: 520,
            ordering: true
        });




    $(window).bind('resize', function (e) {
        if (window.RT) clearTimeout(window.RT);
        window.RT = setTimeout(function () {
            table.ajax.reload(); /* false to get page from cache */
        }, 250);
    });
    $('.dataTables_filter input').unbind();
    $('.dataTables_filter input').keyup(function (e) {
        if (e.keyCode === 39) /* if enter is pressed */ {
            table.search($(this).val()).draw();
        }
    });
    $('.sorting').click(function () {
        document.addEventListener("mouseover", exportAllData);
    })
    $(function () {
        var start = moment().subtract(6, 'days').startOf('day');
        var end = moment().endOf('day');

        function cb(start, end) {
            startDate = start;
            dateRange = document.getElementById("reportrangeinput").value = start.format('YYYY-MM-DD HH:mm:ss') + ' - ' + end.format('YYYY-MM-DD HH:mm:ss');
            document.getElementById("reportrangeinput").value = start.format('YYYY-MM-DD HH:mm:ss') + ' - ' + end.format('YYYY-MM-DD HH:mm:ss');
            alert("dateRange: " + dateRange);
            table.column(39).search(dateRange).draw();
            $('.btnDatatable').hide();
            $('#exportData').show();
        }

        $('#reportrange').daterangepicker({
            startDate: start,
            timePicker: true,
            timePicker24Hour: true,
            endDate: end,
            ranges: {
                'Today': [moment().startOf('day'), moment().endOf('day')],
                'Yesterday': [moment().subtract(1, 'days').startOf('day'), moment().subtract(1, 'days').endOf('day')],
                'Last 7 Days': [moment().subtract(6, 'days').startOf('day'), moment().endOf('day')],
                'Last 30 Days': [moment().subtract(29, 'days').startOf('day'), moment().endOf('day')],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        cb(start, end);

    });


    $('body').on('change', '#extSelect', function (e) {
        e.preventDefault();
        filter1 = '';
        var i, j;
        filter1 = $(this).val();
        alert("filter is " + filter1)
    });

    $(document).ready(function () {
        filter1 = '';
        filter1 = $("#extSelect").val();
        alert("filter2 is " + filter1 + " " + filter);
    });


    table.columns().visible(false);
    table.columns([3, 7, 23, 36, 37, 38]).visible(true);
    $('#submitForm').click(function () {
        alert("after button ");
        var myform = document.getElementById("attributedTouchTypeSelect").value;

        alert("heello" + myform);
        table.column(37).search(myform).draw();

        var values = Array.from(document.querySelectorAll('input[type=checkbox]:checked'))
            .map(item => item.value);
        /*.join(',');*/

        alert("after button " + values);
        alert("after button " + values.length);

        var i, j;
        $("#exampleModalAd1").hide();
        /*console.log(filter1.length);*/

        if (values.length > 0) {
            alert("inside if " + values)
            table.columns().visible(false);
            table.columns([3, 7, 23, 36, 37, 38]).visible(true);
            for (i = 0; i < values.length; i++) {
                switch (values[i]) {
                    case 'App Version':
                        table.columns(0).visible(true);
                        break;
                    case 'SDK Version':
                        table.columns(1).visible(true);
                        break;
                    case 'App Id':
                        table.columns(2).visible(true);
                        break;
                    case 'App Name':
                        table.columns(3).visible(true);
                        break;
                    case 'App Type':
                        table.columns(4).visible(true);
                        break;
                    case 'Bundle Id':
                        table.columns(5).visible(true);
                        break;

                    case 'Event Time':
                        table.columns(6).visible(true);
                        break;
                    case 'Event Name':
                        table.columns(7).visible(true);
                        break;
                    case 'Event Value':
                        table.columns(8).visible(true);
                        break;
                    case 'Event Revenue':
                        table.columns(9).visible(true);
                        break;
                    case 'Event Revenue Currency':
                        table.columns(10).visible(true);
                        break;
                    case 'Event Revenue USD':
                        table.columns(11).visible(true);
                        break;

                    case 'ATT':
                        table.columns(12).visible(true);
                        break;
                    case 'WIFI':
                        table.columns(13).visible(true);
                        break;
                    case 'Operator':
                        table.columns(14).visible(true);
                        break;
                    case 'Carrier':
                        table.columns(15).visible(true);
                        break;
                    case 'Language':
                        table.columns(16).visible(true);
                        break;
                    case 'Android Id':
                        table.columns(17).visible(true);
                        break;
                    case 'IMEI':
                        table.columns(18).visible(true);
                        break;
                    case 'IDFA':
                        table.columns(19).visible(true);
                        break;
                    case 'IDFV':
                        table.columns(20).visible(true);
                        break;
                    case 'Advertising Id':
                        table.columns(21).visible(true);
                        break;
                    case 'Device Model':
                        table.columns(22).visible(true);
                        break;
                    case 'Device Type':
                        table.columns(23).visible(true);
                        break;
                    case 'Device Category':
                        table.columns(24).visible(true);
                        break;
                    case 'Platform':
                        table.columns(25).visible(true);
                        break;
                    case 'OS Version':
                        table.columns(26).visible(true);
                        break;
                    case 'User Agent':
                        table.columns(27).visible(true);
                        break;
                    case 'Device Download Time':
                        table.columns(28).visible(true);
                        break;
                    case 'OAID':
                        table.columns(29).visible(true);
                        break;
                    case 'is LAT':
                        table.columns(30).visible(true);
                        break;

                    case 'Region':
                        table.columns(31).visible(true);
                        break;
                    case 'Country Code':
                        table.columns(32).visible(true);
                        break;
                    case 'State':
                        table.columns(33).visible(true);
                        break;
                    case 'City':
                        table.columns(34).visible(true);
                        break;
                    case 'Postal Code':
                        table.columns(35).visible(true);
                        break;
                    case 'IP':
                        table.columns(36).visible(true);
                        break;

                    case 'Attributed Touch Type':
                        table.columns(37).visible(true);
                        break;
                    case 'Attributed Touch Time':
                        table.columns(38).visible(true);
                        break;
                    case 'Partner Id':
                        table.columns(39).visible(true);
                        break;
                    case 'Partner Name':
                        table.columns(40).visible(true);
                        break;


                }

            }
        }


        $('.btnDatatable').hide();
        $('#exportData').show();

    });

    /*table.columns().visible(false);
    table.columns([]).visible(true);
    $('#submitForm').click(function () {
        var i, j;
        $("#exampleModalAd1").hide();
        /!*console.log(filter1.length);*!/
        if (filter1.length > 0) {
            table.columns().visible(false);
            table.columns([1]).visible(true);
            for (i = 0; i < filter1.length; i++) {
                switch (filter1[i]) {
                    case 'App ID':
                        table.columns(2).visible(true);
                        break;
                    case 'App Name':
                        table.columns(3).visible(true);
                        break;
                    case 'Partner Id':
                        table.columns(4).visible(true);
                        break;
                    case 'Partner Name':
                        table.columns(5).visible(true);
                        break;
                    case 'Click Count':
                        table.columns(6).visible(true);
                        break;
                    case 'Approved Conversion':
                        table.columns(7).visible(true);
                        break;
                    case 'Cancelled Conversion':
                        table.columns(8).visible(true);
                        break;
                    case 'Pending Conversion':
                        table.columns(9).visible(true);
                        break;
                    case 'Gross Conversion':
                        table.columns(10).visible(true);
                        break;
                    case 'Approved Revenue':
                        table.columns(11).visible(true);
                        break;
                    case 'Cancelled Revenue':
                        table.columns(12).visible(true);
                        break;
                    case 'Pending Revenue':
                        table.columns(13).visible(true);
                        break;
                    case 'Gross Revenue':
                        table.columns(14).visible(true);
                        break;
                    case 'Ratio':
                        table.columns(15).visible(true);
                        break;
                    case 'CTR':
                        table.columns(16).visible(true);
                        break;
                    case 'Day Timestamp':
                        table.columns(17).visible(true);
                        break;
                    case 'Rejected Clicks':
                        table.columns(18).visible(true);
                        break;
                }

            }
        }

        table.column(0).search(advFilter);
        table.column(21).search(appFilter);
        table.column(4).search(pubFilter);
        table.column(5).search(conversionFilter).draw();
        $('.btnDatatable').hide();
        $('#exportData').show();

    });*/


    $('#appFilter').click(function () {
        $("#exampleModalAd1").show();
    });


    /*    $('select#publisherListAjax')
            .change(
                function () {
                    pubFilter = '';
                    $(
                        'select#publisherListAjax option:selected')
                        .each(function () {
                            pubFilter += $(this).val() + ",";
                        });
                    pubFilter = pubFilter.substring(0, pubFilter.length - 1);
                    /!*console.log("pubFilter:" + pubFilter);*!/
                });*/


    /*    $('select#advertiserListAjax')
            .change(
                function () {
                    advFilter = '';
                    $(
                        'select#advertiserListAjax option:selected')
                        .each(function () {
                            advFilter += $(this).val() + ",";
                        });
                    advFilter = advFilter.substring(0, advFilter.length - 1);
                    /!*console.log("pubFilter:" + pubFilter);*!/
                });*/


    /*    $('select#offerListAjax')
            .change(
                function () {
                    appFilter = '';
                    $(
                        'select#offerListAjax option:selected')
                        .each(function () {
                            appFilter += $(this).val() + ",";
                        });
                    appFilter = appFilter.substring(0, appFilter.length - 1);
                });*/


    /*
    $('select#conversionStatus')
        .change(
            function () {
                conversionFilter = '';
                $(
                    'select#conversionStatus option:selected')
                    .each(function () {
                        conversionFilter += $(this).val() + ",";
                    });
                conversionFilter = conversionFilter.substring(0, conversionFilter.length - 1);
            });*/


    $('#searchbox').on('keyup', function () {
        table.search(this.value).draw();
        /*console.log(this.value);*/
    });

</script>

<script>

    function addFilter() {
        var myform = document.getElementById("formSubmit1");
        alert("myForm " + myform);
        var fd = new FormData(myform);
        alert("heello");
        event.preventDefault();
        $.ajax({
            url: "/adminConsole/addFilterForm",
            data: fd,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            beforeSend: function () {

                alert("before.");

            },
            success: function (response) {
                alert("success ");
                showNotification('top', 'left', 'Event has been created.', 'success');
                setTimeout(function () {
                    window.location.reload();
                }, 1000);
                $('#signupModal').modal('hide');
            },
            complete: function () {
                alert("complete ");
                // Hide image container

            },
            error: function (jqXHR) {
                var o = $.parseJSON(jqXHR.responseText);
                window.open('/adminConsole/error?status=' + o.status + '&message=' + o.error, "_self")
            }
        });
    }

</script>

<script>

    <%--<script>
        // var dateRange = '';
        var appFilter = '';
        var advertiserFilter = '';
        var subAffFilter = '';
        var partnerFilter = '';
        var groupByFilter = '';
        var filter1 = '';

        var dateFromServer = "<%=reportFilterDTO.getDaterange()%>";
        /*console.log("dateFromServer:" + dateFromServer);*/

        $(function () {
            var start = (dateFromServer == "null" || dateFromServer == "") ? moment().subtract(6, 'days').startOf('day') :
                new Date(dateFromServer.split(" - ")[0]);
            var end = (dateFromServer == "null" || dateFromServer == "") ? moment().endOf('day') :
                new Date(dateFromServer.split(" - ")[1]);

            function cb(start, end) {
                document.getElementById("reportrangeinput").value = start.format('YYYY-MM-DD HH:mm:ss') + ' - ' + end.format('YYYY-MM-DD HH:mm:ss');
                dateFilter();
            }

            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                    'Today': [moment().startOf('day'), moment().endOf('day')],
                    'Yesterday': [moment().subtract(1, 'days').startOf('day'), moment().subtract(1, 'days').endOf('day')],
                    'Last 7 Days': [moment().subtract(6, 'days').startOf('day'), moment().endOf('day')],
                    'Last 30 Days': [moment().subtract(29, 'days').startOf('day'), moment().endOf('day')],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);
            cb(start, end);
            dateFilter();

        });

        document.getElementById("dateHiddenField").value = document.getElementById("reportrangeinput").value;

        function dateFilter() {
            /*console.log("Inside dateFilterWithFormData");*/
            /*console.log(document.getElementById("reportrangeinput").value);*/
            document.getElementById("dateHiddenField").value = document.getElementById("reportrangeinput").value;
            /*document.getElementById("formSubmit").submit();*/
        }--%>


    $('body').on('change', '#extSelect', function (e) {
        /*console.log("Inside filter1")*/
        e.preventDefault();
        filter1 = '';
        filter1 = $(this).val();
    });

    $('body').on('change', '#subAffiliate', function (e) {
        subAffFilter = $(this).val();
        /*console.log("subAffFilter:" + subAffFilter);*/
    });

    $('#click').click(function () {
        $("#exampleModalAd1").show();
    });


    $('select#advertSelect')
        .change(
            function () {
                advertiserFilter = '';
                $(
                    'select#advertSelect option:selected')
                    .each(function () {
                        advertiserFilter += $(this).val() + ",";
                    });
                advertiserFilter = advertiserFilter.substring(0, advertiserFilter.length - 1);
                /*console.log("advertiserFilter:" + advertiserFilter);*/
            });


    $('select#appSelect')
        .change(
            function () {
                appFilter = '';
                $(
                    'select#appSelect option:selected')
                    .each(function () {
                        appFilter += $(this).val() + ",";
                    });
                appFilter = appFilter.substring(0, appFilter.length - 1);
                /*console.log("appFilter:" + appFilter);*/
            });


    $('select#partnerSelect')
        .change(
            function () {
                partnerFilter = '';
                $(
                    'select#partnerSelect option:selected')
                    .each(function () {
                        partnerFilter += $(this).val() + ",";
                    });
                partnerFilter = partnerFilter.substring(0, partnerFilter.length - 1);
                /*console.log("partnerFilter:" + partnerFilter);*/
            });
    $('select#groupBy')
        .change(
            function () {
                groupByFilter = '';
                $(
                    'select#groupBy option:selected')
                    .each(function () {
                        groupByFilter += $(this).val() + ",";
                    });
                groupByFilter = groupByFilter.substring(0, groupByFilter.length - 1);
                /*console.log("groupByFilter:" + groupByFilter);*/
            });

</script>

<script>
    $(document).ready(function () {
        filter1 = '';
        filter1 = $("#extSelect").val();
        // initialise Datetimepicker and Sliders
        md.initFormExtendedDatetimepickers();
        if ($('.slider').length != 0) {
            md.initSliders();
        }
    });
</script>

<script>
    $('#groupBy').select2();
    $('#extSelect').select2();

    $('.multipleSelect').select2();


    $("#appFilter").click(function () {
        alert("appFilter");
        offerLoad('#exampleModalAd', 'SELECT PARTNER', '/adminConsole/findByPartnerCompany1', '.partnerSelect');

    });

    function offerLoad(modalName, title, url, className) {
        /*console.log("Inside offerLoad:" + className);*/
        var perPageRecord = 10;
        $(className).select2({
            ajax: {
                url: url,
                type: 'GET',
                dataType: 'json',
                delay: 500,
                data: function (params) {
                    return {
                        search: params.term, // search term
                        page: params.page || 0,
                        size: perPageRecord//Per page records
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 0;
                    return {
                        results: data.results,
                        pagination: {
                            more: params.page < data.count_filtered - 1
                        }
                    };
                },
                cache: true
            },
            placeholder: title,
            minimumInputLength: 0
        });
    }

</script>


<%--    $(window).bind('resize', function (e) {
        if (window.RT) clearTimeout(window.RT);
        window.RT = setTimeout(function () {
            table.ajax.reload(); /* false to get page from cache */
        }, 250);
    });
    $('.dataTables_filter input').unbind();
    $('.dataTables_filter input').keyup(function (e) {
        if (e.keyCode === 13) /* if enter is pressed */ {
            table.search($(this).val()).draw();
        }
    });
    $('.sorting').click(function () {
        document.addEventListener("mouseover", exportAllData);
    })
    $(function () {
        var start = moment().subtract(6, 'days').startOf('day');
        var end = moment().endOf('day');

        function cb(start, end) {
            startDate = start;
            dateRange = document.getElementById("reportrangeinput").value = start.format('YYYY-MM-DD HH:mm:ss') + ' - ' + end.format('YYYY-MM-DD HH:mm:ss');
            document.getElementById("reportrangeinput").value = start.format('YYYY-MM-DD HH:mm:ss') + ' - ' + end.format('YYYY-MM-DD HH:mm:ss');
            /*console.log("dateRange: " + dateRange);*/
            table.column(13).search(dateRange).draw();
            $('.btnDatatable').hide();
            $('#exportData').show();
        }

        $('#reportrange').daterangepicker({
            startDate: start,
            timePicker: true,
            timePicker24Hour: true,
            endDate: end,
            ranges: {
                'Today': [moment().startOf('day'), moment().endOf('day')],
                'Yesterday': [moment().subtract(1, 'days').startOf('day'), moment().subtract(1, 'days').endOf('day')],
                'Last 7 Days': [moment().subtract(6, 'days').startOf('day'), moment().endOf('day')],
                'Last 30 Days': [moment().subtract(29, 'days').startOf('day'), moment().endOf('day')],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        cb(start, end);

    });

    /*
        $('body').on('change', '#extSelect', function (e) {
            e.preventDefault();
            filter1 = '';
            var i, j;
            filter1 = $(this).val();
            alert("filter is " + filter1)
        });

        $(document).ready(function () {
            filter1 = '';
            filter1 = $("input[type=checkbox]:checked");
            alert("filter2 is " + filter1 + " " + filter);
        });*/



    /*function getCheckedOptions(form) {
        alert("getCheckedOptions ");
        var inputs = form.children;  // get all inputs in the form
        alert("inputs " + inputs);
        var products = [];           // will store the selected products values

        const values = Array.from(document.querySelectorAll('input[type=checkbox]:checked'))
            .map(item => item.value)
            .join(',');

        // cycle through all inputs
        for( var i = 0; i < inputs.length; i++ ){
            if( inputs[i].checked ){
                // only record the checked inputs
                products.push(inputs[i].value);
            }
        }
        alert("getCheckedOptions " + products);

        // update the `action` of the form - alternatively you could just store it as a string to use somewhere else in your code if you're using AJAX or something
        /!*form.action = "http://test.com/addtocart?"+products.join(",");*!/
    }*/

    /*    function submit() {
            alert("values.join()");
            var values = [];
            $("input[type=checkbox]:checked").each(function(){
                values.push($(this).next("a").text());
            });
            alert(values.join());
        }*/


    table.columns().visible(false);
    table.columns([2, 3, 4, 5, 11, 12, 13]).visible(true);
    $('#submitForm').click(function () {
        alert("after button ");

        var values = Array.from(document.querySelectorAll('input[type=checkbox]:checked'))
            .map(item => item.value);
        /*.join(',');*/

        alert("after button " + values);
        alert("after button " + values.length);

        var i, j;
        $("#exampleModalAd1").hide();
        /*console.log(filter1.length);*/

        if (values.length > 0) {
            alert("inside if " + filter1)
            table.columns().visible(false);
            table.columns([2, 3, 4, 5, 11, 12]).visible(true);
            for (i = 0; i < filter1.length; i++) {
                switch (filter1[i]) {
                    case 'App ID':
                        table.columns(0).visible(true);
                        break;
                    case 'App Name':
                        table.columns(1).visible(true);
                        break;
                    case 'Partner Id':
                        table.columns(2).visible(true);
                        break;
                    case 'Partner Name':
                        table.columns(3).visible(true);
                        break;
                    case 'Click Count':
                        table.columns(4).visible(true);
                        break;
                    case 'Approved Conversion':
                        table.columns(5).visible(true);
                        break;
                    case 'Cancelled Conversion':
                        table.columns(6).visible(true);
                        break;
                    case 'Pending Conversion':
                        table.columns(7).visible(true);
                        break;
                    case 'Approved Revenue':
                        table.columns(8).visible(true);
                        break;
                    case 'Cancelled Revenue':
                        table.columns(9).visible(true);
                        break;
                    case 'Pending Revenue':
                        table.columns(10).visible(true);
                        break;
                    case 'Ratio':
                        table.columns(11).visible(true);
                        break;
                    case 'CTR':
                        table.columns(12).visible(true);
                        break;
                    case 'Day Timestamp':
                        table.columns(13).visible(true);
                        break;
                    case 'Rejected Clicks':
                        table.columns(14).visible(true);
                        break;
                }

            }
        }


        $('.btnDatatable').hide();
        $('#exportData').show();

    });


    /*table.columns().visible(false);
    table.columns([]).visible(true);
    $('#submitForm').click(function () {
        var i, j;
        $("#exampleModalAd1").hide();
        /!*console.log(filter1.length);*!/
        if (filter1.length > 0) {
            table.columns().visible(false);
            table.columns([1]).visible(true);
            for (i = 0; i < filter1.length; i++) {
                switch (filter1[i]) {
                    case 'App ID':
                        table.columns(2).visible(true);
                        break;
                    case 'App Name':
                        table.columns(3).visible(true);
                        break;
                    case 'Partner Id':
                        table.columns(4).visible(true);
                        break;
                    case 'Partner Name':
                        table.columns(5).visible(true);
                        break;
                    case 'Click Count':
                        table.columns(6).visible(true);
                        break;
                    case 'Approved Conversion':
                        table.columns(7).visible(true);
                        break;
                    case 'Cancelled Conversion':
                        table.columns(8).visible(true);
                        break;
                    case 'Pending Conversion':
                        table.columns(9).visible(true);
                        break;
                    case 'Gross Conversion':
                        table.columns(10).visible(true);
                        break;
                    case 'Approved Revenue':
                        table.columns(11).visible(true);
                        break;
                    case 'Cancelled Revenue':
                        table.columns(12).visible(true);
                        break;
                    case 'Pending Revenue':
                        table.columns(13).visible(true);
                        break;
                    case 'Gross Revenue':
                        table.columns(14).visible(true);
                        break;
                    case 'Ratio':
                        table.columns(15).visible(true);
                        break;
                    case 'CTR':
                        table.columns(16).visible(true);
                        break;
                    case 'Day Timestamp':
                        table.columns(17).visible(true);
                        break;
                    case 'Rejected Clicks':
                        table.columns(18).visible(true);
                        break;
                }

            }
        }

        table.column(0).search(advFilter);
        table.column(21).search(appFilter);
        table.column(4).search(pubFilter);
        table.column(5).search(conversionFilter).draw();
        $('.btnDatatable').hide();
        $('#exportData').show();

    });*/


    $('#appFilter').click(function () {
        $("#exampleModalAd1").show();
    });


    /*    $('select#publisherListAjax')
            .change(
                function () {
                    pubFilter = '';
                    $(
                        'select#publisherListAjax option:selected')
                        .each(function () {
                            pubFilter += $(this).val() + ",";
                        });
                    pubFilter = pubFilter.substring(0, pubFilter.length - 1);
                    /!*console.log("pubFilter:" + pubFilter);*!/
                });*/


    /*    $('select#advertiserListAjax')
            .change(
                function () {
                    advFilter = '';
                    $(
                        'select#advertiserListAjax option:selected')
                        .each(function () {
                            advFilter += $(this).val() + ",";
                        });
                    advFilter = advFilter.substring(0, advFilter.length - 1);
                    /!*console.log("pubFilter:" + pubFilter);*!/
                });*/


    /*    $('select#offerListAjax')
            .change(
                function () {
                    appFilter = '';
                    $(
                        'select#offerListAjax option:selected')
                        .each(function () {
                            appFilter += $(this).val() + ",";
                        });
                    appFilter = appFilter.substring(0, appFilter.length - 1);
                });*/


    /*
    $('select#conversionStatus')
        .change(
            function () {
                conversionFilter = '';
                $(
                    'select#conversionStatus option:selected')
                    .each(function () {
                        conversionFilter += $(this).val() + ",";
                    });
                conversionFilter = conversionFilter.substring(0, conversionFilter.length - 1);
            });*/


    $('#searchbox').on('keyup', function () {
        table.search(this.value).draw();
        /*console.log(this.value);*/
    });

</script>

<script>

    function addFilter() {
        var myform = document.getElementById("formSubmit1");
        alert("myForm " + myform);
        var fd = new FormData(myform);
        alert("heello");
        event.preventDefault();
        $.ajax({
            url: "/adminConsole/addFilterForm",
            data: fd,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            beforeSend: function () {

                alert("before.");

            },
            success: function (response) {
                alert("success ");
                showNotification('top', 'left', 'Event has been created.', 'success');
                setTimeout(function () {
                    window.location.reload();
                }, 1000);
                $('#signupModal').modal('hide');
            },
            complete: function () {
                alert("complete ");
                // Hide image container

            },
            error: function (jqXHR) {
                var o = $.parseJSON(jqXHR.responseText);
                window.open('/adminConsole/error?status=' + o.status + '&message=' + o.error, "_self")
            }
        });
    }

</script>

<script>

    &lt;%&ndash;<script>
        // var dateRange = '';
        var appFilter = '';
        var advertiserFilter = '';
        var subAffFilter = '';
        var partnerFilter = '';
        var groupByFilter = '';
        var filter1 = '';

        var dateFromServer = "<%=reportFilterDTO.getDaterange()%>";
        /*console.log("dateFromServer:" + dateFromServer);*/

        $(function () {
            var start = (dateFromServer == "null" || dateFromServer == "") ? moment().subtract(6, 'days').startOf('day') :
                new Date(dateFromServer.split(" - ")[0]);
            var end = (dateFromServer == "null" || dateFromServer == "") ? moment().endOf('day') :
                new Date(dateFromServer.split(" - ")[1]);

            function cb(start, end) {
                document.getElementById("reportrangeinput").value = start.format('YYYY-MM-DD HH:mm:ss') + ' - ' + end.format('YYYY-MM-DD HH:mm:ss');
                dateFilter();
            }

            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                    'Today': [moment().startOf('day'), moment().endOf('day')],
                    'Yesterday': [moment().subtract(1, 'days').startOf('day'), moment().subtract(1, 'days').endOf('day')],
                    'Last 7 Days': [moment().subtract(6, 'days').startOf('day'), moment().endOf('day')],
                    'Last 30 Days': [moment().subtract(29, 'days').startOf('day'), moment().endOf('day')],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);
            cb(start, end);
            dateFilter();

        });

        document.getElementById("dateHiddenField").value = document.getElementById("reportrangeinput").value;

        function dateFilter() {
            /*console.log("Inside dateFilterWithFormData");*/
            /*console.log(document.getElementById("reportrangeinput").value);*/
            document.getElementById("dateHiddenField").value = document.getElementById("reportrangeinput").value;
            /*document.getElementById("formSubmit").submit();*/
        }&ndash;%&gt;


    $('body').on('change', '#extSelect', function (e) {
        /*console.log("Inside filter1")*/
        e.preventDefault();
        filter1 = '';
        filter1 = $(this).val();
    });

    $('body').on('change', '#subAffiliate', function (e) {
        subAffFilter = $(this).val();
        /*console.log("subAffFilter:" + subAffFilter);*/
    });

    $('#click').click(function () {
        $("#exampleModalAd1").show();
    });


    $('select#advertSelect')
        .change(
            function () {
                advertiserFilter = '';
                $(
                    'select#advertSelect option:selected')
                    .each(function () {
                        advertiserFilter += $(this).val() + ",";
                    });
                advertiserFilter = advertiserFilter.substring(0, advertiserFilter.length - 1);
                /*console.log("advertiserFilter:" + advertiserFilter);*/
            });


    $('select#appSelect')
        .change(
            function () {
                appFilter = '';
                $(
                    'select#appSelect option:selected')
                    .each(function () {
                        appFilter += $(this).val() + ",";
                    });
                appFilter = appFilter.substring(0, appFilter.length - 1);
                /*console.log("appFilter:" + appFilter);*/
            });


    $('select#partnerSelect')
        .change(
            function () {
                partnerFilter = '';
                $(
                    'select#partnerSelect option:selected')
                    .each(function () {
                        partnerFilter += $(this).val() + ",";
                    });
                partnerFilter = partnerFilter.substring(0, partnerFilter.length - 1);
                /*console.log("partnerFilter:" + partnerFilter);*/
            });
    $('select#groupBy')
        .change(
            function () {
                groupByFilter = '';
                $(
                    'select#groupBy option:selected')
                    .each(function () {
                        groupByFilter += $(this).val() + ",";
                    });
                groupByFilter = groupByFilter.substring(0, groupByFilter.length - 1);
                /*console.log("groupByFilter:" + groupByFilter);*/
            });

</script>

<script>
    $(document).ready(function () {
        filter1 = '';
        filter1 = $("#extSelect").val();
        // initialise Datetimepicker and Sliders
        md.initFormExtendedDatetimepickers();
        if ($('.slider').length != 0) {
            md.initSliders();
        }
    });
</script>

<script>
    $('#groupBy').select2();
    $('#extSelect').select2();

    $('.multipleSelect').select2();


    $("#appFilter").click(function () {
        alert("appFilter");
        offerLoad('#exampleModalAd', 'SELECT PARTNER', '/adminConsole/findByPartnerCompany1', '.partnerSelect');

    });

    function offerLoad(modalName, title, url, className) {
        /*console.log("Inside offerLoad:" + className);*/
        var perPageRecord = 10;
        $(className).select2({
            ajax: {
                url: url,
                type: 'GET',
                dataType: 'json',
                delay: 500,
                data: function (params) {
                    return {
                        search: params.term, // search term
                        page: params.page || 0,
                        size: perPageRecord//Per page records
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 0;
                    return {
                        results: data.results,
                        pagination: {
                            more: params.page < data.count_filtered - 1
                        }
                    };
                },
                cache: true
            },
            placeholder: title,
            minimumInputLength: 0
        });
    }

</script>--%>

</body>

</html>


<%--<%@ page import="constants.com.attribution.panel.UIConstants" %>
<%@ page import="dto.console.com.attribution.panel.ReportFilterDTO" %>
<%@ page import="java.util.List" %>
<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html lang="en">

<head>
    <meta charset="utf-8"/>
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="../../asset/img/logoAttribution.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>All Report</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport'/>
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- CSS Files -->
    <link href="../assets/css/material-dashboard.css?v=2.1.0" rel="stylesheet"/>
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="../assets/demo/demo.css" rel="stylesheet"/>
    <link href="../assets/css/style-sheet-adjar.css?v=2.1.0" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="../assets/css/daterangepicker.css"/>


    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <link rel="stylesheet" href="../assets/charts/dark/styles.css">
    <link href="../assets/css/style-sheet-attribution.css?v=2.1.0" rel="stylesheet"/>

    <link rel="stylesheet" href="../assets/fastselect.min.css">
    &lt;%&ndash;loader CSS Files&ndash;%&gt;
    <link href="../assets/css/loader.css" rel="stylesheet"/>
    <link href="../dist/css/select2.css" rel="stylesheet"/>
    &lt;%&ndash;responsive css file&ndash;%&gt;
    <link href="../../assets/css/responsive-modal-pages.css" rel="stylesheet"/>

    &lt;%&ndash;    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>&ndash;%&gt;

    &lt;%&ndash;<style>
        .dataTables_length::after {
            top: -2px;
            right: 65px;
        }

        .dataTables_paginate {
            margin-top: -30px !important;
        }

        .dataTables_info {
            margin-left: 20px;
        }

        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            padding: 3px 10px !important;
        }

        .hide {
            display: none;
        }

        .dataTables_wrapper .dt-buttons {
            position: absolute;
            float: none;
        !important;
            text-align: left !important;
            margin-top: -83px;
            margin-right: 53%;

        }

        @media screen and (min-width: 576px) and (max-width: 767px) {
            .dt-buttons {
                width: 285px !important;
                margin-top: -108px !important;
                margin-left: -115px;

            }

            .filterButtonRow1 {
                margin-right: -75px !important;
            }

            .reportRange {
                position: absolute;
                right: 50px !important;
                top: -5px;

            }

            .navbar {
                z-index: 1 !important;
            }
        }

        @media screen and (max-width: 575px) {

            .filterButtonRow1 {
                margin-left: 300px !important;
                margin-top: -2px !important;
                float: left !important;
            }

            .dt-buttons {
                margin-right: 100px !important;
                margin-top: -85px !important;
            }

        }

        @media screen and (min-width: 768px) and (max-width: 812px) {
            .dt-buttons {
                width: 285px !important;
                margin-top: -75px !important;
                margin-left: -110px;
            }

            .filterButtonRow1 {
                margin-right: -75px !important;
            }
        }

        @media screen and (min-width: 813px) and (max-width: 1039px) {
            .dt-buttons {
                width: 285px !important;
                left: 200px !important;
            }

            #reportrangeinput {
                width: 245px !important;
                font-size: 12px;
            }

            .filterButtonRow1 {
                margin-right: -340px !important;
            }
        }

        @media screen and (min-width: 1040px) {
            .dt-buttons {
                width: 285px !important;
                left: 200px !important;
            }
        }

    </style>&ndash;%&gt;

</head>

<%
    String cp = request.getContextPath();
%>

<body class="" style="background-color: <%=UIConstants.primaryBackgroundColor%>;overflow-x:hidden">

<div class="main-panel">
    <jsp:include page="integratedSidebar.jsp">
        <jsp:param value="active" name="allReport"/>
    </jsp:include>

    <div class="wrapper ">


        <div class="content">
            <div class="container-fluid">
                <div class="cardDiv">
                    <div class="cardSize">
                        <i class="material-icons iconSize">assignment</i>
                    </div>
                    <h2 class="cardHeading">All Report</h2>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card <%=UIConstants.primaryTextColorClass%>"
                             style="background-color: <%=UIConstants.primaryCardBackground%>;">

                            <div class="card-header card-header-warning card-header-icon allReportHeader">

                                <div class="row pull-right reportRange" style="margin-top:10px;margin-right:10px">
                                    <div class="col-sm-9">
                                        <!-- <i class="fa fa-caret-down"></i> -->
                                        <div class=" form-group float-right text-dark" id="reportrange"
                                             style="margin-top: -53px;">
                                            <input readonly type="text"
                                                   class="<%=UIConstants.primaryTextColorClass%>"
                                                   id="reportrangeinput"
                                                   name="daterange"
                                            &lt;%&ndash;value="${filters.daterange}"&ndash;%&gt;
                                                   style="cursor: pointer; background-color: <%=UIConstants.primaryBackgroundColor%>; border:1px solid <%=UIConstants.borderColor%>;border-radius:5px; width: 295px;padding:2px 6px">
                                        </div>
                                    </div>

                                </div>

                                <div class="row pull-right filterButtonRow1"
                                     style="margin-top:-4px;margin-right:-380px">
                                    <div class="col-sm-7" style="">
                                        <button class="btn btn-<%=UIConstants.primaryColorClass%> filterButton pull-right"
                                                id="appFilter"
                                                data-toggle="modal"
                                                style="cursor: pointer;margin-top:-39px;height:28px;border-radius:5px;border:1.8px solid #aaa"
                                                data-target="#exampleModalAd">
                                            <i class="fa fa-search" aria-hidden="true" data-toggle="modal"
                                               data-target="#addRule"
                                               id="reset"
                                               style="margin-left:0px;margin-top:-16px;width:auto;font-size:16px"><b
                                                    style="margin-left:6px">Search&nbsp;</b></i>
                                        </button>
                                    </div>
                                </div>

                            </div>

                            <c:set var="data" value="${groupBy}" scope="request"/>
                            <% List<String> stringList = (List<String>) request.getAttribute("groupBy");%>

                            <div class="card-body">
                                <div class="toolbar <%=UIConstants.primaryTextColorClass%>"
                                     style="font-family: <%=UIConstants.primaryFontFamily%>;">
                                    <!--        Here you can write extra buttons/actions for the toolbar              -->

                                    <!-- Modal -->

                                    <div class="modal fade" id="exampleModalAd" tabindex="-1" role="dialog"
                                         aria-labelledby="exampleModalLongTitle" aria-hidden="true"
                                         data-backdrop="static">
                                        <div class="modal-dialog modal-lg" role="document" id="exampleModalAd1"
                                             style="background-color: <%=UIConstants.primaryModalCardHeaderBackground%>; width: 900px;">
                                            <div class="card modal-content<%=UIConstants.primaryTextColorClass%> allReportContent"
                                                 style="background-color: <%=UIConstants.primaryModalCardHeaderBackground%>; width: 900px;">
                                                <div class="modalClose">
                                                    <i class="fa fa-times" data-dismiss="modal" aria-label="Close"></i>
                                                </div>
                                                <div class="card-header card-header-<%=UIConstants.primaryColorClass%> card-header-icon"
                                                     style="height:80px">
                                                    <div class="card-icon">
                                                        <i class="material-icons">dashboard</i>
                                                    </div>
                                                    <h4 class="card-title <%=UIConstants.primaryTextColorClass%>"
                                                        style="font-family: <%=UIConstants.primaryFontFamily%>; font-weight: bold"
                                                        id="modalHeading1">Search Report</h4>
                                                    <p class="card-category">Enter Information</p>
                                                </div>

                                                <div class="card-body allReportCard"
                                                     style="font-family: <%=UIConstants.primaryFilterFontFamilyTable%>; width: 900px; background-color: <%=UIConstants.primaryCardModalBackground%>">
                                                    &lt;%&ndash;<div id="modalForm">

                                                        <div>

                                                            <hr style="height: 5px;border-width:0;color:gray;background-color:#303654; margin-top: 2px;">

                                                            <div class="navOfferDetails"
                                                                 style="margin-top:14px;background-color:<%=UIConstants.primaryCardBackground%>;height:50px;">
                                                                <ul class="nav nav-pills nav-pills-<%=UIConstants.primaryColorClass%>  <%=UIConstants.primaryTextColorClass%> justify-content-center"
                                                                    role="tablist">

                                                                    <li class="nav-item">
                                                                        <a class="nav-link active <%=UIConstants.textColor%>"
                                                                           data-toggle="tab"
                                                                           href="#preDefineEvent"
                                                                           role="tablist" id="nav-link"
                                                                           style="font-size: 15px;">
                                                                            Pre Define Event
                                                                        </a>
                                                                    </li>

                                                                    <li class="nav-item">
                                                                        <a class="nav-link <%=UIConstants.textColor%>"
                                                                           data-toggle="tab"
                                                                           href="#userDefineEvent"
                                                                           role="tablist" id="nav-link"
                                                                           style="font-size: 15px;">
                                                                            User Define Event
                                                                        </a>
                                                                    </li>

                                                                </ul>
                                                            </div>

                                                            <hr style="height: 5px;border-width:0;color:gray;background-color:#303654; margin-top: 13px;">


                                                            <div class="tab-content tab-space">

                                                                <div class="tab-pane active" id="preDefineEvent">
                                                                    <div class="row"
                                                                         style="width:100%;margin-left:0px"><br>
                                                                        <div class="card <%=UIConstants.primaryTextColorClass%>"
                                                                             Style="font-family:<%=UIConstants.primaryFontFamily%>;background-color:<%=UIConstants.primaryCardBackground%>;margin-top:-25px;">
                                                                            <div class="card-header card-header-warning card-header-text">

                                                                            </div>
                                                                            <div class="card-body <%=UIConstants.primaryTextColorClass%> infoTabCard"
                                                                                 style="margin-left:0px;">
                                                                                <br>

                                                                                &lt;%&ndash;<form method="post"
                                                                                      id="addPreEventForm"
                                                                                      class="form-horizontal"
                                                                                      onsubmit="addPreEvent()">
                                                                                    <input type="text"
                                                                                           name="eventId"
                                                                                           id="eventId"
                                                                                           style="display:none">

                                                                                    <div class="row"
                                                                                         style="margin-top:0;margin-left:0px; width: 500px;">

                                                                                        <div class="&lt;%&ndash;col-sm-3&ndash;%&gt;">
                                                                                            <label class="col-form-label<%=UIConstants.primaryTextColorClass%>"
                                                                                                   style="color: black; font-weight:bold;margin-left:-1px;">SELECT
                                                                                                EVENT:</label>
                                                                                        </div>

                                                                                        <div class="&lt;%&ndash;col-sm-9&ndash;%&gt;"
                                                                                             style="color:<%=UIConstants.textColor%>;margin-left:20px; width: 310px;">

                                                                                            <select class="ajaxPreDefineEvent"
                                                                                                    id="ajaxPreDefineEvent"
                                                                                                    name="ajaxPreDefineEvent"
                                                                                                    multiple
                                                                                                    data-size="7"
                                                                                                    style="width:300px"
                                                                                                    style="color:<%=UIConstants.textColor%>; "
                                                                                            ></select>
                                                                                        </div>

                                                                                    </div>

                                                                                    <br>
                                                                                    <br>
                                                                                    <br>

                                                                                    <input type="hidden"
                                                                                           name="appId"
                                                                                           value="${app.id}">

                                                                                    <div class="row offerDetailsBottomLine"
                                                                                         style="border-top:1px solid #423b3a;width:490px;margin-left:-20px;margin-top: -10px">
                                                                                        <br>
                                                                                        <div class="col-sm-12">
                                                                                            <button class="btn btn-<%=UIConstants.primaryColorClass%> btn-round pull-right submitButton"
                                                                                                    type="submit"
                                                                                                    style="padding:8px 10px;;margin-top:10px;margin-right:30px">
                                                                                                <b style="margin-left:0px">Save
                                                                                                    Event</b>&nbsp;
                                                                                            </button>
                                                                                        </div>
                                                                                    </div>


                                                                                </form>&ndash;%&gt;

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="tab-pane" id="userDefineEvent">
                                                                    <div class="row"
                                                                         style="width:100%;margin-left:0px"><br>
                                                                        <div class="card <%=UIConstants.primaryTextColorClass%>"
                                                                             Style="font-family:<%=UIConstants.primaryFontFamily%>;background-color:<%=UIConstants.primaryCardBackground%>;margin-top:-25px;">
                                                                            <div class="card-header card-header-warning card-header-text">

                                                                            </div>

                                                                            <div class="card-body <%=UIConstants.primaryTextColorClass%> infoTabCard"
                                                                                 style="background-color:<%=UIConstants.primaryCardBackground%>">
                                                                                <br>

                                                                                &lt;%&ndash;<form method="post"
                                                                                      id="addEventForm"
                                                                                      class="form-horizontal"
                                                                                      onsubmit="addEvent()">

                                                                                    <input type="text"
                                                                                           name="eventId"
                                                                                           id="eventId"
                                                                                           style="display:none">

                                                                                    <div class="row">
                                                                                        <div class="col-sm-3">
                                                                                            <label class=" col-form-label <%=UIConstants.primaryTextColorClass%>"
                                                                                                   style="font-weight:bold;margin-left:-1px;margin-top:-9px">Event
                                                                                                Name*:</label>
                                                                                        </div>
                                                                                        <div class="col-sm-9">
                                                                                            <input type="text"
                                                                                                   oninvalid="this.setCustomValidity('Please Enter Event Name')"
                                                                                                   oninput="setCustomValidity('')"
                                                                                                   class="inputText eventName"
                                                                                                   id="eventName"
                                                                                                   name="eventName"
                                                                                                   style="color:<%=UIConstants.textColor%>;margin-left:-20px"
                                                                                                   onchange="limitText(this,255)"
                                                                                                   onkeypress="limitText(this,255)"
                                                                                                   required>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="row rowMargin">
                                                                                        <div class="col-md-3">

                                                                                            <label class="col-form-label <%=UIConstants.primaryTextColorClass%>"
                                                                                                   style="font-weight:bold;margin-left:-1px;margin-top:13px">Description:</label>

                                                                                        </div>
                                                                                        <div class="col-sm-9 eventDescription">
                                                                                            <div class="form-group">
                                                                            <textarea type="text" rows="4"
                                                                                      class="textArea"
                                                                                      oninput="auto_grow(this)"
                                                                                      id="description"
                                                                                      name="description"
                                                                                      style="color:<%=UIConstants.textColor%>;margin-left:-20px;margin-top:12px;padding:6px 10px"
                                                                                      onchange="limitText(this,5000)"
                                                                                      onkeypress="limitText(this,5000)"></textarea>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="row rowMargin">

                                                                                        <div class="col-md-4">
                                                                                            <label class="col-form-label label-checkbox <%=UIConstants.primaryTextColorClass%>"
                                                                                                   style="font-weight:bold;margin-left:-1px">Event
                                                                                                Status*:</label>
                                                                                        </div>

                                                                                        <div class="col-sm-8 checkbox-radios radiosBox">
                                                                                            <div class="col-sm-2 form-check form-check-inline">
                                                                                                <label class="form-check-label <%=UIConstants.primaryTextColorClass%>"
                                                                                                       style="font-weight:bold;margin-left:-40px">
                                                                                                    <input class="form-check-input"
                                                                                                           type="radio"
                                                                                                           id="status"
                                                                                                           name="status"
                                                                                                           value="0"
                                                                                                           checked>
                                                                                                    Active
                                                                                                    <span class="circle"
                                                                                                          style="margin-top:2px"> <span
                                                                                                            class="check"></span></span>
                                                                                                </label>
                                                                                            </div>
                                                                                            <div class="col-sm-3 form-check form-check-inline offerDetailsInactiveStatus">
                                                                                                <label class="form-check-label <%=UIConstants.primaryTextColorClass%>"
                                                                                                       style="font-weight:bold;margin-left:30px">
                                                                                                    <input class="form-check-input"
                                                                                                           type="radio"
                                                                                                           id="status1"
                                                                                                           name="status"
                                                                                                           value="1">Inactive
                                                                                                    <span class="circle"
                                                                                                          style="margin-top:2px"><span
                                                                                                            class="check"></span></span>
                                                                                                </label>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                    <br>

                                                                                    <div class="row eventModalRow">
                                                                                        <div class="col-md-4">
                                                                                            <label class="col-form-label <%=UIConstants.primaryTextColorClass%>"
                                                                                                   style="font-weight:bold;margin-left:-1px">Revenue
                                                                                                Model*:</label>
                                                                                        </div>
                                                                                        <div class="col-lg-5 col-md-6 col-sm-3 revenueModelEvent"
                                                                                             style="margin-left:-40px">
                                                                                            <div class="dropdown bootstrap-select show-tick show <%=UIConstants.primaryTextColorClass%>">
                                                                                                <select class="selectpicker <%=UIConstants.primaryTextColorClass%>"
                                                                                                        id="revenueModel"
                                                                                                        name="revenueModel"
                                                                                                        data-size="7"
                                                                                                        data-style="btn"
                                                                                                        title="Single Select"
                                                                                                        data-width="150px">
                                                                                                    <option value="RPA"
                                                                                                            selected>
                                                                                                        RPA
                                                                                                    </option>
                                                                                                    <option value="RPS">
                                                                                                        RPS
                                                                                                    </option>
                                                                                                    <option value="RPC">
                                                                                                        RPC
                                                                                                    </option>
                                                                                                    <option value="RPI">
                                                                                                        RPI
                                                                                                    </option>
                                                                                                    <option value="RPL">
                                                                                                        RPL
                                                                                                    </option>
                                                                                                    <option value="RPAS">
                                                                                                        RPA+RPS
                                                                                                    </option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="revenueModelEvent1">
                                                                                            <div class="input-group-prepend"
                                                                                                 style="margin-left:-20px">
                                                                                 <span class="input-group-text">
                                                                                  <i class="material-icons">monetization_on</i>
                                                                                  </span>
                                                                                            </div>
                                                                                            <div class="col-sm-4"
                                                                                                 style="margin-left:-10px">
                                                                                                <div class="form-group">
                                                                                                    <input type="text"
                                                                                                           oninvalid="this.setCustomValidity('Please Enter Revenue')"
                                                                                                           oninput="setCustomValidity('')"
                                                                                                           class="form-control"
                                                                                                           id="revenueModel1"
                                                                                                           style="color:<%=UIConstants.textColor%>;width:100px"
                                                                                                           name="revenueModel1"
                                                                                                           required>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                    <br>

                                                                                    <div class="card-collapse"
                                                                                         style="font-family:<%=UIConstants.primaryFontFamily%>;background-color: <%=UIConstants.primaryCardModalBackground%>"
                                                                                         id="advancedOption">
                                                                                        <div class="card-header <%=UIConstants.primaryTextColorClass%>"
                                                                                             role="tab"
                                                                                             id="headingTwo"
                                                                                             style="font-family:<%=UIConstants.primaryFontFamily%>;background-color:<%=UIConstants.primaryCardModalBackground%>;width:140px;margin-left:-1px;margin-top:-30px;;border-color:<%=UIConstants.primaryCardModalBackground%>">
                                                                                            <h4 class="<%=UIConstants.primaryTextColorClass%>">
                                                                                                <a class="collapsed <%=UIConstants.primaryTextColorClass%>"
                                                                                                   data-toggle="collapse"
                                                                                                   href="#collapseTwo"
                                                                                                   aria-expanded="false"
                                                                                                   aria-controls="collapseTwo"
                                                                                                   style="font-family:<%=UIConstants.primaryFontFamily%>;font-weight: bold">
                                                                                                    <i class="fa fa-sort-desc"
                                                                                                       aria-hidden="true"></i>Advanced
                                                                                                    Option
                                                                                                </a>
                                                                                            </h4>
                                                                                        </div>
                                                                                        <div id="collapseTwo"
                                                                                             class="collapse"
                                                                                             role="tabpanel"
                                                                                             aria-labelledby="headingTwo"
                                                                                             data-parent="#accordion">
                                                                                            <div class="card-body <%=UIConstants.primaryTextColorClass%>">

                                                                                                <div class="row"
                                                                                                     style="margin-top:0px">
                                                                                                    <label class="col-form-label eventTokenLabel <%=UIConstants.primaryTextColorClass%>"
                                                                                                           style="font-weight:bold;margin-left:35px">Event
                                                                                                        Token:</label>
                                                                                                </div>

                                                                                                <div class="row">
                                                                                                    <div class="col-lg-6 col-md-6 col-sm-3 protocol"
                                                                                                         style="margin-top:15px;margin-left:-100px;">
                                                                                                        <div class="form-group">
                                                                                                            <input type="text"
                                                                                                                   class="inputText eventTokenInput"
                                                                                                                   id="token"
                                                                                                                   style="position:absolute;color:<%=UIConstants.textColor%>;margin-left:250px;width:160px;height:30px;margin-top:-42px"
                                                                                                                   name="token"
                                                                                                                   onchange="limitText(this,255)"
                                                                                                                   onkeypress="limitText(this,255)">

                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="row eventVisibilityLabel">
                                                                                                    <label class="col-form-label <%=UIConstants.primaryTextColorClass%>"
                                                                                                           style="font-weight:bold;margin-left:40px;margin-top:-10px">Event
                                                                                                        Visibility:</label>
                                                                                                </div>

                                                                                                <div class="row">
                                                                                                    <div class="col-lg-4 col-md-6 col-sm-3 eventVisibility"
                                                                                                         style="margin-left:12px;margin-top:-10px">
                                                                                                        <label style="font-weight:bold">
                                                                                                            <div class="dropdown bootstrap-select show-tick show <%=UIConstants.primaryTextColorClass%>">
                                                                                                                <select id="accessStatus"
                                                                                                                        name="accessStatus"
                                                                                                                        class="selectpicker <%=UIConstants.primaryTextColorClass%>"
                                                                                                                        data-size="7"
                                                                                                                        data-style="btn"
                                                                                                                        title="Single Select">
                                                                                                                    <option value="NeedApproval">
                                                                                                                        NeedApproval
                                                                                                                    </option>
                                                                                                                    <option value="Public"
                                                                                                                            selected>
                                                                                                                        Public
                                                                                                                    </option>
                                                                                                                    <option value="Private">
                                                                                                                        Private
                                                                                                                    </option>
                                                                                                                    <option value="Approved">
                                                                                                                        Approved
                                                                                                                    </option>
                                                                                                                    <option value="Pending">
                                                                                                                        Pending
                                                                                                                    </option>
                                                                                                                </select>
                                                                                                            </div>
                                                                                                        </label>
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="row conversionsRow">
                                                                                                    <label class=" col-form-label <%=UIConstants.primaryTextColorClass%>"
                                                                                                           style="font-weight:bold;margin-left:320px;margin-top:-100px">Multiple
                                                                                                        Conversions<br>(With
                                                                                                        Same
                                                                                                        Click
                                                                                                        Id)</label>
                                                                                                </div>

                                                                                                <div class="row"
                                                                                                     style="position:absolute;margin-top:-80px;margin-left:270px">
                                                                                                    <div class="togglebutton col-lg-5 col-md-6 col-sm-3"
                                                                                                         style="margin-top:22px;margin-left:50px">
                                                                                                        <label class="<%=UIConstants.primaryTextColorClass%>"
                                                                                                               style="font-weight:bold">
                                                                                                            <input type="checkbox"
                                                                                                                   id="multiConv"
                                                                                                                   name="multiConv">
                                                                                                            <span class="toggle"></span>

                                                                                                        </label>
                                                                                                    </div>
                                                                                                </div>

                                                                                                <div class="row"
                                                                                                     style="margin-top:40px;margin-left:-27px;display: none"
                                                                                                     id="partnersInEvent">
                                                                                                    <label class="col-sm-3 col-form-label partnerLabel <%=UIConstants.primaryTextColorClass%>"
                                                                                                           style="font-weight:bold;margin-top:-54px">Partners:</label>
                                                                                                    <div class="col-lg-6"
                                                                                                         style="margin-left:-107px;margin-top:-18px">
                                                                                                        <select class="ajaxDropdownPartner"
                                                                                                                name="partnersInEvent"
                                                                                                                id="partnersInEvent1"
                                                                                                                multiple
                                                                                                                style="width:420px;height:auto">
                                                                                                        </select>
                                                                                                    </div>
                                                                                                </div>

                                                                                            </div>

                                                                                        </div>
                                                                                    </div>

                                                                                    <br>

                                                                                    <input type="hidden"
                                                                                           name="appId"
                                                                                           value="${app.id}">
                                                                                    <div class="row offerDetailsBottomLine"
                                                                                         style="border-top:1px solid #423b3a;width:490px;margin-left:-20px;margin-top: -10px">
                                                                                        <br>
                                                                                        <div class="col-sm-12">
                                                                                            <button class="btn btn-<%=UIConstants.primaryColorClass%> btn-round pull-right submitButton"
                                                                                                    type="submit"
                                                                                                    style="padding:8px 10px;;margin-top:10px;margin-right:30px">
                                                                                                <b style="margin-left:0px">Save
                                                                                                    Event</b>&nbsp;
                                                                                            </button>
                                                                                        </div>
                                                                                    </div>
                                                                                </form>&ndash;%&gt;

                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                            </div>

                                                            <i class="col-lg-6"
                                                               style="position:absolute;margin-top:-15px">Note:Please
                                                                fill * columns</i>
                                                        </div>


                                                        <form id="formSubmit">

                                                            <input type="hidden" name="daterange" id="dateHiddenField">

                                                            <input type="text" name="appsId"
                                                                   value=${app.id} id="appSelect"
                                                                   style="display:none">

                                                            <br>
                                                            <div class=""
                                                                 style="position: relative; height: 30px;">
                                                                <label class="<%=UIConstants.primaryTextColorClass%>"
                                                                       style="font-family: <%=UIConstants.primaryFontFamily%>; font-size: 30px; position: absolute; top: 50%; left: 50%; transform: translate(-50% , -50%); ">Select
                                                                    Columns</label>
                                                            </div>
                                                            <br>

                                                            &lt;%&ndash;<div class="&lt;%&ndash;col-md-12&ndash;%&gt;">

                                                                <hr style="height: 5px;border-width:0;color:gray;background-color:#303654; margin-top: 2px;">

                                                                <div class="navOfferDetails"
                                                                     style="margin-top:14px;background-color:<%=UIConstants.primaryCardBackground%>;height:50px;">
                                                                    <ul class="nav nav-pills nav-pills-<%=UIConstants.primaryColorClass%>  <%=UIConstants.primaryTextColorClass%> justify-content-center"
                                                                        role="tablist">

                                                                        <li class="nav-item">
                                                                            <a class="nav-link active <%=UIConstants.textColor%>"
                                                                               data-toggle="tab"
                                                                               href="#appSelects"
                                                                               role="tablist" id="nav-link"
                                                                               style="font-size: 15px;">
                                                                                App
                                                                            </a>
                                                                        </li>

                                                                        <li class="nav-item">
                                                                            <a class="nav-link <%=UIConstants.textColor%>"
                                                                               data-toggle="tab"
                                                                               href="#eventSelects"
                                                                               role="tablist" id="nav-link"
                                                                               style="font-size: 15px;">
                                                                                Event
                                                                            </a>
                                                                        </li>

                                                                        <li class="nav-item">
                                                                            <a class="nav-link <%=UIConstants.textColor%>"
                                                                               data-toggle="tab"
                                                                               href="#deviceSelects"
                                                                               role="tablist" id="nav-link"
                                                                               style="font-size: 15px;">
                                                                                Device
                                                                            </a>
                                                                        </li>

                                                                        <li class="nav-item">
                                                                            <a class="nav-link <%=UIConstants.textColor%>"
                                                                               data-toggle="tab"
                                                                               href="#deviceLocSelects"
                                                                               role="tablist" id="nav-link"
                                                                               style="font-size: 15px;">
                                                                                Device Location
                                                                            </a>
                                                                        </li>

                                                                        <li class="nav-item">
                                                                            <a class="nav-link <%=UIConstants.textColor%>"
                                                                               data-toggle="tab"
                                                                               href="#attributionSelects"
                                                                               role="tablist" id="nav-link"
                                                                               style="font-size: 15px;">
                                                                                Attribution
                                                                            </a>
                                                                        </li>

                                                                        <li class="nav-item">
                                                                            <a class="nav-link <%=UIConstants.textColor%>"
                                                                               data-toggle="tab"
                                                                               href="#fraudSelects"
                                                                               role="tablist" id="nav-link"
                                                                               style="font-size: 15px;">
                                                                                Fraud
                                                                            </a>
                                                                        </li>

                                                                    </ul>
                                                                </div>

                                                                <hr style="height: 5px;border-width:0;color:gray;background-color:#303654; margin-top: 2px;">

                                                                <div class="tab-content tab-space">

                                                                    <div class="tab-pane active" id="appSelects">
                                                                        <div class="row" style="width:100%;margin-left:0px"><br>
                                                                            <div class="card <%=UIConstants.primaryTextColorClass%>"
                                                                                 Style="font-family:<%=UIConstants.primaryFontFamily%>;background-color:<%=UIConstants.primaryCardBackground%>;margin-top:-25px;">
                                                                                <div class="card-header card-header-warning card-header-text">

                                                                                </div>
                                                                                <div class="card-body <%=UIConstants.primaryTextColorClass%> infoTabCard"
                                                                                     style="margin-left:0px;">
                                                                                    <br>
                                                                                    <h4>hello brather 1</h4>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="tab-pane" id="eventSelects">
                                                                        <div class="row" style="width:100%;margin-left:0px"><br>
                                                                            <div class="card <%=UIConstants.primaryTextColorClass%>"
                                                                                 Style="font-family:<%=UIConstants.primaryFontFamily%>;background-color:<%=UIConstants.primaryCardBackground%>;margin-top:-25px;">
                                                                                <div class="card-header card-header-warning card-header-text">

                                                                                </div>

                                                                                <div class="card-body <%=UIConstants.primaryTextColorClass%> infoTabCard"
                                                                                     style="background-color:<%=UIConstants.primaryCardBackground%>">
                                                                                    <br>
                                                                                    <h4>hello brather 2</h4>

                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>

                                                                    <div class="tab-pane" id="deviceSelects">
                                                                        <div class="row" style="width:100%;margin-left:0px"><br>
                                                                            <div class="card <%=UIConstants.primaryTextColorClass%>"
                                                                                 Style="font-family:<%=UIConstants.primaryFontFamily%>;background-color:<%=UIConstants.primaryCardBackground%>;margin-top:-25px;">
                                                                                <div class="card-header card-header-warning card-header-text">

                                                                                </div>

                                                                                <div class="card-body <%=UIConstants.primaryTextColorClass%> infoTabCard"
                                                                                     style="background-color:<%=UIConstants.primaryCardBackground%>">
                                                                                    <br>
                                                                                    <h4>hello brather 3</h4>

                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>

                                                                    <div class="tab-pane" id="deviceLocSelects">
                                                                        <div class="row" style="width:100%;margin-left:0px"><br>
                                                                            <div class="card <%=UIConstants.primaryTextColorClass%>"
                                                                                 Style="font-family:<%=UIConstants.primaryFontFamily%>;background-color:<%=UIConstants.primaryCardBackground%>;margin-top:-25px;">
                                                                                <div class="card-header card-header-warning card-header-text">

                                                                                </div>

                                                                                <div class="card-body <%=UIConstants.primaryTextColorClass%> infoTabCard"
                                                                                     style="background-color:<%=UIConstants.primaryCardBackground%>">
                                                                                    <br>
                                                                                    <h4>hello brather 4</h4>

                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>

                                                                    <div class="tab-pane" id="attributionSelects">
                                                                        <div class="row" style="width:100%;margin-left:0px"><br>
                                                                            <div class="card <%=UIConstants.primaryTextColorClass%>"
                                                                                 Style="font-family:<%=UIConstants.primaryFontFamily%>;background-color:<%=UIConstants.primaryCardBackground%>;margin-top:-25px;">
                                                                                <div class="card-header card-header-warning card-header-text">

                                                                                </div>

                                                                                <div class="card-body <%=UIConstants.primaryTextColorClass%> infoTabCard"
                                                                                     style="background-color:<%=UIConstants.primaryCardBackground%>">
                                                                                    <br>
                                                                                    <h4>hello brather 5</h4>

                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>

                                                                    <div class="tab-pane" id="fraudSelects">
                                                                        <div class="row" style="width:100%;margin-left:0px"><br>
                                                                            <div class="card <%=UIConstants.primaryTextColorClass%>"
                                                                                 Style="font-family:<%=UIConstants.primaryFontFamily%>;background-color:<%=UIConstants.primaryCardBackground%>;margin-top:-25px;">
                                                                                <div class="card-header card-header-warning card-header-text">

                                                                                </div>

                                                                                <div class="card-body <%=UIConstants.primaryTextColorClass%> infoTabCard"
                                                                                     style="background-color:<%=UIConstants.primaryCardBackground%>">
                                                                                    <br>
                                                                                    <h4>hello brather 6</h4>

                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>

                                                                </div>

                                                            </div>&ndash;%&gt;

                                                            <br>

                                                            &lt;%&ndash;<div class="row">
                                                                <div class="col-lg-6"
                                                                     style="margin-left:-103px;margin-top:0">
                                                                    <div class="col-sm-6"
                                                                         style="margin-top: 9px;margin-left: 85px">
                                                                        <label class="<%=UIConstants.primaryTextColorClass%>"
                                                                               style="font-family: <%=UIConstants.primaryFontFamily%>; font-weight: bold; margin-left: 0px; position: absolute">Select
                                                                            Columns</label> <i class="fa fa-table"
                                                                                               style="margin-left: 123px"></i>
                                                                    </div>
                                                                    <div class="col-lg-6 allReportDataColumns"
                                                                         style="margin-left:90%;margin-top:-33px">
                                                                        <select class="multipleSelect <%=UIConstants.primaryTextColorClass%>"
                                                                                data-style="btn btn-warning"
                                                                                multiple="" title="Select Column"
                                                                                data-size="7"
                                                                                data-width="380px"
                                                                                tabindex="-98" id="extSelect"
                                                                                name="columns">
                                                                            <!-- selected="selected" -->
                                                                            <option value="App Id">App Id
                                                                            </option>
                                                                            <option value="App Name">App Name
                                                                            </option>
                                                                            <option value="Partner Id" selected>Partner
                                                                                Id
                                                                            </option>
                                                                            <option value="Partner Name" selected>
                                                                                Partner Name
                                                                            </option>
                                                                            <option value="Click" selected>Click
                                                                            </option>
                                                                            <option value="Approved Conversion"
                                                                                    selected>Approved Conversion
                                                                            </option>
                                                                            <option value="Cancelled Conversion">
                                                                                Cancelled Conversion
                                                                            </option>
                                                                            <option value="Pending Conversion">
                                                                                Pending Conversion
                                                                            </option>
                                                                            <option value="Gross Conversion">
                                                                                Gross Conversion
                                                                            </option>
                                                                            <option value="Approved Revenue">
                                                                                Approved Revenue
                                                                            </option>
                                                                            <option value="Cancelled Revenue">
                                                                                Cancelled Revenue
                                                                            </option>
                                                                            <option value="Pending Revenue">
                                                                                Pending Revenue
                                                                            </option>
                                                                            <option value="Gross Revenue">
                                                                                Gross Revenue
                                                                            </option>
                                                                            <option value="Ratio" selected>
                                                                                Ratio
                                                                            </option>
                                                                            <option value="CTR" selected>
                                                                                CTR
                                                                            </option>
                                                                            <option value="Day Timestamp">
                                                                                Day Timestamp
                                                                            </option>
                                                                            <option value="Rejected Clicks">
                                                                                Rejected Clicks
                                                                            </option>


                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>&ndash;%&gt;

                                                            &lt;%&ndash;<div class="row">

                                                                <div class="col-sm-6" style="margin-top: 9px">
                                                                    <label class="<%=UIConstants.primaryTextColorClass%>"
                                                                           style="font-family: <%=UIConstants.primaryFontFamily%>; font-weight: bold; margin-left: 0px; position: absolute">Select
                                                                        Partner </label> <i class="material-icons"
                                                                                            style="margin-top: -5px; margin-left: 115px; position: absolute;">person</i>&nbsp;
                                                                </div>
                                                                <div class="col-sm-6 conversionSelectBox"
                                                                     style="margin-left:-105px;margin-top:-7px">
                                                                    <select class="partnerSelect"
                                                                            id="partnerSelect"
                                                                            name="partners"
                                                                            multiple
                                                                            data-size="7" style="width:300px"
                                                                            style="color:<%=UIConstants.textColor%>; "
                                                                    ></select>
                                                                </div>

                                                            </div>&ndash;%&gt;

                                                            <div class="row bottomLineAllReport">
                                                                <div class="col-sm-12">
                                                                    <button type="button" id="submitForm"
                                                                            class="btn btn-<%=UIConstants.primaryColorClass%> btn-round pull-right submitButton"
                                                                            data-dismiss="modal"
                                                                            style="margin-top:10px;margin-right:20px">
                                                                        Apply
                                                                    </button>
                                                                </div>
                                                            </div>

                                                            <div class="clearfix"></div>

                                                        </form>

                                                        <!-- Image loader -->
                                                    </div>&ndash;%&gt;
                                                    <div id="modalForm">

                                                        <form id="formSubmit">

                                                            <input type="hidden" name="daterange" id="dateHiddenField">

                                                            <input type="text" name="appsId"
                                                                   value=${app.id} id="appSelect"
                                                                   style="display:none">

                                                            <div class=""
                                                                 style="position: relative; height: 30px;">
                                                                <label class="<%=UIConstants.primaryTextColorClass%>"
                                                                       style="font-family: <%=UIConstants.primaryFontFamily%>; font-size: 30px; position: absolute; top: 50%; left: 50%; transform: translate(-50% , -50%); ">Select
                                                                    Columns</label>
                                                            </div>

                                                            <br>

                                                            <hr style="height: 5px;border-width:0;color:gray;background-color:#303654; margin-top: 2px;">

                                                            <div class="navOfferDetails"
                                                                 style="margin-top:14px;background-color:<%=UIConstants.primaryCardBackground%>;height:50px;">
                                                                <ul class="nav nav-pills nav-pills-<%=UIConstants.primaryColorClass%>  <%=UIConstants.primaryTextColorClass%> justify-content-center"
                                                                    role="tablist">

                                                                    <li class="nav-item">
                                                                        <a class="nav-link active <%=UIConstants.textColor%>"
                                                                           data-toggle="tab"
                                                                           href="#selectAppss"
                                                                           role="tablist" id="nav-link"
                                                                           style="font-size: 15px;">
                                                                            App
                                                                        </a>
                                                                    </li>

                                                                    <li class="nav-item">
                                                                        <a class="nav-link <%=UIConstants.textColor%>"
                                                                           data-toggle="tab"
                                                                           href="#selectEvents"
                                                                           role="tablist" id="nav-link"
                                                                           style="font-size: 15px;">
                                                                            Event
                                                                        </a>
                                                                    </li>

                                                                    <li class="nav-item">
                                                                        <a class="nav-link <%=UIConstants.textColor%>"
                                                                           data-toggle="tab"
                                                                           href="#selectDevices"
                                                                           role="tablist" id="nav-link"
                                                                           style="font-size: 15px;">
                                                                            Device
                                                                        </a>
                                                                    </li>

                                                                    <li class="nav-item">
                                                                        <a class="nav-link <%=UIConstants.textColor%>"
                                                                           data-toggle="tab"
                                                                           href="#selectDeviceLoc"
                                                                           role="tablist" id="nav-link"
                                                                           style="font-size: 15px;">
                                                                            Device Location
                                                                        </a>
                                                                    </li>

                                                                    <li class="nav-item">
                                                                        <a class="nav-link <%=UIConstants.textColor%>"
                                                                           data-toggle="tab"
                                                                           href="#selectAttributions"
                                                                           role="tablist" id="nav-link"
                                                                           style="font-size: 15px;">
                                                                            Attribution
                                                                        </a>
                                                                    </li>

                                                                    <li class="nav-item">
                                                                        <a class="nav-link <%=UIConstants.textColor%>"
                                                                           data-toggle="tab"
                                                                           href="#selectFraud"
                                                                           role="tablist" id="nav-link"
                                                                           style="font-size: 15px;">
                                                                            Fraud
                                                                        </a>
                                                                    </li>

                                                                    <li class="nav-item">
                                                                        <a class="nav-link <%=UIConstants.textColor%>"
                                                                           data-toggle="tab"
                                                                           href="#selectConversion"
                                                                           role="tablist" id="nav-link"
                                                                           style="font-size: 15px;">
                                                                            Conversion
                                                                        </a>
                                                                    </li>


                                                                </ul>
                                                            </div>

                                                            <br>
                                                            <br>

                                                            <hr style="height: 5px;border-width:0;color:gray;background-color:#303654; margin-top: 13px;">


                                                            <div class="tab-content tab-space">

                                                                <div class="tab-pane active" id="selectAppss">
                                                                    <div class="row" style="width:100%;margin-left:0px">
                                                                        <br>
                                                                        <div class="card <%=UIConstants.primaryTextColorClass%>"
                                                                             Style="font-family:<%=UIConstants.primaryFontFamily%>;background-color:<%=UIConstants.primaryCardBackground%>;margin-top:-25px;">
                                                                            <div class="card-header card-header-warning card-header-text">

                                                                            </div>
                                                                            <div class="card-bodys <%=UIConstants.primaryTextColorClass%> infoTabCard"
                                                                                 style="margin-left:0px;">
                                                                                <br>

                                                                                <div class="containers">
                                                                                    <ul class="ks-cboxtags">
                                                                                        <li><input type="checkbox"
                                                                                                   id="appVersion"
                                                                                                   value="App Version"><label
                                                                                                for="appVersion">App
                                                                                            Version</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="sdkVersion"
                                                                                                   value="SDK Version"
                                                                                        ><label
                                                                                                for="sdkVersion">SDK
                                                                                            Version</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="appId"
                                                                                                   value="App Id"><label
                                                                                                for="appId">App
                                                                                            ID</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="appName"
                                                                                                   value="App Name"
                                                                                                   checked><label
                                                                                                for="appName">App
                                                                                            Name</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="appType"
                                                                                                   value="App Type"
                                                                                        ><label
                                                                                                for="appType">App
                                                                                            Type</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="bundleId"
                                                                                                   value="Bundle Id"><label
                                                                                                for="bundleId">Bundle
                                                                                            Id</label>
                                                                                        </li>
                                                                                    </ul>

                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="tab-pane" id="selectEvents">
                                                                    <div class="row" style="width:100%;margin-left:0px">
                                                                        <br>
                                                                        <div class="card <%=UIConstants.primaryTextColorClass%>"
                                                                             Style="font-family:<%=UIConstants.primaryFontFamily%>;background-color:<%=UIConstants.primaryCardBackground%>;margin-top:-25px;">
                                                                            <div class="card-header card-header-warning card-header-text">

                                                                            </div>

                                                                            <div class="card-body <%=UIConstants.primaryTextColorClass%> infoTabCard"
                                                                                 style="background-color:<%=UIConstants.primaryCardBackground%>">
                                                                                <br>

                                                                                <div class="containers">
                                                                                    <ul class="ks-cboxtags">
                                                                                        <li><input type="checkbox"
                                                                                                   id="eventTime"
                                                                                                   value="Event Time"><label
                                                                                                for="eventTime">Event
                                                                                            Time</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="eventName"
                                                                                                   value="Event Name"
                                                                                        ><label
                                                                                                for="eventName">Event
                                                                                            Name</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="eventValue"
                                                                                                   value="Event Value"><label
                                                                                                for="eventValue">Event
                                                                                            Value</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="eventRevenue"
                                                                                                   value="Event Revenue"
                                                                                                   checked><label
                                                                                                for="eventRevenue">Event
                                                                                            Revenue</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="eventRevenueCurrency"
                                                                                                   value="Event Revenue Currency"
                                                                                        ><label
                                                                                                for="eventRevenueCurrency">Event
                                                                                            Revenue Currency</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="eventRevenueUsd"
                                                                                                   value="Event Revenue USD"><label
                                                                                                for="eventRevenueUsd">Event
                                                                                            Revenue USD</label>
                                                                                        </li>
                                                                                    </ul>

                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                                <div class="tab-pane" id="selectDevices">
                                                                    <div class="row" style="width:100%;margin-left:0px">
                                                                        <br>
                                                                        <div class="card <%=UIConstants.primaryTextColorClass%>"
                                                                             Style="font-family:<%=UIConstants.primaryFontFamily%>;background-color:<%=UIConstants.primaryCardBackground%>;margin-top:-25px;">
                                                                            <div class="card-header card-header-warning card-header-text">

                                                                            </div>

                                                                            <div class="card-body <%=UIConstants.primaryTextColorClass%> infoTabCard"
                                                                                 style="background-color:<%=UIConstants.primaryCardBackground%>">
                                                                                <br>

                                                                                <div class="containers">
                                                                                    <ul class="ks-cboxtags">
                                                                                        <li><input type="checkbox"
                                                                                                   id="att"
                                                                                                   value="ATT"><label
                                                                                                for="att">ATT</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="wifi"
                                                                                                   value="WIFI"
                                                                                        ><label
                                                                                                for="wifi">WIFI</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="operator"
                                                                                                   value="Operator"><label
                                                                                                for="operator">Operator</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="carrier"
                                                                                                   value="Carrier"
                                                                                                   checked><label
                                                                                                for="carrier">Carrier</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="language"
                                                                                                   value="Language"
                                                                                        ><label
                                                                                                for="language">Language</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="androidId"
                                                                                                   value="Android Id"><label
                                                                                                for="androidId">Android
                                                                                            Id</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="imei"
                                                                                                   value="IMEI"
                                                                                        ><label
                                                                                                for="imei">IMEI</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="idfa"
                                                                                                   value="IDFA"><label
                                                                                                for="idfa">IDFA</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="idfv"
                                                                                                   value="IDFV"
                                                                                        ><label
                                                                                                for="idfv">IDFV</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="advertisingId"
                                                                                                   value="Advertising Id"><label
                                                                                                for="advertisingId">Advertising
                                                                                            Id</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="deviceModel"
                                                                                                   value="Device Model"
                                                                                        ><label
                                                                                                for="deviceModel">Device
                                                                                            Model</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="deviceType"
                                                                                                   value="Device Type"><label
                                                                                                for="deviceType">Device
                                                                                            Type</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="deviceCategory"
                                                                                                   value="Device Category"
                                                                                        ><label
                                                                                                for="deviceCategory">Device
                                                                                            Category</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="platform"
                                                                                                   value="Platform"><label
                                                                                                for="platform">Platform</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="osVersion"
                                                                                                   value="OS Version"
                                                                                        ><label
                                                                                                for="osVersion">OS
                                                                                            Version</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="userAgent"
                                                                                                   value="User Agent"><label
                                                                                                for="userAgent">User
                                                                                            Agent</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="deviceDownloadTime"
                                                                                                   value="Device Download Time"
                                                                                        ><label
                                                                                                for="deviceDownloadTime">Device
                                                                                            Download Time</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="oaid"
                                                                                                   value="OAID"><label
                                                                                                for="oaid">OAID</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="isLat"
                                                                                                   value="is LAT"><label
                                                                                                for="isLat">is
                                                                                            LAT</label>
                                                                                        </li>
                                                                                    </ul>

                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                                <div class="tab-pane" id="selectDeviceLoc">
                                                                    <div class="row" style="width:100%;margin-left:0px">
                                                                        <br>
                                                                        <div class="card <%=UIConstants.primaryTextColorClass%>"
                                                                             Style="font-family:<%=UIConstants.primaryFontFamily%>;background-color:<%=UIConstants.primaryCardBackground%>;margin-top:-25px;">
                                                                            <div class="card-header card-header-warning card-header-text">

                                                                            </div>

                                                                            <div class="card-body <%=UIConstants.primaryTextColorClass%> infoTabCard"
                                                                                 style="background-color:<%=UIConstants.primaryCardBackground%>">
                                                                                <br>

                                                                                <div class="containers">
                                                                                    <ul class="ks-cboxtags">
                                                                                        <li><input type="checkbox"
                                                                                                   id="region"
                                                                                                   value="Region"><label
                                                                                                for="region">Region</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="countryCode"
                                                                                                   value="Country Code"
                                                                                        ><label
                                                                                                for="countryCode">Country
                                                                                            Code</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="state"
                                                                                                   value="State"><label
                                                                                                for="state">State</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="city"
                                                                                                   value="City" checked><label
                                                                                                for="city">City</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="postalCode"
                                                                                                   value="Postal Code"
                                                                                        ><label
                                                                                                for="postalCode">Postal
                                                                                            Code</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="ip"
                                                                                                   value="IP"><label
                                                                                                for="ip">IP</label>
                                                                                        </li>
                                                                                    </ul>

                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                                <div class="tab-pane" id="selectAttributions">
                                                                    <div class="row" style="width:100%;margin-left:0px">
                                                                        <br>
                                                                        <div class="card <%=UIConstants.primaryTextColorClass%>"
                                                                             Style="font-family:<%=UIConstants.primaryFontFamily%>;background-color:<%=UIConstants.primaryCardBackground%>;margin-top:-25px;">
                                                                            <div class="card-header card-header-warning card-header-text">

                                                                            </div>

                                                                            <div class="card-body <%=UIConstants.primaryTextColorClass%> infoTabCard"
                                                                                 style="background-color:<%=UIConstants.primaryCardBackground%>">
                                                                                <br>

                                                                                <div class="containers">
                                                                                    <ul class="ks-cboxtags">
                                                                                        <li><input type="checkbox"
                                                                                                   id="attributedTouchType"
                                                                                                   value="Attributed Touch Type"><label
                                                                                                for="attributedTouchType">Attributed
                                                                                            Touch Type</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="attributedTouchTime"
                                                                                                   value="Attributed Touch Time"
                                                                                        ><label
                                                                                                for="attributedTouchTime">Attributed
                                                                                            Touch Time</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="installTime"
                                                                                                   value="Install Time"><label
                                                                                                for="installTime">Install
                                                                                            Time</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="costModel"
                                                                                                   value="Cost Model"
                                                                                        ><label
                                                                                                for="costModel">Cost
                                                                                            Model</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="costValue"
                                                                                                   value="Cost Value"
                                                                                        ><label
                                                                                                for="costValue">Cost
                                                                                            Value</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="subParam1"
                                                                                                   value="Sub Param 1"><label
                                                                                                for="subParam1">Sub
                                                                                            Param 1</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="subParam2"
                                                                                                   value="Sub Param 2"><label
                                                                                                for="subParam2">Sub
                                                                                            Param 2</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="subParam3"
                                                                                                   value="Sub Param 3"><label
                                                                                                for="subParam3">Sub
                                                                                            Param 3</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="subParam4"
                                                                                                   value="Sub Param 4"><label
                                                                                                for="subParam4">Sub
                                                                                            Param 4</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="customerUserId"
                                                                                                   value="Customer User Id"
                                                                                        ><label
                                                                                                for="customerUserId">Customer
                                                                                            User Id</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="isRetargeting"
                                                                                                   value="is Retargeting"><label
                                                                                                for="isRetargeting">is
                                                                                            Retargeting</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="storeProductPage"
                                                                                                   value="Store Product Page"
                                                                                        ><label
                                                                                                for="storeProductPage">Store
                                                                                            Product Page</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="costCurrency"
                                                                                                   value="Cost Currency"><label
                                                                                                for="costCurrency">Cost
                                                                                            Currency</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="eventSource"
                                                                                                   value="Event Source"
                                                                                        ><label
                                                                                                for="eventSource">Event
                                                                                            Source</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="partner"
                                                                                                   value="Partner"
                                                                                                   checked><label
                                                                                                for="partner">Partner</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="mediaSource"
                                                                                                   value="Media Source"
                                                                                        ><label
                                                                                                for="mediaSource">Media
                                                                                            Source</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="channel"
                                                                                                   value="Channel"><label
                                                                                                for="channel">Channel</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="keywords"
                                                                                                   value="Keywords"
                                                                                        ><label
                                                                                                for="keywords">Keywords</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="retargetingConversionType"
                                                                                                   value="Retargeting Conversion Type"><label
                                                                                                for="retargetingConversionType">Retargeting
                                                                                            Conversion Type</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="isPrimaryAttribution"
                                                                                                   value="Is Primary Attribution"
                                                                                        ><label
                                                                                                for="isPrimaryAttribution">Is
                                                                                            Primary Attribution</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="attributionLookback"
                                                                                                   value="Attribution Lookback"><label
                                                                                                for="attributionLookback">Attribution
                                                                                            Lookback</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="reengagementWindow"
                                                                                                   value="Reengagement Window"><label
                                                                                                for="reengagementWindow">Reengagement
                                                                                            Window</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="matchType"
                                                                                                   value="Match Type"><label
                                                                                                for="matchType">Match
                                                                                            Type</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="httpReferrer"
                                                                                                   value="HTTP Referrer"
                                                                                        ><label
                                                                                                for="httpReferrer">HTTP
                                                                                            Referrer</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="campaign"
                                                                                                   value="Campaign"><label
                                                                                                for="campaign">Campaign</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="campaignId"
                                                                                                   value="Campaign Id"
                                                                                        ><label
                                                                                                for="campaignId">Campaign
                                                                                            Id</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="adset"
                                                                                                   value="Adset"
                                                                                        ><label
                                                                                                for="adset">Adset</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="adsetId"
                                                                                                   value="Adset Id"><label
                                                                                                for="adsetId">Adset
                                                                                            Id</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="ad"
                                                                                                   value="Ad"><label
                                                                                                for="ad">Ad</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="originalUrl"
                                                                                                   value="Original Url"><label
                                                                                                for="originalUrl">Original
                                                                                            Url</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="adId"
                                                                                                   value="Ad Id"><label
                                                                                                for="adId">Ad Id</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="adType"
                                                                                                   value="Ad Type"
                                                                                        ><label
                                                                                                for="adType">Ad
                                                                                            Type</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="siteId"
                                                                                                   value="Site Id"><label
                                                                                                for="siteId">Site
                                                                                            Id</label>
                                                                                        </li>

                                                                                    </ul>

                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                                <div class="tab-pane" id="selectFraud">
                                                                    <div class="row" style="width:100%;margin-left:0px">
                                                                        <br>
                                                                        <div class="card <%=UIConstants.primaryTextColorClass%>"
                                                                             Style="font-family:<%=UIConstants.primaryFontFamily%>;background-color:<%=UIConstants.primaryCardBackground%>;margin-top:-25px;">
                                                                            <div class="card-header card-header-warning card-header-text">

                                                                            </div>

                                                                            <div class="card-body <%=UIConstants.primaryTextColorClass%> infoTabCard"
                                                                                 style="background-color:<%=UIConstants.primaryCardBackground%>">
                                                                                <br>

                                                                                <div class="containers">
                                                                                    <ul class="ks-cboxtags">
                                                                                        <li><input type="checkbox"
                                                                                                   id="rejectedReason"
                                                                                                   value="Rejected Reason"><label
                                                                                                for="rejectedReason">Rejected
                                                                                            Reason</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="rejectedReasonValue"
                                                                                                   value="Rejected Reason Value"
                                                                                        ><label
                                                                                                for="rejectedReasonValue">Rejected
                                                                                            Reason Value</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="blockedReason"
                                                                                                   value="Blocked Reason"><label
                                                                                                for="blockedReason">Blocked
                                                                                            Reason</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="blockedReasonRule"
                                                                                                   value="Blocked Reason Rule"
                                                                                                   checked><label
                                                                                                for="blockedReasonRule">Blocked
                                                                                            Reason Rule</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="blockedReasonValue"
                                                                                                   value="Blocked Reason Value"
                                                                                        ><label
                                                                                                for="blockedReasonValue">Blocked
                                                                                            Reason Value</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="blockedSubReason"
                                                                                                   value="Blocked Sub Reason"><label
                                                                                                for="blockedSubReason">Blocked
                                                                                            Sub Reason</label>
                                                                                        </li>
                                                                                    </ul>

                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                                <div class="tab-pane" id="selectConversion">
                                                                    <div class="row" style="width:100%;margin-left:0px">
                                                                        <br>
                                                                        <div class="card <%=UIConstants.primaryTextColorClass%>"
                                                                             Style="font-family:<%=UIConstants.primaryFontFamily%>;background-color:<%=UIConstants.primaryCardBackground%>;margin-top:-25px;">
                                                                            <div class="card-header card-header-warning card-header-text">

                                                                            </div>

                                                                            <div class="card-body <%=UIConstants.primaryTextColorClass%> infoTabCard"
                                                                                 style="background-color:<%=UIConstants.primaryCardBackground%>">
                                                                                <br>

                                                                                <div class="containers">
                                                                                    <ul class="ks-cboxtags">
                                                                                        <li><input type="checkbox"
                                                                                                   id="approvedConversions"
                                                                                                   value="Approved Conversions"><label
                                                                                                for="approvedConversions">Approved
                                                                                            Conversions</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="cancelledConversions"
                                                                                                   value="Cancelled Conversions"><label
                                                                                                for="cancelledConversions">Cancelled
                                                                                            Conversions</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="pendingConversions"
                                                                                                   value="Pending Conversions"><label
                                                                                                for="pendingConversions">Pending
                                                                                            Conversions</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="rejectedClicks"
                                                                                                   value="Rejected Clicks"><label
                                                                                                for="rejectedClicks">Rejected
                                                                                            Clicks</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="approvedRevenue"
                                                                                                   value="Approved Revenue"><label
                                                                                                for="approvedRevenue">Approved
                                                                                            Revenue</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="cancelledRevenue"
                                                                                                   value="Cancelled Revenue"><label
                                                                                                for="cancelledRevenue">Cancelled
                                                                                            Revenue</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="pendingRevenue"
                                                                                                   value="Pending Revenue"><label
                                                                                                for="pendingRevenue">Pending
                                                                                            Revenue</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="approvedPayout"
                                                                                                   value="Approved Payout"><label
                                                                                                for="approvedPayout">Approved
                                                                                            Payout</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="clickCount"
                                                                                                   value="Click Count"><label
                                                                                                for="clickCount">Click
                                                                                            Count</label></li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="ratio"
                                                                                                   value="Ratio"><label
                                                                                                for="ratio">Ratio</label>
                                                                                        </li>
                                                                                        <li><input type="checkbox"
                                                                                                   id="ctr"
                                                                                                   value="CTR"><label
                                                                                                for="ctr">CTR</label>
                                                                                        </li>
                                                                                    </ul>

                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                            </div>


                                                            &lt;%&ndash;<div class="row">

                                                                <div class="col-sm-6" style="margin-top: 9px">
                                                                    <label class="<%=UIConstants.primaryTextColorClass%>"
                                                                           style="font-family: <%=UIConstants.primaryFontFamily%>; font-weight: bold; margin-left: 0px; position: absolute">Select
                                                                        Partner </label> <i class="material-icons"
                                                                                            style="margin-top: -5px; margin-left: 115px; position: absolute;">person</i>&nbsp;
                                                                </div>
                                                                <div class="col-sm-6 conversionSelectBox"
                                                                     style="margin-left:-105px;margin-top:-7px">
                                                                    <select class="partnerSelect"
                                                                            id="partnerSelect"
                                                                            name="partners"
                                                                            multiple
                                                                            data-size="7" style="width:300px"
                                                                            style="color:<%=UIConstants.textColor%>; "
                                                                    ></select>
                                                                </div>

                                                            </div>

                                                            <br>

                                                            <div class="row">
                                                                <div class="col-lg-6"
                                                                     style="margin-left:-103px;margin-top:0">
                                                                    <div class="col-sm-6"
                                                                         style="margin-top: 9px;margin-left: 85px">
                                                                        <label class="<%=UIConstants.primaryTextColorClass%>"
                                                                               style="font-family: <%=UIConstants.primaryFontFamily%>; font-weight: bold; margin-left: 0px; position: absolute">Select
                                                                            Columns</label> <i class="fa fa-table"
                                                                                               style="margin-left: 123px"></i>
                                                                    </div>
                                                                    <div class="col-lg-6 allReportDataColumns"
                                                                         style="margin-left:90%;margin-top:-33px">
                                                                        <select class="multipleSelect <%=UIConstants.primaryTextColorClass%>"
                                                                                data-style="btn btn-warning"
                                                                                multiple="" title="Select Column"
                                                                                data-size="7"
                                                                                data-width="380px"
                                                                                tabindex="-98" id="extSelect"
                                                                                name="columns">
                                                                            <!-- selected="selected" -->
                                                                            <option value="App Id">App Id
                                                                            </option>
                                                                            <option value="App Name">App Name
                                                                            </option>
                                                                            <option value="Partner Id" selected>Partner
                                                                                Id
                                                                            </option>
                                                                            <option value="Partner Name" selected>
                                                                                Partner Name
                                                                            </option>
                                                                            <option value="Click" selected>Click
                                                                            </option>
                                                                            <option value="Approved Conversion"
                                                                                    selected>Approved Conversion
                                                                            </option>
                                                                            <option value="Cancelled Conversion">
                                                                                Cancelled Conversion
                                                                            </option>
                                                                            <option value="Pending Conversion">
                                                                                Pending Conversion
                                                                            </option>
                                                                            <option value="Gross Conversion">
                                                                                Gross Conversion
                                                                            </option>
                                                                            <option value="Approved Revenue">
                                                                                Approved Revenue
                                                                            </option>
                                                                            <option value="Cancelled Revenue">
                                                                                Cancelled Revenue
                                                                            </option>
                                                                            <option value="Pending Revenue">
                                                                                Pending Revenue
                                                                            </option>
                                                                            <option value="Gross Revenue">
                                                                                Gross Revenue
                                                                            </option>
                                                                            <option value="Ratio" selected>
                                                                                Ratio
                                                                            </option>
                                                                            <option value="CTR" selected>
                                                                                CTR
                                                                            </option>
                                                                            <option value="Day Timestamp">
                                                                                Day Timestamp
                                                                            </option>
                                                                            <option value="Rejected Clicks">
                                                                                Rejected Clicks
                                                                            </option>


                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>&ndash;%&gt;

                                                            <div class="row bottomLineAllReport">
                                                                <div class="col-sm-12">
                                                                    <button type="button" id="submitForm"
                                                                            class="btn btn-<%=UIConstants.primaryColorClass%> btn-round pull-right submitButton"
                                                                            data-dismiss="modal"
                                                                            style="margin-top:10px;margin-right:20px">
                                                                        Apply
                                                                    </button>
                                                                </div>
                                                            </div>

                                                            <div class="clearfix"></div>
                                                        </form>

                                                        <!-- Image loader -->
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <!-- </form> -->
                                </div>

                            </div>


                            <div class="card-body" id="tableBody" style="display: none">
                            </div>

                            <div class="table-responsive" style="width:98%;margin-left:10px;margin-top:-20px">

                                <table id="datatables"
                                       class="display nowrap <%=UIConstants.primaryTextColorClass%>"
                                       style="font-family: <%=UIConstants.primaryFontFamily%>;width:100%"
                                       cellspacing="0">
                                    <thead>
                                    <tr class="<%=UIConstants.primaryTextColorClass%>" style="font-weight: bold; ">
                                        <th style="font-weight:bold">App Version</th>
                                        <th style="font-weight:bold">SDK Version</th>
                                        <th style="font-weight:bold">App Id</th>
                                        <th style="font-weight:bold">App Name</th>
                                        <th style="font-weight:bold">App Type</th>
                                        <th style="font-weight:bold">Bundle Id</th>

                                        <th style="font-weight:bold">Event Time</th>
                                        <th style="font-weight:bold">Event Name</th>
                                        <th style="font-weight:bold">Event Value</th>
                                        <th style="font-weight:bold">Event Revenue</th>
                                        <th style="font-weight:bold">Event Revenue Currency</th>
                                        <th style="font-weight:bold">Event Revenue USD</th>

                                        <th style="font-weight:bold">ATT</th>
                                        <th style="font-weight:bold">WIFI</th>
                                        <th style="font-weight:bold">Operator</th>
                                        <th style="font-weight:bold">Carrier</th>
                                        <th style="font-weight:bold">Language</th>
                                        <th style="font-weight:bold">Android Id</th>
                                        <th style="font-weight:bold">IMEI</th>
                                        <th style="font-weight:bold">IDFA</th>
                                        <th style="font-weight:bold">IDFV</th>
                                        <th style="font-weight:bold">Advertising Id</th>
                                        <th style="font-weight:bold">Device Model</th>
                                        <th style="font-weight:bold">Device Type</th>
                                        <th style="font-weight:bold">Device Category</th>
                                        <th style="font-weight:bold">Platform</th>
                                        <th style="font-weight:bold">OS Version</th>
                                        <th style="font-weight:bold">User Agent</th>
                                        <th style="font-weight:bold">Device Download Time</th>
                                        <th style="font-weight:bold">OAID</th>
                                        <th style="font-weight:bold">is LAT</th>

                                        <th style="font-weight:bold">Region</th>
                                        <th style="font-weight:bold">Country Code</th>
                                        <th style="font-weight:bold">State</th>
                                        <th style="font-weight:bold">City</th>
                                        <th style="font-weight:bold">Postal Code</th>
                                        <th style="font-weight:bold">IP</th>

                                        <th style="font-weight:bold">Attributed Touch Type</th>
                                        <th style="font-weight:bold">Attributed Touch Time</th>
                                        <th style="font-weight:bold">Install Time</th>
                                        <th style="font-weight:bold">Cost Model</th>
                                        <th style="font-weight:bold">Cost Value</th>
                                        <th style="font-weight:bold">Sub Param 1</th>
                                        <th style="font-weight:bold">Sub Param 2</th>
                                        <th style="font-weight:bold">Sub Param 3</th>
                                        <th style="font-weight:bold">Sub Param 4</th>
                                        <th style="font-weight:bold">Customer User Id</th>
                                        <th style="font-weight:bold">is Retargeting</th>
                                        <th style="font-weight:bold">Store Product Page</th>
                                        <th style="font-weight:bold">Cost Currency</th>
                                        <th style="font-weight:bold">Event Source</th>
                                        <th style="font-weight:bold">Partner</th>
                                        <th style="font-weight:bold">Media Source</th>
                                        <th style="font-weight:bold">Channel</th>
                                        <th style="font-weight:bold">Keywords</th>
                                        <th style="font-weight:bold">Retargeting Conversion Type</th>
                                        <th style="font-weight:bold">Is Primary Attribution</th>
                                        <th style="font-weight:bold">Attribution Lookback</th>
                                        <th style="font-weight:bold">Reengagement Window</th>
                                        <th style="font-weight:bold">Match Type</th>
                                        <th style="font-weight:bold">HTTP Referrer</th>
                                        <th style="font-weight:bold">Campaign</th>
                                        <th style="font-weight:bold">Campaign Id</th>
                                        <th style="font-weight:bold">Adset</th>
                                        <th style="font-weight:bold">Adset Id</th>
                                        <th style="font-weight:bold">Ad</th>
                                        <th style="font-weight:bold">Original Url</th>
                                        <th style="font-weight:bold">Ad Id</th>
                                        <th style="font-weight:bold">Ad Type</th>
                                        <th style="font-weight:bold">Site Id</th>

                                        <th style="font-weight:bold">Rejected Reason</th>
                                        <th style="font-weight:bold">Rejected Reason Value</th>
                                        <th style="font-weight:bold">Blocked Reason</th>
                                        <th style="font-weight:bold">Blocked Reason Rule</th>
                                        <th style="font-weight:bold">Blocked Reason Value</th>
                                        <th style="font-weight:bold">Blocked Sub Reason</th>

                                        <th style="font-weight:bold">Approved Conversions</th>
                                        <th style="font-weight:bold">Cancelled Conversions</th>
                                        <th style="font-weight:bold">Pending Conversions</th>
                                        <th style="font-weight:bold">Rejected Clicks</th>
                                        <th style="font-weight:bold">Approved Revenue</th>
                                        <th style="font-weight:bold">Cancelled Revenue</th>
                                        <th style="font-weight:bold">Pending Revenue</th>
                                        <th style="font-weight:bold">Approved Payout</th>
                                        <th style="font-weight:bold">Click Count</th>
                                        <th style="font-weight:bold">Ratio</th>
                                        <th style="font-weight:bold">CTR</th>

                                    </tr>
                                    &lt;%&ndash;<tr class="<%=UIConstants.primaryTextColorClass%>" style="font-weight: bold; ">
                                        <th style="font-weight:bold">App ID</th>
                                        <th style="font-weight:bold">App Name</th>
                                        <th style="font-weight:bold">Partner Id</th>
                                        <th style="font-weight:bold">Partner Name</th>
                                        <th style="font-weight:bold">Click Count</th>
                                        <th style="font-weight:bold">Approved Conversion</th>
                                        <th style="font-weight:bold">Cancelled Conversion</th>
                                        <th style="font-weight:bold">Pending Conversion</th>
                                        <th style="font-weight:bold">Approved Revenue</th>
                                        <th style="font-weight:bold">Cancelled Revenue</th>
                                        <th style="font-weight:bold">Pending Revenue</th>
                                        <th style="font-weight:bold">Ratio</th>
                                        <th style="font-weight:bold">CTR</th>
                                        <th style="font-weight:bold">Day Timestamp</th>
                                        <th style="font-weight:bold">Rejected Clicks</th>

                                    </tr>&ndash;%&gt;
                                    </thead>

                                </table>
                            </div>


                            &lt;%&ndash;<div class="table-responsive allReportTable" style="width:98%;margin-left:10px">
                                <table id="datatables"
                                       class="dataTable display nowrap <%=UIConstants.primaryTextColorClass%>"
                                       style="font-family: <%=UIConstants.primaryFontFamily%>;width:100%"
                                       cellspacing="0">
                                    <thead>
                                    <tr class="<%=UIConstants.primaryTextColorClass%>" style="font-weight: bold; ">

                                        <c:if test="${fn:length(data)==0 || fn:contains(data, 'App')}">
                                            <th style="font-weight:bold;" <%=(stringList != null && !stringList.contains("App")) ? "class= hide" : ""%>>
                                                App ID
                                            </th>
                                            <th style="font-weight:bold;" <%=(stringList != null && !stringList.contains("App")) ? "class= hide" : ""%>>
                                                App Name
                                            </th>
                                        </c:if>

                                        &lt;%&ndash;<c:if test="${fn:contains(data, 'Advertiser')}">
                                            <th style="font-weight:bold;" <%=(stringList != null && !stringList.contains("Advertiser")) ? "class= hide" : ""%>>
                                                Advertiser ID
                                            </th>
                                            <th style="font-weight:bold;" <%=(stringList != null && !stringList.contains("Advertiser")) ? "class= hide" : ""%>>
                                                Advertiser Name
                                            </th>
                                        </c:if>&ndash;%&gt;

                                        <c:if test="${fn:contains(data, 'Partner')}">
                                            <th style="font-weight:bold" <%=(stringList != null && !stringList.contains("Partner")) ? "class= hide" : ""%>>
                                                Partner Id
                                            </th>
                                            <th style="font-weight:bold" <%=(stringList != null && !stringList.contains("Partner")) ? "class= hide" : ""%>>
                                                Partner Name
                                            </th>
                                        </c:if>

                                        &lt;%&ndash;<c:if test="${fn:contains(data, 'Sub_Aff')}">
                                            <th style="font-weight:bold" <%=(stringList != null && !stringList.contains("Sub_Aff")) ? "class= hide" : ""%>>
                                                Sub_Aff
                                            </th>
                                        </c:if>&ndash;%&gt;

                                        <th style="font-weight:bold" <%=(!columns.contains("7") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                            Click
                                        </th>

                                        <sec:authorize access="!hasRole('PUBLISHER')">
                                            <th style="font-weight:bold" <%=(!columns.contains("8") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                                Approved Conversion
                                            </th>
                                        </sec:authorize>

                                        &lt;%&ndash;<sec:authorize access="hasRole('PUBLISHER')">
                                            <th style="font-weight:bold" <%=(!columns.contains("8") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                                Conversion
                                            </th>
                                        </sec:authorize>&ndash;%&gt;

                                        <sec:authorize access="!hasRole('PUBLISHER')">
                                            <th style="font-weight:bold" <%=(!columns.contains("9") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                                Cancelled Conversion
                                            </th>
                                            <th style="font-weight:bold" <%=(!columns.contains("10") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                                Pending Conversion
                                            </th>
                                            <th style="font-weight:bold" <%=(!columns.contains("11") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                                Gross Conversion
                                            </th>

                                            <th style="font-weight:bold" <%=(!columns.contains("12") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                                Approved Revenue
                                            </th>
                                            <th style="font-weight:bold" <%=(!columns.contains("13") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                                Cancelled Revenue
                                            </th>
                                            <th style="font-weight:bold" <%=(!columns.contains("14") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                                Pending Revenue
                                            </th>
                                            <th style="font-weight:bold" <%=(!columns.contains("15") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                                Gross Revenue
                                            </th>
                                        </sec:authorize>

                                        &lt;%&ndash;<sec:authorize access="!hasRole('PUBLISHER')">
                                            <th style="font-weight:bold" <%=(!columns.contains("16") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                                Approved Payout
                                            </th>
                                        </sec:authorize>&ndash;%&gt;
                                        <sec:authorize access="!hasRole('PUBLISHER')">
                                            <th style="font-weight:bold" <%=(!columns.contains("16") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                                Ratio
                                            </th>
                                        </sec:authorize>


                                        &lt;%&ndash;<sec:authorize access="hasRole('PUBLISHER')">
                                            <th style="font-weight:bold" <%=(!columns.contains("16") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                                Payout
                                            </th>
                                        </sec:authorize>&ndash;%&gt;

                                        <th style="font-weight:bold" <%=(!columns.contains("17") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                            Day Timestamp
                                        </th>
                                        <th style="font-weight:bold" <%=(!columns.contains("18") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                            Rejected Clicks
                                        </th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <c:forEach var="responseList" items="${list}">
                                        <tr>

                                            <c:if test="${fn:length(data)==0 || fn:contains(data, 'App')}">
                                                <td <%=(stringList != null && !stringList.contains("App")) ? "class= hide" : ""%>>
                                                    <div style="margin-left:150px"></div>
                                                        ${responseList.appId}</td>
                                                <td <%=(stringList != null && !stringList.contains("App")) ? "class= hide" : ""%>>
                                                    <div style="margin-left:100px"></div>
                                                    <a href='/adminConsole/viewoffer?id=${responseList.appId}'
                                                       target="_blank"
                                                       style="color:<%=UIConstants.primaryHyperLinkColor%>;font-weight:bold;">${responseList.appName}</a>
                                                </td>
                                            </c:if>

                                                &lt;%&ndash;<c:if test="${fn:contains(data, 'Advertiser')}">
                                                    <td <%=(stringList != null && !stringList.contains("Advertiser")) ? "class= hide" : ""%>>
                                                        <div style="margin-left:150px"></div>
                                                            ${responseList.advertiserId}</td>
                                                    <td <%=(stringList != null && !stringList.contains("Advertiser")) ? "class= hide" : ""%>>
                                                        <div style="margin-left:150px"></div>
                                                            ${responseList.advertiserName}</td>
                                                </c:if>&ndash;%&gt;

                                            <c:if test="${fn:contains(data, 'Partner')}">
                                                <td <%=(stringList != null && !stringList.contains("Partner")) ? "class= hide" : ""%>>
                                                    <div style="margin-left:150px"></div>
                                                        ${responseList.partnerId}</td>
                                                <td <%=(stringList != null && !stringList.contains("Partner")) ? "class= hide" : ""%>>
                                                    <div style="margin-left:150px"></div>
                                                        ${responseList.partnerName}</td>
                                            </c:if>

                                                &lt;%&ndash;<c:if test="${fn:contains(data, 'Sub_Aff')}">
                                                    <td <%=(stringList != null && !stringList.contains("Sub_Aff")) ? "class= hide" : ""%>>
                                                        <div style="margin-left:150px"></div>
                                                            ${responseList.subAffiliate}</td>
                                                </c:if>&ndash;%&gt;

                                            <td <%=(!columns.contains("7") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                                <div style="margin-left:150px"></div>
                                                    ${responseList.click}</td>

                                            <td <%=(!columns.contains("8") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                                <div style="margin-left:190px"></div>
                                                    ${responseList.approvedConversions}</td>

                                            <sec:authorize access="!hasRole('PUBLISHER')">
                                                <td <%=(!columns.contains("9") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                                    <div style="margin-left:200px"></div>
                                                        ${responseList.cancelledConversions}</td>
                                                <td <%=(!columns.contains("10") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                                    <div style="margin-left:190px"></div>
                                                        ${responseList.pendingConversions}</td>
                                                <td <%=(!columns.contains("11") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                                    <div style="margin-left:190px"></div>
                                                        ${responseList.grossConversions}</td>
                                                <td <%=(!columns.contains("12") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                                    <div style="margin-left:190px"></div>
                                                        ${responseList.approvedRevenue}</td>
                                                <td <%=(!columns.contains("13") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                                    <div style="margin-left:190px"></div>
                                                        ${responseList.cancelledRevenue}</td>
                                                <td <%=(!columns.contains("14") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                                    <div style="margin-left:190px"></div>
                                                        ${responseList.pendingRevenue}</td>
                                                <td <%=(!columns.contains("15") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                                    <div style="margin-left:190px"></div>
                                                        ${responseList.grossRevenue}</td>
                                            </sec:authorize>

                                                &lt;%&ndash;<td <%=(!columns.contains("16") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                                    <div style="margin-left:190px"></div>
                                                        ${responseList.approvedPayout}</td>&ndash;%&gt;

                                            <td <%=(!columns.contains("16") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                                <div style="margin-left:190px"></div>
                                                    ${responseList.ratio}</td>

                                            <td <%=(!columns.contains("17") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                                <div style="margin-left:190px"></div>
                                                    ${responseList.dayTimestamp}</td>
                                            <td <%=(!columns.contains("18") && !columns.isEmpty()) ? "class= hide" : ""%>>
                                                <div style="margin-left:190px"></div>
                                                    ${responseList.rejectedClicks}</td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>

                                </table>
                            </div>&ndash;%&gt;

                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>


<jsp:include page="adminFooter.jsp"/>


<!--   Core JS Files   -->
<!-- multiselect -->
<script src="../assets/js/core/jquery.min.js"></script>
<script src="../assets/js/core/popper.min.js"></script>
<script src="../assets/js/core/bootstrap-material-design.min.js"></script>
<script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!-- Plugin for the momentJs  -->
<script src="../assets/js/plugins/moment.min.js"></script>
<!--  Plugin for Sweet Alert -->
<script src="../assets/js/plugins/sweetalert2.js"></script>
<!-- Forms Validations Plugin -->
<script src="../assets/js/plugins/jquery.validate.min.js"></script>
<!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
<script src="../assets/js/plugins/jquery.bootstrap-wizard.js"></script>
<!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script src="../assets/js/plugins/bootstrap-selectpicker.js"></script>
<!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
<script src="../assets/js/plugins/bootstrap-datetimepicker.min.js"></script>
<!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
<script src="../assets/js/plugins/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../assets/js/jquery.spring-friendly.js"></script>
<!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
<script src="../assets/js/plugins/bootstrap-tagsinput.js"></script>
<!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="../assets/js/plugins/jasny-bootstrap.min.js"></script>
<!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
<script src="../assets/js/plugins/fullcalendar.min.js"></script>
<!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
<script src="../assets/js/plugins/jquery-jvectormap.js"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="../assets/js/plugins/nouislider.min.js"></script>
<!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
<!-- Library for adding elements -->
<script src="../assets/js/plugins/arrive.min.js"></script>
<script src="../assets/js/plugins/chartist.min.js"></script>
<!--  Notifications Plugin    -->
<script src="../assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="../assets/js/material-dashboard.js?v=2.1.0" type="text/javascript"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="../assets/demo/demo.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="../dist/js/select2.js"></script>

<script>


    $(document).ready(function () {
        $().ready(function () {
            $sidebar = $('.sidebar');

            $sidebar_img_container = $sidebar.find('.sidebar-background');

            $full_page = $('.full-page');

            $sidebar_responsive = $('body > .navbar-collapse');

            window_width = $(window).width();

            fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

            if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
                if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
                    $('.fixed-plugin .dropdown').addClass('open');
                }

            }

            $('.fixed-plugin a').click(function (event) {
                // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
                if ($(this).hasClass('switch-trigger')) {
                    if (event.stopPropagation) {
                        event.stopPropagation();
                    } else if (window.event) {
                        window.event.cancelBubble = true;
                    }
                }
            });

            $('.fixed-plugin .active-color span').click(function () {
                $full_page_background = $('.full-page-background');

                $(this).siblings().removeClass('active');
                $(this).addClass('active');

                var new_color = $(this).data('color');

                if ($sidebar.length != 0) {
                    $sidebar.attr('data-color', new_color);
                }

                if ($full_page.length != 0) {
                    $full_page.attr('filter-color', new_color);
                }

                if ($sidebar_responsive.length != 0) {
                    $sidebar_responsive.attr('data-color', new_color);
                }
            });

            $('.fixed-plugin .background-color .badge').click(function () {
                $(this).siblings().removeClass('active');
                $(this).addClass('active');

                var new_color = $(this).data('background-color');

                if ($sidebar.length != 0) {
                    $sidebar.attr('data-background-color', new_color);
                }
            });

            $('.fixed-plugin .img-holder').click(function () {
                $full_page_background = $('.full-page-background');

                $(this).parent('li').siblings().removeClass('active');
                $(this).parent('li').addClass('active');


                var new_image = $(this).find("img").attr('src');

                if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
                    $sidebar_img_container.fadeOut('fast', function () {
                        $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
                        $sidebar_img_container.fadeIn('fast');
                    });
                }

                if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
                    var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

                    $full_page_background.fadeOut('fast', function () {
                        $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
                        $full_page_background.fadeIn('fast');
                    });
                }

                if ($('.switch-sidebar-image input:checked').length == 0) {
                    var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
                    var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

                    $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
                    $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
                }

                if ($sidebar_responsive.length != 0) {
                    $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
                }
            });

            $('.switch-sidebar-image input').change(function () {
                $full_page_background = $('.full-page-background');

                $input = $(this);

                if ($input.is(':checked')) {
                    if ($sidebar_img_container.length != 0) {
                        $sidebar_img_container.fadeIn('fast');
                        $sidebar.attr('data-image', '#');
                    }

                    if ($full_page_background.length != 0) {
                        $full_page_background.fadeIn('fast');
                        $full_page.attr('data-image', '#');
                    }

                    background_image = true;
                } else {
                    if ($sidebar_img_container.length != 0) {
                        $sidebar.removeAttr('data-image');
                        $sidebar_img_container.fadeOut('fast');
                    }

                    if ($full_page_background.length != 0) {
                        $full_page.removeAttr('data-image', '#');
                        $full_page_background.fadeOut('fast');
                    }

                    background_image = false;
                }
            });

            $('.switch-sidebar-mini input').change(function () {
                $body = $('body');

                $input = $(this);

                if (md.misc.sidebar_mini_active == true) {
                    $('body').removeClass('sidebar-mini');
                    md.misc.sidebar_mini_active = false;

                    $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

                } else {

                    $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');

                    setTimeout(function () {
                        $('body').addClass('sidebar-mini');

                        md.misc.sidebar_mini_active = true;
                    }, 300);
                }

                // we simulate the window Resize so the charts will get updated in realtime.
                var simulateWindowResize = setInterval(function () {
                    window.dispatchEvent(new Event('resize'));
                }, 180);

                // we stop the simulation of Window Resize after the animations are completed
                setTimeout(function () {
                    clearInterval(simulateWindowResize);
                }, 1000);

            });
        });

    });

</script>


<script>

    var dateRange = '';
    var startDate = '';
    var filter = '';
    var filter1 = '';
    let appFilter = '';
    let pubFilter = '';
    let advFilter = '';
    let conversionFilter = '';
    var offerId = 0;
    var pubId = 0;
    var search = {};
    var status = '';
    var id = 0;
    var table = $('#datatables').DataTable({
        ajax: "/adminConsole/getAllReportAjax?id=${app.id}",
        serverSide: true,
        processing: true,
        scrollX: true,
        columns: [
            {
                data: 'appVersion',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'sdkVersion',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'appId',
                render: function (data) {
                    offerId = data;
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'appName',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + '<a href="/adminConsole/viewoffer?id=' + offerId + '" target = "_blank" style="color:<%=UIConstants.primaryHyperLinkColor%>;font-weight:bold">' + data + ' </a>';
                }
            },
            {
                data: 'appType',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'bundleId',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },


            {
                data: 'eventTime',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'eventName',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'eventValue',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'eventRevenue',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'eventRevenueCurrency',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'eventRevenueUSD',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },


            {
                data: 'att',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'wifi',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'operator',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'carrier',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'language',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'androidId',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'imei',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'idfa',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'idfv',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'advertisingId',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'deviceModel',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'deviceType',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'deviceCategory',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'platform',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'osVersion',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'userAgent',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'deviceDownloadTime',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'oaid',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'isLAT',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },


            {
                data: 'region',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'countryCode',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'state',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'city',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'postalCode',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'ip',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },


            {
                data: 'attributedTouchType',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'attributedTouchTime',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'installTime',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'costModel',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'costValue',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'subParam1',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'subParam2',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'subParam3',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'subParam4',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'customerUserId',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'isRetargeting',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'storeProductPage',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'costCurrency',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'eventSource',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'partnerId',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'partnerName',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'mediaSource',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'channel',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'keywords',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'retargetingConversionType',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'isPrimaryAttribution',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'attributionLookback',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'reengagementWindow',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'matchType',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'httpReferrer',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'campaign',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'campaignId',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'adset',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'adsetId',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'ad',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'originalUrl',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'adId',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'adType',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'siteId',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },


            {
                data: 'rejectedReason',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'rejectedReasonValue',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'blockedReason',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'blockedReasonRule',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'blockedReasonValue',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'blockedSubReason',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },


            {
                data: 'approvedConversions',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'cancelledConversions',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'pendingConversions',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'rejectedClicks',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'approvedRevenue',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'cancelledRevenue',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'pendingRevenue',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'approvedPayout',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'clickCount',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'dayTimestamp',
                render: function (data) {
                    if (data != null) {
                        var date = new Date(data);
                        const options = {weekday: 'long', year: 'numeric', month: 'long', day: 'numeric'};
                        return '<div style="margin-left:160px"></div>' + date.toLocaleString('en-US', options) + " " + date.toLocaleTimeString('en-US');
                    } else
                        return '<div style="margin-left:160px"></div>NA';
                }

            },
            {
                data: 'ratio',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            },
            {
                data: 'ctr',
                render: function (data) {
                    return ' <div style="margin-left:120px"></div>' + data;
                }
            }
        ],
        "scrollCollapse": true,
        scrollY: 520,
        ordering: true
    });


    $(window).bind('resize', function (e) {
        if (window.RT) clearTimeout(window.RT);
        window.RT = setTimeout(function () {
            table.ajax.reload(); /* false to get page from cache */
        }, 250);
    });
    $('.dataTables_filter input').unbind();
    $('.dataTables_filter input').keyup(function (e) {
        if (e.keyCode === 13) /* if enter is pressed */ {
            table.search($(this).val()).draw();
        }
    });
    $('.sorting').click(function () {
        document.addEventListener("mouseover", exportAllData);
    })
    $(function () {
        var start = moment().subtract(6, 'days').startOf('day');
        var end = moment().endOf('day');

        function cb(start, end) {
            startDate = start;
            dateRange = document.getElementById("reportrangeinput").value = start.format('YYYY-MM-DD HH:mm:ss') + ' - ' + end.format('YYYY-MM-DD HH:mm:ss');
            document.getElementById("reportrangeinput").value = start.format('YYYY-MM-DD HH:mm:ss') + ' - ' + end.format('YYYY-MM-DD HH:mm:ss');
            /*console.log("dateRange: " + dateRange);*/
            table.column(13).search(dateRange).draw();
            $('.btnDatatable').hide();
            $('#exportData').show();
        }

        $('#reportrange').daterangepicker({
            startDate: start,
            timePicker: true,
            timePicker24Hour: true,
            endDate: end,
            ranges: {
                'Today': [moment().startOf('day'), moment().endOf('day')],
                'Yesterday': [moment().subtract(1, 'days').startOf('day'), moment().subtract(1, 'days').endOf('day')],
                'Last 7 Days': [moment().subtract(6, 'days').startOf('day'), moment().endOf('day')],
                'Last 30 Days': [moment().subtract(29, 'days').startOf('day'), moment().endOf('day')],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        cb(start, end);

    });


    $('body').on('change', '#extSelect', function (e) {
        e.preventDefault();
        filter1 = '';
        var i, j;
        filter1 = $(this).val();
        alert("filter is " + filter1)
    });

    $(document).ready(function () {
        filter1 = '';
        filter1 = $("#extSelect").val();
        alert("filter2 is " + filter1 + " " + filter);
    });


    table.columns().visible(false);
    table.columns([2, 3, 4, 5, 11, 12, 13]).visible(true);
    $('#submitForm').click(function () {
        var i, j;
        $("#exampleModalAd1").hide();
        /*console.log(filter1.length);*/
        if (filter1.length > 0) {
            alert("inside if " + filter1)
            table.columns().visible(false);
            table.columns([2, 3, 4, 5, 11, 12]).visible(true);
            for (i = 0; i < filter1.length; i++) {
                switch (filter1[i]) {
                    case 'App ID':
                        table.columns(0).visible(true);
                        break;
                    case 'App Name':
                        table.columns(1).visible(true);
                        break;
                    case 'Partner Id':
                        table.columns(2).visible(true);
                        break;
                    case 'Partner Name':
                        table.columns(3).visible(true);
                        break;
                    case 'Click Count':
                        table.columns(4).visible(true);
                        break;
                    case 'Approved Conversion':
                        table.columns(5).visible(true);
                        break;
                    case 'Cancelled Conversion':
                        table.columns(6).visible(true);
                        break;
                    case 'Pending Conversion':
                        table.columns(7).visible(true);
                        break;
                    case 'Approved Revenue':
                        table.columns(8).visible(true);
                        break;
                    case 'Cancelled Revenue':
                        table.columns(9).visible(true);
                        break;
                    case 'Pending Revenue':
                        table.columns(10).visible(true);
                        break;
                    case 'Ratio':
                        table.columns(11).visible(true);
                        break;
                    case 'CTR':
                        table.columns(12).visible(true);
                        break;
                    case 'Day Timestamp':
                        table.columns(13).visible(true);
                        break;
                    case 'Rejected Clicks':
                        table.columns(14).visible(true);
                        break;
                }

            }
        }


        $('.btnDatatable').hide();
        $('#exportData').show();

    });


    /*table.columns().visible(false);
    table.columns([]).visible(true);
    $('#submitForm').click(function () {
        var i, j;
        $("#exampleModalAd1").hide();
        /!*console.log(filter1.length);*!/
        if (filter1.length > 0) {
            table.columns().visible(false);
            table.columns([1]).visible(true);
            for (i = 0; i < filter1.length; i++) {
                switch (filter1[i]) {
                    case 'App ID':
                        table.columns(2).visible(true);
                        break;
                    case 'App Name':
                        table.columns(3).visible(true);
                        break;
                    case 'Partner Id':
                        table.columns(4).visible(true);
                        break;
                    case 'Partner Name':
                        table.columns(5).visible(true);
                        break;
                    case 'Click Count':
                        table.columns(6).visible(true);
                        break;
                    case 'Approved Conversion':
                        table.columns(7).visible(true);
                        break;
                    case 'Cancelled Conversion':
                        table.columns(8).visible(true);
                        break;
                    case 'Pending Conversion':
                        table.columns(9).visible(true);
                        break;
                    case 'Gross Conversion':
                        table.columns(10).visible(true);
                        break;
                    case 'Approved Revenue':
                        table.columns(11).visible(true);
                        break;
                    case 'Cancelled Revenue':
                        table.columns(12).visible(true);
                        break;
                    case 'Pending Revenue':
                        table.columns(13).visible(true);
                        break;
                    case 'Gross Revenue':
                        table.columns(14).visible(true);
                        break;
                    case 'Ratio':
                        table.columns(15).visible(true);
                        break;
                    case 'CTR':
                        table.columns(16).visible(true);
                        break;
                    case 'Day Timestamp':
                        table.columns(17).visible(true);
                        break;
                    case 'Rejected Clicks':
                        table.columns(18).visible(true);
                        break;
                }

            }
        }

        table.column(0).search(advFilter);
        table.column(21).search(appFilter);
        table.column(4).search(pubFilter);
        table.column(5).search(conversionFilter).draw();
        $('.btnDatatable').hide();
        $('#exportData').show();

    });*/


    $('#appFilter').click(function () {
        $("#exampleModalAd1").show();
    });


    /*    $('select#publisherListAjax')
            .change(
                function () {
                    pubFilter = '';
                    $(
                        'select#publisherListAjax option:selected')
                        .each(function () {
                            pubFilter += $(this).val() + ",";
                        });
                    pubFilter = pubFilter.substring(0, pubFilter.length - 1);
                    /!*console.log("pubFilter:" + pubFilter);*!/
                });*/


    /*    $('select#advertiserListAjax')
            .change(
                function () {
                    advFilter = '';
                    $(
                        'select#advertiserListAjax option:selected')
                        .each(function () {
                            advFilter += $(this).val() + ",";
                        });
                    advFilter = advFilter.substring(0, advFilter.length - 1);
                    /!*console.log("pubFilter:" + pubFilter);*!/
                });*/


    /*    $('select#offerListAjax')
            .change(
                function () {
                    appFilter = '';
                    $(
                        'select#offerListAjax option:selected')
                        .each(function () {
                            appFilter += $(this).val() + ",";
                        });
                    appFilter = appFilter.substring(0, appFilter.length - 1);
                });*/


    /*
    $('select#conversionStatus')
        .change(
            function () {
                conversionFilter = '';
                $(
                    'select#conversionStatus option:selected')
                    .each(function () {
                        conversionFilter += $(this).val() + ",";
                    });
                conversionFilter = conversionFilter.substring(0, conversionFilter.length - 1);
            });*/


    $('#searchbox').on('keyup', function () {
        table.search(this.value).draw();
        /*console.log(this.value);*/
    });

</script>

<script>

    function addFilter() {
        var myform = document.getElementById("formSubmit1");
        alert("myForm " + myform);
        var fd = new FormData(myform);
        alert("heello");
        event.preventDefault();
        $.ajax({
            url: "/adminConsole/addFilterForm",
            data: fd,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            beforeSend: function () {

                alert("before.");

            },
            success: function (response) {
                alert("success ");
                showNotification('top', 'left', 'Event has been created.', 'success');
                setTimeout(function () {
                    window.location.reload();
                }, 1000);
                $('#signupModal').modal('hide');
            },
            complete: function () {
                alert("complete ");
                // Hide image container

            },
            error: function (jqXHR) {
                var o = $.parseJSON(jqXHR.responseText);
                window.open('/adminConsole/error?status=' + o.status + '&message=' + o.error, "_self")
            }
        });
    }

</script>

<script>

    &lt;%&ndash;<script>
        // var dateRange = '';
        var appFilter = '';
        var advertiserFilter = '';
        var subAffFilter = '';
        var partnerFilter = '';
        var groupByFilter = '';
        var filter1 = '';

        var dateFromServer = "<%=reportFilterDTO.getDaterange()%>";
        /*console.log("dateFromServer:" + dateFromServer);*/

        $(function () {
            var start = (dateFromServer == "null" || dateFromServer == "") ? moment().subtract(6, 'days').startOf('day') :
                new Date(dateFromServer.split(" - ")[0]);
            var end = (dateFromServer == "null" || dateFromServer == "") ? moment().endOf('day') :
                new Date(dateFromServer.split(" - ")[1]);

            function cb(start, end) {
                document.getElementById("reportrangeinput").value = start.format('YYYY-MM-DD HH:mm:ss') + ' - ' + end.format('YYYY-MM-DD HH:mm:ss');
                dateFilter();
            }

            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                    'Today': [moment().startOf('day'), moment().endOf('day')],
                    'Yesterday': [moment().subtract(1, 'days').startOf('day'), moment().subtract(1, 'days').endOf('day')],
                    'Last 7 Days': [moment().subtract(6, 'days').startOf('day'), moment().endOf('day')],
                    'Last 30 Days': [moment().subtract(29, 'days').startOf('day'), moment().endOf('day')],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);
            cb(start, end);
            dateFilter();

        });

        document.getElementById("dateHiddenField").value = document.getElementById("reportrangeinput").value;

        function dateFilter() {
            /*console.log("Inside dateFilterWithFormData");*/
            /*console.log(document.getElementById("reportrangeinput").value);*/
            document.getElementById("dateHiddenField").value = document.getElementById("reportrangeinput").value;
            /*document.getElementById("formSubmit").submit();*/
        }&ndash;%&gt;


    $('body').on('change', '#extSelect', function (e) {
        /*console.log("Inside filter1")*/
        e.preventDefault();
        filter1 = '';
        filter1 = $(this).val();
    });

    $('body').on('change', '#subAffiliate', function (e) {
        subAffFilter = $(this).val();
        /*console.log("subAffFilter:" + subAffFilter);*/
    });

    $('#click').click(function () {
        $("#exampleModalAd1").show();
    });


    $('select#advertSelect')
        .change(
            function () {
                advertiserFilter = '';
                $(
                    'select#advertSelect option:selected')
                    .each(function () {
                        advertiserFilter += $(this).val() + ",";
                    });
                advertiserFilter = advertiserFilter.substring(0, advertiserFilter.length - 1);
                /*console.log("advertiserFilter:" + advertiserFilter);*/
            });


    $('select#appSelect')
        .change(
            function () {
                appFilter = '';
                $(
                    'select#appSelect option:selected')
                    .each(function () {
                        appFilter += $(this).val() + ",";
                    });
                appFilter = appFilter.substring(0, appFilter.length - 1);
                /*console.log("appFilter:" + appFilter);*/
            });


    $('select#partnerSelect')
        .change(
            function () {
                partnerFilter = '';
                $(
                    'select#partnerSelect option:selected')
                    .each(function () {
                        partnerFilter += $(this).val() + ",";
                    });
                partnerFilter = partnerFilter.substring(0, partnerFilter.length - 1);
                /*console.log("partnerFilter:" + partnerFilter);*/
            });
    $('select#groupBy')
        .change(
            function () {
                groupByFilter = '';
                $(
                    'select#groupBy option:selected')
                    .each(function () {
                        groupByFilter += $(this).val() + ",";
                    });
                groupByFilter = groupByFilter.substring(0, groupByFilter.length - 1);
                /*console.log("groupByFilter:" + groupByFilter);*/
            });

</script>

<script>
    $(document).ready(function () {
        filter1 = '';
        filter1 = $("#extSelect").val();
        // initialise Datetimepicker and Sliders
        md.initFormExtendedDatetimepickers();
        if ($('.slider').length != 0) {
            md.initSliders();
        }
    });
</script>

<script>
    $('#groupBy').select2();
    $('#extSelect').select2();

    $('.multipleSelect').select2();


    $("#appFilter").click(function () {
        alert("appFilter");
        offerLoad('#exampleModalAd', 'SELECT PARTNER', '/adminConsole/findByPartnerCompany1', '.partnerSelect');

    });

    function offerLoad(modalName, title, url, className) {
        /*console.log("Inside offerLoad:" + className);*/
        var perPageRecord = 10;
        $(className).select2({
            ajax: {
                url: url,
                type: 'GET',
                dataType: 'json',
                delay: 500,
                data: function (params) {
                    return {
                        search: params.term, // search term
                        page: params.page || 0,
                        size: perPageRecord//Per page records
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 0;
                    return {
                        results: data.results,
                        pagination: {
                            more: params.page < data.count_filtered - 1
                        }
                    };
                },
                cache: true
            },
            placeholder: title,
            minimumInputLength: 0
        });
    }

</script>

&lt;%&ndash;<script>
    columns: [
        {
            data: 'appId',
            render: function (data) {
                offerId = data;
                return ' <div style="margin-left:120px"></div>' + data;
            }
        },
        {
            data: 'appName',
            render: function (data) {
                return ' <div style="margin-left:120px"></div>' + '<a href="/adminConsole/viewoffer?id=' + offerId + '" target = "_blank" style="color:<%=UIConstants.primaryHyperLinkColor%>;font-weight:bold">' + data + ' </a>';
            }
        },
        {
            data: 'partnerId',
            render: function (data) {
                pubId = data;
                return ' <div style="margin-left:120px"></div>' + data;
            }
        },
        {
            data: 'partnerName',
            render: function (data) {
                return ' <div style="margin-left:120px"></div>' + data;
            }
        },
        {
            data: 'clickCount',
            render: function (data) {
                return ' <div style="margin-left:120px"></div>' + data;
            }
        },
        {
            data: 'approvedConversions',
            render: function (data) {
                return ' <div style="margin-left:120px"></div>' + data;
            }
        },
        {
            data: 'cancelledConversions',
            render: function (data) {
                return ' <div style="margin-left:120px"></div>' + data;
            }
        }, {
            data: 'pendingConversions',
            render: function (data) {
                return ' <div style="margin-left:120px"></div>' + data;
            }
        }, {
            data: 'approvedRevenue',
            render: function (data) {
                return ' <div style="margin-left:120px"></div>' + data;
            }
        }, {
            data: 'cancelledRevenue',
            render: function (data) {
                return ' <div style="margin-left:140px"></div>' + data;
            }
        },
        {
            data: 'pendingRevenue',
            render: function (data) {
                return ' <div style="margin-left:140px"></div>' + data;
            }
        },
        {
            data: 'ratio',
            render: function (data) {
                return ' <div style="margin-left:140px"></div>' + data;
            }
        },
        {
            data: 'ctr',
            render: function (data) {
                return ' <div style="margin-left:140px"></div>' + data;
            }
        },
        {
            data: 'dayTimestamp',
            render: function (data) {
                if (data != null) {
                    var date = new Date(data);
                    const options = {weekday: 'long', year: 'numeric', month: 'long', day: 'numeric'};
                    return '<div style="margin-left:160px"></div>' + date.toLocaleString('en-US', options) + " " + date.toLocaleTimeString('en-US');
                } else
                    return '<div style="margin-left:160px"></div>NA';
            }

        },
        {
            data: 'rejectedClicks',
            render: function (data) {
                pubId = data;
                return ' <div style="margin-left:120px"></div>' + data;
            }
        }
    ],
</script>&ndash;%&gt;

</body>

</html>--%>
