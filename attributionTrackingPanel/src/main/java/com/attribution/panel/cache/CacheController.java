package com.attribution.panel.cache;

import com.attribution.panel.cache.dto.*;
import com.attribution.panel.cache.repository.*;
import com.attribution.panel.constants.BackendConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;

@RestController
@Slf4j
@RequestMapping("/cache")
public final class CacheController {

    @Autowired
    CompanyTrackingDTORepository companyTrackingDTORepository;
    @Autowired
    PartnerTrackingDTORepository partnerTrackingDTORepository;
//    @Autowired
//    CampaignTrackingDTORepository campaignTrackingDTORepository;
    @Autowired
    AppTrackingDTORepository appTrackingDTORepository;
    @Autowired
    BackendConstants backendConstants;
//    @Autowired
//    CapCacheDTORepository capCacheDTORepository;
    @Autowired
    Environment environment;
    @Autowired
    EventCacheDTORepository eventCacheDTORepository;
//    @Autowired
//    BusinessRuleCacheRepository businessRuleCacheRepository;

    RestTemplate restTemplate = new RestTemplate();

    public String getEnvironment() {
        String[] activeProfiles = environment.getActiveProfiles();
        return activeProfiles[0];
    }

    @PostConstruct
    public void init() {
        try {
//            if (getEnvironment().equals("prod")) {
            initCache();
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @GetMapping("/initCache")
    public ResponseEntity<?> initCache() {
        log.info("Inside initCache:");
        try {
            ResponseEntity<CompanyTrackingDTO[]> advertiserArr = restTemplate.getForEntity(backendConstants.baseUrlDbAccess + "/cache/advertisers", CompanyTrackingDTO[].class);
            log.info("Inside initCache2:" + advertiserArr);
            List<CompanyTrackingDTO> advertiserList = Arrays.asList(advertiserArr.getBody());
            companyTrackingDTORepository.saveAll(advertiserList);
            log.info("Inside initCache3:" + advertiserList);
        } catch (Exception e) {
            e.printStackTrace();
        }




        try {
            ResponseEntity<AppTrackingDTO[]> offerArr = restTemplate.getForEntity(backendConstants.baseUrlDbAccess + "/cache/offers", AppTrackingDTO[].class);
            log.info("Inside initCache4:" + offerArr);
            List<AppTrackingDTO> offerList = Arrays.asList(offerArr.getBody());
            log.info("Inside initCache4:" + offerList);
            appTrackingDTORepository.saveAll(offerList);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            ResponseEntity<PartnerTrackingDTO[]> publisherArr = restTemplate.getForEntity(backendConstants.baseUrlDbAccess + "/cache/publishers", PartnerTrackingDTO[].class);
            List<PartnerTrackingDTO> publisherList = Arrays.asList(publisherArr.getBody());
            partnerTrackingDTORepository.saveAll(publisherList);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            ResponseEntity<EventCacheDTO[]> eventArr = restTemplate.getForEntity(backendConstants.baseUrlDbAccess + "/cache/events", EventCacheDTO[].class);
            eventCacheDTORepository.deleteAll();
            List<EventCacheDTO> eventCacheDTOList = Arrays.asList(eventArr.getBody());
            eventCacheDTORepository.saveAll(eventCacheDTOList);
        } catch (Exception e) {
            e.printStackTrace();
        }

        log.info("Cache Initialized!!");
        return ResponseEntity.ok().body("Cache Initialized!!!");
    }

}
