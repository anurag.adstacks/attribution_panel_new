package com.attribution.panel.cache.dto;

import com.attribution.panel.enums.*;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.UpdateTimestamp;
import org.javers.core.metamodel.annotation.DiffIgnore;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@RedisHash("appCache")
@Data
@NoArgsConstructor
public class AppTrackingDTO implements Serializable {


    @Id
    private Long id;

    private String name;

    /*@Enumerated(EnumType.STRING)
    private OS os;*/

    private String devOS;
    private String description;

    private String appVersion;
    private String sdkVersion;

    private String appType;
    private String bundleId;  //The Apple bundle ID is a unique identifier associated with iOS apps.


    private String gameLink;

    private Date creationDateTime;

    private Date updateDateTime;

    private UserTrackingDTO user;

    private List<PartnerTrackingDTO> approvedPartner;

    private Long compId;

    private Set<CompanyTrackingDTO> approvedCompany;

    private byte[] appLogo;

    private String previewUrl;

    private String trackingUrl;

    private String uuid = UUID.randomUUID().toString().replace("-", "");

    private Long allClicks = 0L;

    private Long grossClicks = 0L;

    private Long conversions = 0L;

    private List<EventCacheDTO> events;

    private List<Category> category;

    private AppStatus status; //

    public List<Country> countries;


//    @Id
//    private Long id;
//
//    private String name;
//
//    /*@Enumerated(EnumType.STRING)
//    private OS os;*/
//
//    private String devOS;
//    private String description;
//
//    private String appVersion;
//    private String sdkVersion;
//
//    private String appType;
//    private String bundleId;  //The Apple bundle ID is a unique identifier associated with iOS apps.
//
//
//    private String gameLink;
//
//    private Date creationDateTime;
//
//    private Date updateDateTime;
//
//    private UserTrackingDTO user;
//
//    private List<PartnerTrackingDTO> approvedPartner;
//
//    private Long compId;
//
//    private Set<CompanyTrackingDTO> approvedCompany;
//
//    private byte[] appLogo;
//
//    private String previewUrl;
//
//    private String trackingUrl;
//
//    private String uuid = UUID.randomUUID().toString().replace("-", "");
//
//    private Long allClicks = 0L;
//
//    private Long grossClicks = 0L;
//
//    private Long conversions = 0L;
//
//    private List<Long> events;
//
//    private List<Long> category;
//
//    private AppStatus status; //
//
//    public List<String> countries;


//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Long id;
//    private String name;
//
//    /*@Enumerated(EnumType.STRING)
//    private OS os;*/
//
//    private String devOS;
//    private String description;
//
//    private String appVersion;
//    private String sdkVersion;
//
//    private String appType;
//    private String bundleId;  //The Apple bundle ID is a unique identifier associated with iOS apps.
//
//
//    private String gameLink;
//
//    @CreationTimestamp
//    private Date creationDateTime;
//
//    @UpdateTimestamp
//    private Date updateDateTime;
//
//    @ManyToOne(fetch = FetchType.EAGER)
//    private UserTrackingDTO user;
//
//    @ManyToMany(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
//    private List<PartnerTrackingDTO> approvedPartner;
//
//    private Long compId;
//
//    @ManyToMany(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
//    private Set<CompanyTrackingDTO> approvedCompany;
//
//
//    @Lob
//    private byte[] appLogo;
//
//    private String previewUrl;
//
//    private String trackingUrl;
//
//    @Column(unique = true)
//    @DiffIgnore
//    private String uuid = UUID.randomUUID().toString().replace("-", "");
//
//    @Column(columnDefinition = "bigint(20) default 0")
//    private Long allClicks = (long) 0;
//
//    @Column(columnDefinition = "bigint(20) default 0")
//    private Long grossClicks = (long) 0;
//
//    @Column(columnDefinition = "bigint(20) default 0")
//    private Long conversions = (long) 0;
//
//    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "app", orphanRemoval = true)
//    @Fetch(FetchMode.SELECT)
//    @JsonIgnoreProperties("app")
//    @DiffIgnore
//    private List<EventCacheDTO> events;
//
//    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "app")
//    @Fetch(FetchMode.SELECT)
//    @JsonIgnoreProperties("app")
//    @DiffIgnore
//    private List<Category> category;
//
//    @Enumerated(EnumType.STRING)
//    private AppStatus status; //
//
//
//
//    @ManyToMany(fetch = FetchType.LAZY)
//    @Enumerated(EnumType.STRING)
//    public List<Country> countries;

}
