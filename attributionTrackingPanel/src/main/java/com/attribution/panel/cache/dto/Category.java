package com.attribution.panel.cache.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.*;
import java.util.Date;

//@Entity
//@Data
//@NoArgsConstructor
//@AllArgsConstructor
@RedisHash("categoryCache")
@Data
@NoArgsConstructor
public class Category {

    private Long id;

    private String name;

    private Date creationDateTime; // date

    private Date updateDateTime;

    private AppTrackingDTO app;

//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Long id;
//
//    @Column(unique = true)
//    private String name;
//
//    @CreationTimestamp
//    private Date creationDateTime; // date
//
//    @UpdateTimestamp
//    private Date updateDateTime;
//
//    @ManyToOne(fetch = FetchType.EAGER)
//    @JoinColumn(name = "app_id", nullable = false)
//    @JsonIgnore
//    @ToString.Exclude
//    @EqualsAndHashCode.Include
//    private AppTrackingDTO app;

}
