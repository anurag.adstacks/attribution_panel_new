package com.attribution.panel.cache.repository;

import com.attribution.panel.cache.dto.CompanyTrackingDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyTrackingDTORepository extends CrudRepository<CompanyTrackingDTO, Long> {
}
