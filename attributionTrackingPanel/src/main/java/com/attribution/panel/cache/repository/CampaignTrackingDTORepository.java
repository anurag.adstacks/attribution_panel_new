//package com.attribution.panel.cache.repository;
//
//import com.attribution.panel.cache.dto.CampaignTrackingDTO;
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.stereotype.Repository;
//
//@Repository
//public interface CampaignTrackingDTORepository extends JpaRepository<CampaignTrackingDTO, String> {
//    CampaignTrackingDTO findFirstByOfferIdAndPubId(Long offerId, Long pubId);
//
//    CampaignTrackingDTO findByOfferId(Long offerId);
//}
