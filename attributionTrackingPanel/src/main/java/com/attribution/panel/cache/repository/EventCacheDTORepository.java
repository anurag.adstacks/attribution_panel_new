package com.attribution.panel.cache.repository;

import com.attribution.panel.cache.dto.AppTrackingDTO;
import com.attribution.panel.cache.dto.EventCacheDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.redis.core.index.Indexed;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EventCacheDTORepository extends CrudRepository<EventCacheDTO, Long> {

    List<EventCacheDTO> findByOfferId(Long offerId);

    List<EventCacheDTO> findByAppId(Long appId);

    List<EventCacheDTO> findByAppIn(Optional<AppTrackingDTO> appTrackingDTO);

    List<EventCacheDTO> findByAppsId(Long appId);
}
