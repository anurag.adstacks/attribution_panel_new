package com.attribution.panel.dto;


import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class ConversionMapDTO implements Serializable {

    Long appId;
    String gaid;
    String token;
    String agent;
    String ip;
    String s1;
    boolean isApk;
    Long pId;
    String agentRequest;
    String ipRequest;


    @Override
    public String toString() {
        return "ConversionMapDTO{" +
                "appId=" + appId +
                ", gaid='" + gaid + '\'' +
                ", token='" + token + '\'' +
                ", agent='" + agent + '\'' +
                ", ip='" + ip + '\'' +
                ", s1='" + s1 + '\'' +
                ", isApk=" + isApk +
                ", pId=" + pId +
                ", agentRequest='" + agentRequest + '\'' +
                ", ipRequest='" + ipRequest + '\'' +
                '}';
    }


}
