package com.attribution.panel.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class ImpressionTrackingDTO implements Serializable {

    private String id;

    private String ip;
    private String os;
    private String google_aid;
    private String deviceName;
    private String sectionId;
    private int sectionNum;

    private String country;

    private Long appId;
    private String appName;

    private Long partnerId;
    private String partnerName;

    private String agent;

//    private App app;

    private String creationDateTime;

}
