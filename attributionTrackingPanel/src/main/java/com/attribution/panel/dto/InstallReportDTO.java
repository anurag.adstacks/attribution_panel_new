package com.attribution.panel.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Date;

//@Entity
//@Data

@Data
@NoArgsConstructor
@Component
public class InstallReportDTO {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Long id;

    private String id;
    private String ip;
    private String os;
    private String gaid;
    private String deviceName;
    private String agent;

    private String country;
    private Boolean status;

    private Long appId;
    private String appName;

    private Long partnerId;
    private String partnerName;


    private String creationDateTime;

    private String updateDateTime;

}
