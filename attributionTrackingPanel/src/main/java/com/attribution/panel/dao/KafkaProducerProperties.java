package com.attribution.panel.dao;


import com.attribution.panel.dto.AttributionClickTrackingDTO;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties("kafka")
@EnableConfigurationProperties
public class KafkaProducerProperties {

    private String topic = "click";
    private String topic2 = "conversion";
    private String topic3 = "report";
    private String topic4 = "impression";
    private String topic5 = "installreport";
    private String topic6 = "dailyactiveusers";
    private String topic7 = "installgeowise";

}
