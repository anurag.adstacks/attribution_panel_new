package com.attribution.panel.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;


@Data
@Configuration
@PropertySource("classpath:application.propertiess")
public class AppConfig {

	@Value("${server.port}")
	private String appPort;


}