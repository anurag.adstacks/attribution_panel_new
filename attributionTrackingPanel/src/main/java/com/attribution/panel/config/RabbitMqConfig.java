//package com.attribution.panel.config;
//
//import com.attribution.panel.constants.RabbitMqConstants;
//import lombok.Data;
//import org.springframework.amqp.core.Binding;
//import org.springframework.amqp.core.BindingBuilder;
//import org.springframework.amqp.core.Queue;
//import org.springframework.amqp.core.TopicExchange;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//@Data
//@Configuration
//public class RabbitMqConfig {
//
//    @Bean
//    public TopicExchange getApp1Exchange() {
//        return new TopicExchange(RabbitMqConstants.app1Exchange);
//    }
//
//    @Bean
//    public Queue getSaveClickQueue() {
//        return new Queue(RabbitMqConstants.saveClickQueue);
//    }
//
//    @Bean
//    public Binding declareBindingSaveClick() {
//        return BindingBuilder.bind(getSaveClickQueue()).to(getApp1Exchange())
//                .with(RabbitMqConstants.saveClickRouting);
//    }
//
//    @Bean
//    public Queue getSaveConversionQueue() {
//        return new Queue(RabbitMqConstants.saveConversionQueue);
//    }
//
//    @Bean
//    public Binding declareBindingSaveConversion() {
//        return BindingBuilder.bind(getSaveConversionQueue()).to(getApp1Exchange())
//                .with(RabbitMqConstants.saveConversionRouting);
//    }
//
//    @Bean
//    public Queue getSaveAttributionConversionQueue() {
//        return new Queue(RabbitMqConstants.saveAttributionConversionQueue);
//    }
//
//    @Bean
//    public Binding declareBindingSaveAttributionConversionQueue() {
//        return BindingBuilder.bind(getSaveAttributionConversionQueue()).to(getApp1Exchange())
//                .with(RabbitMqConstants.saveAttributionConversionRouting);
//    }
//
//}