package com.attribution.panel.config;


import com.attribution.panel.dto.*;
import com.attribution.panel.dao.KafkaProducerProperties;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;

import java.util.HashMap;
import java.util.Map;

@Configuration
@RequiredArgsConstructor
public class KafkaProducerConfigurationClick {

    @Autowired
    private final KafkaProperties kafkaProperties;

    @Autowired
    private final KafkaProducerProperties kafkaProducerProp;


    @Bean
    public Map<String, Object> producerConfigs() {
        Map<String, Object> props = new HashMap<>(kafkaProperties.buildProducerProperties());
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
        return props;
    }
/*    @Bean
    public Map<String, Object> producerConfigsConv() {
        Map<String, Object> props = new HashMap<>(kafkaProperties.buildProducerProperties());
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
        return props;
    }
    @Bean
    public Map<String, Object> producerConfigsReport() {
        Map<String, Object> props = new HashMap<>(kafkaProperties.buildProducerProperties());
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
        return props;
    }*/




    @Bean
    public ProducerFactory<String, AttributionClickTrackingDTO> producerFactory() {
        return new DefaultKafkaProducerFactory<>(producerConfigs());
    }
    @Bean
    public ProducerFactory<String, ConversionMapDTO> producerFactoryConv() {
        return new DefaultKafkaProducerFactory<>(producerConfigs());
    }
    @Bean
    public ProducerFactory<String, AttributionAllReportTrackingDTO> producerFactoryReport() {
        return new DefaultKafkaProducerFactory<>(producerConfigs());
    }
    @Bean
    public ProducerFactory<String, ImpressionTrackingDTO> producerFactoryImpression() {
        return new DefaultKafkaProducerFactory<>(producerConfigs());
    }
    @Bean
    public ProducerFactory<String, InstallReportDTO> producerFactoryInstallReport() {
        return new DefaultKafkaProducerFactory<>(producerConfigs());
    }
    @Bean
    public ProducerFactory<String, DailyActiveUsersDTO> producerFactoryDailyActiveUsers() {
        return new DefaultKafkaProducerFactory<>(producerConfigs());
    }
    @Bean
    public ProducerFactory<String, InstallGeoWise> producerFactoryInstallGeoWise() {
        return new DefaultKafkaProducerFactory<>(producerConfigs());
    }





    @Bean
    public KafkaTemplate<String, AttributionClickTrackingDTO> kafkaTemplate() {
        return new KafkaTemplate<>(producerFactory());
    }
    @Bean
    public KafkaTemplate<String, ConversionMapDTO> kafkaTemplateConv() {
        return new KafkaTemplate<>(producerFactoryConv());
    }
    @Bean
    public KafkaTemplate<String, AttributionAllReportTrackingDTO> kafkaTemplateReport() {
        return new KafkaTemplate<>(producerFactoryReport());
    }
    @Bean
    public KafkaTemplate<String, ImpressionTrackingDTO> kafkaTemplateImpression() {
        return new KafkaTemplate<>(producerFactoryImpression());
    }
    @Bean
    public KafkaTemplate<String, InstallReportDTO> kafkaTemplateInstallReport() {
        return new KafkaTemplate<>(producerFactoryInstallReport());
    }
    @Bean
    public KafkaTemplate<String, DailyActiveUsersDTO> kafkaTemplateDailyActiveUsers() {
        return new KafkaTemplate<>(producerFactoryDailyActiveUsers());
    }
    @Bean
    public KafkaTemplate<String, InstallGeoWise> kafkaTemplateInstallGeoWise() {
        return new KafkaTemplate<>(producerFactoryInstallGeoWise());
    }





    @Bean
    public NewTopic adviceTopic() {
        return new NewTopic(kafkaProducerProp.getTopic(), 3, (short) 1);
    }

    @Bean
    public NewTopic adviceTopicConv() {
        return new NewTopic(kafkaProducerProp.getTopic2(), 3, (short) 1);
    }

    @Bean
    public NewTopic adviceTopicReport() {
        return new NewTopic(kafkaProducerProp.getTopic3(), 3, (short) 1);
    }

    @Bean
    public NewTopic adviceTopicImpression() {
        return new NewTopic(kafkaProducerProp.getTopic4(), 3, (short) 1);
    }

    @Bean
    public NewTopic adviceTopicInstallReport() {
        return new NewTopic(kafkaProducerProp.getTopic5(), 3, (short) 1);
    }

    @Bean
    public NewTopic adviceTopicDailyActiveUsers() {
        return new NewTopic(kafkaProducerProp.getTopic6(), 3, (short) 1);
    }

    @Bean
    public NewTopic adviceTopicInstallGeoWise() {
        return new NewTopic(kafkaProducerProp.getTopic7(), 3, (short) 1);
    }

}
