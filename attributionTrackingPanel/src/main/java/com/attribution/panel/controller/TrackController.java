package com.attribution.panel.controller;

//import com.attribution.panel.service.ConversionsService;
import com.attribution.panel.service.ConversionsService;
import com.attribution.panel.service.TrackService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@Controller
@Slf4j
@RequestMapping("/track")
public class TrackController {

    @Autowired
    TrackService trackService;

    @Autowired
    ConversionsService conversionsService;

//    @Autowired
//    AppRepository appRepository;

//    @Autowired
//    ConversionsService conversionsService;

    //Tracking api
    // http://localhost:8080/track/tracking?app_id=1&p_id=2&p_clickId={CLICK_ID}&dev_os={DEV_OS}&sub_pId={SUB_PID}&source={SOURCE}&s1={S1}

    @GetMapping("/tracking")
    public Object tracking(@RequestParam("app_id") Long appId, @RequestParam("p_id") Long pId,
                           // format of click id
                           @RequestParam(value = "p_click_id", required = false, defaultValue = "") String pClickId,
                           @RequestParam(value = "dev_os", required = false, defaultValue = "") String devOs,
                           @RequestParam(value = "sub_pId", required = false, defaultValue = "") String subPid,
                           @RequestParam(value = "source", required = false, defaultValue = "") String source,
                           @RequestParam(value = "s1", required = false, defaultValue = "") String s1,
                           HttpServletRequest request,
                           HttpServletResponse response) throws JsonProcessingException {
        System.out.println("insid controller");
        String url = "", msg = "";


        try {
            url = trackService.tracking(appId, pId, pClickId, devOs, subPid, source, s1, request);
            System.out.println("urlyoo = "+url);
            if (!url.startsWith("http"))
                url = "http://" + url;
            response.setHeader("Location", url);
            response.setStatus(302);
            RedirectView redirectView = new RedirectView();
            redirectView.setUrl(url);
            return redirectView;
        } catch (Exception e) {
            e.printStackTrace();
            msg = e.getMessage();
            response.setStatus(400);
            return ResponseEntity.badRequest().body(msg);
        }
    }

//    http://localhost:9098/track/attributionPostback?app_id=1&p_id=2&token={token}&gaid={gaid}&userAgent={userAgent}&s1={s1}&isApk=1&ip={ip}

//    http://localhost:9098/track/attributionPostback?app_id=1&p_id=2&token=first&gaid=cdda802e-fb9c-47ad-9866-0794d394c912&userAgent=Mozilla/5.0 (Linux; Android 10; POCO X2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.101 Mobile Safari/537.36&s1={s1}&isApk=1&ip=192.168.1.14
//    http://localhost:8080/track/attributionPostback?app_id=1&p_id=2&token=af_test2&gaid=cdda802e-fb9c-47ad-9866-0794d394c912&userAgent=Mozilla/5.0%20(Linux;%20Android%2010;%20POCO%20X2)%20AppleWebKit/537.36%20(KHTML,%20like%20Gecko)%20Chrome/83.0.4103.101%20Mobile%20Safari/537.36&s1={s1}&isApk=1&ip=122.176.43.40
//    http://localhost:8080/track/attributionPostback?app_id=2&p_id=1&token=token&gaid=cdda802e-fb9c-47ad-9866-0794d394c912&userAgent=Mozilla/5.0%20(Linux;%20U;%20Windows%204.4.2;%20en-us;%20SCH-I535%20Build/KOT49H)%20AppleWebKit/534.30%20(KHTML,%20like%20Gecko)%20Version/4.0%20Mobile%20Safari/534.30&s1={s1}&isApk=1&ip=192.168.1.7
//    http://localhost:8080/track/attributionPostback?app_id=1&p_id=2&token=af_test2&gaid=cdda802e-fb9c-47ad-9866-0794d394c912&userAgent=Mozilla/5.0%20(Linux;%20Android%2010;%20POCO%20X2)%20AppleWebKit/537.36%20(KHTML,%20like%20Gecko)%20Chrome/83.0.4103.101%20Mobile%20Safari/537.36&s1={s1}&isApk=1&ip=124.41.204.21
//    http://localhost:8080/track/attributionPostback?app_id=1&p_id=2&token=af_test2&gaid=cdda802e-fb9c-47ad-9866-0794d394c912&userAgent=Mozilla/5.0%20(Linux;%20Android%2010;%20POCO%20X2)%20AppleWebKit/537.36%20(KHTML,%20like%20Gecko)%20Chrome/83.0.4103.101%20Mobile%20Safari/537.36&s1={s1}&isApk=1&ip=122.160.74.236


    //    Attribution platform api
    @GetMapping("/attributionPostback")
    public Object attributionPostback(@RequestParam(name = "app_id", required = false, defaultValue = "") Long appId,
                                                  @RequestParam(name = "p_id", required = false, defaultValue = "") Long pId,
                                                  @RequestParam(name = "token", required = false, defaultValue = "") String token,
                                                  @RequestParam(name = "gaid", required = false, defaultValue = "") String gaid,
                                                  @RequestParam(name = "userAgent", required = false, defaultValue = "") String userAgent,
                                                  @RequestParam(value = "s1", required = false, defaultValue = "") String s1,
                                                  @RequestParam(value = "isApk", required = false, defaultValue = "false") Boolean isApk,
                                                  @RequestParam(name = "ip", required = false, defaultValue = "") String ip,
                                                  HttpServletRequest request, HttpServletResponse response)  throws JsonProcessingException {

        log.info("Inside attribution postback" + userAgent);

        String url = "", msg = "";
        try {
            Map<String, String> msg1 = conversionsService.attributionPostback(appId, gaid, token, userAgent, ip, s1, isApk, pId, request);
            System.out.println("urlyoo = "+url);
            response.setHeader("Location", url);
            response.setStatus(302);

            return ResponseEntity.ok().body(msg1);
        } catch (Exception e) {
            e.printStackTrace();
            msg = e.getMessage();
            response.setStatus(400);
            return ResponseEntity.badRequest().body(msg);
        }
    }


/*   @GetMapping("/tracking")
    public Object tracking(@RequestParam("app") Long appId, @RequestParam("partnerId") Long partnerId,
                           // format of click id
                           @RequestParam(value = "partnerClickId", required = false, defaultValue = "") String partnerClickId,
                           @RequestParam(value = "gaid", required = false, defaultValue = "") String gaid,
                           @RequestParam(value = "idfa", required = false, defaultValue = "") String idfa,// change to device_id
                           @RequestParam(value = "sub_affid", required = false, defaultValue = "") String subAff,
                           @RequestParam(value = "source", required = false, defaultValue = "") String source,
                           @RequestParam(value = "s1", required = false, defaultValue = "") String s1,
                           @RequestParam(value = "s2", required = false, defaultValue = "") String s2,
                           @RequestParam(value = "s3", required = false, defaultValue = "") String s3,
                           @RequestParam(value = "s4", required = false, defaultValue = "") String s4,
                           @RequestParam(value = "s5", required = false, defaultValue = "") String s5,
                           // @RequestParam(value ="aff_test", required = false, defaultValue="0") int test
                           HttpServletRequest request,
                           HttpServletResponse response) throws JsonProcessingException {
        String url = "", msg = "";
        try {
            url = trackService.tracking(appId, partnerId, partnerClickId, gaid, idfa, subAff, source, s1, s2, s3, s4, s5, request);
            System.out.println("url = "+url);
            if (!url.startsWith("http"))
                url = "http://" + url;
            response.setHeader("Location", url);
            response.setStatus(302);
            RedirectView redirectView = new RedirectView();
            redirectView.setUrl(url);
            return redirectView;
        } catch (Exception e) {
            e.printStackTrace();
            msg = e.getMessage();
            response.setStatus(400);
            return ResponseEntity.badRequest().body(msg);
        }
    }*/


/*
    //Conversion postback api
    @GetMapping("/pb")
    public ResponseEntity<?> postback(@RequestParam("click_id") String uuid,
                                      @RequestParam(name = "token", required = false, defaultValue = "") String token,
                                      HttpServletRequest request
                                      ) {
        return conversionsService.getPostbackResponse(uuid, token,request);
    }

        @GetMapping("/attributionPostback")
    public ResponseEntity<?>  attributionPostback(@RequestParam(name = "offerId", required = false, defaultValue = "0") Long offerId,
                                                   @RequestParam(name = "affid", required = false, defaultValue = "0") Long affid,
                                                   @RequestParam(name = "token", required = false, defaultValue = "") String token,
                                                   @RequestParam(name = "gaid", required = false, defaultValue = "") String gaid,
                                                   @RequestParam(name = "userAgent", required = false, defaultValue = "") String userAgent,
                                                   @RequestParam(value = "s1", required = false, defaultValue = "") String s1,
                                                   @RequestParam(value = "s2", required = false, defaultValue = "") String s2,
                                                   @RequestParam(value = "s3", required = false, defaultValue = "") String s3,
                                                   @RequestParam(value = "s4", required = false, defaultValue = "") String s4,
                                                   @RequestParam(value = "s5", required = false, defaultValue = "") String s5,
                                                   @RequestParam(value = "isApk", required = false, defaultValue = "false") Boolean isApk,
                                                   @RequestParam(name = "ip", required = false, defaultValue = "") String ip,
                                                   HttpServletRequest request) {
        log.info("Inside attribution postback");
        return ResponseEntity.ok().body(conversionsService.attributionPostback(offerId, gaid, token, userAgent, ip, s1, s2, s3, s4, s5, isApk, affid, request)
        );
    }*/






    //Dummy api for tracking
//    @GetMapping("/api")
//    public Object api(@RequestParam("app_id") Long appId, @RequestParam("p_id") Long pId,
//                      @RequestParam(value = "p_clickId", required = false, defaultValue = "") String pClickId,
//                      @RequestParam(value = "device_id", required = false, defaultValue = "") DeviceId deviceId,
//                      @RequestParam(value = "sub_pId", required = false, defaultValue = "") String subPid,
//                      @RequestParam(value = "source", required = false, defaultValue = "") String source,
//                      @RequestParam(value = "app_name", required = false, defaultValue = "") String appName,
//                      @RequestParam(value = "s1", required = false, defaultValue = "") String s1,
//                      HttpServletRequest request,
//                      HttpServletResponse response) throws JsonProcessingException {
//        String url = "", msg = "";
//        try {
//            log.info("appID:" + appId + " partnerID:" + pId);
//            url = trackService.tracking(appId, pId, pClickId, deviceId, subPid, source, appName, s1, request);
//            if (url.contains("404"))
//                return ResponseEntity.notFound();
//            else
//                return ResponseEntity.ok().body("Request Registered!!");
//        } catch (Exception e) {
//            e.printStackTrace();
//            msg = e.getMessage();
//            response.setStatus(400);
//            return ResponseEntity.badRequest().body(msg);
//        }
//    }


}
