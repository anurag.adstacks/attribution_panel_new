package com.attribution.panel.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class DefaultTrackController {
    Logger log = LogManager.getLogger();
    @Autowired
    Environment environment;

    public String getEnvironment() {
        String[] activeProfiles = environment.getActiveProfiles();
        return activeProfiles[0];
    }

    @GetMapping("/")
    public ResponseEntity<?> response(){
        return ResponseEntity.ok().body("DefaultTrackController Executed");
    }

    @GetMapping("/getActiveApplicationProperty")
    public ResponseEntity<?> getApplicationProp(){
        return ResponseEntity.ok().header("env",getEnvironment()).body(getEnvironment());
    }

}
