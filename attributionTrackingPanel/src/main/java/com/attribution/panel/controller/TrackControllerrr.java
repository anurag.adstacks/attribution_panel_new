//package com.attribution.panel.controller;
//
//import com.etreetech.panel.service.ConversionsServiceeee;
//import com.etreetech.panel.service.TrackServiceee;
//import com.fasterxml.jackson.core.JsonProcessingException;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.ResponseEntity;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.servlet.view.RedirectView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//@Controller
//@Slf4j
//@RequestMapping("/track")
//public class TrackControllerrr {
//
//    @Autowired
//    TrackServiceee trackService;
//    @Autowired
//    ConversionsServiceeee conversionsService;
//
//    //Tracking api
//    @GetMapping("/tracking")
//    public Object tracking(@RequestParam("off") Long offerId, @RequestParam("aff") Long affId,
//                           // format of click id
//                           @RequestParam(value = "pub_click_id", required = false, defaultValue = "") String clickId,
//                           @RequestParam(value = "gaid", required = false, defaultValue = "") String gaid,
//                           @RequestParam(value = "idfa", required = false, defaultValue = "") String idfa,// change to device_id
//                           @RequestParam(value = "sub_affid", required = false, defaultValue = "") String subAff,
//                           @RequestParam(value = "source", required = false, defaultValue = "") String source,
//                           @RequestParam(value = "s1", required = false, defaultValue = "") String s1,
//                           @RequestParam(value = "s2", required = false, defaultValue = "") String s2,
//                           @RequestParam(value = "s3", required = false, defaultValue = "") String s3,
//                           @RequestParam(value = "s4", required = false, defaultValue = "") String s4,
//                           @RequestParam(value = "s5", required = false, defaultValue = "") String s5,
//                           // @RequestParam(value ="aff_test", required = false, defaultValue="0") int test
//                           HttpServletRequest request,
//                           HttpServletResponse response) throws JsonProcessingException {
//        String url = "", msg = "";
//        try {
//            url = trackService.tracking(offerId, affId, clickId, gaid, idfa, subAff, source, s1, s2, s3, s4, s5, request);
//            System.out.println("url = "+url);
//            if (!url.startsWith("http"))
//                url = "http://" + url;
//            response.setHeader("Location", url);
//            response.setStatus(302);
//            RedirectView redirectView = new RedirectView();
//            redirectView.setUrl(url);
//            return redirectView;
//        } catch (Exception e) {
//            e.printStackTrace();
//            msg = e.getMessage();
//            response.setStatus(400);
//            return ResponseEntity.badRequest().body(msg);
//        }
//    }
//
//    //Conversion postback api
//    @GetMapping("/pb")
//    public ResponseEntity<?> postback(@RequestParam("click_id") String uuid,
//                                      @RequestParam(name = "token", required = false, defaultValue = "") String token,
//                                      HttpServletRequest request
//                                      ) {
//        return conversionsService.getPostbackResponse(uuid, token,request);
//    }
//
//    //Attribution platform api
//    @GetMapping("/attributionPostback")
//    public ResponseEntity<?>  attributionPostback(@RequestParam(name = "offerId", required = false, defaultValue = "0") Long offerId,
//                                                   @RequestParam(name = "affid", required = false, defaultValue = "0") Long affid,
//                                                   @RequestParam(name = "token", required = false, defaultValue = "") String token,
//                                                   @RequestParam(name = "gaid", required = false, defaultValue = "") String gaid,
//                                                   @RequestParam(name = "userAgent", required = false, defaultValue = "") String userAgent,
//                                                   @RequestParam(value = "s1", required = false, defaultValue = "") String s1,
//                                                   @RequestParam(value = "s2", required = false, defaultValue = "") String s2,
//                                                   @RequestParam(value = "s3", required = false, defaultValue = "") String s3,
//                                                   @RequestParam(value = "s4", required = false, defaultValue = "") String s4,
//                                                   @RequestParam(value = "s5", required = false, defaultValue = "") String s5,
//                                                   @RequestParam(value = "isApk", required = false, defaultValue = "false") Boolean isApk,
//                                                   @RequestParam(name = "ip", required = false, defaultValue = "") String ip,
//                                                   HttpServletRequest request) {
//        log.info("Inside attribution postback");
//        return ResponseEntity.ok().body(conversionsService.attributionPostback(offerId, gaid, token, userAgent, ip, s1, s2, s3, s4, s5, isApk, affid, request)
//        );
//    }
//
//    //Dummy api for tracking
//    @GetMapping("/api")
//    public Object api(@RequestParam("off") Long offerId, @RequestParam("aff") Long affId,
//                      @RequestParam(value = "pub_click_id", required = false, defaultValue = "") String clickId,
//                      @RequestParam(value = "gaid", required = false, defaultValue = "") String gaid,
//                      @RequestParam(value = "idfa", required = false, defaultValue = "") String idfa,// change to device_id
//                      @RequestParam(value = "sub_affid", required = false, defaultValue = "") String subAff,
//                      @RequestParam(value = "source", required = false, defaultValue = "") String source,
//                      @RequestParam(value = "s1", required = false, defaultValue = "") String s1,
//                      @RequestParam(value = "s2", required = false, defaultValue = "") String s2,
//                      @RequestParam(value = "s3", required = false, defaultValue = "") String s3,
//                      @RequestParam(value = "s4", required = false, defaultValue = "") String s4,
//                      @RequestParam(value = "s5", required = false, defaultValue = "") String s5,
//                      HttpServletRequest request,
//                      HttpServletResponse response) throws JsonProcessingException {
//        String url = "", msg = "";
//        try {
//            log.info("offerId:" + offerId + " pubId:" + affId);
//            url = trackService.tracking(offerId, affId, clickId, gaid, idfa, subAff, source, s1, s2, s3, s4, s5, request);
//            if (url.contains("404"))
//                return ResponseEntity.notFound();
//            else
//                return ResponseEntity.ok().body("Request Registered!!");
//        } catch (Exception e) {
//            e.printStackTrace();
//            msg = e.getMessage();
//            response.setStatus(400);
//            return ResponseEntity.badRequest().body(msg);
//        }
//    }
//}
