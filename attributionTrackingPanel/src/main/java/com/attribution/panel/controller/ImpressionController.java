package com.attribution.panel.controller;

import com.attribution.panel.dto.ImpressionTrackingDTO;
import com.attribution.panel.kafka.JsonImpressionKafkaProducer;
import com.attribution.panel.service.AttributionAllReportService;
import com.attribution.panel.service.TrackUtilityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/impression")
public class ImpressionController {

    @Autowired
    JsonImpressionKafkaProducer jsonImpressionKafkaProducer;


//    @Autowired
//    AppService appService;
//
//    @Autowired
//    IpRequestService ipRequestService;
//    @Autowired
//    ImpressionService impressionService;
//    @Autowired
//    InstallReportService installReportService;
//    @Autowired
//    DailyActiveUsersService dailyActiveUsersService;
//
//    @Autowired
//    ImpressionRepository impressionRepository;
//
//    @Autowired
//    PartnerService partnerService;

    @Autowired
    AttributionAllReportService attributionAllReportService;

    @Autowired
    TrackUtilityService trackUtilityService;


// http://localhost:8080/impression/saveImpression?appId=1&partnerId=2&os={os}&device={device}&google_aid={google_aid}


    @GetMapping(value = "/saveImpression")
    public ResponseEntity<?> saveData(@RequestParam(value = "appId") Long appId,
                                      @RequestParam(value = "partnerId") Long partnerId,
                                      @RequestParam(value = "os") String os,
                                      @RequestParam(value = "device") String device,
                                      @RequestParam(value = "google_aid") String google_aid,
                                      HttpServletRequest request, Map<String, Object> model) throws Exception {
        System.out.println("Inside Impression");
        String date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").format(new Date());
        System.out.println("date is " + date);

        ImpressionTrackingDTO impressionTrackingDTO = new ImpressionTrackingDTO();
        impressionTrackingDTO.setAppId(appId);
        impressionTrackingDTO.setPartnerId(partnerId);
        impressionTrackingDTO.setOs(os);
        impressionTrackingDTO.setDeviceName(device);
        impressionTrackingDTO.setGoogle_aid(google_aid);

        String agent = request.getHeader("User-Agent");
        impressionTrackingDTO.setAgent(agent);
        impressionTrackingDTO.setCreationDateTime(date);


        jsonImpressionKafkaProducer.sendImpressionReport(impressionTrackingDTO);

//        CountryEnum country = null;
//        String ip = null;
//        App app = appService.findById(appId);
//        Partner partner = partnerService.findById(partnerId);
//
//        String date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").format(new Date());
//        System.out.println("date is " + date);
//
//        ip = "122.160.74.236";
//
//        File database = new File(UrlConstants.DATABASE_CITY_PATH);
//        File database1 = new File(UrlConstants.DATABASE_COUNTRY_PATH);
//        DatabaseReader reader = new DatabaseReader.Builder(database).build();
//        DatabaseReader reader1 = new DatabaseReader.Builder(database1).build();
//
////        File dbFile = new File(UrlConstants.DATABASE_CITY_PATH);
//
//        // This creates the DatabaseReader object,
//        // which should be reused across lookups.
//
////        DatabaseReader reader = new DatabaseReader.Builder(dbFile).build();
//
//        // A IP Address
//        InetAddress ipAddress = InetAddress.getByName(ip);
//        System.out.println("InetAddress " + ipAddress);
//
//        // Get City info
//        CityResponse response = reader.city(ipAddress);
//
//        // Country Info
//        Country country = response.getCountry();
//        System.out.println("Country IsoCode: " + country.getIsoCode()); // 'US'
//        System.out.println("Country Name: " + country.getName()); // 'United States'
//        System.out.println(country.getNames().get("zh-CN")); // '美国'
//
//        Subdivision subdivision = response.getMostSpecificSubdivision();
//        System.out.println("Subdivision Name: " + subdivision.getName()); // 'Minnesota'
//        System.out.println("Subdivision IsoCode: " + subdivision.getIsoCode()); // 'MN'
//
//        // City Info.
//        City city = response.getCity();
//        System.out.println("City Name: " + city.getName()); // 'Minneapolis'
//
//        // Postal info
//        Postal postal = response.getPostal();
//        System.out.println(postal.getCode()); // '55455'
//
//        // Geo Location info.
//        Location location = response.getLocation();
//
//        // Latitude
//        System.out.println("Latitude: " + location.getLatitude()); // 44.9733
//
//        // Longitude
//        System.out.println("Longitude: " + location.getLongitude()); // -93.2323
//
//        String postals = String.valueOf(postal);
//        Map<String, Object> map1 = new HashMap<>();
//        map1.put("countryIsoCode", country.getIsoCode());
//        map1.put("countryName", country.getName());
//        map1.put("subdivisionIsoCode", subdivision.getIsoCode());
//        map1.put("subdivisionName", subdivision.getName());
//        map1.put("city", city.getName());
//        map1.put("Latitude", location.getLatitude());
//        map1.put("Longitude", location.getLongitude());
//        map1.put("postal", postals);
//        map1.put("ip", ip);
//
//
////        ip = ipRequestService.getClientIp(request);
////        String countryCode = request.getHeader("CF-IPCountry");
//
//        /*String countryCode = "IN";
//        System.out.println("countryCode " + countryCode);
//        try {
//            country = CountryEnum.valueOf(countryCode);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }*/
//
//
//        InstallReportDTO installReport = installReportService.findByAppIdAndGaid(appId, google_aid);
//        System.out.println("InstallReportDTO " + installReport);
//
//        System.out.println("countryCode " + country);
//
//        String sectionId = RandomStringUtils.randomAlphanumeric(12);
//
//
//        String agent = request.getHeader("User-Agent");
//        UserAgent userAgent = uaa.parse(agent);
//
//
////        if (installReport != null) {
//
//
//        Impression impression = new Impression();
//        impression.setId(UUID.randomUUID().toString().replace("-", ""));
//        impression.setIp(ip);
//        impression.setCountry(String.valueOf(country));
//        impression.setDeviceName(device);
//        impression.setGoogle_aid(google_aid);
//        impression.setOs(os);
//        impression.setAppId(appId);
//        impression.setAppName(app.getName());
//        impression.setPartnerId(partnerId);
//        impression.setPartnerName(partner.getName());
//        impression.setCreationDateTime(date);
//        impressionService.save(impression);
//    attributionAllReportService.saveImpresionDailyAllReportRecord(userAgent, map1, impression);
//        model.put("status", true);
//        String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
//        DailyActiveUsersDTO dailyActiveUsers = dailyActiveUsersService.findByAppIDAndGaid(appId, google_aid, currentDate);
//        System.out.println("dailyActiveUsers  = " + dailyActiveUsers);
//        if (dailyActiveUsers == null) {
//            dailyActiveUsers = new DailyActiveUsersDTO();
//            dailyActiveUsers.setIp(ip);
//            dailyActiveUsers.setCountry(String.valueOf(country));
//            dailyActiveUsers.setDeviceName(device);
//            dailyActiveUsers.setGaid(google_aid);
//            dailyActiveUsers.setOs(os);
////                dailyActiveUsers.setApp(app);
//            dailyActiveUsers.setAppID(appId);
//            dailyActiveUsers.setAppName(app.getName());
//            dailyActiveUsers.setPartnerId(partnerId);
//            dailyActiveUsers.setPartnerName(partner.getName());
//            dailyActiveUsersService.save(dailyActiveUsers);
////                List<Impression> impressions = (List<Impression>) impressionRepository.findAll();
//            System.out.println("impressionService " + dailyActiveUsers);
//
//            model.put("response DAU", "Save Daily Active User");
//        } else
//            model.put("response DAU", "Daily Active User Already Exist");


        model.put("response Imp.", "Saved User Impression");
        return ResponseEntity.ok().body(model);
        /*} else
            return ResponseEntity.ok().body("User Does Not Exist");*/
    }

    @GetMapping(value = "/count")
    public ResponseEntity<?> Count(@RequestParam(value = "appId") Long appId, @RequestParam(value = "dateRange") String dateRange) {
        Map<String, Object> data = new HashMap<String, Object>();
        Map<String, String> map = getDates(dateRange);
        String startDate = map.get("startDate");
        String endDate = map.get("endDate");
//        Long count = impressionService.findByAppIDAndStartBetweenEndDate(appId, startDate, endDate);
//        data.put("count", count);
        return ResponseEntity.ok().body(data);
    }

    public Map<String, String> getDates(String dateRange) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat formatterForJsDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String startDate = null;
        String endDate = null;
        if (dateRange.equals("")) {
            endDate = formatter.format(new Date());
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, -7);
            startDate = formatter.format(cal.getTime());
        } else {
            String[] startDateArr = dateRange.split(" - ");
            try {
                startDate = formatter.format(formatterForJsDate.parse(startDateArr[0]));
                endDate = formatter.format(formatterForJsDate.parse(startDateArr[1]));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Map<String, String> mp = new HashMap<>();
        mp.put("startDate", startDate);
        mp.put("endDate", endDate);
        return mp;
    }
}
