package com.attribution.panel.controller;

import com.attribution.panel.dto.InstallReportDTO;
import com.attribution.panel.kafka.JsonInstallReportKafkaProducer;
import com.attribution.panel.service.IpRequestService;
import nl.basjes.parse.useragent.UserAgent;
import nl.basjes.parse.useragent.UserAgentAnalyzer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
@RequestMapping("/install")
public class InstallReportController {

    @Autowired
    IpRequestService ipRequestService;

    @Autowired
    JsonInstallReportKafkaProducer jsonInstallReportKafkaProducer;


    static UserAgentAnalyzer uaa;
    static UserAgent userAgent;

//  http://localhost:8080/install/saveInstall?appId=1&partnerId=2&os={os}&device={device}&google_aid={google_aid}

    @GetMapping(value = "/saveInstall")
    public ResponseEntity<?> saveData(@RequestParam(value = "appId") Long appId,
                                      @RequestParam(value = "partnerId") Long partnerId,
                                      @RequestParam(value = "os") String os,
                                      @RequestParam(value = "device") String device,
                                      @RequestParam(value = "google_aid") String google_aid,
                                      HttpServletRequest request, Map<String, Object> model) throws Exception {


        InstallReportDTO installReportDTO = new InstallReportDTO();
        installReportDTO.setAppId(appId);
        installReportDTO.setPartnerId(partnerId);
        installReportDTO.setOs(os);
        installReportDTO.setDeviceName(device);
        installReportDTO.setGaid(google_aid);

        String agent = request.getHeader("User-Agent");
        installReportDTO.setAgent(agent);

        String ip = ipRequestService.getClientIp(request);
        installReportDTO.setIp(ip);

        jsonInstallReportKafkaProducer.sendInstallReport(installReportDTO);

        model.put("response Imp.", "Saved User Install Report");
        return ResponseEntity.ok().body(model);

    }


}
