package com.attribution.panel.controller;

import com.attribution.panel.dto.DailyActiveUsersDTO;
import com.attribution.panel.kafka.JsonDailyActiveUsersKafkaProducer;
import com.attribution.panel.service.IpRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

@Controller
@RequestMapping("/dailyActiveUsers")
public class DailyActiveUsersController {

    @Autowired
    IpRequestService ipRequestService;

    @Autowired
    JsonDailyActiveUsersKafkaProducer jsonDailyActiveUsersKafkaProducer;

    // http://localhost:8080/dailyActiveUsers/saveDAU?appID=1&partnerId=2&os={os}&device={device}&google_aid={google_aid}

    @GetMapping(value = "/saveDAU")
    public ResponseEntity<?> saveData(@RequestParam(value = "appID") Long appID,
                                      @RequestParam(value = "partnerId") Long partnerId,
                                      @RequestParam(value = "os") String os,
                                      @RequestParam(value = "device") String device,
                                      @RequestParam(value = "google_aid") String google_aid,
                                      HttpServletRequest request, Map<String, Object> model) throws Exception {
        System.out.println("google_aid  = " + google_aid + "device = " + device + "os = " + os + "appId = " + appID);

        String date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").format(new Date());
        System.out.println("date is " + date);

        DailyActiveUsersDTO dailyActiveUsersDTO = new DailyActiveUsersDTO();
        dailyActiveUsersDTO.setAppID(appID);
        dailyActiveUsersDTO.setPartnerId(partnerId);
        dailyActiveUsersDTO.setOs(os);
        dailyActiveUsersDTO.setDeviceName(device);
        dailyActiveUsersDTO.setGaid(google_aid);
        dailyActiveUsersDTO.setCreationDateTime(date);

//        String agent = request.getHeader("User-Agent");
//        dailyActiveUsers.setAgent(agent);

        String ip = ipRequestService.getClientIp(request);
        dailyActiveUsersDTO.setIp(ip);

        jsonDailyActiveUsersKafkaProducer.sendDailyActiveUsersReport(dailyActiveUsersDTO);

        return ResponseEntity.ok().body("Save Daily Active User");
    }

}
