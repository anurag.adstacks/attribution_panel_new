package com.attribution.panel.kafka;


import com.attribution.panel.dao.KafkaProducerProperties;
import com.attribution.panel.dto.AttributionClickTrackingDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class JsonClickKafkaProducer {

//    @Value("${spring.kafka.topic-json.name}")
//    private String topicJsonName;

    @Autowired
    KafkaProducerProperties kafkaProducerProperties;

    private static final Logger LOGGER = LoggerFactory.getLogger(JsonClickKafkaProducer.class);

    Map<String, Object> map = null;

    private KafkaTemplate<String, AttributionClickTrackingDTO> kafkaTemplate;

//    private KafkaTemplate<String, Object> kafkaTemplateConv;


    public JsonClickKafkaProducer(KafkaTemplate<String, AttributionClickTrackingDTO> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }



    public void sendMessageClick(AttributionClickTrackingDTO attributionClickTrackingDTO){

        String topicName = kafkaProducerProperties.getTopic();

        LOGGER.info(String.format("Message sent -> %s", attributionClickTrackingDTO.toString()));

        /*Message<AttributionClickTrackingDTO> message = MessageBuilder
                .withPayload(attributionClickTrackingDTO)
                .setHeader(KafkaHeaders.TOPIC, "click")
                .build();*/

        this.kafkaTemplate.send(topicName, attributionClickTrackingDTO);
        LOGGER.info(String.format("Dome Send"));


//        kafkaTemplate.send(message);

    }



//    public void sendMessageConversion(ConversionMapDTO conversionMapDTO){
//
//        String topicName = kafkaProducerProperties.getTopic2();
//
////        LOGGER.info(String.format("Message sent -> %s", map.toString()));
//
//        /*Message<AttributionClickTrackingDTO> message = MessageBuilder
//                .withPayload(attributionClickTrackingDTO)
//                .setHeader(KafkaHeaders.TOPIC, "click")
//                .build();*/
//
//        this.kafkaTemplate.send(topicName, conversionMapDTO);
//        LOGGER.info(String.format("Dome Send"));
//
//
////        kafkaTemplate.send(message);
//
//    }

}
