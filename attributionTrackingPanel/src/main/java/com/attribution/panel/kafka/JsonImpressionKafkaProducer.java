package com.attribution.panel.kafka;

import com.attribution.panel.dao.KafkaProducerProperties;
import com.attribution.panel.dto.ImpressionTrackingDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class JsonImpressionKafkaProducer {

    @Autowired
    KafkaProducerProperties kafkaProducerProperties;

    private static final Logger LOGGER = LoggerFactory.getLogger(JsonImpressionKafkaProducer.class);

    Map<String, Object> map = null;

    private KafkaTemplate<String, ImpressionTrackingDTO> kafkaImpressionTemplate;

//    private KafkaTemplate<String, Object> kafkaTemplateConv;


    public JsonImpressionKafkaProducer(KafkaTemplate<String, ImpressionTrackingDTO> kafkaImpressionTemplate) {
        this.kafkaImpressionTemplate = kafkaImpressionTemplate;
    }


    public void sendImpressionReport(ImpressionTrackingDTO impressionTrackingDTO) {

        String topicName = kafkaProducerProperties.getTopic4();

        LOGGER.info(String.format("Message sent -> %s", impressionTrackingDTO.toString()));

        /*Message<ImpressionTrackingDTO> message = MessageBuilder
                .withPayload(impressionTrackingDTO)
                .setHeader(KafkaHeaders.TOPIC, "click")
                .build();*/

        this.kafkaImpressionTemplate.send(topicName, impressionTrackingDTO);
        LOGGER.info(String.format("Dome Send"));


//        kafkaTemplate.send(message);

    }

}
