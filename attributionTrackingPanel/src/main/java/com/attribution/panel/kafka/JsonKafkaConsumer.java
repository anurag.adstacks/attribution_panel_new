//package com.attribution.panel.kafka;
//
//import com.attribution.panel.dto.AttributionClickTrackingDTO;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.kafka.annotation.KafkaListener;
//import org.springframework.stereotype.Service;
//
//@Service
//public class JsonKafkaConsumer {
//    private static final Logger LOGGER = LoggerFactory.getLogger(JsonKafkaConsumer.class);
//
//    @KafkaListener(topics = "${spring.kafka.topic-json.name}", groupId = "${spring.kafka.consumer.group-id}")
//    public void consume(AttributionClickTrackingDTO attributionClickTrackingDTO){
//        System.out.println("Json message recieved " + attributionClickTrackingDTO.getAgent());
//        LOGGER.info(String.format("Json message recieved -> %s", attributionClickTrackingDTO.toString()));
//    }
//}