package com.attribution.panel.kafka;


import com.attribution.panel.dao.KafkaProducerProperties;
import com.attribution.panel.dto.InstallReportDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class JsonInstallReportKafkaProducer {


    @Autowired
    KafkaProducerProperties kafkaProducerProperties;

    private static final Logger LOGGER = LoggerFactory.getLogger(JsonInstallReportKafkaProducer.class);

    Map<String, Object> map = null;

    private KafkaTemplate<String, InstallReportDTO> kafkaTemplate;

//    private KafkaTemplate<String, Object> kafkaTemplateConv;


    public JsonInstallReportKafkaProducer(KafkaTemplate<String, InstallReportDTO> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }


    public void sendInstallReport(InstallReportDTO installReportDTO) {

        String topicName = kafkaProducerProperties.getTopic5();

        LOGGER.info(String.format("Message sent -> %s", installReportDTO.toString()));

        /*Message<InstallReportDTO> message = MessageBuilder
                .withPayload(installReport)
                .setHeader(KafkaHeaders.TOPIC, "click")
                .build();*/

        this.kafkaTemplate.send(topicName, installReportDTO);
        LOGGER.info(String.format("Dome Send"));


//        kafkaTemplate.send(message);

    }


}
