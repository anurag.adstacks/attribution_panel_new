package com.attribution.panel.kafka;


import com.attribution.panel.dao.KafkaProducerProperties;
import com.attribution.panel.dto.DailyActiveUsersDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class JsonDailyActiveUsersKafkaProducer {

    @Autowired
    KafkaProducerProperties kafkaProducerProperties;

    private static final Logger LOGGER = LoggerFactory.getLogger(JsonDailyActiveUsersKafkaProducer.class);

    Map<String, Object> map = null;

    private KafkaTemplate<String, DailyActiveUsersDTO> kafkaDAUTemplate;

//    private KafkaTemplate<String, Object> kafkaTemplateConv;


    public JsonDailyActiveUsersKafkaProducer(KafkaTemplate<String, DailyActiveUsersDTO> kafkaDAUTemplate) {
        this.kafkaDAUTemplate = kafkaDAUTemplate;
    }


    public void sendDailyActiveUsersReport(DailyActiveUsersDTO dailyUsers) {

        String topicName = kafkaProducerProperties.getTopic6();

        LOGGER.info(String.format("Message sent -> %s", dailyUsers.toString()));

        /*Message<DailyActiveUsersDTO> message = MessageBuilder
                .withPayload(dailyUsers)
                .setHeader(KafkaHeaders.TOPIC, "click")
                .build();*/

        this.kafkaDAUTemplate.send(topicName, dailyUsers);
        LOGGER.info(String.format("Dome Send"));


//        kafkaTemplate.send(message);

    }

}
