package com.attribution.panel.kafka;


import com.attribution.panel.dao.KafkaProducerProperties;
import com.attribution.panel.dto.AttributionAllReportTrackingDTO;
import com.attribution.panel.dto.AttributionClickTrackingDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class JsonReportKafkaProducer {

    @Autowired
    KafkaProducerProperties kafkaProducerProperties;

    private static final Logger LOGGER = LoggerFactory.getLogger(JsonReportKafkaProducer.class);

    Map<String, Object> map = null;

    private KafkaTemplate<String, AttributionAllReportTrackingDTO> kafkaReportTemplate;

//    private KafkaTemplate<String, Object> kafkaTemplateConv;


    public JsonReportKafkaProducer(KafkaTemplate<String, AttributionAllReportTrackingDTO> kafkaReportTemplate) {
        this.kafkaReportTemplate = kafkaReportTemplate;
    }


    public void sendMessageReport(AttributionAllReportTrackingDTO attributionAllReportTrackingDTO) {

        String topicName = kafkaProducerProperties.getTopic3();

        LOGGER.info(String.format("Message sent -> %s", attributionAllReportTrackingDTO.toString()));

        /*Message<AttributionAllReportTrackingDTO> message = MessageBuilder
                .withPayload(attributionAllReportTrackingDTO)
                .setHeader(KafkaHeaders.TOPIC, "click")
                .build();*/

        this.kafkaReportTemplate.send(topicName, attributionAllReportTrackingDTO);
        LOGGER.info(String.format("Dome Send"));


//        kafkaTemplate.send(message);

    }


}
