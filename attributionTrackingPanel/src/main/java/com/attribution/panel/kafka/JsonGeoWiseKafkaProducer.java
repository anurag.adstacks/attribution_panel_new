package com.attribution.panel.kafka;

import com.attribution.panel.dao.KafkaProducerProperties;
import com.attribution.panel.dto.AttributionClickTrackingDTO;
import com.attribution.panel.dto.InstallGeoWise;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class JsonGeoWiseKafkaProducer {

    @Autowired
    KafkaProducerProperties kafkaProducerProperties;

    private static final Logger LOGGER = LoggerFactory.getLogger(JsonGeoWiseKafkaProducer.class);

    Map<String, Object> map = null;

    private KafkaTemplate<String, InstallGeoWise> kafkaTemplate;

//    private KafkaTemplate<String, Object> kafkaTemplateConv;


    public JsonGeoWiseKafkaProducer(KafkaTemplate<String, InstallGeoWise> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }



    public void sendMessageGeoWise(InstallGeoWise installGeoWise){

        String topicName = kafkaProducerProperties.getTopic7();

        LOGGER.info(String.format("Message sent -> %s", installGeoWise.toString()));

        /*Message<InstallGeoWise> message = MessageBuilder
                .withPayload(installGeoWise)
                .setHeader(KafkaHeaders.TOPIC, "click")
                .build();*/

        this.kafkaTemplate.send(topicName, installGeoWise);
        LOGGER.info(String.format("Dome Send"));


//        kafkaTemplate.send(message);

    }

}
