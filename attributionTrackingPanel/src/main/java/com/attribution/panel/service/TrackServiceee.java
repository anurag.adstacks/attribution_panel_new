//package com.etreetech.panel.service;
//
//import au.com.flyingkite.mobiledetect.UAgentInfo;
//import com.etreetech.panel.cache.dto.*;
//import com.etreetech.panel.cache.repository.*;
//import com.etreetech.panel.constants.BackendConstants;
//import com.etreetech.panel.dto.AttributionClickTrackingDTO;
//import com.etreetech.panel.enums.*;
//import lombok.extern.slf4j.Slf4j;
//import nl.basjes.shaded.com.google.common.collect.ImmutableMap;
//import org.springframework.amqp.rabbit.core.RabbitTemplate;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.core.env.Environment;
//import org.springframework.data.redis.core.RedisTemplate;
//import org.springframework.stereotype.Service;
//
//import javax.servlet.http.HttpServletRequest;
//import java.time.Duration;
//import java.time.LocalDateTime;
//import java.util.List;
//import java.util.Map;
//
//@Service
//@Slf4j
//public class TrackServiceee {
//
//    @Autowired
//    private RabbitTemplate template;
//    @Autowired
//    RedisTemplate redisTemplate;
//    @Autowired
//    PartnerTrackingDTORepository publisherTrackingDTORepository;
//    @Autowired
//    AppTrackingDTORepository offerTrackingDTORepository;
//    @Autowired
//    CampaignTrackingDTORepository campaignTrackingDTORepository;
//    @Autowired
//    CompanyTrackingDTORepository advertiserTrackingDTORepository;
//    @Autowired
//    CapCacheDTORepository capCacheDTORepository;
//    @Autowired
//    BackendConstants backendConstants;
//    @Autowired
//    Environment environment;
//    @Autowired
//    BusinessRuleService businessRuleService;
//    @Autowired
//    BlockSubAffiliateRepository blockSubAffiliateRepository;
//    @Autowired
//    IpRequestService ipRequestService;
//
//    public String getEnvironment() {
//        String[] activeProfiles = environment.getActiveProfiles();
//        return activeProfiles[0];
//    }
//
//    public String tracking(Long offerId, Long affId, String clickId, String gaid, String idfa, String subAff, String source,
//                           String s1, String s2, String s3, String s4, String s5, HttpServletRequest request) throws Exception {
//        LocalDateTime initRequestTime = LocalDateTime.now();
//        CompanyTrackingDTO advertiserTrackingDTO = null;
//        AppTrackingDTO offerTrackingDTO = null;
//        CampaignTrackingDTO campaignTrackingDTO = null;
//        PartnerTrackingDTO publisherTrackingDTO = null;
//        LocalDateTime completedRequestTime = null;
//        long durationInMillis = 0;
//        Map<String, Object> map = null;
//        AttributionClickTrackingDTO click = null;
//        String id = null;
//        Long advertiserId = null;
//        String agent = null;
//        String ip = null;
//        CountryEnum country = null;
//
//        //fetch offer from cache
//        try {
//            offerTrackingDTO = offerTrackingDTORepository.findById(offerId).orElse(null);
//            if (offerTrackingDTO == null) {
//                throw new Exception("Offer Not Found!");
//            }
//        } catch (Exception e) {
//            throw new Exception("Offer Not Found!");
//        }
//
//        //fetch publisher from cache
//        try {
//            publisherTrackingDTO = publisherTrackingDTORepository.findById(affId).orElse(null);
//            if (publisherTrackingDTO == null) {
//                throw new Exception("Publisher Not Found!");
//            }
//        } catch (Exception e) {
//            throw new Exception("Publisher Not Found!");
//        }
//
//        //fetch campaign from cache
//        id = "offerId:" + offerId + "pubId:" + affId;
//        campaignTrackingDTO = campaignTrackingDTORepository.findById(id).orElse(null);
//
//        //fetch advertiser id from offer and then fetch advertiser from cache
//        advertiserId = offerTrackingDTO.getAdvertiserId();
//        try {
//            advertiserTrackingDTO = advertiserTrackingDTORepository.findById(advertiserId).orElse(null);
//            if (advertiserTrackingDTO == null) {
//                throw new Exception("Advertiser Not Found!");
//            }
//        } catch (Exception ex) {
//            throw new Exception("Advertiser Not Found!");
//        }
//
//        String url = offerTrackingDTO.getTrackingUrl();
//
//        //Fetching user agent,ip and country from header only in production environment.
//        if (getEnvironment().equals("test"))
//            agent = request.getHeader("User-Agent");
//        else if (getEnvironment().equals("prod")) {
//            agent = request.getHeader("User-Agent");
//            ip=ipRequestService.getClientIp(request);
//            try {
//                country = CountryEnum.valueOf(request.getHeader("CF-IPCountry"));
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//        //Creating a new click entity from above parameters.
//        click = new AttributionClickTrackingDTO(clickId, gaid, idfa, agent, subAff, s1, s2, s3, s4, s5, source, publisherTrackingDTO,
//                offerTrackingDTO, advertiserId, true, ip, country);
//
//        //Gross Check
//        click = isGross(click, campaignTrackingDTO, advertiserTrackingDTO, offerTrackingDTO, publisherTrackingDTO);
//        //If gross is true redirect user to offer tracking url, else stop redirection and redirect to base url.
//        if (click.isGross()) {
//            url = transformTrackingUrl(url, click);
//        } else {
//            url = backendConstants.baseUrl;
//        }
//        completedRequestTime = LocalDateTime.now();
//        //Calculate total duration of time in completing a request.
//        durationInMillis = Duration.between(initRequestTime, completedRequestTime).toMillis();
//        click.setDurationInMillis(durationInMillis);
//
//        //Sending click entity to rabbbitmq through map.
//        map = ImmutableMap.of("click", click);
//        template.convertAndSend
//                ("app1-exchange", "Click-routing-key", map);
//
//        //Setting all variables to null for faster garbage collection
//        initRequestTime = null;
//        advertiserTrackingDTO = null;
//        offerTrackingDTO = null;
//        campaignTrackingDTO = null;
//        publisherTrackingDTO = null;
//        completedRequestTime = null;
//        durationInMillis = 0;
//        map = null;
//        click = null;
//        id = null;
//        advertiserId = null;
//        agent = null;
//        ip = null;
//        country = null;
//        return url;
//    }
//
//    //Method to apply various rules on click and decide whether click is approved and user should be redirected to offerurl or not.
//    private AttributionClickTrackingDTO isGross(AttributionClickTrackingDTO click, CampaignTrackingDTO campaign, CompanyTrackingDTO advertiser,
//                                     AppTrackingDTO offer, PartnerTrackingDTO publisher) {
//        // CAPS
//        CapsStatus capsStatus = CapsStatus.APPROVED;
//        boolean isGross = true;
//        String passed = "Passed: ";
//        String rejected = "Rejected: ";
//        click.setClickMessage(passed + capsStatus.name());
//
////        If advertiser is disabled
//        isGross = checkAdvertiserActive(click, advertiser, true);
//
////        If publisher is disabled/inactive
//        if (isGross) {
//            isGross = checkPublisherActive(click, publisher, true);
//        }
//
//        // If advertiser has not approved publisher
//        if (isGross) {
//            isGross = checkPublisherApprovedByAdvertiser(click, advertiser, publisher, true);
//        }
//
////        Only active offers are passed
//        if (isGross) {
//            isGross = checkOfferActive(click, offer, true);
//        }
//
////      if non-public campaign is null or blocked
//        if (isGross) {
//            isGross = checkNonPublicCampaignIsNullOrBlocked(click, campaign, true, offer);
//        }
//
//        // check IP location match with offer allowed locations
//        if (isGross) {
//            isGross = checkCountry(click, offer, true);
//        }
//        // check user agent OS match with offer allowed OS
//        if (isGross) {
//            isGross = checkOs(click, offer, true, rejected);
//        }
//
//        // RepeatedIP
//        // check recent IP within set time from other clicks for same campaigns
//
//        //Check caps
//        if (isGross) {
//            isGross = checkCaps(offer, publisher, true, click);
//        }
//
//        //subAffRule
//        if (isGross) {
//            isGross = subAffFilter(offer, publisher, true, click);
//        }
//
//        click.setGross(isGross);
//        return click;
//    }
//
//    public boolean checkAdvertiserActive(AttributionClickTrackingDTO click, CompanyTrackingDTO advertiser, boolean isGross) {
//        if (advertiser.getStatus() != null && !advertiser.getStatus()) {
//            isGross = false;
//            click.setClickMessage("Rejected: Advertiser status Inactive");
//        }
//        return isGross;
//    }
//
//    public boolean checkPublisherActive(AttributionClickTrackingDTO click, PartnerTrackingDTO publisher,
//                                        boolean isGross) {
//        if (publisher.getStatus() != null && !publisher.getStatus()) {
//            isGross = false;
//            click.setClickMessage("Rejected: Publisher status Inactive");
//        }
//        return isGross;
//    }
//
//    public boolean checkPublisherApprovedByAdvertiser(AttributionClickTrackingDTO click, CompanyTrackingDTO advertiser,
//                                                      PartnerTrackingDTO publisher, boolean isGross) {
//        if (advertiser.getApprovedPubs() == null || !advertiser.getApprovedPubs().contains(publisher.getId())) {
//            isGross = false;
//            click.setClickMessage("Rejected: Publisher not Approved By Advertiser");
//        }
//        return isGross;
//    }
//
//    public boolean checkOfferActive(AttributionClickTrackingDTO click, AppTrackingDTO offer, boolean isGross) {
//        if (offer.getStatus() != OfferStatus.Active) {
//            isGross = false;
//            click.setClickMessage("Rejected: Offer " + offer.getStatus());
//        }
//        return isGross;
//    }
//
//    public boolean checkNonPublicCampaignIsNullOrBlocked(AttributionClickTrackingDTO click, CampaignTrackingDTO campaign, boolean isGross,
//                                                         AppTrackingDTO offer) {
//        if (offer.getAccessStatus() != AccessStatus.Public && campaign == null) {
//            isGross = false;
//            click.setClickMessage("Rejected: Campaign not approved");
//        } else if (campaign != null && campaign.getCampaignStatus() != CampaignStatus.APPROVED) {
//            click.setClickMessage("Rejected: Campaign Status " + campaign.getCampaignStatus());
//            isGross = false;
//        }
//        return isGross;
//    }
//
//    public boolean checkCountry(AttributionClickTrackingDTO click, AppTrackingDTO offer, boolean isGross) {
//        if (offer.getCountryEnum() != null &&
//                !offer.getCountryEnum().contains(CountryEnum.ALL) && !offer.getCountryEnum().contains(click.getCountry())) {
//            isGross = false;
//            click.setClickMessage("Rejected: Geo blocked");
//        }
//        return isGross;
//    }
//
//    public boolean checkOs(AttributionClickTrackingDTO click, AppTrackingDTO offer, boolean isGross, String rejected) {
//        if (click.getAgent() != null) {
//            try {
//                String agent = click.getAgent().toLowerCase();
//                // setup the class with the user agent
//                UAgentInfo agentInfo = new UAgentInfo(agent, null);
//                // check if mobile device
//                boolean isMobileDevice = agentInfo.detectMobileQuick();
//                //or check if tablet
//                boolean isTabletDevice = agentInfo.detectTierTablet();
////            String agent = "Mozilla/5.0 (iPhone; CPU iPhone OS 13_7 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148";
////            String agent = "Mozilla/5.0 (Linux; Android 10; POCO X2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.101 Mobile Safari/537.36";
//                String offerType = (offer.getOfferType() != null) ? offer.getOfferType().toLowerCase() : "";
//                String devOs = (offer.getDevOS() != null) ? offer.getDevOS().toLowerCase() : null;
////            assert offerType != null;
//                switch (offerType) {
//                    case "mobile,desktop":
//                    case "all":
//                        if (isMobileDevice || isTabletDevice) {
////                        assert devOs != null;
//                            if (devOs.equals("android,ios") || devOs.equals("all")) {
//                                if (!agent.contains("iphone") && !agent.contains("android")) {
//                                    isGross = false;
//                                    click.setClickMessage(rejected + "OS blocked");
//                                }
//                            } else {
//                                if (devOs.equals("ios"))
//                                    devOs = "iphone";
//                                int index = agent.indexOf(devOs);
//                                if (index < 0) {
//                                    isGross = false;
//                                    click.setClickMessage(rejected + "OS blocked");
//                                }
//                            }
//                        }
//                        break;
//                    case "desktop":
//                        if (isMobileDevice || isTabletDevice) {
//                            isGross = false;
//                            click.setClickMessage(rejected + "OS blocked");
//                        }
//                        break;
//                    case "mobile":
////                    assert devOs != null;
//                        if (devOs.equals("android,ios") || devOs.equals("all")) {
//                            if (!agent.contains("iphone") && !agent.contains("android")) {
//                                isGross = false;
//                                click.setClickMessage(rejected + "OS blocked");
//                            }
//                        } else {
//                            if (devOs.contains("ios"))
//                                devOs = "iphone";
//                            int index = agent.indexOf(devOs);
//                            if (index < 0) {
//                                isGross = false;
//                                click.setClickMessage(rejected + "OS blocked");
//                            }
//                        }
//                        break;
//                    default:
//                        break;
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//        return isGross;
//    }
//
//    //Fetching list of caps according to offerId if any cap is used then match if the cap has current offer and publisher
//    public boolean checkCaps(AppTrackingDTO offer, PartnerTrackingDTO publisher, boolean isGross, AttributionClickTrackingDTO click) {
//        List<CapCacheDTO> capCacheDTOList = capCacheDTORepository.findByOfferId(offer.getId());
//        for (CapCacheDTO capCacheDTO : capCacheDTOList) {
//            if (capCacheDTO.isUsed()) {
//                if (capCacheDTO.getPublishers() == null || (capCacheDTO.getPublishers() != null && capCacheDTO.getPublishers()
//                        .contains(publisher.getId()))) {
//                    isGross = false;
//                    click.setClickMessage("Rejected: " + capCacheDTO.getCapsType() + " cap expired");
//                    break;
//                }
//            }
//        }
//        return isGross;
//    }
//
//    //Method to replace parameters according to our panel in redirecting url.
//    public String transformTrackingUrl(String url, AttributionClickTrackingDTO click) {
//        url = url.replaceAll("\\{CLICK_ID\\}", click.getId());
//        if (click.getGaid().contains("{") || click.getGaid().contains("}"))
//            click.setGaid("");
//        url = url.replaceAll("\\{DEVICE_ID\\}", click.getGaid())
//                .replaceAll("\\{AFFID\\}", click.getPubId().toString())
//                .replaceAll("\\{SUB_AFFID\\}", click.getSubAff())
//                .replaceAll("\\{SOURCE\\}", click.getSource())
//                .replaceAll("\\{S1\\}", click.getS1())
//                .replaceAll("\\{S2\\}", click.getS2())
//                .replaceAll("\\{S3\\}", click.getS3())
//                .replaceAll("\\{S4\\}", click.getS4())
//                .replaceAll("\\{S5\\}", click.getS5());
//        while (url.contains("{")) {
//            url = url.substring(0, url.indexOf("{")) + url.substring(url.indexOf("}") + 1);
//        }
//        return url;
//    }
//
//    //SubAff filter to block clicks from particular subAffiliates
//    public boolean subAffFilter(AppTrackingDTO offer, PartnerTrackingDTO publisherTrackingDTO, boolean isGross,
//                                AttributionClickTrackingDTO click) {
//        try {
//            String id = "offerId:" + offer.getId() + "pubId:" + publisherTrackingDTO.getId() + "subAff:" + click.getSubAff();
//            BlockSubAffiliate blockSubAffiliate = blockSubAffiliateRepository.findById(id).orElse(null);
//            if (blockSubAffiliate != null) {
//                isGross = false;
//                click.setClickMessage("Rejected: Sub affiliate blocked");
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return isGross;
//    }
//
//}
