package com.attribution.panel.service;


import com.attribution.panel.cache.dto.AppTrackingDTO;
import com.attribution.panel.cache.dto.EventCacheDTO;
import com.attribution.panel.cache.repository.AppTrackingDTORepository;
import com.attribution.panel.cache.repository.EventCacheDTORepository;
import com.attribution.panel.cache.repository.PartnerTrackingDTORepository;
import com.attribution.panel.dto.AttributionAllReportTrackingDTO;
import com.attribution.panel.dto.AttributionClickTrackingDTO;
import com.attribution.panel.dto.AttributionConversionTrackingDTO;
import com.attribution.panel.dto.ImpressionTrackingDTO;
import com.attribution.panel.kafka.JsonReportKafkaProducer;
import lombok.extern.slf4j.Slf4j;
import nl.basjes.parse.useragent.UserAgent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Map;

@Service
@Slf4j
@Transactional
public class AttributionAllReportService {

//    @Autowired
//    JsonKafkaConsumer jsonKafkaConsumer;

    @Autowired
    Environment environment;

//    @Autowired
//    KafkaProducer kafkaProducer;

    @Autowired
    IpRequestService ipRequestService;

    @Autowired
    AppTrackingDTORepository appRepository;

    @Autowired
    PartnerTrackingDTORepository partnerRepository;

    @Autowired
    EventCacheDTORepository eventCacheDTORepository;

    @Autowired
    JsonReportKafkaProducer jsonReportKafkaProducer;

//    @Autowired
//    ConversionRepo conversionRepository;

//    @Autowired
//    AttributionAllReportRepo attributionAllReportRepo;

//    @Autowired
//    AttributionRecordService attributionRecordService;

//    @Autowired
//    AttributionRecordRepo attributionRecordRepo;

//    @Autowired
//    ClickRepo clickRepository;

//    @Autowired
//    ClickService clickService;

//    @Autowired
//    AppService appService;

//    @Autowired
//    EventService eventService;

//    @Autowired
//    EventRepository eventRepository;

//    @Autowired
//    ClickRepo clickRepo;

//    @Autowired
//    ReportQueryBuilder reportQueryBuilder;



    public void saveConversionDailyAllReportRecord(AttributionConversionTrackingDTO conversion, AttributionClickTrackingDTO attributionclick, String dayTimestamp, boolean isClickFound, UserAgent userAgent, Map deviceLocation, ImpressionTrackingDTO impression) {

        AppTrackingDTO app = appRepository.findById(attributionclick.getAppID()).orElse(null);

        EventCacheDTO event = null;
        if (conversion.getId() != null) {
            event = eventCacheDTORepository.findById(conversion.getEventID()).orElse(null);
            System.out.println("apppsps is1 " + event.getEventName());
        }


        System.out.println("apppsps is " + conversion.getEventID());

        System.out.println("apppsps is2 " + app.getName());
//        System.out.println("apppsps is3 " + impression.getAppId());


        try {

            System.out.println("saveConversionDailyRecord1 " + attributionclick.getIp());
            System.out.println("saveConversionDailyRecord " + conversion);

            AttributionAllReportTrackingDTO attributionAllReport = null;

            if (conversion.getId() == null) {
                System.out.println("horlyhourlu");
//                attributionAllReport = attributionAllReportRepo.findByAppIdAndPartnerIdAndDayTimestamp(attributionclick.getAppID(), attributionclick.getPId(), dayTimestamp);
                attributionAllReport = new AttributionAllReportTrackingDTO(conversion, dayTimestamp, attributionclick, app, userAgent, deviceLocation, event);
                saveAttributionAllReportRecord(conversion, attributionAllReport, attributionclick, isClickFound);
                System.out.println("horlyhourlu2" + attributionAllReport);
                System.out.println("horlyhourlu4 " + attributionAllReport);
                /*if (attributionAllReport == null) {
                    attributionAllReport = new AttributionAllReportTrackingDTO(conversion, dayTimestamp, attributionclick, app, userAgent, deviceLocation, event);
                    saveAttributionAllReportRecord(conversion, attributionAllReport, attributionclick, isClickFound);
                    System.out.println("horlyhourlu2" + attributionAllReport);
                } else {
                    saveAttributionAllReportRecord(conversion, attributionAllReport, attributionclick, isClickFound);
                    System.out.println("horlyhourlu3" + attributionAllReport);
                }*/
            } else {
                attributionAllReport = new AttributionAllReportTrackingDTO(conversion, dayTimestamp, attributionclick, app, userAgent, deviceLocation, event);
                saveAttributionAllReportRecord(conversion, attributionAllReport, attributionclick, isClickFound);
                System.out.println("horlyhourlu5" + attributionAllReport);
//                attributionAllReport = attributionAllReportRepo.findByAppIdAndPartnerIdAndDayTimestamp(conversion.getAppId(), conversion.getPartnerId(), dayTimestamp);
                System.out.println("horlyhourlu8 " + attributionAllReport);
                /*if (attributionAllReport == null) {
                    attributionAllReport = new AttributionAllReportTrackingDTO(conversion, dayTimestamp, attributionclick, app, userAgent, deviceLocation, event);
                    saveAttributionAllReportRecord(conversion, attributionAllReport, attributionclick, isClickFound);
                    System.out.println("horlyhourlu5" + attributionAllReport);
                } else {
                    saveAttributionAllReportRecord(conversion, attributionAllReport, attributionclick, isClickFound);
                    System.out.println("horlyhourlu6" + attributionAllReport);
                }
                System.out.println("horlyhourlu7" + attributionAllReport);*/
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*public void saveImpresionDailyAllReportRecord(UserAgent userAgent, Map deviceLocation, Impression impression) {

        App app = appService.findById(impression.getAppId());
        Event event = null;

//        event = eventService.findById(conversion.getEventID());
//        System.out.println("apppsps is1 " + event.getEventName());


//        System.out.println("apppsps is " + conversion.getEventID());

        System.out.println("apppsps is2 " + app.getName());
        System.out.println("apppsps is3 " + impression);

        System.out.println("horlyhourlu");
        AttributionAllReportTrackingDTO attributionAllReport = null;

        attributionAllReport = new AttributionAllReportTrackingDTO(app, userAgent, deviceLocation, impression);
//        saveAttributionAllReportRecord(conversion, attributionAllReport, attributionclick, isClickFound);
        System.out.println("horlyhourlu2" + attributionAllReport);

        save(attributionAllReport);


    }*/



    public void saveAttributionAllReportRecord(AttributionConversionTrackingDTO conversion, AttributionAllReportTrackingDTO attributionAllReport, AttributionClickTrackingDTO attributionclick, boolean isClickFound) {


        String sDate = "Apr 1, 2023";
        String eDate = "Apr 5, 2023";
        Long count = null;
        System.out.println("thisIsSaveConversionDailyRecord0  " + conversion.getAppId() + " " + attributionclick.getPId());
//        Long count = reportQueryBuilder.countByAppIdAndPartnerIdAndCreationDateTimeBetween(attributionclick.getAppID(), attributionclick.getPId(), sDate, eDate );
/*        if (conversion.getId() == null) {
            count = reportQueryBuilder.countByAppIdAndPartnerIdAndCreationDateTimeBetween(attributionclick.getAppID(), attributionclick.getPId());
            System.out.println("thisIsSaveConversionDailyRecord00  " + count);
        } else {
            count = reportQueryBuilder.countByAppIdAndPartnerIdAndCreationDateTimeBetween(attributionclick.getAppID(), conversion.getPartnerId());
            System.out.println("thisIsSaveConversionDailyRecord000  " + count);
        }*/
//
        System.out.println("thisIsSaveConversionDailyRecord0000  " + count);

/*        try {
            if (conversion.getId() == null) {
                System.out.println("thisIsSaveConversionDailyRecord0  " + conversion.getConversionStatus() + attributionAllReport.getApprovedConversions() + attributionAllReport.getClickCount());
                attributionAllReport.setClickCount(attributionAllReport.getClickCount() + 1);
                double ac = attributionAllReport.getApprovedConversions();
                double cc = attributionAllReport.getClickCount();
                double ratio = ac / cc * 100;
                double ctr = cc / count * 100;
                System.out.println("clickcount4  " + ratio);
                attributionAllReport.setRatio(ratio);
                attributionAllReport.setCtr(ctr);
            } else {
                System.out.println("thisIsSaveConversionDailyRecord1  " + conversion.getConversionStatus() + attributionAllReport.getApprovedConversions() + attributionAllReport.getClickCount());
                if(!isClickFound){
                    attributionAllReport.setClickCount(attributionAllReport.getClickCount() + 1);
                }
                double ac = attributionAllReport.getApprovedConversions();
                double cc = attributionAllReport.getClickCount();
                double ratio = ac / cc * 100;
                double ctr = cc / count * 100;
                System.out.println("clickcount4  " + ratio);
                attributionAllReport.setRatio(ratio);
                attributionAllReport.setCtr(ctr);
                System.out.println("clickcount3  " + (attributionAllReport.getApprovedConversions()/attributionAllReport.getClickCount()) * 100);
                switch (conversion.getConversionStatus()) {
                    case APPROVED:
                        attributionAllReport.setApprovedConversions(attributionAllReport.getApprovedConversions() + 1);
//                    attributionAllReport.setApprovedRevenue(attributionAllReport.getApprovedRevenue() + conversion.getEvent().getRevenue());
//                    attributionAllReport.setApprovedPayout(attributionAllReport.getApprovedPayout() + conversion.getEvent().getPayout());
                        System.out.println("hhihihihihihih" + attributionAllReport);
                        break;
                    case PENDING:
                        attributionAllReport.setPendingConversions(attributionAllReport.getPendingConversions() + 1);
//                    attributionAllReport.setPendingRevenue((attributionAllReport.getPendingRevenue() + conversion.getEvent().getRevenue()));
                        System.out.println("hahahaahahaha");
                        break;
                    case CANCELLED:
                        attributionAllReport.setCancelledConversions(attributionAllReport.getCancelledConversions() + 1);
//                    attributionAllReport.setCancelledRevenue(attributionAllReport.getCancelledRevenue() + conversion.getEvent().getRevenue());
                        System.out.println("hahahaahahaha");
                        break;
                }
            }

            System.out.println("clickcount6  " + attributionAllReport);
            attributionAllReportService.save(attributionAllReport);

        } catch (Exception e) {
            e.printStackTrace();
        }*/

        System.out.println("clickcount6  " + attributionAllReport);

        jsonReportKafkaProducer.sendMessageReport(attributionAllReport);

//        save(attributionAllReport);

    }



}
