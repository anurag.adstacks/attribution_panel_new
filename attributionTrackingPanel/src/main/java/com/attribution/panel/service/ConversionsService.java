package com.attribution.panel.service;

import com.attribution.panel.cache.dto.AppTrackingDTO;
import com.attribution.panel.cache.dto.EventCacheDTO;
import com.attribution.panel.cache.repository.AppTrackingDTORepository;
import com.attribution.panel.cache.repository.EventCacheDTORepository;
import com.attribution.panel.dto.ConversionMapDTO;
import com.attribution.panel.enums.CountryEnum;
import com.attribution.panel.kafka.JsonConversionKafkaProducer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;


@Service
@Slf4j
public class ConversionsService {
//    @Autowired
//    private RabbitTemplate template;

    @Autowired
    JsonConversionKafkaProducer jsonConversionKafkaProducer;

    @Autowired
    Environment environment;

    @Autowired
    IpRequestService ipRequestService;

    @Autowired
    EventCacheDTORepository eventCacheDTORepository;

    @Autowired
    AppTrackingDTORepository appTrackingDTORepository;


    public String getEnvironment() {
        String[] activeProfiles = environment.getActiveProfiles();
        return activeProfiles[0];
    }

//    public ResponseEntity<?> getPostbackResponse(String uuid, String token,HttpServletRequest request) {
//        CountryEnum country = CountryEnum.ALL;
//        //Fetching country from header only in production environment.
//        if (getEnvironment().equals("prod")) {
//            try {
//                country = CountryEnum.valueOf(request.getHeader("CF-IPCountry"));
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//
//        Map<String, Object> map = new HashMap<>();
//        map.put("uuid", uuid);
//        map.put("token", token);
//        map.put("date", new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").format(new Date()));
//        map.put("debug", "/pb?click_id=" + uuid + "&token=" + token);
//        map.put("country",country);
//        template.convertSendAndReceive
//                ("app1-exchange", "Conversion-routing-key", map);
//        ResponseEntity<?> response = ResponseEntity.ok().body("Conversion Registered");
//        return response;
//    }


    public Map<String, String> attributionPostback(Long appId, String gaid, String token, String agent, String ip, String s1, boolean isApk, Long pId, HttpServletRequest request) {
        CountryEnum country = null;
        if (getEnvironment().equals("prod")) {
            agent = request.getHeader("User-Agent");
            ip = ipRequestService.getClientIp(request);
            try {
                country = CountryEnum.valueOf(request.getHeader("CF-IPCountry"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        ConversionMapDTO conversionMapDTO = new ConversionMapDTO();
        conversionMapDTO.setAppId(appId);
        conversionMapDTO.setGaid(gaid);
        conversionMapDTO.setToken(token);
        conversionMapDTO.setAgent(agent);
        conversionMapDTO.setIp(ip);
        conversionMapDTO.setS1(s1);
        conversionMapDTO.setApk(isApk);
        conversionMapDTO.setPId(pId);


        String   agentRequest = request.getHeader("User-Agent");
        conversionMapDTO.setAgentRequest(agentRequest);
        System.out.println("agentif " + agent);

        String ipRequest = ipRequestService.getClientIp(request);
        conversionMapDTO.setIpRequest(ipRequest);
        System.out.println("ip is  " + ip);

//        Map<String, Object> map = new HashMap<>();
//        map.put("appId", appId);
//        map.put("token", token);
//        map.put("agent", agent);
//        map.put("gaid", gaid);
//        map.put("ip", ip);
//        map.put("s1", s1);
//        map.put("isApk", isApk);
//        map.put("pId", pId);
//        map.put("country", country);
//        map.put("date", new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").format(new Date()));
//
//        String debug = "/attributionPostback?appId=" + appId + "&pId=" + pId + "&token=" + token + "&gaid=" + gaid + "&agent=" + agent
//                + "&s1=" + s1 + "&s2=" + "&isApk=" + isApk + "&ip=" + ip;
//        map.put("debug", debug);
////        template.convertSendAndReceive("app1-exchange", "AttributionConversion-routing-key", map);
//
//        Gson gson = new Gson();
//        Type gsonType = new TypeToken<HashMap>(){}.getType();
//        String gsonString = gson.toJson(map,gsonType);
//        System.out.println(gsonString);

        List<EventCacheDTO> events = (List<EventCacheDTO>) eventCacheDTORepository.findAll();
        log.info("event List:" + events);
        System.out.println("event " + events);

        List<EventCacheDTO> event = eventCacheDTORepository.findByAppsId(appId);
        log.info("event List:" + event);
        System.out.println("event " + event.size());

        jsonConversionKafkaProducer.sendMessageConversion(conversionMapDTO);

        Map<String,String> responseMap = new HashMap<>();
        responseMap.put("message","Conversion Registered");
        responseMap.put("status","true");
        return responseMap;
    }

}
