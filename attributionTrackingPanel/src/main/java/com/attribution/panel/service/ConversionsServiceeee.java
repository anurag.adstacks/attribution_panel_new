//package com.etreetech.panel.service;
//
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.amqp.rabbit.core.RabbitTemplate;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.core.env.Environment;
//import org.springframework.http.ResponseEntity;
//import org.springframework.stereotype.Service;
//
//import javax.servlet.http.HttpServletRequest;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.Map;
//
//@Service
//@Slf4j
//public class ConversionsServiceeee {
//    @Autowired
//    private RabbitTemplate template;
//
//    @Autowired
//    Environment environment;
//
//    @Autowired
//    IpRequestService ipRequestService;
//
//
//    public String getEnvironment() {
//        String[] activeProfiles = environment.getActiveProfiles();
//        return activeProfiles[0];
//    }
//
//    public ResponseEntity<?> getPostbackResponse(String uuid, String token,HttpServletRequest request) {
//        CountryEnum country = CountryEnum.ALL;
//        //Fetching country from header only in production environment.
//        if (getEnvironment().equals("prod")) {
//            try {
//                country = CountryEnum.valueOf(request.getHeader("CF-IPCountry"));
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//
//        Map<String, Object> map = new HashMap<>();
//        map.put("uuid", uuid);
//        map.put("token", token);
//        map.put("date", new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").format(new Date()));
//        map.put("debug", "/pb?click_id=" + uuid + "&token=" + token);
//        map.put("country",country);
//        template.convertSendAndReceive
//                ("app1-exchange", "Conversion-routing-key", map);
//        ResponseEntity<?> response = ResponseEntity.ok().body("Conversion Registered");
//        return response;
//    }
//
//    public Map<String, String> attributionPostback(Long offerId, String gaid, String token, String userAgent, String ip, String s1, String s2, String s3, String s4, String s5, boolean isApk, Long affid, HttpServletRequest request) {
//        CountryEnum country = null;
//        if (getEnvironment().equals("prod")) {
//            userAgent = request.getHeader("User-Agent");
//            ip = ipRequestService.getClientIp(request);
//            try {
//                country = CountryEnum.valueOf(request.getHeader("CF-IPCountry"));
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//        Map<String, Object> map = new HashMap<>();
//        map.put("offerId", offerId);
//        map.put("token", token);
//        map.put("userAgent", userAgent);
//        map.put("gaid", gaid);
//        map.put("ip", ip);
//        map.put("s1", s1);
//        map.put("s2", s2);
//        map.put("s3", s3);
//        map.put("s4", s4);
//        map.put("s5", s5);
//        map.put("isApk", isApk);
//        map.put("affid", affid);
//        map.put("country", country);
//        map.put("date", new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").format(new Date()));
//        String debug = "/attributionPostback?offerId=" + offerId + "&affid=" + affid + "&token=" + token + "&gaid=" + gaid + "&userAgent=" + userAgent
//                + "&s1=" + s1 + "&s2=" + s2 + "&s3=" + s3 + "&s4=" + s4 + "&s5=" + s5 + "&isApk=" + isApk + "&ip=" + ip;
//        map.put("debug", debug);
//        template.convertSendAndReceive("app1-exchange", "AttributionConversion-routing-key", map);
//        Map<String,String> responseMap = new HashMap<>();
//        responseMap.put("message","Conversion Registered");
//        responseMap.put("status","true");
//        return responseMap;
//    }
//
//}
