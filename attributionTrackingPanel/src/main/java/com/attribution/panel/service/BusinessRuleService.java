//package com.etreetech.panel.service;
//
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.List;
//import java.util.stream.Collectors;
//
//@Service
//@Transactional
//@Slf4j
//public class BusinessRuleService {
//    @Autowired
//    BusinessRuleCacheRepository businessRuleCacheRepository;
//    @Autowired
//    CompanyTrackingDTORepository advertiserTrackingDTORepository;
//
//    public BusinessRuleCacheDTO fetchBusinessRules(StatusRule statusRule,
//                                                   AppTrackingDTO offer, PartnerTrackingDTO publisher) {
////        System.out.println("Inside fetch business rules");
//        BusinessRuleCacheDTO businessRule = null;
//        List<BusinessRuleCacheDTO> businessRules = null;
//        CompanyTrackingDTO advertiser = advertiserTrackingDTORepository.findById(offer.getAdvertiserId()).orElse(null);
//
//        List<BusinessRuleCacheDTO> businessRuleList = (List<BusinessRuleCacheDTO>) businessRuleCacheRepository.findAll();
////        log.info("bsRule:"+businessRuleList);
//
//        businessRules = businessRuleList.stream().filter(bsRule -> bsFilter(bsRule, statusRule, Scope.OFFER, false))
//                .collect(Collectors.toList());
//        for (BusinessRuleCacheDTO businessRuleCacheDTO : businessRules) {
//            if (isRuleApplicable(offer, publisher, businessRuleCacheDTO, Scope.OFFER, null)) {
//                return businessRuleCacheDTO;
//            }
//        }
//
//        businessRules = businessRuleList.stream().filter(bsRule -> bsFilter(bsRule, statusRule, Scope.ADVERTISER, false)).
//                collect(Collectors.toList());
//
//        for (BusinessRuleCacheDTO businessRuleCacheDTO : businessRules) {
//            if (isRuleApplicable(null, publisher, businessRuleCacheDTO, Scope.ADVERTISER, advertiser)) {
//                return businessRuleCacheDTO;
//            }
//        }
//
//        businessRules = businessRuleList.stream().filter(bsRule -> bsFilter(bsRule, statusRule, Scope.ALL, false)).
//                collect(Collectors.toList());
//
//        for (BusinessRuleCacheDTO businessRuleCacheDTO : businessRules) {
//            if (isRuleApplicable(null, publisher, businessRuleCacheDTO, Scope.ALL, null)) {
//                return businessRuleCacheDTO;
//            }
//        }
//
//        businessRules = businessRuleList.stream().filter(bsRule -> bsFilter(bsRule, statusRule, Scope.OFFER, true)).
//                collect(Collectors.toList());
//
//        for (BusinessRuleCacheDTO businessRuleCacheDTO : businessRules) {
//            if (isRuleApplicable(offer, null, businessRuleCacheDTO, Scope.OFFER, null)) {
//                return businessRuleCacheDTO;
//            }
//        }
//
//        businessRules = businessRuleList.stream().filter(bsRule -> bsFilter(bsRule, statusRule, Scope.ADVERTISER, true)).
//                collect(Collectors.toList());
//
//        for (BusinessRuleCacheDTO businessRuleCacheDTO : businessRules) {
//            if (isRuleApplicable(null, null, businessRuleCacheDTO, Scope.ADVERTISER, advertiser)) {
//                return businessRuleCacheDTO;
//            }
//        }
//
//        businessRules = businessRuleList.stream().filter(bsRule -> bsFilter(bsRule, statusRule, Scope.ALL, true)).
//                collect(Collectors.toList());
//
//        for (BusinessRuleCacheDTO businessRuleCacheDTO : businessRules) {
//            if (isRuleApplicable(null, null, businessRuleCacheDTO, Scope.ALL, null)) {
//                return businessRuleCacheDTO;
//            }
//        }
//
//        return businessRule;
//    }
//
//    public boolean isRuleApplicable(AppTrackingDTO offerTrackingDTO, PartnerTrackingDTO publisherTrackingDTO,
//                                    BusinessRuleCacheDTO businessRuleCacheDTO, Scope scope, CompanyTrackingDTO advertiserTrackingDTO) {
//        boolean isRuleApplicable = false;
//        try {
//            switch (scope) {
//                case OFFER:
//                    if ((businessRuleCacheDTO.getOffer().contains(offerTrackingDTO.getId()))
//                            && (publisherTrackingDTO == null || businessRuleCacheDTO.getPub().contains(publisherTrackingDTO.getId()))) {
//                        isRuleApplicable = true;
//                    }
//                    break;
//                case ADVERTISER:
//                    if (businessRuleCacheDTO.getAdvert().contains(advertiserTrackingDTO.getId())
//                            && (publisherTrackingDTO == null || businessRuleCacheDTO.getPub().contains(publisherTrackingDTO.getId()))) {
//                        isRuleApplicable = true;
//                    }
//                    break;
//                case ALL:
//                    if (publisherTrackingDTO == null || businessRuleCacheDTO.getPub().contains(publisherTrackingDTO.getId())) {
//                        isRuleApplicable = true;
//                    }
//                    break;
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return isRuleApplicable;
//    }
//
//    public Boolean bsFilter(BusinessRuleCacheDTO businessRule, StatusRule statusRule, Scope scope,
//                            Boolean isAllPub) {
//        boolean toIncludeRule = false;
//        try {
//            toIncludeRule = businessRule.getStatusRule().equals(statusRule)
//                    && businessRule.getScope().equals(scope)
//                    && businessRule.isAllPublishers() == isAllPub;
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return toIncludeRule;
//    }
//
//    public int getNum(){
//        return 5;
//    }
//}
