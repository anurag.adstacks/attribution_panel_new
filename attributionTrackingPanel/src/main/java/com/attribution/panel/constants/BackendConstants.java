package com.attribution.panel.constants;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public final class BackendConstants {

    final public String baseUrl = "http://localhost:9098";
    final public String baseUrlDbAccess = "http://localhost:9099/adminConsole";

//    @Autowired
//    public BackendConstants(
//            @Value("${baseUrl}") String baseUrl,
//            @Value("${baseUrlDbAccess}") String baseUrlDbAccess
//    ) {
//        this.baseUrl = baseUrl;
//        this.baseUrlDbAccess = baseUrlDbAccess;
//    }

}
