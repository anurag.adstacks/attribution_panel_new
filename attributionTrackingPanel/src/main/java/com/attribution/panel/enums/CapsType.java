package com.attribution.panel.enums;

public enum CapsType {
    GrossConversions, Payout, GrossRevenue, GrossClicks, PendingRevenue, Event, GrossEvent, ApprovedEvent, ApprovedConversions, ApprovedClicks

}
