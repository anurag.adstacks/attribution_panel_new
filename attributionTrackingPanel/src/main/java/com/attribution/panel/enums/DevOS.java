package com.attribution.panel.enums;

/**
 * Revenue Model Enum.
 */

public enum DevOS {
	Android,
	iOS,
	Windows,
	All
}
