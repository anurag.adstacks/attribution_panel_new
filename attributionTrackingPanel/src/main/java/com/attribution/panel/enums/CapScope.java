package com.attribution.panel.enums;

public enum CapScope {
    EACH, GROUP
}
