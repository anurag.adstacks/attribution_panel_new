package com.attribution.panel.enums;

public enum CampaignStatus {
    BLOCKED,
    APPROVED,
    PENDING
}