package com.attribution.panel.enums;

public enum CurrencyEnum {
    Dollar,
    Euro,
    Pound
}
