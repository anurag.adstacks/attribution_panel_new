package com.attribution.panel.enums;

public enum RevenueModel {
    RPA,
    RPI,
    RPC,
    RPL,
    RPS,
    RPAS
}
