package com.attribution.panel.enums;

public enum OfferStatus {
	Active,
	Pause,
	Archived,
	Pending

}
