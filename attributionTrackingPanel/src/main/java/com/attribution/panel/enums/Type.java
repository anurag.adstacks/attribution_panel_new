package com.attribution.panel.enums;

public enum Type {

	Mobile,
	Desktop,
	Tablet,
	All
}
