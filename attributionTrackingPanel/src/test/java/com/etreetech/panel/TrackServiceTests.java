//package com.etreetech.panel;
//
//import com.etreetech.panel.cache.dto.*;
//import com.etreetech.panel.dto.ClickTrackingDTO;
//import com.etreetech.panel.enums.*;
//import com.etreetech.panel.service.BusinessRuleService;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.junit.MockitoJUnitRunner;
//
//import java.util.Collections;
//
//import static org.hamcrest.Matchers.equalTo;
//import static org.junit.Assert.assertThat;
//import static org.mockito.Mockito.when;
//
//@RunWith(MockitoJUnitRunner.class)
//public class TrackServiceTests {
//
//    @Mock
//    BusinessRuleService businessRuleService;
//    @Mock
//    CapCacheDTORepository capCacheDTORepository;
//
//    @InjectMocks
//    com.etreetech.panel.service.TrackServiceee trackService;
//
//    //SubAff rule test//
///*
//    @Test
//    public void trackingTestForBlockedSubAffiliate_BusinessRule_TrueCondition() {
//        BusinessRuleCacheDTO businessRuleCacheDTO = new BusinessRuleCacheDTO();
//        businessRuleCacheDTO.setStatusRule(StatusRule.BLOCK_SUB_AFF);
//        businessRuleCacheDTO.setScope(Scope.OFFER);
//        businessRuleCacheDTO.setId(Long.MAX_VALUE - 1000);
//        businessRuleCacheDTO.setName("test_rule_new");
//        businessRuleCacheDTO.setSubPubIds("abcde,ababab");
//
//        AttributionClickTrackingDTO clickTrackingDTO = new AttributionClickTrackingDTO();
//        clickTrackingDTO.setSubAff("abcde");
//        clickTrackingDTO.setClickMessage("");
//
//        when(businessRuleService.fetchBusinessRules(StatusRule.BLOCK_SUB_AFF, new AppTrackingDTO(), new PartnerTrackingDTO()))
//                .thenReturn(businessRuleCacheDTO);
//
//        boolean res = trackService.subAffFilter(new AppTrackingDTO(), new PartnerTrackingDTO(), true, clickTrackingDTO);
//        assertThat(res, equalTo(false));
//        assertThat(clickTrackingDTO.getClickMessage(), equalTo("Rejected: Sub affiliate blocked"));
//    }
//
//    @Test
//    public void trackingTestForBlockedSubAffiliate_BusinessRule_FalseCondition() {
//        BusinessRuleCacheDTO businessRuleCacheDTO = new BusinessRuleCacheDTO();
//        businessRuleCacheDTO.setStatusRule(StatusRule.BLOCK_SUB_AFF);
//        businessRuleCacheDTO.setScope(Scope.OFFER);
//        businessRuleCacheDTO.setId(Long.MAX_VALUE - 1000);
//        businessRuleCacheDTO.setName("test_rule_new");
//        businessRuleCacheDTO.setSubPubIds("abcde,ababab");
//
//        AttributionClickTrackingDTO clickTrackingDTO = new AttributionClickTrackingDTO();
//        clickTrackingDTO.setSubAff("abd");
//        clickTrackingDTO.setClickMessage("");
//
//        when(businessRuleService.fetchBusinessRules(StatusRule.BLOCK_SUB_AFF, new AppTrackingDTO(), new PartnerTrackingDTO()))
//                .thenReturn(businessRuleCacheDTO);
//
//        boolean res = trackService.subAffFilter(new AppTrackingDTO(), new PartnerTrackingDTO(), true, clickTrackingDTO);
//        assertThat(res, equalTo(true));
//    }
//*/
//
//    //Cap Test
//    @Test
//    public void testCapExpiredRule_TrueCondition() {
//        CapCacheDTO cap = new CapCacheDTO();
//        cap.setId(Long.MAX_VALUE);
//        cap.setCapScope(CapScope.GROUP);
//        cap.setCapsType(CapsType.GrossClicks);
//        cap.setDaily(5);
//        cap.setMonthly(15);
//        cap.setLifetime(20);
//        cap.setOfferId(10l);
//        cap.setUsed(true);
//        cap.setPublishers(Collections.singleton(10l));
//
//        when(capCacheDTORepository.findByOfferId(10l))
//                .thenReturn(Collections.singletonList(cap));
//
//        PublisherTrackingDTO publisherTrackingDTO = new PublisherTrackingDTO();
//        publisherTrackingDTO.setId(10l);
//
//        OfferTrackingDTO offerTrackingDTO = new OfferTrackingDTO();
//        offerTrackingDTO.setId(10l);
//
//        ClickTrackingDTO clickTrackingDTO = new ClickTrackingDTO();
//        clickTrackingDTO.setClickMessage("");
//
//        boolean res = trackService.checkCaps(offerTrackingDTO, publisherTrackingDTO, true, clickTrackingDTO);
//
//        assertThat(res, equalTo(false));
//        assertThat(clickTrackingDTO.getClickMessage(), equalTo("Rejected: " + cap.getCapsType() + " cap expired"));
//    }
//
//    @Test
//    public void testCapExpiredRule_FalseCondition() {
//        CapCacheDTO cap = new CapCacheDTO();
//        cap.setId(Long.MAX_VALUE);
//        cap.setCapScope(CapScope.GROUP);
//        cap.setCapsType(CapsType.GrossClicks);
//        cap.setDaily(5);
//        cap.setMonthly(15);
//        cap.setLifetime(20);
//        cap.setOfferId(10l);
//        cap.setUsed(false);
//        cap.setPublishers(Collections.singleton(10l));
//
//        when(capCacheDTORepository.findByOfferId(10l))
//                .thenReturn(Collections.singletonList(cap));
//
//        PublisherTrackingDTO publisherTrackingDTO = new PublisherTrackingDTO();
//        publisherTrackingDTO.setId(10l);
//
//        OfferTrackingDTO offerTrackingDTO = new OfferTrackingDTO();
//        offerTrackingDTO.setId(10l);
//
//        ClickTrackingDTO clickTrackingDTO = new ClickTrackingDTO();
//        clickTrackingDTO.setClickMessage("");
//
//        boolean res = trackService.checkCaps(offerTrackingDTO, publisherTrackingDTO, true, clickTrackingDTO);
//
//        assertThat(res, equalTo(true));
//    }
//
//    //Advertiser rule test
//    @Test
//    public void testAdvertiserActive_True() {
//        AdvertiserTrackingDTO advertiserTrackingDTO = new AdvertiserTrackingDTO();
//        advertiserTrackingDTO.setStatus(false);
//
//        PublisherTrackingDTO publisherTrackingDTO = new PublisherTrackingDTO();
//        publisherTrackingDTO.setId(10l);
//
//        OfferTrackingDTO offerTrackingDTO = new OfferTrackingDTO();
//        offerTrackingDTO.setId(10l);
//
//        ClickTrackingDTO clickTrackingDTO = new ClickTrackingDTO();
//        clickTrackingDTO.setClickMessage("");
//
//        boolean res = trackService.checkAdvertiserActive(clickTrackingDTO, advertiserTrackingDTO, true);
//
//        assertThat(res, equalTo(false));
//        assertThat(clickTrackingDTO.getClickMessage(), equalTo("Rejected: Advertiser status Inactive"));
//    }
//
//    @Test
//    public void testAdvertiserActive_false() {
//        AdvertiserTrackingDTO advertiserTrackingDTO = new AdvertiserTrackingDTO();
//        advertiserTrackingDTO.setStatus(true);
//
//        PublisherTrackingDTO publisherTrackingDTO = new PublisherTrackingDTO();
//        publisherTrackingDTO.setId(10l);
//
//        OfferTrackingDTO offerTrackingDTO = new OfferTrackingDTO();
//        offerTrackingDTO.setId(10l);
//
//        ClickTrackingDTO clickTrackingDTO = new ClickTrackingDTO();
//        clickTrackingDTO.setClickMessage("");
//
//        boolean res = trackService.checkAdvertiserActive(clickTrackingDTO, advertiserTrackingDTO, true);
//
//        assertThat(res, equalTo(true));
//    }
//
//    //Publisher rule test
//    @Test
//    public void testPublisherActiveTrue() {
//        AdvertiserTrackingDTO advertiserTrackingDTO = new AdvertiserTrackingDTO();
//        advertiserTrackingDTO.setStatus(true);
//        PublisherTrackingDTO publisherTrackingDTO = new PublisherTrackingDTO();
//        publisherTrackingDTO.setId(10l);
//        publisherTrackingDTO.setStatus(false);
//
//        OfferTrackingDTO offerTrackingDTO = new OfferTrackingDTO();
//        offerTrackingDTO.setId(10l);
//
//        ClickTrackingDTO clickTrackingDTO = new ClickTrackingDTO();
//        clickTrackingDTO.setClickMessage("");
//
//        boolean res = trackService.checkPublisherActive(clickTrackingDTO, publisherTrackingDTO, true);
//
//        assertThat(res, equalTo(false));
//        assertThat(clickTrackingDTO.getClickMessage(), equalTo("Rejected: Publisher status Inactive"));
//    }
//
//    @Test
//    public void testPublisherActiveFalse() {
//        AdvertiserTrackingDTO advertiserTrackingDTO = new AdvertiserTrackingDTO();
//        advertiserTrackingDTO.setStatus(true);
//        PublisherTrackingDTO publisherTrackingDTO = new PublisherTrackingDTO();
//        publisherTrackingDTO.setId(10l);
//        publisherTrackingDTO.setStatus(true);
//
//        OfferTrackingDTO offerTrackingDTO = new OfferTrackingDTO();
//        offerTrackingDTO.setId(10l);
//
//        ClickTrackingDTO clickTrackingDTO = new ClickTrackingDTO();
//        clickTrackingDTO.setClickMessage("");
//
//        boolean res = trackService.checkPublisherActive(clickTrackingDTO, publisherTrackingDTO, true);
//
//        assertThat(res, equalTo(true));
//    }
//
//    @Test
//    public void testPublisherIsApprovedByAdvertiserTrue() {
//        AdvertiserTrackingDTO advertiserTrackingDTO = new AdvertiserTrackingDTO();
//        advertiserTrackingDTO.setStatus(true);
//
//        PublisherTrackingDTO publisherTrackingDTO = new PublisherTrackingDTO();
//        publisherTrackingDTO.setId(10l);
//
//        OfferTrackingDTO offerTrackingDTO = new OfferTrackingDTO();
//        offerTrackingDTO.setId(10l);
//
//        ClickTrackingDTO clickTrackingDTO = new ClickTrackingDTO();
//        clickTrackingDTO.setClickMessage("");
//
//        boolean res = trackService.checkPublisherApprovedByAdvertiser(clickTrackingDTO, advertiserTrackingDTO, publisherTrackingDTO, true);
//
//        assertThat(res, equalTo(false));
//        assertThat(clickTrackingDTO.getClickMessage(), equalTo("Rejected: Publisher not Approved By Advertiser"));
//    }
//
//    @Test
//    public void testPublisherIsApprovedByAdvertiserFalse() {
//        PublisherTrackingDTO publisherTrackingDTO = new PublisherTrackingDTO();
//        publisherTrackingDTO.setId(10l);
//
//        AdvertiserTrackingDTO advertiserTrackingDTO = new AdvertiserTrackingDTO();
//        advertiserTrackingDTO.setApprovedPubs(Collections.singletonList(publisherTrackingDTO.getId()));
//        advertiserTrackingDTO.setStatus(true);
//
//        OfferTrackingDTO offerTrackingDTO = new OfferTrackingDTO();
//        offerTrackingDTO.setId(10l);
//
//        ClickTrackingDTO clickTrackingDTO = new ClickTrackingDTO();
//        clickTrackingDTO.setClickMessage("");
//
//        boolean res = trackService.checkPublisherApprovedByAdvertiser(clickTrackingDTO, advertiserTrackingDTO, publisherTrackingDTO, true);
//
//        assertThat(res, equalTo(true));
//    }
//
//    //Offer test
//    @Test
//    public void testOfferActiveTrue() {
//        OfferTrackingDTO offerTrackingDTO = new OfferTrackingDTO();
//        offerTrackingDTO.setId(10l);
//        offerTrackingDTO.setStatus(OfferStatus.Pause);
//
//        ClickTrackingDTO clickTrackingDTO = new ClickTrackingDTO();
//        clickTrackingDTO.setClickMessage("");
//
//        boolean res = trackService.checkOfferActive(clickTrackingDTO, offerTrackingDTO, true);
//
//        assertThat(res, equalTo(false));
//        assertThat(clickTrackingDTO.getClickMessage(), equalTo("Rejected: Offer " + OfferStatus.Pause));
//    }
//
//    @Test
//    public void testOfferActiveFalse() {
//        OfferTrackingDTO offerTrackingDTO = new OfferTrackingDTO();
//        offerTrackingDTO.setId(10l);
//        offerTrackingDTO.setStatus(OfferStatus.Active);
//
//        ClickTrackingDTO clickTrackingDTO = new ClickTrackingDTO();
//        clickTrackingDTO.setClickMessage("");
//
//        boolean res = trackService.checkOfferActive(clickTrackingDTO, offerTrackingDTO, true);
//
//        assertThat(res, equalTo(true));
//    }
//
//    //Campaign tests
//    @Test
//    public void testNonPublicCampaignIsNullTrue() {
//        OfferTrackingDTO offerTrackingDTO = new OfferTrackingDTO();
//        offerTrackingDTO.setId(10l);
//        offerTrackingDTO.setAccessStatus(AccessStatus.Private);
//
//        offerTrackingDTO.setStatus(OfferStatus.Pause);
//
//        ClickTrackingDTO clickTrackingDTO = new ClickTrackingDTO();
//        clickTrackingDTO.setClickMessage("");
//
//
//        boolean res = trackService.checkNonPublicCampaignIsNullOrBlocked(clickTrackingDTO, null, true, offerTrackingDTO);
//
//        assertThat(res, equalTo(false));
//        assertThat(clickTrackingDTO.getClickMessage(), equalTo("Rejected: Campaign not approved"));
//    }
//
//    @Test
//    public void testNonPublicCampaignIsNullFalse() {
//        OfferTrackingDTO offerTrackingDTO = new OfferTrackingDTO();
//        offerTrackingDTO.setId(10l);
//        offerTrackingDTO.setAccessStatus(AccessStatus.Public);
//        offerTrackingDTO.setStatus(OfferStatus.Active);
//
//        ClickTrackingDTO clickTrackingDTO = new ClickTrackingDTO();
//        clickTrackingDTO.setClickMessage("");
//
//        boolean res = trackService.checkNonPublicCampaignIsNullOrBlocked(clickTrackingDTO, null, true, offerTrackingDTO);
//
//        assertThat(res, equalTo(true));
//    }
//
//    @Test
//    public void testNonPublicCampaignIsBlockedTrue() {
//        OfferTrackingDTO offerTrackingDTO = new OfferTrackingDTO();
//        offerTrackingDTO.setId(10l);
//        offerTrackingDTO.setAccessStatus(AccessStatus.Public);
//        offerTrackingDTO.setStatus(OfferStatus.Active);
//
//        ClickTrackingDTO clickTrackingDTO = new ClickTrackingDTO();
//        clickTrackingDTO.setClickMessage("");
//
//        CampaignTrackingDTO campaignTrackingDTO = new CampaignTrackingDTO();
//        campaignTrackingDTO.setCampaignStatus(CampaignStatus.BLOCKED);
//
//        boolean res = trackService.checkNonPublicCampaignIsNullOrBlocked(clickTrackingDTO, campaignTrackingDTO, true, offerTrackingDTO);
//
//        assertThat(res, equalTo(false));
//        assertThat(clickTrackingDTO.getClickMessage(), equalTo("Rejected: Campaign Status " + CampaignStatus.BLOCKED));
//    }
//
//    @Test
//    public void testNonPublicCampaignIsBlockedFalse() {
//        OfferTrackingDTO offerTrackingDTO = new OfferTrackingDTO();
//        offerTrackingDTO.setId(10l);
//        offerTrackingDTO.setAccessStatus(AccessStatus.Public);
//        offerTrackingDTO.setStatus(OfferStatus.Active);
//
//        ClickTrackingDTO clickTrackingDTO = new ClickTrackingDTO();
//        clickTrackingDTO.setClickMessage("");
//
//        CampaignTrackingDTO campaignTrackingDTO = new CampaignTrackingDTO();
//        campaignTrackingDTO.setCampaignStatus(CampaignStatus.APPROVED);
//
//        boolean res = trackService.checkNonPublicCampaignIsNullOrBlocked(clickTrackingDTO, campaignTrackingDTO, true, offerTrackingDTO);
//
//        assertThat(res, equalTo(true));
//    }
//
//    //Geo test
//    @Test
//    public void testCountryTrue() {
//        OfferTrackingDTO offerTrackingDTO = new OfferTrackingDTO();
//        offerTrackingDTO.setCountryEnum(Collections.singleton(CountryEnum.AT));
//
//        ClickTrackingDTO clickTrackingDTO = new ClickTrackingDTO();
//        clickTrackingDTO.setClickMessage("");
//        clickTrackingDTO.setCountry(CountryEnum.AE);
//
//        boolean res = trackService.checkCountry(clickTrackingDTO, offerTrackingDTO, true);
//
//        assertThat(res, equalTo(false));
//        assertThat(clickTrackingDTO.getClickMessage(), equalTo("Rejected: Geo blocked"));
//    }
//
//    @Test
//    public void testCountryFalse() {
//        OfferTrackingDTO offerTrackingDTO = new OfferTrackingDTO();
//        offerTrackingDTO.setCountryEnum(Collections.singleton(CountryEnum.ALL));
//
//        ClickTrackingDTO clickTrackingDTO = new ClickTrackingDTO();
//        clickTrackingDTO.setClickMessage("");
//        clickTrackingDTO.setCountry(CountryEnum.AT);
//
//        boolean res = trackService.checkCountry(clickTrackingDTO, offerTrackingDTO, true);
//
//        assertThat(res, equalTo(true));
//    }
//
//    //Device tests
//    @Test
//    public void trackingTestForGrossDeviceDesktop() {
//        OfferTrackingDTO offerTrackingDTO = new OfferTrackingDTO();
//        offerTrackingDTO.setOfferType("Desktop");
//        offerTrackingDTO.setDevOS("All");
//
//        ClickTrackingDTO clickTrackingDTO = new ClickTrackingDTO();
//        clickTrackingDTO.setClickMessage("");
//
//        String agent = "Mozilla/5.0 (Linux; Android 7.0; SM-A310F Build/NRD90M) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.91 Mobile Safari/537.36 OPR/42.7.2246.114996";
//        clickTrackingDTO.setAgent(agent);
//
//        boolean res = trackService.checkOs(clickTrackingDTO, offerTrackingDTO, true, "Rejected: ");
//
//        assertThat(res, equalTo(false));
//        assertThat(clickTrackingDTO.getClickMessage(), equalTo("Rejected: OS blocked"));
//    }
//
//    @Test
//    public void trackingTestForGrossDeviceMobileOsAndroid() {
//        OfferTrackingDTO offerTrackingDTO = new OfferTrackingDTO();
//        offerTrackingDTO.setOfferType("Mobile");
//        offerTrackingDTO.setDevOS("Android");
//
//        ClickTrackingDTO clickTrackingDTO = new ClickTrackingDTO();
//        clickTrackingDTO.setClickMessage("");
//
//        String agent = "Mozilla/5.0 (iPhone; CPU iPhone OS 13_7 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148";
//        clickTrackingDTO.setAgent(agent);
//
//        boolean res = trackService.checkOs(clickTrackingDTO, offerTrackingDTO, true, "Rejected: ");
//
//        assertThat(res, equalTo(false));
//        assertThat(clickTrackingDTO.getClickMessage(), equalTo("Rejected: OS blocked"));
//    }
//
//    @Test
//    public void trackingTestForGrossDeviceMobileOsIOS() {
//        OfferTrackingDTO offerTrackingDTO = new OfferTrackingDTO();
//        offerTrackingDTO.setOfferType("Mobile");
//        offerTrackingDTO.setDevOS("iOS");
//
//        ClickTrackingDTO clickTrackingDTO = new ClickTrackingDTO();
//        clickTrackingDTO.setClickMessage("");
//
//        String agent = "Mozilla/5.0 (Linux; Android 7.0; SM-A310F Build/NRD90M) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.91 Mobile Safari/537.36 OPR/42.7.2246.114996";
//        clickTrackingDTO.setAgent(agent);
//
//        boolean res = trackService.checkOs(clickTrackingDTO, offerTrackingDTO, true, "Rejected: ");
//
//        assertThat(res, equalTo(false));
//        assertThat(clickTrackingDTO.getClickMessage(), equalTo("Rejected: OS blocked"));
//    }
//
//    @Test
//    public void trackingTestForGrossDeviceDesktopAndMobileOsAll() {
//        OfferTrackingDTO offerTrackingDTO = new OfferTrackingDTO();
//        offerTrackingDTO.setOfferType("Mobile,Desktop");
//        offerTrackingDTO.setDevOS("All");
//
//        ClickTrackingDTO clickTrackingDTO = new ClickTrackingDTO();
//        clickTrackingDTO.setClickMessage("");
//
//        String agent = "Mozilla/5.0 (Linux; Android 7.0; SM-A310F Build/NRD90M) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.91 Mobile Safari/537.36 OPR/42.7.2246.114996";
//        clickTrackingDTO.setAgent(agent);
//
//        boolean res = trackService.checkOs(clickTrackingDTO, offerTrackingDTO, true, "Rejected: ");
//
//        assertThat(res, equalTo(true));
////        assertThat(clickTrackingDTO.getClickMessage(), equalTo("Rejected: OS blocked"));
//    }
//
//    @Test
//    public void trackingTestForGrossDeviceAllOsAndroidAndiOS() {
//        OfferTrackingDTO offerTrackingDTO = new OfferTrackingDTO();
//        offerTrackingDTO.setOfferType("All");
//        offerTrackingDTO.setDevOS("Android,iOS");
//
//        ClickTrackingDTO clickTrackingDTO = new ClickTrackingDTO();
//        clickTrackingDTO.setClickMessage("");
//
//        String agent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1636.0 Safari/537.36";
//
//        clickTrackingDTO.setAgent(agent);
//
//        boolean res = trackService.checkOs(clickTrackingDTO, offerTrackingDTO, true, "Rejected: ");
//
//        assertThat(res, equalTo(true));
////        assertThat(clickTrackingDTO.getClickMessage(), equalTo("Rejected: OS blocked"));
//    }
//
//    @Test
//    public void trackingTestForGrossDeviceAllOsAll() {
//        OfferTrackingDTO offerTrackingDTO = new OfferTrackingDTO();
//        offerTrackingDTO.setOfferType("All");
//        offerTrackingDTO.setDevOS("All");
//
//        ClickTrackingDTO clickTrackingDTO = new ClickTrackingDTO();
//        clickTrackingDTO.setClickMessage("");
//
//        String agent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1636.0 Safari/537.36";
//
//        clickTrackingDTO.setAgent(agent);
//
//        boolean res = trackService.checkOs(clickTrackingDTO, offerTrackingDTO, true, "Rejected: ");
//
//        assertThat(res, equalTo(true));
//    }
//
//
//}
//
//
